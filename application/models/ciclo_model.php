<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ciclo_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function get_datos_proveedor($id) {
        $query = "SELECT * FROM cat_proveedores WHERE clave_prov = ?;";
        $query = $this->db->query($query, array($id));
        $result = $query->row();
        return $result;
    }

    function get_datos_beneficiario($id) {
        $query = "SELECT * FROM cat_beneficiarios WHERE id_beneficiarios = ?;";
        $query = $this->db->query($query, array($id));
        $result = $query->row();
        return $result;
    }

    function ultimo_precompromiso() {
        $sql = "SELECT numero_pre as ultimo FROM mov_precompromiso_caratula ORDER BY numero_pre DESC LIMIT 1;";
        $query = $this->db->query($sql);
        $row = $query->row();
        if(!$row) {
            $row = 0;
        }
        return $row;
    }

    function apartarPrecompromiso($ultimo) {
        $id = $this->tank_auth->get_user_id();

        $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
        $resultado_query_usuario = $this->db->query($query_usuario, array($id));
        $nombre_encontrado = $resultado_query_usuario->row();
        $nombre_completo = $nombre_encontrado->nombre." ".$nombre_encontrado->apellido_paterno." ".$nombre_encontrado->apellido_materno;

        $data = array(
            'numero_pre' => $ultimo,
            'estatus' => "espera",
            'id_usuario' => $this->tank_auth->get_user_id(),
            'creado_por' => $nombre_completo,
        );

        $query = $this->db->insert('mov_precompromiso_caratula', $data);

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function datos_precompromisoCaratula(){
        $sql = "SELECT * FROM mov_precompromiso_caratula ORDER BY numero_pre DESC;";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    function datos_precompromisoCaratulaEstatus($sql){
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    function datos_precompromisoDetalle($precompromiso, $sql) {
        $query = $this->db->query($sql, array($precompromiso));
        $result = $query->result();
        return $result;
    }

    function datos_gasto() {
        $sql = "SELECT * FROM cat_conceptos_gasto;";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    function cancelarPrecompromisoCaratula($precompromiso, $query) {
        $query = $this->db->query($query, array(
            date("Y-m-d"), $precompromiso,
        ));
        if($query){
            return TRUE;
        }
        else {
            return FALSE;
        }

    }

    function cancelarPrecompromiso($precompromiso) {

        $this->db->where('id_precompromiso_detalle', $precompromiso);
        $this->db->select('*');
        $query = $this->db->get('mov_precompromiso_detalle');
        $result = $query->row();

        $this->db->where('numero_pre', $result->numero_pre);
        $this->db->select('fecha_autoriza');
        $query2 = $this->db->get('mov_precompromiso_caratula');
        $result_caratula = $query2->row();

        $fecha = $result_caratula->fecha_autoriza;

        $mes_actual = $this->utilerias->convertirFechaAMes($fecha);

//        Se tiene que comprobar si hay dinero para hacer el precompromiso
        $query_revisar_dinero = "SELECT COLUMN_GET(nivel, 'total_anual' as char) AS total_anual,
                                    COLUMN_GET(nivel, '".$mes_actual."_saldo' as char) AS mes_saldo,
                                    COLUMN_GET(nivel, '".$mes_actual."_precompromiso' as char) AS mes_precompromiso,
                                    COLUMN_GET(nivel, 'precomprometido_anual' as char) AS precomprometido_anual
                                    FROM cat_niveles WHERE id_niveles = ?";

        $resultado_dinero = $this->ciclo_model->revisarDinero($result->id_nivel, $query_revisar_dinero);

        $precompromiso_mes = $resultado_dinero->mes_precompromiso - $result->importe;

        $precompromiso_anual = $resultado_dinero->precomprometido_anual - $result->importe;

        $query_transaccion = "UPDATE cat_niveles
                              SET nivel = COLUMN_ADD(nivel, '".$mes_actual."_precompromiso', ?),
                              nivel = COLUMN_ADD(nivel, 'precomprometido_anual', ?)
                              WHERE id_niveles = ?;";

        $resultado_actualizar = $this->ciclo_model->actualizar_total_anual_precompromiso($query_transaccion, $precompromiso_mes, $precompromiso_anual, $result->id_nivel);

        if ($resultado_actualizar) {
            return TRUE;
        } else {
            return FALSE;
        }

    }

    function borrarDetallePrecompromiso($precompromiso) {
        $this->db->where('id_precompromiso_detalle', $precompromiso);
        $query = $this->db->delete('mov_precompromiso_detalle');
        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }

    }

    function borrarDetallePrecompromisoViatico($viatico) {
        
        $this->db->where('id_tabla_lugares_periodos', $viatico);
        $query = $this->db->delete('mov_tabla_lugares_periodos');

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function tomarDetalleBorrar($precompromiso) {
        $this->db->select('id_precompromiso_detalle, COLUMN_JSON(nivel) AS estructura, importe')->from('mov_precompromiso_detalle')->where('numero_pre', $precompromiso);
        $query = $this->db->get();
        return $query->result_array();
    }

    /**
     * Esta función sirve para saber si existe la estructura ingresada
     * @param $datos
     * @param $query
     * @return mixed
     */
    function existeEstructura($datos, $query) {
        $query = $this->db->query($query, array(
            $datos["nivel1"],
            $datos["nivel2"],
            $datos["nivel3"],
            $datos["nivel4"],
            $datos["nivel5"],
            $datos["nivel6"]));
        $result = $query->row();
        return $result;
    }

    function existeEstructuraArchivo($datos, $query) {
        $query = $this->db->query($query, array(
            $datos["TIPO PRECOMPROMISO"],
            $datos["PROVEEDOR"],
            $datos["TIPO DE GARANTIA"],
            $datos["PORCENTAJE DE GARANTIA"],
            $datos["IMPORTE DE RESPONSABILIDAD CIVIL"],
            $datos["IVA"]));
        $result = $query->row();
        return $result;
    }

    function revisarDinero($id_nivel, $query) {
        $query = $this->db->query($query, array($id_nivel));
        $result = $query->row();
        return $result;
    }

    /**
     * Esta función se encarga de insertar los datos del precompromiso en la carátula.
     * @param $datos
     * @return bool
     */
    function insertar_caratula_precompromiso($datos) {

        if(!$datos["total_hidden"]) {
            $datos["total_hidden"] = 0;
        }

        if(!$datos["fecha_entrega"]) {
            $datos["fecha_entrega"] = "0000-00-00";
        }
        
        if(!$datos["numero_plazo"]) {
            $datos["numero_plazo"] = 0;
        }

        if(!$datos["importe_civil"]) {
            $datos["importe_civil"] = 0;
        }

//        Este arreglo son los datos que van a ser insertados en la caratula del precompromiso
        $data = array(
            "numero_pre" => $datos["ultimo_pre"],
            "tipo_requisicion" => $datos["tipo_precompromiso"],
            "tipo_gasto" => $datos["tipo_radio"],
            "enfirme" => $datos["check_firme"],
            "firma1" => $datos["firma1"],
            "firma2" => $datos["firma2"],
            "firma3" => $datos["firma3"],
            "fecha_emision" => $datos["fecha_solicitud"],
            "fecha_autoriza" => $datos["fecha_autoriza"],
            "fecha_programada" => $datos["fecha_entrega"],
            "total" => $datos["total_hidden"],
            "estatus" => $datos["estatus"],
            "concepto_especifico" => $datos["concepto_especifico"],
            "descripcion" => $datos["descripcion_general"],
            "ley_aplicable" => $datos["ley"],
            "articulo_procedencia" => $datos["articulo"],
            "lugar_entrega" => $datos["lugar_entrega"],
            "plazo_numero" => $datos["numero_plazo"],
            "plazo_tipo" => $datos["tipo_plazo"],
            "plurianualidad" => $datos["plurianualidad"],
            "tipo_garantia" => $datos["tipo_garantia"],
            "porciento_garantia" => $datos["porciento_garantia"],
            "anexos" => $datos["anexos"],
            "id_proveedor" => $datos["id_proveedor"],
            "proveedor" => $datos["proveedor"],
            "importe_responsabilidad_civil" => $datos["importe_civil"],
            "lugar_adquisiciones" => $datos["lugar_adquisicion"],
            "existencia_almacen" => $datos["almacen"],
            "norma_inspeccion" => $datos["normas"],
            "registros_sanitarios" => $datos["registros"],
            "capacitacion" => $datos["capacitacion"],
            "id_persona" => $datos["id_persona"],
            "nombre_completo" => $datos["nombre_persona"],
            "puesto" => $datos["puesto"],
            "clave" => $datos["clave"],
            "area" => $datos["area"],
            "no_empleado" => $datos["no_empleado"],
            "forma_pago" => $datos["forma_pago"],
            "transporte" => $datos["transporte"],
            "grupo_jerarquico" => $datos["grupo_jerarquico"],
            "zona_marginada" => $datos["zona_marginada_check"],
            "zona_mas_economica" => $datos["zona_mas_economica_check"],
            "zona_menos_economica" => $datos["zona_menos_economica_check"],
        );

//        Esta función es la que se encarga de actualizar el precompromiso
        $this->db->where('numero_pre', $datos["ultimo_pre"]);
        $query = $this->db->update('mov_precompromiso_caratula', $data);

//        Si se pudieron insertar los datos, se devuelve una variable booleana como verdadera
        if($query) {
            return TRUE;
        }
//        De lo contrario devuelve una variable booleana falsa
        else {
            return FALSE;
        }

    }

    function insertar_caratula_precompromisoArchivo($datos, $ultimo) {
        $id = $this->tank_auth->get_user_id();
        /** En esta parte se toma el nombre del usuario */
        $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
        $resultado_query_usuario = $this->db->query($query_usuario, array($id));
        $nombre_encontrado = $resultado_query_usuario->row();
        $nombre_completo = $nombre_encontrado->nombre." ".$nombre_encontrado->apellido_paterno." ".$nombre_encontrado->apellido_materno;

        /** En esta parte se toma el nomnbre del proveedor */
        $query_proveedor = "SELECT nombre FROM cat_proveedores WHERE clave_prov = ?;";
        $resultado_query_proveedor = $this->db->query($query_proveedor, array($datos["B"]));
        $nombre_proveedor = $resultado_query_proveedor->row();

        if(!$nombre_proveedor) {
            $DB1 = $this->load->database('pedidos', TRUE);
            
            $sql_caratula = "SELECT nomedi AS nombre
                                FROM editor
                                WHERE cveedi = ?";
            $query_caratula = $DB1->query($sql_caratula, array($datos["B"]));
            $nombre_proveedor = $query_caratula->row();
        }

        if($datos["S"] == "Si") {
            $anexos = 1;
        } elseif($datos["S"] == "No") {
            $anexos = 0;
        }

        if(trim($datos["H"]) == "Gasto Corriente") {
            $datos["H"] = 1;
        } elseif(trim($datos["E"]) == "Gasto de Capital") {
            $datos["H"] = 2;
        } elseif(trim($datos["E"]) == "Amortización de la cuenta y disminución de pasivos") {
            $datos["H"] = 3;
        }

        $fecha = ($datos['O'] - 25569) * 86400;
        $fecha = date('Y-m-d', $fecha);

        $data = array(
            "numero_pre" => $ultimo,
            "tipo_requisicion" => ucfirst(strtolower($datos["A"])),
            "tipo_gasto" => $datos["H"],
            "enfirme" => 0,
            "firma1" => 0,
            "firma2" => 0,
            "firma3" => 0,
            "fecha_emision" => date("Y-m-d"),
            "fecha_autoriza" => 0,
            "fecha_programada" => $fecha,
            "subtotal" => 0,
            "total" => 0,
            "estatus" => "espera",
            "id_usuario" => $this->tank_auth->get_user_id(),
            "creado_por" => $nombre_completo,
            "descripcion" => $datos["T"],
            "ley_aplicable" => $datos["I"],
            "articulo_procedencia" => $datos["J"],
            "lugar_entrega" => $datos["F"],
            "plazo_numero" => $datos["Q"],
            "plazo_tipo" => $datos["R"],
            "plurianualidad" => $datos["K"],
            "tipo_garantia" => $datos["C"],
            "porciento_garantia" => $datos["D"],
            "anexos" => $anexos,
            "id_proveedor" => $datos["B"],
            "proveedor" => $nombre_proveedor->nombre,
            "importe_responsabilidad_civil" => $datos["E"],
            "lugar_adquisiciones" => $datos["F"],
            "existencia_almacen" => $datos["G"],
            "norma_inspeccion" => $datos["L"],
            "registros_sanitarios" => $datos["M"],
            "capacitacion" => $datos["N"],
        );

        $query = $this->db->insert('mov_precompromiso_caratula', $data);

        if($query) {
            return $this->db->insert_id();
        }
        else {
            return FALSE;
        }
    }

    function insertar_caratula_precompromiso_cancelado($datos) {
        $id = $this->tank_auth->get_user_id();

        $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
        $resultado_query_usuario = $this->db->query($query_usuario, array($id));
        $nombre_encontrado = $resultado_query_usuario->row();
        $nombre_completo = $nombre_encontrado->nombre." ".$nombre_encontrado->apellido_paterno." ".$nombre_encontrado->apellido_materno;

        if(!$datos["total_hidden"]) {
            $datos["total_hidden"] = 0;
        }

        $data = array(
            "numero_pre" => $datos["ultimo_pre"],
            "tipo_requisicion" => $datos["tipo_precompromiso"],
            "tipo_gasto" => $datos["tipo_radio"],
            "enfirme" => $datos["check_firme"],
            "firma1" => $datos["firma1"],
            "firma2" => $datos["firma2"],
            "firma3" => $datos["firma3"],
            "fecha_emision" => $datos["fecha_solicitud"],
            "fecha_autoriza" => "0000-00-00",
            "fecha_programada" => "0000-00-00",
            "total" => $datos["total_hidden"],
            "estatus" => $datos["estatus"],
            "id_usuario" => $this->tank_auth->get_user_id(),
            "creado_por" => $nombre_completo,
            "descripcion" => $datos["descripcion_general"],
            "ley_aplicable" => $datos["ley"],
            "articulo_procedencia" => $datos["articulo"],
            "lugar_entrega" => $datos["lugar_entrega"],
            "plazo_numero" => 0,
            "plazo_tipo" => $datos["tipo_plazo"],
            "plurianualidad" => $datos["plurianualidad"],
            "tipo_garantia" => $datos["tipo_garantia"],
            "porciento_garantia" => $datos["porciento_garantia"],
            "anexos" => $datos["anexos"],
            "id_proveedor" => $datos["id_proveedor"],
            "proveedor" => $datos["proveedor"],
            "importe_responsabilidad_civil" => 0,
            "lugar_adquisiciones" => $datos["lugar_adquisicion"],
            "existencia_almacen" => $datos["almacen"],
            "norma_inspeccion" => $datos["normas"],
            "registros_sanitarios" => $datos["registros"],
            "capacitacion" => $datos["capacitacion"],
            'usada' => 0,
            'cancelada' => 1,
            'fecha_cancelada' => date("Y-m-d"),
        );

        $this->db->where('numero_pre', $datos["ultimo_pre"]);
        $query = $this->db->update('mov_precompromiso_caratula', $data);

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }

    }

    function get_datos_precompromiso_caratula($precompromiso, $query) {
        $query = $this->db->query($query, array($precompromiso));
        $result = $query->row();
        return $result;
    }

    function get_datos_precompromiso_detalle($precompromiso){
        $query = "SELECT *, COLUMN_JSON(nivel) AS estructura FROM mov_precompromiso_detalle WHERE numero_pre = ?;";
        $query = $this->db->query($query, array($precompromiso));
        $result = $query->result();
        return $result;
    }

    function get_datos_precompromiso_lugares($precompromiso){
        $query = "SELECT * FROM mov_tabla_lugares_periodos WHERE numero_precompromiso = ?;";
        $query = $this->db->query($query, array($precompromiso));
        $result = $query->result();
        return $result;
    }

    function insertar_detalle_precompromiso($datos, $query) {
        $query = $this->db->query($query, array(
            $datos["ultimo_pre"],
            $datos["id_nivel"],
            $datos["gasto"],
            $datos["u_medida"],
            $datos["cantidad"],
            $datos["precio"],
            $datos["subtotal"],
            $datos["iva"],
            $datos["importe"],
            $datos["titulo_gasto"],
            date("Y"),
            $datos["descripcion_detalle"],
            $datos["nivel1"],
            $datos["nivel2"],
            $datos["nivel3"],
            $datos["nivel4"],
            $datos["nivel5"],
            $datos["nivel6"],
            $datos["titulo_gasto"]));
        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function insertar_detalle_precompromisoArchivo($datos, $query) {
        /** En esta parte se toma el nombre del gasto */
        $query_gasto = "SELECT descripcion FROM cat_conceptos_gasto WHERE articulo = ?;";
        $resultado_query_gasto = $this->db->query($query_gasto, array($datos["G"]));
        $nombre_gasto = $resultado_query_gasto->row();

        $query = $this->db->query($query, array(
            $datos["precompromiso"],
            $datos["id_nivel"],
            $datos["G"],
            $datos["H"],
            $datos["I"],
            $datos["K"],
            $datos["subtotal"],
            $datos["iva"],
            $datos["importe"],
            $nombre_gasto->descripcion,
            date("Y"),
            $datos["L"],
            $datos["A"],
            $datos["B"],
            $datos["C"],
            $datos["D"],
            $datos["E"],
            $datos["F"],
            $nombre_gasto->descripcion));
        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function insertar_detalle_viaticos_precompromiso($datos) {

        $data = array(
            'numero_precompromiso' => $datos['ultimo_pre'],
            'lugar' => $datos['lugar_comision'],
            'periodo_inicio' => $datos['fecha_inicio'],
            'periodo_fin' => $datos['fecha_termina'],
            'cuota_diaria' => $datos['cuota_diaria'],
            'dias' => $datos['dias'],
            'importe' => $datos['importe'],
        );

        $query = $this->db->insert('mov_tabla_lugares_periodos', $data);

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function editarDetallePrecompromiso($datos) {

        $this->db->where('id_precompromiso_detalle', $datos["id_precompromiso"]);
        $this->db->select('id_nivel, id_precompromiso_detalle AS numero_precompromiso');
        $query = $this->db->get('mov_precompromiso_detalle');
        $result = $query->row();

        $data = array(
            'cantidad' => $datos["cantidad"],
            'p_unitario' => $datos["precio"],
            'subtotal' => $datos["subtotal"],
            'iva' => $datos["iva"],
            'importe' => $datos["importe"],
        );

        $this->db->where('id_precompromiso_detalle', $datos["id_precompromiso"]);
        $this->db->update('mov_precompromiso_detalle', $data);
        if ($query) {
            return TRUE;
        } else {
            return FALSE;
        }

    }

    function actualizarTotalCaratulaPrecompromiso($id, $total){
        $data = array(
            'total' => $total,
        );

        $this->db->where('id_precompromiso_caratula', $id);
        $this->db->update('mov_precompromiso_caratula', $data);
    }

    function actualizar_total_anual_precompromiso($query, $mes, $anual, $nivel) {
        $query = $this->db->query($query, array(
            $mes,
            $anual,
            $nivel));

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function datos_viaticosDetalle($precompromiso) {
        $this->db->select('id_tabla_lugares_periodos, lugar, periodo_inicio, periodo_fin, cuota_diaria, dias, importe')->from('mov_tabla_lugares_periodos')->where('numero_precompromiso', $precompromiso);
        $query = $this->db->get();
        return $query->result();
    }

    function solicitar_terminar_precompromiso($precompromiso) {
        $data = array(
            'solicitar_terminar' => 1,
        );

        $this->db->where('numero_pre', $precompromiso);
        $query = $this->db->update('mov_precompromiso_caratula', $data);

        if($query) {
            return TRUE;
        } else  {
            return FALSE;
        }
    }

    function checar_comprmisos_cancelar_precompromisos($precompromiso){
        $this->db->select('numero_compromiso')->from('mov_compromiso_caratula')->where('num_precompromiso', $precompromiso)->where('enfirme', 1)->where('firma1', 1)->where('firma2', 1)->where('firma3', 1)->where('cancelada', 0);
        $query = $this->db->get();
        return $query->result_array();
    }


    /**
     * Aqui comienza la seccion de compromiso
     */

    function ultimo_compromiso() {
        $sql = "SELECT numero_compromiso as ultimo FROM mov_compromiso_caratula ORDER BY numero_compromiso DESC LIMIT 1;";
        $query = $this->db->query($sql);
        $row = $query->row();
        if(!$row) {
            $row = 0;
        }
        return $row;
    }

    function ultimo_tipoimpresion($datos) {
        $sql = "SELECT num_impresion as ultimo FROM mov_compromiso_caratula WHERE tipo_impresion = ? ORDER BY num_impresion DESC LIMIT 1;";
        $query = $this->db->query($sql, array($datos["tipo"]));
        $row = $query->row();
        if(!$row) {
            $row = 0;
        }
        return $row;
    }

    function apartarcompromiso($ultimo) {
        $id = $this->tank_auth->get_user_id();

        $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
        $resultado_query_usuario = $this->db->query($query_usuario, array($id));
        $nombre_encontrado = $resultado_query_usuario->row();
        $nombre_completo = $nombre_encontrado->nombre." ".$nombre_encontrado->apellido_paterno." ".$nombre_encontrado->apellido_materno;

        $data = array(
            'numero_compromiso' => $ultimo,
            'estatus' => "espera",
            'id_usuario' => $this->tank_auth->get_user_id(),
            'creado_por' => $nombre_completo,
        );

        $query = $this->db->insert('mov_compromiso_caratula', $data);

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function apartartipoimpresion($ultimo) {
        $id = $this->tank_auth->get_user_id();

        $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
        $resultado_query_usuario = $this->db->query($query_usuario, array($id));
        $nombre_encontrado = $resultado_query_usuario->row();
        $nombre_completo = $nombre_encontrado->nombre." ".$nombre_encontrado->apellido_paterno." ".$nombre_encontrado->apellido_materno;

        $data = array(
            'num_impresion' => $ultimo,
        );

        $query = $this->db->insert('mov_compromiso_caratula', $data);

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function datos_precompromisoCompromiso() {
        $sql = "SELECT * FROM mov_precompromiso_caratula WHERE enfirme = 1 AND firma1 = 1 AND firma2 = 1 AND firma3 = 1 AND cancelada = 0 AND solicitar_terminar != 1 AND terminado != 1 AND total > total_compromiso ORDER BY numero_pre DESC;";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    function datos_Caratula_Precompromiso_Compromiso($precompromiso) {
        $sql = "SELECT * FROM mov_precompromiso_caratula WHERE numero_pre = ?;";
        $query = $this->db->query($sql, array($precompromiso));
        $result = $query->row();
        return $result;
    }

    function copiarDatosPrecompromiso($precompromiso, $compromiso) {

//        Se inicializa el estatus del precompromiso
        $estatus = "";

//        Se borra el detalle del compromiso en caso de que no se haya hecho ya
        $this->db->delete('mov_compromiso_detalle', array('numero_compromiso' => $compromiso));

//        Se toma el la información de la caratula del compromiso para poder determinar la fecha de donde se va a tomar el presupuesto
        $sql_caratula = "SELECT * FROM mov_precompromiso_caratula WHERE numero_pre = ?;";
        $query_caratula = $this->db->query($sql_caratula, array($precompromiso));
        $result_caratula = $query_caratula->row();

//        Se toma el detalle del precompromiso que se va a copiar
        $sql = "SELECT *, COLUMN_JSON(nivel) as estructura FROM mov_precompromiso_detalle WHERE numero_pre = ?;";
        $query = $this->db->query($sql, array($precompromiso));
        $result = $query->result();

        foreach($result as $fila) {
//            Se toma la estructura del detalle y se convierte de JSON a un arreglo para poder trabajar con la información
            $estructura = json_decode($fila->estructura);

//            Se toma el mes del presupuesto a calcular
            $mes_actual = $this->utilerias->convertirFechaAMes($result_caratula->fecha_emision);

//            Se tiene que comprobar si hay dinero para hacer el compromiso
            $query_revisar_dinero = "SELECT COLUMN_GET(nivel, '".$mes_actual."_saldo' as char) AS saldo_mes,
                                    COLUMN_GET(nivel, '".$mes_actual."_compromiso' as char) AS mes_compromiso,
                                    COLUMN_GET(nivel, 'comprometido_anual' as char) AS comprometido_anual
                                    FROM cat_niveles WHERE id_niveles = ?";

//            Se llama a la función que se encarga de tomar los datos de la partida para revisar si hay dinero disponible
            $resultado_dinero = $this->ciclo_model->revisarDinero($fila->id_nivel, $query_revisar_dinero);

//            Se suma el importe del detalle del precompromiso al total del dinero comprometido del mes seleccionado
            $check = $resultado_dinero->mes_compromiso + $fila->importe;

//            Se hace la comprobación para poder determinar si el dinero a comprometer es mayor a 0 y menor o igual al saldo del mes
            if($resultado_dinero->saldo_mes > 0 && $check <= $resultado_dinero->saldo_mes) {
//                Se pasa un estatus de exito
                $estatus .= '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Datos de la partida '.$estructura->partida.' del mes de '.ucfirst($mes_actual).' copiados correctamente.</div>';
            } else {
//                Se pasa un estatus de error
                $estatus .= '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Presupuesto mensual insuficiente en la partida '.$estructura->partida.' dentro del mes '.ucfirst($mes_actual).'</div>';
            }
//            Se el resultado que sea, se inserta el detalle la table del detalle del compromiso
            $sql = "INSERT INTO mov_compromiso_detalle (numero_compromiso, id_nivel, gasto, unidad_medida, cantidad, p_unitario, subtotal, iva, importe, titulo, year, especificaciones, nivel) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
            $resultado = $this->db->query($sql, array($compromiso, $fila->id_nivel, $fila->gasto, $fila->unidad_medida, $fila->cantidad, $fila->p_unitario, $fila->subtotal, $fila->iva, $fila->importe, $fila->titulo, $fila->year, $fila->especificaciones, $fila->nivel));

        }

//        Se revisa si el precompromiso tiene viáticos asociados
        $this->db->select('numero_precompromiso')->from('mov_tabla_lugares_periodos')->where('numero_precompromiso', $precompromiso)->limit(1);
        $query = $this->db->get();
        if($query->row()) {
            $datos_actualizar = array(
                'numero_compromiso' => $compromiso,
            );

            $this->db->where('numero_precompromiso', $precompromiso);
            $this->db->update('mov_tabla_lugares_periodos', $datos_actualizar);
        }

        return $estatus;

    }

    function actualizar_saldo_compromiso($datos) {
//        Se inicializa la variable de la respuesta de la inserción de datos
        $respuesta = array(
            "mensaje" => ''
        );

//        Se toman los datos del detalle del compromiso
        $sql = "SELECT *, COLUMN_JSON(nivel) as estructura FROM mov_compromiso_detalle WHERE numero_compromiso = ?;";
        $query = $this->db->query($sql, array($datos["ultimo_compromiso"]));
        $result = $query->result();

        foreach($result as $fila) {

//            Se toma la estructura del detalle y se convierte de JSON a un arreglo para poder manejar la información
            $estructura = json_decode($fila->estructura);

//            En caso de que exista la estructura, se toma el mes de donde se va a calcular el presupuesto
            $mes_actual = $this->utilerias->convertirFechaAMes($datos["fecha_solicitud"]);

            if($datos["no_precompromiso"] != 0){
//                Se toma la fecha de autorizacion del precompromiso
                $this->db->select('fecha_emision AS fecha')->from('mov_precompromiso_caratula')->where('numero_pre', $datos["no_precompromiso"]);
                $query = $this->db->get();
                $fecha_precompro = $query->row_array();

                $mes_precompro = $this->utilerias->convertirFechaAMes($fecha_precompro["fecha"]);

//                Se tiene que comprobar si hay dinero para hacer el compromiso
                $query_revisar_dinero = "SELECT COLUMN_GET(nivel, '" . $mes_actual . "_saldo' as char) AS mes_saldo,
                                            COLUMN_GET(nivel, '" . $mes_actual . "_compromiso' as char) AS mes_compromiso,
                                            COLUMN_GET(nivel, '" . $mes_precompro . "_precompromiso' as char) AS mes_precompromiso,
                                            COLUMN_GET(nivel, 'comprometido_anual' as char) AS comprometido_anual,
                                            COLUMN_GET(nivel, 'precomprometido_anual' as char) AS precomprometido_anual,
                                            COLUMN_GET(nivel, 'total_anual' as char) AS total_anual
                                            FROM cat_niveles WHERE id_niveles = ?";

                $resultado_dinero = $this->ciclo_model->revisarDinero($fila->id_nivel, $query_revisar_dinero);


//            Se hace la suma del saldo comprometido del mes junto con el importe
                $check = round($resultado_dinero->mes_saldo, 2) - round($fila->importe, 2);

//            $this->debugeo->imprimir_pre($resultado_dinero);

//            Se verifica que el saldo del mes sea mayor a 0 y que la operación anterior no sea mayor al saldo del mes
                if($check >= 0) {

//                    Si el resultado es mayor o igual a cero, se suma el importe al total comprometido del mes
                    $compromiso_mes = round($resultado_dinero->mes_compromiso, 2) + round($fila->importe, 2);

//                    Si el resultado es mayor o igual a cero, se suma el importe al total comprometido del año
                    $compromiso_anual = round($resultado_dinero->comprometido_anual, 2) + round($fila->importe, 2);

                    $check_precompromiso = round($resultado_dinero->mes_precompromiso, 2) - round($fila->importe, 2);

                    if($check_precompromiso < 0) {
                        $precompromiso_mes = round($resultado_dinero->mes_precompromiso, 2) - round($resultado_dinero->mes_precompromiso, 2);

                        $precompromiso_anual = round($resultado_dinero->precomprometido_anual, 2) - round($resultado_dinero->mes_precompromiso, 2);

                    } else {

//                        Se le resta la cantidad al precomprometido para poder liberar el saldo
                        $precompromiso_mes = round($resultado_dinero->mes_precompromiso, 2) - round($fila->importe, 2);

//                        Se le resta la cantaidad al precomprometido anual para poder liberar el saldo
                        $precompromiso_anual = round($resultado_dinero->precomprometido_anual, 2) - round($fila->importe, 2);

                    }

                    $total_anual = round($resultado_dinero->total_anual, 2) - round($fila->importe, 2);

                    $query_transaccion = "UPDATE cat_niveles
                                      SET nivel = COLUMN_ADD(nivel, '".$mes_actual."_compromiso', ?),
                                      nivel = COLUMN_ADD(nivel, '".$mes_actual."_saldo', ?),
                                      nivel = COLUMN_ADD(nivel, '".$mes_precompro."_precompromiso', ?),
                                      nivel = COLUMN_ADD(nivel, 'comprometido_anual', ?),
                                      nivel = COLUMN_ADD(nivel, 'precomprometido_anual', ?),
                                      nivel = COLUMN_ADD(nivel, 'total_anual', ?)
                                      WHERE id_niveles = ?;";

//                    Se llama a la función que se encarga de actualizar el saldo total del compromiso
                    $resultado_actualizar = $this->ciclo_model->actualizar_total_compromiso_con_precompromiso($query_transaccion, $compromiso_mes, $check, $precompromiso_mes, $compromiso_anual, $precompromiso_anual, $total_anual, $fila->id_nivel);

//                    Si el resultado es exitoso, se manda un mensaje al usuario marcando que fue un exito
                    if($resultado_actualizar) {
                        $respuesta["mensaje"] .= '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Datos guardados correctamente en la partida '.$estructura->partida.'.</div>';
                    }
//                    De lo contrario, se mandao un error de que no hay suficiente dinero
                    else {
                        $respuesta["mensaje"] .= '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>La partida '.$estructura->partida.', no cuenta con suficiencia presupuestaria durante el mes de '.ucfirst($mes_actual).'.</div>';
                    }

                } else {
                    $respuesta["mensaje"] .= '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>La partida '.$estructura->partida.', no cuenta con suficiencia presupuestaria durante el mes de '.ucfirst($mes_actual).'.</div>';
                }

            } else {

//                Se tiene que comprobar si hay dinero para hacer el compromiso
                $query_revisar_dinero = "SELECT COLUMN_GET(nivel, '" . $mes_actual . "_saldo' as char) AS mes_saldo,
                                        COLUMN_GET(nivel, '" . $mes_actual . "_compromiso' as char) AS mes_compromiso,
                                        COLUMN_GET(nivel, 'comprometido_anual' as char) AS comprometido_anual,
                                        COLUMN_GET(nivel, 'total_anual' as char) AS total_anual
                                        FROM cat_niveles WHERE id_niveles = ?";

                $resultado_dinero = $this->ciclo_model->revisarDinero($fila->id_nivel, $query_revisar_dinero);


//            Se hace la suma del saldo comprometido del mes junto con el importe
                $check = round($resultado_dinero->mes_saldo, 2) - round($fila->importe, 2);

//            $this->debugeo->imprimir_pre($resultado_dinero);

//            Se verifica que el saldo del mes sea mayor a 0 y que la operación anterior no sea mayor al saldo del mes
                if($check >= 0) {

//                    Si el resultado es mayor o igual a cero, se suma el importe al total comprometido del mes
                    $compromiso_mes = round($resultado_dinero->mes_compromiso, 2) + round($fila->importe, 2);

//                    Si el resultado es mayor o igual a cero, se suma el importe al total comprometido del año
                    $compromiso_anual = round($resultado_dinero->comprometido_anual, 2) + round($fila->importe, 2);


                    $total_anual = round($resultado_dinero->total_anual, 2) - round($fila->importe, 2);

                    $query_transaccion = "UPDATE cat_niveles
                                      SET nivel = COLUMN_ADD(nivel, '".$mes_actual."_compromiso', ?),
                                      nivel = COLUMN_ADD(nivel, '".$mes_actual."_saldo', ?),
                                      nivel = COLUMN_ADD(nivel, 'comprometido_anual', ?),
                                      nivel = COLUMN_ADD(nivel, 'total_anual', ?)
                                      WHERE id_niveles = ?;";

//                    Se llama a la función que se encarga de actualizar el saldo total del compromiso
                    $resultado_actualizar = $this->ciclo_model->actualizar_total_compromiso($query_transaccion, $compromiso_mes, $check, $compromiso_anual, $total_anual, $fila->id_nivel);

//                    Si el resultado es exitoso, se manda un mensaje al usuario marcando que fue un exito
                    if($resultado_actualizar) {
                        $respuesta["mensaje"] .= '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Datos guardados correctamente en la partida '.$estructura->partida.'.</div>';
                    }
//                    De lo contrario, se mandao un error de que no hay suficiente dinero
                    else {
                        $respuesta["mensaje"] .= '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>La partida '.$estructura->partida.', no cuenta con suficiencia presupuestaria durante el mes de '.ucfirst($mes_actual).'.</div>';
                    }

                } else {
                    $respuesta["mensaje"] .= '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>La partida '.$estructura->partida.', no cuenta con suficiencia presupuestaria durante el mes de '.ucfirst($mes_actual).'.</div>';
                }

            }

        }

        return $respuesta;

    }

    function datos_compromisoCaratula(){
        $sql = "SELECT * FROM mov_compromiso_caratula ORDER BY numero_compromiso DESC ;";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    function datos_compromisoCaratulaEstatus($sql){
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    function datos_compromisoDetalle($compromiso, $sql) {
        $query = $this->db->query($sql, array($compromiso));
        $result = $query->result();
        return $result;
    }

    function cancelarcompromisoCaratula($compromiso, $query) {

        $query = $this->db->query($query, array(
            date("Y-m-d"), $compromiso,
        ));
        if($query){
            return TRUE;
        }
        else {
            return FALSE;
        }

    }

    function existeDetalleCompromiso($compromiso) {
        $this->db->where('numero_compromiso', $compromiso);
        $query = $this->db->get('mov_compromiso_detalle');
        $result = $query->row();
        return $result;
    }

    function borrarDetallecompromiso($compromiso) {
        $this->db->where('id_compromiso_detalle', $compromiso);
        $query = $this->db->delete('mov_compromiso_detalle');

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }


    }

    function editarDetallecompromiso($datos) {

        // echo("Datos de entrada: ");
        // $this->debugeo->imprimir_pre($datos);

        $this->db->where('id_compromiso_detalle', $datos["id_compromiso"]);
        $this->db->select('id_nivel, id_compromiso_detalle AS numero_compromiso');
        $query = $this->db->get('mov_compromiso_detalle');
        $result = $query->row();

       // echo("Datos del detalle: ");
       // $this->debugeo->imprimir_pre($result);

        if($datos["fecha"] != '0000-00-00' && $datos["fecha"] != NULL) {

            $mes_actual = $this->utilerias->convertirFechaAMes($datos["fecha"]);

//            Se tiene que comprobar si hay dinero para hacer el compromiso
            $query_revisar_dinero = "SELECT COLUMN_GET(nivel, '" . $mes_actual . "_saldo' as char) AS saldo_mes,
                                    COLUMN_GET(nivel, '" . $mes_actual . "_compromiso' as char) AS mes_compromiso,
                                    COLUMN_GET(nivel, 'comprometido_anual' as char) AS comprometido_anual
                                    FROM cat_niveles WHERE id_niveles = ?";

            $resultado_dinero = $this->ciclo_model->revisarDinero($result->id_nivel, $query_revisar_dinero);

           // echo("Resultado de dinero: ");
           // $this->debugeo->imprimir_pre($resultado_dinero);

            $check = round($resultado_dinero->saldo_mes, 2) - round($datos["importe"], 2);

           // echo("Check: ");
           // $this->debugeo->imprimir_pre($check);

            if ($check >= 0) {

                $data = array(
                    'cantidad' => $datos["cantidad"],
                    'p_unitario' => $datos["precio"],
                    'subtotal' => $datos["subtotal"],
                    'iva' => $datos["iva"],
                    'importe' => $datos["importe"],
                );

                $this->db->where('id_compromiso_detalle', $datos["id_compromiso"]);
                $this->db->update('mov_compromiso_detalle', $data);
                if ($query) {
                    return TRUE;
                } else {
                    return FALSE;
                }

            } else {
                $data = array(
                    'cantidad' => 0,
                    'p_unitario' => 0,
                    'subtotal' => 0,
                    'iva' => 0,
                    'importe' => 0,
                );

                $this->db->where('id_compromiso_detalle', $datos["id_compromiso"]);
                $this->db->update('mov_compromiso_detalle', $data);
                return FALSE;
            }

        } else {
            return "no_fecha";
        }


    }

    function revisarDineroCompromiso($id_nivel, $query) {
        $query = $this->db->query($query, array($id_nivel));
        $result = $query->row();
        return $result;
    }

    function insertar_caratula_compromiso($datos) {

        if(!$datos["total_hidden"]) {
            $datos["total_hidden"] = 0;
        }

        if(!$datos["fecha_entrega"]) {
            $datos["fecha_entrega"] = "0000-00-00";
        }

        if(!$datos["id_tipo_impresion"]) {
            $datos["id_tipo_impresion"] = 0;
        }

        $data = array(
            'num_precompromiso' => $datos["no_precompromiso"],
            'id_proveedor' => $datos["id_proveedor"],
            'nombre_proveedor' => $datos["proveedor"],
            'lugar_entrega' => $datos["lugar_entrega"],
            'condicion_entrega' => $datos["condicion_entrega"],
            'total' => $datos["total_hidden"],
            'tipo_compromiso' => $datos["tipo_compromiso"],
            'tipo_gasto' => $datos["tipo_radio"],
            'enfirme' => $datos["check_firme"],
            'firma1' => $datos["firma1"],
            'firma2' => $datos["firma2"],
            'firma3' => $datos["firma3"],
            'fecha_emision' => $datos["fecha_solicitud"],
            'fecha_autoriza' => $datos["fecha_autoriza"],
            'fecha_programada' => $datos["fecha_entrega"],
            'estatus' => $datos["estatus"],
            'cancelada' => 0,
            'fecha_cancelada' => 0,
            'concepto_especifico' => $datos["concepto_especifico"],
            'descripcion_general' => $datos["descripcion_general"],
            'tipo_impresion' => $datos["tipo_impresion"],
            'num_impresion' => $datos["id_tipo_impresion"],
            "id_persona" => $datos["id_persona"],
            "nombre_completo" => $datos["nombre_persona"],
            "puesto" => $datos["puesto"],
            "clave" => $datos["clave"],
            "area" => $datos["area"],
            "no_empleado" => $datos["no_empleado"],
            "forma_pago" => $datos["forma_pago"],
            "transporte" => $datos["transporte"],
            "grupo_jerarquico" => $datos["grupo_jerarquico"],
            "zona_marginada" => $datos["zona_marginada_check"],
            "zona_mas_economica" => $datos["zona_mas_economica_check"],
            "zona_menos_economica" => $datos["zona_menos_economica_check"],
        );

        $this->db->where('numero_compromiso', $datos["ultimo_compromiso"]);
        $query = $this->db->update('mov_compromiso_caratula', $data);

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }

    }

    function insertar_detalle_viaticos_compromiso($datos) {

        $data = array(
            'numero_compromiso' => $datos['ultimo_compromiso'],
            'lugar' => $datos['lugar_comision'],
            'periodo_inicio' => $datos['fecha_inicio'],
            'periodo_fin' => $datos['fecha_termina'],
            'cuota_diaria' => $datos['cuota_diaria'],
            'dias' => $datos['dias'],
            'importe' => $datos['importe'],
        );

        $query = $this->db->insert('mov_tabla_lugares_periodos', $data);

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function insertar_caratula_compromisoArchivo($datos, $ultimo) {
        foreach ($datos as $key => $value) {
            if($datos[$key]) {
                $datos[$key] = trim($value);
            } else {
                $datos[$key] = '';
            }
        }

        $id = $this->tank_auth->get_user_id();
        /** En esta parte se toma el nombre del usuario */
        $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
        $resultado_query_usuario = $this->db->query($query_usuario, array($id));
        $nombre_encontrado = $resultado_query_usuario->row();
        $nombre_completo = $nombre_encontrado->nombre." ".$nombre_encontrado->apellido_paterno." ".$nombre_encontrado->apellido_materno;

        /** En esta parte se toma el nomnbre del proveedor */

        $nombre_proveedor = array(
            "nombre" => ''
        );

        if(isset($datos["B"])){
            $query_proveedor = "SELECT nombre FROM cat_proveedores WHERE clave_prov = ?;";
            $resultado_query_proveedor = $this->db->query($query_proveedor, array($datos["B"]));
            $proveedor = $resultado_query_proveedor->row_array();

            if(!$proveedor) {
                $DB1 = $this->load->database('pedidos', TRUE);

                $sql_caratula = "SELECT nomedi AS nombre
                                FROM editor
                                WHERE cveedi = ?";
                $query_caratula = $DB1->query($sql_caratula, array($datos["B"]));
                $proveedor = $query_caratula->row_array();
                if(!$proveedor) {
                    $nombre_proveedor["nombre"] = '';
                } else {
                    $nombre_proveedor["nombre"] = $proveedor["nombre"];
                }
            } else {
                $nombre_proveedor["nombre"] = $proveedor["nombre"];
            }
        } else {
            $datos["B"] = "";
            $nombre_proveedor["nombre"] = '';
        }

//        $this->debugeo->imprimir_pre($nombre_proveedor);

        if(strtolower(trim($datos["E"])) == strtolower("Gasto Corriente")) {
            $datos["E"] = 1;
        } elseif(strtolower(trim($datos["E"])) == strtolower("Gasto de Capital")) {
            $datos["E"] = 2;
        } elseif(strtolower(trim($datos["E"])) == strtolower("Amortización de la cuenta y disminución de pasivos")) {
            $datos["E"] = 3;
        }

        if(isset($datos["Q"]) && strtolower($datos["Q"]) == strtolower("Si")){
            $datos["Q"] = 1;
        } else{
            $datos["Q"] = 0;
        }

        if(isset($datos["R"]) && strtolower($datos["R"]) == strtolower("Si")){
            $datos["R"] = 1;
        } else{
            $datos["R"] = 0;
        }

        if(isset($datos["S"]) && strtolower($datos["S"]) == strtolower("Si")){
            $datos["S"] = 1;
        } else{
            $datos["S"] = 0;
        }

        $fecha = ($datos['F'] - 25569) * 86400;
        $fecha_programada = date('Y-m-d', $fecha);

        if(isset($datos["H"])) {
            $sql = "SELECT num_impresion as ultimo FROM mov_compromiso_caratula WHERE tipo_impresion = ? ORDER BY num_impresion DESC LIMIT 1;";
            $query = $this->db->query($sql, array($datos["H"]));
            $num_impresion = $query->row();
            if(!$num_impresion) {
                $datos["H"] = 0;
            } else {
                $datos["H"] = $num_impresion->ultimo;
            }
        } else {
            $datos["H"] = 0;
        }

        if(array_key_exists('D', $datos) == FALSE || $datos["D"] == NULL || $datos["D"] == '') {
            $datos["D"] = '';
        }

        if(array_key_exists('G', $datos) == FALSE || $datos["G"] == NULL || $datos["G"] == '') {
            $datos["G"] = '';
        }

        if(array_key_exists('I', $datos) == FALSE || $datos["I"] == NULL || $datos["I"] == '') {
            $datos["I"] = '';
        }

        if(array_key_exists('J', $datos) == FALSE || $datos["J"] == NULL || $datos["J"] == '') {
            $datos["J"] = 0;
        }

        if(array_key_exists('K', $datos) == FALSE || $datos["K"] == NULL || $datos["K"] == '') {
            $datos["K"] = '';
        }

        if(array_key_exists('L', $datos) == FALSE || $datos["L"] == NULL || $datos["L"] == '') {
            $datos["L"] = '';
        }

        if(array_key_exists('M', $datos) == FALSE || $datos["M"] == NULL || $datos["M"] == '') {
            $datos["M"] = '';
        }

        if(array_key_exists('N', $datos) == FALSE || $datos["N"] == NULL || $datos["N"] == '') {
            $datos["N"] = '';
        }

        if(array_key_exists('O', $datos) == FALSE || $datos["O"] == NULL || $datos["O"] == '') {
            $datos["O"] = '';
        }

        if(array_key_exists('P', $datos) == FALSE || $datos["P"] == NULL || $datos["P"] == '') {
            $datos["P"] = '';
        }

        if(array_key_exists('Q', $datos) == FALSE || $datos["Q"] == NULL || $datos["Q"] == '') {
            $datos["Q"] = 0;
        }

        if(array_key_exists('R', $datos) == FALSE || $datos["R"] == NULL || $datos["R"] == '') {
            $datos["R"] = 0;
        }

        if(array_key_exists('S', $datos) == FALSE || $datos["S"] == NULL || $datos["S"] == '') {
            $datos["S"] = 0;
        }

        $data = array(
            'id_proveedor' => $datos["B"],
            'numero_compromiso' => $ultimo,
            'nombre_proveedor' => $nombre_proveedor["nombre"],
            'lugar_entrega' => $datos["C"],
            'condicion_entrega' => $datos["D"],
            'total' => 0,
            'tipo_compromiso' => $datos["A"],
            'tipo_gasto' => $datos["E"],
            'enfirme' => 0,
            'firma1' => 0,
            'firma2' => 0,
            'firma3' => 0,
            'fecha_emision' => date("Y-m-d"),
            'fecha_autoriza' => "0000-00-00",
            'fecha_programada' => $fecha_programada,
            'estatus' => "espera",
            'cancelada' => 0,
            'fecha_cancelada' => 0,
            'id_usuario' => $id,
            'creado_por' => $nombre_completo,
            'descripcion_general' => $datos["G"],
            'tipo_impresion' => $datos["H"],
            'num_impresion' => $datos["H"],
            'nombre_completo' => $datos["I"],
            'no_empleado' => $datos["J"],
            'puesto' => $datos["J"],
            'area' => $datos["L"],
            'clave' => substr($datos["M"], 0, 3),
            'forma_pago' => $datos["N"],
            'transporte' => $datos["O"],
            'grupo_jerarquico' => $datos["P"],
            'zona_marginada' => $datos["Q"],
            'zona_mas_economica' => $datos["R"],
            'zona_menos_economica' => $datos["S"],
        );

        $query = $this->db->insert('mov_compromiso_caratula', $data);

        if($query) {
            return $this->db->insert_id();
        }
        else {
            return FALSE;
        }
    }

    function insertar_caratula_compromisoArchivoMasivo($datos, $ultimo) {
        foreach ($datos as $key => $value) {
            if($datos[$key]) {
                $datos[$key] = trim($value);
            } else {
                $datos[$key] = '';
            }
        }

        $id = $this->tank_auth->get_user_id();
        /** En esta parte se toma el nombre del usuario */
        $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
        $resultado_query_usuario = $this->db->query($query_usuario, array($id));
        $nombre_encontrado = $resultado_query_usuario->row();
        $nombre_completo = $nombre_encontrado->nombre." ".$nombre_encontrado->apellido_paterno." ".$nombre_encontrado->apellido_materno;

        /** En esta parte se toma el nomnbre del proveedor */

        $nombre_proveedor = array(
            "nombre" => ''
        );

        if(isset($datos["B"])){
            $query_proveedor = "SELECT nombre FROM cat_proveedores WHERE clave_prov = ?;";
            $resultado_query_proveedor = $this->db->query($query_proveedor, array($datos["B"]));
            $proveedor = $resultado_query_proveedor->row_array();

            if(!$proveedor) {
                $DB1 = $this->load->database('pedidos', TRUE);

                $sql_caratula = "SELECT nomedi AS nombre
                                FROM editor
                                WHERE cveedi = ?";
                $query_caratula = $DB1->query($sql_caratula, array($datos["B"]));
                $proveedor = $query_caratula->row_array();
                if(!$proveedor) {
                    $nombre_proveedor["nombre"] = '';
                } else {
                    $nombre_proveedor["nombre"] = utf8_encode($proveedor["nombre"]);
                }
            } else {
                $nombre_proveedor["nombre"] = $proveedor["nombre"];
            }
        } else {
            $nombre_proveedor["nombre"] = '';
        }

//        $this->debugeo->imprimir_pre($nombre_proveedor);

        if(strtolower(trim($datos["E"])) == strtolower("Gasto Corriente")) {
            $datos["E"] = 1;
        } elseif(strtolower(trim($datos["E"])) == strtolower("Gasto de Capital")) {
            $datos["E"] = 2;
        } elseif(strtolower(trim($datos["E"])) == strtolower("Amortización de la cuenta y disminución de pasivos")) {
            $datos["E"] = 3;
        }

        if(isset($datos["Q"]) && strtolower($datos["Q"]) == strtolower("Si")){
            $datos["Q"] = 1;
        } else{
            $datos["Q"] = 0;
        }

        if(isset($datos["R"]) && strtolower($datos["R"]) == strtolower("Si")){
            $datos["R"] = 1;
        } else{
            $datos["R"] = 0;
        }

        if(isset($datos["S"]) && strtolower($datos["S"]) == strtolower("Si")){
            $datos["S"] = 1;
        } else{
            $datos["S"] = 0;
        }

        if(array_key_exists('D', $datos) == FALSE || $datos["D"] == NULL || $datos["D"] == '') {
            $datos["D"] = '';
        }

        if(array_key_exists('G', $datos) == FALSE || $datos["G"] == NULL || $datos["G"] == '') {
            $datos["G"] = '';
        }

        if(array_key_exists('I', $datos) == FALSE || $datos["I"] == NULL || $datos["I"] == '') {
            $datos["I"] = '';
        }

        if(array_key_exists('J', $datos) == FALSE || $datos["J"] == NULL || $datos["J"] == '') {
            $datos["J"] = 0;
        }

        if(array_key_exists('K', $datos) == FALSE || $datos["K"] == NULL || $datos["K"] == '') {
            $datos["K"] = '';
        }

        if(array_key_exists('L', $datos) == FALSE || $datos["L"] == NULL || $datos["L"] == '') {
            $datos["L"] = '';
        }

        if(array_key_exists('M', $datos) == FALSE || $datos["M"] == NULL || $datos["M"] == '') {
            $datos["M"] = '';
        }

        if(array_key_exists('N', $datos) == FALSE || $datos["N"] == NULL || $datos["N"] == '') {
            $datos["N"] = '';
        }

        if(array_key_exists('O', $datos) == FALSE || $datos["O"] == NULL || $datos["O"] == '') {
            $datos["O"] = '';
        }

        if(array_key_exists('P', $datos) == FALSE || $datos["P"] == NULL || $datos["P"] == '') {
            $datos["P"] = '';
        }

        if(array_key_exists('Q', $datos) == FALSE || $datos["Q"] == NULL || $datos["Q"] == '') {
            $datos["Q"] = 0;
        }

        if(array_key_exists('R', $datos) == FALSE || $datos["R"] == NULL || $datos["R"] == '') {
            $datos["R"] = 0;
        }

        if(array_key_exists('S', $datos) == FALSE || $datos["S"] == NULL || $datos["S"] == '') {
            $datos["S"] = 0;
        }

        $data = array(
            'id_proveedor' => $datos["B"],
            'numero_compromiso' => $ultimo,
            'num_precompromiso' => 0,
            'nombre_proveedor' => $nombre_proveedor["nombre"],
            'lugar_entrega' => $datos["C"],
            'condicion_entrega' => $datos["D"],
            'total' => 0,
            'tipo_compromiso' => $datos["A"],
            'tipo_gasto' => $datos["E"],
            'enfirme' => 1,
            'firma1' => 1,
            'firma2' => 1,
            'firma3' => 1,
            'fecha_emision' => $datos["F"],
            'fecha_autoriza' => $datos["F"],
            'fecha_programada' => $datos["F"],
            'estatus' => "espera",
            'cancelada' => 0,
            'fecha_cancelada' => 0,
            'id_usuario' => $id,
            'creado_por' => $nombre_completo,
            'descripcion_general' => $datos["G"],
            'tipo_impresion' => $datos["H"],
            'num_impresion' => $datos["numero_impresion"],
            'nombre_completo' => $datos["I"],
            'no_empleado' => $datos["J"],
            'puesto' => $datos["J"],
            'area' => $datos["L"],
            'clave' => substr($datos["M"], 0, 3),
            'forma_pago' => $datos["N"],
            'transporte' => $datos["O"],
            'grupo_jerarquico' => $datos["P"],
            'zona_marginada' => $datos["Q"],
            'zona_mas_economica' => $datos["R"],
            'zona_menos_economica' => $datos["S"],
        );

        $query = $this->db->insert('mov_compromiso_caratula', $data);

        if($query) {
            return $this->db->insert_id();
        }
        else {
            return FALSE;
        }
    }

    function insertar_caratula_compromiso_cancelado($datos) {
        $id = $this->tank_auth->get_user_id();

        $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
        $resultado_query_usuario = $this->db->query($query_usuario, array($id));
        $nombre_encontrado = $resultado_query_usuario->row();
        $nombre_completo = $nombre_encontrado->nombre." ".$nombre_encontrado->apellido_paterno." ".$nombre_encontrado->apellido_materno;

        $data = array(
            'num_precompromiso' => $datos["no_precompromiso"],
            'id_proveedor' => $datos["id_proveedor"],
            'nombre_proveedor' => $datos["proveedor"],
            'lugar_entrega' => $datos["lugar_entrega"],
            'condicion_entrega' => $datos["condicion_entrega"],
            'subtotal' => $datos["subtotal_hidden"],
            'iva' => $datos["iva"],
            'total' => $datos["importe"],
            'tipo_compromiso' => $datos["tipo_compromiso"],
            'tipo_gasto' => $datos["tipo_radio"],
            'enfirme' => $datos["check_firme"],
            'firma1' => $datos["firma1"],
            'firma2' => $datos["firma2"],
            'firma3' => $datos["firma3"],
            'fecha_emision' => $datos["fecha_solicitud"],
            'fecha_autoriza' => $datos["fecha_autoriza"],
            'fecha_programada' => $datos["fecha_entrega"],
            'estatus' => $datos["estatus"],
            'creado_por' => $nombre_completo,
            'descripcion_general' => $datos["descripcion_general"],
            'cancelada' => 1,
            'fecha_cancelada' => date("Y-m-d"),
        );

        $this->db->where('numero_compromiso', $datos["ultimo_compromiso"]);
        $query = $this->db->update('mov_compromiso_caratula', $data);

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }

    }

    function insertar_detalle_compromisoArchivo($datos, $query, $ultimo) {
        /** En esta parte se toma el nombre del gasto */
        $query_gasto = "SELECT descripcion FROM cat_conceptos_gasto WHERE articulo = ?;";
        $resultado_query_gasto = $this->db->query($query_gasto, array($datos["G"]));
        $nombre_gasto = $resultado_query_gasto->row();

        $query = $this->db->query($query, array(
            $ultimo,
            $datos["id_nivel"],
            $datos["G"],
            $datos["H"],
            $datos["I"],
            $datos["K"],
            $datos["subtotal"],
            $datos["iva"],
            $datos["importe"],
            $nombre_gasto->descripcion,
            date("Y"),
            $datos["L"],
            $datos["A"],
            $datos["B"],
            $datos["C"],
            $datos["D"],
            $datos["E"],
            $datos["F"],
            $nombre_gasto->descripcion));
        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function get_datos_compromiso_caratula($compromiso, $query) {
        $query = $this->db->query($query, array($compromiso));
        $result = $query->row();
        return $result;
    }

    function get_datos_compromiso_detalle($compromiso){
        $query = "SELECT *, COLUMN_JSON(nivel) AS estructura FROM mov_compromiso_detalle WHERE numero_compromiso = ?;";
        $query = $this->db->query($query, array($compromiso));
        $result = $query->result();
        return $result;
    }

    function get_datos_compromiso_lugares($compromiso){
        $query = "SELECT * FROM mov_tabla_lugares_periodos WHERE numero_compromiso = ?;";
        $query = $this->db->query($query, array($compromiso));
        $result = $query->result();
        return $result;
    }

    function insertar_detalle_compromiso($datos, $query) {
        $query = $this->db->query($query, array(
            $datos["ultimo_compromiso"],
            $datos["id_nivel"],
            $datos["gasto"],
            $datos["u_medida"],
            $datos["cantidad"],
            $datos["precio"],
            $datos["subtotal"],
            $datos["iva"],
            $datos["importe"],
            $datos["titulo_gasto"],
            date("Y"),
            $datos["descripcion_detalle"],
            $datos["nivel1"],
            $datos["nivel2"],
            $datos["nivel3"],
            $datos["nivel4"],
            $datos["nivel5"],
            $datos["nivel6"],
            $datos["gasto"]));
        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function actualizar_total_compromiso($query, $mes, $saldo_mes, $anual, $total_anual, $nivel) {
        $query = $this->db->query($query, array(
            $mes,
            $saldo_mes,
            $anual,
            $total_anual,
            $nivel));

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function actualizar_total_compromiso_con_precompromiso($query, $compro_mes, $saldo_mes, $precompro_mes, $anual_compro, $anual_precompro, $total_anual, $nivel) {
        $query = $this->db->query($query, array(
            $compro_mes,
            $saldo_mes,
            $precompro_mes,
            $anual_compro,
            $anual_precompro,
            $total_anual,
            $nivel));

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function actualizar_saldo_activo_compromiso($query, $cantidad, $nivel) {
        $query = $this->db->query($query, array(
            $cantidad,
            $nivel));

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function actualizarTotalCaratulaCompromiso($id, $total){
        $data = array(
            'total' => $total,
        );

        $this->db->where('id_compromiso_caratula', $id);
        $this->db->update('mov_compromiso_caratula', $data);
    }

    function datosProveedores() {
        $this->db->select('id_proveedores, nombre');
        $query = $this->db->get('cat_proveedores');
        $result = $query->result();
        return $result;
    }

    function tomar_tipo_impresion($datos) {
        $this->db->select('tipo_impresion, num_impresion')->from('mov_compromiso_caratula')->where('tipo_impresion', $datos["tipo"])->where('num_impresion', $datos["numero"]);
        $query = $this->db->get();
        $resultado = $query->row();

        if(!$resultado) {
            return FALSE;
        } else {
            return $resultado->num_impresion;
        }
    }

    function datos_viaticosDetalleCompromiso($compromiso) {
        $this->db->select('id_tabla_lugares_periodos, lugar, periodo_inicio, periodo_fin, cuota_diaria, dias, importe')->from('mov_tabla_lugares_periodos')->where('numero_compromiso', $compromiso);
        $query = $this->db->get();
        return $query->result();
    }

    function tomarDetalleBorrarCompromiso($compromiso) {
        $this->db->select('id_compromiso_detalle, COLUMN_JSON(nivel) AS estructura, importe')->from('mov_compromiso_detalle')->where('numero_compromiso', $compromiso);
        $query = $this->db->get();
        return $query->result_array();
    }

    function datosProveedoresEDUCAL() {
        $DB1 = $this->load->database('pedidos', TRUE);
        $sql = "SELECT cveedi, nomedi FROM editor;";
        $query = $DB1->query($sql);
        return $query->result();
    }

    function checar_contrarecibo_cancelar_compromisos($compromiso){
        $this->db->select('id_contrarecibo_caratula')->from('mov_contrarecibo_caratula')->where('numero_compromiso', $compromiso)->where('enfirme', 1)->where('cancelada', 0);
        $query = $this->db->get();
        return $query->result_array();
    }

    /**
     * Aqui empieza la sección del Contra Recibo de Pago
     */

    function ultimo_contrarecibo() {
        $sql = "SELECT id_contrarecibo_caratula as ultimo FROM mov_contrarecibo_caratula ORDER BY id_contrarecibo_caratula DESC LIMIT 1;";
        $query = $this->db->query($sql);
        $row = $query->row();
        if(!$row) {
            $row = 0;
        }
        return $row;
    }

    function apartarContrarecibo($ultimo) {
        $id = $this->tank_auth->get_user_id();

        $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
        $resultado_query_usuario = $this->db->query($query_usuario, array($id));
        $nombre_encontrado = $resultado_query_usuario->row();
        $nombre_completo = $nombre_encontrado->nombre." ".$nombre_encontrado->apellido_paterno." ".$nombre_encontrado->apellido_materno;

        $data = array(
            'id_contrarecibo_caratula' => $ultimo,
            'estatus' => "espera",
            'id_usuario' => $this->tank_auth->get_user_id(),
            'creado_por' => $nombre_completo,
        );

        $query = $this->db->insert('mov_contrarecibo_caratula', $data);

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function datos_contrareciboCaratula() {
        $sql = "SELECT * FROM mov_contrarecibo_caratula ORDER BY id_contrarecibo_caratula DESC ;";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    function datos_contrareciboCaratulaEstatus($sql){
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    function datos_compromisoContrarecibo() {
        $sql = "SELECT * FROM mov_compromiso_caratula WHERE enfirme = 1 AND firma1 = 1 AND firma2 = 1 AND firma3 = 1 AND cancelada = 0 AND usado = 0 ORDER BY numero_compromiso DESC;";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    function copiarDatosCompromiso($compromiso, $contrarecibo) {
        $this->db->trans_start();
        $this->db->delete('mov_contrarecibo_detalle', array('numero_contrarecibo' => $contrarecibo));

        $sql = "SELECT * FROM mov_compromiso_caratula WHERE numero_compromiso = ?;";
        $query = $this->db->query($sql, array($compromiso));
        $result = $query->row();

        $sql = "SELECT * FROM mov_compromiso_detalle WHERE numero_compromiso = ?;";
        $query = $this->db->query($sql, array($compromiso));
        $result2 = $query->result();

        $subtotal = 0;
        $iva = 0;
        $total = 0;

        foreach ($result2 as $row) {
            $subtotal += $row->subtotal;
            if(is_numeric($row->iva)) {
                $iva += $row->iva;
            }
            $total += $row->importe;
        }

//        $this->debugeo->imprimir_pre($result);

        $sql = "INSERT INTO mov_contrarecibo_detalle (numero_contrarecibo, numero_compromiso, nombre, tipo, id_proveedor, subtotal, iva, total, descripcion ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ? );";
        $resultado = $this->db->query($sql, array($contrarecibo, $compromiso, $result->nombre_proveedor, $result->tipo_compromiso, $result->id_proveedor, $subtotal, $iva, $total, $result->descripcion_general));
        $this->db->trans_complete();

        if($resultado) {
            return TRUE;
        }
        else {
            return FALSE;
        }

    }

    function datos_Caratula_Compromiso_Contrarecibo($compromiso) {
        $sql = "SELECT * FROM mov_compromiso_caratula WHERE numero_compromiso = ?;";
        $query = $this->db->query($sql, array($compromiso));
        $result = $query->row();
        return $result;
    }

    function insertar_caratula_contrarecibo($datos) {
        $sql_check_compromiso = "SELECT id_contrarecibo_caratula as ID FROM mov_contrarecibo_caratula WHERE numero_compromiso = ?;";
        $query_check_compromiso = $this->db->query($sql_check_compromiso, array($datos["num_compromiso"]));
        $result_check_compromiso = $query_check_compromiso->row_array();

        // $this->debugeo->imprimir_pre($result_check_compromiso);

        if(isset($result_check_compromiso["ID"]) && $result_check_compromiso["ID"] != NULL && $result_check_compromiso["ID"] != "") {
            if($result_check_compromiso["ID"] != $datos["contrarecibo"]) {
                return "usado";
            }
        }

        $data = array(
            "usado" => 1
        );

        $this->db->where('numero_compromiso', $datos["num_compromiso"]);
        $this->db->update('mov_compromiso_caratula', $data);

        $informacion_insertar = array(
            'id_proveedor' => $datos["clave_proveedor"],
            'proveedor' => $datos["proveedor"],
            'concepto' => $datos["concepto"],
            'concepto_especifico' => $datos["concepto_especifico"],
            'descripcion' => $datos["descripcion"],
            'documento' => $datos["documento"],
            'fecha_pago' => $datos["fecha_pago"],
            'fecha_emision' => $datos["fecha_solicita"],
            'enfirme' => $datos["firme"],
            'numero_compromiso' => $datos["num_compromiso"],
            'destino' => $datos["tipo"],
            'importe' => $datos["importe"],
            'tipo_documento' => $datos["tipo_documento"],
            'estatus' => $datos["estatus"],
            'pagada' => 0,
            'sifirme' => 0,
            'cancelada' => 0,
            'documentacion_anexa' => $datos["documentacion_anexa"],
            'id_persona' => $datos["id_persona"],
            'nombre_completo' => $datos["nombre_completo"],
            'tipo_impresion' => $datos["tipo_impresion"],
        );

        $this->db->where('id_contrarecibo_caratula', $datos["contrarecibo"]);
        $query = $this->db->update('mov_contrarecibo_caratula', $informacion_insertar);

        if($query) {

            $informacion = array(
                "usado" => 1,
            );
            $this->db->where('numero_compromiso', $datos["num_compromiso"]);
            $query2 = $this->db->update('mov_compromiso_caratula', $informacion);
            if($query2) {
                return "exito";
            }
            else {
                return "error";
            }

        }
        else {
            return "error";
        }
    }

    function get_datos_contrarecibo_caratula($contrarecibo, $query) {
        $query = $this->db->query($query, array($contrarecibo));
        $result = $query->row();
        return $result;
    }

    function get_datos_contrarecibo_detalle($contrarecibo) {
        $query = "SELECT * FROM mov_contrarecibo_detalle WHERE numero_contrarecibo = ?;";
        $query = $this->db->query($query, array($contrarecibo));
        $result = $query->result();
        return $result;
    }

    function get_datos_contrarecibo_compromiso($contrarecibo){
        $query = "SELECT p1.numero_compromiso, p1.numero_contrarecibo,
        p2.numero_compromiso, p2.gasto, p2.titulo,  p2.unidad_medida, p2.cantidad, p2.p_unitario, p2.subtotal, p2.iva, p2.importe
        FROM mov_contrarecibo_detalle p1
        INNER JOIN mov_compromiso_detalle p2 ON p2.numero_compromiso = p1.numero_compromiso
        WHERE numero_contrarecibo = ?;";
        $query = $this->db->query($query, array($contrarecibo));
        $result = $query->result();
        return $result;
    }

    function borrarDetalleContrarecibo($contrarecibo) {

        $this->db->where('id_contrarecibo_detalle', $contrarecibo);
        $query = $this->db->delete('mov_contrarecibo_detalle');

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }

    }

    function insertar_caratula_contrarecibo_cancelado($datos) {
        $id = $this->tank_auth->get_user_id();

        $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
        $resultado_query_usuario = $this->db->query($query_usuario, array($id));
        $nombre_encontrado = $resultado_query_usuario->row();
        $nombre_completo = $nombre_encontrado->nombre." ".$nombre_encontrado->apellido_paterno." ".$nombre_encontrado->apellido_materno;

        $data = array(
            'alterno' => $datos["alterno"],
            'id_proveedor' => $datos["clave_proveedor"],
            'proveedor' => $datos["proveedor"],
            'concepto' => $datos["concepto"],
            'descripcion' => $datos["descripcion"],
            'documento' => $datos["documento"],
            'fecha_pago' => $datos["fecha_pago"],
            'fecha_emision' => $datos["fecha_solicita"],
            'enfirme' => $datos["firme"],
            'numero_compromiso' => $datos["num_compromiso"],
            'destino' => $datos["tipo"],
            'importe' => $datos["importe"],
            'tipo_documento' => $datos["tipo_documento"],
            'creado_por' => $nombre_completo,
            'pagada' => 0,
            'sifirme' => 0,
            'cancelada' => 1,
            'fecha_cancelada' => date("Y-m-d"),
            'estatus' => $datos["estatus"],
        );

        $this->db->where('id_contrarecibo_caratula', $datos["contrarecibo"]);
        $query = $this->db->update('mov_contrarecibo_caratula', $data);

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }

    }

    function activarCompromiso($compromiso) {

        $datos = array(
            "usado" => 0,
        );

        $this->db->where('numero_compromiso', $compromiso);
        $query = $this->db->update('mov_compromiso_caratula', $datos);

        if($query) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function cancelarcompromisoContrarecibo($contrarecibo, $query) {
        $query = $this->db->query($query, array(
            date("Y-m-d"), $contrarecibo,
        ));
        if($query){
            return TRUE;
        }
        else {
            return FALSE;
        }

    }

    function marcar_poliza_usada($contrarecibo, $query) {
        $query = $this->db->query($query, array($contrarecibo));
        if($query){
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function marcar_devengado($compromiso, $fecha) {

        $sql = "SELECT * FROM mov_compromiso_detalle WHERE numero_compromiso = ?;";
        $query = $this->db->query($sql, array($compromiso));
        $result = $query->result();

        foreach($result as $row) {

            $mes = $this->utilerias->convertirFechaAMes($fecha);

            $sql_devengado = "SELECT COLUMN_GET(nivel, '".$mes."_devengado' as char) AS devengado_mes FROM cat_niveles WHERE id_niveles = ?;";
            $query_devengado = $this->db->query($sql_devengado, array($row->id_nivel));
            $resultado_nivel = $query_devengado->row();

            $total_devengado = round($row->importe, 2) + round($resultado_nivel->devengado_mes, 2);

            $sql_actualizar_devengado = "UPDATE cat_niveles SET nivel = COLUMN_ADD(nivel, '".$mes."_devengado', ?) WHERE id_niveles = ?;";
            $this->db->query($sql_actualizar_devengado, array($total_devengado, $row->id_nivel));
        }

    }

    function tomarSubsidio($clave) {
        $this->db->select('descripcion')->from('cat_clasificador_fuentes_financia')->where('codigo', $clave);
        $query = $this->db->get();
        return $query->row()->descripcion;
    }

    function tomar_cuentas_contrarecibo($partida) {
//        Se toman los datos de la cuenta que le corresponde a la partida a buscar
        $sql = "SELECT * FROM cat_correlacion_partidas_contables WHERE clave LIKE BINARY ? AND destino = 'Devengado Gasto';";
        $query = $this->db->query($sql, array($partida));
//        Si existe la clave a buscar, se le agrega el centro de costos para saber si existe esa clave
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function ultima_poliza() {
        $sql = "SELECT numero_poliza as ultimo FROM mov_polizas_cabecera ORDER BY numero_poliza DESC LIMIT 1;";
        $query = $this->db->query($sql);
        $row = $query->row();
        if(!$row) {
            $row = 0;
        }
        return $row;
    }

    function insertar_detalle_poliza($query, $datos) {
        $query = $this->db->query($query, $datos);
        if ($query) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function checar_movimiento_cancelar_contrarecibo($contrarecibo){
        $sql = "SELECT mbpc.movimiento
                FROM mov_bancos_pagos_contrarecibos mbpc
                JOIN mov_bancos_movimientos mbm
                ON mbpc.movimiento = mbm.movimiento
                WHERE mbm.enfirme = 1
                AND mbm.cancelada = 0
                AND mbm.contrarecibo = ?";
        $query = $this->db->query($sql, array($contrarecibo));
        return $query->result_array();
    }

    /**
     * Aqui empieza la parte de movimientos bancarios
     */

    function apartarMovimientoBancario($ultimo, $cuenta, $id_cuenta, $t_movimiento) {
        $id = $this->tank_auth->get_user_id();

        $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
        $resultado_query_usuario = $this->db->query($query_usuario, array($id));
        $nombre_encontrado = $resultado_query_usuario->row();
        $nombre_completo = $nombre_encontrado->nombre." ".$nombre_encontrado->apellido_paterno." ".$nombre_encontrado->apellido_materno;

        $data = array(
            'movimiento' => $ultimo,
            'id_cuenta' => $id_cuenta,
            'cuenta' => $cuenta,
            'id_usuario' => $this->tank_auth->get_user_id(),
            'creado_por' => $nombre_completo,
            't_movimiento' => $t_movimiento,
        );

        $query = $this->db->insert('mov_bancos_movimientos', $data);

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function cancelarCaratulaMovimiento($datos) {
        $this->db->select('*')->from('mov_bancos_movimientos')->where('movimiento', $datos["movimiento"]);
        $query = $this->db->get();
        $firme = $query->row_array();

        if($firme["enfirme"] == 1) {
            $this->db->where('id_cuentas_bancarias', $firme["id_cuenta"]);
            $this->db->select('saldo');

            $query = $this->db->get('cat_cuentas_bancarias');

            $cuenta =  $query->row();

            $saldo = $cuenta->saldo - $firme["importe"];

            $data = array(
                'saldo' => $saldo,
            );

            $this->db->where('id_cuentas_bancarias', $firme["id_cuenta"]);
            $this->db->update('cat_cuentas_bancarias', $data);
        }

        if($firme["t_movimiento"] == "presupuesto") {
            $this->ciclo_model->desmarcar_ejercido($firme["contrarecibo"], $firme["fecha_pago"]);
        }

        $datos_actualizar = array(
            "cancelada" => 1,
            "enfirme" => 0,
            "fecha_cancelada" => date("Y-m-d"),
        );

        $this->db->where('movimiento', $datos["movimiento"]);
        $query_actualizar = $this->db->update('mov_bancos_movimientos', $datos_actualizar);

        if($query_actualizar) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function getCuentasBancarias() {
        //$query = $this->db->get('cat_cuentas_bancarias');
        //return $query->result();
        $sql = "SELECT id_cuentas_bancarias, banco, cuenta, moneda FROM cat_cuentas_bancarias
                WHERE id_cuentas_bancarias = 4
                OR id_cuentas_bancarias = 6
                OR id_cuentas_bancarias = 10
                OR id_cuentas_bancarias = 11;";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    function getCuentasBancariasConciliacion() {
        $query = $this->db->get('cat_cuentas_bancarias');
        return $query->result();
    }

    function getCuenta($id) {
        $query = $this->db->get_where('cat_cuentas_bancarias', array('id_cuentas_bancarias' => $id));
        return $query->row();
    }

    function datos_CuentaBancaria($id) {
        $sql = "SELECT * FROM mov_bancos_movimientos WHERE id_cuenta = ? ORDER BY movimiento DESC;";
        $query = $this->db->query($sql, array('id_cuenta' => $id));
        return $query->result();
    }

    function datosMovimiento($id) {
        $query = $this->db->get_where('mov_bancos_movimientos', array('movimiento' => $id));
        return $query->row();
    }

    function ultimo_movimientoBancario() {
        $sql = "SELECT movimiento as ultimo FROM mov_bancos_movimientos ORDER BY movimiento DESC LIMIT 1;";
        $query = $this->db->query($sql);
        $row = $query->row();
        if(!$row) {
            $row = 0;
        }
        return $row;
    }

    function insertar_movimiento_bancario($datos) {

        $id = $this->tank_auth->get_user_id();

        $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
        $resultado_query_usuario = $this->db->query($query_usuario, array($id));
        $nombre_encontrado = $resultado_query_usuario->row();
        $nombre_completo = $nombre_encontrado->nombre." ".$nombre_encontrado->apellido_paterno." ".$nombre_encontrado->apellido_materno;

        $data = array(
            "id_cuenta" => $datos["hidden_id_cuenta"],
            "cuenta" => $datos["hidden_cuenta"],
            "movimiento" => $datos["movimiento"],
            "clave" => $datos["hidden_clave_concepto"],
            "concepto" => $datos["concepto"],
            "t_movimiento" => "extra",
            "fecha_emision" => $datos["fecha_emision"],
            "fecha_emision_real" => date("Y-m-d"),
            "fecha_movimiento" => date("Y-m-d"),
            "fecha_movimiento_real" => date("Y-m-d"),
            "hora_movimiento" => $datos["tiempo_movimiento"],
            "cheque" => $datos["cheque"],
            "movimiento_bancario" => $datos["movimiento_bancario"],
            "importe" => $datos["importe"],
            "cargo" => $datos["importe"],
            "id_proveedor" => $datos["id_proveedor"],
            "proveedor" => $datos["proveedor"],
            "fecha_pago" => $datos["fecha_pago"],
            "concepto_especifico" => $datos["concepto_especifico"],
            "descripcion" => $datos["descripcion_general"],
            "tipo" => $datos["tipo_movimiento"],
            'creado_por' => $nombre_completo,
            'enfirme' => $datos["check_firme"],
            'id_usuario' => $this->tank_auth->get_user_id(),
        );

        $this->db->where('movimiento', $datos["movimiento"]);
        $query = $this->db->update('mov_bancos_movimientos', $data);

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function insertar_movimiento_bancario_presupuesto($datos) {
        $sql_check_contrarecibo = "SELECT contrarecibo FROM mov_bancos_movimientos WHERE contrarecibo = ?;";
        $query_check_contrarecibo = $this->db->query($sql_check_contrarecibo, array($datos["numero_contrarecibo"]));
        $result_check_contrarecibo = $query_check_contrarecibo->row_array();

        // $this->debugeo->imprimir_pre($result_check_compromiso);

        if(isset($result_check_contrarecibo["contrarecibo"]) && $result_check_contrarecibo["contrarecibo"] != NULL && $result_check_contrarecibo["contrarecibo"] != "") {
            if($result_check_contrarecibo["contrarecibo"] != $datos["numero_contrarecibo"]) {
                return "usado";
            }
        }

        $usado = array(
            "usado" => 1
        );

        $this->db->where('id_contrarecibo_caratula', $datos["numero_contrarecibo"]);
        $this->db->update('mov_contrarecibo_caratula', $usado);

        $id = $this->tank_auth->get_user_id();

        $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
        $resultado_query_usuario = $this->db->query($query_usuario, array($id));
        $nombre_encontrado = $resultado_query_usuario->row();
        $nombre_completo = $nombre_encontrado->nombre." ".$nombre_encontrado->apellido_paterno." ".$nombre_encontrado->apellido_materno;

        // $this->debugeo->imprimir_pre($datos);

        $data = array(
            "id_cuenta" => $datos["hidden_id_cuenta"],
            "cuenta" => $datos["hidden_cuenta"],
            "movimiento" => $datos["ultimo_movimiento"],
            "clave" => $datos["hidden_clave_concepto"],
            "concepto" => $datos["concepto"],
            "t_movimiento" => "presupuesto",
            "fecha_emision" => $datos["fecha_emision"],
            "fecha_emision_real" => date("Y-m-d"),
            "fecha_movimiento" => date("Y-m-d"),
            "fecha_movimiento_real" => date("Y-m-d"),
            "hora_movimiento" => $datos["tiempo_movimiento"],
            "cheque" => $datos["cheque"],
            "movimiento_bancario" => $datos["movimiento_bancario"],
            "importe" => $datos["importe"],
            "neto" => $datos["neto"],
            "cargo" => $datos["importe"],
            "id_proveedor" => $datos["id_proveedor"],
            "proveedor" => $datos["proveedor"],
            "fecha_pago" => $datos["fecha_pago"],
            "concepto_especifico" => $datos["concepto_especifico"],
            "descripcion" => $datos["descripcion_general"],
            "tipo" => $datos["tipo_movimiento"],
            'creado_por' => $nombre_completo,
            'enfirme' => $datos["check_firme"],
            'id_usuario' => $this->tank_auth->get_user_id(),
            'contrarecibo' => $datos["numero_contrarecibo"],
            'honorarios' => $datos["honorarios"],
        );

        $this->db->where('movimiento', $datos["ultimo_movimiento"]);
        $query = $this->db->update('mov_bancos_movimientos', $data);

        if($query) {
            return "exito";
        }
        else {
            return "error";
        }
    }

    function insertar_movimiento_bancario_conciliacion($datos) {
        $id = $this->tank_auth->get_user_id();

        $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
        $resultado_query_usuario = $this->db->query($query_usuario, array($id));
        $nombre_encontrado = $resultado_query_usuario->row();
        $nombre_completo = $nombre_encontrado->nombre." ".$nombre_encontrado->apellido_paterno." ".$nombre_encontrado->apellido_materno;

        $data = array(
            "id_cuenta" => $datos["hidden_id_cuenta"],
            "cuenta" => $datos["hidden_cuenta"],
            "movimiento" => $datos["ultimo_movimiento"],
            "clave" => $datos["hidden_clave_concepto"],
            "concepto" => $datos["concepto"],
            "t_movimiento" => $datos["tipo_hidden"],
            "fecha_emision" => $datos["fecha_emision"],
            "fecha_emision_real" => date("Y-m-d"),
            "fecha_movimiento" => date("Y-m-d"),
            "fecha_movimiento_real" => date("Y-m-d"),
            "hora_movimiento" => $datos["tiempo_movimiento"],
            "importe" => $datos["importe"],
            "cargo" => $datos["importe"],
            "id_proveedor" => $datos["id_proveedor"],
            "proveedor" => $datos["proveedor"],
            "fecha_pago" => $datos["fecha_pago"],
            "descripcion" => $datos["descripcion_general"],
            "tipo" => $datos["tipo_movimiento"],
            "cuenta_cargo" => $datos["cuenta_cargo"],
            "descripcion_cuenta_cargo" => $datos["descripcion_cuenta_cargo"],
            "cuenta_abono" => $datos["cuenta_abono"],
            "descripcion_cuenta_abono" => $datos["descripcion_cuenta_abono"],
            'creado_por' => $nombre_completo,
            'enfirme' => $datos["check_firme"],
            'id_usuario' => $this->tank_auth->get_user_id(),
        );

        $query = $this->db->insert('mov_bancos_movimientos', $data);

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function actualizarPoliza($datos) {
        $data = array(
            'cuenta_cargo' => $datos["cuenta_cargo"],
            'descripcion_cuenta_cargo' => $datos["descripcion_cuenta_cargo"],
            'cuenta_abono' => $datos["cuenta_abono"],
            'descripcion_cuenta_abono' => $datos["descripcion_cuenta_abono"],
            'enfirme' => $datos["firme"],
        );

        $this->db->where('id_bancos_movimientos', $datos["movimiento"]);
        $query = $this->db->update('mov_bancos_movimientos', $data);

        if($query) {
            return TRUE;
        }
        return FALSE;
    }

    function actualizar_movimiento_bancario($datos) {

        $data = array(
            "clave" => $datos["hidden_clave_concepto"],
            "concepto" => $datos["concepto"],
            "t_movimiento" => "extra",
            "fecha_emision" => $datos["fecha_emision"],
            "hora_movimiento" => $datos["tiempo_movimiento"],
            "cheque" => $datos["cheque"],
            "movimiento_bancario" => $datos["movimiento_bancario"],
            "importe" => $datos["importe"],
            "cargo" => $datos["importe"],
            "id_proveedor" => $datos["id_proveedor"],
            "proveedor" => $datos["proveedor"],
            "fecha_pago" => $datos["fecha_pago"],
            "concepto_especifico" => $datos["concepto_especifico"],
            "descripcion" => $datos["descripcion_general"],
            "tipo" => $datos["tipo_movimiento"],
            'enfirme' => $datos["check_firme"],
        );

        $this->db->where('id_bancos_movimientos', $datos["movimiento"]);
        $query = $this->db->update('mov_bancos_movimientos', $data);

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function actualizar_movimiento_bancario_presupuesto($datos) {
        $sql_check_contrarecibo = "SELECT contrarecibo FROM mov_bancos_movimientos WHERE contrarecibo = ?;";
        $query_check_contrarecibo = $this->db->query($sql_check_contrarecibo, array($datos["numero_contrarecibo"]));
        $result_check_contrarecibo = $query_check_contrarecibo->row_array();

        // $this->debugeo->imprimir_pre($datos);

        if(isset($result_check_contrarecibo["contrarecibo"]) && $result_check_contrarecibo["contrarecibo"] != NULL && $result_check_contrarecibo["contrarecibo"] != "") {
            if($result_check_contrarecibo["contrarecibo"] != $datos["numero_contrarecibo"]) {
                return "usado";
            }
        }


        $data = array(
            "id_cuenta" => $datos["hidden_id_cuenta"],
            "cuenta" => $datos["hidden_cuenta"],
            "clave" => $datos["hidden_clave_concepto"],
            "concepto" => $datos["concepto"],
            "t_movimiento" => "presupuesto",
            "fecha_emision" => $datos["fecha_emision"],
            "hora_movimiento" => $datos["tiempo_movimiento"],
            "cheque" => $datos["cheque"],
            "movimiento_bancario" => $datos["movimiento_bancario"],
            "importe" => $datos["importe"],
            "neto" => $datos["neto"],
            "cargo" => $datos["importe"],
            "id_proveedor" => $datos["id_proveedor"],
            "proveedor" => $datos["proveedor"],
            "fecha_pago" => $datos["fecha_pago"],
            "descripcion" => $datos["descripcion_general"],
            "tipo" => $datos["tipo_movimiento"],
            'enfirme' => $datos["check_firme"],
            'id_usuario' => $this->tank_auth->get_user_id(),
            'contrarecibo' => $datos["numero_contrarecibo"],
        );

        $this->db->where('movimiento', $datos["movimiento"]);
        $query = $this->db->update('mov_bancos_movimientos', $data);

        if($query) {
            return "exito";
        }
        else {
            return "error";
        }
    }

    function datosBancosConceptos() {
        $sql = "SELECT * FROM cat_bancos_conceptos;";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    function datosCuentasContables() {
        $sql = "SELECT * FROM cat_cuentas_contables WHERE tipo = 'D';";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    function datosCuentasContablesDetalle($numero) {
        $sql = "SELECT * FROM mov_bancos_pagos_contrarecibos WHERE movimiento = ?;";
        $query = $this->db->query($sql, array($numero));
        $result = $query->result();
        return $result;
    }

    function conciliarMovimiento($id, $fecha) {
        $data = array(
            "sifirme" => 1,
            "fecha_conciliado" => $fecha,
        );

        $this->db->where('id_bancos_movimientos', $id);
        $query = $this->db->update('mov_bancos_movimientos', $data);

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function quitarConciliarMovimiento($id) {
        $data = array(
            "sifirme" => 0,
            "fecha_conciliado" => "0000-00-00",
        );

        $this->db->where('id_bancos_movimientos', $id);
        $query = $this->db->update('mov_bancos_movimientos', $data);

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function datos_contrarecibo() {
        $sql = "SELECT * FROM mov_contrarecibo_caratula WHERE enfirme = 1 AND cancelada = 0 AND usado = 0 ORDER BY id_contrarecibo_caratula DESC;";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    function copiarDatosContrarecibo($contrarecibo, $cuenta, $movimiento) {

    $sql = "SELECT * FROM mov_contrarecibo_caratula WHERE id_contrarecibo_caratula = ?;";
    $query = $this->db->query($sql, array($contrarecibo));
    $result = $query->row();

//        $this->debugeo->imprimir_pre($result);

    $sql = "INSERT INTO mov_bancos_pagos_contrarecibos (cuenta, movimiento, numero, nombre, importe) VALUES (?, ?, ?, ?, ?);";
    $resultado = $this->db->query($sql, array($cuenta, $movimiento, $result->id_contrarecibo_caratula, $result->proveedor, $result->importe));

    if($resultado) {
        return TRUE;
    }
    else {
        return FALSE;
    }

}

    function datos_Caratula_Contrarecibo_Movimiento($contrarecibo) {
        $sql = "SELECT * FROM mov_contrarecibo_caratula WHERE id_contrarecibo_caratula = ?;";
        $query = $this->db->query($sql, array($contrarecibo));
        $result = $query->row();
        return $result;
    }

    function borrarDetalleMovimiento($movimiento) {

        $this->db->where('id_bancos_pagos_contrarecibos', $movimiento);
        $query = $this->db->delete('mov_bancos_pagos_contrarecibos');
        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }

    }

    function borrarMovimiento($movimiento) {

        $this->db->where('movimiento', $movimiento);
        $query = $this->db->delete('mov_bancos_pagos_contrarecibos');
        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }

    }

    function datosMovimientos($id_cuenta) {
        $sql = "SELECT * FROM mov_bancos_movimientos m
                JOIN cat_cuentas_bancarias c
                ON c.cuenta = m.cuenta where m.id_cuenta = $id_cuenta and m.enfirme = 1";

        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    function imprimirMovimientos($datos) {
        $sql = "SELECT * FROM mov_bancos_movimientos m
                JOIN cat_cuentas_bancarias c ON c.cuenta = m.cuenta
                WHERE m.id_cuenta = ?
				AND m.t_movimiento LIKE ?
				AND m.tipo LIKE ?
				AND m.id_proveedor LIKE ?
				AND m.movimiento LIKE ?
                AND m.enfirme = 1
                -- AND m.cancelada = 0
                AND m.fecha_pago >= ?
                AND m.fecha_pago <= ?;";

        $query = $this->db->query($sql, array($datos["id_cuenta"], "%".$datos["tipo"]."%", "%".$datos["tipo_movimiento"]."%",
                                            "%".$datos["id_proveedor"]."%", "%".$datos["movimiento"]."%", $datos["fecha_inicial"], $datos["fecha_final"]));
        $result = $query->result();
        return $result;
    }

    function get_centro_costo_API($libreria) {
        $sql = "SELECT clave_centro_costo FROM cat_centro_libreria WHERE clave_libreria = ?";
        $query = $this->db->query($sql, array($libreria));
        $result = $query->row();
        return $result->clave_centro_costo;
    }

    function tomar_cuentas_movimiento($partida) {
//        Se toman los datos de la cuenta que le corresponde a la partida a buscar
        $sql = "SELECT * FROM cat_correlacion_partidas_contables WHERE clave = ? AND destino = 'Pagado Gasto';";
        $query = $this->db->query($sql, array($partida));
//        Si existe la clave a buscar, se le agrega el centro de costos para saber si existe esa clave
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function tomar_cuentas_movimiento_egresos($partida) {
//        Se toman los datos de la cuenta que le corresponde a la partida a buscar
        $sql = "SELECT * FROM cat_correlacion_partidas_contables WHERE clave = ?;";
        $query = $this->db->query($sql, array($partida));
//        Si existe la clave a buscar, se le agrega el centro de costos para saber si existe esa clave
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    /**
     * Aqui empieza la parte de conciliación bancaria
     */

    function datosConciliacion($cuenta) {
        $sql = "SELECT * FROM mov_bancos_movimientos WHERE id_cuenta = $cuenta AND fecha_conciliado != '0000-00-00'";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    function imprimirConciliacion($datos) {
        $sql = "SELECT * FROM mov_bancos_movimientos m
                JOIN cat_cuentas_bancarias c ON c.cuenta = m.cuenta
                WHERE m.id_cuenta = ?
				AND m.tipo LIKE ?
				AND m.id_proveedor LIKE ?
				AND m.movimiento LIKE ?
                AND m.enfirme = 1
				AND m.cancelada = 0
                AND m.fecha_emision >= ?
                AND m.fecha_emision <= ?;";

        $query = $this->db->query($sql, array($datos["id_cuenta"], "%".$datos["tipo_movimiento"]."%",
            "%".$datos["id_proveedor"]."%", "%".$datos["movimiento"]."%", $datos["fecha_inicial"], $datos["fecha_final"]));
        $result = $query->result();
        return $result;
    }

    function get_datos_conciliacion($cuenta, $query) {
        $query = $this->db->query($query, array($cuenta));
        $result = $query->result();
        return $result;
    }

    function get_nombre_banco() {
        $sql = "SELECT c1.cuenta,  c2.banco
                FROM mov_bancos_movimientos c1
                LEFT JOIN cat_cuentas_bancarias c2 ON c1.cuenta = c2.cuenta
                GROUP BY c1.cuenta;";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    function marcar_ejercido($contrarecibo, $fecha) {
        $sql = "SELECT numero_compromiso FROM mov_contrarecibo_caratula WHERE id_contrarecibo_caratula = ?;";
        $query = $this->db->query($sql, array($contrarecibo));
        $resultado = $query->row_array();

        $sql_compromiso = "SELECT COLUMN_JSON(nivel) AS estructura, importe FROM mov_compromiso_detalle WHERE numero_compromiso = ?;";
        $query_compromiso = $this->db->query($sql_compromiso, array($resultado["numero_compromiso"]));
        $result = $query_compromiso->result_array();

        $query_actualizar = FALSE;

        foreach($result as $key => $value) {
            $estructura = json_decode($value["estructura"], TRUE);

            $mes = $this->utilerias->convertirFechaAMes($fecha);

            $sql_detalle = "SELECT COLUMN_GET(nivel, '".$mes."_ejercido' as char) as ejercido_mes,
                    COLUMN_GET(nivel, 'ejercido_anual' as char) as ejercido_anual
                        FROM cat_niveles
                            WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) = ?
                                AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) = ?
                                AND COLUMN_GET(nivel, 'centro_de_costos' as char) = ?
                                AND COLUMN_GET(nivel, 'capitulo' as char) = ?
                                AND COLUMN_GET(nivel, 'concepto' as char) = ?
                                AND COLUMN_GET(nivel, 'partida' as char) = ?";
            $query_detalle = $this->db->query($sql_detalle, array(
                $estructura["fuente_de_financiamiento"],
                $estructura["programa_de_financiamiento"],
                $estructura["centro_de_costos"],
                $estructura["capitulo"],
                $estructura["concepto"],
                $estructura["partida"],
            ));
            $result_detalle = $query_detalle->row_array();

            $result_detalle["ejercido_mes"] += round($value["importe"], 2);
            $result_detalle["ejercido_anual"] += round($value["importe"], 2);

            $sql_actualizar = "UPDATE cat_niveles SET
                            nivel = COLUMN_ADD(nivel, '".$mes."_ejercido', ?),
                            nivel = COLUMN_ADD(nivel, 'ejercido_anual', ?)
                                WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) = ?
                                    AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) = ?
                                    AND COLUMN_GET(nivel, 'centro_de_costos' as char) = ?
                                    AND COLUMN_GET(nivel, 'capitulo' as char) = ?
                                    AND COLUMN_GET(nivel, 'concepto' as char) = ?
                                    AND COLUMN_GET(nivel, 'partida' as char) = ?;";
            $query_actualizar = $this->db->query($sql_actualizar, array(
                $result_detalle["ejercido_mes"],
                $result_detalle["ejercido_anual"],
                $estructura["fuente_de_financiamiento"],
                $estructura["programa_de_financiamiento"],
                $estructura["centro_de_costos"],
                $estructura["capitulo"],
                $estructura["concepto"],
                $estructura["partida"],
            ));

        }

        if($query_actualizar) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function desmarcar_ejercido($contrarecibo, $fecha) {
        $sql = "SELECT numero_compromiso FROM mov_contrarecibo_caratula WHERE id_contrarecibo_caratula = ?;";
        $query = $this->db->query($sql, array($contrarecibo));
        $resultado = $query->row_array();

//        $this->debugeo->imprimir_pre($resultado);

        $sql_compromiso = "SELECT COLUMN_JSON(nivel) AS estructura, importe FROM mov_compromiso_detalle WHERE numero_compromiso = ?;";
        $query_compromiso = $this->db->query($sql_compromiso, array($resultado["numero_compromiso"]));
        $result = $query_compromiso->result_array();

//        $this->debugeo->imprimir_pre($result);

        $query_actualizar = FALSE;

        foreach($result as $key => $value) {
            $estructura = json_decode($value["estructura"], TRUE);

            $mes = $this->utilerias->convertirFechaAMes($fecha);

            $sql_detalle = "SELECT COLUMN_GET(nivel, '".$mes."_ejercido' as char) as ejercido_mes,
                    COLUMN_GET(nivel, 'ejercido_anual' as char) as ejercido_anual
                        FROM cat_niveles
                            WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) = ?
                                AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) = ?
                                AND COLUMN_GET(nivel, 'centro_de_costos' as char) = ?
                                AND COLUMN_GET(nivel, 'capitulo' as char) = ?
                                AND COLUMN_GET(nivel, 'concepto' as char) = ?
                                AND COLUMN_GET(nivel, 'partida' as char) = ?";
            $query_detalle = $this->db->query($sql_detalle, array(
                $estructura["fuente_de_financiamiento"],
                $estructura["programa_de_financiamiento"],
                $estructura["centro_de_costos"],
                $estructura["capitulo"],
                $estructura["concepto"],
                $estructura["partida"],
            ));
            $result_detalle = $query_detalle->row_array();

            $result_detalle["ejercido_mes"] -= round($value["importe"], 2);
            $result_detalle["ejercido_anual"] -= round($value["importe"], 2);

            $sql_actualizar = "UPDATE cat_niveles SET
                            nivel = COLUMN_ADD(nivel, '".$mes."_ejercido', ?),
                            nivel = COLUMN_ADD(nivel, 'ejercido_anual', ?)
                                WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) = ?
                                    AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) = ?
                                    AND COLUMN_GET(nivel, 'centro_de_costos' as char) = ?
                                    AND COLUMN_GET(nivel, 'capitulo' as char) = ?
                                    AND COLUMN_GET(nivel, 'concepto' as char) = ?
                                    AND COLUMN_GET(nivel, 'partida' as char) = ?;";
            $query_actualizar = $this->db->query($sql_actualizar, array(
                $result_detalle["ejercido_mes"],
                $result_detalle["ejercido_anual"],
                $estructura["fuente_de_financiamiento"],
                $estructura["programa_de_financiamiento"],
                $estructura["centro_de_costos"],
                $estructura["capitulo"],
                $estructura["concepto"],
                $estructura["partida"],
            ));

        }

        if($query_actualizar) {

//            $this->debugeo->imprimir_pre($contrarecibo);

            $sql_actualizar_usado = "UPDATE mov_contrarecibo_caratula SET usado = 0 WHERE id_contrarecibo_caratula = ?;";
            $query_actualizar_usado = $this->db->query($sql_actualizar_usado, array(
                $contrarecibo
            ));

//            if($query_actualizar_usado) {
//                $this->debugeo->imprimir_pre("Exito al actualizar el contrarecibo");
//            } else {
//                $this->debugeo->imprimir_pre("Hubo un error");
//            }

            return TRUE;
        } else {
            return FALSE;
        }
    }

    function devolver_dinero_cancelado($fecha, $estructura, $importe, $tipo_movimiento) {
        $anual = "";

        if($tipo_movimiento == "precompromiso") {
            $anual = "precomprometido";
        } elseif($tipo_movimiento == "compromiso") {
            $anual = "comprometido";
        } elseif($tipo_movimiento == "devengado") {
            $anual = "devengado";
        }

        $mes_actual = $this->utilerias->convertirFechaAMes($fecha);

        $sql = "SELECT COLUMN_GET(nivel, '".$mes_actual."_".$tipo_movimiento."' as char) as movimiento_mes,
                COLUMN_GET(nivel, '".$mes_actual."_".$tipo_movimiento."' as char) as devengado_mes,
                COLUMN_GET(nivel, '".$mes_actual."_saldo' as char) as saldo_mes,
                COLUMN_GET(nivel, 'total_anual' as char) as saldo_anual,
                COLUMN_GET(nivel, 'devengado_anual' as char) as devengado_anual,
                COLUMN_GET(nivel, '".$anual."_anual' as char) as movimiento_anual
                FROM cat_niveles
                    WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) = ?
                        AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) = ?
                        AND COLUMN_GET(nivel, 'centro_de_costos' as char) = ?
                        AND COLUMN_GET(nivel, 'capitulo' as char) = ?
                        AND COLUMN_GET(nivel, 'concepto' as char) = ?
                        AND COLUMN_GET(nivel, 'partida' as char) = ?";
        $query = $this->db->query($sql, array(
            $estructura["fuente_de_financiamiento"],
            $estructura["programa_de_financiamiento"],
            $estructura["centro_de_costos"],
            $estructura["capitulo"],
            $estructura["concepto"],
            $estructura["partida"],
            ));
        $result = $query->row_array();

        $query_actualizar = FALSE;

        if($tipo_movimiento == "compromiso") {
            $result["movimiento_mes"] -= $importe;
            $result["movimiento_anual"] -= $importe;

            $result["saldo_mes"] += $importe;
            $result["saldo_anual"] += $importe;

            $sql_actualizar = "UPDATE cat_niveles SET
                            nivel = COLUMN_ADD(nivel, '".$mes_actual."_".$tipo_movimiento."', ?),
                            nivel = COLUMN_ADD(nivel, '".$mes_actual."_saldo', ?),
                            nivel = COLUMN_ADD(nivel, 'total_anual', ?),
                            nivel = COLUMN_ADD(nivel, '".$anual."_anual', ?)
                                WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) = ?
                                    AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) = ?
                                    AND COLUMN_GET(nivel, 'centro_de_costos' as char) = ?
                                    AND COLUMN_GET(nivel, 'capitulo' as char) = ?
                                    AND COLUMN_GET(nivel, 'concepto' as char) = ?
                                    AND COLUMN_GET(nivel, 'partida' as char) = ?;";
            $query_actualizar = $this->db->query($sql_actualizar, array(
                $result["movimiento_mes"],
                $result["saldo_mes"],
                $result["saldo_anual"],
                $result["movimiento_anual"],
                $estructura["fuente_de_financiamiento"],
                $estructura["programa_de_financiamiento"],
                $estructura["centro_de_costos"],
                $estructura["capitulo"],
                $estructura["concepto"],
                $estructura["partida"],
            ));

        } elseif($tipo_movimiento == "precompromiso") {
            $result["movimiento_mes"] -= $importe;
            $result["movimiento_anual"] -= $importe;

            $sql_actualizar = "UPDATE cat_niveles SET
                            nivel = COLUMN_ADD(nivel, '".$mes_actual."_".$tipo_movimiento."', ?),
                            nivel = COLUMN_ADD(nivel, '".$anual."_anual', ?)
                                WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) = ?
                                    AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) = ?
                                    AND COLUMN_GET(nivel, 'centro_de_costos' as char) = ?
                                    AND COLUMN_GET(nivel, 'capitulo' as char) = ?
                                    AND COLUMN_GET(nivel, 'concepto' as char) = ?
                                    AND COLUMN_GET(nivel, 'partida' as char) = ?;";
            $query_actualizar = $this->db->query($sql_actualizar, array(
                $result["movimiento_mes"],
                $result["movimiento_anual"],
                $estructura["fuente_de_financiamiento"],
                $estructura["programa_de_financiamiento"],
                $estructura["centro_de_costos"],
                $estructura["capitulo"],
                $estructura["concepto"],
                $estructura["partida"],
            ));
        } elseif($tipo_movimiento == "devengado") {
            $result["devengado_mes"] -= $importe;
            $result["devengado_anual"] -= $importe;

            $sql_actualizar = "UPDATE cat_niveles SET
                            nivel = COLUMN_ADD(nivel, '".$mes_actual."_".$tipo_movimiento."', ?),
                            nivel = COLUMN_ADD(nivel, '".$anual."_anual', ?)
                                WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) = ?
                                    AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) = ?
                                    AND COLUMN_GET(nivel, 'centro_de_costos' as char) = ?
                                    AND COLUMN_GET(nivel, 'capitulo' as char) = ?
                                    AND COLUMN_GET(nivel, 'concepto' as char) = ?
                                    AND COLUMN_GET(nivel, 'partida' as char) = ?;";
            $query_actualizar = $this->db->query($sql_actualizar, array(
                $result["devengado_mes"],
                $result["devengado_anual"],
                $estructura["fuente_de_financiamiento"],
                $estructura["programa_de_financiamiento"],
                $estructura["centro_de_costos"],
                $estructura["capitulo"],
                $estructura["concepto"],
                $estructura["partida"],
            ));
        }

        if($query_actualizar) {
            return TRUE;
        } else {
            return FALSE;
        }

    }

    function devolver_dinero_precompromiso_compromiso($precompromiso, $importe, $estructura) {

        $this->db->select('total_compromiso, fecha_emision')->from('mov_precompromiso_caratula')->where('numero_pre', $precompromiso);
        $query = $this->db->get();
        $datos_precompromiso = $query->row_array();

        $anual = "";

        $mes_actual = $this->utilerias->convertirFechaAMes($datos_precompromiso["fecha_emision"]);

        $sql_precompro = "SELECT COLUMN_GET(nivel, '".$mes_actual."_precompromiso' as char) as movimiento_mes,
                COLUMN_GET(nivel, 'precomprometido_anual' as char) as movimiento_anual
                FROM cat_niveles
                    WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) = ?
                        AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) = ?
                        AND COLUMN_GET(nivel, 'centro_de_costos' as char) = ?
                        AND COLUMN_GET(nivel, 'capitulo' as char) = ?
                        AND COLUMN_GET(nivel, 'concepto' as char) = ?
                        AND COLUMN_GET(nivel, 'partida' as char) = ?";
        $query_precompro = $this->db->query($sql_precompro, array(
            $estructura["fuente_de_financiamiento"],
            $estructura["programa_de_financiamiento"],
            $estructura["centro_de_costos"],
            $estructura["capitulo"],
            $estructura["concepto"],
            $estructura["partida"],
        ));
        $result_precompro = $query_precompro->row_array();

        $result_precompro["movimiento_mes"] += $importe;
        $result_precompro["movimiento_anual"] += $importe;

        $sql_actualizar = "UPDATE cat_niveles SET
                            nivel = COLUMN_ADD(nivel, '".$mes_actual."_precompromiso', ?),
                            nivel = COLUMN_ADD(nivel, 'precomprometido_anual', ?)
                                WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) = ?
                                    AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) = ?
                                    AND COLUMN_GET(nivel, 'centro_de_costos' as char) = ?
                                    AND COLUMN_GET(nivel, 'capitulo' as char) = ?
                                    AND COLUMN_GET(nivel, 'concepto' as char) = ?
                                    AND COLUMN_GET(nivel, 'partida' as char) = ?;";
        $query_actualizar = $this->db->query($sql_actualizar, array(
            $result_precompro["movimiento_mes"],
            $result_precompro["movimiento_anual"],
            $estructura["fuente_de_financiamiento"],
            $estructura["programa_de_financiamiento"],
            $estructura["centro_de_costos"],
            $estructura["capitulo"],
            $estructura["concepto"],
            $estructura["partida"],
        ));

        $total = $datos_precompromiso["total_compromiso"] - $importe;

        $datos_actualizar = array(
            'total_compromiso' => $total,
        );

        $this->db->where('numero_pre', $precompromiso);
        $resultado_actualizar = $this->db->update('mov_precompromiso_caratula', $datos_actualizar);

        if($resultado_actualizar) {
            return TRUE;
        } else {
            return FALSE;
        }
//        $this->debugeo->imprimir_pre($importe);
    }

    function devolver_dinero_contrarecibo_cancelado($contrarecibo, $estructura, $importe, $tipo_movimiento) {
        $anual = "";

        if($tipo_movimiento == "precompromiso") {
            $anual = "precomprometido";
        } elseif($tipo_movimiento == "compromiso") {
            $anual = "comprometido";
        }

        $mes_actual = $this->utilerias->convertirFechaAMes($fecha);

        $sql = "SELECT COLUMN_GET(nivel, '".$mes_actual."_".$tipo_movimiento."' as char) as movimiento_mes,
                COLUMN_GET(nivel, '".$mes_actual."_saldo' as char) as saldo_mes,
                COLUMN_GET(nivel, 'total_anual' as char) as saldo_anual,
                COLUMN_GET(nivel, '".$anual."_anual' as char) as movimiento_anual
                FROM cat_niveles
                    WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) = ?
                        AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) = ?
                        AND COLUMN_GET(nivel, 'centro_de_costos' as char) = ?
                        AND COLUMN_GET(nivel, 'capitulo' as char) = ?
                        AND COLUMN_GET(nivel, 'concepto' as char) = ?
                        AND COLUMN_GET(nivel, 'partida' as char) = ?";
        $query = $this->db->query($sql, array(
            $estructura["fuente_de_financiamiento"],
            $estructura["programa_de_financiamiento"],
            $estructura["centro_de_costos"],
            $estructura["capitulo"],
            $estructura["concepto"],
            $estructura["partida"],
        ));
        $result = $query->row_array();

        $query_actualizar = FALSE;

        if($tipo_movimiento == "compromiso") {
            $result["movimiento_mes"] -= $importe;
            $result["movimiento_anual"] -= $importe;

            $result["saldo_mes"] += $importe;
            $result["saldo_anual"] += $importe;

            $sql_actualizar = "UPDATE cat_niveles SET
                            nivel = COLUMN_ADD(nivel, '".$mes_actual."_".$tipo_movimiento."', ?),
                            nivel = COLUMN_ADD(nivel, '".$mes_actual."_saldo', ?),
                            nivel = COLUMN_ADD(nivel, 'total_anual', ?),
                            nivel = COLUMN_ADD(nivel, '".$anual."_anual', ?)
                                WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) = ?
                                    AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) = ?
                                    AND COLUMN_GET(nivel, 'centro_de_costos' as char) = ?
                                    AND COLUMN_GET(nivel, 'capitulo' as char) = ?
                                    AND COLUMN_GET(nivel, 'concepto' as char) = ?
                                    AND COLUMN_GET(nivel, 'partida' as char) = ?;";
            $query_actualizar = $this->db->query($sql_actualizar, array(
                $result["movimiento_mes"],
                $result["saldo_mes"],
                $result["saldo_anual"],
                $result["movimiento_anual"],
                $estructura["fuente_de_financiamiento"],
                $estructura["programa_de_financiamiento"],
                $estructura["centro_de_costos"],
                $estructura["capitulo"],
                $estructura["concepto"],
                $estructura["partida"],
            ));

        } elseif($tipo_movimiento == "compromiso") {
            $result["movimiento_mes"] -= $importe;
            $result["movimiento_anual"] -= $importe;

            $sql_actualizar = "UPDATE cat_niveles SET
                            nivel = COLUMN_ADD(nivel, '".$mes_actual."_".$tipo_movimiento."', ?),
                            nivel = COLUMN_ADD(nivel, '".$anual."_anual', ?)
                                WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) = ?
                                    AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) = ?
                                    AND COLUMN_GET(nivel, 'centro_de_costos' as char) = ?
                                    AND COLUMN_GET(nivel, 'capitulo' as char) = ?
                                    AND COLUMN_GET(nivel, 'concepto' as char) = ?
                                    AND COLUMN_GET(nivel, 'partida' as char) = ?;";
            $query_actualizar = $this->db->query($sql_actualizar, array(
                $result["movimiento_mes"],
                $result["movimiento_anual"],
                $estructura["fuente_de_financiamiento"],
                $estructura["programa_de_financiamiento"],
                $estructura["centro_de_costos"],
                $estructura["capitulo"],
                $estructura["concepto"],
                $estructura["partida"],
            ));
        }

        if($query_actualizar) {
            return TRUE;
        } else {
            return FALSE;
        }

    }

    function get_arreglo_datos($sql) {
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }

}