<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Egresos_model extends CI_Model {

    function __construct() {
    // Call the Model constructor
        parent::__construct();
    }

    /**
     * Esta funcion sirve para contar los niveles dentro de la estructura de los egresos
     * @return conteo
     */
    function contar_egresos_elementos() {
        $sql = "SELECT COUNT(id_egresos_elementos) AS conteo FROM cat_egresos_elementos;";
        $query = $this->db->query($sql);
        $row = $query->row();
        return $row;
    }

    /**
     * Esta funcion sirve para obtener los nombres de los niveles de la estructura de egresos
     * @return arreglo de nombres
     */
    function obtener_nombre_niveles() {
        $sql = "SELECT descripcion FROM cat_egresos_elementos;";
        $query = $this->db->query($sql);
        $resultado = $query->result();
        return $resultado;
    }

    /**
     * Esta funcion pasa a minusculas el nombre del primer nivel, y reemplaza los espacios en blanco por guión bajo, para su busqueda en la base de datos
     * @param $nombre del primer nivel
     * @return nombre del primer nivel y su valor
     */
    function datos_primerNivel($nombre) {
        $sql = "SELECT COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre))."' as char) AS ".strtolower(str_replace(' ', '_', $nombre)).", COLUMN_GET(nivel, 'descripcion' as char) AS descripcion FROM cat_niveles WHERE COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre))."' as char) != '' GROUP BY ".strtolower(str_replace(' ', '_', $nombre))." ;";
        $query = $this->db->query($sql);
        $resultado = $query->result();
        return $resultado;
    }

    /**
     * Esta funcion busca el segundo nivel, conforme al primer nivel de la estructura de egresos
     * @param $valor del primer nivel
     * @return nombre y valor del segundo nivel
     */
    function datos_nivel2($nivel1, $nombre) {
        $sql = "SELECT COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[1]))."' as char) AS ".strtolower(str_replace(' ', '_', $nombre[1])).",
                    COLUMN_GET(nivel, 'descripcion' as char) AS descripcion
                    FROM cat_niveles
                    WHERE COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[0]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[1]))."' as char) != '' GROUP BY ".strtolower(str_replace(' ', '_', $nombre[1])).";";
        $query = $this->db->query($sql, array($nivel1));
        $resultado = $query->result();
        return $resultado;
    }

    function buscar_nivel1($nivel_buscar1, $nombre) {
        $sql = "SELECT COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[0]))."' as char) AS ".strtolower(str_replace(' ', '_', $nombre[0])).",
                COLUMN_GET(nivel, 'descripcion' as char) AS descripcion
                FROM cat_niveles
                WHERE COLUMN_GET(nivel, 'descripcion' as char) LIKE ?
                AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[1]))."' as char) = ''
                AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[2]))."' as char) = ''
                AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[3]))."' as char) = ''
                AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[4]))."' as char) = ''
                AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[5]))."' as char) = ''
                AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[0]))."' as char) != ''
                GROUP BY ".strtolower(str_replace(' ', '_', $nombre[0])).";";
        $query = $this->db->query($sql, array("%".$nivel_buscar1."%"));
        $resultado = $query->result();
        return $resultado;
    }

    function buscar_nivel2($nivel_buscar1, $nivel_buscar2, $nombre) {
        $sql = "SELECT COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[1]))."' as char) AS ".strtolower(str_replace(' ', '_', $nombre[1])).",
                COLUMN_GET(nivel, 'descripcion' as char) AS descripcion
                FROM cat_niveles
                WHERE COLUMN_GET(nivel, 'descripcion' as char) LIKE ?
                AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[0]))."' as char) = ?
                AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[2]))."' as char) = ''
                AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[3]))."' as char) = ''
                AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[4]))."' as char) = ''
                AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[5]))."' as char) = ''
                AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[1]))."' as char) != ''
                GROUP BY ".strtolower(str_replace(' ', '_', $nombre[1])).";";
        $query = $this->db->query($sql, array("%".$nivel_buscar2."%", $nivel_buscar1));
        $resultado = $query->result();
        return $resultado;
    }

    /**
     * Esta funcion busca el tercer nivel, conforme al primer y segundo nivel de la estructura de egresos
     * @param $nombre primer nivel
     * @param $nombre segundo nivel
     * @return nombre y valor del tercer nivel
     */
    function datos_nivel3($nivel1, $nivel2, $nombre) {
        $sql = "SELECT COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[2]))."' as char) AS ".strtolower(str_replace(' ', '_', $nombre[2])).",
                    COLUMN_GET(nivel, 'descripcion' as char) AS descripcion
                    FROM cat_niveles
                    WHERE COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[0]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[1]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[2]))."' as char) != '' GROUP BY ".strtolower(str_replace(' ', '_', $nombre[2])).";";
        $query = $this->db->query($sql, array($nivel1, $nivel2));
        $resultado = $query->result();
        return $resultado;
    }

    function buscar_nivel3($nivel_buscar1, $nivel_buscar2, $nivel_buscar3, $nombre) {
        $sql = "SELECT COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[2]))."' as char) AS ".strtolower(str_replace(' ', '_', $nombre[2])).",
                COLUMN_GET(nivel, 'descripcion' as char) AS descripcion
                FROM cat_niveles
                WHERE COLUMN_GET(nivel, 'descripcion' as char) LIKE ?
                AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[0]))."' as char) = ?
                AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[1]))."' as char) = ?
                AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[3]))."' as char) = ''
                AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[4]))."' as char) = ''
                AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[5]))."' as char) = ''
                AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[2]))."' as char) != ''
                GROUP BY ".strtolower(str_replace(' ', '_', $nombre[2])).";";
        $query = $this->db->query($sql, array("%".$nivel_buscar3."%", $nivel_buscar1, $nivel_buscar2));
        $resultado = $query->result();
        return $resultado;
    }

    /**
     * Esta funcion busca el cuarto nivel, conforme al primer, segundo y tercer nivel de la estructura de egresos
     * @param $nombre primer nivel
     * @param $nombre segundo nivel
     * @param $nombre tercer nivel
     * @return nombre y valor del cuarto nivel
     */
    function datos_nivel4($nivel1, $nivel2, $nivel3, $nombre) {
        $sql = "SELECT COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[3]))."' as char) AS ".strtolower(str_replace(' ', '_', $nombre[3])).",
                    COLUMN_GET(nivel, 'descripcion' as char) AS descripcion
                    FROM cat_niveles
                    WHERE COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[0]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[1]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[2]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[3]))."' as char) != '' GROUP BY ".strtolower(str_replace(' ', '_', $nombre[3])).";";
        $query = $this->db->query($sql, array($nivel1, $nivel2, $nivel3));
        $resultado = $query->result();
        return $resultado;
    }

    function buscar_nivel4($nivel_buscar1, $nivel_buscar2, $nivel_buscar3, $nivel_buscar4, $nombre) {
        $sql = "SELECT COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[3]))."' as char) AS ".strtolower(str_replace(' ', '_', $nombre[3])).",
                COLUMN_GET(nivel, 'descripcion' as char) AS descripcion
                FROM cat_niveles
                WHERE COLUMN_GET(nivel, 'descripcion' as char) LIKE ?
                AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[0]))."' as char) = ?
                AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[1]))."' as char) = ?
                AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[2]))."' as char) = ?
                AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[4]))."' as char) = ''
                AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[5]))."' as char) = ''
                AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[3]))."' as char) != ''
                GROUP BY ".strtolower(str_replace(' ', '_', $nombre[3])).";";
        $query = $this->db->query($sql, array("%".$nivel_buscar4."%", $nivel_buscar1, $nivel_buscar2, $nivel_buscar3));
        $resultado = $query->result();
        return $resultado;
    }

    /**
     * Esta funcion busca el quinto nivel, conforme al primer, segundo, tercer y cuarto nivel de la estructura de egresos
     * @param $nombre primer nivel
     * @param $nombre segundo nivel
     * @param $nombre tercer nivel
     * @param $nombre cuarto nivel
     * @return nombre y valor del quinto nivel
     */
    function datos_nivel5($nivel1, $nivel2, $nivel3, $nivel4, $nombre) {
        $sql = "SELECT COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[4]))."' as char) AS ".strtolower(str_replace(' ', '_', $nombre[4])).",
                    COLUMN_GET(nivel, 'descripcion' as char) AS descripcion
                    FROM cat_niveles
                    WHERE COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[0]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[1]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[2]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[3]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[4]))."' as char) != '' GROUP BY ".strtolower(str_replace(' ', '_', $nombre[4])).";";
        $query = $this->db->query($sql, array($nivel1, $nivel2, $nivel3, $nivel4));
        $resultado = $query->result();
        return $resultado;
    }

    function buscar_nivel5($nivel_buscar1, $nivel_buscar2, $nivel_buscar3, $nivel_buscar4, $nivel_buscar5, $nombre) {
        $sql = "SELECT COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[4]))."' as char) AS ".strtolower(str_replace(' ', '_', $nombre[4])).",
                COLUMN_GET(nivel, 'descripcion' as char) AS descripcion
                FROM cat_niveles
                WHERE COLUMN_GET(nivel, 'descripcion' as char) LIKE ?
                AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[0]))."' as char) = ?
                AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[1]))."' as char) = ?
                AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[2]))."' as char) = ?
                AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[3]))."' as char) = ?
                AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[4]))."' as char) = ''
                AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[5]))."' as char) != ''
                GROUP BY ".strtolower(str_replace(' ', '_', $nombre[4])).";";
        $query = $this->db->query($sql, array("%".$nivel_buscar5."%", $nivel_buscar1, $nivel_buscar2, $nivel_buscar3, $nivel_buscar4));
        $resultado = $query->result();
        return $resultado;
    }

    /**
     * Esta funcion busca el sexto nivel, conforme al primer, segundo, tercer, cuarto y quinto nivel de la estructura de egresos
     * @param $nombre primer nivel
     * @param $nombre segundo nivel
     * @param $nombre tercer nivel
     * @param $nombre cuarto nivel
     * @param $nombre quinto nivel
     * @return nombre y valor del sexto nivel
     */
    function datos_nivel6($nivel1, $nivel2, $nivel3, $nivel4, $nivel5, $nombre) {
        $sql = "SELECT COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[5]))."' as char) AS ".strtolower(str_replace(' ', '_', $nombre[5])).",
                    COLUMN_GET(nivel, 'descripcion' as char) AS descripcion
                    FROM cat_niveles
                    WHERE COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[0]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[1]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[2]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[3]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[4]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[5]))."' as char) != '' GROUP BY ".strtolower(str_replace(' ', '_', $nombre[5])).";";
        $query = $this->db->query($sql, array($nivel1, $nivel2, $nivel3, $nivel4, $nivel5));
        $resultado = $query->result();
        return $resultado;
    }

    function buscar_nivel6($nivel_buscar1, $nivel_buscar2, $nivel_buscar3, $nivel_buscar4, $nivel_buscar5, $nivel_buscar6, $nombre) {
        $sql = "SELECT COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[5]))."' as char) AS ".strtolower(str_replace(' ', '_', $nombre[5])).",
                COLUMN_GET(nivel, 'descripcion' as char) AS descripcion
                FROM cat_niveles
                WHERE COLUMN_GET(nivel, 'descripcion' as char) LIKE ?
                AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[0]))."' as char) = ?
                AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[1]))."' as char) = ?
                AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[2]))."' as char) = ?
                AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[3]))."' as char) = ?
                AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[4]))."' as char) = ?
                AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[5]))."' as char) != ''
                GROUP BY ".strtolower(str_replace(' ', '_', $nombre[5])).";";
        $query = $this->db->query($sql, array("%".$nivel_buscar6."%", $nivel_buscar1, $nivel_buscar2, $nivel_buscar3, $nivel_buscar4, $nivel_buscar5));
        $resultado = $query->result();
        return $resultado;
    }

    /**
     * Esta funcion busca datos en especifico del ultimo nivel de la estructura de egresos
     * @param $nombre primer nivel
     * @param $nombre segundo nivel
     * @param $nombre tercer nivel
     * @param $nombre cuarto nivel
     * @param $nombre quinto nivel
     * @param $nombre sexto nivel
     * @param $mes actual en minusculas y en español
     * @return dinero del mes actual, dinero comprometido del mes, dinero precomprometido del mes, saldo del mes, total anual del dinero en la partida
     */
    function datos_de_ultimo_nivel($nivel1, $nivel2, $nivel3, $nivel4, $nivel5, $nivel6, $mes, $nombre) {
        $sql = "SELECT COLUMN_GET(nivel, '".$mes."_inicial' as char) AS mes_inicial,
                    COLUMN_GET(nivel, '".$mes."_compromiso' as char) AS mes_compromiso,
				    COLUMN_GET(nivel, '".$mes."_precompromiso' as char) AS mes_precompromiso,
				    COLUMN_GET(nivel, '".$mes."_saldo' as char) AS saldo,
				    COLUMN_GET(nivel, 'precomprometido_anual' as char) AS precomprometido_anual,
				    COLUMN_GET(nivel, 'comprometido_anual' as char) AS comprometido_anual,
				    COLUMN_GET(nivel, 'total_anual' as char) AS total_anual
                    FROM cat_niveles
                    WHERE COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[0]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[1]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[2]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[3]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[4]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[5]))."' as char) = ?;";
        $query = $this->db->query($sql, array($nivel1, $nivel2, $nivel3, $nivel4, $nivel5, $nivel6));
        $resultado = $query->row();
        $resultado->mes = ucfirst($mes);
        return $resultado;
    }

    function datos_de_partida($datos, $nombre) {
        $sql = "SELECT id_niveles AS ID, COLUMN_JSON(nivel) as estructura
                    FROM cat_niveles
                    WHERE COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[0]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[1]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[2]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[3]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[4]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[5]))."' as char) = ?;";
        $query = $this->db->query($sql, array($datos["nivel1"], $datos["nivel2"], $datos["nivel3"], $datos["nivel4"], $datos["nivel5"], $datos["nivel6"]));
        $resultado = $query->result();
        return $resultado;
    }

    function get_ID_partida($datos, $nombre, $nivel) {
        $sql = "SELECT id_niveles AS ID
                    FROM cat_niveles
                    WHERE COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[0]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[1]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[2]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[3]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[4]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[5]))."' as char) = ?;";
        $query = $this->db->query($sql, array($datos["nivel1".$nivel], $datos["nivel2".$nivel], $datos["nivel3".$nivel], $datos["nivel4".$nivel], $datos["nivel5".$nivel], $datos["nivel6".$nivel]));
        $resultado = $query->row();
        return $resultado;
    }

    function get_datos_caratula($id) {
        $query = $this->db->get_where('mov_adecuaciones_egresos_caratula', array('id_adecuaciones_egresos_caratula' => $id));
        return $query->row();
    }

    function get_datos_detalle_numero($numero) {
        $query = $this->db->get_where('mov_adecuaciones_egresos_detalle', array('numero_adecuacion' => $numero));
        return $query->result();
    }

    function get_datos_partida_detalle_numero($numero) {
        $this->db->select('id_nivel')->from('mov_adecuaciones_egresos_detalle')->where('numero_adecuacion', $numero)->group_by("id_nivel");
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_datos_partida($id) {
        $sql = "SELECT COLUMN_JSON(nivel) as estructura FROM cat_niveles WHERE id_niveles = ?;";
        $query = $this->db->query($sql, array($id));
        return $query->row();
    }

    function get_saldo_partida($mes, $id) {
        $sql = "SELECT COLUMN_GET(nivel, '".$mes."_saldo' as char) as mes_saldo,
                       COLUMN_GET(nivel, '".$mes."_compromiso' as char) as mes_compromiso,
                       COLUMN_GET(nivel, 'total_anual' as char) as total_anual,
                       COLUMN_GET(nivel, 'comprometido_anual' as char) as comprometido_anual,
                       COLUMN_GET(nivel, 'precomprometido_anual' as char) as precomprometido_anual
                       FROM cat_niveles WHERE id_niveles = ?;";
        $query = $this->db->query($sql, array($id));
        return $query->row();
    }

    function borrar_adecuacion_detalle($id){
        $this->db->where('id_adecuaciones_egresos_detalle', $id);
        $query = $this->db->delete('mov_adecuaciones_egresos_detalle');
        if($query) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function get_saldos_totales($id) {
        $sql = "SELECT COLUMN_GET(nivel, 'total_anual' as char) as total_anual
                       FROM cat_niveles WHERE id_niveles = ?;";
        $query = $this->db->query($sql, array($id));
        return $query->row();
    }

    function get_datos_detalle($id) {
        $sql = "SELECT * FROM mov_adecuaciones_egresos_detalle WHERE id_adecuaciones_egresos_detalle = ?;";
        $query = $this->db->query($sql, array($id));
        return $query->row();
    }

    function get_datos_detalle_transferencia($id) {
        $sql = "SELECT * FROM mov_adecuaciones_egresos_detalle WHERE id_enlace = ?;";
        $query = $this->db->query($sql, array($id));
        return $query->result();
    }

    function actualizar_saldo_partida($mes, $saldo_mes, $total_anual, $id) {
        $query_transaccion = "UPDATE cat_niveles
                              SET nivel = COLUMN_ADD(nivel, '".$mes."_saldo', ?),
                              nivel = COLUMN_ADD(nivel, 'total_anual', ?)
                              WHERE id_niveles = ?;";
        $query = $this->db->query($query_transaccion, array($saldo_mes, $total_anual, $id));
        return $query;
    }

    function actualizar_saldo_partida_completo($mes, $saldo_mes, $precompromiso_anual, $total_anual, $id) {
        $query_transaccion = "UPDATE cat_niveles
                              SET nivel = COLUMN_ADD(nivel, '".$mes."_saldo', ?),
                              nivel = COLUMN_ADD(nivel, 'precomprometido_anual', ?),
                              nivel = COLUMN_ADD(nivel, 'total_anual', ?)
                              WHERE id_niveles = ?;";
        $query = $this->db->query($query_transaccion, array($saldo_mes, $precompromiso_anual, $total_anual, $id));
        return $query;
    }

    function insertarCaratula($datos) {
        $id = $this->tank_auth->get_user_id();

        /** En esta parte se toma el nombre del usuario */
        $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
        $resultado_query_usuario = $this->db->query($query_usuario, array($id));
        $nombre_encontrado = $resultado_query_usuario->row();
        $nombre_completo = $nombre_encontrado->nombre." ".$nombre_encontrado->apellido_paterno." ".$nombre_encontrado->apellido_materno;

        $data = array(
            "tipo" => $datos["tipo"],
            "numero" => $datos["ultimo"],
            "fecha_solicitud" => $datos["fecha_solicitud"],
            "fecha_aplicacion" => $datos["fecha_aplicar_caratula"],
            "fecha_sql" => date( 'Y-m-d H:i:s'),
            "clasificacion" => $datos["clasificacion"],
            "importe_total" => $datos["total_hidden"],
            "enfirme" => $datos["check_firme"],
            "year" => date("Y"),
            "cancelada" => 0,
            "fecha_cancelada" => 0,
            "tipo_gasto" => $datos["tipo_radio"],
            "creado_por" => $nombre_completo,
            "descripcion" => $datos["descripcion"],
            "estatus" => $datos["estatus"],
        );

        $this->db->where('numero', $datos["ultimo"]);
        $query = $this->db->update('mov_adecuaciones_egresos_caratula', $data);

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }

    }

    function insertarCaratulaArchivo($datos) {

        switch($datos["C"]) {
            case "Gasto Corriente":
                $datos["C"] = 1;
                break;
            case "Gasto de Capital":
                $datos["C"] = 2;
                break;
            case "Amortización de la cuenta y disminución de pasivos":
                $datos["C"] = 3;
                break;
        }

        $id = $this->tank_auth->get_user_id();

        /** En esta parte se toma el nombre del usuario */
        $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
        $resultado_query_usuario = $this->db->query($query_usuario, array($id));
        $nombre_encontrado = $resultado_query_usuario->row();
        $nombre_completo = $nombre_encontrado->nombre." ".$nombre_encontrado->apellido_paterno." ".$nombre_encontrado->apellido_materno;

        $data = array(
            "tipo" => $datos["tipo"],
            "numero" => $datos["ultimo"],
            "fecha_solicitud" => $datos["D"],
            "fecha_aplicacion" => $datos["E"],
            "fecha_sql" => date( 'Y-m-d H:i:s'),
            "clasificacion" => $datos["B"],
            "importe_total" => $datos["total"],
            "enfirme" => 0,
            "year" => date("Y"),
            "cancelada" => 0,
            "fecha_cancelada" => 0,
            "tipo_gasto" => $datos["C"],
            "creado_por" => $nombre_completo,
            "descripcion" => $datos["F"],
            "estatus" => "espera",
        );

        $query = $this->db->insert('mov_adecuaciones_egresos_caratula', $data);

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }

    }

    function insertar_detalle_transaccion($datos, $id_superior, $id_inferior) {
//        Se llama la funcion del modelo de egresos encargado de contar los niveles que existen
        $total_egresos = $this->egresos_model->contar_egresos_elementos();
//        Se llama a la funcion del model de egresos encargada de tomar los nombres delos niveles que existen
        $nombres_egresos = $this->egresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_egresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

        $query_insertar = " COLUMN_CREATE(";

        for($i = 0; $i < $total_egresos->conteo; $i++){
            $query_insertar .= "'".strtolower(str_replace(' ', '_', $nombre[$i]))."', ?, ";
        }

        $query_insertar .= "'id_nivel', ?), ";


        $sql = "SELECT COLUMN_GET(nivel, 'descripcion' as char) as descripcion
                FROM cat_niveles
                WHERE id_niveles = ?;";
        $query = $this->db->query($sql, array($id_superior));
        $descripcion_superior = $query->row();

        $sql = "SELECT COLUMN_GET(nivel, 'descripcion' as char) as descripcion
                FROM cat_niveles
                WHERE id_niveles = ?;";
        $query = $this->db->query($sql, array($id_inferior));
        $descripcion_inferior = $query->row();


        $sql_subsidio_superior = "SELECT titulo AS subsidio FROM cat_maestro WHERE clave = ? AND tipo = '(FF) Fuente Financ.';";
        $query = $this->db->query($sql_subsidio_superior , array($datos["nivel1_superior"]));
        $subsidio_superior = $query->row();

        $sql_subsidio_inferior = "SELECT titulo AS subsidio FROM cat_maestro WHERE clave = ? AND tipo = '(FF) Fuente Financ.';";
        $query = $this->db->query($sql_subsidio_inferior, array($datos["nivel1_inferior"]));
        $subsidio_inferior = $query->row();

        $num_aleatorio = rand();

        $total_importe = 0;
        for($i = 1; $i <= 12; $i++) {
            $nombre_mes = date('F', mktime(0, 0, 0, $i, 10));
            $total_importe = $datos[$this->utilerias->traducir_mes($nombre_mes)."_inferior"];
        }

        $query_superior = "INSERT INTO mov_adecuaciones_egresos_detalle
                          (numero_adecuacion, tipo, subsidio, year, fecha_aplicacion,
                          hora_aplicacion, fecha_sql, partida, id_nivel, estructura, capitulo, enero_movimiento,
                          febrero_movimiento, marzo_movimiento, abril_movimiento, mayo_movimiento, junio_movimiento,
                          julio_movimiento, agosto_movimiento, septiembre_movimiento, octubre_movimiento, noviembre_movimiento,
                          diciembre_movimiento, total, titulo, texto, id_enlace)
                          VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ".$query_insertar." ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ) ";

        $this->db->query($query_superior, array(
                           $datos["ultimo"], $datos["tipo"], $subsidio_superior->subsidio, $datos["year"], $datos["fecha_aplicada_detalle"],
                           $datos["hora_aplicacion"], $datos["fecha_sql"], $datos["nivel6_superior"], $id_superior, $datos["nivel1_superior"], $datos["nivel2_superior"], $datos["nivel3_superior"], $datos["nivel4_superior"], $datos["nivel5_superior"], $datos["nivel6_superior"], $id_superior, $datos["nivel4_superior"], $datos["enero_inferior"],
                           $datos["febrero_inferior"], $datos["marzo_inferior"], $datos["abril_inferior"], $datos["mayo_inferior"], $datos["junio_inferior"],
                           $datos["julio_inferior"], $datos["agosto_inferior"], $datos["septiembre_inferior"], $datos["octubre_inferior"], $datos["noviembre_inferior"],
                           $datos["diciembre_inferior"], $total_importe, $descripcion_superior->descripcion, "Reducción", $num_aleatorio,
                            ));

        $query_superior = "INSERT INTO mov_adecuaciones_egresos_detalle
                          (numero_adecuacion, tipo, subsidio, year, fecha_aplicacion,
                          hora_aplicacion, fecha_sql, partida, id_nivel, estructura, capitulo, enero_movimiento,
                          febrero_movimiento, marzo_movimiento, abril_movimiento, mayo_movimiento, junio_movimiento,
                          julio_movimiento, agosto_movimiento, septiembre_movimiento, octubre_movimiento, noviembre_movimiento,
                          diciembre_movimiento, total, titulo, texto, id_enlace)
                          VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ".$query_insertar." ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ) ";

        $resultado = $this->db->query($query_superior, array(
            $datos["ultimo"], $datos["tipo"], $subsidio_inferior->subsidio, $datos["year"], $datos["fecha_aplicada_detalle"],
            $datos["hora_aplicacion"], $datos["fecha_sql"], $datos["nivel6_superior"], $id_superior, $datos["nivel1_inferior"], $datos["nivel2_inferior"], $datos["nivel3_inferior"], $datos["nivel4_inferior"], $datos["nivel5_inferior"], $datos["nivel6_inferior"], $id_inferior, $datos["nivel4_superior"], $datos["enero_inferior"],
            $datos["febrero_inferior"], $datos["marzo_inferior"], $datos["abril_inferior"], $datos["mayo_inferior"], $datos["junio_inferior"],
            $datos["julio_inferior"], $datos["agosto_inferior"], $datos["septiembre_inferior"], $datos["octubre_inferior"], $datos["noviembre_inferior"],
            $datos["diciembre_inferior"], $total_importe, $descripcion_inferior->descripcion, "Ampliación", $num_aleatorio,
        ));

        if($resultado) {
            return TRUE;
        } else {
            return FALSE;
        }

    }

    function datos_transferenciaDetalle($sql, $numero) {
        $query = $this->db->query($sql, array($numero));
        return $query->result();
    }

    function cancelarAdecuacion($id) {
        $data = array(
            'cancelada' => 1,
            'fecha_cancelada' => date( 'Y-m-d' ),
            'estatus' => "cancelado",
        );

        $this->db->where('id_adecuaciones_egresos_caratula', $id);
        $this->db->update('mov_adecuaciones_egresos_caratula', $data);
    }

    /**
     * Esta funcion se encarga de insertar las partidas en sus correspondientes niveles, junto con el dinero que les corresponde, si es que son del ultimo nivel
     * @param $arreglo con los datos de la fila por insertar
     * @param $nombres de los niveles
     * @return devuelve true si se inserto la fila, de lo contrario devuelve false
     */
    function insertar_niveles_egresos($field, $nombres) {

        if(array_key_exists('F', $field)) {
            $sql = "SELECT descripcion FROM cat_clasificador_fuentes_financia WHERE codigo = ?;";
            $query = $this->db->query($sql, array($field['T']));
            $resultado = $query->row();

            $total = $field['H'] + $field['I'] + $field['J'] + $field['K'] + $field['L'] + $field['M'] + $field['N'] + $field['O'] + $field['P'] + $field['Q'] + $field['R'] + $field['S'] ;
            $sql = "INSERT INTO cat_niveles (id_niveles,  nivel) VALUES (NULL, COLUMN_CREATE('" . strtolower(str_replace(' ', '_', $nombres[0])) . "','" . $field['A'] . "',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[1])) . "', '" . $field['B'] . "',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[2])) . "', '" . $field['C'] . "',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[3])) . "', '" . $field['D'] . "',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[4])) . "', '" . $field['E'] . "',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[5])) . "', '" . $field['F'] . "',
                                                                    'descripcion', '" . $field['G'] . "',
                                                                    'enero_inicial', '" . $field['H'] . "',
                                                                    'febrero_inicial', '" . $field['I'] . "',
                                                                    'marzo_inicial', '" . $field['J'] . "',
                                                                    'abril_inicial', '" . $field['K'] . "',
                                                                    'mayo_inicial', '" . $field['L'] . "',
                                                                    'junio_inicial', '" . $field['M'] . "',
                                                                    'julio_inicial', '" . $field['N'] . "',
                                                                    'agosto_inicial', '" . $field['O'] . "',
                                                                    'septiembre_inicial', '" . $field['P'] . "',
                                                                    'octubre_inicial', '" . $field['Q'] . "',
                                                                    'noviembre_inicial', '" . $field['R'] . "',
                                                                    'diciembre_inicial', '" . $field['S'] . "',
                                                                    'enero_precompromiso', 0.0,
                                                                    'febrero_precompromiso', 0.0,
                                                                    'marzo_precompromiso', 0.0,
                                                                    'abril_precompromiso', 0.0,
                                                                    'mayo_precompromiso', 0.0,
                                                                    'junio_precompromiso', 0.0,
                                                                    'julio_precompromiso', 0.0,
                                                                    'agosto_precompromiso', 0.0,
                                                                    'septiembre_precompromiso', 0.0,
                                                                    'octubre_precompromiso', 0.0,
                                                                    'noviembre_precompromiso', 0.0,
                                                                    'diciembre_precompromiso', 0.0,
                                                                    'enero_compromiso', 0.0,
                                                                    'febrero_compromiso', 0.0,
                                                                    'marzo_compromiso', 0.0,
                                                                    'abril_compromiso', 0.0,
                                                                    'mayo_compromiso', 0.0,
                                                                    'junio_compromiso', 0.0,
                                                                    'julio_compromiso', 0.0,
                                                                    'agosto_compromiso', 0.0,
                                                                    'septiembre_compromiso', 0.0,
                                                                    'octubre_compromiso', 0.0,
                                                                    'noviembre_compromiso', 0.0,
                                                                    'diciembre_compromiso', 0.0,
                                                                    'enero_devengado', 0.0,
                                                                    'febrero_devengado', 0.0,
                                                                    'marzo_devengado', 0.0,
                                                                    'abril_devengado', 0.0,
                                                                    'mayo_devengado', 0.0,
                                                                    'junio_devengado', 0.0,
                                                                    'julio_devengado', 0.0,
                                                                    'agosto_devengado', 0.0,
                                                                    'septiembre_devengado', 0.0,
                                                                    'octubre_devengado', 0.0,
                                                                    'noviembre_devengado', 0.0,
                                                                    'diciembre_devengado', 0.0,
                                                                    'enero_ejercido', 0.0,
                                                                    'febrero_ejercido', 0.0,
                                                                    'marzo_ejercido', 0.0,
                                                                    'abril_ejercido', 0.0,
                                                                    'mayo_ejercido', 0.0,
                                                                    'junio_ejercido', 0.0,
                                                                    'julio_ejercido', 0.0,
                                                                    'agosto_ejercido', 0.0,
                                                                    'septiembre_ejercido', 0.0,
                                                                    'octubre_ejercido', 0.0,
                                                                    'noviembre_ejercido', 0.0,
                                                                    'diciembre_ejercido', 0.0,
                                                                    'enero_saldo', '" . $field['H'] . "',
                                                                    'febrero_saldo', '" . $field['I'] . "',
                                                                    'marzo_saldo', '" . $field['J'] . "',
                                                                    'abril_saldo', '" . $field['K'] . "',
                                                                    'mayo_saldo', '" . $field['L'] . "',
                                                                    'junio_saldo', '" . $field['M'] . "',
                                                                    'julio_saldo', '" . $field['N'] . "',
                                                                    'agosto_saldo', '" . $field['O'] . "',
                                                                    'septiembre_saldo', '" . $field['P'] . "',
                                                                    'octubre_saldo', '" . $field['Q'] . "',
                                                                    'noviembre_saldo', '" . $field['R'] . "',
                                                                    'diciembre_saldo', '" . $field['S'] . "',
                                                                    'total_anual', '".$total."',
                                                                    'precomprometido_anual', 0.0,
                                                                    'comprometido_anual', 0.0,
                                                                    'devengado_anual', 0.0,
                                                                    'ejercido_anual', 0.0,
                                                                    'clave_financiamiento', '".$resultado->descripcion."'));";
        } else {
            if (!array_key_exists('B', $field)) {
                $field['B'] = '';
            } if(!array_key_exists('C', $field)) {
                $field['C'] = '';
            } if(!array_key_exists('D', $field)) {
                $field["D"] = '';
            } if(!array_key_exists('E', $field)) {
                $field["E"] = '';
            } if(!array_key_exists('F', $field)) {
                $field["F"] = '';
            } if(!array_key_exists('G', $field)) {
                $field["G"] = '';
            } if(!array_key_exists('H', $field)) {
                $field["H"] = '';
            } if(!array_key_exists('I', $field)) {
                $field["I"] = '';
            } if(!array_key_exists('J', $field)) {
                $field["J"] = '';
            } if(!array_key_exists('K', $field)) {
                $field["K"] = '';
            } if(!array_key_exists('L', $field)) {
                $field["L"] = '';
            } if(!array_key_exists('M', $field)) {
                $field["M"] = '';
            } if(!array_key_exists('N', $field)) {
                $field["N"] = '';
            } if(!array_key_exists('O', $field)) {
                $field["O"] = '';
            } if(!array_key_exists('P', $field)) {
                $field["P"] = '';
            } if(!array_key_exists('Q', $field)) {
                $field["Q"] = '';
            } if(!array_key_exists('R', $field)) {
                $field["R"] = '';
            } if(!array_key_exists('S', $field)) {
                $field["S"] = '';
            } if(!array_key_exists('T', $field)) {
                $field["T"] = '';
            } if(!array_key_exists('U', $field)) {
                $field["U"] = '';
            } if(!array_key_exists('V', $field)) {
                $field["V"] = '';
            } if(!array_key_exists('W', $field)) {
                $field["W"] = '';
            } if(!array_key_exists('X', $field)) {
                $field["X"] = '';
            } if(!array_key_exists('Y', $field)) {
                $field["Y"] = '';
            } if(!array_key_exists('Z', $field)) {
                $field["Z"] = '';
            }

            $sql = "INSERT INTO cat_niveles (id_niveles,  nivel) VALUES (NULL, COLUMN_CREATE('" . strtolower(str_replace(' ', '_', $nombres[0])) . "','" . $field['A'] . "',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[1])) . "', '" . $field['B'] . "',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[2])) . "', '" . $field['C'] . "',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[3])) . "', '" . $field['D'] . "',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[4])) . "', '" . $field['E'] . "',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[5])) . "', '" . $field['F'] . "',
                                                                    'descripcion', '" . $field['G'] . "'));";
        }

        $query = $this->db->query($sql);

//        Seccion para saber si se isertaron las filas
        if ($query) {
            return TRUE;
        } else {
            return FALSE;
        }

    }

    function insertar_nivel($sql) {
        $this->db->trans_start();

        $query = $this->db->query($sql);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        }
        else {
            $this->db->trans_commit();
            return TRUE;
        }

    }

    function editar_nivel($sql) {
        $this->db->trans_start();

        $query = $this->db->query($sql);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        }
        else {
            $this->db->trans_commit();
            return TRUE;
        }

    }

    /**
     * Esta es la seccion de las ampliaciones
     * @param $datos
     */

    function insertarDetalle($datos) {
//        Se llama a la funcion del model de egresos encargada de tomar los nombres delos niveles que existen
        $nombres_egresos = $this->egresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_egresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

        $id_nivel = $this->get_id_titulo_estructura($datos, $nombre);

//        estructura, capitulo, mes_destino, total, titulo, texto, id_enlace
        $sql = "SELECT titulo AS subsidio FROM cat_maestro WHERE clave = ? AND tipo = '(FF) Fuente Financ.';";
        $query = $this->db->query($sql, array($datos["nivel1_inferior"]));
        $datos_subsidio = $query->row();

        $sql = "INSERT INTO mov_adecuaciones_egresos_detalle
                  (numero_adecuacion, tipo, subsidio, year, fecha_aplicacion,
                  hora_aplicacion, fecha_sql, partida, id_nivel,
                  estructura,
                  capitulo, mes_destino, total, titulo, texto, id_enlace) VALUES
                  (?, ?, ?, ?, ?,
                  ?, ?, ?, ?,
                  COLUMN_CREATE('" . strtolower(str_replace(' ', '_', $nombre[0])) . "', ?,
                  '" . strtolower(str_replace(' ', '_', $nombre[1])) . "', ?,
                  '" . strtolower(str_replace(' ', '_', $nombre[2])) . "', ?,
                  '" . strtolower(str_replace(' ', '_', $nombre[3])) . "', ?,
                  '" . strtolower(str_replace(' ', '_', $nombre[4])) . "', ?,
                  '" . strtolower(str_replace(' ', '_', $nombre[5])) . "', ?),
                  ?, ?, ?, ?, ?, ?);";
        $query = $this->db->query($sql, array(
            $datos["ultimo"], $datos["tipo"], $datos_subsidio->subsidio, date("Y"), $datos["fecha_aplicar_detalle"],
            date( 'H:i'), date( 'Y-m-d H:i:s'), $datos["nivel6_inferior"], $id_nivel->ID,
            $datos["nivel1_inferior"], $datos["nivel2_inferior"], $datos["nivel3_inferior"], $datos["nivel4_inferior"], $datos["nivel5_inferior"], $datos["nivel6_inferior"],
            $datos["nivel4_inferior"], $datos["mes_destino"], $datos["cantidad"], $id_nivel->descripcion, $datos["texto"], $datos["enlace"] ));

        if($query) {
            return TRUE;
        } else {
            return FALSE;
        }

    }

    function insertarDetalleSuperior($datos) {
//         $this->debugeo->imprimir_pre($datos);
//        Se llama a la funcion del model de egresos encargada de tomar los nombres delos niveles que existen
        $nombres_egresos = $this->egresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_egresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

        $id_nivel = $this->get_id_titulo_estructura_superior($datos, $nombre);

        // $this->debugeo->imprimir_pre($id_nivel);

//        estructura, capitulo, mes_destino, total, titulo, texto, id_enlace
        $sql = "SELECT titulo AS subsidio FROM cat_maestro WHERE clave = ? AND tipo = '(FF) Fuente Financ.';";
        $query = $this->db->query($sql, array($datos["nivel1_superior"]));
        $datos_subsidio = $query->row();

        $sql = "INSERT INTO mov_adecuaciones_egresos_detalle
                  (numero_adecuacion, tipo, subsidio, year, fecha_aplicacion,
                  hora_aplicacion, fecha_sql, partida, id_nivel,
                  estructura,
                  capitulo, mes_destino, total, titulo, texto, id_enlace) VALUES
                  (?, ?, ?, ?, ?,
                  ?, ?, ?, ?,
                  COLUMN_CREATE('" . strtolower(str_replace(' ', '_', $nombre[0])) . "', ?,
                  '" . strtolower(str_replace(' ', '_', $nombre[1])) . "', ?,
                  '" . strtolower(str_replace(' ', '_', $nombre[2])) . "', ?,
                  '" . strtolower(str_replace(' ', '_', $nombre[3])) . "', ?,
                  '" . strtolower(str_replace(' ', '_', $nombre[4])) . "', ?,
                  '" . strtolower(str_replace(' ', '_', $nombre[5])) . "', ?),
                  ?, ?, ?, ?, ?, ?);";
        //$this->debugeo->imprimir_pre($datos);
        //$this->debugeo->imprimir_pre($datos_subsidio);
        //$this->debugeo->imprimir_pre($id_nivel);
        $query = $this->db->query($sql, array(
            $datos["ultimo"], $datos["tipo"], $datos_subsidio->subsidio, date("Y"), $datos["fecha_aplicar_detalle"],
            date( 'H:i'), date( 'Y-m-d H:i:s'), $datos["nivel6_superior"], $id_nivel->ID,
            $datos["nivel1_superior"], $datos["nivel2_superior"], $datos["nivel3_superior"], $datos["nivel4_superior"], $datos["nivel5_superior"], $datos["nivel6_superior"],
            $datos["nivel4_superior"], $datos["mes_destino"], floatval($datos["cantidad"]), $id_nivel->descripcion, $datos["texto"], $datos["enlace"] ));

        if($query) {
            return TRUE;
        } else {
            return FALSE;
        }

    }

    function get_id_titulo_estructura_superior($datos, $nombre) {
        $sql = "SELECT id_niveles as ID, COLUMN_GET(nivel, 'descripcion' as char) AS descripcion
                    FROM cat_niveles
                    WHERE COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[0]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[1]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[2]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[3]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[4]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[5]))."' as char) = ?;";
        $query = $this->db->query($sql, array($datos["nivel1_superior"], $datos["nivel2_superior"], $datos["nivel3_superior"], $datos["nivel4_superior"], $datos["nivel5_superior"], $datos["nivel6_superior"]));
        $resultado = $query->row();
        return $resultado;
    }

    function get_id_titulo_estructura($datos, $nombre) {
        $sql = "SELECT id_niveles as ID, COLUMN_GET(nivel, 'descripcion' as char) AS descripcion
                    FROM cat_niveles
                    WHERE COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[0]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[1]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[2]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[3]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[4]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[5]))."' as char) = ?;";
        $query = $this->db->query($sql, array($datos["nivel1_inferior"], $datos["nivel2_inferior"], $datos["nivel3_inferior"], $datos["nivel4_inferior"], $datos["nivel5_inferior"], $datos["nivel6_inferior"]));
        $resultado = $query->row();
        return $resultado;
    }

    function get_datos_adecuaciones() {
        $sql = "SELECT * FROM mov_adecuaciones_egresos_caratula ORDER BY numero DESC;";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_datos_adecuaciones_tipo($sql) {
        $query = $this->db->query($sql);
        return $query->result();
    }

    function ultimo_adecuaciones() {
        $sql = "SELECT numero as ultimo FROM mov_adecuaciones_egresos_caratula ORDER BY numero DESC LIMIT 1;";
        $query = $this->db->query($sql);
        $row = $query->row();
        if(!$row) {
            $row = 0;
        }
        return $row;
    }

    function apartarAdecuacion($ultimo, $tipo) {
        $id = $this->tank_auth->get_user_id();

        /** En esta parte se toma el nombre del usuario */
        $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
        $resultado_query_usuario = $this->db->query($query_usuario, array($id));
        $nombre_encontrado = $resultado_query_usuario->row();
        $nombre_completo = $nombre_encontrado->nombre." ".$nombre_encontrado->apellido_paterno." ".$nombre_encontrado->apellido_materno;

        $data = array(
            'tipo' => $tipo,
            'numero' => $ultimo,
            'estatus' => "espera",
            'creado_por' => $nombre_completo,
            'fecha_solicitud' => date("Y-m-d"),
        );

        $query = $this->db->insert('mov_adecuaciones_egresos_caratula', $data);

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

}