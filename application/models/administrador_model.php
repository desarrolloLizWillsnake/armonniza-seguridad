<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Administrador_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    /*Datos Empresa*/
    function get_datos_empresa($query) {
        $query = $this->db->query($query);
        $result = $query->row();
        return $result;
    }

    function insertar_datos_empresa($datos) {
//        Este arreglo son los datos de la empresa que van a ser actualizados
        $data = array(
            "nombre" => $datos["nombre_empresa"],
            'rfc' =>  $datos["rfc"],
            'calle' => $datos["calle"],
            'no_ext' => $datos["no_ext"],
            'no_int' => $datos["no_int"],
            'colonia' => $datos["colonia"],
            'cp' => $datos["cp"],
            'delegacion' => $datos["delegacion"],
            'ciudad' => $datos["ciudad"],
        );

//        Esta función es la que se encarga de actualizar los datos de la empresa
        $this->db->where('id_datos_empresa', 1);
        $query = $this->db->update('datos_empresa', $data);
//        Si se pudieron insertar los datos, se devuelve una variable booleana como verdadera
        if($query) {
            return TRUE;
        }
//        De lo contrario devuelve una variable booleana falsa
        else {
            return FALSE;
        }

    }

    function insertar_datos_estructura($datos) {
//        Este arreglo son los datos de la empresa que van a ser actualizados
        $data = array(
            "clasificacion_admin" => $datos["clasificacion_admin"],
            'unidad_programatica' =>  $datos["unidad_programatica"],
            'eje' => $datos["eje"],
            'programa_sectorial' => $datos["programa_sectorial"],
            'programa_presupuestario' => $datos["programa_presupuestario"],
            'subprograma' => $datos["subprograma"],
            'proyecto' => $datos["proyecto"],
            'anio' => $datos["anio"],
            'estado' => $datos["estado"],
            'tipo_grupo' => $datos["tipo_grupo"],
            'tipo_genero' => $datos["tipo_genero"],
            'eje_plan' => $datos["eje_plan"],
        );

//        Esta función es la que se encarga de actualizar los datos de la empresa
        $this->db->where('id_clave', 1);
        $query = $this->db->update('datos_estructura', $data);
//        Si se pudieron insertar los datos, se devuelve una variable booleana como verdadera
        if($query) {
            return TRUE;
        }
//        De lo contrario devuelve una variable booleana falsa
        else {
            return FALSE;
        }

    }

    function subir($titulo,$imagen)
    {
        $data = array(
            'ruta_logotipo' => $titulo,
            'nombre_imagen' => $imagen
        );

        $this->db->where('id_datos_empresa', 1);
        $query = $this->db->update('datos_empresa', $data);
//        Si se pudieron insertar los datos, se devuelve una variable booleana como verdadera
        if($query) {
            return TRUE;
        }
//        De lo contrario devuelve una variable booleana falsa
        else {
            return FALSE;
        }


    }

    /*Usuarios*/
    function datos_usuariosCaratula()
    {
        $sql = "SELECT u1.*, u2.id, u2.username, u2.email, u2.activated, u2.banned, u2.created
                FROM datos_usuario u1
                INNER JOIN users u2 ON u1.id_usuario = u2.id;";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    function get_datos_usuarios($usuario, $query) {
        $query = $this->db->query($query, array($usuario));
        $result = $query->row();
        return $result;
    }

    function get_datos_permisos($usuario, $query) {
        $query = $this->db->query($query, array($usuario));
        return $query->result();
    }

    function insertar_caratula_usuario($datos) {
//        Este arreglo son los datos que van a ser insertados en la caratula del usuario
        $data = array(
            'username' =>  $datos["nombre_usuario"],
            'activated' => $datos["activo"],
            'banned' => $datos["bloqueado"],
            'email' => $datos["correo"],
        );

//        Esta función es la que se encarga de actualizar el devengado
        $this->db->where('id', $datos["no_usuario"]);
        $query = $this->db->update('users', $data);

        $data = array(
            'nombre' =>  $datos["nombre"],
            'apellido_paterno' => $datos["apellido_paterno"],
            'apellido_materno' => $datos["apellido_materno"],
            'puesto' => $datos["puesto"],
        );

//        Esta función es la que se encarga de actualizar el devengado
        $this->db->where('id_usuario', $datos["no_usuario"]);
        $query = $this->db->update('datos_usuario', $data);
//        Si se pudieron insertar los datos, se devuelve una variable booleana como verdadera
        if($query) {
            return TRUE;
        }
//        De lo contrario devuelve una variable booleana falsa
        else {
            return FALSE;
        }

    }

    function insertar_privilegios_usuario($datos, $usuario) {

        foreach($datos as $key => $value) {
//            Este arreglo son los datos que van a ser insertados en la caratula del usuario
            $data = array(
                'activo' =>  $value,
            );

//            Esta función es la que se encarga de actualizar los privilegios
            $this->db->where('id_usuario', $usuario);
            $this->db->where('modulo', $key);
            $query = $this->db->update('permisos', $data);

            if(!$query) {
                break;
            }
        }

        if($query){
            return TRUE;
        }
        else {
            return FALSE;
        }

    }

    function actualizacion_estado_usuario($usuario, $query) {
        $query = $this->db->query($query, array($usuario,
        ));
        if($query){
            return TRUE;
        }
        else {
            return FALSE;
        }

    }

    function restaurarContrasenaUsuario($new_password, $usuario) {
        $sql = "UPDATE users SET password = ? WHERE id = ?;";
        $query = $this->db->query($sql, array($new_password, $usuario,));
        if($query){
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function actualizar_grupo_usuario($usuario, $grupo) {
        $data = array(
            'id_grupo' => $grupo,
        );

        $this->db->where('id_usuario', $usuario);
        $this->db->update('grupos_usuarios', $data);
    }

    /*Autorizaciones*/

    function insertar_datos_autorizador($datos) {
//        Este arreglo son los datos que van a ser insertados en la caratula del usuario
        $data = array(
            'nombre' =>  $datos["nombre_autorizador"],
            'puesto' => $datos["puesto_autorizador"],
        );

//        Esta función es la que se encarga de actualizar el devengado
        $this->db->where('id_persona', $datos["id_autorizador"]);
        $query = $this->db->update('cat_autorizadores_firmas', $data);

//        Si se pudieron insertar los datos, se devuelve una variable booleana como verdadera
        if($query) {
            return TRUE;
        }
//        De lo contrario devuelve una variable booleana falsa
        else {
            return FALSE;
        }

    }

    function insertar_datos_autorizador_contabilidad($datos) {
//        Este arreglo son los datos que van a ser insertados en la caratula del usuario
        $data = array(
            'nombre' =>  $datos["nombre_autorizador"],
            'puesto' => $datos["puesto_autorizador"],
        );

//        Esta función es la que se encarga de actualizar el devengado
        $this->db->where('id_persona', $datos["id_autorizador"]);
        $query = $this->db->update('cat_autorizadores_firmas', $data);

//        Si se pudieron insertar los datos, se devuelve una variable booleana como verdadera
        if($query) {
            return TRUE;
        }
//        De lo contrario devuelve una variable booleana falsa
        else {
            return FALSE;
        }

    }

    function insertar_datos_vobo_contabilidad($datos) {
//        Este arreglo son los datos que van a ser insertados en la caratula del usuario
        $data = array(
            'nombre' =>  $datos["nombre_autorizador"],
            'puesto' => $datos["puesto_autorizador"],
        );

//        Esta función es la que se encarga de actualizar el devengado
        $this->db->where('id_persona', $datos["id_autorizador"]);
        $query = $this->db->update('cat_autorizadores_firmas', $data);

//        Si se pudieron insertar los datos, se devuelve una variable booleana como verdadera
        if($query) {
            return TRUE;
        }
//        De lo contrario devuelve una variable booleana falsa
        else {
            return FALSE;
        }

    }



}