<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Nomina_model extends CI_Model {

    function __construct() {
    // Call the Model constructor
        parent::__construct();
    }

    function get_datos_indice_empleados() {
    	$this->db->select('*')
    				->from('cat_usuarios_nomina');
    	$query = $this->db->get();
    	return $query->result_array();
    }

    function get_datos_filtros_personal($datos){
        $sql = "SELECT * FROM cat_usuarios_nomina
                WHERE a_paterno LIKE ?
                AND a_materno LIKE ?
                AND nombre LIKE ?
                AND rfc LIKE ?
                AND curp LIKE ?
                AND puesto LIKE ?
                AND fuente_financiamiento LIKE ?
                AND tipo_contrato LIKE ?
                AND fech_ing_gob >= ?
                AND fech_ing_gob <= ?;";
        $query = $this->db->query($sql, array("%".$datos["a_paterno"]."%", "%".$datos["a_materno"]."%", "%".$datos["nombre"]."%", "%".$datos["rfc"]."%",
            "%".$datos["curp"]."%", "%".$datos["puesto"]."%", "%".$datos["fuente_financiamiento"]."%", "%".$datos["tipo_contrato"]."%", $datos["fecha_inicial"], $datos["fecha_final"]));
        return $query->result();
    }

}