<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reportes_model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    function get_datos_beneficiarios() {
        $sql = "SELECT * FROM cat_beneficiarios;";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    function get_datos_precompromisos() {
        $sql = "SELECT * FROM mov_precompromiso_caratula WHERE enfirme = 1 AND firma1 = 1 AND firma2 = 1 AND firma3 = 1 AND cancelada = 0;";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    function get_datos_compromisos() {
        $sql = "SELECT * FROM mov_compromiso_caratula WHERE enfirme = 1 AND firma1 = 1 AND firma2 = 1 AND firma3 = 1 AND cancelada = 0;";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    function get_datos_precompromisos_viaticos_nacionales($datos) {
        if($datos["opciones"] == "gral") {
            $sql = "SELECT p1.numero_pre, p1.tipo_requisicion, p1.no_empleado, p1.nombre_completo, p1.puesto, p1.fecha_autoriza, p1.total, p1.enfirme,
			        p2.num_precompromiso, p2.numero_compromiso,
			        p3.id_contrarecibo_caratula, p3.numero_compromiso, p3.fecha_pago,
                    p4.numero_contrarecibo, p4.subtotal, p4.iva
                    FROM mov_precompromiso_caratula p1
                    LEFT JOIN mov_compromiso_caratula p2 ON p1.numero_pre = p2.num_precompromiso
                    LEFT JOIN mov_contrarecibo_caratula p3 ON p3.numero_compromiso = p2.numero_compromiso
                    LEFT JOIN mov_contrarecibo_detalle p4 ON p4.numero_contrarecibo = p3.id_contrarecibo_caratula
                    WHERE p1.tipo_requisicion = 'Viáticos Nacionales'
                    AND p1.enfirme = 1
                    AND p1.fecha_autoriza >= ?
                    AND p1.fecha_autoriza <= ?;";

            $query = $this->db->query($sql, array($datos["fecha_inicial"], $datos["fecha_final"]));
            return $query->result();
        }else{
            $sql = "SELECT p1.numero_pre, p1.tipo_requisicion, p1.no_empleado, p1.nombre_completo, p1.puesto, p1.fecha_autoriza, p1.total, p1.enfirme,
			        p2.num_precompromiso, p2.numero_compromiso,
			        p3.id_contrarecibo_caratula, p3.numero_compromiso, p3.fecha_pago,
                    p4.numero_contrarecibo, p4.subtotal, p4.iva
                    FROM mov_precompromiso_caratula p1
                    LEFT JOIN mov_compromiso_caratula p2 ON p1.numero_pre = p2.num_precompromiso
                    LEFT JOIN mov_contrarecibo_caratula p3 ON p3.numero_compromiso = p2.numero_compromiso
                    LEFT JOIN mov_contrarecibo_detalle p4 ON p4.numero_contrarecibo = p3.id_contrarecibo_caratula
                    WHERE p1.tipo_requisicion = 'Viáticos Nacionales'
                    AND p1.enfirme = 1
                    AND p1.fecha_autoriza >= ?
                    AND p1.fecha_autoriza <= ?
                    AND p1.nombre_completo = ?;";

            $query = $this->db->query($sql, array($datos["fecha_inicial"], $datos["fecha_final"] , $datos["id_beneficiario"]));
            return $query->result();
        }
    }

    function get_datos_compromisos_viaticos_nacionales($datos) {
        if($datos["opciones"] == "gral") {
            $sql = "SELECT cp.numero_compromiso AS compromiso, cp.tipo_compromiso, cp.no_empleado, cp.nombre_completo, cp.fecha_autoriza, cp.total,
                    cc.id_contrarecibo_caratula, cc.numero_compromiso, cc.fecha_pago,
                    cd.numero_contrarecibo, cd.subtotal, cd.iva
                    FROM mov_compromiso_caratula cp
                    LEFT JOIN mov_contrarecibo_caratula cc ON cc.numero_compromiso = cp.numero_compromiso
                    LEFT JOIN mov_contrarecibo_detalle cd ON cd.numero_contrarecibo = cc.id_contrarecibo_caratula
                    WHERE cp.tipo_compromiso = 'Viáticos Nacionales'
                    AND cp.enfirme = 1
                    AND cp.num_precompromiso = 0
                    AND cp.fecha_autoriza >= ?
                    AND cp.fecha_autoriza <= ?;";

            $query = $this->db->query($sql, array($datos["fecha_inicial"], $datos["fecha_final"]));
            return $query->result();
        }else{
            $sql = "SELECT cp.numero_compromiso AS compromiso, cp.tipo_compromiso, cp.no_empleado, cp.nombre_completo, cp.fecha_autoriza, cp.total,
                    cc.id_contrarecibo_caratula, cc.numero_compromiso, cc.fecha_pago,
                    cd.numero_contrarecibo, cd.subtotal, cd.iva
                    FROM mov_compromiso_caratula cp
                    LEFT JOIN mov_contrarecibo_caratula cc ON cc.numero_compromiso = cp.numero_compromiso
                    LEFT JOIN mov_contrarecibo_detalle cd ON cd.numero_contrarecibo = cc.id_contrarecibo_caratula
                    WHERE cp.tipo_compromiso = 'Viáticos Nacionales'
                    AND cp.enfirme = 1
                    AND cp.num_precompromiso = 0
                    AND cp.fecha_autoriza >= ?
                    AND cp.fecha_autoriza <= ?
                     AND cp.nombre_completo = ?;";

            $query = $this->db->query($sql, array($datos["fecha_inicial"], $datos["fecha_final"] , $datos["id_beneficiario"]));
            return $query->result();
        }
    }

    function get_datos_precompromisos_viaticos_internacionales($datos) {
        if($datos["opciones"] == "gral") {
            $sql = "SELECT p1.numero_pre, p1.tipo_requisicion, p1.no_empleado, p1.nombre_completo, p1.puesto, p1.fecha_autoriza, p1.total, p1.enfirme,
			        p2.num_precompromiso, p2.numero_compromiso,
			        p3.id_contrarecibo_caratula, p3.numero_compromiso, p3.fecha_pago,
                    p4.numero_contrarecibo, p4.subtotal, p4.iva
                    FROM mov_precompromiso_caratula p1
                    LEFT JOIN mov_compromiso_caratula p2 ON p1.numero_pre = p2.num_precompromiso
                    LEFT JOIN mov_contrarecibo_caratula p3 ON p3.numero_compromiso = p2.numero_compromiso
                    LEFT JOIN mov_contrarecibo_detalle p4 ON p4.numero_contrarecibo = p3.id_contrarecibo_caratula
                    WHERE p1.tipo_requisicion = 'Viáticos Internacionales'
                    AND p1.enfirme = 1
                    AND  p1.fecha_autoriza >= ?
                    AND p1.fecha_autoriza <= ?;";

            $query = $this->db->query($sql, array($datos["fecha_inicial"], $datos["fecha_final"]));
            return $query->result();
        }else{
            $sql = "SELECT p1.numero_pre, p1.tipo_requisicion, p1.no_empleado, p1.nombre_completo, p1.puesto, p1.fecha_autoriza, p1.total, p1.enfirme,
			        p2.num_precompromiso, p2.numero_compromiso,
			        p3.id_contrarecibo_caratula, p3.numero_compromiso, p3.fecha_pago,
                    p4.numero_contrarecibo, p4.subtotal, p4.iva
                    FROM mov_precompromiso_caratula p1
                    LEFT JOIN mov_compromiso_caratula p2 ON p1.numero_pre = p2.num_precompromiso
                    LEFT JOIN mov_contrarecibo_caratula p3 ON p3.numero_compromiso = p2.numero_compromiso
                    LEFT JOIN mov_contrarecibo_detalle p4 ON p4.numero_contrarecibo = p3.id_contrarecibo_caratula
                    WHERE p1.tipo_requisicion = 'Viáticos Internacionales'
                    AND p1.enfirme = 1
                    AND p1.fecha_autoriza >= ?
                    AND p1.fecha_autoriza <= ?
                    AND p1.nombre_completo = ?;";

            $query = $this->db->query($sql, array($datos["fecha_inicial"], $datos["fecha_final"] , $datos["id_beneficiario"]));
            return $query->result();
        }
    }

    function get_datos_compromisos_viaticos_internacionales($datos) {
        if($datos["opciones"] == "gral") {
            $sql = "SELECT cp.numero_compromiso AS compromiso, cp.tipo_compromiso, cp.no_empleado, cp.nombre_completo, cp.fecha_autoriza, cp.total,
                    cc.id_contrarecibo_caratula, cc.numero_compromiso, cc.fecha_pago,
                    cd.numero_contrarecibo, cd.subtotal, cd.iva
                    FROM mov_compromiso_caratula cp
                    LEFT JOIN mov_contrarecibo_caratula cc ON cc.numero_compromiso = cp.numero_compromiso
                    LEFT JOIN mov_contrarecibo_detalle cd ON cd.numero_contrarecibo = cc.id_contrarecibo_caratula
                    WHERE cp.tipo_compromiso = 'Viáticos Internacionales'
                    AND cp.enfirme = 1
                    AND cp.num_precompromiso = 0
                    AND cp.fecha_autoriza >= ?
                    AND cp.fecha_autoriza <= ?;";

            $query = $this->db->query($sql, array($datos["fecha_inicial"], $datos["fecha_final"]));
            return $query->result();
        }else{
            $sql = "SELECT cp.numero_compromiso AS compromiso, cp.tipo_compromiso, cp.no_empleado, cp.nombre_completo, cp.fecha_autoriza, cp.total,
                    cc.id_contrarecibo_caratula, cc.numero_compromiso, cc.fecha_pago,
                    cd.numero_contrarecibo, cd.subtotal, cd.iva
                    FROM mov_compromiso_caratula cp
                    LEFT JOIN mov_contrarecibo_caratula cc ON cc.numero_compromiso = cp.numero_compromiso
                    LEFT JOIN mov_contrarecibo_detalle cd ON cd.numero_contrarecibo = cc.id_contrarecibo_caratula
                    WHERE cp.tipo_compromiso = 'Viáticos Internacionales'
                    AND cp.enfirme = 1
                    AND cp.num_precompromiso = 0
                    AND cp.fecha_autoriza >= ?
                    AND cp.fecha_autoriza <= ?
                    AND cp.nombre_completo = ?;";
            $query = $this->db->query($sql, array($datos["fecha_inicial"], $datos["fecha_final"] , $datos["id_beneficiario"]));
            return $query->result();
        }
    }

    function get_datos_precompromisos_gastos_comprobar($datos) {
        if($datos["opciones"] == "gral") {
            $sql = "SELECT p1.numero_pre, p1.tipo_requisicion, p1.no_empleado, p1.nombre_completo, p1.puesto, p1.area, p1.fecha_autoriza, p1.total, p1.enfirme,
			        p2.num_precompromiso, p2.numero_compromiso,
			        p3.id_contrarecibo_caratula, p3.numero_compromiso, p3.fecha_pago,
                    p4.numero_contrarecibo, p4.subtotal, p4.iva
                    FROM mov_precompromiso_caratula p1
                    LEFT JOIN mov_compromiso_caratula p2 ON p1.numero_pre = p2.num_precompromiso
                    LEFT JOIN mov_contrarecibo_caratula p3 ON p3.numero_compromiso = p2.numero_compromiso
                    LEFT JOIN mov_contrarecibo_detalle p4 ON p4.numero_contrarecibo = p3.id_contrarecibo_caratula
                    WHERE p1.tipo_requisicion = 'Gastos a Comprobar'
                    AND p1.enfirme = 1
                    AND p1.fecha_autoriza >= ?
                    AND p1.fecha_autoriza <= ?;";

            $query = $this->db->query($sql, array($datos["fecha_inicial"], $datos["fecha_final"]));
            return $query->result();
        }else{
            $sql = "SELECT p1.numero_pre, p1.tipo_requisicion, p1.no_empleado, p1.nombre_completo, p1.puesto, p1.area, p1.fecha_autoriza, p1.total, p1.enfirme,
			        p2.num_precompromiso, p2.numero_compromiso,
			        p3.id_contrarecibo_caratula, p3.numero_compromiso, p3.fecha_pago,
                    p4.numero_contrarecibo, p4.subtotal, p4.iva
                    FROM mov_precompromiso_caratula p1
                    LEFT JOIN mov_compromiso_caratula p2 ON p1.numero_pre = p2.num_precompromiso
                    LEFT JOIN mov_contrarecibo_caratula p3 ON p3.numero_compromiso = p2.numero_compromiso
                    LEFT JOIN mov_contrarecibo_detalle p4 ON p4.numero_contrarecibo = p3.id_contrarecibo_caratula
                    WHERE p1.tipo_requisicion = 'Gastos a Comprobar'
                    AND p1.enfirme = 1
                    AND p1.fecha_autoriza >= ?
                    AND p1.fecha_autoriza <= ?
                    AND p1.nombre_completo = ?;";

            $query = $this->db->query($sql, array($datos["fecha_inicial"], $datos["fecha_final"] , $datos["id_beneficiario"]));
            return $query->result();
        }
    }

    function get_datos_compromisos_gastos_comprobar($datos) {
        if($datos["opciones"] == "gral") {
            $sql = "SELECT cp.numero_compromiso, cp.tipo_compromiso, cp.no_empleado, cp.nombre_completo, cp.puesto, cp.area, cp.fecha_autoriza, cp.total,
                    cc.id_contrarecibo_caratula, cc.numero_compromiso, cc.fecha_pago,
                    cd.numero_contrarecibo, cd.subtotal, cd.iva
                    FROM mov_compromiso_caratula cp
                    LEFT JOIN mov_contrarecibo_caratula cc ON cc.numero_compromiso = cp.numero_compromiso
                    LEFT JOIN mov_contrarecibo_detalle cd ON cd.numero_contrarecibo = cc.id_contrarecibo_caratula
                    WHERE cp.tipo_compromiso = 'Gastos a Comprobar'
                    AND cp.enfirme = 1
                    AND cp.num_precompromiso = 0
                    AND cp.fecha_autoriza >= ?
                    AND cp.fecha_autoriza <= ?;";

            $query = $this->db->query($sql, array($datos["fecha_inicial"], $datos["fecha_final"]));
            return $query->result();
        }else{
            $sql = "SELECT cp.numero_compromiso, cp.tipo_compromiso, cp.no_empleado, cp.nombre_completo, cp.puesto, cp.area, cp.fecha_autoriza, cp.total,
                    cc.id_contrarecibo_caratula, cc.numero_compromiso, cc.fecha_pago,
                    cd.numero_contrarecibo, cd.subtotal, cd.iva
                    FROM mov_compromiso_caratula cp
                    LEFT JOIN mov_contrarecibo_caratula cc ON cc.numero_compromiso = cp.numero_compromiso
                    LEFT JOIN mov_contrarecibo_detalle cd ON cd.numero_contrarecibo = cc.id_contrarecibo_caratula
                    WHERE cp.tipo_compromiso = 'Gastos a Comprobar'
                    AND cp.enfirme = 1
                    AND cp.num_precompromiso = 0
                    AND cp.fecha_autoriza >= ?
                    AND cp.fecha_autoriza <= ?
                    AND cp.nombre_completo = ?;";
            $query = $this->db->query($sql, array($datos["fecha_inicial"], $datos["fecha_final"] , $datos["id_beneficiario"]));
            return $query->result();
        }
    }

    function get_datos_precompromisos_fondo_revolvente($datos) {
        if($datos["opciones"] == "gral") {
            $sql = "SELECT p1.numero_pre, p1.tipo_requisicion, p1.no_empleado, p1.nombre_completo, p1.puesto, p1.area, p1.fecha_autoriza, p1.total, p1.enfirme,
			        p2.num_precompromiso, p2.numero_compromiso,
			        p3.id_contrarecibo_caratula, p3.numero_compromiso, p3.fecha_pago,
                    p4.numero_contrarecibo, p4.subtotal, p4.iva
                    FROM mov_precompromiso_caratula p1
                    LEFT JOIN mov_compromiso_caratula p2 ON p1.numero_pre = p2.num_precompromiso
                    LEFT JOIN mov_contrarecibo_caratula p3 ON p3.numero_compromiso = p2.numero_compromiso
                    LEFT JOIN mov_contrarecibo_detalle p4 ON p4.numero_contrarecibo = p3.id_contrarecibo_caratula
                    WHERE p1.tipo_requisicion = 'Fondo Revolvente'
                    AND p1.enfirme = 1
                    AND p1.fecha_autoriza >= ?
                    AND p1.fecha_autoriza <= ?;";

            $query = $this->db->query($sql, array($datos["fecha_inicial"], $datos["fecha_final"]));
            return $query->result();
        }else{
            $sql = "SELECT p1.numero_pre, p1.tipo_requisicion, p1.no_empleado, p1.nombre_completo, p1.puesto, p1.area, p1.fecha_autoriza, p1.total, p1.enfirme,
			        p2.num_precompromiso, p2.numero_compromiso,
			        p3.id_contrarecibo_caratula, p3.numero_compromiso, p3.fecha_pago,
                    p4.numero_contrarecibo, p4.subtotal, p4.iva
                    FROM mov_precompromiso_caratula p1
                    LEFT JOIN mov_compromiso_caratula p2 ON p1.numero_pre = p2.num_precompromiso
                    LEFT JOIN mov_contrarecibo_caratula p3 ON p3.numero_compromiso = p2.numero_compromiso
                    LEFT JOIN mov_contrarecibo_detalle p4 ON p4.numero_contrarecibo = p3.id_contrarecibo_caratula
                    WHERE p1.tipo_requisicion = 'Fondo Revolvente'
                    AND p1.enfirme = 1
                    AND p1.fecha_autoriza >= ?
                    AND p1.fecha_autoriza <= ?
                    AND p1.nombre_completo = ?;";

            $query = $this->db->query($sql, array($datos["fecha_inicial"], $datos["fecha_final"] , $datos["id_beneficiario"]));
            return $query->result();
        }
    }

    function get_datos_compromisos_fondo_revolvente($datos) {
        if($datos["opciones"] == "gral") {
            $sql = "SELECT cp.numero_compromiso AS compromiso, cp.tipo_compromiso, cp.no_empleado, cp.nombre_completo, cp.puesto, cp.area, cp.fecha_autoriza, cp.total,
                    cc.id_contrarecibo_caratula, cc.numero_compromiso, cc.fecha_pago,
                    cd.numero_contrarecibo, cd.subtotal, cd.iva
                    FROM mov_compromiso_caratula cp
                    LEFT JOIN mov_contrarecibo_caratula cc ON cc.numero_compromiso = cp.numero_compromiso
                    LEFT JOIN mov_contrarecibo_detalle cd ON cd.numero_contrarecibo = cc.id_contrarecibo_caratula
                    WHERE cp.tipo_compromiso = 'Fondo Revolvente'
                    AND cp.enfirme = 1
                    AND cp.num_precompromiso = 0
                    AND cp.fecha_autoriza >= ?
                    AND cp.fecha_autoriza <= ?;";

            $query = $this->db->query($sql, array($datos["fecha_inicial"], $datos["fecha_final"]));
            return $query->result();
        }else{
            $sql = "SELECT cp.numero_compromiso AS compromiso, cp.tipo_compromiso, cp.no_empleado, cp.nombre_completo, cp.puesto, cp.area, cp.fecha_autoriza, cp.total,
                    cc.id_contrarecibo_caratula, cc.numero_compromiso, cc.fecha_pago,
                    cd.numero_contrarecibo, cd.subtotal, cd.iva
                    FROM mov_compromiso_caratula cp
                    LEFT JOIN mov_contrarecibo_caratula cc ON cc.numero_compromiso = cp.numero_compromiso
                    LEFT JOIN mov_contrarecibo_detalle cd ON cd.numero_contrarecibo = cc.id_contrarecibo_caratula
                    WHERE cp.tipo_compromiso = 'Fondo Revolvente'
                    AND cp.enfirme = 1
                    AND cp.num_precompromiso = 0
                    AND cp.fecha_autoriza >= ?
                    AND cp.fecha_autoriza <= ?
                    AND cp.nombre_completo = ?;";
            $query = $this->db->query($sql, array($datos["fecha_inicial"], $datos["fecha_final"] , $datos["id_beneficiario"]));
            return $query->result();
        }
    }

    function get_datos_contrarecibo_movimientos($contrarecibo) {
        $sql = "SELECT numero_contrarecibo, subtotal, iva, total FROM mov_contrarecibo_detalle WHERE numero_contrarecibo = ?;";
        $query = $this->db->query($sql, array($contrarecibo));
        $result = $query->result();
        return $result;
    }

    function get_datos_proveedores_movimientos($datos){
        if($datos["opciones"] == "gral") {
            $sql = "SELECT m1.proveedor, m2.clave_prov, m1.movimiento, m1.cuenta, m1.fecha_pago, m1.importe, m1.contrarecibo
                    FROM  mov_bancos_movimientos m1
                    RIGHT JOIN cat_proveedores m2 ON m1.proveedor = m2.nombre
                    WHERE enfirme = 1
                    AND fecha_pago >= ?
                    AND fecha_pago <= ?;";

            $query = $this->db->query($sql, array($datos["fecha_inicial"], $datos["fecha_final"]));
            return $query->result();
        }else{
            $sql = "SELECT m1.proveedor, m2.clave_prov, m1.movimiento, m1.cuenta, m1.fecha_pago, m1.importe, m1.contrarecibo
                    FROM  mov_bancos_movimientos m1
                    RIGHT JOIN cat_proveedores m2 ON m1.proveedor = m2.nombre
                    WHERE enfirme = 1
                    AND fecha_pago >= ?
                    AND fecha_pago <= ?
                    AND m2.clave_prov = ?";
            $query = $this->db->query($sql, array($datos["fecha_inicial"], $datos["fecha_final"] , $datos["id_proveedor"]));
            return $query->result();
        }
        $query = "";
        $query = $this->db->query($query);
        $result = $query->result();
        return $result;
    }

    function get_ampliacion_reduccion_egresos_diot($datos){
        if($datos["opciones"] == "gral") {
            $sql = "SELECT proveedor, clave_prov, RFC, pais, movimiento, p1.cuenta ,fecha_movimiento, fecha_pago, importe, enfirme
                    FROM mov_bancos_movimientos p1
                    INNER JOIN cat_proveedores p2 ON p1.proveedor = p2.nombre
                    WHERE enfirme = 1
                    AND fecha_movimiento >= ?
                    AND fecha_movimiento <= ?;";

            $query = $this->db->query($sql, array($datos["fecha_inicial"], $datos["fecha_final"]));
            return $query->result();
        }else{
            $sql = "SELECT proveedor, clave_prov, RFC, pais, movimiento, p1.cuenta ,fecha_movimiento, fecha_pago, importe
                  FROM mov_bancos_movimientos p1
                  INNER JOIN cat_proveedores p2 ON p1.proveedor = p2.nombre
                  WHERE p1.enfirme = 1
                  AND fecha_movimiento >= ?
                  AND fecha_movimiento <= ?
                  AND clave_prov = ?;";

            $query = $this->db->query($sql, array($datos["fecha_inicial"], $datos["fecha_final"] , $datos["id_proveedor"]));
            return $query->result();
        }




        $query = "";
        $query = $this->db->query($query);
        $result = $query->result();
        return $result;
    }

    function get_datos_proveedores_movimientos_diot_excel(){
        $sql = "SELECT proveedor, clave_prov, pais, movimiento, p1.cuenta ,fecha_movimiento, fecha_pago, importe, enfirme
                    FROM mov_bancos_movimientos p1
                    INNER JOIN cat_proveedores p2 ON p1.proveedor = p2.nombre
                    WHERE enfirme = 1;";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    function get_datos_seguimiento_procesos_excel($datos){
        if($datos["opciones"] == "gral") {
            $sql = "SELECT c1.numero_pre, c1.tipo_requisicion, c1.total AS total_precompromiso, c1.fecha_emision AS fecha_precompromiso, c1.total AS total_precompromiso, c1.proveedor AS proveedor_precompromiso, c1.nombre_completo AS nombre_precompromiso, c1.estatus AS estatus_precompromiso,
                         c2.numero_compromiso, c2.num_precompromiso, c2.tipo_compromiso, c2.fecha_emision AS fecha_compromiso, c2.total AS total_compromiso, c2.nombre_proveedor AS proveedor_compromiso, c2.nombre_completo AS nombre_compromiso, c2.estatus AS estatus_compromiso,
                         COLUMN_JSON(c3.nivel) AS niveles, c3.importe AS importe_compromiso_detalle,
                         c4.id_contrarecibo_caratula, c4.poliza AS contrarecibo_poliza, c4.fecha_pago AS fecha_contrarecibo, c4.importe AS importe_contrarecibo, c4.estatus AS estatus_contrarecibo,
                         c5.movimiento, c5.contrarecibo, c5.fecha_pago AS fecha_movimiento, c5.importe AS importe_movimiento, c5.enfirme AS enfirme_movimiento
                FROM mov_precompromiso_caratula c1
                LEFT JOIN mov_compromiso_caratula c2 ON c1.numero_pre  = c2.num_precompromiso
                LEFT JOIN mov_compromiso_detalle c3 ON c2.numero_compromiso = c3.numero_compromiso
                LEFT JOIN mov_contrarecibo_caratula c4 ON c3.numero_compromiso = c4.numero_compromiso
                LEFT JOIN mov_bancos_movimientos c5 ON c4.id_contrarecibo_caratula = c5.contrarecibo
                WHERE c1.enfirme = 1
                AND c1.cancelada = 0
				AND (c1.estatus = 'activo' OR c1.estatus = 'Terminado')
                AND c1.fecha_emision  >= ?
                AND c1.fecha_emision  <= ?
                ORDER BY c1.numero_pre;";
            $query = $this->db->query($sql, array($datos["fecha_inicial"], $datos["fecha_final"]));
            $result_precompromisos = $query->result();
            return $result_precompromisos;
        }else{
            $sql = "SELECT c1.numero_pre, c1.tipo_requisicion, c1.total AS total_precompromiso, c1.fecha_emision AS fecha_precompromiso, c1.total AS total_precompromiso, c1.proveedor AS proveedor_precompromiso, c1.nombre_completo AS nombre_precompromiso, c1.estatus AS estatus_precompromiso,
                         c2.numero_compromiso, c2.num_precompromiso, c2.tipo_compromiso, c2.fecha_emision AS fecha_compromiso, c2.total AS total_compromiso, c2.nombre_proveedor AS proveedor_compromiso, c2.nombre_completo AS nombre_compromiso, c2.estatus AS estatus_compromiso,
                         COLUMN_JSON(c3.nivel) AS niveles, c3.importe AS importe_compromiso_detalle,
                         c4.id_contrarecibo_caratula, c4.poliza AS contrarecibo_poliza, c4.fecha_pago AS fecha_contrarecibo, c4.importe AS importe_contrarecibo, c4.estatus AS estatus_contrarecibo,
                         c5.movimiento, c5.contrarecibo, c5.fecha_pago AS fecha_movimiento, c5.importe AS importe_movimiento, c5.enfirme AS enfirme_movimiento
                FROM mov_precompromiso_caratula c1
                LEFT JOIN mov_compromiso_caratula c2 ON c1.numero_pre  = c2.num_precompromiso
                LEFT JOIN mov_compromiso_detalle c3 ON c2.numero_compromiso = c3.numero_compromiso
                LEFT JOIN mov_contrarecibo_caratula c4 ON c3.numero_compromiso = c4.numero_compromiso
                LEFT JOIN mov_bancos_movimientos c5 ON c4.id_contrarecibo_caratula = c5.contrarecibo
                WHERE c1.enfirme = 1
                AND c1.cancelada = 0
				AND (c1.estatus = 'activo' OR c1.estatus = 'Terminado')
                AND c1.fecha_emision  >= ?
                AND c1.fecha_emision  <= ?
                AND c1.numero_pre = ?
                OR c2.numero_compromiso = ?
                ORDER BY c1.numero_pre;";
            $query = $this->db->query($sql, array($datos["fecha_inicial"], $datos["fecha_final"] , $datos["precompromiso"], $datos["compromiso"]));
            $result = $query->result();
            return $result;
        }
    }

    function get_datos_seguimiento_procesos_excel_compromisos($datos){
        if($datos["opciones"] == "gral") {
            $sql = "SELECT c2.numero_compromiso, c2.num_precompromiso, c2.tipo_compromiso, c2.fecha_emision AS fecha_compromiso, c2.total AS total_compromiso, c2.nombre_proveedor AS proveedor_compromiso, c2.nombre_completo AS nombre_compromiso, c2.estatus AS estatus_compromiso,
                         COLUMN_JSON(c3.nivel) AS niveles, c3.importe AS importe_compromiso_detalle,
                         c4.id_contrarecibo_caratula, c4.poliza AS contrarecibo_poliza, c4.fecha_pago AS fecha_contrarecibo, c4.importe AS importe_contrarecibo, c4.estatus AS estatus_contrarecibo,
                         c5.movimiento, c5.contrarecibo, c5.fecha_pago AS fecha_movimiento, c5.importe AS importe_movimiento, c5.enfirme AS enfirme_movimiento
                FROM mov_compromiso_caratula c2
                LEFT JOIN mov_compromiso_detalle c3 ON c2.numero_compromiso = c3.numero_compromiso
                LEFT JOIN mov_contrarecibo_caratula c4 ON c3.numero_compromiso = c4.numero_compromiso
                LEFT JOIN mov_bancos_movimientos c5 ON c4.id_contrarecibo_caratula = c5.contrarecibo
                WHERE c2.enfirme = 1
                AND c2.estatus = 'activo'
                AND c2.num_precompromiso = 0
                AND c2.cancelada = 0
                AND c2.fecha_emision  >= ?
                AND c2.fecha_emision  <= ?
                ORDER BY c2.numero_compromiso;";
            $query = $this->db->query($sql, array($datos["fecha_inicial"], $datos["fecha_final"]));
            $result_precompromisos = $query->result();
            return $result_precompromisos;
        }else{
            $sql = "SELECT c2.numero_compromiso, c2.num_precompromiso, c2.tipo_compromiso, c2.fecha_emision AS fecha_compromiso, c2.total AS total_compromiso, c2.nombre_proveedor AS proveedor_compromiso, c2.nombre_completo AS nombre_compromiso, c2.estatus AS estatus_compromiso,
                         COLUMN_JSON(c3.nivel) AS niveles, c3.importe AS importe_compromiso_detalle,
                         c4.id_contrarecibo_caratula, c4.poliza AS contrarecibo_poliza, c4.fecha_pago AS fecha_contrarecibo, c4.importe AS importe_contrarecibo, c4.estatus AS estatus_contrarecibo,
                         c5.movimiento, c5.contrarecibo, c5.fecha_pago AS fecha_movimiento, c5.importe AS importe_movimiento, c5.enfirme AS enfirme_movimiento
                FROM mov_compromiso_caratula c2
                LEFT JOIN mov_compromiso_detalle c3 ON c2.numero_compromiso = c3.numero_compromiso
                LEFT JOIN mov_contrarecibo_caratula c4 ON c3.numero_compromiso = c4.numero_compromiso
                LEFT JOIN mov_bancos_movimientos c5 ON c4.id_contrarecibo_caratula = c5.contrarecibo
                WHERE c2.enfirme = 1
                AND c2.estatus = 'activo'
                AND c2.num_precompromiso = 0
                AND c2.cancelada = 0
                AND c2.fecha_emision  >= ?
                AND c2.fecha_emision  <= ?
                AND c2.numero_compromiso = ?
                ORDER BY c2.numero_compromiso;";
            $query = $this->db->query($sql, array($datos["fecha_inicial"], $datos["fecha_final"] , $datos["compromiso"]));
            $result = $query->result();
            return $result;
        }
    }

    function get_datos_poliza_general_egresos_dr($contrarecibo){
        $sql = "SELECT numero_poliza, contrarecibo, movimiento, tipo_poliza, fecha AS fecha_poliza, importe AS importe_poliza, estatus AS estatus_poliza
                FROM mov_polizas_cabecera
                WHERE contrarecibo = ? AND movimiento = 0;";
        $query = $this->db->query($sql, array($contrarecibo));
        $result = $query->row();
        return $result;
    }
    function get_datos_poliza_general_egresos_eg($movimiento){
        $sql = "SELECT numero_poliza, contrarecibo, movimiento, tipo_poliza, fecha AS fecha_poliza, importe AS importe_poliza, estatus AS estatus_poliza
                FROM mov_polizas_cabecera
                WHERE movimiento = ?;";
        $query = $this->db->query($sql, array($movimiento));
        $result = $query->row();
        return $result;
    }

    function get_datos_tipo_gasto($datos){
        $sql = "SELECT mcp.tipo_gasto, SUM(mcc.importe) AS devengado, SUM(mbm.importe) AS pagado FROM mov_contrarecibo_caratula mcc
                INNER JOIN mov_compromiso_caratula mcp ON mcc.numero_compromiso = mcp.numero_compromiso
                LEFT JOIN mov_bancos_movimientos mbm ON mbm.contrarecibo = mcc.id_contrarecibo_caratula
                WHERE mcp.tipo_gasto LIKE ?;";
        $query = $this->db->query($sql, array("%" . $datos . "%"));
        $result = $query->row_array();
        return $result;
    }

    function get_datos_tipo_gasto_ampliaciones($datos){
        $sql = "SELECT mac.tipo_gasto, mad.texto, SUM(mad.total) AS ampliacion FROM mov_adecuaciones_egresos_detalle mad
                INNER JOIN mov_adecuaciones_egresos_caratula mac ON mac.numero = mad.numero_adecuacion
                WHERE mad.texto = 'Ampliación'
                AND mac.tipo_gasto LIKE ?;";
        $query = $this->db->query($sql, array("%" . $datos . "%"));
        $result = $query->row_array();
        return $result;
    }

    function get_datos_tipo_gasto_reducciones($datos){
        $sql = "SELECT  mac.tipo_gasto, mad.texto, SUM(mad.total) AS reduccion FROM mov_adecuaciones_egresos_detalle mad
                INNER JOIN mov_adecuaciones_egresos_caratula mac ON mac.numero = mad.numero_adecuacion
                WHERE mad.texto = 'Reducción'
                AND mac.tipo_gasto LIKE ?;";
        $query = $this->db->query($sql, array("%" . $datos . "%"));
        $result = $query->row_array();
        return $result;
    }


    function get_query_especifico_ingresos($datos) {
        $sql = "SELECT ae1.*, ae2.numero, ae2.enfirme, ae2.cancelada, COLUMN_JSON(estructura) AS estructura_final
                FROM mov_adecuaciones_ingresos_detalle ae1
                LEFT JOIN mov_adecuaciones_ingresos_caratula ae2 ON ae1.numero_adecuacion = ae2.numero
                WHERE COLUMN_GET(estructura, 'gerencia' as char) LIKE ?
                AND COLUMN_GET(estructura, 'centro_de_recaudacion' as char) LIKE ?
                AND COLUMN_GET(estructura, 'rubro' as char) LIKE ?
                AND COLUMN_GET(estructura, 'tipo' as char) LIKE ?
                AND COLUMN_GET(estructura, 'clase' as char) LIKE ?
                AND ae2.enfirme = 1
                AND ae2.cancelada = 0
                AND ae1.fecha_aplicacion >= ?
                AND ae1.fecha_aplicacion <= ?
                AND ae2.tipo LIKE ?
                ORDER BY ae1.fecha_aplicacion;";
        $query = $this->db->query($sql, array("%" . $datos["nivel1"] . "%",
            "%" . $datos["nivel2"] . "%",
            "%" . $datos["nivel3"] . "%",
            "%" . $datos["nivel4"] . "%",
            "%" . $datos["nivel5"] . "%",
            $datos["fecha_inicial"],
            $datos["fecha_final"],
            "%" . $datos["tipo_adecuacion"] . "%"));
        return $query->result();
    }

    function get_query_especifico_egresos($datos) {
            $sql = "SELECT maed.*, maec.numero, maec.enfirme, maec.cancelada, COLUMN_JSON(estructura) AS estructura_final
                FROM mov_adecuaciones_egresos_detalle maed
                JOIN mov_adecuaciones_egresos_caratula maec ON maed.numero_adecuacion = maec.numero
                WHERE COLUMN_GET(maed.estructura, 'fuente_de_financiamiento' as char) LIKE ?
                AND COLUMN_GET(maed.estructura, 'programa_de_financiamiento' as char) LIKE ?
                AND COLUMN_GET(maed.estructura, 'centro_de_costos' as char) LIKE ?
                AND COLUMN_GET(maed.estructura, 'capitulo' as char) LIKE ?
                AND COLUMN_GET(maed.estructura, 'concepto' as char) LIKE ?
                AND COLUMN_GET(maed.estructura, 'partida' as char) LIKE ?
                AND maec.enfirme = 1
                AND maec.cancelada != 1
                AND maed.fecha_aplicacion >= ?
				AND maed.fecha_aplicacion <= ?
                AND maec.tipo LIKE ?
                ORDER BY maed.fecha_aplicacion;";
            $query = $this->db->query($sql, array("%" . $datos["nivel1"] . "%",
                "%" . $datos["nivel2"] . "%",
                "%" . $datos["nivel3"] . "%",
                "%" . $datos["nivel4"] . "%",
                "%" . $datos["nivel5"] . "%",
                "%" . $datos["nivel6"] . "%",
                $datos["fecha_inicial"],
                $datos["fecha_final"],
                "%" . $datos["tipo_adecuacion"]. "%"));
            return $query->result();
    }
    function get_estructura_ingresos() {
        $sql = "SELECT *, COLUMN_JSON(nivel) AS estructura_inicial FROM cat_niveles
                WHERE COLUMN_GET(nivel, 'gerencia' as char)
                AND COLUMN_GET(nivel, 'centro_de_recaudacion' as char)
                AND COLUMN_GET(nivel, 'rubro' as char)
                AND COLUMN_GET(nivel, 'tipo' as char)
                AND COLUMN_GET(nivel, 'clase' as char);";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_estructura_basica_ingresos($datos) {
        $sql = "SELECT *, COLUMN_JSON(nivel) AS estructura_inicial FROM cat_niveles
                WHERE COLUMN_GET(nivel, 'gerencia' as char) LIKE '%".trim($datos["nivel1"])."%'
                AND COLUMN_GET(nivel, 'centro_de_recaudacion' as char) LIKE '%".trim($datos["nivel2"])."%'
                AND COLUMN_GET(nivel, 'rubro' as char) LIKE '%".trim($datos["nivel3"])."%'
                AND COLUMN_GET(nivel, 'tipo' as char) LIKE '%".trim($datos["nivel4"])."%'
                AND COLUMN_GET(nivel, 'clase' as char) LIKE '%".trim($datos["nivel5"])."%';";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    function get_ampliacion_reduccion_ingresos($centro, $rubro, $tipo, $clase, $fecha_inicial, $fecha_final) {
        $sql = "SELECT *, COLUMN_JSON(estructura) AS estructura FROM mov_adecuaciones_ingresos_detalle
                WHERE COLUMN_GET(estructura, 'centro_de_recaudacion' as char) LIKE '%".trim($centro)."%'
                AND COLUMN_GET(estructura, 'rubro' as char) LIKE '%".trim($rubro)."%'
                AND COLUMN_GET(estructura, 'tipo' as char) LIKE '%".trim($tipo)."%'
                AND COLUMN_GET(estructura, 'clase' as char) LIKE '%".trim($clase)."%'
                AND fecha_aplicacion >= ?
                AND fecha_aplicacion <= ?;";
        $query = $this->db->query($sql, array($fecha_inicial, $fecha_final));

        $total_ampliacion = 0;
        $total_reduccion = 0;

        foreach($query->result() as $fila) {
//            $this->debugeo->imprimir_pre($fila);
            $estructura = json_decode($fila->estructura);
            if($estructura->clase) {
//                $this->debugeo->imprimir_pre($estructura);
                if($fila->texto == "Ampliación") {
                    $total_ampliacion += $fila->total;
                } elseif($fila->texto == "Reducción") {
                    $total_reduccion += $fila->total;
                }
            }
        }

        return array(
            "ampliacion" => $total_ampliacion,
            "reduccion" => $total_reduccion,
        );
    }

    function get_estructura_egresos() {
        $sql = "SELECT *, COLUMN_JSON(nivel) AS estructura_inicial FROM cat_niveles
                WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) != ''
                AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) != ''
                AND COLUMN_GET(nivel, 'centro_de_costos' as char) != ''
                AND COLUMN_GET(nivel, 'capitulo' as char) != ''
                AND COLUMN_GET(nivel, 'concepto' as char) != ''
                AND COLUMN_GET(nivel, 'partida' as char) != '';";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_estructura_basica_egresos($datos) {

        $sql = "SELECT *, COLUMN_JSON(nivel) AS estructura_inicial FROM cat_niveles
                WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) LIKE '%".trim($datos["nivel1"])."%'
                AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) LIKE '%".trim($datos["nivel2"])."%'
                AND COLUMN_GET(nivel, 'centro_de_costos' as char) LIKE '%".trim($datos["nivel3"])."%'
                AND COLUMN_GET(nivel, 'capitulo' as char) LIKE '%".trim($datos["nivel4"])."%'
                AND COLUMN_GET(nivel, 'concepto' as char) LIKE '%".trim($datos["nivel5"])."%'
                AND COLUMN_GET(nivel, 'partida' as char) LIKE '%".trim($datos["nivel6"])."%';";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_estructura_basica_egresos_gasto_capitulo($datos, $cap) {

        $sql = "SELECT *, COLUMN_JSON(nivel) AS estructura_inicial FROM cat_niveles
                WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) LIKE '%".trim($datos["nivel1"])."%'
                AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) LIKE '%".trim($datos["nivel2"])."%'
                AND COLUMN_GET(nivel, 'centro_de_costos' as char) LIKE '%".trim($datos["nivel3"])."%'
                AND COLUMN_GET(nivel, 'capitulo' as char) LIKE '%".trim($cap)."%'
                AND COLUMN_GET(nivel, 'concepto' as char) LIKE '%".trim($datos["nivel5"])."%'
                AND COLUMN_GET(nivel, 'partida' as char) LIKE '%".trim($datos["nivel6"])."%';";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_estructura_centro_costos_egresos($fuente, $programa, $centro) {

        $sql = "SELECT *, COLUMN_JSON(nivel) AS estructura_inicial FROM cat_niveles
                WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) LIKE '".trim($fuente)."'
                AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) LIKE '".trim($programa)."'
                AND COLUMN_GET(nivel, 'centro_de_costos' as char) LIKE '%".trim($centro)."%';";
        $query = $this->db->query($sql);
        return $query->row();
    }

    function get_estructura_capitulo_egresos($fuente, $programa, $centro, $capitulo) {
        $sql = "SELECT *, COLUMN_JSON(nivel) AS estructura_inicial FROM cat_niveles
                WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) LIKE '%".trim($fuente)."%'
                AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) LIKE '%".trim($programa)."%'
                AND COLUMN_GET(nivel, 'centro_de_costos' as char) LIKE '%".trim($centro)."%'
                AND COLUMN_GET(nivel, 'capitulo' as char) LIKE '%".trim($capitulo)."%';";
        $query = $this->db->query($sql);
        return $query->row();
    }

    function get_estructura_concepto_egresos($fuente, $programa, $centro, $capitulo, $concepto) {

        $sql = "SELECT *, COLUMN_JSON(nivel) AS estructura_inicial FROM cat_niveles
                WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) LIKE '%".trim($fuente)."%'
                AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) LIKE '%".trim($programa)."%'
                AND COLUMN_GET(nivel, 'centro_de_costos' as char) LIKE '%".trim($centro)."%'
                AND COLUMN_GET(nivel, 'capitulo' as char) LIKE '%".trim($capitulo)."%'
                AND COLUMN_GET(nivel, 'concepto' as char) LIKE '%".trim($concepto)."%'";
        $query = $this->db->query($sql);
        return $query->row();
    }

    function get_estructura_partida_egresos($datos) {
        $respuesta = array();
        foreach($datos as $fila) {
            $estructura = json_decode($fila->estructura_inicial);
            $sql = "SELECT *, COLUMN_JSON(nivel) AS estructura_inicial FROM cat_niveles
                    WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) LIKE '%".trim($estructura->fuente_de_financiamiento)."%'
                    AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) LIKE '%".trim($estructura->programa_de_financiamiento)."%'
                    AND COLUMN_GET(nivel, 'centro_de_costos' as char) LIKE '%".trim($estructura->centro_de_costos)."%'
                    AND COLUMN_GET(nivel, 'capitulo' as char) LIKE '%".trim($estructura->capitulo)."%'
                    AND COLUMN_GET(nivel, 'concepto' as char) LIKE '%".trim($estructura->concepto)."%'
                    AND COLUMN_GET(nivel, 'partida' as char) != '';";
            $query = $this->db->query($sql);
            foreach($query->result() as $row) {
                $respuesta[] = $row;
            }
        }
        //$this->debugeo->imprimir_pre($respuesta);
        return $respuesta;
    }

    function get_ampliacion_reduccion_egresos($fuente_de_financiamiento, $programa_de_financiamiento,$centro_de_costos, $capitulo, $concepto, $partida,  $fecha_inicial, $fecha_final) {
        $sql = "SELECT texto, total, COLUMN_JSON(estructura) AS estructura FROM mov_adecuaciones_egresos_detalle
                WHERE COLUMN_GET(estructura, 'fuente_de_financiamiento' as char) LIKE '%".trim($fuente_de_financiamiento)."%'
                AND COLUMN_GET(estructura, 'programa_de_financiamiento' as char) LIKE '%".trim($programa_de_financiamiento)."%'
                AND COLUMN_GET(estructura, 'centro_de_costos' as char) LIKE '%".trim($centro_de_costos)."%'
                AND COLUMN_GET(estructura, 'capitulo' as char) LIKE '%".trim($capitulo)."%'
                AND COLUMN_GET(estructura, 'concepto' as char) LIKE '%".trim($concepto)."%'
                AND COLUMN_GET(estructura, 'partida' as char) LIKE '%".trim($partida)."%'
                AND fecha_aplicacion >= ?
                AND fecha_aplicacion <= ?;";
        $query = $this->db->query($sql, array($fecha_inicial, $fecha_final));

        $total_ampliacion = 0;
        $total_reduccion = 0;

        foreach($query->result() as $fila) {
            $estructura = json_decode($fila->estructura);
            if($estructura->partida) {
//                $this->debugeo->imprimir_pre($estructura);
                if($fila->texto == "Ampliación") {
                    $total_ampliacion += $fila->total;
//                    echo("Ampliacion <br />");
                } elseif($fila->texto == "Reducción") {
//                    echo("Reduccion <br />");
                    $total_reduccion += $fila->total;
                }
            }
        }

        return array(
            "ampliacion" => $total_ampliacion,
            "reduccion" => $total_reduccion,
        );
    }

    function get_ampliaciones_reducciones_egresos($nivel, $mes) {
        $sql = "SELECT ad.id_nivel, ad.mes_destino, ad.texto, ad.total, ad.fecha_aplicacion, ac.enfirme, ac.cancelada
                FROM mov_adecuaciones_egresos_detalle ad
                INNER JOIN mov_adecuaciones_egresos_caratula ac ON ad.numero_adecuacion = ac.numero
                WHERE ac.enfirme = 1
                AND ac.cancelada != 1
                AND ad.id_nivel = ?
                AND mes_destino = ?;";
        $query = $this->db->query($sql, array($nivel, $mes));

        return $query->result_array();
    }

    function get_ampliaciones_reducciones_ingresos($nivel, $mes) {
        $sql = "SELECT ad.id_nivel, ad.mes_destino, ad.texto, ad.total, ad.fecha_aplicacion, ac.enfirme, ac.cancelada
                FROM mov_adecuaciones_ingresos_detalle ad
                INNER JOIN mov_adecuaciones_ingresos_caratula ac ON ad.numero_adecuacion = ac.numero
                WHERE ac.enfirme = 1
                AND ac.cancelada != 1
                AND ad.id_nivel = ?
                AND mes_destino = ?;";
        $query = $this->db->query($sql, array($nivel, $mes));

        return $query->result_array();
    }

    // Reportes Contabilidad

    function  get_polizas_datos($datos) {
        if($datos["tipo_poliza"] == "Diario" || $datos["tipo_poliza"] == "Ingresos" || $datos["tipo_poliza"] == "Egresos"){
            $sql = "SELECT pc.fecha AS fecha_caratula, pd.* FROM mov_poliza_detalle pd
                    INNER JOIN mov_polizas_cabecera pc ON pd.numero_poliza = pc.numero_poliza
                    WHERE pc.enfirme = 1
                    AND pc.cancelada = 0
                    AND pd.tipo_poliza = ?
                    AND pc.fecha >= ?
                    AND pc.fecha <= ?;";
            $query = $this->db->query($sql, array($datos["tipo_poliza"], $datos["fecha_inicial"], $datos["fecha_final"]));
        } else{
            $sql = "SELECT pc.fecha AS fecha_caratula, pd.* FROM mov_poliza_detalle pd
                    INNER JOIN mov_polizas_cabecera pc ON pd.numero_poliza = pc.numero_poliza
                    WHERE pc.enfirme = 1
                    AND pc.cancelada = 0
                    AND pc.fecha >= ?
                    AND pc.fecha <= ?;";
            $query = $this->db->query($sql, array($datos["fecha_inicial"], $datos["fecha_final"]));
        }

        return $query->result();
    }




    function  get_datos_polizas_caratula_egresos($datos) {
        if($datos["tipo_poliza"] == "Diario" || $datos["tipo_poliza"] == "Ingresos" || $datos["tipo_poliza"] == "Egresos"){
            $sql = "SELECT p1.cuenta, p1.nombre, p1.partida, p1.debe, p1.haber, p2.numero_poliza, p2.fecha AS fecha_caratula, p2.enfirme, p2.contrarecibo, p2.movimiento, p2.concepto_especifico,
                p3.id_contrarecibo_caratula, p3.tipo_documento, p4.movimiento, p4.contrarecibo,
                COLUMN_GET(p5.nivel, 'partida' as char) AS partida, COLUMN_GET(p5.nivel, 'descripcion' as char) AS descripcion
                FROM mov_poliza_detalle p1
                LEFT JOIN mov_polizas_cabecera p2 ON p1.numero_poliza = p2.numero_poliza
                LEFT JOIN mov_bancos_movimientos p4 ON p4.movimiento = p2.movimiento
                LEFT JOIN mov_contrarecibo_caratula p3 ON p3.id_contrarecibo_caratula = p2.contrarecibo OR  p3.id_contrarecibo_caratula = p4.contrarecibo
                JOIN cat_niveles p5 ON  p1.partida = COLUMN_GET(p5.nivel, 'partida' as char)
                WHERE p1.tipo_poliza = ?
                AND p2.cancelada = 0
                AND p2.enfirme = 1
                AND p2.fecha >= ?
                AND p2.fecha <= ?
                 GROUP BY p1.id_poliza_detalle;";
            $query = $this->db->query($sql, array($datos["tipo_poliza"], $datos["fecha_inicial"], $datos["fecha_final"]));
            return $query->result();
        } else{
            $sql = "SELECT p1.cuenta, p1.nombre, p1.partida, p1.debe, p1.haber, p2.numero_poliza, p2.fecha AS fecha_caratula, p2.enfirme, p2.contrarecibo, p2.movimiento, p2.concepto_especifico,
                p3.id_contrarecibo_caratula, p3.tipo_documento, p4.movimiento, p4.contrarecibo,
                COLUMN_GET(p5.nivel, 'partida' as char) AS partida, COLUMN_GET(p5.nivel, 'descripcion' as char) AS descripcion
            FROM mov_poliza_detalle p1
            LEFT JOIN mov_polizas_cabecera p2 ON p1.numero_poliza = p2.numero_poliza
            LEFT JOIN mov_bancos_movimientos p4 ON p4.movimiento = p2.movimiento
            LEFT JOIN mov_contrarecibo_caratula p3 ON p3.id_contrarecibo_caratula = p2.contrarecibo OR  p3.id_contrarecibo_caratula = p4.contrarecibo
            JOIN cat_niveles p5 ON  p1.partida = COLUMN_GET(p5.nivel, 'partida' as char)
            WHERE p2.enfirme = 1
            AND p2.cancelada = 0
            AND p2.fecha >= ?
            AND p2.fecha <= ?
             GROUP BY p1.id_poliza_detalle;";
            $query = $this->db->query($sql, array($datos["fecha_inicial"], $datos["fecha_final"]));
            return $query->result();
        }
    }

    function  get_datos_polizas_caratula_ingresos($datos) {
        if($datos["tipo_poliza"] == "Diario" || $datos["tipo_poliza"] == "Ingresos" || $datos["tipo_poliza"] == "Egresos"){
            $sql = "SELECT  p1.cuenta, p1.nombre, p1.partida, p1.debe, p1.haber, p2.numero_poliza, p2.fecha AS fecha_caratula, p2.enfirme, p2.contrarecibo, p2.movimiento, p2.concepto_especifico,
                COLUMN_GET(p5.nivel, 'centro_de_recaudacion' as char) AS partida, COLUMN_GET(p5.nivel, 'descripcion' as char) AS descripcion
                FROM mov_poliza_detalle p1
                LEFT JOIN mov_polizas_cabecera p2 ON p1.numero_poliza = p2.numero_poliza
                JOIN cat_niveles p5 ON  p1.partida = COLUMN_GET(p5.nivel, 'centro_de_recaudacion' as char)
                WHERE p1.tipo_poliza = ?
                AND p2.contrarecibo = 0
                AND p2.enfirme = 1
                AND p2.cancelada = 0
                AND p2.fecha >= ?
                AND p2.fecha <= ?
                GROUP BY p1.id_poliza_detalle;";
            $query = $this->db->query($sql, array($datos["tipo_poliza"], $datos["fecha_inicial"], $datos["fecha_final"]));
            return $query->result();
        } else {
            $sql = "SELECT  p1.cuenta, p1.nombre, p1.partida, p1.debe, p1.haber, p2.numero_poliza, p2.fecha AS fecha_caratula, p2.enfirme, p2.contrarecibo, p2.movimiento, p2.concepto_especifico,
                COLUMN_GET(p5.nivel, 'centro_de_recaudacion' as char) AS partida, COLUMN_GET(p5.nivel, 'descripcion' as char) AS descripcion
                FROM mov_poliza_detalle p1
                LEFT JOIN mov_polizas_cabecera p2 ON p1.numero_poliza = p2.numero_poliza
                JOIN cat_niveles p5 ON  p1.partida = COLUMN_GET(p5.nivel, 'centro_de_recaudacion' as char)
                WHERE p2.contrarecibo = 0
                AND p2.enfirme = 1
                AND p2.cancelada = 0
                AND p2.fecha >= ?
                AND p2.fecha <= ?
                GROUP BY p1.id_poliza_detalle;";
            $query = $this->db->query($sql, array($datos["fecha_inicial"], $datos["fecha_final"]));
            return $query->result();
        }
    }

    function  get_datos_polizas_detalle($datos){
        $sql = "SELECT SUM(p1.debe) as debe, SUM(p1.haber) as haber, p1.cuenta, p1.concepto, p2.numero_poliza, p2.enfirme, p2.fecha
                FROM mov_poliza_detalle p1
                INNER JOIN mov_polizas_cabecera p2 ON p1.numero_poliza = p2.numero_poliza
                WHERE p2.enfirme = 1
                AND p1.cuenta LIKE ?
                AND p2.fecha >= ?
                AND p2.fecha <= ?
                GROUP BY p1.cuenta;";
        $query = $this->db->query($sql, array($datos["cuenta"]."%",$datos["fecha_inicial"], $datos["fecha_final"]));
        return $query->result();
    }

    function  get_nombre_cuenta($datos){
        $sql = "SELECT nombre FROM cat_cuentas_contables WHERE cuenta = ?;";
        $query = $this->db->query($sql, array($datos["cuenta"]));
        return $query->row();
    }

    function get_balanza(){
        $sql = "SELECT * FROM balanza_temporal;";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

     function get_balanza_activo(){
         $sql = "SELECT cuenta,saldo_inicial, cargo, abono, saldo_final FROM balanza_temporal
                WHERE cuenta = '1.1.1'
                OR cuenta = '1.1.2'
                OR cuenta = '1.1.3'
                OR cuenta = '1.1.4'
                OR cuenta = '1.1.5'
                OR cuenta = '1.1.6'
                OR cuenta = '1.2.3'
                OR cuenta = '1.2.4'
                OR cuenta = '1.2.6'
                OR cuenta = '1.2.7';";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    function get_balanza_analitico_activo_p(){
        $sql = "SELECT cuenta, saldo_inicial, cargo, abono FROM balanza_temporal_reportes
                WHERE cuenta = '1.1.1.2'
                OR cuenta = '1.1.2.9'
                OR cuenta = '1.1.1.3'
                OR cuenta = '1.1.3.9'
                OR cuenta = '1.1.2.2'
                OR cuenta = '1.1.2.3'
                OR cuenta = '1.1.2.7'
                OR cuenta = '1.1.2.8'
                OR cuenta = '1.1.4.1'
                OR cuenta = '1.1.5.1'
                OR cuenta = '1.1.6.1'
                OR cuenta = '1.1.6.2'
                OR cuenta = '1.2.3.1'
                OR cuenta = '1.2.3.3'
                OR cuenta = '1.2.3.6'
                OR cuenta = '1.2.4.1'
                OR cuenta = '1.2.4.4'
                OR cuenta = '1.2.4.6'
                OR cuenta = '1.2.6.1'
                OR cuenta = '1.2.6.3'
                OR cuenta = '1.2.6.7'
                OR cuenta = '1.2.7.3'
                OR cuenta = '1.2.7.9'
                OR cuenta = '1.1.1.1.1'
                OR cuenta = '1.1.1.1.2'
                OR cuenta = '1.1.1.1.3'
                OR cuenta = '1.1.1.1.4';";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_balanza_analitico_pasivo_p(){
        $sql = "SELECT cuenta, saldo_inicial, cargo, abono, saldo_final FROM balanza_temporal_reportes
                WHERE cuenta = '2.1.1.2'
                OR cuenta = '2.1.1.1'
                OR cuenta = '2.1.2.1'
                OR cuenta = '2.1.7.9'
                OR cuenta = '2.1.5.1'
                OR cuenta = '2.1.2.2'
                OR cuenta = '2.1.1.7'
                OR cuenta = '2.1.7.1'
                OR cuenta = '2.1.7.2';";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_balanza_activo_p(){
        $sql = "SELECT cuenta,saldo_inicial, cargo, abono, saldo_final FROM balanza_temporal_reportes
                WHERE cuenta = '1.1.1.1'
                OR cuenta = '1.1.1.2'
                OR cuenta = '1.1.1.3'
                OR cuenta = '1.1.6.1'

                OR cuenta = '1.1.2.2'
                OR cuenta = '1.1.2.3'
                OR cuenta = '1.1.2.9'

                OR cuenta = '1.1.4.1'
                OR cuenta = '1.1.6.2'
                OR cuenta = '1.1.5.1'

                OR cuenta = '1.1.3.9'

                OR cuenta = '1.2.3.1'
                OR cuenta = '1.2.3.3'
                OR cuenta = '1.2.3.6'

                OR cuenta = '1.2.4.1'
                OR cuenta = '1.2.4.4'
                OR cuenta = '1.2.4.6'
                OR cuenta = '1.2.6.1'
                OR cuenta = '1.2.6.3'

                OR cuenta = '1.1.2.7'
                OR cuenta = '1.1.2.8'
                OR cuenta = '1.2.7.3'
                OR cuenta = '1.2.6.7'
                OR cuenta = '1.2.7.9';";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_balanza_activo_p_S(){
        $sql = "SELECT cuenta,saldo_inicial, cargo, abono, saldo_final FROM balanza_temporal_reportes
                WHERE cuenta = '1.1.1.1.1'
                OR cuenta = '1.1.1.1.2'
                OR cuenta = '1.1.1.1.3'
                OR cuenta = '1.1.1.1.4';";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_balanza_pasivo($datos){
        $sql = "SELECT cuenta, saldo_final FROM balanza_temporal
                WHERE cuenta = '2.1.1'
                OR cuenta = '2.1.2'
                OR cuenta = '2.1.5'
                OR cuenta = '2.1.7';";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_balanza_pasivo_p(){
        $sql = "SELECT cuenta,saldo_inicial, cargo, abono, saldo_final FROM balanza_temporal_reportes
                WHERE cuenta = '2.1.1.2'
                OR cuenta = '2.1.1.7'
                OR cuenta = '2.1.1.1'
                OR cuenta = '2.1.2.1'
                OR cuenta = '2.1.2.2'
                OR cuenta = '2.1.5.1'
                OR cuenta = '2.1.7.1'
                OR cuenta = '2.1.7.2'
                OR cuenta = '2.1.7.9'
                OR cuenta = '2.1.7.2.1.1'
                OR cuenta = '2.1.7.2.1.3';";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_balanza_hacienda(){
        $sql = "SELECT cuenta, saldo_final FROM balanza_temporal
                WHERE cuenta = '3.1.1'
                OR cuenta = '3.1.2'
				OR cuenta = '3.1.3'
				OR cuenta = '3.2.1'
				OR cuenta = '3.2.2'
				OR cuenta = '3.2.3'
				OR cuenta = '3.2.4'
				OR cuenta = '3.3.3';";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_balanza_hacienda_p(){
        $sql = "SELECT cuenta, ABS(saldo_final) AS saldo_final, ABS(cargo) AS cargo, ABS(abono) AS abono FROM balanza_temporal_reportes
                WHERE cuenta = '3.1.1.2'
                OR cuenta = '3.1.1.3'
				OR cuenta = '3.1.1.4'
				OR cuenta = '3.1.2'
				OR cuenta = '3.1.3'
				OR cuenta = '3.2.3.1'
				OR cuenta = '3.2.3.2'
				OR cuenta = '3.2.3.9'
				OR cuenta = '3.2.4'
				OR cuenta = '3.3.3.1';";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    function get_balanza_hacienda_p_s(){
        $sql = "SELECT cuenta, saldo_final, ABS(cargo) AS cargo, ABS(abono) AS abono FROM balanza_temporal_reportes
                WHERE cuenta = '3.2.2'
				OR cuenta = '3.3.3.3';";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_balanza_actividades(){
        $sql = "SELECT cuenta, saldo_final FROM balanza_temporal
                WHERE cuenta = '4.1.7'
                OR cuenta = '4.2.2'
				OR cuenta = '4.3.1'
				OR cuenta = '4.3.9'
				OR cuenta = '5.1.1'
				OR cuenta = '5.1.2'
				OR cuenta = '5.1.3'
				OR cuenta = '5.5.1'
				OR cuenta = '5.5.9'
				OR cuenta = '5.6.2';";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    function get_balanza_actividades_p(){
        $sql = "SELECT cuenta, ABS(saldo_final) AS saldo_final, ABS(cargo) AS cargo, ABS(abono) AS abono FROM balanza_temporal_reportes
                WHERE cuenta = '4.1.7.4'
                OR cuenta = '4.2.2.3'
				OR cuenta = '4.3.1.1'
				OR cuenta = '4.3.9'
				OR cuenta = '5.1.1'
				OR cuenta = '5.1.2'
				OR cuenta = '5.1.3'
				OR cuenta = '5.5.1'
				OR cuenta = '5.5.9';";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_balanza_resultados(){
        $sql = "SELECT cuenta,saldo_inicial, cargo, abono, ABS(saldo_final) AS saldo_final FROM balanza_temporal_reportes
                WHERE cuenta = '4.1.7.4'
                OR cuenta = '4.3.9.2'
                OR cuenta = '4.3.9.3'
                OR cuenta = '5.5.9.3'
                OR cuenta = '4.1.7.4.4'
                OR cuenta = '5.6.2.1'
                OR cuenta = '4.2.2'
                OR cuenta = '4.3.1.1'
                OR cuenta = '4.3.1.1.1'
                OR cuenta = '4.3.1.1.1.2'
                OR cuenta = '5.5.9.4'
                OR cuenta = '4.3.9.9'
                OR cuenta = '5.5.9.9.9'
                OR cuenta = '5.5.1.3'
                OR cuenta = '5.5.1.5'
                OR cuenta = '5.5.1.8'
                OR cuenta = '5.5.9.9.4';";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_balanza_resultados_signo(){
        $sql = "SELECT cuenta,saldo_inicial, cargo, abono, saldo_final AS saldo_final FROM balanza_temporal_reportes
                WHERE cuenta = '4.1.7.4.4';";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_balanza_situacion_activo_p(){
        $sql = "SELECT cuenta, saldo_inicial, saldo_final FROM balanza_temporal_reportes
                WHERE cuenta = '1.1.1'
                OR cuenta = '1.1.2'
                OR cuenta = '1.1.3'
                OR cuenta = '1.1.4'
                OR cuenta = '1.1.5'
                OR cuenta = '1.1.6'
                OR cuenta = '1.2.3'
                OR cuenta = '1.2.4'
                OR cuenta = '1.2.6'
                OR cuenta = '1.2.7';";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_balanza_situacion_pasivo_p(){
        $sql = "SELECT cuenta, saldo_inicial, saldo_final FROM balanza_temporal_reportes
                WHERE cuenta = '2.1.1'
                OR cuenta = '2.1.2'
                OR cuenta = '2.1.5'
                OR cuenta = '2.1.7'
                OR cuenta = '2.1.7.2.1.1'
                OR cuenta = '2.1.7.2.1.3';";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    function get_balanza_situacion_hacienda_p(){
        $sql = "SELECT cuenta, saldo_inicial, saldo_final FROM balanza_temporal_reportes
                WHERE cuenta = '3.1.1'
                OR cuenta = '3.1.2'
                OR cuenta = '3.1.3'
                OR cuenta = '3.2.2'
                OR cuenta = '3.2.3'
               OR cuenta = '3.2.4';";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_datos_balanza_resultados($query) {
        $query = $this->db->query($query);
        $result = $query->row();
        return $result;
    }



    /**
     * Funcion para devolver las partidas por mes y las ampliaciones/reducciones egresos
     *
     * @author IBR 22/04/2015
     * @param $numMes  Número de Mes a procesar
     * @param $anio Año a procesar
     * @return mixed
     */
    function get_partidas_por_mes_amp_reduc_egresos($numMes, $anio) {
        $diaFinal = cal_days_in_month(CAL_GREGORIAN, $numMes, $anio);
        $fechaIni = $anio . "-" . $numMes . "-01";
        $fechaFin = $anio . "-" . $numMes . "-" . $diaFinal;
        $mes = $this->utilerias->convertirFechaAMes($fechaIni);

        $sql = "
SELECT
				mov.texto,
				mov.total,
				niv.id_niveles id_nivel,
				COLUMN_JSON(niv.nivel) AS estructura,
				COLUMN_GET(niv.nivel, '".$mes."_inicial' as char) AS mes_inicial,
				COLUMN_GET(niv.nivel, '".$mes."_compromiso' as char) AS mes_compromiso,
				COLUMN_GET(niv.nivel, '".$mes."_precompromiso' as char) AS mes_precompromiso,
				COLUMN_GET(niv.nivel, '".$mes."_devengado' as char) AS mes_devengado,
				COLUMN_GET(niv.nivel, '".$mes."_ejercido' as char) AS mes_ejercido,
				COLUMN_GET(niv.nivel, '".$mes."_saldo' as char) AS saldo,
				COLUMN_GET(niv.nivel, '".$mes."_devuelto' as char) AS mes_devuelto,
				COLUMN_GET(niv.nivel, '".$mes."_pagado' as char) AS mes_pagado,
				COLUMN_GET(niv.nivel, 'precomprometido_anual' as char) AS precomprometido_anual,
				COLUMN_GET(niv.nivel, 'comprometido_anual' as char) AS comprometido_anual,
				COLUMN_GET(niv.nivel, 'total_anual' as char) AS total_anual
FROM cat_niveles niv
INNER JOIN mov_adecuaciones_egresos_detalle mov
								ON mov.id_nivel = niv.id_niveles
									AND mov.fecha_aplicacion >= ?
									AND mov.fecha_aplicacion <= ?
JOIN mov_adecuaciones_egresos_caratula mov_car
                                ON mov.numero_adecuacion = mov_car.numero
WHERE COLUMN_GET(niv.nivel, 'fuente_de_financiamiento' as char) != ''
						AND COLUMN_GET(niv.nivel, 'programa_de_financiamiento' as char) != ''
						AND COLUMN_GET(niv.nivel, 'centro_de_costos' as char) != ''
						AND COLUMN_GET(niv.nivel, 'capitulo' as char) != ''
						AND COLUMN_GET(niv.nivel, 'concepto' as char) != ''
						AND COLUMN_GET(niv.nivel, 'partida' as char) != ''
						AND mov_car.cancelada != 1

UNION ALL

SELECT
				'' texto,
				0 total,
				niv.id_niveles id_nivel,
				COLUMN_JSON(niv.nivel) AS estructura,
				COLUMN_GET(niv.nivel, '".$mes."_inicial' as char) AS mes_inicial,
				COLUMN_GET(niv.nivel, '".$mes."_compromiso' as char) AS mes_compromiso,
				COLUMN_GET(niv.nivel, '".$mes."_precompromiso' as char) AS mes_precompromiso,
				COLUMN_GET(niv.nivel, '".$mes."_devengado' as char) AS mes_devengado,
				COLUMN_GET(niv.nivel, '".$mes."_ejercido' as char) AS mes_ejercido,
				COLUMN_GET(niv.nivel, '".$mes."_saldo' as char) AS saldo,
				COLUMN_GET(niv.nivel, '".$mes."_devuelto' as char) AS mes_devuelto,
				COLUMN_GET(niv.nivel, '".$mes."_pagado' as char) AS mes_pagado,
				COLUMN_GET(niv.nivel, 'precomprometido_anual' as char) AS precomprometido_anual,
				COLUMN_GET(niv.nivel, 'comprometido_anual' as char) AS comprometido_anual,
				COLUMN_GET(niv.nivel, 'total_anual' as char) AS total_anual
FROM cat_niveles niv
WHERE COLUMN_GET(niv.nivel, 'fuente_de_financiamiento' as char) != ''
						AND COLUMN_GET(niv.nivel, 'programa_de_financiamiento' as char) != ''
						AND COLUMN_GET(niv.nivel, 'centro_de_costos' as char) != ''
						AND COLUMN_GET(niv.nivel, 'capitulo' as char) != ''
						AND COLUMN_GET(niv.nivel, 'concepto' as char) != ''
						AND COLUMN_GET(niv.nivel, 'partida' as char) != ''
						AND COLUMN_GET(niv.nivel, '".$mes."_inicial' as char) != 0
ORDER BY id_nivel, texto
                ";
        $query = $this->db->query( $sql, array($fechaIni, $fechaFin) );
        //echo "Total Elemetos [" . $query->num_rows . "]";
        return $query->result();
    }

    function get_partidas_por_mes_amp_reduc_egresos_anual() {
        $sql = "
SELECT
				mov.texto,
				mov.total,
				mov.fecha_aplicacion,
				niv.id_niveles id_nivel,
				COLUMN_JSON(niv.nivel) AS estructura,
				COLUMN_GET(niv.nivel, 'enero_inicial' as char) AS enero_inicial,
				COLUMN_GET(niv.nivel, 'enero_compromiso' as char) AS enero_compromiso,
				COLUMN_GET(niv.nivel, 'enero_precompromiso' as char) AS enero_precompromiso,
				COLUMN_GET(niv.nivel, 'enero_devengado' as char) AS enero_devengado,
				COLUMN_GET(niv.nivel, 'enero_ejercido' as char) AS enero_ejercido,
				COLUMN_GET(niv.nivel, 'enero_pagado' as char) AS enero_pagado,
				COLUMN_GET(niv.nivel, 'enero_saldo' as char) AS enero_saldo,
				COLUMN_GET(niv.nivel, 'enero_devuelto' as char) AS enero_devuelto,
				
				COLUMN_GET(niv.nivel, 'febrero_inicial' as char) AS febrero_inicial,
				COLUMN_GET(niv.nivel, 'febrero_compromiso' as char) AS febrero_compromiso,
				COLUMN_GET(niv.nivel, 'febrero_precompromiso' as char) AS febrero_precompromiso,
				COLUMN_GET(niv.nivel, 'febrero_devengado' as char) AS febrero_devengado,
				COLUMN_GET(niv.nivel, 'febrero_ejercido' as char) AS febrero_ejercido,
				COLUMN_GET(niv.nivel, 'febrero_pagado' as char) AS febrero_pagado,
				COLUMN_GET(niv.nivel, 'febrero_saldo' as char) AS febrero_saldo,
				COLUMN_GET(niv.nivel, 'febrero_devuelto' as char) AS febrero_devuelto,
				
				COLUMN_GET(niv.nivel, 'marzo_inicial' as char) AS marzo_inicial,
				COLUMN_GET(niv.nivel, 'marzo_compromiso' as char) AS marzo_compromiso,
				COLUMN_GET(niv.nivel, 'marzo_precompromiso' as char) AS marzo_precompromiso,
				COLUMN_GET(niv.nivel, 'marzo_devengado' as char) AS marzo_devengado,
				COLUMN_GET(niv.nivel, 'marzo_ejercido' as char) AS marzo_ejercido,
				COLUMN_GET(niv.nivel, 'marzo_pagado' as char) AS marzo_pagado,
				COLUMN_GET(niv.nivel, 'marzo_saldo' as char) AS marzo_saldo,
				COLUMN_GET(niv.nivel, 'marzo_devuelto' as char) AS marzo_devuelto,
				
				COLUMN_GET(niv.nivel, 'abril_inicial' as char) AS abril_inicial,
				COLUMN_GET(niv.nivel, 'abril_compromiso' as char) AS abril_compromiso,
				COLUMN_GET(niv.nivel, 'abril_precompromiso' as char) AS abril_precompromiso,
				COLUMN_GET(niv.nivel, 'abril_devengado' as char) AS abril_devengado,
				COLUMN_GET(niv.nivel, 'abril_ejercido' as char) AS abril_ejercido,
				COLUMN_GET(niv.nivel, 'abril_pagado' as char) AS abril_pagado,
				COLUMN_GET(niv.nivel, 'abril_saldo' as char) AS abril_saldo,
				COLUMN_GET(niv.nivel, 'abril_devuelto' as char) AS abril_devuelto,
				
				COLUMN_GET(niv.nivel, 'mayo_inicial' as char) AS mayo_inicial,
				COLUMN_GET(niv.nivel, 'mayo_compromiso' as char) AS mayo_compromiso,
				COLUMN_GET(niv.nivel, 'mayo_precompromiso' as char) AS mayo_precompromiso,
				COLUMN_GET(niv.nivel, 'mayo_devengado' as char) AS mayo_devengado,
				COLUMN_GET(niv.nivel, 'mayo_ejercido' as char) AS mayo_ejercido,
				COLUMN_GET(niv.nivel, 'mayo_pagado' as char) AS mayo_pagado,
				COLUMN_GET(niv.nivel, 'mayo_saldo' as char) AS mayo_saldo,
				COLUMN_GET(niv.nivel, 'mayo_devuelto' as char) AS mayo_devuelto,
				
				COLUMN_GET(niv.nivel, 'junio_inicial' as char) AS junio_inicial,
				COLUMN_GET(niv.nivel, 'junio_compromiso' as char) AS junio_compromiso,
				COLUMN_GET(niv.nivel, 'junio_precompromiso' as char) AS junio_precompromiso,
				COLUMN_GET(niv.nivel, 'junio_devengado' as char) AS junio_devengado,
				COLUMN_GET(niv.nivel, 'junio_ejercido' as char) AS junio_ejercido,
				COLUMN_GET(niv.nivel, 'junio_pagado' as char) AS junio_pagado,
				COLUMN_GET(niv.nivel, 'junio_saldo' as char) AS junio_saldo,
				COLUMN_GET(niv.nivel, 'junio_devuelto' as char) AS junio_devuelto,
				
				COLUMN_GET(niv.nivel, 'julio_inicial' as char) AS julio_inicial,
				COLUMN_GET(niv.nivel, 'julio_compromiso' as char) AS julio_compromiso,
				COLUMN_GET(niv.nivel, 'julio_precompromiso' as char) AS julio_precompromiso,
				COLUMN_GET(niv.nivel, 'julio_devengado' as char) AS julio_devengado,
				COLUMN_GET(niv.nivel, 'julio_ejercido' as char) AS julio_ejercido,
				COLUMN_GET(niv.nivel, 'julio_pagado' as char) AS julio_pagado,
				COLUMN_GET(niv.nivel, 'julio_saldo' as char) AS julio_saldo,
				COLUMN_GET(niv.nivel, 'julio_devuelto' as char) AS julio_devuelto,
				
				COLUMN_GET(niv.nivel, 'agosto_inicial' as char) AS agosto_inicial,
				COLUMN_GET(niv.nivel, 'agosto_compromiso' as char) AS agosto_compromiso,
				COLUMN_GET(niv.nivel, 'agosto_precompromiso' as char) AS agosto_precompromiso,
				COLUMN_GET(niv.nivel, 'agosto_devengado' as char) AS agosto_devengado,
				COLUMN_GET(niv.nivel, 'agosto_ejercido' as char) AS agosto_ejercido,
				COLUMN_GET(niv.nivel, 'agosto_pagado' as char) AS agosto_pagado,
				COLUMN_GET(niv.nivel, 'agosto_saldo' as char) AS agosto_saldo,
				COLUMN_GET(niv.nivel, 'agosto_devuelto' as char) AS agosto_devuelto,
				
				COLUMN_GET(niv.nivel, 'septiembre_inicial' as char) AS septiembre_inicial,
				COLUMN_GET(niv.nivel, 'septiembre_compromiso' as char) AS septiembre_compromiso,
				COLUMN_GET(niv.nivel, 'septiembre_precompromiso' as char) AS septiembre_precompromiso,
				COLUMN_GET(niv.nivel, 'septiembre_devengado' as char) AS septiembre_devengado,
				COLUMN_GET(niv.nivel, 'septiembre_ejercido' as char) AS septiembre_ejercido,
				COLUMN_GET(niv.nivel, 'septiembre_pagado' as char) AS septiembre_pagado,
				COLUMN_GET(niv.nivel, 'septiembre_saldo' as char) AS septiembre_saldo,
				COLUMN_GET(niv.nivel, 'septiembre_devuelto' as char) AS septiembre_devuelto,
				
				COLUMN_GET(niv.nivel, 'octubre_inicial' as char) AS octubre_inicial,
				COLUMN_GET(niv.nivel, 'octubre_compromiso' as char) AS octubre_compromiso,
				COLUMN_GET(niv.nivel, 'octubre_precompromiso' as char) AS octubre_precompromiso,
				COLUMN_GET(niv.nivel, 'octubre_devengado' as char) AS octubre_devengado,
				COLUMN_GET(niv.nivel, 'octubre_ejercido' as char) AS octubre_ejercido,
				COLUMN_GET(niv.nivel, 'octubre_pagado' as char) AS octubre_pagado,
				COLUMN_GET(niv.nivel, 'octubre_saldo' as char) AS octubre_saldo,
				COLUMN_GET(niv.nivel, 'octubre_devuelto' as char) AS octubre_devuelto,
				
				COLUMN_GET(niv.nivel, 'noviembre_inicial' as char) AS noviembre_inicial,
				COLUMN_GET(niv.nivel, 'noviembre_compromiso' as char) AS noviembre_compromiso,
				COLUMN_GET(niv.nivel, 'noviembre_precompromiso' as char) AS noviembre_precompromiso,
				COLUMN_GET(niv.nivel, 'noviembre_devengado' as char) AS noviembre_devengado,
				COLUMN_GET(niv.nivel, 'noviembre_ejercido' as char) AS noviembre_ejercido,
				COLUMN_GET(niv.nivel, 'noviembre_pagado' as char) AS noviembre_pagado,
				COLUMN_GET(niv.nivel, 'noviembre_saldo' as char) AS noviembre_saldo,
				COLUMN_GET(niv.nivel, 'noviembre_devuelto' as char) AS noviembre_devuelto,
				
				COLUMN_GET(niv.nivel, 'diciembre_inicial' as char) AS diciembre_inicial,
				COLUMN_GET(niv.nivel, 'diciembre_compromiso' as char) AS diciembre_compromiso,
				COLUMN_GET(niv.nivel, 'diciembre_precompromiso' as char) AS diciembre_precompromiso,
				COLUMN_GET(niv.nivel, 'diciembre_devengado' as char) AS diciembre_devengado,
				COLUMN_GET(niv.nivel, 'diciembre_ejercido' as char) AS diciembre_ejercido,
				COLUMN_GET(niv.nivel, 'diciembre_pagado' as char) AS diciembre_pagado,
				COLUMN_GET(niv.nivel, 'diciembre_saldo' as char) AS diciembre_saldo,
				COLUMN_GET(niv.nivel, 'diciembre_devuelto' as char) AS diciembre_devuelto,
				
				COLUMN_GET(niv.nivel, 'precomprometido_anual' as char) AS precomprometido_anual,
				COLUMN_GET(niv.nivel, 'comprometido_anual' as char) AS comprometido_anual,
				COLUMN_GET(niv.nivel, 'devengado_anual' as char) AS devengado_anual,
				COLUMN_GET(niv.nivel, 'ejercido_anual' as char) AS ejercido_anual,
				COLUMN_GET(niv.nivel, 'pagado_anual' as char) AS pagado_anual,
				COLUMN_GET(niv.nivel, 'total_anual' as char) AS total_anual
FROM cat_niveles niv
JOIN mov_adecuaciones_egresos_detalle mov
								ON mov.id_nivel = niv.id_niveles
									AND mov.fecha_aplicacion >= '2015-01-01'
									AND mov.fecha_aplicacion <= '2015-12-31'
JOIN mov_adecuaciones_egresos_caratula mov_car
                                ON mov.numero_adecuacion = mov_car.numero
WHERE COLUMN_GET(niv.nivel, 'fuente_de_financiamiento' as char) != ''
						AND COLUMN_GET(niv.nivel, 'programa_de_financiamiento' as char) != ''
						AND COLUMN_GET(niv.nivel, 'centro_de_costos' as char) != ''
						AND COLUMN_GET(niv.nivel, 'capitulo' as char) != ''
						AND COLUMN_GET(niv.nivel, 'concepto' as char) != ''
						AND COLUMN_GET(niv.nivel, 'partida' as char) != ''
						AND mov_car.cancelada != 1

UNION ALL

SELECT
				'' texto,
				0 total,
				'0000-00-00' fecha_aplicacion,
				niv.id_niveles id_nivel,
				COLUMN_JSON(niv.nivel) AS estructura,
				COLUMN_GET(niv.nivel, 'enero_inicial' as char) AS enero_inicial,
				COLUMN_GET(niv.nivel, 'enero_compromiso' as char) AS enero_compromiso,
				COLUMN_GET(niv.nivel, 'enero_precompromiso' as char) AS enero_precompromiso,
				COLUMN_GET(niv.nivel, 'enero_devengado' as char) AS enero_devengado,
				COLUMN_GET(niv.nivel, 'enero_ejercido' as char) AS enero_ejercido,
				COLUMN_GET(niv.nivel, 'enero_pagado' as char) AS enero_pagado,
				COLUMN_GET(niv.nivel, 'enero_saldo' as char) AS enero_saldo,
				COLUMN_GET(niv.nivel, 'enero_devuelto' as char) AS enero_devuelto,

				COLUMN_GET(niv.nivel, 'febrero_inicial' as char) AS febrero_inicial,
				COLUMN_GET(niv.nivel, 'febrero_compromiso' as char) AS febrero_compromiso,
				COLUMN_GET(niv.nivel, 'febrero_precompromiso' as char) AS febrero_precompromiso,
				COLUMN_GET(niv.nivel, 'febrero_devengado' as char) AS febrero_devengado,
				COLUMN_GET(niv.nivel, 'febrero_ejercido' as char) AS febrero_ejercido,
				COLUMN_GET(niv.nivel, 'febrero_pagado' as char) AS febrero_pagado,
				COLUMN_GET(niv.nivel, 'febrero_saldo' as char) AS febrero_saldo,
				COLUMN_GET(niv.nivel, 'febrero_devuelto' as char) AS febrero_devuelto,

				COLUMN_GET(niv.nivel, 'marzo_inicial' as char) AS marzo_inicial,
				COLUMN_GET(niv.nivel, 'marzo_compromiso' as char) AS marzo_compromiso,
				COLUMN_GET(niv.nivel, 'marzo_precompromiso' as char) AS marzo_precompromiso,
				COLUMN_GET(niv.nivel, 'marzo_devengado' as char) AS marzo_devengado,
				COLUMN_GET(niv.nivel, 'marzo_ejercido' as char) AS marzo_ejercido,
				COLUMN_GET(niv.nivel, 'marzo_pagado' as char) AS marzo_pagado,
				COLUMN_GET(niv.nivel, 'marzo_saldo' as char) AS marzo_saldo,
				COLUMN_GET(niv.nivel, 'marzo_devuelto' as char) AS marzo_devuelto,

				COLUMN_GET(niv.nivel, 'abril_inicial' as char) AS abril_inicial,
				COLUMN_GET(niv.nivel, 'abril_compromiso' as char) AS abril_compromiso,
				COLUMN_GET(niv.nivel, 'abril_precompromiso' as char) AS abril_precompromiso,
				COLUMN_GET(niv.nivel, 'abril_devengado' as char) AS abril_devengado,
				COLUMN_GET(niv.nivel, 'abril_ejercido' as char) AS abril_ejercido,
				COLUMN_GET(niv.nivel, 'abril_pagado' as char) AS abril_pagado,
				COLUMN_GET(niv.nivel, 'abril_saldo' as char) AS abril_saldo,
				COLUMN_GET(niv.nivel, 'abril_devuelto' as char) AS abril_devuelto,

				COLUMN_GET(niv.nivel, 'mayo_inicial' as char) AS mayo_inicial,
				COLUMN_GET(niv.nivel, 'mayo_compromiso' as char) AS mayo_compromiso,
				COLUMN_GET(niv.nivel, 'mayo_precompromiso' as char) AS mayo_precompromiso,
				COLUMN_GET(niv.nivel, 'mayo_devengado' as char) AS mayo_devengado,
				COLUMN_GET(niv.nivel, 'mayo_ejercido' as char) AS mayo_ejercido,
				COLUMN_GET(niv.nivel, 'mayo_pagado' as char) AS mayo_pagado,
				COLUMN_GET(niv.nivel, 'mayo_saldo' as char) AS mayo_saldo,
				COLUMN_GET(niv.nivel, 'mayo_devuelto' as char) AS mayo_devuelto,

				COLUMN_GET(niv.nivel, 'junio_inicial' as char) AS junio_inicial,
				COLUMN_GET(niv.nivel, 'junio_compromiso' as char) AS junio_compromiso,
				COLUMN_GET(niv.nivel, 'junio_precompromiso' as char) AS junio_precompromiso,
				COLUMN_GET(niv.nivel, 'junio_devengado' as char) AS junio_devengado,
				COLUMN_GET(niv.nivel, 'junio_ejercido' as char) AS junio_ejercido,
				COLUMN_GET(niv.nivel, 'junio_pagado' as char) AS junio_pagado,
				COLUMN_GET(niv.nivel, 'junio_saldo' as char) AS junio_saldo,
				COLUMN_GET(niv.nivel, 'junio_devuelto' as char) AS junio_devuelto,

				COLUMN_GET(niv.nivel, 'julio_inicial' as char) AS julio_inicial,
				COLUMN_GET(niv.nivel, 'julio_compromiso' as char) AS julio_compromiso,
				COLUMN_GET(niv.nivel, 'julio_precompromiso' as char) AS julio_precompromiso,
				COLUMN_GET(niv.nivel, 'julio_devengado' as char) AS julio_devengado,
				COLUMN_GET(niv.nivel, 'julio_ejercido' as char) AS julio_ejercido,
				COLUMN_GET(niv.nivel, 'julio_pagado' as char) AS julio_pagado,
				COLUMN_GET(niv.nivel, 'julio_saldo' as char) AS julio_saldo,
				COLUMN_GET(niv.nivel, 'julio_devuelto' as char) AS julio_devuelto,

				COLUMN_GET(niv.nivel, 'agosto_inicial' as char) AS agosto_inicial,
				COLUMN_GET(niv.nivel, 'agosto_compromiso' as char) AS agosto_compromiso,
				COLUMN_GET(niv.nivel, 'agosto_precompromiso' as char) AS agosto_precompromiso,
				COLUMN_GET(niv.nivel, 'agosto_devengado' as char) AS agosto_devengado,
				COLUMN_GET(niv.nivel, 'agosto_ejercido' as char) AS agosto_ejercido,
				COLUMN_GET(niv.nivel, 'agosto_pagado' as char) AS agosto_pagado,
				COLUMN_GET(niv.nivel, 'agosto_saldo' as char) AS agosto_saldo,
				COLUMN_GET(niv.nivel, 'agosto_devuelto' as char) AS agosto_devuelto,

				COLUMN_GET(niv.nivel, 'septiembre_inicial' as char) AS septiembre_inicial,
				COLUMN_GET(niv.nivel, 'septiembre_compromiso' as char) AS septiembre_compromiso,
				COLUMN_GET(niv.nivel, 'septiembre_precompromiso' as char) AS septiembre_precompromiso,
				COLUMN_GET(niv.nivel, 'septiembre_devengado' as char) AS septiembre_devengado,
				COLUMN_GET(niv.nivel, 'septiembre_ejercido' as char) AS septiembre_ejercido,
				COLUMN_GET(niv.nivel, 'septiembre_pagado' as char) AS septiembre_pagado,
				COLUMN_GET(niv.nivel, 'septiembre_saldo' as char) AS septiembre_saldo,
				COLUMN_GET(niv.nivel, 'septiembre_devuelto' as char) AS septiembre_devuelto,

				COLUMN_GET(niv.nivel, 'octubre_inicial' as char) AS octubre_inicial,
				COLUMN_GET(niv.nivel, 'octubre_compromiso' as char) AS octubre_compromiso,
				COLUMN_GET(niv.nivel, 'octubre_precompromiso' as char) AS octubre_precompromiso,
				COLUMN_GET(niv.nivel, 'octubre_devengado' as char) AS octubre_devengado,
				COLUMN_GET(niv.nivel, 'octubre_ejercido' as char) AS octubre_ejercido,
				COLUMN_GET(niv.nivel, 'octubre_pagado' as char) AS octubre_pagado,
				COLUMN_GET(niv.nivel, 'octubre_saldo' as char) AS octubre_saldo,
				COLUMN_GET(niv.nivel, 'octubre_devuelto' as char) AS octubre_devuelto,

				COLUMN_GET(niv.nivel, 'noviembre_inicial' as char) AS noviembre_inicial,
				COLUMN_GET(niv.nivel, 'noviembre_compromiso' as char) AS noviembre_compromiso,
				COLUMN_GET(niv.nivel, 'noviembre_precompromiso' as char) AS noviembre_precompromiso,
				COLUMN_GET(niv.nivel, 'noviembre_devengado' as char) AS noviembre_devengado,
				COLUMN_GET(niv.nivel, 'noviembre_ejercido' as char) AS noviembre_ejercido,
				COLUMN_GET(niv.nivel, 'noviembre_pagado' as char) AS noviembre_pagado,
				COLUMN_GET(niv.nivel, 'noviembre_saldo' as char) AS noviembre_saldo,
				COLUMN_GET(niv.nivel, 'noviembre_devuelto' as char) AS noviembre_devuelto,

				COLUMN_GET(niv.nivel, 'diciembre_inicial' as char) AS diciembre_inicial,
				COLUMN_GET(niv.nivel, 'diciembre_compromiso' as char) AS diciembre_compromiso,
				COLUMN_GET(niv.nivel, 'diciembre_precompromiso' as char) AS diciembre_precompromiso,
				COLUMN_GET(niv.nivel, 'diciembre_devengado' as char) AS diciembre_devengado,
				COLUMN_GET(niv.nivel, 'diciembre_ejercido' as char) AS diciembre_ejercido,
				COLUMN_GET(niv.nivel, 'diciembre_pagado' as char) AS diciembre_pagado,
				COLUMN_GET(niv.nivel, 'diciembre_saldo' as char) AS diciembre_saldo,
				COLUMN_GET(niv.nivel, 'diciembre_devuelto' as char) AS diciembre_devuelto,

				COLUMN_GET(niv.nivel, 'precomprometido_anual' as char) AS precomprometido_anual,
				COLUMN_GET(niv.nivel, 'comprometido_anual' as char) AS comprometido_anual,
				COLUMN_GET(niv.nivel, 'devengado_anual' as char) AS devengado_anual,
				COLUMN_GET(niv.nivel, 'ejercido_anual' as char) AS ejercido_anual,
				COLUMN_GET(niv.nivel, 'pagado_anual' as char) AS pagado_anual,
				COLUMN_GET(niv.nivel, 'total_anual' as char) AS total_anual
FROM cat_niveles niv
WHERE COLUMN_GET(niv.nivel, 'fuente_de_financiamiento' as char) != ''
						AND COLUMN_GET(niv.nivel, 'programa_de_financiamiento' as char) != ''
						AND COLUMN_GET(niv.nivel, 'centro_de_costos' as char) != ''
						AND COLUMN_GET(niv.nivel, 'capitulo' as char) != ''
						AND COLUMN_GET(niv.nivel, 'concepto' as char) != ''
						AND COLUMN_GET(niv.nivel, 'partida' as char) != ''
						-- AND COLUMN_GET(niv.nivel, 'enero_inicial' as char) != 0
						-- AND COLUMN_GET(niv.nivel, 'febrero_inicial' as char) != 0
						-- AND COLUMN_GET(niv.nivel, 'marzo_inicial' as char) != 0
						-- AND COLUMN_GET(niv.nivel, 'abril_inicial' as char) != 0
						-- AND COLUMN_GET(niv.nivel, 'mayo_inicial' as char) != 0
						-- AND COLUMN_GET(niv.nivel, 'junio_inicial' as char) != 0
						-- AND COLUMN_GET(niv.nivel, 'julio_inicial' as char) != 0
						-- AND COLUMN_GET(niv.nivel, 'agosto_inicial' as char) != 0
						-- AND COLUMN_GET(niv.nivel, 'septiembre_inicial' as char) != 0
						-- AND COLUMN_GET(niv.nivel, 'octubre_inicial' as char) != 0
						-- AND COLUMN_GET(niv.nivel, 'noviembre_inicial' as char) != 0
						-- AND COLUMN_GET(niv.nivel, 'diciembre_inicial' as char) != 0
ORDER BY id_nivel, texto
                ";
        $query = $this->db->query( $sql);
//        echo "Total Elemetos [" . $query->num_rows . "]";
        return $query->result_array();
    }

    function get_precompromisos_autorizados($datos) {
        $sql = "SELECT mpc.tipo_requisicion, mpc.fecha_emision AS fecha_emision_pre, mpc.fecha_autoriza AS fecha_autoriza_pre, mpc.creado_por,
                mpd.numero_pre, mpd.subtotal, mpd.importe,
                COLUMN_GET(mpd.nivel, 'fuente_de_financiamiento' as char) AS fuente_de_financiamiento,
                COLUMN_GET(mpd.nivel, 'programa_de_financiamiento' as char) AS programa_de_financiamiento,
                COLUMN_GET(mpd.nivel, 'centro_de_costos' as char) AS centro_de_costos,
                COLUMN_GET(mpd.nivel, 'capitulo' as char) AS capitulo,
                COLUMN_GET(mpd.nivel, 'concepto' as char) AS concepto,
                COLUMN_GET(mpd.nivel, 'partida' as char) AS partida,
                mpc.estatus AS estatus_precompromiso,
                '' numero_compromiso,
                '' numero_precompromiso,
                '' importe_compromiso,
                '' fuente_compromiso,
                '' programa_compromiso,
                '' centro_costos_compromiso,
                '' capitulo_compromiso,
                '' concepto_compromiso,
                '' partida_compromiso,
                '' estatus_compromiso
                FROM mov_precompromiso_detalle AS mpd
                INNER JOIN mov_precompromiso_caratula AS mpc
                ON mpd.numero_pre = mpc.numero_pre
                WHERE mpc.enfirme = 1
                AND mpc.firma1 = 1
                AND mpc.firma2 = 1
                AND mpc.firma3 = 1
                AND mpc.estatus != 'cancelado'
                AND mpc.estatus != 'Terminado'
                AND mpc.fecha_autoriza >= ?
                AND mpc.fecha_autoriza <= ?

                UNION ALL

                SELECT '' tipo_requisicion,
                '' fecha_emision_pre,
                '' fecha_autoriza_pre,
                '' creado_por,
                '' numero_pre,
                '' subtotal,
                '' importe,
                '' fuente_de_financiamiento,
                '' programa_de_financiamiento,
                '' centro_de_costos,
                '' capitulo,
                '' concepto,
                '' partida,
                '' estatus_precompromiso,
                mcd.numero_compromiso,
                mcc.num_precompromiso AS numero_precompromiso,
                mcd.importe AS importe_compromiso,
                COLUMN_GET(mcd.nivel, 'fuente_de_financiamiento' as char) AS fuente_compromiso,
                COLUMN_GET(mcd.nivel, 'programa_de_financiamiento' as char) AS programa_compromiso,
                COLUMN_GET(mcd.nivel, 'centro_de_costos' as char) AS centro_costos_compromiso,
                COLUMN_GET(mcd.nivel, 'capitulo' as char) AS capitulo_compromiso,
                COLUMN_GET(mcd.nivel, 'concepto' as char) AS concepto_compromiso,
                COLUMN_GET(mcd.nivel, 'partida' as char) AS partida_compromiso,
                mcc.estatus AS estatus_compromiso
                FROM mov_compromiso_detalle AS mcd
                INNER JOIN mov_compromiso_caratula AS mcc
                ON mcd.numero_compromiso = mcc.numero_compromiso
                WHERE mcc.enfirme = 1
                AND mcc.firma1 = 1
                AND mcc.firma2 = 1
                AND mcc.firma3 = 1
                AND mcc.estatus != 'cancelado'
                AND mcc.num_precompromiso != ''
                OR mcc.num_precompromiso != 0;";
        $query = $this->db->query($sql, array($datos["fecha_inicial"], $datos["fecha_final"]));
        return $query->result_array();
    }
    function get_precompromisos_autorizados_estructura($precompromiso) {
        $sql = "SELECT numero_pre, COLUMN_JSON(nivel) AS niveles, subtotal, importe FROM mov_precompromiso_detalle WHERE numero_pre = ?;";
        $query = $this->db->query($sql, $precompromiso);
        return $query->result();
    }


    /*Provicional Reportes Compromisos*/
    function get_compromisos_autorizados() {
        $sql = "SELECT numero_compromiso, num_precompromiso, nombre_proveedor, nombre_completo, fecha_emision, fecha_autoriza, tipo_compromiso, total
                FROM mov_compromiso_caratula
                WHERE enfirme = 1
                AND firma1 = 1
                AND firma2 = 1
                AND firma3 = 1
                AND cancelada = 0
                AND fecha_emision >= '2015-01-01'
                AND fecha_emision <= '2015-03-31'
                GROUP BY numero_compromiso;";
        $query = $this->db->query($sql);
        return $query->result();
    }
    function get_compromisos_autorizados_estructura($compromiso) {
        $sql = "SELECT numero_compromiso, COLUMN_JSON(nivel) AS niveles, subtotal, importe FROM mov_compromiso_detalle
                WHERE numero_compromiso = ?;";
        $query = $this->db->query($sql, $compromiso);
        return $query->result();
    }

    function get_partidas_egresos() {
        $sql = "SELECT id_niveles AS ID, COLUMN_JSON(nivel) AS estructura
                FROM cat_niveles
                WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) != ''
                AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) != ''
                AND COLUMN_GET(nivel, 'centro_de_costos' as char) != ''
                AND COLUMN_GET(nivel, 'capitulo' as char) != ''
                AND COLUMN_GET(nivel, 'concepto' as char) != ''
                AND COLUMN_GET(nivel, 'partida' as char) != '';";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_partidas_ingresos() {
        $sql = "SELECT id_niveles AS ID, COLUMN_JSON(nivel) AS estructura
                FROM cat_niveles
                WHERE COLUMN_GET(nivel, 'gerencia' as char) != ''
                AND COLUMN_GET(nivel, 'centro_de_recaudacion' as char) != ''
                AND COLUMN_GET(nivel, 'rubro' as char) != ''
                AND COLUMN_GET(nivel, 'tipo' as char) != ''
                AND COLUMN_GET(nivel, 'clase' as char) != '';";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_adecuaciones_egresos_por_partida($datos) {
        $sql = "SELECT md.mes_destino, md.total, md.texto
                            FROM mov_adecuaciones_egresos_detalle md
                            JOIN mov_adecuaciones_egresos_caratula mc
                            ON md.numero_adecuacion = mc.numero
                            WHERE COLUMN_GET(md.estructura, 'fuente_de_financiamiento' as char) = ?
                            AND COLUMN_GET(md.estructura, 'programa_de_financiamiento' as char) = ?
                            AND COLUMN_GET(md.estructura, 'centro_de_costos' as char) = ?
                            AND COLUMN_GET(md.estructura, 'capitulo' as char) = ?
                            AND COLUMN_GET(md.estructura, 'concepto' as char) = ?
                            AND COLUMN_GET(md.estructura, 'partida' as char) = ?
                            AND mc.enfirme = 1
                            AND mc.cancelada != 1;";
        $query = $this->db->query($sql, array($datos["nivel1"], $datos["nivel2"], $datos["nivel3"], $datos["nivel4"], $datos["nivel5"], $datos["nivel6"]));
//        $this->debugeo->imprimir_pre($query->result_array());
        return $query->result_array();
    }

    function get_adecuaciones_ingresos_por_partida($datos) {
        $sql = "SELECT md.mes_destino, md.total, md.texto
                            FROM mov_adecuaciones_ingresos_detalle md
                            JOIN mov_adecuaciones_ingresos_caratula mc
                            ON md.numero_adecuacion = mc.numero
                            WHERE COLUMN_GET(md.estructura, 'gerencia' as char) = ?
                            AND COLUMN_GET(md.estructura, 'centro_de_recaudacion' as char) = ?
                            AND COLUMN_GET(md.estructura, 'rubro' as char) = ?
                            AND COLUMN_GET(md.estructura, 'tipo' as char) = ?
                            AND COLUMN_GET(md.estructura, 'clase' as char) = ?
                            AND mc.enfirme = 1
                            AND mc.cancelada != 1;";
        $query = $this->db->query($sql, array($datos["nivel1"], $datos["nivel2"], $datos["nivel3"], $datos["nivel4"], $datos["nivel5"]));
//        $this->debugeo->imprimir_pre($query->result_array());
        return $query->result_array();
    }




    function get_partidas_por_mes($mes, $fechas) {
        $sql = "SELECT id_niveles, COLUMN_JSON(nivel) AS estructura,
                COLUMN_GET(nivel, '".$mes."_inicial' as char) AS mes_inicial,
                COLUMN_GET(nivel, '".$mes."_compromiso' as char) AS mes_compromiso,
				COLUMN_GET(nivel, '".$mes."_precompromiso' as char) AS mes_precompromiso,
				COLUMN_GET(nivel, '".$mes."_devengado' as char) AS mes_devengado,
				COLUMN_GET(nivel, '".$mes."_ejercido' as char) AS mes_ejercido,
				COLUMN_GET(nivel, '".$mes."_saldo' as char) AS saldo,
				COLUMN_GET(nivel, 'precomprometido_anual' as char) AS precomprometido_anual,
				COLUMN_GET(nivel, 'comprometido_anual' as char) AS comprometido_anual,
				COLUMN_GET(nivel, 'total_anual' as char) AS total_anual
                FROM cat_niveles
                WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) != ''
                AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) != ''
                AND COLUMN_GET(nivel, 'centro_de_costos' as char) != ''
                AND COLUMN_GET(nivel, 'capitulo' as char) != ''
                AND COLUMN_GET(nivel, 'concepto' as char) != ''
                AND COLUMN_GET(nivel, 'partida' as char) != '';";
        $query = $this->db->query($sql);
        return $query->result();
    }
}