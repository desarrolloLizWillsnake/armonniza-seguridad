<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contabilidad_model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }
    /**/
    function datosProveedores() {
        $this->db->select('id_proveedores, nombre, nombref');
        $query = $this->db->get('cat_proveedores');
        $result = $query->result();
        return $result;
    }

    /**
     * Aqui empieza la seeción de Polizas
     */
    function ultima_poliza() {
        $sql = "SELECT numero_pre as ultimo FROM mov_precompromiso_caratula ORDER BY numero_pre DESC LIMIT 1;";
        $query = $this->db->query($sql);
        $row = $query->row();
        if(!$row) {
            $row = 0;
        }
        return $row;
    }

    function datosCaratulaPolizas() {
        $sql = "SELECT * FROM mov_polizas_cabecera ORDER BY numero_poliza DESC;";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    function datosCaratulaPolizasDiario() {
        $sql = "SELECT * FROM mov_polizas_cabecera WHERE tipo_poliza =  'Diario' ORDER BY numero_poliza DESC;";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    function datosCaratulaPolizasIngresos() {
        $sql = "SELECT * FROM mov_polizas_cabecera WHERE tipo_poliza =  'Ingresos' ORDER BY numero_poliza DESC;";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    function datosCaratulaPolizasEgresos() {
        $sql = "SELECT * FROM mov_polizas_cabecera WHERE tipo_poliza =  'Egresos' ORDER BY numero_poliza DESC;";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    function get_datos_polizas_caratula($query, $poliza) {
        $query = $this->db->query($query, array($poliza));
        $result = $query->row();
        return $result;
    }

    function getdatosDetallePolizas($poliza){
        $sql = "SELECT p1.*, COLUMN_GET(p2.nivel, 'partida' as char) AS partida, COLUMN_GET(p2.nivel, 'descripcion' as char) AS descripcion
                  FROM mov_poliza_detalle p1
                  JOIN cat_niveles p2
                  WHERE p1.partida = COLUMN_GET(p2.nivel, 'partida' as char)
                  AND p1.numero_poliza = ?
                  GROUP BY p1.id_poliza_detalle;";
        $query = $this->db->query($sql, array($poliza));
        $result = $query->result();
        return $result;
    }

    function getdatosDetallePolizasIngresos($poliza){
        $sql = "SELECT p1.*, COLUMN_GET(p2.nivel, 'clase' as char) AS partida, COLUMN_GET(p2.nivel, 'descripcion' as char) AS descripcion
                  FROM mov_poliza_detalle p1
                  JOIN cat_niveles p2
                  WHERE p1.partida = COLUMN_GET(p2.nivel, 'clase' as char)
                  AND p1.numero_poliza = ?
                  GROUP BY p1.id_poliza_detalle;";
        $query = $this->db->query($sql, array($poliza));
        $result = $query->result();
        return $result;
    }

    function getdatosDetallePolizasDirectas($poliza){
        $sql = "SELECT * FROM mov_poliza_detalle WHERE numero_poliza = ? GROUP BY id_poliza_detalle;";
        $query = $this->db->query($sql, array($poliza));
        $result = $query->result();
        return $result;
    }

    function getdatosDetallePolizasPartida($poliza){
        $sql = "SELECT numero_poliza, partida FROM mov_poliza_detalle WHERE numero_poliza = ? GROUP BY id_poliza_detalle;";
        $query = $this->db->query($sql,$poliza);
        $result = $query->row();
        return $result;
    }

    function apartarPoliza($ultimo) {
        $id = $this->tank_auth->get_user_id();

        $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
        $resultado_query_usuario = $this->db->query($query_usuario, array($id));
        $nombre_encontrado = $resultado_query_usuario->row();
        $nombre_completo = $nombre_encontrado->nombre." ".$nombre_encontrado->apellido_paterno." ".$nombre_encontrado->apellido_materno;

        $data = array(
            'numero_poliza' => $ultimo,
            'estatus' => "espera",
            'creado_por' => $nombre_completo,
        );

        $query = $this->db->insert('mov_polizas_cabecera', $data);

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function insertar_detalle_poliza($datos) {
        $nivel = explode(".", $datos["cuenta"]);

        $data = array(
            'numero_poliza' => $datos['ultima_poliza'],
            'tipo_poliza' => $datos['tipo_poliza'],
            'fecha' => $datos['fecha'],
            'hora' => date("H:i:s"),
            'fecha_real' =>date("Y-m-d"),
            'cuenta' => $datos['cuenta'],
            'nivel' => count($nivel),
            'concepto' => $datos['concepto'],
            'debe' => $datos['debe'],
            'haber' => $datos['haber'],
            'subsidio' => $datos['subsidio'],
            'nombre' => $datos['descripcion_cuenta'],
            'centro_costo' => $datos['centro_costos'],
//            'partida' => $datos['ultima_poliza'],
        );

        $query = $this->db->insert('mov_poliza_detalle', $data);

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function datos_polizaDetalle($poliza) {
        $this->db->select('id_poliza_detalle, numero_poliza, fecha, cuenta, debe, haber, subsidio, nombre, centro_costo, partida')->from('mov_poliza_detalle')->where('numero_poliza', $poliza);
        $query = $this->db->get();
        return $query->result();
    }

    function editar_detalle_partida($datos) {
        $data = array(
            'cuenta' => $datos['editar_cuenta_poliza'],
            'centro_costo' => $datos['editar_centro_costo_poliza'],
            'subsidio' => $datos['editar_subsidio_poliza'],
            'concepto' => $datos['editar_concepto_poliza'],
            'debe' => $datos['editar_debe_poliza'],
            'haber' => $datos['editar_haber_poliza'],
        );

        $this->db->where('id_poliza_detalle', $datos['editar_id_detalle_poliza']);
        $query = $this->db->update('mov_poliza_detalle', $data);

        if($query) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function actualizar_poliza_caratula($datos) {
        $id = $this->tank_auth->get_user_id();

        $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
        $resultado_query_usuario = $this->db->query($query_usuario, array($id));
        $nombre_encontrado = $resultado_query_usuario->row();
        $nombre_completo = $nombre_encontrado->nombre." ".$nombre_encontrado->apellido_paterno." ".$nombre_encontrado->apellido_materno;


        $this->db->select('COUNT(numero_poliza) AS conteo')->from('mov_poliza_detalle')->where('id_poliza_detalle', $datos['ultima_poliza']);
        $query = $this->db->get();
        $partidas = $query->row();

        $data = array(
            'enfirme' => $datos['check_firme'],
            'tipo_poliza' => $datos['tipo_poliza'],
            'importe' => $datos['importe'],
            'concepto_especifico' =>  $datos['concepto_especifico'],
            'concepto' => $datos['concepto'],
            'fecha' => $datos['fecha'],
            'fecha_real' => date("Y-m-d H:i:s"),
            'id_proveedor' => $datos['id_proveedor'],
            'proveedor' => $datos['proveedor'],
            'no_movimiento' => $datos['no_movimiento'],
            'no_partidas' => $partidas->conteo,
            'creado_por' => $nombre_completo,
            'estatus' => $datos['estatus'],
            'cargos' => $datos['debe_hidden'],
            'abonos' => $datos['haber_hidden'],
        );

        $this->db->where('numero_poliza', $datos['ultima_poliza']);
        $query = $this->db->update('mov_polizas_cabecera', $data);

        if($query) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function get_polizas_excel($datos) {
        $sql = 'SELECT mpd.numero_poliza, mpd.subsidio, mpc.fecha, mpc.no_movimiento, mpd.tipo_poliza,
                mpd.centro_costo, mpd.partida, mpc.proveedor, mpc.cliente, mpc.contrarecibo, mpc.movimiento,
                mpc.no_devengado, mpc.no_recaudado, mpc.concepto, mpd.cuenta, mpd.nombre, mpd.debe, mpd.haber,
                mpc.cargos, mpc.abonos
                FROM mov_poliza_detalle mpd
                JOIN mov_polizas_cabecera mpc
                ON mpd.numero_poliza = mpc.numero_poliza
                WHERE mpc.fecha >= ?
                AND mpc.fecha <= ?
                AND mpc.estatus LIKE ?
                AND mpc.tipo_poliza LIKE ?;';
        $query = $this->db->query($sql, array(
            $datos["fecha_inicial"],
            $datos["fecha_final"],
            "%".$datos["estatus_poliza"]."%",
            "%".$datos["tipo_poliza"]."%",
            ));
        return $query->result_array();
    }

    /*Matriz*/

    function verificar_partida($partida) {
        $sql = "SELECT COLUMN_GET(nivel, 'partida' as char) AS partida,
                    COLUMN_GET(nivel, 'descripcion' as char) AS descripcion
                    FROM cat_niveles
                        WHERE COLUMN_GET(nivel, 'partida' as char) = ?;";
        $query = $this->db->query($sql, array($partida));
        return $query->row_array(); 
    }

    function verificar_centro_recaudacion($centro_recaudacion) {
        $sql = "SELECT COLUMN_GET(nivel, 'centro_de_recaudacion' as char) AS centro_de_recaudacion,
                    COLUMN_GET(nivel, 'descripcion' as char) AS descripcion
                    FROM cat_niveles
                        WHERE COLUMN_GET(nivel, 'centro_de_recaudacion' as char) = ?;";
        $query = $this->db->query($sql, array($centro_recaudacion));
        return $query->row_array(); 
    }

    function verificar_cuenta_cargo($cuenta) {
        $sql = "SELECT cuenta, descripcion FROM cat_cuentas_contables WHERE cuenta = ?;";
        $query = $this->db->query($sql, array($cuenta));
        return $query->row_array(); 
    }

    function verificar_cuenta_abono($cuenta) {
        $sql = "SELECT cuenta, descripcion FROM cat_cuentas_contables WHERE cuenta = ?;";
        $query = $this->db->query($sql, array($cuenta));
        return $query->row_array(); 
    }

    function insertar_matriz($datos) {
        
        $this->db->trans_begin();

        $datos_insertar = array(
           'clave' => $datos["partida"],
           'descripcion' => $datos["descripcion_partida"],
           'cuenta_cargo' => $datos["cuenta_cargo"],
           'nombre_cargo' => $datos["descripcion_cuenta_cargo"],
           'cuenta_abono' => $datos["cuenta_abono"],
           'nombre_abono' => $datos["descripcion_cuenta_abono"],
           'tipo_gasto' => 1,
           'destino' => $datos["D"],
        );

        $this->db->insert('cat_correlacion_partidas_contables', $datos_insertar);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        }
        else {
            $this->db->trans_commit();
            return TRUE;
        }

    }

    function get_tabla_matriz() {
        $query = "SELECT * FROM cat_correlacion_partidas_contables;";
        $query = $this->db->query($query);
        return $query->result();
    }

    function borrar_matriz($matriz) {
        $this->db->where('id_correlacion_partidas_contables', $matriz);
        $query = $this->db->delete('cat_correlacion_partidas_contables');
        if($query) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function get_tabla_cog() {
        $query = "SELECT * FROM cat_partidas_presupuestales;";
        $query = $this->db->query($query);
        return $query->result();
    }

    function tomar_datos_COG($cog) {
        $query = "SELECT * FROM cat_partidas_presupuestales WHERE id_partidas_presupuestales = ?;";
        $query = $this->db->query($query, array($cog));
        return $query->row();
    }

    function get_tabla_cri() {
        $query = "SELECT * FROM cat_maestro WHERE tipo = '(CRI) Clasificador  ';";
        $query = $this->db->query($query);
        return $query->result();
    }

    function tomar_datos_CRI($cri) {
        $query = "SELECT * FROM cat_maestro WHERE tipo = '(CRI) Clasificador  ' AND id_maestro = ?;";
        $query = $this->db->query($query, array($cri));
        return $query->row();
    }

    function get_tabla_cuentas() {
        $query = "SELECT * FROM cat_cuentas_contables;";
        $query = $this->db->query($query);
        return $query->result();
    }

    function tomar_datos_cuentas($cuenta) {
        $query = "SELECT * FROM cat_cuentas_contables WHERE id_cuentas_contables = ?;";
        $query = $this->db->query($query, array($cuenta));
        return $query->row();
    }

    function insertar_matriz_individual($datos) {
        $data = array(
            "clave" => $datos["clave"],
            "descripcion" => $datos["titulo_clave"],
            "cuenta_cargo" => $datos["cuenta_cargo"],
            "nombre_cargo" => $datos["descripcion_cuenta_cargo"],
            "cuenta_abono" => $datos["cuenta_abono"],
            "nombre_abono" => $datos["descripcion_cuenta_abono"],
            "tipo_gasto" => $datos["tipo_gasto"],
            "destino" => $datos["destino"],
            "caracteristicas" => $datos["caracteristicas"],
            "medio" => $datos["medio"],
        );

        $query = $this->db->insert('cat_correlacion_partidas_contables', $data);

        if($query) {
            return TRUE;
        } else {
            return FALSE;
        }

    }

    function get_datos_matriz($matriz) {
        $query = $this->db->get_where('cat_correlacion_partidas_contables', array('id_correlacion_partidas_contables' => $matriz));
        return $query->row();
    }

    function actualizar_matriz($datos) {
        $data = array(
            "clave" => $datos["clave"],
            "descripcion" => $datos["titulo_clave"],
            "cuenta_cargo" => $datos["cuenta_cargo"],
            "nombre_cargo" => $datos["descripcion_cuenta_cargo"],
            "cuenta_abono" => $datos["cuenta_abono"],
            "nombre_abono" => $datos["descripcion_cuenta_abono"],
            "tipo_gasto" => $datos["tipo_gasto"],
            "destino" => $datos["destino"],
            "caracteristicas" => $datos["caracteristicas"],
            "medio" => $datos["medio"],
        );

        $this->db->where('id_correlacion_partidas_contables', $datos["matriz"]);
        $query = $this->db->update('cat_correlacion_partidas_contables', $data);

        if($query) {
            return TRUE;
        } else {
            return FALSE;
        }

    }

    /*Balanza*/
    function datos_subsidio() {
        $sql = "SELECT * FROM cat_clasificador_fuentes_financia;";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    function datos_balanza($datos){
        $this->db->select('id_cuentas_contables')->from('cat_cuentas_contables')->where('cuenta', $datos['cuenta_inicial']);
        $query = $this->db->get();
        $cuenta_inicial = $query->row();

        $this->debugeo->imprimir_pre($cuenta_inicial);

        $this->db->select('id_cuentas_contables')->from('cat_cuentas_contables')->where('cuenta', $datos['cuenta_final']);
        $query = $this->db->get();
        $cuenta_final = $query->row();

        $this->debugeo->imprimir_pre($cuenta_final);

        $sql = "SELECT pd.*
                    FROM mov_poliza_detalle pd
                    JOIN mov_polizas_cabecera pc
                    ON pd.numero_poliza = pc.numero_poliza
                    WHERE pd.fecha >= ?
                    AND pd.fecha <= ?
                    AND pd.subsidio = ?
                    AND pd.centro_costo = ?
                    AND pd.id_cuenta >= ?
                    AND pd.id_cuenta <= ?
                    AND pc.enfirme = 1";
        $query = $this->db->query($sql, array($datos['fecha_inicial'], $datos['fecha_final'], $datos['subsidio'], $datos['centro_costos'], $cuenta_inicial->id_cuentas_contables, $cuenta_final->id_cuentas_contables));
        $result = $query->result();
        return $result;
    }

    function datos_cuenta($cuenta) {
        $this->db->select('nombre')->from('cat_cuentas_contables')->where('cuenta', $cuenta);
        $query = $this->db->get();
        return $query->row_array();
    }

    function obtener_datos_balanza() {
        $sql = "SELECT * FROM balanza_temporal;";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*Auxiliar*/
    function get_datos_polizas_detalle($query, $cuenta_inicial, $cuenta_final, $datos) {
        $query = $this->db->query($query, array($cuenta_inicial, $cuenta_final, $datos["fecha_inicial"], $datos["fecha_final"]));
        $result = $query->result();
        return $result;
    }

  /*  function get_datos_polizas_detalle($cuenta, $datos) {
        $sql = "SELECT pd.*, pc.fecha AS fecha_caratula FROM mov_poliza_detalle pd
                    INNER JOIN mov_polizas_cabecera pc ON pd.numero_poliza = pc.numero_poliza
                    WHERE pd.cuenta LIKE ?
                    AND pc.enfirme = 1
                    AND pc.cancelada = 0
                    AND pc.fecha >= ?
                    AND pc.fecha <= ?;";
        $query = $this->db->query($sql,array($cuenta."%",$datos['fecha_inicial'],$datos['fecha_final']));
        $result = $query->result();
        return $result;
    }
  */
    function  get_datos_cuenta($cuenta_inicial){
        $sql = "SELECT * FROM balanza_temporal WHERE cuenta = ?;";
        $query = $this->db->query($sql, $cuenta_inicial);
        return $query->row();
    }

    /*Periodos Contables*/
    function datos_periodos_caratula(){
        $sql = "SELECT * FROM mov_polizas_periodos;";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }

    function insertar_periodo($datos) {

        $datos_insertar = array(
            'year' => $datos["year"],
            'fecha_inicia' => $datos["fecha_inicia"],
            'fecha_termina' => $datos["fecha_termina"],
            'cerrado' => 1,
            'salario_anterior' => round($datos["salario_anterior"], 2),
            'debe' => round($datos["debe"], 2),
            'haber' => round($datos["haber"], 2),
            'salario_actual' => round($datos["salario_actual"], 2),
        );

        $query = $this->db->insert('mov_polizas_periodos', $datos_insertar);
        return $query;

    }

    function editar_periodo($datos) {

        $datos_actualizar = array(
            'cerrado' => $datos["cerrado"],
        );

        $this->db->where('id_polizas_periodos', $datos["periodo"]);
        $query = $this->db->update('mov_polizas_periodos', $datos_actualizar);
        return $query;
    }

    function datos_cuentas_afectadas($datos) {
        $sql = "SELECT * FROM periodos_cuentas_afectadas WHERE fecha_inicial = ? AND fecha_final = ?;";
        $query = $this->db->query($sql, array($datos["fecha_inicial"], $datos["fecha_final"]));
        $result = $query->result_array();
        return $result;
    }

    function tomar_periodos_contables() {
//        Se hace una consulta a la base de datos, tomando todos los periodos contables
        $this->db->select('fecha_inicia, fecha_termina, cerrado')->from('mov_polizas_periodos');
        $query = $this->db->get();
//        Se devuelve un arreglo de datos
        return $query->result_array();
    }

    function get_polizas_cierre_anual_detalle() {
        $this->db->select('*')->from('poliza_detalle_cierre_anual')->where('numero_poliza', 1);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_polizas_cierre_anual_total() {
        $this->db->select('*')->from('poliza_detalle_cierre_anual')->where('numero_poliza', 2);
        $query = $this->db->get();
        return $query->result_array();
    }

}