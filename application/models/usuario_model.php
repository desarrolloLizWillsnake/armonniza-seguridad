<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuario_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_datos_usuario() {
        $this->db->select('*')->from('datos_usuario')->where('id_datos_usuario', $this->tank_auth->get_user_id());
        $query = $this->db->get();
        return $query->row();
    }

    function actualizar_datos($datos) {
        $data = array(
            'nombre' => $datos["nombre"],
            'apellido_paterno' => $datos["a_paterno"],
            'apellido_materno' => $datos["a_materno"],
            'puesto' => $datos["puesto"],
        );

        $this->db->where('id_datos_usuario', $datos["id_datos_usuario"]);
        $query = $this->db->update('datos_usuario', $data);
        if($query) {
            return TRUE;
        }
        return FALSE;
    }

}