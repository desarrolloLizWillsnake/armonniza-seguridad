<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Utilerias {

    function __construct() {
        $this->ci =& get_instance();
    }

    function convertirFechaAMes($fecha) {

        $fecha_mes = explode("-", $fecha);

        switch($fecha_mes[1]) {
            case 1:
                $mes = 'enero';
                break;

            case 2:
                $mes = 'febrero';
                break;

            case 3:
                $mes = 'marzo';
                break;

            case 4:
                $mes = 'abril';
                break;

            case 5:
                $mes = 'mayo';
                break;

            case 6:
                $mes = 'junio';
                break;

            case 7:
                $mes = 'julio';
                break;

            case 8:
                $mes = 'agosto';
                break;

            case 9:
                $mes = 'septiembre';
                break;

            case 10:
                $mes = 'octubre';
                break;

            case 11:
                $mes = 'noviembre';
                break;

            case 12:
                $mes = 'diciembre';
                break;
        }

        return $mes;
    }

    function traducir_mes($mes) {

        switch($mes) {
            case "January":
                $mes = 'enero';
                break;

            case "February":
                $mes = 'febrero';
                break;

            case "March":
                $mes = 'marzo';
                break;

            case "April":
                $mes = 'abril';
                break;

            case "May":
                $mes = 'mayo';
                break;

            case "June":
                $mes = 'junio';
                break;

            case "July":
                $mes = 'julio';
                break;

            case "August":
                $mes = 'agosto';
                break;

            case "September":
                $mes = 'septiembre';
                break;

            case "October":
                $mes = 'octubre';
                break;

            case "November":
                $mes = 'noviembre';
                break;

            case "December":
                $mes = 'diciembre';
                break;
        }

        return $mes;

    }

    function preparar_mes_actual() {
        $mes = date("F");

        switch($mes) {
            case "January":
                $mes = 'enero';
                break;

            case "February":
                $mes = 'febrero';
                break;
            
            case "March":
                $mes = 'marzo';
                break;

            case "April":
                $mes = 'abril';
                break;

            case "May":
                $mes = 'mayo';
                break;

            case "June":
                $mes = 'junio';
                break;

            case "July":
                $mes = 'julio';
                break;

            case "August":
                $mes = 'agosto';
                break;

            case "September":
                $mes = 'septiembre';
                break;

            case "October":
                $mes = 'octubre';
                break;

            case "November":
                $mes = 'noviembre';
                break;

            case "December":
                $mes = 'diciembre';
                break;

        }

        return $mes;
    }

    function get_grupo() {
        $id = $this->ci->tank_auth->get_user_id();
        $sql = "SELECT id_grupo as grupo FROM grupos_usuarios WHERE id_usuario = ?;";
        $query = $this->ci->db->query($sql, array($id));
        $row = $query->row();
        return $row->grupo;
    }

    function get_permisos($modulo) {
        $id = $this->ci->tank_auth->get_user_id();
        $sql = "SELECT modulo FROM permisos WHERE id_usuario = ? AND modulo = ? AND activo = 1;";
        $query = $this->ci->db->query($sql, array($id, $modulo));
        $row = $query->row();
        if($row) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function get_firmas() {
        $id = $this->ci->tank_auth->get_user_id();
        $sql = "SELECT firma FROM firmas WHERE id_usuario = ?;";
        $query = $this->ci->db->query($sql, array($id));
        $row = $query->row();
        if($row) {
            return $row->firma;
        }
        else {
            return NULL;
        }
    }

    function get_firmas_arreglo() {
        $id = $this->ci->tank_auth->get_user_id();
        $sql = "SELECT firma FROM firmas WHERE id_usuario = ?;";
        $query = $this->ci->db->query($sql, array($id));
        $row = $query->result();
        if($row) {
            return $row;
        }
        else {
            return NULL;
        }
    }

    function puede_firmar() {
        $id = $this->ci->tank_auth->get_user_id();
        $sql = "SELECT firma FROM firmas WHERE id_usuario = ?;";
        $query = $this->ci->db->query($sql, array($id));
        if ($query->num_rows() > 0) {
            return TRUE;
        }
        else {
            return NULL;
        }
    }

    function generar_poliza_diario_egresos($numero) {

//        $this->debugeo->imprimir_pre($value);

        try {
            $id = $this->ci->tank_auth->get_user_id();

            $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
            $resultado_query_usuario = $this->ci->db->query($query_usuario, array($id));
            $nombre_encontrado = $resultado_query_usuario->row();
            $nombre_completo = $nombre_encontrado->nombre." ".$nombre_encontrado->apellido_paterno." ".$nombre_encontrado->apellido_materno;

//        Se toma el numero del contrarecibo que se va a buscar
            $contrarecibo = $numero;

//        Se inicizaliza la variable con el ultimo valor de las polizas
            $last = 0;
//        Se toma el numero de la ultima poliza
            $ultimo = $this->ci->ciclo_model->ultima_poliza();

//        Se le suma uno al ultimo valor de las polizas
            if($ultimo) {
                $last = $ultimo->ultimo + 1;
            }
//        De lo contrario se inicia en 1
            else {
                $last = 1;
            }

//        Se prepara el query para llamar todos los datos de la caratula del contrarecibo
            $query_caratula = "SELECT * FROM mov_contrarecibo_caratula WHERE id_contrarecibo_caratula = ?;";

//        Se llama a la funci�n que se encarga de tomar todos los datos de la caratula del contrarecibo
            $datos_caratula_contrarecibo = $this->ci->ciclo_model->get_datos_contrarecibo_caratula($contrarecibo, $query_caratula);

//            $this->debugeo->imprimir_pre($datos_caratula_contrarecibo);

//        Se prepara el query para tomar los datos del detalle de compromiso que est�n ligados al contrarecibo
            $query_detalle_compromiso = "SELECT *, COLUMN_JSON(nivel) AS estructura FROM mov_compromiso_detalle WHERE numero_compromiso = ?;";

//        Se llama ala funcion que se encarga de tomar todos los datos del detalle del compromiso que est� ligado con el contrarecibo
            $datos_detalle_compromiso = $this->ci->ciclo_model->datos_compromisoDetalle($datos_caratula_contrarecibo->numero_compromiso, $query_detalle_compromiso);

//        Se crea un arreglo donde se van a guardar los resultados del detalle del compromiso
            $datos_detalle_poliza = array();

            if (!$datos_detalle_compromiso) {
                throw new Exception('No hay partidas dentro del compromiso.');
            }

            $centro_costo = '';
            $partida = '';
            $nombre_fuente_financiamiento = '';
            $total_filas = 0;
            $cargos = 0;
            $abonos = 0;
            $total_nomina = FALSE;
            $resultado_insertar_caratula_query = FALSE;
            $resultado_insertar_abono_query = FALSE;
            $resultado_insertar_cargo_query = FALSE;

            $this->ci->db->trans_begin();

            $query_insertar_caratula_poliza = "INSERT INTO mov_polizas_cabecera (numero_poliza, tipo_poliza, fecha, fecha_real, concepto, enfirme, sifirme, no_partidas, importe, cancelada, estatus, creado_por, autorizada, cargos, abonos, id_proveedor, proveedor, contrarecibo, concepto_especifico, no_movimiento) VALUES (?, ?, ?, NOW(), ?, 0, 0, ?, ?, 0, ?, ?, 0, ?, ?, ?, ?, ?, ?, ?);";
            $datos_caratula_poliza = array(
                $last,
                'Diario',
                $datos_caratula_contrarecibo->fecha_emision,
                $datos_caratula_contrarecibo->concepto,
                0,
                $datos_caratula_contrarecibo->importe,
                'espera',
                $nombre_completo,
                0,
                0,
                $datos_caratula_contrarecibo->id_proveedor,
                $datos_caratula_contrarecibo->proveedor,
                $datos_caratula_contrarecibo->id_contrarecibo_caratula,
                $datos_caratula_contrarecibo->concepto_especifico,
                $datos_caratula_contrarecibo->documento,
            );

            $resultado_insertar_caratula_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_caratula_poliza, $datos_caratula_poliza);

            $datos_actualizar_poliza = array(
                'poliza' => 1,
            );

            $this->ci->db->where('id_contrarecibo_caratula', $contrarecibo);
            $this->ci->db->update('mov_contrarecibo_caratula', $datos_actualizar_poliza);

            foreach ($datos_detalle_compromiso as $row) {
//                $this->debugeo->imprimir_pre($row);

                $estructura = json_decode($row->estructura);

//                $this->debugeo->imprimir_pre($estructura);

                $centro_costo = $estructura->centro_de_costos;
                $partida = $estructura->partida;

                if($estructura->partida == 33903 && $datos_caratula_contrarecibo->destino != "Honorarios") {
                    $datos_cuentas = $this->ciclo_model->tomar_cuentas_contrarecibo("33903-S");
                }
                elseif($estructura->partida == 32201 && $datos_caratula_contrarecibo->destino == "Honorarios") {
                    $datos_cuentas = $this->ciclo_model->tomar_cuentas_contrarecibo("32201-H");
                }
                elseif($estructura->partida == 33601 && $datos_caratula_contrarecibo->destino == "Honorarios") {
                    $datos_cuentas = $this->ciclo_model->tomar_cuentas_contrarecibo("33601-H");
                }
                elseif($estructura->partida == 33901 && $datos_caratula_contrarecibo->destino == "Honorarios") {
                    $datos_cuentas = $this->ciclo_model->tomar_cuentas_contrarecibo("33901-H");
                }
                else {
                    $datos_cuentas = $this->ci->ciclo_model->tomar_cuentas_contrarecibo($estructura->partida);
                }

//                $this->debugeo->imprimir_pre($datos_cuentas);

                foreach ($datos_cuentas as $cuenta) {

                    if (!$cuenta) {
                        throw new Exception('No existe cuenta contable asociada a la partida.');
                    }

//                $this->debugeo->imprimir_pre($cuenta);

                    $sql = "SELECT descripcion FROM cat_clasificador_fuentes_financia WHERE codigo = ?;";
                    $query = $this->ci->db->query($sql, array($estructura->fuente_de_financiamiento));
                    $resultado_fuente = $query->row();

                    $nombre_fuente_financiamiento = $resultado_fuente->descripcion;

                    if ($estructura->partida == 23801 && $row->iva > 0) {

                        $query_insertar_abono = "INSERT INTO mov_poliza_detalle
                            (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta, cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES
                            (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?),
                            (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?),
                            (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?),
                            (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?),
                            (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?),
                            (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?),
                            (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                        $datos_abono = array(
                            $last,
                            'Diario',
                            1198,
                            '1.1.4.1.6.1.1000.2.1',
                            '1.1.4.1.6.1.1000.2',
                            9,
                            $datos_caratula_contrarecibo->concepto,
                            $row->subtotal,
                            0.0,
                            $resultado_fuente->descripcion,
                            'Material en Firme',
                            $centro_costo,
                            $partida,

                            $last,
                            'Diario',
                            5511,
                            "2.1.1.2.1.1.0001",
                            "2.1.1.2.1.1",
                            7,
                            $datos_caratula_contrarecibo->concepto,
                            0.0,
                            $row->importe,
                            $resultado_fuente->descripcion,
                            "Proveedor Generico de Materiales para Comercializar",
                            $centro_costo,
                            $partida,

                            $last,
                            'Diario',
                            1166,
                            "1.1.2.8.2.0002",
                            "1.1.2.8.2",
                            6,
                            $datos_caratula_contrarecibo->concepto,
                            $row->iva,
                            0.0,
                            $resultado_fuente->descripcion,
                            "I.V.A. Material para Comercializar",
                            $centro_costo,
                            $partida,

                            $last,
                            'Diario',
                            7805,
                            "8.2.5.1.1",
                            "8.2.5.1",
                            5,
                            $datos_caratula_contrarecibo->concepto,
                            $row->importe,
                            0.0,
                            $resultado_fuente->descripcion,
                            "Presupuesto de Egresos Devengado",
                            $centro_costo,
                            $partida,

                            $last,
                            'Diario',
                            7802,
                            "8.2.4.1.1",
                            "8.2.4.1",
                            5,
                            $datos_caratula_contrarecibo->concepto,
                            0.0,
                            $row->importe,
                            $resultado_fuente->descripcion,
                            "Presupuesto de Egresos Comprometido",
                            $centro_costo,
                            $partida,

                            $last,
                            'Diario',
                            7808,
                            "8.2.6.1.1",
                            "8.2.6.1",
                            5,
                            $datos_caratula_contrarecibo->concepto,
                            $row->importe,
                            0.0,
                            $resultado_fuente->descripcion,
                            "Presupuesto de Egresos Ejercido",
                            $centro_costo,
                            $partida,

                            $last,
                            'Diario',
                            7805,
                            "8.2.5.1.1",
                            "8.2.5.1",
                            5,
                            $datos_caratula_contrarecibo->concepto,
                            0.0,
                            $row->importe,
                            $resultado_fuente->descripcion,
                            "Presupuesto de Egresos Devengado",
                            $centro_costo,
                            $partida,
                        );

                        $resultado_insertar_abono_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                        $cargos += $row->subtotal;
                        $abonos += $row->importe;

                        $cargos += $row->iva;

                        $cargos += $row->importe;
                        $abonos += $row->importe;

                        $cargos += $row->importe;
                        $abonos += $row->importe;

                        $total_filas += 7;
                        break;
                    }
                    elseif($estructura->partida == 23801 && $row->iva <= 0) {

                        $query_insertar_abono = "INSERT INTO mov_poliza_detalle
                            (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta, cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES
                            (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?),
                            (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?),
                            (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?),
                            (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?),
                            (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?),
                            (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                        $datos_abono = array(
                            $last,
                            'Diario',
                            1195,
                            '1.1.4.1.6.1.1000.1.1',
                            '1.1.4.1.6.1.1000.1',
                            9,
                            $datos_caratula_contrarecibo->concepto,
                            $row->importe,
                            0.0,
                            $resultado_fuente->descripcion,
                            'Material en Firme',
                            $centro_costo,
                            $partida,

                            $last,
                            'Diario',
                            5511,
                            "2.1.1.2.1.1.0001",
                            "2.1.1.2.1.1",
                            7,
                            $datos_caratula_contrarecibo->concepto,
                            0.0,
                            $row->importe,
                            $resultado_fuente->descripcion,
                            "Proveedor Generico de Materiales para Comercializar",
                            $centro_costo,
                            $partida,

                            $last,
                            'Diario',
                            7805,
                            "8.2.5.1.1",
                            "8.2.5.1",
                            5,
                            $datos_caratula_contrarecibo->concepto,
                            $row->importe,
                            0.0,
                            $resultado_fuente->descripcion,
                            "Presupuesto de Egresos Devengado",
                            $centro_costo,
                            $partida,

                            $last,
                            'Diario',
                            7802,
                            "8.2.4.1.1",
                            "8.2.4.1",
                            5,
                            $datos_caratula_contrarecibo->concepto,
                            0.0,
                            $row->importe,
                            $resultado_fuente->descripcion,
                            "Presupuesto de Egresos Comprometido",
                            $centro_costo,
                            $partida,

                            $last,
                            'Diario',
                            7808,
                            "8.2.6.1.1",
                            "8.2.6.1",
                            5,
                            $datos_caratula_contrarecibo->concepto,
                            $row->importe,
                            0.0,
                            $resultado_fuente->descripcion,
                            "Presupuesto de Egresos Ejercido",
                            $centro_costo,
                            $partida,

                            $last,
                            'Diario',
                            7805,
                            "8.2.5.1.1",
                            "8.2.5.1",
                            5,
                            $datos_caratula_contrarecibo->concepto,
                            0.0,
                            $row->importe,
                            $resultado_fuente->descripcion,
                            "Presupuesto de Egresos Devengado",
                            $centro_costo,
                            $partida,
                        );

                        $resultado_insertar_abono_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                        $cargos += $row->importe;
                        $abonos += $row->importe;

                        $cargos += $row->importe;
                        $abonos += $row->importe;

                        $cargos += $row->importe;
                        $abonos += $row->importe;

                        $total_filas += 6;
                        break;
                    }
                    elseif($datos_caratula_contrarecibo->destino == "Honorarios") {

                        $isr = round($row->subtotal * .10, 2);
                        $iva_retenido = round(($row->iva / 3) * 2, 2);
                        $total_pagar = $row->importe - $isr;
                        $total_pagar -= $iva_retenido;

                        if ($cuenta->cuenta_cargo) {

                            if (strpos(strtolower($cuenta->nombre_cargo), "i.v.a.") !== FALSE || strpos(strtolower($cuenta->nombre_cargo), "i.v.a") !== FALSE) {
                                if ($row->iva !== 0) {
                                    $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                    $query_cargo = $this->ci->db->query($sql_cargo, array($cuenta->cuenta_cargo));
                                    $resultado_cuenta_cargo = $query_cargo->row();

                                    $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_cargo);

                                    if ($existe_cuenta == FALSE) {
                                        throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_cargo.' '.$cuenta->nombre_cargo);
                                    }

                                    $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida, concepto_especifico) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                    $datos_cargo = array(
                                        $last,
                                        'Diario',
                                        $cuenta->id_correlacion_partidas_contables,
                                        $cuenta->cuenta_cargo,
                                        $resultado_cuenta_cargo->id_padre,
                                        $resultado_cuenta_cargo->nivel,
                                        $datos_caratula_contrarecibo->concepto,
                                        $row->iva,
                                        0.0,
                                        $resultado_fuente->descripcion,
                                        $cuenta->nombre_cargo,
                                        $centro_costo,
                                        $partida,
                                        $datos_caratula_contrarecibo->concepto_especifico,
                                    );

                                    $resultado_insertar_cargo_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                    $cargos += $row->iva;

                                    $total_filas += 1;

                                }
                            } elseif (strpos(strtolower($cuenta->nombre_cargo), "presupuesto de egresos") !== FALSE) {
                                $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_cargo = $this->ci->db->query($sql_cargo, array($cuenta->cuenta_cargo));
                                $resultado_cuenta_cargo = $query_cargo->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_cargo);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_cargo.' '.$cuenta->nombre_cargo);
                                }

                                $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida, concepto_especifico) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_cargo = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_cargo,
                                    $resultado_cuenta_cargo->id_padre,
                                    $resultado_cuenta_cargo->nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    $row->importe,
                                    0.0,
                                    $resultado_fuente->descripcion,
                                    $cuenta->nombre_cargo,
                                    $centro_costo,
                                    $partida,
                                    $datos_caratula_contrarecibo->concepto_especifico,
                                );

                                $resultado_insertar_cargo_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                $cargos += $row->importe;

                                $total_filas += 1;
                            } else {

                                $cuenta_insertar = '';

                                $_cuenta = explode(".", $cuenta->cuenta_cargo);

                                if($_cuenta[0] == 5) {
                                    $cuenta_buscar = $cuenta->cuenta_cargo.".".$centro_costo;
                                    $this->ci->db->select('cuenta')->from('cat_cuentas_contables')->where('cuenta', $cuenta_buscar);
                                    $query_cuenta_cargo_buscar = $this->ci->db->get();
                                    $provisional = $query_cuenta_cargo_buscar->row_array();
                                    if($provisional) {
                                        $cuenta_insertar = $provisional["cuenta"];
                                    } else {
                                        throw new Exception('No existe la cuenta contable '.$cuenta_buscar.'.');
                                    }
                                } else {
                                    $cuenta_insertar = $cuenta->cuenta_cargo;
                                }

                                $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_cargo = $this->ci->db->query($sql_cargo, array($cuenta_insertar));
                                $resultado_cuenta_cargo = $query_cargo->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_cargo);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_cargo.' '.$cuenta->nombre_cargo);
                                }

                                $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida, concepto_especifico) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_cargo = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta_insertar,
                                    $resultado_cuenta_cargo->id_padre,
                                    $resultado_cuenta_cargo->nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    $row->subtotal,
                                    0.0,
                                    $resultado_fuente->descripcion,
                                    $cuenta->nombre_cargo,
                                    $centro_costo,
                                    $partida,
                                    $datos_caratula_contrarecibo->concepto_especifico,
                                );

                                $resultado_insertar_cargo_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                $cargos += $row->subtotal;

                                $total_filas += 1;
                            }

                        }

                        if ($cuenta->cuenta_abono) {

                            if (strpos(strtolower($cuenta->nombre_abono), 'i.v.a. retenido') !== FALSE || strpos(strtolower($cuenta->nombre_abono), 'i.v.a retenido') !== FALSE || strpos(strtolower($cuenta->nombre_abono), "iva retenido") !== FALSE) {
                                if ($row->iva !== 0) {
                                    $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                    $query_abono = $this->ci->db->query($sql_abono, array($cuenta->cuenta_abono));
                                    $resultado_cuenta_abono = $query_abono->row();

                                    $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_abono);

                                    if ($existe_cuenta == FALSE) {
                                        throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_abono.' '.$cuenta->nombre_abono);
                                    }

                                    $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                    $datos_abono = array(
                                        $last,
                                        'Diario',
                                        $cuenta->id_correlacion_partidas_contables,
                                        $cuenta->cuenta_abono,
                                        $resultado_cuenta_abono->id_padre,
                                        $resultado_cuenta_abono->nivel,
                                        $datos_caratula_contrarecibo->concepto,
                                        0.0,
                                        $iva_retenido,
                                        $resultado_fuente->descripcion,
                                        $cuenta->nombre_abono,
                                        $centro_costo,
                                        $partida,
                                    );

                                    $resultado_insertar_abono_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                    $abonos += $iva_retenido;

                                    $total_filas += 1;

                                }
                            } elseif(strpos(strtolower($cuenta->nombre_abono), "i.v.a.") !== FALSE || strpos(strtolower($cuenta->nombre_abono), "i.v.a") !== FALSE || strpos(strtolower($cuenta->nombre_abono), "iva") !== FALSE) {
                                if ($row->iva !== 0) {
                                    $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                    $query_abono = $this->ci->db->query($sql_abono, array($cuenta->cuenta_abono));
                                    $resultado_cuenta_abono = $query_abono->row();

                                    $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_abono);

                                    if ($existe_cuenta == FALSE) {
                                        throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_abono.' '.$cuenta->nombre_abono);
                                    }

                                    $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta, cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                    $datos_abono = array(
                                        $last,
                                        'Diario',
                                        $cuenta->id_correlacion_partidas_contables,
                                        $cuenta->cuenta_abono,
                                        $resultado_cuenta_abono->id_padre,
                                        $resultado_cuenta_abono->nivel,
                                        $datos_caratula_contrarecibo->concepto,
                                        0.0,
                                        $row->iva,
                                        $resultado_fuente->descripcion,
                                        $cuenta->nombre_abono,
                                        $centro_costo,
                                        $partida,
                                    );

                                    $resultado_insertar_abono_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                    $abonos += $row->iva;

                                    $total_filas += 1;
                                }
                            } elseif(strpos(strtolower($cuenta->nombre_abono), "i.s.r.") !== FALSE || strpos(strtolower($cuenta->nombre_abono), "i.s.r") !== FALSE || strpos(strtolower($cuenta->nombre_abono), "isr") !== FALSE) {
                                $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_abono = $this->ci->db->query($sql_abono, array($cuenta->cuenta_abono));
                                $resultado_cuenta_abono = $query_abono->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_abono);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_abono.' '.$cuenta->nombre_abono);
                                }

                                $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_abono = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_abono,
                                    $resultado_cuenta_abono->id_padre,
                                    $resultado_cuenta_abono->nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    0.0,
                                    $isr,
                                    $resultado_fuente->descripcion,
                                    $cuenta->nombre_abono,
                                    $centro_costo,
                                    $partida,
                                );

                                $resultado_insertar_abono_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                $abonos += $isr;

                                $total_filas += 1;

                            } elseif (strpos(strtolower($cuenta->nombre_abono), "presupuesto de egresos") !== FALSE) {
                                $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_abono = $this->ci->db->query($sql_abono, array($cuenta->cuenta_abono));
                                $resultado_cuenta_abono = $query_abono->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_abono);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_abono.' '.$cuenta->nombre_abono);
                                }

                                $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta, cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_abono = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_abono,
                                    $resultado_cuenta_abono->id_padre,
                                    $resultado_cuenta_abono->nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    0.0,
                                    $row->importe,
                                    $resultado_fuente->descripcion,
                                    $cuenta->nombre_abono,
                                    $centro_costo,
                                    $partida,
                                );

                                $resultado_insertar_abono_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                $abonos += $row->importe;

                                $total_filas += 1;
                            } else {

                                $cuenta_insertar = '';

                                $_cuenta = explode(".", $cuenta->cuenta_abono);

                                if($_cuenta[0] == 5) {
                                    $cuenta_buscar = $cuenta->cuenta_abono.".".$centro_costo;
                                    $this->ci->db->select('cuenta')->from('cat_cuentas_contables')->where('cuenta', $cuenta_buscar);
                                    $query_cuenta_cargo_buscar = $this->ci->db->get();
                                    $provisional = $query_cuenta_cargo_buscar->row_array();
                                    if($provisional) {
                                        $cuenta_insertar = $provisional["cuenta"];
                                    } else {
                                        throw new Exception('No existe la cuenta contable '.$cuenta_buscar.'.');
                                    }
                                } else {
                                    $cuenta_insertar = $cuenta->cuenta_abono;
                                }

                                $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_abono = $this->ci->db->query($sql_abono, array($cuenta_insertar));
                                $resultado_cuenta_abono = $query_abono->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_abono);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_abono.' '.$cuenta->nombre_abono);
                                }

                                $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida, concepto_especifico) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_abono = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta_insertar,
                                    $resultado_cuenta_abono->id_padre,
                                    $resultado_cuenta_abono->nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    0.0,
                                    $total_pagar,
                                    $resultado_fuente->descripcion,
                                    $cuenta->nombre_abono,
                                    $centro_costo,
                                    $partida,
                                    $datos_caratula_contrarecibo->concepto_especifico,
                                );

                                $resultado_insertar_abono_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                $cargos += $total_pagar;

                                $total_filas += 1;

                            }

                        }

                    }
                    elseif($estructura->capitulo == 1000) {

                        $total_nomina = TRUE;

                        if ($cuenta->cuenta_cargo) {

                            if (strpos(strtolower($cuenta->nombre_cargo), "presupuesto de egresos") !== FALSE) {
                                $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_cargo = $this->ci->db->query($sql_cargo, array($cuenta->cuenta_cargo));
                                $resultado_cuenta_cargo = $query_cargo->row();

                                $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_cargo = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_cargo,
                                    $resultado_cuenta_cargo->id_padre,
                                    $resultado_cuenta_cargo->nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    $row->importe,
                                    0.0,
                                    $resultado_fuente->descripcion,
                                    $cuenta->nombre_cargo,
                                    $centro_costo,
                                    $partida,
                                );

                                $resultado_insertar_cargo_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                $cargos += $row->importe;

                                $total_filas += 1;
                            } else {
                                $cuenta_insertar = '';

                                $_cuenta = explode(".", $cuenta->cuenta_cargo);

                                if($_cuenta[0] == 5) {
                                    $cuenta_buscar = $cuenta->cuenta_cargo.".".$centro_costo;
                                    $this->ci->db->select('cuenta')->from('cat_cuentas_contables')->where('cuenta', $cuenta_buscar);
                                    $query_cuenta_cargo_buscar = $this->ci->db->get();
                                    $provisional = $query_cuenta_cargo_buscar->row_array();
                                    if($provisional) {
                                        $cuenta_insertar = $provisional["cuenta"];
                                    } else {
                                        throw new Exception('No existe la cuenta contable '.$cuenta_buscar.'.');
                                    }
                                } else {
                                    $cuenta_insertar = $cuenta->cuenta_cargo;
                                }

//                            $this->debugeo->imprimir_pre($cuenta);

                                $sql_cargo = "SELECT cuenta_padre AS id_padre, nombre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_cargo = $this->ci->db->query($sql_cargo, array($cuenta_insertar));
                                $resultado_cuenta_cargo = $query_cargo->row();

                                $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_cargo = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta_insertar,
                                    $resultado_cuenta_cargo->id_padre,
                                    $resultado_cuenta_cargo->nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    $row->importe,
                                    0.0,
                                    $resultado_fuente->descripcion,
                                    $resultado_cuenta_cargo->nombre,
                                    $centro_costo,
                                    $partida,
                                );

                                $resultado_insertar_cargo_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                $cargos += $row->importe;

                                $total_filas += 1;
                            }

                        }

                        if ($cuenta->cuenta_abono) {

                            if (strpos(strtolower($cuenta->nombre_abono), "presupuesto de egresos") !== FALSE) {
                                $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_abono = $this->ci->db->query($sql_abono, array($cuenta->cuenta_abono));
                                $resultado_cuenta_abono = $query_abono->row();

                                $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta, cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_abono = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_abono,
                                    $resultado_cuenta_abono->id_padre,
                                    $resultado_cuenta_abono->nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    0.0,
                                    $row->importe,
                                    $resultado_fuente->descripcion,
                                    $cuenta->nombre_abono,
                                    $centro_costo,
                                    $partida,
                                );

                                $resultado_insertar_abono_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                $abonos += $row->importe;

                                $total_filas += 1;
                            } else {
                                $abonos += $row->importe;
                                continue;
                            }

                        }
                    }
                    else {

                        if ($cuenta->cuenta_cargo) {

                            if (strpos(strtolower($cuenta->nombre_cargo), "i.v.a.") !== FALSE || strpos(strtolower($cuenta->nombre_cargo), "i.v.a") !== FALSE) {
                                if ($row->iva !== 0) {
                                    $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                    $query_cargo = $this->ci->db->query($sql_cargo, array($cuenta->cuenta_cargo));
                                    $resultado_cuenta_cargo = $query_cargo->row();

                                    $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                    $datos_cargo = array(
                                        $last,
                                        'Diario',
                                        $cuenta->id_correlacion_partidas_contables,
                                        $cuenta->cuenta_cargo,
                                        $resultado_cuenta_cargo->id_padre,
                                        $resultado_cuenta_cargo->nivel,
                                        $datos_caratula_contrarecibo->concepto,
                                        $row->iva,
                                        0.0,
                                        $resultado_fuente->descripcion,
                                        $cuenta->nombre_cargo,
                                        $centro_costo,
                                        $partida,
                                    );

                                    $resultado_insertar_cargo_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                    $cargos += $row->iva;

                                    $total_filas += 1;

                                }
                            } elseif (strpos(strtolower($cuenta->nombre_cargo), "presupuesto de egresos") !== FALSE) {
                                $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_cargo = $this->ci->db->query($sql_cargo, array($cuenta->cuenta_cargo));
                                $resultado_cuenta_cargo = $query_cargo->row();

                                $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_cargo = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_cargo,
                                    $resultado_cuenta_cargo->id_padre,
                                    $resultado_cuenta_cargo->nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    $row->importe,
                                    0.0,
                                    $resultado_fuente->descripcion,
                                    $cuenta->nombre_cargo,
                                    $centro_costo,
                                    $partida,
                                );

                                $resultado_insertar_cargo_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                $cargos += $row->importe;

                                $total_filas += 1;
                            } else {
                                $cuenta_insertar = '';

                                $_cuenta = explode(".", $cuenta->cuenta_cargo);

                                if($_cuenta[0] == 5) {
                                    $cuenta_buscar = $cuenta->cuenta_cargo.".".$centro_costo;
                                    $this->ci->db->select('cuenta')->from('cat_cuentas_contables')->where('cuenta', $cuenta_buscar);
                                    $query_cuenta_cargo_buscar = $this->ci->db->get();
                                    $provisional = $query_cuenta_cargo_buscar->row_array();
                                    if($provisional) {
                                        $cuenta_insertar = $provisional["cuenta"];
                                    } else {
                                        throw new Exception('No existe la cuenta contable '.$cuenta_buscar.'.');
                                    }
                                } else {
                                    $cuenta_insertar = $cuenta->cuenta_cargo;
                                }

//                            $this->debugeo->imprimir_pre($cuenta);

                                $sql_cargo = "SELECT cuenta_padre AS id_padre, nombre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_cargo = $this->ci->db->query($sql_cargo, array($cuenta_insertar));
                                $resultado_cuenta_cargo = $query_cargo->row();

                                $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_cargo = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta_insertar,
                                    $resultado_cuenta_cargo->id_padre,
                                    $resultado_cuenta_cargo->nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    $row->subtotal,
                                    0.0,
                                    $resultado_fuente->descripcion,
                                    $resultado_cuenta_cargo->nombre,
                                    $centro_costo,
                                    $partida,
                                );

                                $resultado_insertar_cargo_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                $cargos += $row->subtotal;

                                $total_filas += 1;
                            }

                        }

                        if ($cuenta->cuenta_abono) {

                            if (strpos(strtolower($cuenta->nombre_abono), 'i.v.a.') !== FALSE || strpos(strtolower($cuenta->nombre_abono), 'i.v.a') !== FALSE && $row->iva > 0) {
                                $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_abono = $this->ci->db->query($sql_abono, array($cuenta->cuenta_abono));
                                $resultado_cuenta_abono = $query_abono->row();

                                $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta, cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_abono = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_abono,
                                    $resultado_cuenta_abono->id_padre,
                                    $resultado_cuenta_abono->nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    0.0,
                                    $row->iva,
                                    $resultado_fuente->descripcion,
                                    $cuenta->nombre_abono,
                                    $centro_costo,
                                    $partida,
                                );

                                $resultado_insertar_abono_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                $abonos += $row->iva;

                                $total_filas += 1;

                            } elseif (strpos(strtolower($cuenta->nombre_abono), "presupuesto de egresos") !== FALSE) {
                                $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_abono = $this->ci->db->query($sql_abono, array($cuenta->cuenta_abono));
                                $resultado_cuenta_abono = $query_abono->row();

                                $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta, cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_abono = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_abono,
                                    $resultado_cuenta_abono->id_padre,
                                    $resultado_cuenta_abono->nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    0.0,
                                    $row->importe,
                                    $resultado_fuente->descripcion,
                                    $cuenta->nombre_abono,
                                    $centro_costo,
                                    $partida,
                                );

                                $resultado_insertar_abono_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                $abonos += $row->importe;

                                $total_filas += 1;
                            } else {
                                $cuenta_insertar = '';

                                $_cuenta = explode(".", $cuenta->cuenta_abono);

                                if($_cuenta[0] == 5) {
                                    $cuenta_buscar = $cuenta->cuenta_abono.".".$centro_costo;
                                    $this->ci->db->select('cuenta')->from('cat_cuentas_contables')->where('cuenta', $cuenta_buscar);
                                    $query_cuenta_cargo_buscar = $this->ci->db->get();
                                    $provisional = $query_cuenta_cargo_buscar->row_array();
                                    if($provisional) {
                                        $cuenta_insertar = $provisional["cuenta"];
                                    } else {
                                        throw new Exception('No existe la cuenta contable '.$cuenta_buscar.'.');
                                    }
                                } else {
                                    $cuenta_insertar = $cuenta->cuenta_abono;
                                }

//                            $this->debugeo->imprimir_pre($cuenta);

                                $sql_abono = "SELECT cuenta_padre AS id_padre, nombre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_abono = $this->ci->db->query($sql_abono, array($cuenta_insertar));
                                $resultado_cuenta_abono = $query_abono->row();

                                $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_abono = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta_insertar,
                                    $resultado_cuenta_cargo->id_padre,
                                    $resultado_cuenta_cargo->nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    0.0,
                                    $row->importe,
                                    $resultado_fuente->descripcion,
                                    $resultado_cuenta_cargo->nombre,
                                    $centro_costo,
                                    $partida,
                                );

                                $resultado_insertar_abono_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                $abonos += $row->importe;

                                $total_filas += 1;
                            }

                        }
                    }

                }
            }

            if($total_nomina) {
                $query_insertar_nomina = "INSERT INTO mov_poliza_detalle
                            (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta, cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES
                            (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                $datos_insertar_nomina = array(
                    $last,
                    'Diario',
                    1712,
                    '2.1.1.1.1.0001',
                    '2.1.1.1.1',
                    6,
                    $datos_caratula_contrarecibo->concepto,
                    0.0,
                    $datos_caratula_contrarecibo->importe,
                    $resultado_fuente->descripcion,
                    'Sueldos y Salarios por Pagar',
                    $centro_costo,
                    $partida,
                );

                $resultado_insertar_abono_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_nomina, $datos_insertar_nomina);
            }

            $datos_actualizar_caratula = array(
                'no_partidas' => $total_filas,
                'cargos' => $cargos,
                'abonos' => $abonos,
            );

            $this->ci->db->where('numero_poliza', $last);
            $this->ci->db->update('mov_polizas_cabecera', $datos_actualizar_caratula);

            $datos_actualizar_contrarecibo = array(
                'poliza' => 1,
            );

            $this->ci->db->where('id_contrarecibo_caratula', $contrarecibo);
            $this->ci->db->update('mov_contrarecibo_caratula', $datos_actualizar_contrarecibo);

            $this->ci->db->trans_commit();

            $respuesta = array(
                "mensaje" => '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Se ha generado con &eacute;xito la P&oacute;liza de Diario No. '.$last.'</div>',
            );

            return $respuesta["mensaje"];
//            $this->debugeo->imprimir_pre($respuesta["mensaje"]);
//                echo(json_encode($respuesta));

        } catch (Exception $e) {
            $this->ci->db->trans_rollback();

            $respuesta = array(
                "mensaje" => '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$e->getMessage().'.</div>',
            );

            return $respuesta["mensaje"];
//            $this->debugeo->imprimir_pre($respuesta["mensaje"]);
//                echo(json_encode($respuesta));
        }
    }

    function generar_poliza_egresos($numero) {

//        $this->debugeo->imprimir_pre($value["movimiento"]);

        try {
            $id = $this->ci->tank_auth->get_user_id();

            $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
            $resultado_query_usuario = $this->ci->db->query($query_usuario, array($id));
            $nombre_encontrado = $resultado_query_usuario->row();
            $nombre_completo = $nombre_encontrado->nombre . " " . $nombre_encontrado->apellido_paterno . " " . $nombre_encontrado->apellido_materno;

            $movimiento = $numero;

//        Se inicizaliza la variable con el ultimo valor de las polizas
            $last = 0;
//        Se toma el numero de la ultima poliza
            $ultimo = $this->ci->ciclo_model->ultima_poliza();

//        Se le suma uno al ultimo valor de las polizas
            if($ultimo) {
                $last = $ultimo->ultimo + 1;
            }
//        De lo contrario se inicia en 1
            else {
                $last = 1;
            }
            $query_caratula_movimiento = "SELECT numero FROM mov_bancos_pagos_contrarecibos WHERE movimiento = ?;";

            $datos_caratula_movimiento = $this->ci->ciclo_model->get_datos_contrarecibo_caratula($movimiento, $query_caratula_movimiento);

            $contrarecibo = $datos_caratula_movimiento->numero;

//            $this->debugeo->imprimir_pre($contrarecibo);

//        Se prepara el query para llamar todos los datos de la caratula del contrarecibo
            $query_caratula = "SELECT * FROM mov_contrarecibo_caratula WHERE id_contrarecibo_caratula = ?;";

//        Se llama a la funci�n que se encarga de tomar todos los datos de la caratula del contrarecibo
            $datos_caratula_contrarecibo = $this->ci->ciclo_model->get_datos_contrarecibo_caratula($contrarecibo, $query_caratula);

//            $this->debugeo->imprimir_pre($datos_caratula_contrarecibo);

//        Se prepara el query para tomar los datos del detalle de compromiso que est�n ligados al contrarecibo
            $query_detalle_compromiso = "SELECT *, COLUMN_JSON(nivel) AS estructura FROM mov_compromiso_detalle WHERE numero_compromiso = ?;";

//        Se llama ala funcion que se encarga de tomar todos los datos del detalle del compromiso que est� ligado con el contrarecibo
            $datos_detalle_compromiso = $this->ci->ciclo_model->datos_compromisoDetalle($datos_caratula_contrarecibo->numero_compromiso, $query_detalle_compromiso);

//        Se crea un arreglo donde se van a guardar los resultados del detalle del compromiso
            $datos_detalle_poliza = array();

            if (!$datos_detalle_compromiso) {
                throw new Exception('No hay partidas dentro del compromiso.');
            }

            $centro_costo = '';
            $partida = '';
            $nombre_fuente_financiamiento = '';
            $total_filas = 0;
            $cargos = 0;
            $abonos = 0;

            $resultado_insertar_cargo_query = 0;
            $resultado_insertar_abono_query = 0;

            $this->ci->db->select('numero_poliza')->from('mov_polizas_cabecera')->where('contrarecibo', $datos_caratula_contrarecibo->id_contrarecibo_caratula);
            $query_numero_poliza_diario = $this->ci->db->get();
            $numero_poliza_diario = $query_numero_poliza_diario->row_array();

            if (!isset($numero_poliza_diario["numero_poliza"])) {
                throw new Exception('No se ha generado la p�liza de Diario del contrarecibo '.$datos_caratula_contrarecibo->id_contrarecibo_caratula);
            }

            $query_insertar_caratula_poliza = "INSERT INTO mov_polizas_cabecera (numero_poliza, tipo_poliza, fecha, fecha_real, concepto, enfirme, sifirme, no_partidas, importe, cancelada, estatus, creado_por, autorizada, cargos, abonos, movimiento, concepto_especifico, contrarecibo, id_proveedor, proveedor, poliza_contrarecibo, no_movimiento) VALUES (?, ?, ?, NOW(), ?, 0, 0, ?, ?, 0, ?, ?, 0, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
            $datos_caratula_poliza = array(
                $last,
                'Egresos',
                $datos_caratula_contrarecibo->fecha_emision,
                $datos_caratula_contrarecibo->concepto,
                0,
                $datos_caratula_contrarecibo->importe,
                'espera',
                $nombre_completo,
                0,
                0,
                $movimiento,
                $datos_caratula_contrarecibo->concepto_especifico,
                $datos_caratula_contrarecibo->id_contrarecibo_caratula,
                $datos_caratula_contrarecibo->id_proveedor,
                $datos_caratula_contrarecibo->proveedor,
                $numero_poliza_diario["numero_poliza"],
                $datos_caratula_contrarecibo->documento,
            );

            $resultado_insertar_caratula = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_caratula_poliza, $datos_caratula_poliza);

            $datos_actualizar_poliza = array(
                'poliza' => 1,
            );

            $this->ci->db->where('movimiento', $movimiento);
            $this->ci->db->update('mov_bancos_movimientos', $datos_actualizar_poliza);

            foreach ($datos_detalle_compromiso as $row) {
//                $this->debugeo->imprimir_pre($row);

                $estructura = json_decode($row->estructura);

//                $this->debugeo->imprimir_pre($estructura);

                $centro_costo = $estructura->centro_de_costos;
                $partida = $estructura->partida;

                if($estructura->partida == 33903 && $datos_caratula_contrarecibo->destino != "Honorarios") {
                    $datos_cuentas = $this->ci->ciclo_model->tomar_cuentas_movimiento("33903-S");
                }
                elseif($estructura->partida == 32201 && $datos_caratula_contrarecibo->destino == "Honorarios") {
                    $datos_cuentas = $this->ci->ciclo_model->tomar_cuentas_movimiento("32201-H");
                }
                elseif($estructura->partida == 33601 && $datos_caratula_contrarecibo->destino == "Honorarios") {
                    $datos_cuentas = $this->ci->ciclo_model->tomar_cuentas_movimiento("33601-H");
                }
                elseif($estructura->partida == 33901 && $datos_caratula_contrarecibo->destino == "Honorarios") {
                    $datos_cuentas = $this->ci->ciclo_model->tomar_cuentas_movimiento("33901-H");
                }
                else {
                    $datos_cuentas = $this->ci->ciclo_model->tomar_cuentas_movimiento($estructura->partida);
                }

//                $this->debugeo->imprimir_pre($datos_cuentas);

                foreach ($datos_cuentas as $cuenta) {

                    if (!$cuenta) {
                        throw new Exception('No existe cuenta contable asociada a la partida.');
                    }

//                $this->debugeo->imprimir_pre($cuenta);

                    $sql = "SELECT descripcion FROM cat_clasificador_fuentes_financia WHERE codigo = ?;";
                    $query = $this->ci->db->query($sql, array($estructura->fuente_de_financiamiento));
                    $resultado_fuente = $query->row();

                    $nombre_fuente_financiamiento = $resultado_fuente->descripcion;

                    if ($estructura->partida == 23801 && $row->iva > 0) {

                        if($estructura->fuente_de_financiamiento == 1) {
                            $id_cuenta = 1758;
                            $cuenta_banco = "2.1.2.1.1.1.0010";
                            $nombre_cuenta = "TESOFE (Tesorer�a de la Federaci�n";
                            $id_padre = "2.1.2.1.1.1";
                            $nivel = 7;

                        } elseif($estructura->fuente_de_financiamiento == 4 && $estructura->capitulo == 1000){
                            $id_cuenta = 128;
                            $cuenta_banco = "1.1.1.2.1.4.7150";
                            $nombre_cuenta = "Cta. 0643357150";
                            $id_padre = "1.1.1.2.1.4";
                            $nivel = 7;
                        } else {
                            $id_cuenta = 116;
                            $cuenta_banco = "1.1.1.2.1.1.1497";
                            $nombre_cuenta = "Cta. 0443361497";
                            $id_padre = "1.1.1.2.1.1";
                            $nivel = 7;
                        }

                        $query_insertar_abono = "INSERT INTO mov_poliza_detalle
                            (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta, cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES
                            (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?),
                            (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?),
                            (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?),
                            (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?),
                            (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?),
                            (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                        $datos_abono = array(
                            $last,
                            'Egresos',
                            5511,
                            '2.1.1.2.1.1.0001',
                            '2.1.1.2.1.1',
                            7,
                            $datos_caratula_contrarecibo->concepto,
                            $row->importe,
                            0.0,
                            $resultado_fuente->descripcion,
                            'Proveedor Generico de Materiales para Comercializar',
                            $centro_costo,
                            $partida,

                            $last,
                            'Egresos',
                            $id_cuenta,
                            $cuenta_banco,
                            $id_padre,
                            $nivel,
                            $datos_caratula_contrarecibo->concepto,
                            0.0,
                            $row->importe,
                            $resultado_fuente->descripcion,
                            $nombre_cuenta,
                            $centro_costo,
                            $partida,

                            $last,
                            'Egresos',
                            1163,
                            "1.1.2.8.1.0002",
                            "1.1.2.8.1",
                            6,
                            $datos_caratula_contrarecibo->concepto,
                            $row->iva,
                            0.0,
                            $resultado_fuente->descripcion,
                            "I.V.A. Material para Comercializar",
                            $centro_costo,
                            $partida,

                            $last,
                            'Egresos',
                            1166,
                            "1.1.2.8.2.0002",
                            "1.1.2.8.2",
                            6,
                            $datos_caratula_contrarecibo->concepto,
                            0.0,
                            $row->iva,
                            $resultado_fuente->descripcion,
                            "I.V.A. Material para Comercializar",
                            $centro_costo,
                            $partida,

                            $last,
                            'Egresos',
                            7811,
                            "8.2.7.1.1",
                            "8.2.7.1",
                            5,
                            $datos_caratula_contrarecibo->concepto,
                            $row->importe,
                            0.0,
                            $resultado_fuente->descripcion,
                            "Presupuesto de Egresos Pagado",
                            $centro_costo,
                            $partida,

                            $last,
                            'Egresos',
                            7808,
                            "8.2.6.1.1",
                            "8.2.6.1",
                            5,
                            $datos_caratula_contrarecibo->concepto,
                            0.0,
                            $row->importe,
                            $resultado_fuente->descripcion,
                            "Presupuesto de Egresos Ejercido",
                            $centro_costo,
                            $partida,
                        );

                        $resultado_insertar_abono_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                        $cargos += $row->importe;
                        $abonos += $row->importe;

                        $cargos += $row->iva;
                        $abonos += $row->iva;

                        $cargos += $row->importe;
                        $abonos += $row->importe;

                        $total_filas += 6;
                        break;

                    }
                    elseif($estructura->partida == 23801 && $row->iva <= 0) {

                        if($estructura->fuente_de_financiamiento == 1) {
                            $id_cuenta = 1758;
                            $cuenta_banco = "2.1.2.1.1.1.0010";
                            $nombre_cuenta = "TESOFE (Tesorer�a de la Federaci�n";
                            $id_padre = "2.1.2.1.1.1";
                            $nivel = 7;

                        } elseif($estructura->fuente_de_financiamiento == 4 && $estructura->capitulo == 1000){
                            $id_cuenta = 128;
                            $cuenta_banco = "1.1.1.2.1.4.7150";
                            $nombre_cuenta = "Cta. 0643357150";
                            $id_padre = "1.1.1.2.1.4";
                            $nivel = 7;
                        } else {
                            $id_cuenta = 116;
                            $cuenta_banco = "1.1.1.2.1.1.1497";
                            $nombre_cuenta = "Cta. 0443361497";
                            $id_padre = "1.1.1.2.1.1";
                            $nivel = 7;
                        }

                        $query_insertar_abono = "INSERT INTO mov_poliza_detalle
                            (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta, cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES
                            (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?),
                            (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?),
                            (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?),
                            (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                        $datos_abono = array(
                            $last,
                            'Egresos',
                            5511,
                            '2.1.1.2.1.1.0001',
                            '2.1.1.2.1.1',
                            7,
                            $datos_caratula_contrarecibo->concepto,
                            $row->importe,
                            0.0,
                            $resultado_fuente->descripcion,
                            'Proveedor Generico de Materiales para Comercializar',
                            $centro_costo,
                            $partida,

                            $last,
                            'Egresos',
                            $id_cuenta,
                            $cuenta_banco,
                            $id_padre,
                            $nivel,
                            $datos_caratula_contrarecibo->concepto,
                            0.0,
                            $row->importe,
                            $resultado_fuente->descripcion,
                            $nombre_cuenta,
                            $centro_costo,
                            $partida,


                            $last,
                            'Egresos',
                            7811,
                            "8.2.7.1.1",
                            "8.2.7.1",
                            5,
                            $datos_caratula_contrarecibo->concepto,
                            $row->importe,
                            0.0,
                            $resultado_fuente->descripcion,
                            "Presupuesto de Egresos Pagado",
                            $centro_costo,
                            $partida,

                            $last,
                            'Egresos',
                            7808,
                            "8.2.6.1.1",
                            "8.2.6.1",
                            5,
                            $datos_caratula_contrarecibo->concepto,
                            0.0,
                            $row->importe,
                            $resultado_fuente->descripcion,
                            "Presupuesto de Egresos Ejercido",
                            $centro_costo,
                            $partida,
                        );

                        $resultado_insertar_abono_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                        $cargos += $row->importe;
                        $abonos += $row->importe;

                        $cargos += $row->importe;
                        $abonos += $row->importe;

                        $total_filas += 4;
                        break;

                    }
                    elseif($datos_caratula_contrarecibo->destino == "Honorarios") {

                        $isr = round($row->subtotal * .10, 2);
                        $iva_retenido = round(($row->iva / 3) * 2, 2);
                        $total_pagar = $row->importe - $isr;
                        $total_pagar -= $iva_retenido;

                        if($estructura->fuente_de_financiamiento == 1) {
                            $id_cuenta = 1758;
                            $cuenta_banco = "2.1.2.1.1.1.0010";
                            $nombre_cuenta = "TESOFE (Tesorer�a de la Federaci�n";
                            $id_padre = "2.1.2.1.1.1";
                            $nivel = 7;

                        } elseif($estructura->fuente_de_financiamiento == 4 && $estructura->capitulo == 1000){
                            $id_cuenta = 128;
                            $cuenta_banco = "1.1.1.2.1.4.7150";
                            $nombre_cuenta = "Cta. 0643357150";
                            $id_padre = "1.1.1.2.1.4";
                            $nivel = 7;
                        } else {
                            $id_cuenta = 116;
                            $cuenta_banco = "1.1.1.2.1.1.1497";
                            $nombre_cuenta = "Cta. 0443361497";
                            $id_padre = "1.1.1.2.1.1";
                            $nivel = 7;
                        }

                        if ($cuenta->cuenta_cargo) {

                            if (strpos(strtolower($cuenta->nombre_cargo), "i.v.a.") !== FALSE || strpos(strtolower($cuenta->nombre_cargo), "i.v.a") !== FALSE || strpos(strtolower($cuenta->nombre_cargo), "iva") !== FALSE) {
                                if ($row->iva !== 0) {
                                    $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                    $query_cargo = $this->ci->db->query($sql_cargo, array($cuenta->cuenta_cargo));
                                    $resultado_cuenta_cargo = $query_cargo->row();

                                    $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_cargo);

                                    if ($existe_cuenta == FALSE) {
                                        throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_cargo.' '.$cuenta->nombre_cargo);
                                    }

                                    $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida, concepto_especifico) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                    $datos_cargo = array(
                                        $last,
                                        'Diario',
                                        $cuenta->id_correlacion_partidas_contables,
                                        $cuenta->cuenta_cargo,
                                        $resultado_cuenta_cargo->id_padre,
                                        $resultado_cuenta_cargo->nivel,
                                        $datos_caratula_contrarecibo->concepto,
                                        $row->iva,
                                        0.0,
                                        $resultado_fuente->descripcion,
                                        $cuenta->nombre_cargo,
                                        $centro_costo,
                                        $partida,
                                        $datos_caratula_contrarecibo->concepto_especifico,
                                    );

                                    $resultado_insertar_cargo_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                    $cargos += $row->iva;

                                    $total_filas += 1;

                                }
                            } elseif (strpos(strtolower($cuenta->nombre_cargo), "presupuesto de egresos") !== FALSE) {
                                $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_cargo = $this->ci->db->query($sql_cargo, array($cuenta->cuenta_cargo));
                                $resultado_cuenta_cargo = $query_cargo->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_cargo);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_cargo.' '.$cuenta->nombre_cargo);
                                }

                                $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida, concepto_especifico) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_cargo = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_cargo,
                                    $resultado_cuenta_cargo->id_padre,
                                    $resultado_cuenta_cargo->nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    $row->importe,
                                    0.0,
                                    $resultado_fuente->descripcion,
                                    $cuenta->nombre_cargo,
                                    $centro_costo,
                                    $partida,
                                    $datos_caratula_contrarecibo->concepto_especifico,
                                );

                                $resultado_insertar_cargo_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                $cargos += $row->importe;

                                $total_filas += 1;
                            } elseif (strpos(strtolower($cuenta->nombre_cargo), "banco") !== FALSE) {

                                $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida, concepto_especifico) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_cargo = array(
                                    $last,
                                    'Diario',
                                    $id_cuenta,
                                    $cuenta_banco,
                                    $id_padre,
                                    $nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    $total_pagar,
                                    0.0,
                                    $resultado_fuente->descripcion,
                                    $nombre_cuenta,
                                    $centro_costo,
                                    $partida,
                                    $datos_caratula_contrarecibo->concepto_especifico,
                                );

                                $resultado_insertar_cargo_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                $cargos += $total_pagar;

                                $total_filas += 1;
                            } else {

                                $cuenta_insertar = '';

                                $_cuenta = explode(".", $cuenta->cuenta_cargo);

                                if($_cuenta[0] == 5) {
                                    $cuenta_buscar = $cuenta->cuenta_cargo.".".$centro_costo;
                                    $this->ci->db->select('cuenta')->from('cat_cuentas_contables')->where('cuenta', $cuenta_buscar);
                                    $query_cuenta_cargo_buscar = $this->ci->db->get();
                                    $provisional = $query_cuenta_cargo_buscar->row_array();
                                    if($provisional) {
                                        $cuenta_insertar = $provisional["cuenta"];
                                    } else {
                                        throw new Exception('No existe la cuenta contable '.$cuenta_buscar.'.');
                                    }
                                } else {
                                    $cuenta_insertar = $cuenta->cuenta_cargo;
                                }

                                $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_cargo = $this->ci->db->query($sql_cargo, array($cuenta_insertar));
                                $resultado_cuenta_cargo = $query_cargo->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_cargo);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_cargo.' '.$cuenta->nombre_cargo);
                                }

                                $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida, concepto_especifico) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_cargo = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta_insertar,
                                    $resultado_cuenta_cargo->id_padre,
                                    $resultado_cuenta_cargo->nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    $total_pagar,
                                    0.0,
                                    $resultado_fuente->descripcion,
                                    $cuenta->nombre_cargo,
                                    $centro_costo,
                                    $partida,
                                    $datos_caratula_contrarecibo->concepto_especifico,
                                );

                                $resultado_insertar_cargo_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                $cargos += $total_pagar;

                                $total_filas += 1;
                            }

                        }

                        if ($cuenta->cuenta_abono) {

                            if (strpos(strtolower($cuenta->nombre_abono), 'i.v.a. retenido') !== FALSE || strpos(strtolower($cuenta->nombre_abono), 'i.v.a retenido') !== FALSE || strpos(strtolower($cuenta->nombre_abono), 'iva retenido') !== FALSE) {
                                if ($row->iva !== 0) {
                                    $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                    $query_abono = $this->ci->db->query($sql_abono, array($cuenta->cuenta_abono));
                                    $resultado_cuenta_abono = $query_abono->row();

                                    $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_abono);

                                    if ($existe_cuenta == FALSE) {
                                        throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_abono.' '.$cuenta->nombre_abono);
                                    }

                                    $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                    $datos_abono = array(
                                        $last,
                                        'Diario',
                                        $cuenta->id_correlacion_partidas_contables,
                                        $cuenta->cuenta_abono,
                                        $resultado_cuenta_abono->id_padre,
                                        $resultado_cuenta_abono->nivel,
                                        $datos_caratula_contrarecibo->concepto,
                                        0.0,
                                        $iva_retenido,
                                        $resultado_fuente->descripcion,
                                        $cuenta->nombre_abono,
                                        $centro_costo,
                                        $partida,
                                    );

                                    $resultado_insertar_abono_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                    $abonos += $iva_retenido;

                                    $total_filas += 1;

                                }
                            } elseif(strpos(strtolower($cuenta->nombre_abono), "i.v.a.") !== FALSE || strpos(strtolower($cuenta->nombre_abono), "i.v.a") !== FALSE) {
                                if ($row->iva !== 0) {
                                    $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                    $query_abono = $this->ci->db->query($sql_abono, array($cuenta->cuenta_abono));
                                    $resultado_cuenta_abono = $query_abono->row();

                                    $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_abono);

                                    if ($existe_cuenta == FALSE) {
                                        throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_abono.' '.$cuenta->nombre_abono);
                                    }

                                    $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta, cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                    $datos_abono = array(
                                        $last,
                                        'Diario',
                                        $cuenta->id_correlacion_partidas_contables,
                                        $cuenta->cuenta_abono,
                                        $resultado_cuenta_abono->id_padre,
                                        $resultado_cuenta_abono->nivel,
                                        $datos_caratula_contrarecibo->concepto,
                                        0.0,
                                        $row->iva,
                                        $resultado_fuente->descripcion,
                                        $cuenta->nombre_abono,
                                        $centro_costo,
                                        $partida,
                                    );

                                    $resultado_insertar_abono_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                    $abonos += $row->iva;

                                    $total_filas += 1;
                                }
                            } elseif(strpos(strtolower($cuenta->nombre_abono), "i.s.r.") !== FALSE || strpos(strtolower($cuenta->nombre_abono), "i.s.r") !== FALSE || strpos(strtolower($cuenta->nombre_abono), "isr") !== FALSE) {
                                $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_abono = $this->ci->db->query($sql_abono, array($cuenta->cuenta_abono));
                                $resultado_cuenta_abono = $query_abono->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_abono);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_abono.' '.$cuenta->nombre_abono);
                                }

                                $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_abono = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_abono,
                                    $resultado_cuenta_abono->id_padre,
                                    $resultado_cuenta_abono->nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    0.0,
                                    $isr,
                                    $resultado_fuente->descripcion,
                                    $cuenta->nombre_abono,
                                    $centro_costo,
                                    $partida,
                                );

                                $resultado_insertar_abono_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                $abonos += $isr;

                                $total_filas += 1;

                            } elseif (strpos(strtolower($cuenta->nombre_abono), "presupuesto de egresos") !== FALSE) {
                                $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_abono = $this->ci->db->query($sql_abono, array($cuenta->cuenta_abono));
                                $resultado_cuenta_abono = $query_abono->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_abono);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_abono.' '.$cuenta->nombre_abono);
                                }

                                $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta, cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_abono = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_abono,
                                    $resultado_cuenta_abono->id_padre,
                                    $resultado_cuenta_abono->nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    0.0,
                                    $row->importe,
                                    $resultado_fuente->descripcion,
                                    $cuenta->nombre_abono,
                                    $centro_costo,
                                    $partida,
                                );

                                $resultado_insertar_abono_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                $abonos += $row->importe;

                                $total_filas += 1;
                            } elseif (strpos(strtolower($cuenta->nombre_abono), "banco") !== FALSE) {

                                $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta, cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_abono = array(
                                    $last,
                                    'Diario',
                                    $id_cuenta,
                                    $cuenta_banco,
                                    $id_padre,
                                    $nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    0.0,
                                    $total_pagar,
                                    $resultado_fuente->descripcion,
                                    $nombre_cuenta,
                                    $centro_costo,
                                    $partida,
                                );

                                $resultado_insertar_abono_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                $abonos += $total_pagar;

                                $total_filas += 1;
                            } else {

                                $cuenta_insertar = '';

                                $_cuenta = explode(".", $cuenta->cuenta_abono);

                                if($_cuenta[0] == 5) {
                                    $cuenta_buscar = $cuenta->cuenta_abono.".".$centro_costo;
                                    $this->ci->db->select('cuenta')->from('cat_cuentas_contables')->where('cuenta', $cuenta_buscar);
                                    $query_cuenta_cargo_buscar = $this->ci->db->get();
                                    $provisional = $query_cuenta_cargo_buscar->row_array();
                                    if($provisional) {
                                        $cuenta_insertar = $provisional["cuenta"];
                                    } else {
                                        throw new Exception('No existe la cuenta contable '.$cuenta_buscar.'.');
                                    }
                                } else {
                                    $cuenta_insertar = $cuenta->cuenta_abono;
                                }

                                $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_abono = $this->ci->db->query($sql_abono, array($cuenta_insertar));
                                $resultado_cuenta_abono = $query_abono->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_abono);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_abono.' '.$cuenta->nombre_abono);
                                }

                                $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida, concepto_especifico) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_abono = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta_insertar,
                                    $resultado_cuenta_abono->id_padre,
                                    $resultado_cuenta_abono->nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    0.0,
                                    $total_pagar,
                                    $resultado_fuente->descripcion,
                                    $cuenta->nombre_abono,
                                    $centro_costo,
                                    $partida,
                                    $datos_caratula_contrarecibo->concepto_especifico,
                                );

                                $resultado_insertar_abono_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                $cargos += $total_pagar;

                                $total_filas += 1;

                            }

                        }

                    }
                    else {
                        if($estructura->fuente_de_financiamiento == 1) {
                            $id_cuenta = 1758;
                            $cuenta_banco = "2.1.2.1.1.1.0010";
                            $nombre_cuenta = "TESOFE (Tesorer�a de la Federaci�n";
                            $id_padre = "2.1.2.1.1.1";
                            $nivel = 7;

                        } elseif($estructura->fuente_de_financiamiento == 4 && $estructura->capitulo == 1000){
                            $id_cuenta = 128;
                            $cuenta_banco = "1.1.1.2.1.4.7150";
                            $nombre_cuenta = "Cta. 0643357150";
                            $id_padre = "1.1.1.2.1.4";
                            $nivel = 7;
                        } else {
                            $id_cuenta = 116;
                            $cuenta_banco = "1.1.1.2.1.1.1497";
                            $nombre_cuenta = "Cta. 0443361497";
                            $id_padre = "1.1.1.2.1.1";
                            $nivel = 7;
                        }

                        if ($cuenta->cuenta_cargo) {

                            if (strpos(strtolower($cuenta->nombre_cargo), "i.v.a.") !== FALSE || strpos(strtolower($cuenta->nombre_cargo), "i.v.a") !== FALSE || strpos(strtolower($cuenta->nombre_abono), 'iva') !== FALSE) {
                                if ($row->iva !== 0) {
                                    $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                    $query_cargo = $this->ci->db->query($sql_cargo, array($cuenta->cuenta_cargo));
                                    $resultado_cuenta_cargo = $query_cargo->row();

                                    $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                    $datos_cargo = array(
                                        $last,
                                        'Egresos',
                                        $cuenta->id_correlacion_partidas_contables,
                                        $cuenta->cuenta_cargo,
                                        $resultado_cuenta_cargo->id_padre,
                                        $resultado_cuenta_cargo->nivel,
                                        $datos_caratula_contrarecibo->concepto,
                                        $row->iva,
                                        0.0,
                                        $resultado_fuente->descripcion,
                                        $cuenta->nombre_cargo,
                                        $centro_costo,
                                        $partida,
                                    );

                                    $resultado_insertar_cargo_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                    $cargos += $row->iva;

                                    $total_filas += 1;

                                }
                            } elseif (strpos(strtolower($cuenta->nombre_cargo), "presupuesto de egresos") !== FALSE) {
                                $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_cargo = $this->ci->db->query($sql_cargo, array($cuenta->cuenta_cargo));
                                $resultado_cuenta_cargo = $query_cargo->row();

                                $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_cargo = array(
                                    $last,
                                    'Egresos',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_cargo,
                                    $resultado_cuenta_cargo->id_padre,
                                    $resultado_cuenta_cargo->nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    $row->importe,
                                    0.0,
                                    $resultado_fuente->descripcion,
                                    $cuenta->nombre_cargo,
                                    $centro_costo,
                                    $partida,
                                );

                                $resultado_insertar_cargo_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                $cargos += $row->importe;

                                $total_filas += 1;
                            } elseif (strpos(strtolower($cuenta->nombre_cargo), "banco") !== FALSE) {

                                $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_cargo = array(
                                    $last,
                                    'Egresos',
                                    $id_cuenta,
                                    $cuenta_banco,
                                    $id_padre,
                                    $nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    $row->subtotal,
                                    0.0,
                                    $resultado_fuente->descripcion,
                                    $nombre_cuenta,
                                    $centro_costo,
                                    $partida,
                                );

                                $resultado_insertar_cargo_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                $cargos += $row->subtotal;

                                $total_filas += 1;
                            } else {
                                $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_cargo = $this->ci->db->query($sql_cargo, array($cuenta->cuenta_cargo));
                                $resultado_cuenta_cargo = $query_cargo->row();

                                $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_cargo = array(
                                    $last,
                                    'Egresos',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_cargo,
                                    $resultado_cuenta_cargo->id_padre,
                                    $resultado_cuenta_cargo->nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    $row->importe,
                                    0.0,
                                    $resultado_fuente->descripcion,
                                    $cuenta->nombre_cargo,
                                    $centro_costo,
                                    $partida,
                                );

                                $resultado_insertar_cargo_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                $cargos += $row->importe;

                                $total_filas += 1;
                            }

                        }

                        if ($cuenta->cuenta_abono) {

                            if (strpos(strtolower($cuenta->nombre_abono), 'i.v.a.') !== FALSE || strpos(strtolower($cuenta->nombre_abono), 'i.v.a') !== FALSE || strpos(strtolower($cuenta->nombre_abono), 'iva') !== FALSE && $row->iva > 0) {
                                $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_abono = $this->ci->db->query($sql_abono, array($cuenta->cuenta_abono));
                                $resultado_cuenta_abono = $query_abono->row();

                                $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta, cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_abono = array(
                                    $last,
                                    'Egresos',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_abono,
                                    $resultado_cuenta_abono->id_padre,
                                    $resultado_cuenta_abono->nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    0.0,
                                    $row->iva,
                                    $resultado_fuente->descripcion,
                                    $cuenta->nombre_abono,
                                    $centro_costo,
                                    $partida,
                                );

                                $resultado_insertar_abono_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                $abonos += $row->iva;

                                $total_filas += 1;

                            } elseif (strpos(strtolower($cuenta->nombre_abono), "presupuesto de egresos") !== FALSE) {
                                $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_abono = $this->ci->db->query($sql_abono, array($cuenta->cuenta_abono));
                                $resultado_cuenta_abono = $query_abono->row();

                                $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta, cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_abono = array(
                                    $last,
                                    'Egresos',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_abono,
                                    $resultado_cuenta_abono->id_padre,
                                    $resultado_cuenta_abono->nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    0.0,
                                    $row->importe,
                                    $resultado_fuente->descripcion,
                                    $cuenta->nombre_abono,
                                    $centro_costo,
                                    $partida,
                                );

                                $resultado_insertar_abono_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                $abonos += $row->importe;

                                $total_filas += 1;
                            } elseif (strpos(strtolower($cuenta->nombre_abono), "banco") !== FALSE) {

                                $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta, cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_abono = array(
                                    $last,
                                    'Egresos',
                                    $id_cuenta,
                                    $cuenta_banco,
                                    $id_padre,
                                    $nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    0.0,
                                    $row->importe,
                                    $resultado_fuente->descripcion,
                                    $nombre_cuenta,
                                    $centro_costo,
                                    $partida,
                                );

                                $resultado_insertar_abono_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                $abonos += $row->importe;

                                $total_filas += 1;
                            } else {
                                $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_abono = $this->ci->db->query($sql_abono, array($cuenta->cuenta_abono));
                                $resultado_cuenta_abono = $query_abono->row();

                                $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta, cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_abono = array(
                                    $last,
                                    'Egresos',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_abono,
                                    $resultado_cuenta_abono->id_padre,
                                    $resultado_cuenta_abono->nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    0.0,
                                    $row->importe,
                                    $resultado_fuente->descripcion,
                                    $cuenta->nombre_abono,
                                    $centro_costo,
                                    $partida,
                                );

                                $resultado_insertar_abono_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                $abonos += $row->importe;

                                $total_filas += 1;
                            }

                        }
                    }
                }
            }

            $datos_actualizar_caratula = array(
                'no_partidas' => $total_filas,
                'cargos' => $cargos,
                'abonos' => $abonos,
            );

            $this->ci->db->where('numero_poliza', $last);
            $this->ci->db->update('mov_polizas_cabecera', $datos_actualizar_caratula);

            $datos_actualizar_movimiento = array(
                'poliza' => 1,
            );

            $this->ci->db->where('movimiento', $movimiento);
            $this->ci->db->update('mov_bancos_movimientos', $datos_actualizar_movimiento);

            $this->ci->db->trans_commit();

            $respuesta = array(
                "mensaje" => '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>La p�liza se ha generado con el n�mero: '.$last.'.</div>',
            );

            return $respuesta["mensaje"];

        } catch (Exception $e) {
            $this->ci->db->trans_rollback();

            $this->ci->db->where('numero_poliza', $last);
            $this->ci->db->delete('mov_poliza_detalle');

            $this->ci->db->where('numero_poliza', $last);
            $this->ci->db->delete('mov_polizas_cabecera');


            $respuesta = array(
                "mensaje" => '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$e->getMessage().'.</div>',
            );

            return $respuesta["mensaje"];
        }

    }

    function generar_poliza_ingresos($numero) {
        $recaudado = $numero;

        try {
            $id = $this->ci->tank_auth->get_user_id();

            $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
            $resultado_query_usuario = $this->ci->db->query($query_usuario, array($id));
            $nombre_encontrado = $resultado_query_usuario->row();
            $nombre_completo = $nombre_encontrado->nombre." ".$nombre_encontrado->apellido_paterno." ".$nombre_encontrado->apellido_materno;

//        Se inicizaliza la variable con el ultimo valor de las polizas
            $last = 0;
//        Se toma el numero de la ultima poliza
            $ultimo = $this->ci->ciclo_model->ultima_poliza();

//        Se le suma uno al ultimo valor de las polizas
            if($ultimo) {
                $last = $ultimo->ultimo + 1;
            }
//        De lo contrario se inicia en 1
            else {
                $last = 1;
            }

            $query = "SELECT * FROM mov_recaudado_caratula WHERE numero = ?";
            $resultado = $this->ci->recaudacion_model->get_datos_devengado_caratula($recaudado, $query);

//            $this->debugeo->imprimir_pre($resultado);

            $resultado_detalle = $this->ci->recaudacion_model->get_datos_recaudado_detalle_automatico($recaudado);
            $devengado = $resultado->numero_devengado;

//            $this->debugeo->imprimir_pre($resultado_detalle);

//        Se crea un arreglo donde se van a guardar los resultados del detalle del compromiso
            $datos_detalle_poliza = array();

            if (!$resultado_detalle) {
                throw new Exception('No hay movimientos dentro del devengado.');
            }

            $centro_recaudacion = '';
            $clase = '';
            $nombre_fuente_financiamiento = '';
            $total_filas = 0;
            $cargos = 0;
            $abonos = 0;

            $this->ci->db->trans_begin();

            $this->ci->db->select('numero_poliza')->from('mov_polizas_cabecera')->where('no_devengado', $devengado);
            $query_numero_devengado_diario = $this->ci->db->get();
            $numero_devengado_diario = $query_numero_devengado_diario->row_array();

            $query_insertar_caratula_poliza = "INSERT INTO mov_polizas_cabecera (numero_poliza, tipo_poliza, fecha, fecha_real, concepto, enfirme, sifirme, no_partidas, importe, cancelada, estatus, creado_por, autorizada, cargos, abonos, no_devengado, no_recaudado, concepto_especifico, clave_cliente, cliente, no_movimiento, poliza_devengado) VALUES (?, ?, ?, NOW(), ?, 0, 0, ?, ?, 0, ?, ?, 0, ?, ?, ?, ?, ?, ?, ?, ? ,?);";
            $datos_caratula_poliza = array(
                $last,
                'Ingresos',
                $resultado->fecha_solicitud,
                $resultado->descripcion,
                0, //$total_filas
                $resultado->importe_total,
                'espera',
                $nombre_completo,
                0, //$cargos
                0, //$abonos
                $devengado,
                $recaudado,
                $resultado->descripcion,
                $resultado->clave_cliente,
                $resultado->cliente,
                $resultado->no_movimiento,
                $numero_devengado_diario["numero_poliza"],
            );

            $resultado_insertar_caratula = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_caratula_poliza, $datos_caratula_poliza);

            foreach ($resultado_detalle as $row) {
//                $this->debugeo->imprimir_pre($row);

                $estructura = json_decode($row->estructura);

//                $this->debugeo->imprimir_pre($estructura);

                $centro_recaudacion = $estructura->centro_de_recaudacion;
                $clase = $estructura->clase;

                $datos_cuentas = $this->ci->recaudacion_model->tomar_cuentas_recaudacion($centro_recaudacion);

                foreach ($datos_cuentas as $cuenta) {

                    if (!$cuenta) {
                        throw new Exception('No existe cuenta contable asociada a la partida.');
                    }

//                    $this->debugeo->imprimir_pre($cuenta);

                    if ($cuenta->cuenta_cargo) {
                        if (strpos(strtolower($cuenta->nombre_cargo), "i.v.a.") !== FALSE || strpos(strtolower($cuenta->nombre_cargo), "i.v.a") !== FALSE || strpos(strtolower($cuenta->nombre_abono), "iva") !== FALSE) {
                            if ($row->iva !== 0) {
                                $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_cargo = $this->ci->db->query($sql_cargo, array($cuenta->cuenta_cargo));
                                $resultado_cuenta_cargo = $query_cargo->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_cargo);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_cargo.' '.$cuenta->nombre_cargo);
                                }

                                $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida, concepto_especifico) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_cargo = array(
                                    $last,
                                    'Ingresos',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_cargo,
                                    $resultado_cuenta_cargo->id_padre,
                                    $resultado_cuenta_cargo->nivel,
                                    $resultado->descripcion,
                                    $row->iva,
                                    0.0,
                                    4,
                                    $cuenta->nombre_cargo,
                                    $centro_recaudacion,
                                    $clase,
                                    0,
                                );

                                $resultado_insertar_cargo_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                $cargos += $row->iva;

                                $total_filas += 1;
                            }
                        }
                        else {
                            $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                            $query_cargo = $this->ci->db->query($sql_cargo, array($cuenta->cuenta_cargo));
                            $resultado_cuenta_cargo = $query_cargo->row();

                            $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_cargo);

                            if ($existe_cuenta == FALSE) {
                                throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_cargo.' '.$cuenta->nombre_cargo);
                            }

                            $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida, concepto_especifico) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                            $datos_cargo = array(
                                $last,
                                'Ingresos',
                                $cuenta->id_correlacion_partidas_contables,
                                $cuenta->cuenta_cargo,
                                $resultado_cuenta_cargo->id_padre,
                                $resultado_cuenta_cargo->nivel,
                                $resultado->descripcion,
                                $row->total_importe,
                                0.0,
                                4,
                                $cuenta->nombre_cargo,
                                $centro_recaudacion,
                                $clase,
                                0,
                            );

                            $resultado_insertar_cargo_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                            $cargos += $row->total_importe;

                            $total_filas += 1;
                        }
                    }

                    if ($cuenta->cuenta_abono) {
                        if (strpos(strtolower($cuenta->nombre_abono), "i.v.a.") !== FALSE || strpos(strtolower($cuenta->nombre_abono), "i.v.a") !== FALSE || strpos(strtolower($cuenta->nombre_abono), "iva") !== FALSE) {
                            if ($row->iva !== 0) {
                                $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_abono = $this->ci->db->query($sql_abono, array($cuenta->cuenta_abono));
                                $resultado_cuenta_abono = $query_abono->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_abono);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_abono.' '.$cuenta->nombre_abono);
                                }

                                $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida, concepto_especifico) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_abono = array(
                                    $last,
                                    'Ingresos',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_abono,
                                    $resultado_cuenta_abono->id_padre,
                                    $resultado_cuenta_abono->nivel,
                                    $resultado->descripcion,
                                    0.0,
                                    $row->iva,
                                    4,
                                    $cuenta->nombre_abono,
                                    $centro_recaudacion,
                                    $clase,
                                    0,
                                );

                                $resultado_insertar_abono_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                $abonos += $row->iva;

                                $total_filas += 1;
                            }
                        }
                        else {
                            $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                            $query_abono = $this->ci->db->query($sql_abono, array($cuenta->cuenta_abono));
                            $resultado_cuenta_abono = $query_abono->row();

                            $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_abono);

                            if ($existe_cuenta == FALSE) {
                                throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_abono.' '.$cuenta->nombre_abono);
                            }

                            $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida, concepto_especifico) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                            $datos_abono = array(
                                $last,
                                'Ingresos',
                                $cuenta->id_correlacion_partidas_contables,
                                $cuenta->cuenta_abono,
                                $resultado_cuenta_abono->id_padre,
                                $resultado_cuenta_abono->nivel,
                                $resultado->descripcion,
                                0.0,
                                $row->total_importe,
                                4,
                                $cuenta->nombre_abono,
                                $centro_recaudacion,
                                $clase,
                                0,
                            );

                            $resultado_insertar_abono_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                            $abonos += $row->total_importe;

                            $total_filas += 1;
                        }
                    }
                }
            }

            $datos_actualizar_caratula = array(
                'no_partidas' => $total_filas,
                'cargos' => $cargos,
                'abonos' => $abonos,
            );

            $this->ci->db->where('numero_poliza', $last);
            $this->ci->db->update('mov_polizas_cabecera', $datos_actualizar_caratula);

            $datos_actualizar_recaudado = array(
                'poliza' => 1,
            );

            $this->ci->db->where('numero', $recaudado);
            $this->ci->db->update('mov_recaudado_caratula', $datos_actualizar_recaudado);

            $this->ci->db->trans_commit();

            $respuesta = array(
                "mensaje" => '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Se ha generado con &eacute;xito la P&oacute;liza de Ingresos No. '.$last.'</div>',
            );

            return $respuesta["mensaje"];

        } catch (Exception $e) {
            $this->ci->db->trans_rollback();

            $respuesta = array(
                "mensaje" => '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$e->getMessage().'.</div>',
            );

            return $respuesta["mensaje"];
        }
    }

    private function _revisar_cuenta($arreglo_cuenta) {
        if(isset($arreglo_cuenta->id_padre))
            return TRUE;
        return false;
    }

}