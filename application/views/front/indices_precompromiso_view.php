<h3 class="page-header center"><i class="fa fa-file"></i> Precompromisos</h3>
<div id="page-wrapper">
    <!--Opción 2-->
    <!--<h3 class="page-header center"><i class="fa fa-file-o"></i> Precompromisos</h3>-->
    <div class="row cont-btns-c center">
        <div class="col-lg-12">
            <?php if($this->utilerias->get_permisos("agregar_precompromiso") || $this->utilerias->get_grupo() == 1) { ?>
                <a href="<?= base_url("ciclo/agregar_precompromiso") ?>" class="btn btn-default"><i class="fa fa-plus circle ic-color"></i> Agregar Precompromiso</a>
            <?php } ?>
            <a href="<?= base_url("plantillas/plantilla_precompromiso.xls") ?>" class="btn btn-default"><i class="fa fa-download ic-color"></i> Descargar Plantilla</a>
            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#subirArchivo" data-whatever="Subir"><i class="fa fa-upload ic-color"></i> Subir Archivo</button>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <ul class="nav nav-tabs">
                <li id= "btn-todas" class="active"><a onclick="BtnTodas()">Todos</a></li>
                <li id= "btn-aprobado" role="presentation"><a onclick="BtnAprobado()">Aprobado</a></li>
                <li id= "btn-pendiente" role="presentation"><a onclick="BtnPendiente()">Pendiente</a></li>
                <li id= "btn-autorizar" role="presentation"><a onclick="BtnAutorizar()">Por Autorizar</a></li>
                <li id= "btn-cancelado" role="presentation"><a onclick="BtnCancelado()">Cancelado</a></li>
                <li id= "btn-terminado" role="presentation"><a onclick="BtnTerminado()">Terminado</a></li>
            </ul>
            <div class="panel panel-default" id="tab-todas">
                <div class="panel-body table-gral">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover datos_tabla ajuste">
                            <thead>
                            <tr>
                                <th width="50">No.</th>
                                <th width="100">Tipo</th>
                                <th width="100">F. Emitido</th>
                                <th width="150">Importe</th>
                                <th width="150">Importe Compromiso</th>
                                <th width="150">Creado Por</th>
                                <th width="60">Firme</th>
                                <th width="100">Estatus</th>
                                <th width="180">Acciones</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="panel panel-default" id="tab-aprobado" style="display: none;">
                <div class="panel-body table-gral">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover datos_tabla_aprobado ajuste" >
                            <thead>
                            <tr>
                                <th width="50">No.</th>
                                <th width="100">Tipo</th>
                                <th width="100">F. Emitido</th>
                                <th width="150">Importe</th>
                                <th width="150">Importe Compromiso</th>
                                <th width="150">Creado Por</th>
                                <th width="60">Firme</th>
                                <th width="100">Estatus</th>
                                <th width="180">Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="panel panel-default" id="tab-pendiente" style="display: none;">
                <div class="panel-body table-gral">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover datos_tabla_pendiente ajuste">
                            <thead>
                            <tr>
                                <th width="50">No.</th>
                                <th width="100">Tipo</th>
                                <th width="100">F. Emitido</th>
                                <th width="150">Importe</th>
                                <th width="150">Importe Compromiso</th>
                                <th width="150">Creado Por</th>
                                <th width="60">Firme</th>
                                <th width="100">Estatus</th>
                                <th width="180">Acciones</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="panel panel-default" id="tab-autorizar" style="display: none;">
                <div class="panel-body table-gral">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover datos_tabla_autorizar ajuste">
                            <thead>
                            <tr>
                                <th width="50">No.</th>
                                <th width="100">Tipo</th>
                                <th width="100">F. Emitido</th>
                                <th width="150">Importe</th>
                                <th width="150">Importe Compromiso</th>
                                <th width="150">Creado Por</th>
                                <th width="60">Firme</th>
                                <th width="100">Estatus</th>
                                <th width="180">Acciones</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="panel panel-default" id="tab-cancelado" style="display: none;">
                <div class="panel-body table-gral">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover datos_tabla_cancelado ajuste">
                            <thead>
                            <tr>
                                <th width="50">No.</th>
                                <th width="100">Tipo</th>
                                <th width="100">F. Emitido</th>
                                <th width="150">Importe</th>
                                <th width="150">Importe Compromiso</th>
                                <th width="150">Creado Por</th>
                                <th width="60">Firme</th>
                                <th width="100">Estatus</th>
                                <th width="180">Acciones</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="panel panel-default" id="tab-terminado" style="display: none;">
                <div class="panel-body table-gral">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover datos_tabla_terminado ajuste">
                            <thead>
                            <tr>
                                <th width="50">No.</th>
                                <th width="100">Tipo</th>
                                <th width="100">F. Emitido</th>
                                <th width="150">Importe</th>
                                <th width="150">Importe Compromiso</th>
                                <th width="150">Creado Por</th>
                                <th width="60">Firme</th>
                                <th width="100">Estatus</th>
                                <th width="180">Acciones</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>

<div class="modal fade modal_borrar" tabindex="-1" role="dialog" aria-labelledby="modal_borrar" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-remove ic-modal"></i> Cancelar Precompromiso</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="message-text" class="control-label">¿Realmente desea cancelar el precompromiso seleccionado?</label>
                        <input type="hidden" value="" name="cancelar_precompromiso" id="cancelar_precompromiso" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-reply ic-color"></i> Regresar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_cancelar_precompromiso">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_resultado_borrar" tabindex="-1" role="dialog" aria-labelledby="modal_resultado_borrar" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-remove ic-modal"></i> Cancelar Precompromiso</h4>
            </div>
            <div class="modal-body">
                <div id="resultado_borrar"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-green" data-dismiss="modal" >Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="subirArchivo" tabindex="-1" role="dialog" aria-labelledby="subirArchivo" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-file ic-color"></i> Subir Archivo de Precompromiso</h4>
            </div>
            <div class="modal-body">
                <?php
                $attributes = array(
                    'role' => 'form',
                );

                echo(form_open_multipart('ciclo/subir_precompromiso', $attributes));
                ?>
                <div class="form-group center">
                    <div class="fileUpload btn btn-default btn-size">
                        <span>Selecciona Archivo</span>
                        <?php
                        //echo(form_label('', 'cammpoArchivo'));
                        $data = array(
                            'name'        => 'archivoSubir',
                            'id'          => 'archivoSubir',
                            'class'       => 'upload',
                            'accept' => 'application/vnd.ms-excel',
                            'required' => 'required',
                        );

                        echo(form_upload($data));
                        ?>
                    </div>
                    <p class="help-block">El archivo a subir debe de tener la extensión .XLS</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-reply ic-color"></i> Regresar</button>
                <?php
                $atributos_submit = array(
                    'class' => 'btn btn-green',
                    'required' => 'required',
                );

                echo(form_submit($atributos_submit, 'Subir Archivo'));
                ?>
                <?php echo(form_close()); ?>
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal_solicitar_terminar" tabindex="-1" role="dialog" aria-labelledby="modal_solicitar_terminar" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-flag ic-modal"></i> Solicitar Terminar Precompromiso</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="message-text" id="mensaje_solicitud_terminar" class="control-label">¿Realmente desea dar por terminado el precompromiso seleccionado?</label>
                        <input type="hidden" value="" name="solicitar_terminar_hidden" id="solicitar_terminar_hidden" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-reply ic-color"></i> Regresar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_solicitar_terminar">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal_terminar" tabindex="-1" role="dialog" aria-labelledby="modal_terminar" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-archive ic-modal"></i> Terminar Precompromiso</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="message-text" id="mensaje_terminar" class="control-label">¿Realmente desea dar por terminado el precompromiso seleccionado?</label>
                        <input type="hidden" value="" name="terminar_hidden" id="terminar_hidden" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-reply ic-color"></i> Regresar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_terminar">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade resultado" tabindex="-1" role="dialog" aria-labelledby="resultado" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-archive ic-modal"></i> Terminar Precompromiso</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="message-text" id="mensaje_resultado" class="control-label"></label>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

</div>
<!-- /.row -->
</div>
<!-- /#page-wrapper -->