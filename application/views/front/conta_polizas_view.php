<h3 class="page-header title center"><i class="fa fa-leanpub"></i> Asientos / Pólizas</h3>
<div id="page-wrapper">
    <div class="row cont-btns-c center">
        <div class="col-lg-12">
            <?php if($this->utilerias->get_permisos("agregar_polizas") || $this->utilerias->get_grupo() == 1){ ?>
                <a href="<?= base_url("contabilidad/agregarPoliza")?>" class="btn btn-default"><i class="fa fa-plus-circle circle ic-color"></i> Agregar Asiento / Póliza</a>
            <?php } ?>
            <a href="<?= base_url("contabilidad/exportarPolizas")?>" class="btn btn-default"><i class="fa fa-plus-circle circle ic-color"></i> Exportar Pólizas</a>
            <a href="<?= base_url("contabilidad/ponerPolizasEnFirme")?>" class="btn btn-default"><i class="fa fa-plus-circle circle ic-color"></i> Poner Pólizas En Firme</a>
<!--            <a href="#" class="btn btn-default" data-toggle="modal" data-target="#modal_agregar"><i class="fa fa-download circle ic-color"></i> Exportar Asiento Modelo</a>-->
<!--            <a href="#" class="btn btn-default" data-toggle="modal" data-target="#modal_agregar"><i class="fa fa-upload circle ic-color"></i> Importar Asiento Modelo</a>-->
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <ul class="nav nav-tabs">
                <li id= "btn-todas" class="active"><a onclick="BtnTodas()">Todas</a></li>
                <li id= "btn-diario" role="presentation"><a onclick="BtnDiario()">Diario</a></li>
                <li id= "btn-ingresos" role="presentation"><a onclick="BtnIngresos()">Ingresos</a></li>
                <li id= "btn-egresos" role="presentation"><a onclick="BtnEgresos()">Egresos</a></li>
            </ul>
            <div class="panel panel-default" id="tab-todas">
                <div class="panel-body table-gral">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover datos_tabla ajuste">
                            <thead>
                                <tr>
                                    <th width="8%">No.</th>
                                    <th width="11%">Tipo Póliza</th>
                                    <th width="10%">Fecha</th>
                                    <th width="15%">Factura</th>
                                    <th width="15%">Importe</th>
                                    <th width="16%">Creado Por</th>
                                    <th width="7%">Firme</th>
                                    <th width="5%">Estatus</th>
                                    <th width="13%">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="panel panel-default" id="tab-diario" style="display: none;">
                <div class="panel-body table-gral">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover datos_tabla_diario ajuste" >
                            <thead>
                            <tr>
                                <th width="30">No.</th>
                                <th width="40">Contra Recibo / Devengado</th>
                                <th width="60">Tipo Póliza</th>
                                <th width="50">Fecha</th>
                                <th width="50">Factura</th>
                                <th width="100">Importe</th>
                                <th width="100">Creado Por</th>
                                <th width="30">Firme</th>
                                <th width="40">Estatus</th>
                                <th width="100">Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="panel panel-default" id="tab-ingresos" style="display: none;">
                <div class="panel-body table-gral">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover datos_tabla_ingresos ajuste">
                            <thead>
                            <tr>
                                <th width="30">No.</th>
                                <th width="30">No. Recudado</th>
                                <th width="60">Tipo Póliza</th>
                                <th width="50">Fecha</th>
                                <th width="50">Factura</th>
                                <th width="100">Importe</th>
                                <th width="100">Creado Por</th>
                                <th width="30">Firme</th>
                                <th width="40">Estatus</th>
                                <th width="100">Acciones</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="panel panel-default" id="tab-egresos" style="display: none;">
                <div class="panel-body table-gral">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover datos_tabla_egresos ajuste">
                            <thead>
                            <tr>
                                <th width="40">No.</th>
                                <th width="40">M. Bancario</th>
                                <th width="60">Tipo Póliza</th>
                                <th width="60">Fecha</th>
                                <th width="35">Factura</th>
                                <th width="100">Importe</th>
                                <th width="100">Creado Por</th>
                                <th width="35">Firme</th>
                                <th width="40">Estatus</th>
                                <th width="100">Acciones</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal_borrar" tabindex="-1" role="dialog" aria-labelledby="modal_borrar" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-remove ic-modal"></i> Cancelar póliza</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="message-text" class="control-label">¿Realmente desea cancelar la póliza seleccionada?</label>
                        <input type="hidden" value="" name="cancelar_poliza" id="cancelar_poliza" />
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_cancelar_poliza">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal_regenerar" tabindex="-1" role="dialog" aria-labelledby="modal_regenerar" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-remove ic-modal"></i> Volver a Generar Póliza</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="message-text" class="control-label">Se va a volver a generar la póliza seleccionada</label>
                        <input type="hidden" value="" name="regenerar_poliza" id="regenerar_poliza" />
                    </div>
                    <div id="resultado_regenerar_poliza">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-green" id="elegir_regenerar_poliza">Aceptar</button>
            </div>
        </div>
    </div>
</div>