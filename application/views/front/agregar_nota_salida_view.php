<h3 class="page-header title center"><i class="fa fa-plus-circle"></i> Agregar Nota de Salida</h3>
<div id="page-wrapper">
    <form class="forma_nota" role="form">
        <div class="row add-pre error-gral">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        General
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <input type="hidden" name="ultima_nota" id="ultima_nota" value="<?= $ultima_nota ?>">
                        <div class="row">
                            <!--Primera Columna-->
                            <div class="col-lg-3">
                                <!--No. Nota de Entrada-->
                                <div class="row">
                                    <div class="col-lg-6"><label>No. Nota Salida</label></div>
                                    <div class="col-lg-6"><p class="form-control-static input_ver"><?= $ultima_nota ?></p></div>
                                </div>
                                <!-- Centro de Costos -->
                                <div class="form-group input-group">
                                    <input type="text" class="form-control" name="centro_costos" id="centro_costos" placeholder="Centro de Costos" required/>
                                    <span class="input-group-btn ic-buscar-btn">
                                        <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_centro_costo"><i class="fa fa-search"></i></button>
                                    </span>
                                </div>
                                <!-- Descripción Centro de Costos -->
                                <input type="hidden" name="descripcion_centro_costos" id="descripcion_centro_costos" />
                                <!--Concepto de Salida-->
                                <select class="form-control" id="tipo_concepto" name="tipo_concepto" required>
                                    <option value="">Concepto de Salida</option>
                                    <option value="Regular">Regular</option>
                                    <option value="Deterioro">Deterioro</option>
                                    <option value="Robo">Robo</option>
                                    <option value="Inutilizable">Inutilizable</option>
                                    <option value="Defecto de Fábrica">Defecto de Fábrica</option>
                                    <option value="Devolución">Devolución</option>
                                    <option value="Otro">Otro</option>
                                </select>
                                <!-- Otro Tipo Concepto -->
                                <input type="text" class="form-control" name="tipo_concepto_otro" id="tipo_concepto_otro" placeholder="Descripción Otro" style="display:none;">
                            </div>
                            <!--Fin Primera Columna-->

                            <!--Segunda Columna-->
                            <div class="col-lg-6">
                                <!-- Condiciones de Entrega -->
                                <textarea style="height: 8em; margin-top: .5%;" class="form-control" id="observaciones" name="observaciones" placeholder="Observaciones" required></textarea>

                            </div>
                            <!--Fin Segunda Columna-->
                            <!--Tercera Columna-->
                            <div class="col-lg-3">
                                <!-- Fecha -->
                                <input type="text" class="form-control ic-calendar" name="fecha" id="fecha" placeholder="Fecha Emisión">
                            </div>
                            <!--Fin Tercera Columna-->

                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="row add-pre error-gral">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Detalle Nota de Salida
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-3"></div>
                            <div class="col-lg-5">
                                <div class="form-group input-group" >
                                    <input type="hidden" name="id_nota_entrada" id="id_nota_entrada" />
                                    <input type="text" class="form-control" name="producto_nota_entrada" id="producto_nota_entrada" placeholder="Productos Nota de Entrada" readonly="readonly" />
                                        <span class="input-group-btn ic-buscar-btn">
                                            <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_nota_entrada"><i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                </div>
                            </div>
                            <div class="col-lg-4"></div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3 niveles-pc">
                                <!--Estructura Administrativa de Egresos-->
                                <?= $niveles ?>
                                <a href="#modal_estructura" class="btn btn-default" data-toggle="modal" data-target="#modal_estructura">¿No conoces la
                                    <br/>estructura?</a>
                            </div>
                            <div class="col-lg-5"></div>
                            <div class="col-lg-4">
                                <div class="form-group input-group">
                                    <input type="text" class="form-control" style="margin-top: -.5%;" name="gasto" id="gasto" placeholder="Gasto" required/>
                                    <span class="input-group-btn ic-buscar-btn" >
                                        <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_gasto"><i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                </div>

                                <!--
                                <div class="form-group input-group" >
                                    <input type="hidden" name="id_nota_entrada" id="id_nota_entrada" />
                                    <input type="text" class="form-control" name="no_nota_entrada" id="no_nota_entrada"  style="margin-top: -1%;" placeholder="No. Nota de Salida" disabled/>
                                    <span class="input-group-btn ic-buscar-btn">
                                        <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_nota_entrada"><i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                </div>
                                -->

                                <input id="titulo_gasto" class="form-control" name="titulo_gasto" placeholder="Título" required />

                                <input type="text" class="form-control" name="u_medida" id="u_medida" placeholder="Unidad de Medida" required />

                                <input type="text" pattern="[0-9]+" class="form-control" name="cantidad" id="cantidad" placeholder="Cantidad" required>

                                <div class="form-group input-group">
                                    <span class="input-group-btn ic-peso-btn">
                                        <button class="btn btn-default sg-dollar" ><i class="fa fa-dollar"></i>
                                        </button>
                                    </span>
                                    <input type="text" pattern="[0-9]+([\.|,][0-9]+)?" class="form-control dinero" name="precio" id="precio" placeholder="Precio Unitario" required />
                                </div>

                                <textarea class="form-control" id="descripcion_detalle" name="descripcion_detalle" placeholder="Descripción" required></textarea>

                                <!-- style="height: 6em;" -->
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <br />
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <button type="button" class="btn btn-green btn-block btn-gd" id="guardar_nota_detalle">Guardar Detalle de Nota</button>
                <div id="resultado_insertar_detalle"></div>
            </div>
        </div>
    </form>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body table-gral">
                    <div class="table-responsive">

                        <h4 id="suma_total" class="text-center"></h4>
                        <input type="hidden" value="" name="importe_total" id="importe_total" />

                        <table class="table table-striped table-bordered table-hover" id="tabla_detalle">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>F.F.</th>
                                <th>P.F.</th>
                                <th>C.C.</th>
                                <th>Capítulo</th>
                                <th>Concepto</th>
                                <th>Partida</th>
                                <th>Artículo / CUCOP</th>
                                <th>U/M</th>
                                <th>Cantidad</th>
                                <th>Precio U.</th>
                                <th>Subtotal</th>
                                <th>Importe</th>
                                <th>Título</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-body text-center">
            <div class="form-group c-firme">
                <h3>¿Nota de Salida en Firme?</h3>
                <label class="checkbox-inline">
                    <input type="checkbox" id="check_firme" name="check_firme" />Sí
                </label>
            </div>
        </div>
    </div>
    <div class="btns-finales text-center">
        <div class="text-center" id="resultado_insertar_caratula"></div>
        <a class="btn btn-default" href="<?= base_url("patrimonio/nota_salida") ?>"><i class="fa fa-reply ic-color"></i> Regresar</a>
        <a id="guardar_nota" class="btn btn-green">Guardar Nota de Salida</a>
    </div>


</div>
</div>
<!-- Modal Gasto -->
<div class="modal fade" id="modal_gasto" tabindex="-1" role="dialog" aria-labelledby="modal_gasto" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-usd"></span> Gastos</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-gasto">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tabla_gastos">
                        <thead>
                        <tr>
                            <th>Clave</th>
                            <th>Clasificación</th>
                            <th>Descripción</th>
                            <th>U/M</th>
                            <th>Existencia</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" disabled>Elegir</button>
            </div>
        </div>
    </div>
</div>

</div>
<!-- /.row -->
</div>

<!-- Modal Producto Nota de Entrada -->
<div class="modal fade" id="modal_nota_entrada" tabindex="-1" role="dialog" aria-labelledby="modal_nota_entrada" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-tasks ic-color"></i> Producto Nota de Entrada</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-gasto">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tabla_nota_entrada">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Artículo</th>
                            <th>Título</th>
                            <th>U/M</th>
                            <th>Cantidad</th>
                            <th>Precio Unitario</th>
                            <th>Descripción</th>
                            <th>Fuente Financiamiento</th>
                            <th>Programa Financiamiento</th>
                            <th>Centro Costos</th>
                            <th>Capitulo</th>
                            <th>Concepto</th>
                            <th>Partida</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-reply ic-color"></i> Regresar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" disabled>Elegir</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal estructura -->
<div class="modal fade" id="modal_estructura" tabindex="-1" role="dialog" aria-labelledby="modal_estructura" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-indent-left"></span> Estructura de Egresos</h4>
            </div>
            <div class="modal-body">
                <?= $niveles_modal ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" disabled id="elegir_ultimoNivel">Elegir</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Borrar Detalle -->
<div class="modal fade modal_borrar" tabindex="-1" role="dialog" aria-labelledby="modal_borrar" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-trash ic-modal"></i> Eliminar Detalle</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="message-text" class="control-label">¿Realmente desea eliminar el detalle seleccionado?</label>
                        <input type="hidden" value="" name="borrar_nota_hidden" id="borrar_nota_hidden" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_borrar_nota">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Centros de Costo -->
<div class="modal fade" id="modal_centro_costo" tabindex="-1" role="dialog" aria-labelledby="modal_centro_costo" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-users ic-modal"></i> Centro de Costo</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-3">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tabla_centro_costos">
                        <thead>
                        <tr>
                            <th>Centro de Costo</th>
                            <th>Nombre</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

</div>

