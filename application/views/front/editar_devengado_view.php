<?php
$this->db->select('cuenta, banco, moneda')->from('cat_cuentas_bancarias');
$query = $this->db->get();
$bancos = $query->result();
?>
<h3 class="page-header title center"><i class="fa fa-edit"></i> Editar Devengado</h3>
<div id="page-wrapper">
    <form class="forma_devengado" role="form">
        <div class="row add-pre error-gral">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        General
                    </div>
                    <div class="panel-body">
                        <input type="hidden" name="ultimo" id="ultimo" value="<?= $ultimo ?>">
                        <div class="row">
                            <!--Primer Columna-->
                            <div class="col-lg-4">
                                <!---No. Consecutivo-->
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-6"><label>No. Devengado</label></div>
                                        <div class="col-lg-6"><p class="form-control-static input_ver"><?= $ultimo ?></p></div>
                                    </div>
                                </div>
                                <!--Clasificación-->
                                <select class="form-control" id="clasificacion" name="clasificacion">
                                    <option value="">Clasificación</option>
                                    <option value="Impuestos" <?= $clasificacion == 'Impuestos' ? ' selected="selected"' : '';?>>Impuestos</option>
                                    <option value="Derechos" <?= $clasificacion == 'Derechos' ? ' selected="selected"' : '';?>>Derechos</option>
                                    <option value="Productos y/o Bienes" <?= $clasificacion == 'Productos y/o Bienes' ? ' selected="selected"' : '';?>>Productos y/o Bienes</option>
                                    <option value="Servicios" <?= $clasificacion == 'Servicios' ? ' selected="selected"' : '';?>>Servicios</option>
                                    <option value="Aprovechamientos" <?= $clasificacion == 'Aprovechamientos' ? ' selected="selected"' : '';?>>Aprovechamientos</option>
                                    <option value="Participaciones" <?= $clasificacion == 'Participaciones' ? ' selected="selected"' : '';?>>Participaciones</option>
                                    <option value="Aportaciones" <?= $clasificacion == 'Aportaciones' ? ' selected="selected"' : '';?>>Aportaciones</option>
                                    <option value="Financiamientos" <?= $clasificacion == 'Financiamientos' ? ' selected="selected"' : '';?>>Financiamientos</option>
                                    <option value="Presupuesto" <?= $clasificacion == 'Presupuesto' ? ' selected="selected"' : '';?>>Presupuesto</option>
                                    <option value="Transferencias y/o Subsidios" <?= $clasificacion == 'Transferencias y/o Subsidios' ? ' selected="selected"' : '';?>>Transferencias y/o Subsidios</option>
                                    <option value="Otros" <?= $clasificacion == 'Otros' ? ' selected="selected"' : '';?>>Otros</option>
                                </select>
                                <!--No. Factura-->
                                <input type="text" class="form-control" name="num_movimiento" id="num_movimiento" placeholder="No. Factura"  <?= $no_movimiento != NULL ? ' value="'.$no_movimiento.'"' : '';?>/>
                            </div>
                            <!--Fin Primer Columna-->
                            <!--Segunda Columna-->
                            <div class="col-lg-4">
                                <!--Clave Cliente-->
                                <input type="text" class="form-control" name="clave_cliente" id="clave_cliente" placeholder="Clave Cliente"  <?= $clave_cliente != NULL ? ' value="'.$clave_cliente.'"' : '';?>/>
                                <!--Nombre Cliente-->
                                <input type="text" class="form-control" name="cliente" id="cliente" placeholder="Cliente"  <?= $cliente != NULL ? ' value="'.$cliente.'"' : '';?>/>
                                <!--Clave Librería-->
                               <!-- <div class="form-group input-group">
                                    <input type="hidden" name="id_clave_libreria" id="id_clave_libreria">
                                    <input type="text" class="form-control" name="clave_libreria" id="clave_libreria" placeholder="Clave Librería" disabled <?= $clave_libreria != NULL ? ' value="'.$clave_libreria.'"' : '';?> >
                                    <span class="input-group-btn ic-buscar-btn">
                                         <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_librerias"><i class="fa fa-search"></i></button>
                                    </span>
                                </div>-->
                            </div>
                            <!--Fin Segunda Columna-->
                            <!--Tercera Columna-->
                            <div class="col-lg-4">
                                <!--<label>Fecha Solicitud</label>-->
                                <input type="text" class="form-control ic-calendar" name="fecha_solicitud" id="fecha_solicitud" placeholder="Fecha Solicitud" required <?= $fecha_solicitud != NULL ? ' value="'.$fecha_solicitud.'"' : '';?>>
                                <!-- Descripción General-->
                                <textarea class="form-control" name="descripcion" id="descripcion" style="height: 8em;" placeholder="Descripción Devengado" <?= $descripcion != NULL ? '>'.$descripcion.'</textarea>' : '> </textarea>' ;?>
                            </div>
                            <!--Fin Tecera Columna-->
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row add-pre error-detalle">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Detalles
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="row niveles-pc" id="consulta_especifica" name="consulta_especifica" style="margin-left: 1%;">
                                    <!--Estructura Administrativa de ingresos-->
                                    <?= $niveles ?>
                                    <a href="#modal_estructura_ingresos" class="btn btn-default" data-toggle="modal" data-target="#modal_estructura_ingresos" style="margin-top: 0;">¿No conoces la
                                        <br/>estructura?</a>
                                </div>
                            </div>
                            <div class="col-lg-4"></div>
                            <div class="col-lg-4">
                                <!--Fecha -->
                                <input type="text" class="form-control ic-calendar" name="fecha_detalle" id="fecha_detalle" placeholder="Fecha">
                                <!--Subsidio-->
                                <div class="form-group input-group">
                                    <input type="text" class="form-control" name="subsidio" id="subsidio" placeholder="Subsidio" disabled/>
                                    <span class="input-group-btn ic-buscar-btn">
                                         <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_subsidio"><i class="fa fa-search"></i></button>
                                    </span>
                                </div>
                                <!--Cantidad -->
                                <input type="number" class="form-control" name="cantidad" id="cantidad" placeholder="Cantidad">

                                <!-- Forma de Pago -->
                                <select class="form-control" id="tipo_pago" name="tipo_pago" required="">
                                    <option value="">Forma de Pago</option>
                                    <option value="Efectivo">Efectivo</option>
                                    <option value="Tarjeta de Débito">Tarjeta de Débito</option>
                                    <option value="Tarjeta de Crédito">Tarjeta de Crédito</option>
                                    <option value="Puntos">Puntos</option>
                                    <option value="No Identificado">No Identificado</option>
                                </select>

                                <!-- Importe -->
                                <label>Importe</label>
                                <div class="form-group input-group">
                                    <span class="input-group-btn ic-peso-btn">
                                        <button class="btn btn-default" ><i class="fa fa-dollar"></i></button>
                                    </span>
                                    <input style="margin-top: .5%;" type="text" pattern="[0-9]+([\.|,][0-9]+)?" class="form-control dinero" name="importe" id="importe" placeholder="Importe">
                                </div>
                                <!--Descuentos -->
                                <label>Descuento</label>
                                <div class="form-group input-group">
                                    <span class="input-group-btn ic-peso-btn">
                                        <button class="btn btn-default" ><i class="fa fa-dollar"></i></button>
                                    </span>
                                    <input style="margin-top: .5%;" type="text" pattern="[0-9]+([\.|,][0-9]+)?" class="form-control dinero" name="descuento" id="descuentos" placeholder="Descuentos">
                                </div>
                                <!--IVA -->
                                <label>IVA</label>
                                <div class="form-group input-group">
                                    <span class="input-group-btn ic-peso-btn">
                                        <button class="btn btn-default" ><i class="fa fa-dollar"></i></button>
                                    </span>
                                    <input style="margin-top: .5%;" type="text" pattern="[0-9]+([\.|,][0-9]+)?" class="form-control dinero" name="iva" id="iva" placeholder="I.V.A.">
                                </div>
                                <!-- Importe Gravado -->
                                <!--
                                <label>Importe Gravado</label>
                                <div class="form-group input-group">
                                    <span class="input-group-btn ic-peso-btn">
                                        <button class="btn btn-default" ><i class="fa fa-dollar"></i></button>
                                    </span>
                                    <input style="margin-top: .5%;" type="text" pattern="[0-9]+([\.|,][0-9]+)?" class="form-control dinero" name="importe_grabado" id="importe_grabado" placeholder="Importe Gravado" value="0">
                                </div>
                                -->
                                <!--Descuentos -->
                                <!--
                                <label>Descuento Gravado</label>
                                <div class="form-group input-group">
                                    <span class="input-group-btn ic-peso-btn">
                                        <button class="btn btn-default" ><i class="fa fa-dollar"></i></button>
                                    </span>
                                    <input style="margin-top: .5%;" type="text" pattern="[0-9]+([\.|,][0-9]+)?" class="form-control dinero" name="descuento_gravado" id="descuento_gravado" placeholder="Descuentos" value="0">
                                </div>
                                -->
                                <!-- Importe NO Grabado -->
                                <!--
                                <label>Importe NO Gravado</label>
                                <div class="form-group input-group">
                                    <span class="input-group-btn ic-peso-btn">
                                        <button class="btn btn-default" ><i class="fa fa-dollar"></i></button>
                                    </span>
                                    <input style="margin-top: .5%;" type="text" pattern="[0-9]+([\.|,][0-9]+)?" class="form-control dinero" name="importe_no_grabado" id="importe_no_grabado" placeholder="Importe NO Gravado" value="0">
                                </div>
                                -->
                                <!--Descuentos -->
                                <!--
                                <label>Descuento NO Gravado</label>
                                <div class="form-group input-group">
                                    <span class="input-group-btn ic-peso-btn">
                                        <button class="btn btn-default" ><i class="fa fa-dollar"></i></button>
                                    </span>
                                    <input style="margin-top: .5%;" type="text" pattern="[0-9]+([\.|,][0-9]+)?" class="form-control dinero" name="descuento_no_gravado" id="descuento_no_gravado" placeholder="Descuentos" value="0">
                                </div>
                                -->
                                <!-- Cuentas Bancarias-->
                                <!--<select class="form-control" id="cuenta_bancaria" name="cuenta_bancaria" required>
                                    <option value="">Cuenta Bancaria</option>
                                    <?php foreach($bancos as $fila) { ?>
                                        <option value="<?= $fila->cuenta ?>"><?= $fila->cuenta." ".$fila->banco." ".$fila->moneda ?></option>
                                    <?php } ?>
                                </select>-->

                                <!-- Descripción Detalle-->
                                <textarea class="form-control" name="descripcion_detalle" id="descripcion_detalle"  style="height: 20px;" placeholder="Descripción Detalle"></textarea>

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <button type="button" class="btn btn-green btn-block btn-gd" id="guardar_devengado_detalle">Guardar Detalle</button>
                                <div id="resultado_insertar_detalle"></div>
                            </div>
                        </div>

                        <!-- Tabla Detalle -->
                        <!--
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-body table-gral">
                                        <div class="table-responsive">
                                            <h4 id="suma_total" class="text-center"></h4>
                                            <input type="hidden" value="" name="importe_total" id="importe_total" />
                                            <table class="table table-striped table-bordered table-hover" id="tabla_datos_devengado">
                                                <thead>
                                                    <th>ID</th>
                                                    <th width="8%">Gerencia</th>
                                                    <th width="5%">C.C.</th>
                                                    <th width="6%">Rubro</th>
                                                    <th width="5%">Tipo</th>
                                                    <th width="7%">Clase</th>
                                                    <th width="9%">F. Aplicada</th>
                                                    <th width="10%">Forma Pago</th>
                                                    <th width="11%">Subtotal No Gravado</th>
                                                    <th width="11%">Descuento No Gravado</th>
                                                    <th width="11%">Subtotal Gravado</th>
                                                    <th width="11%">Descuento Gravado</th>
                                                    <th width="11%">IVA</th>
                                                    <th width="12%">Importe</th>
                                                    <th width="9%">Acciones</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <input type="hidden" value="" name="borrar_devengado_hidden" id="borrar_devengado_hidden" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        -->
                        <!-- Tabla Detalle -->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-body table-gral">
                                        <div class="table-responsive">
                                            <h4 id="suma_total" class="text-center"></h4>
                                            <input type="hidden" value="" name="importe_total" id="importe_total" />
                                            <table class="table table-striped table-bordered table-hover" id="tabla_datos_devengado">
                                                <thead>
                                                <th>ID</th>
                                                <th width="8%">Gerencia</th>
                                                <th width="5%">C.R.</th>
                                                <th width="6%">Rubro</th>
                                                <th width="5%">Tipo</th>
                                                <th width="6.5%">Clase</th>
                                                <th width="10%">F. Aplicada</th>
                                                <th width="10%">Forma Pago</th>
                                                <th width="6%">Cant.</th>
                                                <th width="11%">Importe</th>
                                                <th width="11%">Descuento</th>
                                                <th width="11%">IVA</th>
                                                <th width="11%">Total</th>
                                                <th width="9%">Acciones</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <input type="hidden" value="" name="borrar_devengado_hidden" id="borrar_devengado_hidden" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-body text-center">
                <div class="form-group c-firme">
                    <h3>¿Devengado en Firme?</h3>
                    <label class="checkbox-inline">
                        <input type="checkbox" id="check_firme" name="check_firme" <?= $enfirme == 1 ? ' checked="checked" disabled' : '';?> />Sí
                    </label>
                </div>
            </div>
        </div>

        <div class="btns-finales text-center">
            <div class="text-center" id="resultado_insertar_caratula"></div>
            <a id="" href="<?= base_url("recaudacion/devengado") ?>" class="btn btn-default"><i class="fa fa-reply ic-color"></i> Regresar</a>
            <input type="submit" id="guardar_devengado" class="btn btn-green" name="guardar_devengado" value="Guardar Devengado"/>
        </div>

    </form>
</div>


<!-- Modal estructura Ingresos-->
<div class="modal fade" id="modal_estructura_ingresos" tabindex="-1" role="dialog" aria-labelledby="modal_estructura_ingresos" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-indent-left"></span> Estructura de Ingresos</h4>
            </div>
            <div class="modal-body text-center">
                <?= $niveles_modal ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-reply ic-color"></i> Regresar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" disabled id="elegir_ultimoNivel">Elegir</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Subsidio-->
<div class="modal fade" id="modal_subsidio" tabindex="-1" role="dialog" aria-labelledby="modal_subsidio" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-indent-left"></span> Subsidio</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-gasto">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tabla_subsidio">
                        <thead>
                        <tr>
                            <th>Código</th>
                            <th>Fuentes de Financiamiento</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-reply ic-color"></i> Regresar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" disabled>Elegir</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Clave Librerías-->
<div class="modal fade" id="modal_librerias" tabindex="-1" role="dialog" aria-labelledby="modal_librerias" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-indent-left"></span> Librerías</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-gasto">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tabla_librerias">
                        <thead>
                        <tr>
                            <th>Clave Centro de Costos</th>
                            <th>Clave Librería</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Borrar Detalle -->
<div class="modal fade modal_borrar" tabindex="-1" role="dialog" aria-labelledby="modal_borrar" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-trash ic-modal"></i> Eliminar Partida</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="message-text" class="control-label">¿Realmente desea eliminar la partida seleccionada?</label>
                        <input type="hidden" value="" name="borrar_devengado_hidden" id="borrar_devengado_hidden" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-reply ic-color"></i> Regresar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_cancelar_devengado">Aceptar</button>
            </div>
        </div>
    </div>
</div>