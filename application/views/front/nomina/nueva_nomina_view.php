<h3 class="page-header center"><i class="fa fa-plus-circle circle ic-color"></i> Nueva N&oacute;mina</h3>
<div id="page-wrapper">
    <form class="forma_empleado" role="form">
        <div class="row add-pre error-gral">
            <div class="col-lg-12">

                <center>
                    <div class="">
                        <div class="row">
                            <div class="">
                                <!--Estructura Administrativa de Egresos-->
                                <div class="col-lg-12">
<!--                                    <a href="--><?//= base_url("nomina/nueva_")?><!--" class="btn btn-default"><i class="fa fa-plus-circle circle ic-color"></i> Nuevo Registro</a>-->
<!--                                    <a href="--><?//= base_url("nomina/exportar_empleados") ?><!--" class="btn btn-default"><i class="fa fa-download ic-color"></i> Exportar Datos</a>-->
                                    <a href="#modal_nueva_nom1" class="btn btn-default" data-toggle="modal" data-target="#modal_nueva_nom1">Datos Extras</a>
                                    <a href="#modal_nueva_nom1" class="btn btn-default" data-toggle="modal" data-target="#modal_nueva_nom1">Datos Extras</a>
                                </div>

                            </div>
                            <div class="col-lg-6">
                                <div id="mes_grafica" class="text-center text-muted"></div>
                                <div id="grafica_partida"></div>
                            </div>
                        </div>
                    </div>
                </center>

                <div class="panel panel-default">


                    <div class="panel-heading">
                        Datos del Personal
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-6"><label>No. Registro</label></div>
                                    <div class="col-lg-6"><p class="form-control-static input_ver">11</p></div>
                                </div>

                                <!-- Nombre -->
                                <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Nombre(s)">
                            </div>
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <!--Apellido Paterno-->
                                        <input type="text" class="form-control" name="apellido_paterno" id="apellido_paterno" placeholder="Apellido Paterno">
                                    </div>
                                    <div class="col-lg-6">
                                        <!--Apellido Materno-->
                                        <input type="text" class="form-control" name="apellido_materno" id="apellido_materno" placeholder="Apellido Materno">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <!--RFC-->
                                        <input type="text" class="form-control" name="rfc" id="rfc" placeholder="R.F.C.">
                                    </div>

                                    <div class="col-lg-6">
                                        <!--CURP-->
                                        <input type="text" class="form-control" name="curp" id="curp" placeholder="Curp">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row add-pre error-gral">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Datos de N&oacute;mina
                    </div>
                    <div class="panel-body">
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-6">
                                    <!-- Fecha -->
                                    <input type="text" class="form-control" name="fecha_nomina" id="fecha_nomina" placeholder="Fecha Nómina">
                                </div>
                                <div class="col-lg-6">
                                    <!-- Año -->
                                    <input type="text" class="form-control" name="año" id="año" placeholder="Año">
                                </div>
                            </div>
                            <!-- Horas Contrato-->
                            <label>Horas Contrato</label>
                            <input type="text" class="form-control" name="horas_contrato" id="horas_contrato" placeholder="Horas Contrato" value='39'/>
                        </div>
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-6">
                                    <!-- Horas Contrato-->
                                    <label>Salario Base</label>
                                    <input type="text" class="form-control" name="salario_base" id="salario_base" placeholder="Salario Base" value='29.26552'/>
                                </div>
                                <div class="col-lg-6">
                                    <!-- Horas Contrato-->
                                    <label>Salario Extra</label>
                                    <input type="text" class="form-control" name="salario_extra" id="salario_extra" placeholder="Salario Extra" value='29.26552'/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <!-- Horas Extra-->
                                    <label>Horas Extra</label>
                                    <input type="text" class="form-control" name="horas_extra" id="horas_extra" placeholder="Horas Extra"/>
                                </div>
                                <div class="col-lg-6">
                                    <!-- Salario Hora Extra-->
                                    <label>Salario Hora Extra</label>
                                    <input type="text" class="form-control" name="salario_hr_extra" id="salario_hr_extra" placeholder="Salario Hora Extra" value="6.35645"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row add-pre error-gral">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Datos Extras
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-6">

                                <!-- Horas Trabajadas -->
                                <input type="text" class="form-control" name="hrs_trabajadas" id="hrs_trabajadas" placeholder="Horas Trabajadas">

                                <!-- Salario Horas -->
                                <label>Salario Horas</label>
                                <input type="text" class="form-control" name="salario_hrs" id="salario_hrs" placeholder="Salario Horas" value="1.56">

                                <!-- Plus Transporte -->
                                <label>Plus Transporte</label>
                                <input type="text" class="form-control" name="plus_transporte" id="plus_transporte" placeholder="Plus Transporte" value="1.56">

                                <!-- Días Festivos -->
                                <h4>Días Festivos</h4>

                                <!-- Hora Festivo-->
                                <input type="text" class="form-control" name="hr_festivo" id="hr_festivo" placeholder="Hora Festivo">

                                <!-- Salario Hora Festivo-->
                                <input type="text" class="form-control" name="salario_hr_festivo" id="salario_hr_festivo" placeholder="Salario Hora Festivo">

                                <!-- Hora Nocturna-->
                                <input type="text" class="form-control" name="hr_nocturna" id="hr_nocturna" placeholder="Hora Nocturna">

                                <!-- Salario Hora Nocturna-->
                                <input type="text" class="form-control" name="salario_hr_nocturna" id="salario_hr_nocturna" placeholder="Salario Hora Nocturna">
                            </div>
                            <div class="col-lg-6">

                                <!-- Retenciones y Deducciones -->
                                <h4>Retenciones y Deducciones</h4>

                                <!-- ISR-->
                                <input type="text" class="form-control" name="isr" id="isr" placeholder="ISR%">

                                <!-- PTU-->
                                <input type="text" class="form-control" name="ptu" id="ptu" placeholder="PTU%">

                                <!-- IMSS -->
                                <input type="text" class="form-control" name="imss" id="imss" placeholder="IMSS">

                                <!-- Sueldo IMSS -->
                                <input type="text" class="form-control" name="sueldo_imss" id="sueldo_imss" placeholder="Sueldo Fijo IMSS">

                                <!-- Subsidio para el Empleo -->
                                <input type="text" class="form-control" name="subsidio_empleo" id="subsidio_empleo" placeholder="Subsidio para el Empleo">

                                <!-- Subs. para Empleo Aplicado -->
                                <input type="text" class="form-control" name="subsidio_empleo_aplicado" id="subsidio_empleo_aplicado" placeholder="Subs. para Empleo Aplicado">


                                <div class="modal fade" id="modal_nueva_nom1" tabindex="-1" role="dialog" aria-labelledby="modal_nueva_nom1" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-indent-left"></span>Salario</h4>
                                            </div>
                                            <div class="modal-body text-center">


                                                <form name="lista" action="" method="post">
                                                    <p></p>
                                                    <div class="col-lg-12">

                                                    <ul align="left">
                                                        <li><input type="checkbox" name="check_list[]" value="impuesto"><label>Impuesto</label><br/></li>
                                                        <li><input type="checkbox" name="check_list[]" value="pension"><label>Pensión Alimenticia</label><br/></li>
                                                        <li><input type="checkbox" name="check_list[]" value="fondoa"><label>Fondo de Ahorro</label><br/></li>
                                                        <li><input type="checkbox" name="check_list[]" value="segurov"><label>Seguro de Vida</label><br/></li>
                                                        <li><input type="checkbox" name="check_list[]" value="cuotas"><label>Cuotas Sindicales (Estatales)</label><br/></li>
                                                        <li><input type="checkbox" name="check_list[]" value="fovissste"><label>Seguros Fovissste</label><br/></li>
                                                        <li><input type="checkbox" name="check_list[]" value="fovissste"><label>Resposabilidades</label><br/></li>
                                                        <li><input type="checkbox" name="check_list[]" value="fovissste"><label>Crédito Maestro</label><br/></li>
                                                    </ul>
                                                </form>


                                                </div>



                                                <label for='person3'>Impuesto Sobre la Renta:</label>  <td>  <input name='hfest' value=''/> </td>
                                                <label for='person3'>Seguro de invalidez y vida:</label>  <td>  <input name='hfest' value=''/> </td><br>
                                                <label for='person3'>Servicios Sociales y Culturales:</label>  <td>  <input name='sfest' value=''/></td>
                                                <label for='person3'>Seguro  de Retiro:</label> <td>  <input name='hfest' value=''/> </td><br>

                                                <!--                                                <label for='person3'>Seguro de Riesgo Profesionales:</label>  <td>  <input name='hfest' value=''/> </td>-->
                                                <!--                                                <label for='person3'>Seguro de Salud de las Pensionados y Fam. Derechohabientes:</label>  <td>  <input name='sfest' value=''/><br>-->
                                                <!--                                                <label for='person3'>Seguro de Salud de los Trab. en Activo y Fam. Derechohabientes:</label>  <td>  <input name='hfest' value=''/> </td>-->
                                                <!--                                                <label for='person3'>Seguro de Retiro Aseguradora Hidalgo:</label>  <td>  <input name='sfest' value=''/><br>-->
                                                <!--                                                <label for='person3'>Ahorro Solidario:</label>  <td>  <input name='hfest' value=''/> </td>-->
                                                <!--                                                <label for='person3'>Desc. Fondo del Ahorro Capitalizable (Fonac):</label>  <td>  <input name='hfest' value=''/> </td>-->
                                                <!--                                                <label for='person3'>Cuotas Sindicales:</label>  <td>  <input name='hfest' value=''/> </td><br>-->
                                                <!--                                                <label for='person3'>Fondo de Ahorro para Aux. de Defuncón:</label>  <td>  <input name='sfest' value=''/></td>-->
                                                <!--                                                <label for='person3'>Rentas Fovissste, Fondo de la vivienda:</label> <td>  <input name='hfest' value=''/> </td><br>-->
                                                <!--                                                <label for='person3'>Seguros Fovissste:</label>  <td>  <input name='hfest' value=''/> </td>-->
                                                <!--                                                <label for='person3'>Prestamos a Corto Plazo del Issste:</label>  <td>  <input name='sfest' value=''/><br>-->
                                                <!--<!--                                                <label for='person3'>518-compensaci&oacute;n por Retiro:</label>  <td>  <input name='sfest' value=''/><br>--
                                                <!--<!--                                                <label for='person3'>520-Prestamo Fondo de Ahorro:</label>  <td>  <input name='hfest' value=''/> </td>-->
                                                <!--<!--                                                <label for='person3'>522-F.A. Aportaci&oacute;n Empleado:</label>  <td>  <input name='sfest' value=''/><br>-->
                                                <!--<!--                                                <label for='person3'>523-F.A. Aportaci&oacute;n Empresa:</label>  <td>  <input name='hfest' value=''/> </td>-->
                                                <!--<!--                                                <label for='person3'>599-Subsidio para el Empleo:</label>  <td>  <input name='sfest' value=''/><br>-->







                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                                <button type="button" class="btn btn-green" data-dismiss="modal" disabled id="elegir_ultimoNivel">Aceptar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>




                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="btns-finales text-center">
        <div class="text-center" id="resultado_insertar_caratula"></div>
        <a class="btn btn-default" href="<?= base_url("nomina/nominas") ?>"><i class="fa fa-reply ic-color"></i> Regresar</a>
        <a id="guardar_nota" class="btn btn-green">Guardar Nómina</a>
    </div>
</div>
</div>




