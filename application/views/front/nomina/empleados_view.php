<h3 class="page-header title center"><i class="fa fa-users ic-color"></i> Personal</h3>
<div id="page-wrapper">
    <div class="row cont-btns-c center">
        <div class="col-lg-12">
            <a href="<?= base_url("nomina/nuevo_empleado")?>" class="btn btn-default"><i class="fa fa-plus-circle circle ic-color"></i> Nuevo Registro</a>
            <a href="<?= base_url("nomina/exportar_empleados") ?>" class="btn btn-default"><i class="fa fa-download ic-color"></i> Exportar Datos</a>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body table-gral">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="datos_tabla">
                            <thead>
                            <tr>
                                <th >Num. de Usuarios</th>

                                <th >RFC</th>
                                <th >Apellido Paterno</th>
                                <th >Apellido Materno</th>
                                <th >Nombre</th>
                                <th >CURP</th>
                                <th >Sexo</th>
                                <th >Fecha Ingreso</th>
                                <th >CRESPO</th>
<!--                                <th >Descripci&oacute;n</th>-->
<!--                                <th >Clave CLUES</th>-->
                                <th >Prog. Subp.</th>
                                <th >Unidad</th>
                                <th >Partida</th>
                                <th >Puesto</th>
                                <th >Proy</th>
                                <th >N&uacute;mero Plaza</th>
                                <th >Descripci&oacute;n del Puesto</th>
                                <th >Rama</th>
                                <th >Fuente de Financiamiento</th>
                                <th >Tipo de Contrato</th>
                                <th >Juridicción</th>
                                <th >Municipio</th>
                                <th >Tipo de Plaza</th>
                                <th >Nacionalidad</th>
                                <th >Días Laborales</th>
                                <th >E-mail</th>
                                <th >Escolaridad</th>
                                <th >Jornada</th>
                                <th >TU</th>
                                <th >CLUES Adscripción Real</th>
                                <th >CLUES Adscripción en Nómina</th>
                                <th >Entidad Federativa de la Plaza</th>
                                <th >Descripción CLUES Real2</th>
                                <th >Descripción CRESPO 2</th>
                                <th >Actividad Principal Desempeño</th>
                                <th >Area/Servicio de trabajo2</th>
                                <th >Instancia de Pertenencia de la Plaza</th>
                                <th >Cédula</th>


                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal_borrar" tabindex="-1" role="dialog" aria-labelledby="modal_borrar" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-remove ic-modal"></i> Cancelar Devengado</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="message-text" class="control-label">¿Realmente desea cancelar el devengado seleccionado?</label>
                        <input type="hidden" value="" name="cancelar_devengado" id="cancelar_devengado" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_cancelar_devengado">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal_recaudar" tabindex="-1" role="dialog" aria-labelledby="modal_recaudar" aria-hidden="true" style="z-index: 1; margin-top: 5%;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-cart-plus ic-modal"></i> Recaudar</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <h5 for="message-text" class="control-label center">Fecha en que se generará el recaudado</h5>
                        <div class="row">
                            <div class="col-lg-3"></div>
                            <div class="col-lg-6"><input type="text" class="form-control ic-calendar" name="fecha_solicitud" id="fecha_solicitud" placeholder="Fecha de Solicitud"></div>
                            <div class="col-lg-3"></div>
                        </div>
                        <input type="hidden" value="" name="id_recaudar_hidden" id="id_recaudar_hidden" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_recaudar">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade resultado_poliza" tabindex="-1" role="dialog" aria-labelledby="resultado_poliza" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-cart-plus ic-modal"></i> Resultado Póliza</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="message-text" class="control-label"><p id="mensaje_resultado_poliza"></p></label>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-green" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade resultado_recaudado" tabindex="-1" role="dialog" aria-labelledby="resultado_recaudado" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-cart-plus ic-modal"></i> Resultado Recaudado</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="message-text" class="control-label"><p id="mensaje_resultado_recaudado"></p></label>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-green" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal_poliza" tabindex="-1" role="dialog" aria-labelledby="modal_poliza" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-dot-circle-o ic-modal"></i> Póliza</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="message-text" class="control-label">Se generara Póliza de Diario correspondiente al Devengado No. <span id="numero_devengado_poliza"></span></label>
                        <input type="hidden" value="" name="numero_devengado_poliza_hidden" id="numero_devengado_poliza_hidden" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_generar_poliza">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="subirArchivo" tabindex="-1" role="dialog" aria-labelledby="subirArchivo" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><span class="glyphicon glyphicon-indent-left"></span> Subir Archivo(s) de Devengado</h4>
            </div>
            <div class="modal-body">
                <?php
                $attributes = array(
                    'role' => 'form',
                );

                echo(form_open_multipart('recaudacion/subir_devengado', $attributes));
                ?>
                <div class="form-group center">
                    <div class="fileUpload btn btn-default btn-size">
                        <span>Selecciona Archivo(s)</span>
                        <?php
                        $data = array(
                            'name'        => 'archivoSubir[]',
                            'id'          => 'archivoSubir',
                            'class'       => 'upload',
                            'accept' => 'application/vnd.ms-excel',
                            'required' => 'required',
                            'multiple' => 'multiple',
                        );

                        echo(form_upload($data));
                        ?>
                    </div>
                    <p class="help-block">El archivo(s) a subir debe(n) de tener la extensión .XLS</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <?php
                $atributos_submit = array(
                    'class' => 'btn btn-default',
                    'required' => 'required',
                );

                echo(form_submit($atributos_submit, 'Subir Archivo(s)'));
                ?>
                <?php echo(form_close()); ?>
            </div>
        </div>
    </div>
</div>