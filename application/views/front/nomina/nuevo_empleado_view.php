<h3 class="page-header center"><i class="fa fa-plus-circle circle ic-color"></i> Nuevo Registro</h3>
<div id="page-wrapper">
    <form class="forma_empleado" role="form">
        <div class="row add-pre error-gral">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-6"><label>No. Registro</label></div>
                                    <div class="col-lg-6"><p class="form-control-static input_ver">11</p></div>
                                </div>

                                <!-- Nombre -->
                                <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Nombre(s)">

                                <div class="row">
                                    <div class="col-lg-6">
                                        <!--Apellido Paterno-->
                                        <input type="text" class="form-control" name="apellido_paterno" id="apellido_paterno" placeholder="Apellido Paterno">
                                    </div>
                                    <div class="col-lg-6">
                                        <!--Apellido Materno-->
                                        <input type="text" class="form-control" name="apellido_materno" id="apellido_materno" placeholder="Apellido Materno">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <!--RFC-->
                                        <input type="text" class="form-control" name="rfc" id="rfc" placeholder="R.F.C.">
                                    </div>

                                    <div class="col-lg-6">
                                        <!--CURP-->
                                        <input type="text" class="form-control" name="curp" id="curp" placeholder="Curp">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <!--Teléfono Contacto-->
                                        <input type="text" class="form-control" name="telefono" id="telefono" placeholder="Tel&eacute;fono Contacto">
                                    </div>
                                    <div class="col-lg-6">
                                        <!--Estado del Emepleado-->
                                        <select class="form-control" id="estado_empleado" name="estado_empleado" required>
                                            <option value="">Estado Empleado</option>
                                            <option value="Activo">Activo</option>
                                            <option value="Inactivo">Inactivo</option>
                                            <option value="Pr&aacute;cticas">Pr&aacute;cticas</option>
                                            <option value="Maternidad">Maternidad</option>
                                            <option value="Permiso">Permiso</option>
                                            <option value="Baja">Baja</option>
                                            <option value="Otro">Otro</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <!--Sexo-->
                                        <select class="form-control" id="estado_empleado" name="estado_empleado" required>
                                            <option value="">Sexo</option>
                                            <option value="Femenino">F</option>
                                            <option value="Masculino">M</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-6">
                                        <!--Edad-->
                                        <input type="text" class="form-control" name="edad" id="edad" placeholder="Edad">
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6 center">
                                <!-- Cargar Fotografía Emepleado -->
                                <script>
                                    $(function() {
                                        // Botón para subir la imagen
                                        var btn_firma = $('#addImage'), interval;
                                        new AjaxUpload('#addImage', {
                                            action: 'includes/uploadFile.php',
                                            onSubmit : function(file , ext){
                                                if (! (ext && /^(jpg|png)$/.test(ext))){
                                                    // extensiones permitidas
                                                    alert('Sólo se permiten Imagenes .jpg o .png');
                                                    // cancela upload
                                                    return false;
                                                } else {
                                                    $('#loaderAjax').show();
                                                    btn_firma.text('Espere por favor');
                                                    this.disable();
                                                }
                                            },
                                        : function(file, response){
                                            // alert(response);
                                            btn_firma.text('Cambiar Imagen');

                                            respuesta = $.parseJSON(response);

                                            if(respuesta.respuesta == 'done'){
                                                $('#fotografia').removeAttr('scr');
                                                $('#fotografia').attr('src','images/' + respuesta.fileName);
                                                $('#loaderAjax').show();
                                                // alert(respuesta.mensaje);
                                            }
                                            else{
                                                alert(respuesta.mensaje);
                                            }

                                            $('#loaderAjax').hide();
                                            this.enable();
                                        }
                                    });
                                    });
                                </script>

                                <header class="headerLayout">
                                    <h4>Fotografía</h4>
                                </header>

                                <section class="contentLayout" id="contentLayout">
                                    <div id="contenedorImagen">
                                        <img id="fotografia" class="fotografia" src="<?= base_url("assets/templates/front/nomina/images/nofoto.jpg") ?>">
                                    </div>
                                    <div style="margin: 5%;">
                                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#subirArchivo" data-whatever="Subir"><i class="fa fa-refresh ic-color"></i> Actualizar Imagen</button>
                                        <div class="loaderAjax" id="loaderAjax" style="margin: 3%;">
                                            <img src="<?= base_url("assets/templates/front/nomina/images/default-loader.gif") ?>">
                                            <span>Subiendo Imagen...</span>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <h4 class="center"><b>Dirección</b></h4>
                            <div class="col-lg-6">
                                <!-- Calle -->
                                <input type="text" class="form-control" name="calle" id="calle" placeholder="Calle">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <!-- No. Ext. -->
                                        <input type="text" class="form-control" name="no_exterior" id="no_exterior" placeholder="No. Interior">
                                    </div>
                                    <div class="col-lg-6">
                                        <!-- No. Int. -->
                                        <input type="text" class="form-control" name="no_interior" id="no_interior" placeholder="No. Exterior">
                                    </div>
                                </div>

                                <!-- Colonia -->
                                <input type="text" class="form-control" name="colonia" id="colonia" placeholder="Colonia">

                            </div>
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <!-- País -->
                                        <input type="text" class="form-control" name="pais" id="pais" placeholder="País">
                                    </div>
                                    <div class="col-lg-6">
                                        <!-- Ciudad/Estado -->
                                        <input type="text" class="form-control" name="ciudad" id="ciudad" placeholder="Ciudad/Estado">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <!-- Código Postal -->
                                        <input type="text" class="form-control" name="cp" id="cp" placeholder="Código Postal">
                                    </div>
                                    <div class="col-lg-6">
                                        <!-- Delegación/Municipio -->
                                        <input type="text" class="form-control" name="delegacion" id="delegacion" placeholder="Delegación/Municipio">
                                    </div>
                                </div>

                            </div>
                        </div>

                        <hr>

                        <div class="row">
                            <div class="col-lg-6">

                                <!-- Lugar de Nacimiento -->
                                <input type="text" class="form-control" name="lugar_nacimiento" id="lugar_nacimiento" placeholder="Lugar de Nacimiento">
                                <!-- Fecha de Nacimiento -->
                                <input type="text" class="form-control" name="fechadenacimiento" id="fechadenacimiento" placeholder="Fecha de Nacimiento">
                                <!-- Email -->
                                <input type="text" class="form-control" name="email" id="email" placeholder="Email">
                                <!-- Estado Civil -->
                                <select class="form-control" id="edo_civil" name="edo_civil" required>
                                    <option value="">Estado Civil</option>
                                    <option value="Soltero/a">Soltero/a</option>
                                    <option value="Casado/a">Casado/a</option>
                                    <option value="Viudo/a">Viudo/a</option>
                                </select>
                                <!-- Puesto -->
                                <select class="form-control" id="edo_civil" name="edo_civil" required>
                                    <option value="">Puesto</option>
                                    <option value="ADMON. PATRONATO DE LA BENEFICIENCIA PUBLICA">ADMON. PATRONATO DE LA BENEFICIENCIA PUBLICA</option>
                                    <option value="C.S. LA JOYA (PSC), ZITACUARO">C.S. LA JOYA (PSC), ZITACUARO</option>
                                    <option value="CENTRO ESTATAL DE ATENCION ONCOLOGICA">CENTRO ESTATAL DE ATENCION ONCOLOGICA</option>
                                    <option value="CENTRO ESTATAL DE LA TRANSFUSION SANGUINEA">CENTRO ESTATAL DE LA TRANSFUSION SANGUINEA</option>
                                    <option value="CENTRO MICHOACANO DE SALUD MENTAL">CENTRO MICHOACANO DE SALUD MENTAL</option>
                                    <option value="CENTRO REGIONAL DE DRLLO INFANTIL Y ESTIMULACION TEMPRANA">CENTRO REGIONAL DE DRLLO INFANTIL Y ESTIMULACION TEMPRANA</option>
                                    <option value="COORDINACION DE CALIDAD DE LA ATENCION MEDICA">COORDINACION DE CALIDAD DE LA ATENCION MEDICA</option>
                                    <option value="COORDINACION DE INFRAESTRUCTURA HOSPITALARIA">COORDINACION DE INFRAESTRUCTURA HOSPITALARIA</option>
                                    <option value="COORDINACION DE PROGRAMAS DE GRATUIDAD">COORDINACION DE PROGRAMAS DE GRATUIDAD</option>
                                    <option value="CS/H-07 EN COAHUAYANA DE HIDALGO, COAHUAYANA">CS/H-07 EN COAHUAYANA DE HIDALGO, COAHUAYANA</option>
                                    <option value="CS/H-08 EN ARTEAGA, ARTEAGA">CS/H-08 EN ARTEAGA, ARTEAGA</option>
                                </select>
                            </div>

                            <div class="col-lg-6">
                                <!-- N&uacute;mero Seguridad Social -->
                                <input type="text" class="form-control" name="num_seguro_social" id="num_seguro_social" placeholder="N&uacute;mero Seguridad Social">
                                <!-- Descripción -->
                                <input type="text" class="form-control" name="descripcion" id="descripcion" placeholder="Descripción">

                                <!-- CRESPO -->
                                <input type="text" class="form-control" name="crespo" id="crespo" placeholder="CRESPO">
                                <!-- FEcha Alta -->
                                <input type="text" class="form-control" name="fecha_alta" id="fecha_alta" placeholder="Fecha de Alta" step="1" min="2000-01-01" max="2099-12-31" value="2015-01-01">
                                <!-- Fecha Baja -->
                                <input type="text" class="form-control" name="fecha_baja" id="fecha_baja" placeholder="Fecha Baja" step="1" min="2000-01-01" max="2099-12-31" value="2015-01-01">
                                <!-- Lugar -->
                                <input type="text" class="form-control" name="lugar_nacimiento" id="lugar_nacimiento" placeholder="Lugar">
                                <!-- Unidad -->
                                <input type="text" class="form-control" name="unidad" id="unidad" placeholder="Unidad">
                                <!-- Percepcion -->
                                <input type="text" class="form-control" name="percepcion" id="percepcion" placeholder="Percepciones">
                                <!-- Deducciones -->
                                <input type="text" class="form-control" name="deducciones" id="deducciones" placeholder="Deducciones">
                                <!-- Total Neto -->
                                <input type="text" class="form-control" name="total_neto" id="total_neto" placeholder="Total Neto">
                                <!-- jurisdicción -->
                                <input type="text" class="form-control" name="deducciones" id="jurisdiccion" placeholder="Jurisdicción">
                                <!-- Total Neto -->
                                <input type="text" class="form-control" name="municipio" id="total_neto" placeholder="Municipio">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <div class="btns-finales text-center">
        <div class="text-center" id="resultado_insertar_caratula"></div>
        <a class="btn btn-default" href="<?= base_url("nomina/empleados") ?>"><i class="fa fa-reply ic-color"></i> Regresar</a>
        <a id="guardar_nota" class="btn btn-green">Guardar Registro</a>
    </div>

</div>

<!---
    <a href="#modal_estructura" class="btn btn-default" data-toggle="modal" data-target="#modal_estructura">Datos Extras</a>
    <a href="#modal_estructura1" class="btn btn-default" data-toggle="modal" data-target="#modal_estructura1">Salario de<br/>N&oacute;mina</a>
    <a href="#modal_estructura2" class="btn btn-default" data-toggle="modal" data-target="#modal_estructura2">Bonos y<br/>Prestaciones</a>
    <a href="#modal_estructura3" class="btn btn-default" data-toggle="modal" data-target="#modal_estructura3">Datos <br/>Extras</a>
     <a href="#modal_estructura4" class="btn btn-default" data-toggle="modal" data-target="#modal_estructura4">Extras de la<br/>N&oacute;mina</a>

-->
                                        <div class="modal fade" id="modal_estructura" tabindex="-1" role="dialog" aria-labelledby="modal_estructura" aria-hidden="true">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                        <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-indent-left"></span>Datos Extras</h4>
                                                    </div>
                                                    <div class="modal-body text-center">

                                                        <label for='person3'>Num. Plaza: </label>  <td>  <input name='hfest' value='0'/> </td>
                                                        <label for='person3'>Descripción del Puesto: </label>  <td>  <input name='hfest' value='0'/> </td><br>
                                                        <label for='person3'>Rama: </label>  <td>  <input name='sfest' value='7.87775'/></td>
                                                        <label for='person3'>Fuente de Financiamiento:</label> <td>  <input name='hfest' value='0'/> </td><br>
                                                        <label for='person3'>Tipo de Contrato:</label>  <td>  <input name='hfest' value='0'/> </td>
                                                        <label for='person3'>Clave Clues:</label>  <td>  <input name='sfest' value='7.87775'/><br>
                                                        <label for='person3'>No. de Tarjeta: </label>  <td>  <input name='hfest' value='0'/> </td>
                                                        <label for='person3'>Prog_Subp:</label>  <td>  <input name='hfest' value='0'/> </td><br>
                                                        <label for='person3'>Proy:</label>  <td>  <input name='sfest' value='7.87775'/></td>
                                                        <label for='person3'>PTDA:</label>  <td>  <input name='hfest' value='0'/> </td><br>
                                                        <tr><td colspan="4"> <HR align="center"> </td></tr>


                                                       <center><h3><p>Extras Salario</p></h3> </center>


                                                        <label for='person3'>Sueldo Integrado:</label>  <td>  <input name='hfest' value='0'/> </td>
                                                        <label for='person3'>Factor de Integraci&oacute;n:</label> <td>  <input name='hfest' value='0'/> </td><br>
                                                        <label for='person3'>Sueldo Retroactivo:</label>  <td>  <input name='hfest' value='0'/> </td>
                                                        <label for='person3'>Diferencia de Sueldo:</label>  <td>  <input name='sfest' value='7.87775'/><br>
                                                        <label for='person3'>Horas Extras Dobles:</label>  <td>  <input name='hfest' value='0'/> </td>
                                                        <label for='person3'>Horas extras Triples:</label>  <td>  <input name='sfest' value='7.87775'/><br>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                                        <button type="button" class="btn btn-green" data-dismiss="modal" disabled id="elegir_ultimoNivel">Aceptar</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="modal fade" id="modal_estructura1" tabindex="-1" role="dialog" aria-labelledby="modal_estructura" aria-hidden="true">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                        <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-indent-left"></span>Salario</h4>
                                                    </div>
                                                    <div class="modal-body text-center">

                                                        <label for='person3'>Complemento de Beca:</label>  <td>  <input name='hfest' value='0'/> </td>
                                                        <label for='person3'>Compensación a Médicos Residentes:</label>  <td>  <input name='hfest' value='0'/> </td><br>
                                                        <label for='person3'>Compensación Adicional Por Servicios Especiales:</label>  <td>  <input name='sfest' value='7.87775'/></td>
                                                        <label for='person3'>Sueldos Compactados:</label> <td>  <input name='hfest' value='0'/> </td><br>
                                                        <label for='person3'>Ayuda de Despensa:</label>  <td>  <input name='hfest' value='0'/> </td>
                                                        <label for='person3'>Asignación Neta al Personal de Rama Médica:</label>  <td>  <input name='sfest' value='7.87775'/><br>
                                                        <label for='person3'>Prevensión Social Multiple:</label>  <td>  <input name='hfest' value='0'/> </td>
                                                        <label for='person3'>Ayuda de Servicios:</label>  <td>  <input name='sfest' value='7.87775'/><br>

                                                            <tr><td colspan="4"> <HR align="center"> </td></tr>
                                                        <label for='person3'>Ayuda para Gastos de Actualización:</label>  <td>  <input name='hfest' value='0'/> </td>
                                                        <label for='person3'>Quienquenio por A-OS Servs Efectivo Prestado:</label>  <td>  <input name='sfest' value='7.87775'/><br>
                                                        <label for='person3'>Compensación a Pasantes en Servicio Social:</label>  <td>  <input name='hfest' value='0'/> </td>
                                                        <label for='person3'>Seguros de Vida para Pasantes Internos:</label>  <td>  <input name='sfest' value='7.87775'/><br>
                                                        <label for='person3'>Material Didactico:</label>  <td>  <input name='sfest' value='7.87775'/></td>
                                                        <label for='person3'>Gratificación Fin A-O para Internos y Pasantes:</label> <td>  <input name='hfest' value='0'/> </td><br>
                                                        <label for='person3'>Comp. por Lab. en Comunicaciones de difícil acceso:</label>  <td>  <input name='hfest' value='0'/> </td>
                                                        <label for='person3'>Comp. Garantizada Personal de Enlace y M.M. y Sup.:</label>  <td>  <input name='sfest' value='7.87775'/><br>
<!--                                                        <label for='person3'>668-Aportaci&oacute;n Caja de Ahorro:</label>  <td>  <input name='hfest' value='0'/> </td>-->
<!--                                                        <label for='person3'>669-Prestamo Caja de Ahorro:</label>  <td>  <input name='sfest' value='7.87775'/><br>-->
<!--                                                            <tr><td colspan="4"> <HR align="center"> </td></tr>-->
<!---->
<!--                                                        <label for='person3'>670-Cruz Roja</label>  <td>  <input name='hfest' value='0'/> </td>-->
<!--                                                        <label for='person3'>671-Compra de Libros:</label>  <td>  <input name='sfest' value='7.87775'/><br>-->
<!--                                                        <label for='person3'>672-Alimentaci&oacute;n:</label>  <td>  <input name='hfest' value='0'/> </td>-->
<!--                                                        <label for='person3'>673-Habitaci&oacute;n:</label>  <td>  <input name='sfest' value='7.87775'/><br>-->
<!--                                                        <label for='person3'>674-Gastos por Comprobar:</label>  <td>  <input name='hfest' value='0'/> </td>-->
<!--                                                        <label for='person3'>675-Anticipo de N&oacute;mina:</label>  <td>  <input name='sfest' value='7.87775'/><br>-->
<!--                                                        <label for='person3'>676-Aseguradora Metlife:</label>  <td>  <input name='hfest' value='0'/> </td>-->
<!--                                                        <label for='person3'>677-Funcionarios (Tarj. en Ventas):</label>  <td>  <input name='sfest' value='7.87775'/><br>-->
<!--                                                        <label for='person3'>678-Dif. en Ventas:</label>  <td>  <input name='hfest' value='0'/> </td>-->
<!--                                                        <label for='person3'>679-Diferencia en Fondo:</label>  <td>  <input name='sfest' value='7.87775'/><br>-->
<!--                                                        <label for='person3'>680-Vi&aacute;tico no comprobados:</label>  <td>  <input name='sfest' value='7.87775'/></td>-->
<!--                                                        <label for='person3'>681-Seguro de Separaci&oacute;n Individual:</label> <td>  <input name='hfest' value='0'/> </td><br>-->
<!--                                                        <label for='person3'>682-Seg. SEp. Ind. por Cita del IMSS:</label>  <td>  <input name='hfest' value='0'/> </td>-->
<!--                                                        <label for='person3'>683-Potenciaci&oacute;n GMM:</label>  <td>  <input name='sfest' value='7.87775'/><br>-->
<!--                                                        <label for='person3'>684-Soluciones Life:</label>  <td>  <input name='hfest' value='0'/> </td>-->

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                                        <button type="button" class="btn btn-green" data-dismiss="modal" disabled id="elegir_ultimoNivel">Aceptar</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="modal fade" id="modal_estructura2" tabindex="-1" role="dialog" aria-labelledby="modal_estructur2a" aria-hidden="true">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                        <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-indent-left"></span>Prestaciones</h4>
                                                    </div>
                                                    <div class="modal-body text-center">

                                                        <label for='person3'>Vacaciones:</label>  <td>  <input name='hfest' value='0'/> </td>
                                                        <label for='person3'>D&iacute;a de Descanso Obligatorio:</label>  <td>  <input name='hfest' value='0'/> </td><br>

                                                        <label for='person3'>Vales de despensa:</label>  <td>  <input name='sfest' value='7.87775'/></td>
                                                        <label for='person3'>Vales de Despensa Finiquito:</label> <td>  <input name='hfest' value='0'/> </td><br>
                                                        <label for='person3'>Vales de Fiestas:</label>  <td>  <input name='hfest' value='0'/> </td>
                                                        <label for='person3'>Diferencia de Sueldo:</label>  <td>  <input name='sfest' value='7.87775'/><br>
                                                        <label for='person3'>Vales de 10 de Mayo:</label>  <td>  <input name='hfest' value='0'/> </td>
                                                        <label for='person3'>Vales de Pavo:</label>  <td>  <input name='sfest' value='7.87775'/><br>

                                                            <tr><td colspan="4"> <HR align="center"> </td></tr>

                                                        <label for=''>Prima Dominical:</label>  <td>  <input name='sfest' value='0'/></td>
                                                        <label for=''>Comisiones Extras:</label> <td>  <input name='sfest' value='0'/> </td><br>
                                                        <label for=''>Comisiones:</label>  <td>  <input name='sfest' value='0'/> </td>
                                                        <label for='person3'>Compensaci&oacute;n de Vida Cara:</label>  <td>  <input name='sfest' value='0'/><br>
                                                        <label for='person3'>Est&iacute;mulo de Productividad:</label>  <td>  <input name='sfest' value='0'/> </td>
                                                        <label for='person3'>Despensa Navide&ntilde;a:</label>  <td>  <input name='sfest' value=''/><br>
                                                        <label for='person3'>Premio de puntualidad:</label>  <td>  <input name='sfest' value=''/></td>
                                                        <label for='person3'>Ayuda de Despensa:</label> <td>  <input name='hfest' value='0'/> </td><br>
                                                        <label for='person3'>Gratificaci&oacute;n de Fin de A&ntilde;o:</label>  <td>  <input name='hfest' value='0'/> </td>
                                                        <label for='person3'>Pago Extraordinario:</label>  <td>  <input name='sfest' value=''/></td><br>
                                                        <label for='person3'>Compensaci&oacute;n:</label>  <td>  <input name='sfest' value=''/></td>

                                                        <tr><td colspan="4"> <HR align="center"> </td></tr>

                                                        <label for=''>472-Reembolso justif. Extemp.:</label>  <td>  <input name='sfest' value='0'/></td>
                                                        <label for=''>473-Compensaci&oacute;n Garantizada</label> <td>  <input name='sfest' value='0'/> </td><br>
                                                        <label for=''>474-Dif.de Comp.Garantiza (Falta):</label>  <td>  <input name='sfest' value='0'/> </td>
                                                        <label for='person3'>475-Dif. Compensaci&oacute;n Garantizada:</label>  <td>  <input name='sfest' value='0'/><br>
                                                        <label for='person3'>476-Seguro Separación Individual:</label>  <td>  <input name='sfest' value='0'/> </td>
                                                        <label for='person3'>477-Vales de Despensa:</label>  <td>  <input name='sfest' value=''/><br>
                                                        <label for='person3'>478-Dif. Ayuda para Despensa:</label>  <td>  <input name='sfest' value=''/></td>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                                        <button type="button" class="btn btn-green" data-dismiss="modal" disabled id="elegir_ultimoNivel">Aceptar</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="modal fade" id="modal_estructura3" tabindex="-1" role="dialog" aria-labelledby="modal_estructura3" aria-hidden="true">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                        <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-indent-left"></span>Salario</h4>
                                                    </div>
                                                    <div class="modal-body text-center">

                                                        <label for='person3'>501-Prima Vacacional:</label>  <td>  <input name='hfest' value='0'/> </td>
                                                        <label for='person3'>502-Aguinaldo Sueldo:</label>  <td>  <input name='hfest' value='0'/> </td><br>
                                                        <label for='person3'>503-P.T.U:</label>  <td>  <input name='sfest' value='7.87775'/></td>
                                                        <label for='person3'>504-Ajuste aguinaldo:</label> <td>  <input name='hfest' value='0'/> </td><br>
                                                        <label for='person3'>507-Vacaciones Pendientes:</label>  <td>  <input name='hfest' value='0'/> </td>
                                                        <label for='person3'>510-Vacaciones Finiquito:</label>  <td>  <input name='sfest' value='7.87775'/><br>
                                                        <label for='person3'>513-Prima de Antiguedad (12 d&iacute;as):</label>  <td>  <input name='hfest' value='0'/> </td>
                                                        <label for='person3'>514-Indemnizaci&oacute;n (Art. 50 Fracc.II):</label>  <td>  <input name='sfest' value='7.87775'/><br>
                                                        <label for='person3'>517-Pagos por Separaci&oacute;n:</label>  <td>  <input name='hfest' value='0'/> </td>
                                                        <label for='person3'>518-compensaci&oacute;n por Retiro:</label>  <td>  <input name='sfest' value='7.87775'/><br>
                                                        <label for='person3'>520-Prestamo Fondo de Ahorro:</label>  <td>  <input name='hfest' value='0'/> </td>
                                                        <label for='person3'>522-F.A. Aportaci&oacute;n Empleado:</label>  <td>  <input name='sfest' value='7.87775'/><br>
                                                        <label for='person3'>523-F.A. Aportaci&oacute;n Empresa:</label>  <td>  <input name='hfest' value='0'/> </td>
                                                        <label for='person3'>599-Subsidio para el Empleo:</label>  <td>  <input name='sfest' value='7.87775'/><br>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                                        <button type="button" class="btn btn-green" data-dismiss="modal" disabled id="elegir_ultimoNivel">Aceptar</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="modal fade" id="modal_estructura4" tabindex="-1" role="dialog" aria-labelledby="modal_estructura4" aria-hidden="true">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                        <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-indent-left"></span>Salario</h4>
                                                    </div>
                                                    <div class="modal-body text-center">

                                                        <label for='person3'>600-Total de Percepciones:</label>  <td>  <input name='hfest' value='0'/> </td>
                                                        <label for='person3'>601-Incapacidad Eg:</label>  <td>  <input name='hfest' value='0'/> </td><br>
                                                        <label for='person3'>602-Incapacidad AT:</label>  <td>  <input name='sfest' value='7.87775'/></td>
                                                        <label for='person3'>603-Incapacidad RT:</label> <td>  <input name='hfest' value='0'/> </td><br>
                                                        <label for='person3'>604-Incapacidad MT:</label>  <td>  <input name='hfest' value='0'/> </td>
                                                        <label for='person3'>605-Falta Injustificada:</label>  <td>  <input name='sfest' value='7.87775'/><br>
                                                        <label for='person3'>606-Faltas Infonavit:</label>  <td>  <input name='hfest' value='0'/> </td>
                                                        <label for='person3'>607-Falta Justificada:</label>  <td>  <input name='sfest' value='7.87775'/><br>
                                                        <label for='person3'>608-Castigo/Sanci&oacute;n</label>  <td>  <input name='hfest' value='0'/> </td>
                                                        <label for='person3'>609-Falta por Retardo:</label>  <td>  <input name='sfest' value='7.87775'/><br>
                                                            <tr><td colspan="4"> <HR align="center"> </td></tr>
                                                        <label for='person3'>621-Impuesto sobre la Renta:</label>  <td>  <input name='hfest' value='0'/> </td>
                                                        <label for='person3'>622-Suicidio al Empleo:</label>  <td>  <input name='sfest' value='7.87775'/><br>
                                                        <label for='person3'>623Impuesto Indemnizaci&oacute;n:</label>  <td>  <input name='hfest' value='0'/> </td>
                                                        <label for='person3'>624-Impuesto Art.142:</label>  <td>  <input name='sfest' value='7.87775'/><br>
                                                        <label for='person3'>625-Impuesto Ajuste Anual:</label>  <td>  <input name='hfest' value='0'/> </td>
                                                        <label for='person3'>626-Dif. Subsidio del a&ntilde;o:</label>  <td>  <input name='hfest' value='0'/> </td><br>
                                                        <label for='person3'>627-Dif. ISR del a&ntilde;o:</label>  <td>  <input name='sfest' value='7.87775'/></td>
                                                        <label for='person3'>628-Dev. ISR Aguinaldo:</label> <td>  <input name='hfest' value='0'/> </td><br>
                                                        <tr><td colspan="4"> <HR align="center"> </td></tr>
                                                        <label for='person3'>630-IMSS:</label>  <td>  <input name='hfest' value='0'/> </td>
                                                        <label for='person3'>632-Abono Infonavit:</label>  <td>  <input name='sfest' value='7.87775'/><br>
                                                        <label for='person3'>633-Devoluci&oacute;n Infonavit:</label>  <td>  <input name='hfest' value='0'/> </td>
                                                        <label for='person3'>637-Pensi&oacute;n Alimenticia:</label>  <td>  <input name='sfest' value='7.87775'/><br>
                                                        <label for='person3'>638-Sueldo no Devengado</label>  <td>  <input name='hfest' value='0'/> </td>
                                                        <label for='person3'>639-Vacaciones no Devengadas:</label>  <td>  <input name='sfest' value='7.87775'/><br>
                                                            <tr><td colspan="4"> <HR align="center"> </td></tr>
                                                        <label for='person3'>640-Comisiones no Devengadas:</label>  <td>  <input name='hfest' value='0'/> </td>
                                                        <label for='person3'>641-Faltante Activo Fijo:</label>  <td>  <input name='sfest' value='7.87775'/><br>
                                                        <label for='person3'>650-Abono Fonacot:</label>  <td>  <input name='hfest' value='0'/> </td>
                                                        <label for='person3'>651-Devoloci&oacute;n Fonacot:</label>  <td>  <input name='sfest' value='7.87775'/><br>
                                                        <label for='person3'>652-Ajuste Fonacot:</label>  <td>  <input name='sfest' value='7.87775'/></td>
                                                        <label for='person3'>660-F.A. Empleado:</label> <td>  <input name='hfest' value='0'/> </td><br>
                                                        <label for='person3'>661-F.A. Empresa:</label>  <td>  <input name='hfest' value='0'/> </td>
                                                        <label for='person3'>667-Intereses Fondo de Ahorro :</label>  <td>  <input name='sfest' value='7.87775'/><br>
                                                        <label for='person3'>668-Aportaci&oacute;n Caja de Ahorro:</label>  <td>  <input name='hfest' value='0'/> </td>
                                                        <label for='person3'>669-Prestamo Caja de Ahorro:</label>  <td>  <input name='sfest' value='7.87775'/><br>
                                                            <tr><td colspan="4"> <HR align="center"> </td></tr>

                                                        <label for='person3'>670-Cruz Roja</label>  <td>  <input name='hfest' value='0'/> </td>
                                                        <label for='person3'>671-Compra de Libros:</label>  <td>  <input name='sfest' value='7.87775'/><br>
                                                        <label for='person3'>672-Alimentaci&oacute;n:</label>  <td>  <input name='hfest' value='0'/> </td>
                                                        <label for='person3'>673-Habitaci&oacute;n:</label>  <td>  <input name='sfest' value='7.87775'/><br>
                                                        <label for='person3'>674-Gastos por Comprobar:</label>  <td>  <input name='hfest' value='0'/> </td>
                                                        <label for='person3'>675-Anticipo de N&oacute;mina:</label>  <td>  <input name='sfest' value='7.87775'/><br>
                                                        <label for='person3'>676-Aseguradora Metlife:</label>  <td>  <input name='hfest' value='0'/> </td>
                                                        <label for='person3'>677-Funcionarios (Tarj. en Ventas):</label>  <td>  <input name='sfest' value='7.87775'/><br>
                                                        <label for='person3'>678-Dif. en Ventas:</label>  <td>  <input name='hfest' value='0'/> </td>
                                                        <label for='person3'>679-Diferencia en Fondo:</label>  <td>  <input name='sfest' value='7.87775'/><br>
                                                        <label for='person3'>680-Vi&aacute;tico no comprobados:</label>  <td>  <input name='sfest' value='7.87775'/></td>
                                                        <label for='person3'>681-Seguro de Separaci&oacute;n Individual:</label> <td>  <input name='hfest' value='0'/> </td><br>
                                                        <label for='person3'>682-Seg. SEp. Ind. por Cita del IMSS:</label>  <td>  <input name='hfest' value='0'/> </td>
                                                        <label for='person3'>683-Potenciaci&oacute;n GMM:</label>  <td>  <input name='sfest' value='7.87775'/><br>
                                                        <label for='person3'>684-Soluciones Life:</label>  <td>  <input name='hfest' value='0'/> </td>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                                        <button type="button" class="btn btn-green" data-dismiss="modal" disabled id="elegir_ultimoNivel">Aceptar</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                </center>






