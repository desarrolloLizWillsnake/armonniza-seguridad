<h3 class="page-header center"><i class="fa fa-users ic-color"></i> Consulta N&oacute;minas</h3>
<div id="page-wrapper">

    <form method="POST" id="daros_persona" role="form">
        <div class="row add-pre error-gral">
            <div class="col-lg-12">
                <div class="panel panel-default" style="margin: 0 auto; margin-top: 2%; width: 50%;">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <select class="form-control" name="mes">
                                    <option>Mes</option>
                                    <option value="Enero">Enero</option>
                                    <option value="Febrero">Febrero</option>
                                    <option value="Marzo">Marzo</option>
                                    <option value="Abril">Abril</option>
                                    <option value="Mayo">Mayo</option>
                                    <option value="Junio">Junio</option>
                                    <option value="Julio">Julio</option>
                                    <option value="Agosto">Agosto</option>
                                    <option value="Septiembre">Septiembre</option>
                                    <option value="Octubre">Octubre</option>
                                    <option value="Noviembre">Noviembre</option>
                                    <option value="Diciembre">Diciembre</option>
                                </select>
                            </div>
                            <div class="col-lg-6">
                                <!--A�o-->
                                <input type="text" class="form-control" name="a�o" id="a�o" placeholder="A&ntilde;o" >
                            </div>
                        </div>
                        <div class="btns-finales text-center">
                            <input class="btn btn-green" type="submit" id="consultar_reporte" value="Continuar"/>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
</div>