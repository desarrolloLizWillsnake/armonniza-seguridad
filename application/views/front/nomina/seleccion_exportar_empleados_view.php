<h3 class="page-header title center"><i class="fa fa-download"></i> Exportar Personal</h3>
<div id="page-wrapper">
    <div class="row center">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <div class="list-group error-completar">
                <?php if(isset($mensaje)) { ?>
                    <div class="alert alert-danger">
                        <?= $mensaje ?>
                    </div>
                    <div class="text-center">
                        <div class="btns-finales">
                            <a class="btn btn-default" href="<?= base_url("nomina/empleados") ?>"><i class="fa fa-reply ic-color"></i> Regresar</a>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-8">
                            <div class="panel panel-default datos-requeridos">
                                <div class="panel-body">
                                    <form class="" action="<?= base_url("nomina/exportar_empleados_formato") ?>" method="POST" role="form">
                                        <!--Rango Fechas-->
                                        <h5><b>Fecha Ingreso</b></h5>
                                        <div class="row marg_inf">
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control ic-calendar" name="fecha_inicial" id="fecha_inicial" placeholder="Fecha Inicial" value="1961-01-01">
                                            </div>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control ic-calendar" name="fecha_final" id="fecha_final" placeholder="Fecha Final" value="2015-12-31">
                                            </div>
                                        </div>

                                        <!--Apellido Paterno-->
                                        <input type="text" class="form-control marg_inf" name="a_paterno" id="a_paterno" placeholder="Apellido Paterno"/>
                                        <!--Apellido Materno-->
                                        <input type="text" class="form-control marg_inf" name="a_materno" id="a_materno" placeholder="Apellido Materno"/>
                                        <!--Nombre(s)-->
                                        <input type="text" class="form-control marg_inf" name="nombre" id="nombre" placeholder="Nombre(s)"/>
                                        <!--RFC-->
                                        <input type="text" class="form-control marg_inf" name="rfc" id="rfc" placeholder="RFC"/>
                                        <!--CURP-->
                                        <input type="text" class="form-control marg_inf" name="curp" id="curp" placeholder="Curp"/>
                                        <!--Puesto-->
                                        <input type="text" class="form-control marg_inf" name="puesto" id="puesto" placeholder="Puesto"/>
                                        <!--Fuente Financiamiento-->
                                        <input type="text" class="form-control marg_inf" name="fuente_financiamiento" id="fuente_financiamiento" placeholder="Fuente Financiamiento"/>
                                        <!--Tipo Contrato-->
                                        <input type="text" class="form-control marg_inf" name="tipo_contrato" id="tipo_contrato" placeholder="Tipo Contrato"/>

                                        <div class="text-center">
                                            <div class="btns-finales text-center">
                                                <a class="btn btn-default" href="<?= base_url("nomina/empleados") ?>"><i class="fa fa-reply ic-color"></i> Regresar</a>
                                                <button type="submit" class="btn btn-green">Continuar</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

</div>

<!-- Modal Centros de Costo -->
<div class="modal fade" id="modal_centro_recaudacion" tabindex="-1" role="dialog" aria-labelledby="modal_centro_recaudacion" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-users ic-modal"></i> Centro de Recaudación</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-3">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tabla_centro_recaudacion">
                        <thead>
                        <tr>
                            <th width="25%">Centro de Recaudación</th>
                            <th width="65%">Nombre</th>
                            <th width="10%">Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal Cliente-->
<div class="modal fade" id="modal_cliente" tabindex="-1" role="dialog" aria-labelledby="modal_cliente" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-users ic-modal"></i> Clientes</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-3">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tabla_cliente">
                        <thead>
                        <tr>
                            <th width="25%">Clave Cliente</th>
                            <th width="65%">Nombre</th>
                            <th width="10%">Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>