<h3 class="page-header center"><i class="fa fa-eye"></i>Ver Precompromiso</h3>
<div id="page-wrapper">
    <form class="forma_precompromiso" role="form">
        <div class="row add-pre">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <!--Apartado General-->
                    <div class="panel-heading">
                        General
                    </div>
                    <div class="panel-body">
                        <input type="hidden" name="ultimo_pre" id="ultimo_pre" value="<?= $ultimo ?>">
                        <!--Apartado General-->
                        <div class="row">
                            <!--Primer Columna-->
                            <div class="col-lg-3">

                                <!---No. Precompromiso-->
                                <div class="form-group">
                                    <label>No. Precompromiso</label>
                                    <?php if(isset($numero_pre)) { ?>
                                        <p class="form-control-static input_view"><?= $numero_pre ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>

                                <!---Tipo Precompromiso-->
                                <div class="form-group">
                                    <label>Tipo de Precompromiso</label>
                                    <?php if(isset($tipo_requisicion)) { ?>
                                        <p class="form-control-static input_view" id="tipo_precompromiso"><?= $tipo_requisicion ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>

                                <!--Tipo de garantía-->
                                <div class="form-group forma_precompromis_dato">
                                    <label>Tipo de Garantía</label>
                                    <?php if(isset($tipo_garantia)) { ?>
                                        <p class="form-control-static input_view"><?= $tipo_garantia ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>

                                <!--Porcentaje de Garantía-->
                                <div class="form-group forma_precompromis_dato">
                                    <label>Porcentaje de Garantía</label>
                                    <?php if(isset($porciento_garantia)) { ?>
                                        <p class="form-control-static input_view"><?= $porciento_garantia. '%'?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>

                                <!--Importe de Responsabilidad Civil-->
                                <div class="form-group forma_precompromis_dato">
                                    <label>Importe de Responsabilidad Civil</label>
                                    <?php if(isset($importe_responsabilidad_civil)) { ?>
                                        <p class="form-control-static input_view">$<?= number_format($importe_responsabilidad_civil, 2, ".", ",") ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>

                            </div>
                            <!--Fin Primer Columna-->

                            <!--Segunda Columna-->
                            <div class="col-lg-5">

                                <!-- Proveedor-->
                                <div class="form-group forma_precompromis_dato">
                                    <label>Proveedor</label>
                                    <?php if(isset($proveedor)) { ?>
                                        <p class="form-control-static input_view"><?= $proveedor ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>

                                <!--Tipo de Gasto-->
                                <div class="form-group forma_precompromis_dato">
                                    <label>Tipo de Gasto</label>
                                    <?php if(isset($tipo_gasto) && $tipo_gasto == 1) { ?>
                                        <p class="form-control-static input_view">Gasto Corriente</p>
                                    <?php } elseif(isset($tipo_gasto) && $tipo_gasto == 2) { ?>
                                        <p class="form-control-static input_view">Gasto de Capital</p>
                                    <?php } elseif(isset($tipo_gasto) && $tipo_gasto == 3) { ?>
                                        <p class="form-control-static input_view">Amortización de la deuda y disminución de pasivos</p>
                                    <?php } elseif(isset($tipo_gasto) && $tipo_gasto == 4) { ?>
                                        <p class="form-control-static input_view">Pensiones y Jubilaciones</p>
                                    <?php } elseif(isset($tipo_gasto) && $tipo_gasto == 5) { ?>
                                        <p class="form-control-static input_view">Participaciones</p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>

                                <!--Leyes-->
                                <div class="row forma_precompromis_dato">
                                    <div class="col-lg-5">
                                        <!--Ley aplicable-->
                                        <div class="form-group">
                                            <label>Ley aplicable</label>
                                            <?php if(isset($ley_aplicable)) { ?>
                                                <p class="form-control-static input_view"><?= $ley_aplicable ?></p>
                                            <?php } else { ?>
                                                <p class="form-control-static input_view_null"></p>
                                            <?php }  ?>
                                        </div>
                                    </div>
                                    <div class="col-lg-7 text-left forma_precompromis_dato">
                                        <!--Articulo-->
                                        <div class="form-group">
                                            <label>Art. de Procedimiento</label>
                                            <?php if(isset($articulo_procedencia)) { ?>
                                                <p class="form-control-static input_view"><?= $articulo_procedencia ?></p>
                                            <?php } else { ?>
                                                <p class="form-control-static input_view"></p>
                                            <?php }  ?>
                                        </div>
                                    </div>
                                </div>

                                <!-- Plurianualidad -->
                                <div class="form-group forma_precompromis_dato">
                                    <label>Plurianualidad</label>
                                    <?php if(isset($plurianualidad) && $plurianualidad == "Si") { ?>
                                        <p class="form-control-static">Si aplica</p>
                                    <?php } elseif(isset($plurianualidad) && $plurianualidad == "No") { ?>
                                        <p class="form-control-static input_view">No aplica</p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>

                                <!-- Normas / Niveles de Inspección -->
                                <div class="form-group forma_precompromis_dato">
                                    <label>Normas / Niveles de Inspección</label>
                                    <?php if(isset($norma_inspeccion)) { ?>
                                        <p class="form-control-static input_view"><?= $norma_inspeccion ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>

                                <!-- Registros Sanitarios -->
                                <div class="form-group forma_precompromis_dato">
                                    <label>Registros Sanitarios</label>
                                    <?php if(isset($registros_sanitarios)) { ?>
                                        <p class="form-control-static input_view"><?= $registros_sanitarios ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>

                                <!-- Capacitación -->
                                <div class="form-group forma_precompromis_dato">
                                    <label>Capacitación</label>
                                    <?php if(isset($capacitacion)) { ?>
                                        <p class="form-control-static input_view"><?= $capacitacion ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>

                            </div>
                            <!--Fin Segunda Columna-->

                            <!--Tercera Columna-->
                            <div class="col-lg-4">
                                <!---Fecha de Solicitud-->
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-6"><label class="label-f">Fecha Solicitud</label></div>
                                        <div class="col-lg-6">
                                            <?php if(isset($fecha_emision)) { ?>
                                                <p class="form-control-static input_view"><?= $fecha_emision ?></p>
                                            <?php } else { ?>
                                                <p class="form-control-static input_view"></p>
                                            <?php }  ?>
                                        </div>
                                    </div>
                                </div>

                                <!--Fecha Autotizada-->
                                <div class="form-group forma_precompromis_dato">
                                    <div class="row">
                                        <div class="col-lg-6"><label class="label-f">Fecha Autorizada</label></div>
                                        <div class="col-lg-6">
                                            <?php if(isset($fecha_autoriza)) { ?>
                                                <p class="form-control-static input_view"><?= $fecha_autoriza ?></p>
                                            <?php } else { ?>
                                                <p class="form-control-static input_view"></p>
                                            <?php }  ?>
                                        </div>
                                    </div>
                                </div>

                                <!--Fecha Entrega-->
                                <div class="form-group forma_precompromis_dato">
                                    <div class="row">
                                        <div class="col-lg-6"><label class="label-f">Fecha Entrega</label></div>
                                        <div class="col-lg-6">
                                            <?php if(isset($fecha_programada)) { ?>
                                                <p class="form-control-static input_view"><?= $fecha_programada ?></p>
                                            <?php } else { ?>
                                                <p class="form-control-static input_view"></p>
                                            <?php }  ?>
                                        </div>
                                    </div>
                                </div>

                                <!--Lugar de Entrega-->
                                <div class="form-group forma_precompromis_dato">
                                    <label>Lugar de Entrega</label>
                                    <?php if(isset($lugar_entrega)) { ?>
                                        <p class="form-control-static input_view"><?= $lugar_entrega ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>


                                <!--Plazo-->
                                <div class="form-group forma_precompromis_dato">
                                    <label >Plazo</label>
                                    <div class="row">
                                        <div class="col-lg-5">
                                            <?php if(isset($plazo_numero)) { ?>
                                                <p class="form-control-static input_view"><?= $plazo_numero ?></p>
                                            <?php } else { ?>
                                                <p class="form-control-static input_view"></p>
                                            <?php }  ?>
                                        </div>
                                        <div class="col-lg-7">
                                            <?php if(isset($plazo_tipo)) { ?>
                                                <p class="form-control-static input_view"><?= $plazo_tipo ?></p>
                                            <?php } else { ?>
                                                <p class="form-control-static input_view"></p>
                                            <?php }  ?>
                                        </div>
                                    </div>
                                </div>

                                <!--Anexos-->
                                <div class="form-group">
                                    <label>Anexos</label>
                                    <?php if(isset($anexos) && $anexos == 1) { ?>
                                        <p class="form-control-static input_view">Sí</p>
                                    <?php } elseif(isset($anexos) && $anexos == 0) { ?>
                                        <p class="form-control-static input_view">No</p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>

                                <!--Precompromiso en Firme-->
                                <div class="form-group c-firme" style="margin-top: 5%;">
                                    <div class="form-group">
                                        <label>¿Precompromiso en Firme?</label>
                                        <?php if(isset($enfirme) && $enfirme == 1) { ?>
                                            <p class="form-control-static"><i class="fa fa-check-circle i-firmesi"></i></p>
                                        <?php } else { ?>
                                            <p class="form-control-static"><i class="fa fa-times-circle i-firmeno"></i></p>
                                        <?php }  ?>
                                    </div>
                                </div>
                            </div>
                            <!--Fin Tecera Columna-->
                        </div>
                        <hr class="forma_fondo" style="display: none;"/>
                        <!--Apartado Datos del Comisionado-->
                        <div class="row forma_fondo" style="display: none;">
                            <div class="col-lg-12 c-firme center">
                                <label>Datos del Comisionado o Beneficiario</label>

                            </div>
                            <div class="col-lg-4">
                                <!---Nombre del Comisionado ó Beneficiario-->
                                <div class="form-group">
                                    <label class="label-f">Nombre Comisionado / Beneficiario</label>
                                    <?php if(isset($nombre_completo)) { ?>
                                        <p class="form-control-static input_view"><?= $nombre_completo ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>
                                <!--Número de Empleado-->
                                <div class="form-group">
                                    <label class="label-f">No. Empleado</label>
                                    <?php if(isset($no_empleado)) { ?>
                                        <p class="form-control-static input_view"><?= $no_empleado ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <!--Puesto o Categoría-->
                                <div class="form-group">
                                    <label class="label-f">Puesto o Categoría</label>
                                    <?php if(isset($puesto)) { ?>
                                        <p class="form-control-static input_view"><?= $puesto?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>
                                <!--Área de Adscripción-->
                                <div class="form-group">
                                    <label >Área de Adscripción</label>
                                    <?php if(isset($area)) { ?>
                                        <p class="form-control-static input_view"><?= $area ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <!--Clave-->
                                <div class="form-group">
                                    <label>Centro de Costos</label>
                                    <?php if(isset($clave)) { ?>
                                        <p class="form-control-static input_view"><?= $clave ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>
                            </div>
                        </div>
                        <hr class="forma_fondo" style="display: none;"/>
                        <!--Apartado Características de viáticos-->
                        <div class="row forma_viaticos" style="display: none;">
                            <div class="col-lg-4">
                                <!--Forma de pago-->
                                <div class="form-group">
                                    <label >Forma de pago</label>
                                    <?php if(isset($forma_pago)) { ?>
                                        <p class="form-control-static input_view"><?= $forma_pago ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>

                                <!--Medio de Transporte-->
                                <div class="form-group">
                                    <label >Medio de Transporte</label>
                                    <?php if(isset($transporte)) { ?>
                                        <p class="form-control-static input_view"><?= $transporte ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>

                                <!--Grupos Jerárquicos-->
                                <div class="form-group">
                                    <label >Grupos Jerárquicos</label>
                                    <?php if(isset($grupo_jerarquico)) { ?>
                                        <p class="form-control-static input_view"><?= $grupo_jerarquico ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>
                            </div>
                            <div class="col-lg-4"></div>
                            <div class="col-lg-4">
                                <!--Tipo de Zona-->
                                <div class="form-group">
                                    <div class="form-group">
                                        <label style="margin-top:6%;">Tipo de Zona</label>
                                        <?php if(isset($zona_marginada) && $zona_marginada == 1) { ?>
                                            <p class="form-control-static"><i class="fa fa-check-square-o"></i> Zona Marginada</p>
                                        <?php } else { ?>
                                            <p class="form-control-static"><i class="fa fa-square-o"></i> Zona Marginada</p>
                                        <?php }  ?>

                                        <?php if(isset($zona_mas_economica) && $zona_mas_economica == 1) { ?>
                                            <p class="form-control-static"><i class="fa fa-check-square-o"></i> Zona más Económica</p>
                                        <?php } else { ?>
                                            <p class="form-control-static"><i class="fa fa-square-o"></i> Zona más Económica</p>
                                        <?php }  ?>

                                        <?php if(isset($zona_menos_economica) && $zona_menos_economica == 1) { ?>
                                            <p class="form-control-static"><i class="fa fa-check-square-o"></i> Zona menos Económica</p>
                                        <?php } else { ?>
                                            <p class="form-control-static"><i class="fa fa-square-o"></i> Zona menos Económica</p>
                                        <?php }  ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr class="forma_viaticos" style="display: none;"/>

                        <!--Descripción y Firmas-->
                        <div class="row">
                            <div class="col-lg-8">

                                <!-- Lugar de Adquisición de los bienes y/o ejecución de (los) servicio(s) -->
                                <div class="form-group forma_precompromis_dato">
                                    <label>Lugar de Adquisición de bienes y/o ejecución de servicios</label>
                                    <?php if(isset($lugar_adquisiciones)) { ?>
                                        <p class="form-control-static input_view"><?= $lugar_adquisiciones ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>

                                <!-- Existencia en Almacen -->
                                <div class="form-group forma_precompromis_dato">
                                    <label>Existencia en Almacén</label>
                                    <?php if(isset($existencia_almacen)) { ?>
                                        <p class="form-control-static input_view"><?= $existencia_almacen ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>
                                <!---Concepto Específico-->
                                <div class="form-group">
                                    <label>Concepto Específico</label>
                                    <?php if(isset($concepto_especifico)) { ?>
                                        <p class="form-control-static input_view"><?= $concepto_especifico ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>
                                <!---Descripción-->
                                <div class="form-group">
                                    <label>Justificación</label>
                                    <?php if(isset($descripcion)) { ?>
                                        <p class="form-control-static input_view"><?= $descripcion ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <!---Firmas de Autorización-->
                                <div class="form-group">
                                    <label style="margin-top:6%;"> Firmas de Autorización</label>
                                    <?php if(isset($firma1) && $firma1 == 1) { ?>
                                        <p class="form-control-static"><i class="fa fa-check-square-o"></i> Autorizado por Programación y Evaluación</p>
                                    <?php } else { ?>
                                        <p class="form-control-static"><i class="fa fa-square-o"></i> Por autorizar de Programación y Evaluación</p>
                                    <?php }  ?>

                                    <?php if(isset($firma2) && $firma2 == 1) { ?>
                                        <p class="form-control-static"><i class="fa fa-check-square-o"></i> Autorizado por Recursos Materiales</p>
                                    <?php } else { ?>
                                        <p class="form-control-static"><i class="fa fa-square-o"></i> Por autorizar de Recursos Materiales </p>
                                    <?php }  ?>

                                    <?php if(isset($firma3) && $firma3 == 1) { ?>
                                        <p class="form-control-static"><i class="fa fa-check-square-o"></i> Autorizado por Gerencia Técnica Administrativa</p>
                                    <?php } else { ?>
                                        <p class="form-control-static"><i class="fa fa-square-o"></i> Por autorizar de Gerencia Técnica Administrativa</p>
                                    <?php }  ?>
                                </div>
                            </div>
                        </div>
                        <!--Fin Descripción y Firmas-->
                    </div>
                </div>
            </div>
        </div>

        <!--Tabla Detalle-->
        <div class="row add-pre error-detalle forma_viaticos">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Detalle Viáticos
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-body table-gral">
                                        <div class="table-responsive">
                                            <h4 id="suma_total_viaticos" class="text-center"></h4>
                                            <input type="hidden" value="" name="total_viaticos_hidden" id="total_viaticos_hidden" />

                                            <table class="table table-striped table-bordered table-hover" id="tabla_datos_viaticos_precompromiso">
                                                <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Lugar de Comisión</th>
                                                    <th width="12%">Fecha Inicio</th>
                                                    <th width="13%">Fecha Término</th>
                                                    <th>Días</th>
                                                    <th width="16%">Cuota Diaria</th>
                                                    <th width="16%">Importe Total</th>
                                                    <th>Acciones</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--Fin Apartado General-->
        <!--Apartado Detalle-->
        <div class="row add-pre">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Detalles
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-default">

                                    <div class="panel-body table-gral">
                                        <div class="table-responsive">
                                            <h4 id="suma_total" class="text-center"></h4>
                                            <input type="hidden" value="" name="subtotal_hidden" id="subtotal_hidden" />
                                            <input type="hidden" value="" name="total_hidden" id="total_hidden" />
                                            <table class="table table-striped table-bordered table-hover" id="tabla_datos_precompromiso">
                                                <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>F. F.</th>
                                                    <th>P. F.</th>
                                                    <th>C. C.</th>
                                                    <th>Capítulo</th>
                                                    <th>Concepto</th>
                                                    <th>Partida</th>
                                                    <th>Gasto</th>
                                                    <th>U/M</th>
                                                    <th>Cantidad</th>
                                                    <th>Precio U.</th>
                                                    <th>Importe</th>
                                                    <th>IVA</th>
                                                    <th>Total</th>
                                                    <th>Título</th>
                                                    <th>Descripción</th>
                                                    <th>Acciones</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-center">

            <div class="btns-finales">
                <a class="btn btn-default" href="<?= base_url("ciclo/precompromiso") ?>"><i class="fa fa-reply ic-color"></i> Regresar</a>
            </div>
        </div>
    </form>
</div>

</div>
<!-- /.row -->

</div>
<!-- /#page-wrapper -->