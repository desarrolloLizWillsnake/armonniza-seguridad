<h3 class="page-header title center"><i class="fa fa-plus-circle"></i> Generar Recaudado</h3>
<div id="page-wrapper">
    <form class="forma_devengado" role="form">
        <div class="row add-pre error-gral">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        General
                    </div>
                    <div class="panel-body">
                        <input type="hidden" name="ultimo" id="ultimo" value="<?= $ultimo ?>">
                        <div class="row">
                            <!--Primer Columna-->
                            <div class="col-lg-4">
                                <!---No. Consecutivo Recaudado-->
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-6"><label>No. Recaudado</label></div>
                                        <div class="col-lg-6"><p class="form-control-static input_ver"><?= $ultimo ?></p></div>
                                    </div>
                                </div>
                                <!---No. Devengado-->
                                <!--
                                <div class="form-group input-group">
                                    <input type="text" class="form-control marg_sup1" name="no_devengado" id="no_devengado" placeholder="No. Devengado" />
                                    <span class="input-group-btn ic-buscar-btn">
                                        <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_devengado" style="margin-top: -3px;"><i class="fa fa-search"></i></button>
                                    </span>
                                </div>
                                -->
                                <!--Clasificación-->
                                <select class="form-control" id="clasificacion" name="clasificacion">
                                    <option value="">Clasificación</option>
                                    <option value="Impuestos">Impuestos</option>
                                    <option value="Derechos">Derechos</option>
                                    <option value="Productos y/o Bienes">Productos y/o Bienes</option>
                                    <option value="Servicios">Servicios</option>
                                    <option value="Aprovechamientos">Aprovechamientos</option>
                                    <option value="Participaciones">Participaciones</option>
                                    <option value="Aportaciones">Aportaciones</option>
                                    <option value="Financiamientos">Financiamientos</option>
                                    <option value="Presupuesto">Presupuesto</option>
                                    <option value="Transferencias y/o Subsidios">Transferencias y/o Subsidios</option>
                                    <option value="Otros">Otros</option>
                                </select>

                                <!--No. Factura-->
                                <input type="text" class="form-control" name="no_factura" id="no_factura" value="" placeholder="No. Factura" />
                            </div>
                            <!--Fin Primer Columna-->
                            <!--Segunda Columna-->
                            <div class="col-lg-4">
                                <!--Clave Cliente-->
                                <input type="text" class="form-control" name="clave_cliente" id="clave_cliente" value="" placeholder="Clave Cliente" />
                                <!--Cliente-->
                                <input type="text" class="form-control" name="cliente" id="cliente" value="" placeholder="Nombre Cliente" />
                            </div>
                            <!--Fin Segunda Columna-->
                            <!--Tercera Columna-->
                            <div class="col-lg-4">
                                <!--Fecha Recaudado-->
                                <div class="row">
                                    <div class="col-lg-6"><label>Fecha Recaudado</label></div>
                                    <div class="col-lg-6"><input type="text" class="form-control" name="fecha_recaudado" id="fecha_recaudado" value="" /></div>
                                </div>
                                <!-- Descripción General-->
                                <textarea class="form-control" name="descripcion" id="descripcion" style="height: 8em;" placeholder="Descripción Recaudado"></textarea>
                            </div>
                            <!--Fin Tecera Columna-->
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row add-pre error-detalle">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Detalles
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="row niveles-pc" id="consulta_especifica" name="consulta_especifica" style="margin-left: 1%;">
                                    <!--Estructura Administrativa de ingresos-->
                                    <?= $niveles ?>
                                    <a href="#modal_estructura_ingresos" class="btn btn-default" data-toggle="modal" data-target="#modal_estructura_ingresos" style="margin-top: 0;">¿No conoces la
                                        <br/>estructura?</a>
                                </div>
                            </div>
                            <div class="col-lg-4"></div>
                            <div class="col-lg-4">
                                <!--Fecha -->
                                <input type="text" class="form-control ic-calendar" name="fecha_detalle" id="fecha_detalle" placeholder="Fecha">

                                <!--Subsidio-->
                                <div class="form-group input-group">
                                    <input type="text" class="form-control" name="subsidio" id="subsidio" placeholder="Subsidio" disabled/>
                                    <span class="input-group-btn ic-buscar-btn">
                                         <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_subsidio"><i class="fa fa-search"></i></button>
                                    </span>
                                </div>

                                <!--Cantidad -->
                                <input type="number" class="form-control" name="cantidad" id="cantidad" placeholder="Cantidad">

                                <!-- Forma de Pago -->
                                <select class="form-control" id="tipo_pago" name="tipo_pago" required="">
                                    <option value="">Forma de Pago</option>
                                    <option value="Efectivo">Efectivo</option>
                                    <option value="Tarjeta de Débito">Tarjeta de Débito</option>
                                    <option value="Tarjeta de Crédito">Tarjeta de Crédito</option>
                                    <option value="Puntos">Puntos</option>
                                    <option value="No Identificado">No Identificado</option>
                                </select>

                                <!-- Importe -->
                                <label>Importe</label>
                                <div class="form-group input-group">
                                    <span class="input-group-btn ic-peso-btn">
                                        <button class="btn btn-default" ><i class="fa fa-dollar"></i></button>
                                    </span>
                                    <input style="margin-top: .5%;" type="text" pattern="[0-9]+([\.|,][0-9]+)?" class="form-control dinero" name="importe" id="importe" placeholder="Importe">
                                </div>
                                <!--Descuentos -->
                                <label>Descuento</label>
                                <div class="form-group input-group">
                                    <span class="input-group-btn ic-peso-btn">
                                        <button class="btn btn-default" ><i class="fa fa-dollar"></i></button>
                                    </span>
                                    <input style="margin-top: .5%;" type="text" pattern="[0-9]+([\.|,][0-9]+)?" class="form-control dinero" name="descuento" id="descuentos" placeholder="Descuentos">
                                </div>
                                <!--IVA -->
                                <label>IVA</label>
                                <div class="form-group input-group">
                                    <span class="input-group-btn ic-peso-btn">
                                        <button class="btn btn-default" ><i class="fa fa-dollar"></i></button>
                                    </span>
                                    <input style="margin-top: .5%;" type="text" pattern="[0-9]+([\.|,][0-9]+)?" class="form-control dinero" name="iva" id="iva" placeholder="I.V.A.">
                                </div>

                                <!-- Descripción Detalle-->
                                <textarea class="form-control" name="descripcion_detalle" id="descripcion_detalle"  style="height: 20px;" placeholder="Descripción Detalle"></textarea>

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <button type="button" class="btn btn-green btn-block btn-gd" id="guardar_recaudado_detalle">Guardar Detalle</button>
                                <div id="resultado_insertar_detalle"></div>
                            </div>
                        </div>

                        <!-- Tabla Detalle -->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="text-center" id="resultado_insertar_detalle"></div>
                                <div class="panel panel-default">
                                    <div class="panel-body table-gral">
                                        <div class="table-responsive">
                                            <h4 id="suma_total" class="text-center"></h4>
                                            <input type="hidden" value="" name="importe_total" id="importe_total" />
                                            <table class="table table-striped table-bordered table-hover" id="tabla_datos_recaudado">
                                                <thead>
                                                <th>ID</th>
                                                <th width="8%">Gerencia</th>
                                                <th width="5%">C.R.</th>
                                                <th width="6%">Rubro</th>
                                                <th width="5%">Tipo</th>
                                                <th width="6.5%">Clase</th>
                                                <th width="10%">F. Aplicada</th>
                                                <th width="10%">Forma Pago</th>
                                                <th width="6%">Cant.</th>
                                                <th width="11%">Importe</th>
                                                <th width="11%">IVA</th>
                                                <th width="11%">Total</th>
                                                <th>Acciones</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <input type="hidden" value="" name="borrar_devengado_hidden" id="borrar_devengado_hidden" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-body text-center">
                <div class="form-group c-firme">
                    <h3>¿Recaudado en Firme?</h3>
                    <label class="checkbox-inline">
                        <input type="checkbox" id="check_firme" name="check_firme" />Sí
                    </label>
                </div>
            </div>
        </div>

        <div class="btns-finales text-center">
            <div class="text-center" id="resultado_insertar_caratula"></div>
            <a id="" href="<?= base_url("recaudacion/recaudado") ?>" class="btn btn-default"><i class="fa fa-reply ic-color"></i> Regresar</a>
            <input type="submit" id="guardar_recaudado" class="btn btn-green" name="guardar_recaudado" value="Guardar Recaudado"/>
        </div>
    </form>
</div>

<!-- Devengado -->
<div class="modal fade" id="modal_devengado" tabindex="-1" role="dialog" aria-labelledby="modal_devengado" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-file ic-modal"></i> Devengado</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-10">
                <input type="hidden" name="devengado_hidden" id="devengado_hidden" />
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tabla_devengado">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th width="6%">No.</th>
                            <th width="11%">Clasificación</th>
                            <th width="10%">Factura</th>
                            <th width="7%">C.R.</th>
                            <th width="10%">F. Solicitud</th>
                            <th width="10%">Importe</th>
                            <th width="11%">Creado por</th>
                            <th width="6%">Firme</th>
                            <th width="8%">Estatus</th>
                            <th width="19%">Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Editar Detalle -->
<div class="modal fade modal_editar" tabindex="-1" role="dialog" aria-labelledby="modal_editar" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-edit ic-modal"></i> Editar Detalle Recaudado</h4>
            </div>
            <div class="modal-body modal_edit_detalle">
                <form role="form">
                    <div class="form-group" style="margin: 0 auto; width: 60%;">
                        <input type="hidden" name="editar_id_detalle" id="editar_id_detalle" />
                        <label>Importe</label>
                        <input type="text" class="form-control dinero" name="editar_importe" id="editar_importe" />
                        <label>IVA</label>
                        <input type="text" class="form-control dinero" name="editar_iva" id="editar_iva" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-reply ic-color"></i> Regresar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_editar_detalle">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal estructura Ingresos-->
<div class="modal fade" id="modal_estructura_ingresos" tabindex="-1" role="dialog" aria-labelledby="modal_estructura_ingresos" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-indent-left" style="color: #25B7BC;"></span> Estructura de Ingresos</h4>
            </div>
            <div class="modal-body text-center">
                <?= $niveles_modal ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" disabled id="elegir_ultimoNivel">Elegir</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Subsidio-->
<div class="modal fade" id="modal_subsidio" tabindex="-1" role="dialog" aria-labelledby="modal_subsidio" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-indent-left" style="color: #25B7BC;"></span> Subsidio</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-3">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tabla_subsidio">
                        <thead>
                        <tr>
                            <th width="100">Código</th>
                            <th width="400">Fuentes de Financiamiento</th>
                            <th width="100">Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
