<?php
$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
$fecha = $meses[date('n')-1]." ".date('d');
$a�o = date("Y");
$hora = date('h:i:s A');
?>
<h3 class="page-header center"><i class="fa fa-laptop"></i> Panel Administrador</h3>
<div id="page-wrapper">
    <div class="cont-menu-admin left">
        <a class="menu-admin center" href="<?= base_url("administrador/empresa") ?>">
            <i class="fa fa-building fa-2x"></i><br><span>Empresa</span>
        </a>
        <a class="menu-admin center" href="<?= base_url("administrador/usuarios") ?>">
            <i class="fa fa-users fa-2x"></i><br><span>Usuarios</span>
        </a>
        <a class="menu-admin center" href="<?= base_url("administrador/catalogos") ?>">
            <i class="fa fa-cubes fa-2x"></i><br><span>Cat&aacute;logos</span>
        </a>
        <a class="menu-admin center" href="<?= base_url("administrador/autorizaciones") ?>">
            <i class="fa fa-check-square  fa-2x"></i><br><span>Autorizaciones</span>
        </a>
        <a class="menu-admin center" href="<?= base_url("administrador/config_estructuras") ?>">
            <i class="fa fa-bar-chart fa-2x"></i><br><span>Estructuras</span>
        </a>
        <a class="menu-admin center" href="<?= base_url("administrador/config_contabilidad") ?>">
            <i class="fa fa-dollar fa-2x"></i><br><span>Contabilidad</span>
        </a>
        <div class="menu-admin-calendar-ic center">
            <i class="fa fa-calendar  fa-2x"></i><br><span><?= $a�o ?></span>
        </div>
        <div class="menu-admin-calendar center">
            <span><b><?= $fecha ?></b></span><br><?= $hora ?>
        </div>
    </div>
    <div class="cont-admin">
        <div class="panel panel-default">
            <div class="panel-footer" style="padding: 4%;">
                <div class="container-fluid">
                    <h3 class="center" style="margin-top: -2%;">Estructuras Administrativas</h3>
                    <div class="row">
                        <div class="col-lg-1"></div>
                        <div class="col-lg-4 center" style="background: #FFF; padding: 2%; margin-top: 2%; margin-buttom: 2%;">
                            <h4>Ingresos</h4>
                            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#subirArchivoIngresos" data-whatever="Subir" style="margin-top: 5%; margin-buttom: 5%;"><i class="fa fa-upload ic-color"></i> Subir Archivo / Estructura Inicial</button>
                            <button type="button" class="btn btn-default" id="descargar_plantilla_ingresos" style="margin-top: 5%; margin-buttom: 5%;"><i class="fa fa-download ic-color"></i> Descargar Plantilla Ejemplo Ingresos</button>
                        </div>
                        <div class="col-lg-2"></div>
                        <div class="col-lg-4 center" style="background: #FFF; padding: 2%; margin-top: 2%; margin-buttom: 2%;">
                            <h4>Egresos</h4>
                            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#subirArchivoEgresos" data-whatever="Subir" style="margin-top: 5%; margin-buttom: 5%;"><i class="fa fa-upload ic-color"></i> Subir Archivo / Estructura Inicial</button>
                            <br>
                            <button type="button" class="btn btn-default" id="descargar_plantilla_egresos" style="margin-top: 5%; margin-buttom: 5%;"><i class="fa fa-download ic-color"></i> Descargar Plantilla Ejemplo Egresos</button>
                        </div>
                        <div class="col-lg-1"></div>

                        <!-- /.col-lg-12 -->
                    </div>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-footer" style="padding: 4%;">
                <div class="container-fluid">
                    <h3 class="center" style="margin-top: -2%;">Datos Fijos Estructura</h3>
                    <form class="form_datos_estructura" role="form" action="<?= base_url("administrador/insertar_datos_estructura") ?>" name="datos_empresa" method="POST">
                        <div class="row">
                            <div class="col-lg-1"></div>
                            <div class="col-lg-5">

                                <input type="hidden" name="id_datos_usuario" id="id_datos_usuario"/>

                                <label>Clasificaci&oacute;n Administrativa</label>
                                <input class="form-control" name="clasificacion_admin" id="clasificacion_admin" <?= $clasificacion_admin != NULL ? ' value="'.$clasificacion_admin.'"' : '';?> required/>

                                <label>Unidad Program&aacute;tica Presupuestaria</label>
                                <input class="form-control" name="unidad_programatica" id="unidad_programatica" <?= $unidad_programatica != NULL ? ' value="'.$unidad_programatica.'"' : '';?> required/>

                                <label>Programa Sectorial</label>
                                <input class="form-control" name="programa_sectorial" id="programa_sectorial" <?= $programa_sectorial != NULL ? ' value="'.$programa_sectorial.'"' : '';?> required/>

                                <label>Programa Presupuestario</label>
                                <input class="form-control" name="programa_presupuestario" id="programa_presupuestario" <?= $programa_presupuestario != NULL ? ' value="'.$programa_presupuestario.'"' : '';?> required/>

                                <label>Subprograma Presupuestario</label>
                                <input class="form-control" name="subprograma" id="subprograma" <?= $subprograma != NULL ? ' value="'.$subprograma.'"' : '';?> required/>
                            </div>
                            <div class="col-lg-5">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <label>Eje</label>
                                        <input class="form-control" name="eje" id="eje" <?= $eje != NULL ? ' value="'.$eje.'"' : '';?> required/>
                                    </div>
                                    <div class="col-lg-6">
                                        <label>Proyecto</label>
                                        <input class="form-control" name="proyecto" id="proyecto" <?= $proyecto != NULL ? ' value="'.$proyecto.'"' : '';?> required/>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <label>A&ntilde;o</label>
                                        <input class="form-control" name="anio" id="anio" <?= $anio != NULL ? ' value="'.$anio.'"' : '';?> required/>
                                    </div>
                                    <div class="col-lg-6">
                                        <label>Estado</label>
                                        <input class="form-control" name="estado" id="estado" <?= $estado != NULL ? ' value="'.$estado.'"' : '';?> required/>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <label>Tipo Grupo</label>
                                        <input class="form-control" name="tipo_grupo" id="tipo_grupo" <?= $tipo_grupo != NULL ? ' value="'.$tipo_grupo.'"' : '';?> required/>
                                    </div>
                                    <div class="col-lg-6">
                                        <label>Tipo Genero</label>
                                        <input class="form-control" name="tipo_genero" id="tipo_genero" <?= $tipo_genero != NULL ? ' value="'.$tipo_genero.'"' : '';?> required/>
                                    </div>
                                </div>

                                <label>Eje Plan Michoac&aacute;n</label>
                                <input class="form-control" name="eje_plan" id="eje_plan" <?= $eje_plan != NULL ? ' value="'.$eje_plan.'"' : '';?> required/>
                            </div>
                            <div class="col-lg-1"></div>
                        </div>
                    </form>

                    <div class="row">
                        <div class="col-lg-12 center" style="margin-top:3%;">
                            <div class="text-center" id="resultado_actualizacion"></div>
                            <button type="submit" id="actualizar_info"  name="actualizar_info" class="btn btn-green">Actualizar Informaci&oacute;n</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>



    </div>
</div>
<!-- Modal Subir Archivo Ingresos -->
<div class="modal fade" id="subirArchivoIngresos" tabindex="-1" role="dialog" aria-labelledby="subirArchivo" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-upload ic-color"></i> Estructura Administrativa de Ingresos</h4>
            </div>
            <div class="modal-body">
                <?php
                $attributes = array(
                    'role' => 'form',
                );

                echo(form_open_multipart('ingresos/subir_niveles', $attributes));
                ?>
                <div class="form-group center">
                    <div class="fileUpload btn btn-default btn-size">
                        <span>Selecciona Archivo</span>
                        <?php
                        //echo(form_label('', 'cammpoArchivo'));
                        $data = array(
                            'name'        => 'archivoSubir',
                            'id'          => 'archivoSubir',
                            'class'       => 'upload',
                            'accept' => '*',
                            'required' => 'required',
                        );

                        echo(form_upload($data));
                        ?>
                    </div>
                    <p class="help-block">El archivo a subir debe de tener la extensi&oacute;n .CSV</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-reply ic-color"></i> Regresar</button>
                <?php
                $atributos_submit = array(
                    'class' => 'btn btn-green',
                    'required' => 'required',
                );

                echo(form_submit($atributos_submit, 'Subir Archivo'));
                ?>
                <?php echo(form_close()); ?>
            </div>
        </div>
    </div>
</div>

<!-- Modal Subir Archivo Egresos-->

<div class="modal fade" id="subirArchivoEgresos" tabindex="-1" role="dialog" aria-labelledby="subirArchivo" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-upload ic-color"></i> Estructura Administrativa de Egresos</h4>
            </div>
            <div class="modal-body">
                <?php
                $attributes = array(
                    'role' => 'form',
                );

                echo(form_open_multipart('egresos/subir_niveles', $attributes));
                ?>
                <div class="form-group center">
                    <div class="fileUpload btn btn-default btn-size">
                        <span>Selecciona Archivo</span>
                        <?php
                        //echo(form_label('', 'cammpoArchivo'));
                        $data = array(
                            'name'        => 'archivoSubir',
                            'id'          => 'archivoSubir',
                            'class'       => 'upload',
                            'accept' => '*',
                            'required' => 'required',
                        );

                        echo(form_upload($data));
                        ?>
                    </div>
                    <p class="help-block">El archivo a subir debe de tener la extensi&oacute;n .CSV</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-reply ic-color"></i> Regresar</button>
                <?php
                $atributos_submit = array(
                    'class' => 'btn btn-green',
                    'required' => 'required',
                );

                echo(form_submit($atributos_submit, 'Subir Archivo'));
                ?>
                <?php echo(form_close()); ?>
            </div>
        </div>
    </div>
</div>
