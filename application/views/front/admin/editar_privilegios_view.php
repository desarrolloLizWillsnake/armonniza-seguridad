<?php
$sql = "SELECT * FROM permisos p LEFT JOIN modulos m ON p.modulo = m.nombre_modulo WHERE p.id_usuario = ?;";
$query = $this->db->query($sql, $id_usuario);
$arreglo_permisos = $query->result_array();
$permisos = array();

foreach($arreglo_permisos as $key => $value) {
    $permisos[$value["modulo"]] = $value["activo"];
}

//$this->debugeo->imprimir_pre($permisos);

$this->db->select('*')->from('modulos');
$modulos = $this->db->get()->result();
?>
<?php
$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
$fecha = $meses[date('n')-1]." ".date('d');
$año = date("Y");
$hora = date('h:i:s A');
?>
<h3 class="page-header center"><i class="fa fa-laptop"></i> Panel Administrador</h3>
<div id="page-wrapper">
    <div class="cont-menu-admin left">
        <a class="menu-admin center" href="<?= base_url("administrador/empresa") ?>">
            <i class="fa fa-building fa-2x"></i><br><span>Empresa</span>
        </a>
        <a class="menu-admin center" href="<?= base_url("administrador/usuarios") ?>">
            <i class="fa fa-users fa-2x"></i><br><span>Usuarios</span>
        </a>
        <a class="menu-admin center" href="<?= base_url("administrador/catalogos") ?>">
            <i class="fa fa-cubes fa-2x"></i><br><span>Catálogos</span>
        </a>
        <a class="menu-admin center" href="<?= base_url("administrador/autorizaciones") ?>">
            <i class="fa fa-check-square  fa-2x"></i><br><span>Autorizaciones</span>
        </a>
        <div class="menu-admin-calendar-ic center" style="margin-left: 15.99%;">
            <i class="fa fa-calendar  fa-2x"></i><br><span><?= $año ?></span>
        </div>
        <div class="menu-admin-calendar center">
            <span style="font-size: 1.45em;"><b><?= $fecha ?></b></span><br><?= $hora ?>
        </div>
    </div>

    <div class="row add-pre">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <h3 class="page-header sub-page center"><i class="fa fa-edit"></i> Privilegios Usuario</h3>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-8">
                            <input type="hidden" name="usuario" id="usuario" value="<?= $id_usuario ?>">
                            <label>Grupo</label>
                            <select name="grupo" id="grupo"  class="form-control" style="color: #555;">
                                <option value="">Grupo</option>
                                <option value="1" <?= $id_grupo == '1' ? ' selected="selected"' : '';?>>Administrador del Sistema</option>
                                <option value="2" <?= $id_grupo == '2' ? ' selected="selected"' : '';?>>Contabilidad</option>
                                <option value="3" <?= $id_grupo == '3' ? ' selected="selected"' : '';?>>Recursos Humanos</option>
                                <option value="4" <?= $id_grupo == '4' ? ' selected="selected"' : '';?>>Ventas</option>
                                <option value="5" <?= $id_grupo == '5' ? ' selected="selected"' : '';?>>Compras</option>
                                <option value="6" <?= $id_grupo == '6' ? ' selected="selected"' : '';?>>Sistemas</option>
                                <option value="7" <?= $id_grupo == '7' ? ' selected="selected"' : '';?>>Control Administrativo</option>
                                <option value="8" <?= $id_grupo == '8' ? ' selected="selected"' : '';?>>Programación y Evaluación</option>
                                <option value="9" <?= $id_grupo == '9' ? ' selected="selected"' : '';?>>Gerencia Técnico Administrativa</option>
                                <option value="10" <?= $id_grupo == '10' ? ' selected="selected"' : '';?>>Financieros</option>
                                <option value="11" <?= $id_grupo == '11' ? ' selected="selected"' : '';?>>Recursos Materiales </option>
                                <option value="12" <?= $id_grupo == '12' ? ' selected="selected"' : '';?>>Jefe de Recursos Materiales</option>
                                <option value="13" <?= $id_grupo == '13' ? ' selected="selected"' : '';?>>Enlace</option>
                                <option value="14" <?= $id_grupo == '14' ? ' selected="selected"' : '';?>>Gerencia de Librerías</option>
                                <option value="15" <?= $id_grupo == '15' ? ' selected="selected"' : '';?>>Librerías Zona Centro</option>
                                <option value="16" <?= $id_grupo == '16' ? ' selected="selected"' : '';?>>Funciones de Enlace</option>
                                <option value="17" <?= $id_grupo == '17' ? ' selected="selected"' : '';?>>Gerencia de Sistemas</option>
                                <option value="18" <?= $id_grupo == '18' ? ' selected="selected"' : '';?>>Unidad de Asuntos Jurídicos</option>
                                <option value="19" <?= $id_grupo == '19' ? ' selected="selected"' : '';?>>Gerencia de Distribución</option>
                                <option value="20" <?= $id_grupo == '20' ? ' selected="selected"' : '';?>>Organo interno de control</option>
                                <option value="21" <?= $id_grupo == '21' ? ' selected="selected"' : '';?>>Crédito y Cobranza</option>
                            </select>
                            <br />
                            <label>Módulos</label>
                            <!--<ul style="list-style: none;">
                                <li>
                                    <label class="checkbox-inline">
                                        <input type="checkbox" id="select_todos"  />Seleccionar / Quitar Todos
                                    </label>
                                </li>
                            </ul>
                            -->
                            <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="form-group">
                                <!-- Grupo Estructuras -->
                                <ul style="list-style: none; ">
                                    <li>
                                        <label class="checkbox-inline">
                                            <input type="checkbox"name="datos[estructuras]" id="estructuras" <?= isset($permisos["estructuras"]) && $permisos["estructuras"] == 1 ? ' checked="checked" ' : ''?> onclick="if(this.checked)document.getElementById('ingresos_maestro').checked=true;document.getElementById('ingresos_estructura').checked=true;document.getElementById('ingresos_adecuaciones').checked=true;document.getElementById('egresos_maestro').checked=true;document.getElementById('egresos_estructura').checked=true;document.getElementById('egresos_adecuaciones').checked=true;"/>Estructuras
                                        </label>
                                    </li>
                                    <li>
                                        <ul style="list-style: none;">
                                            <!-- Grupo Estructuras / Ingresos -->
                                            <li>
                                                <label class="checkbox-inline">
                                                    <input name="datos[ingresos_maestro]" type="checkbox" id="ingresos_maestro" <?= isset($permisos["ingresos_maestro"]) && $permisos["ingresos_maestro"] == 1 ? ' checked="checked" ' : ''?>/>Ingresos
                                                </label>
                                            </li>
                                            <li>
                                                <ul style="list-style: none;">
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[ingresos_estructura]" type="checkbox" id="ingresos_estructura"  <?= isset($permisos["ingresos_estructura"]) && $permisos["ingresos_estructura"] == 1 ? ' checked="checked" ' : ''?>/>Estructura Administrativa
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[ingresos_adecuaciones]" type="checkbox" id="ingresos_adecuaciones" <?= isset($permisos["ingresos_adecuaciones"]) && $permisos["ingresos_adecuaciones"] == 1 ? ' checked="checked" ' : ''?>/>Adecuaciones Presupuestarias
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[ingresos_consulta_estructura]" type="checkbox" id="ingresos_consulta_estructura" <?= isset($permisos["ingresos_consulta_estructura"]) && $permisos["ingresos_consulta_estructura"] == 1 ? ' checked="checked" ' : ''?>/>Consulta de partidas
                                                        </label>
                                                    </li>
                                                </ul>
                                            </li>
                                            <!-- Grupo Estructuras / Egresos -->
                                            <li>
                                                <label class="checkbox-inline">
                                                    <input name="datos[egresos_maestro]" type="checkbox" id="egresos_maestro" <?= isset($permisos["egresos_maestro"]) && $permisos["egresos_maestro"] == 1 ? ' checked="checked" ' : ''?>/>Egresos
                                                </label>
                                            </li>
                                            <li>
                                                <ul style="list-style: none;">
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[egresos_estructura]" type="checkbox" id="egresos_estructura" <?= isset($permisos["egresos_estructura"]) && $permisos["egresos_estructura"] == 1 ? ' checked="checked" ' : ''?>/>Estructura Administrativa
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[egresos_adecuaciones]" type="checkbox" id="egresos_adecuaciones" <?= isset($permisos["egresos_adecuaciones"]) && $permisos["egresos_adecuaciones"] == 1 ? ' checked="checked" ' : ''?>/>Adecuaciones Presupuestarias
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[egresos_consulta_estructura]" type="checkbox" id="egresos_consulta_estructura" <?= isset($permisos["egresos_consulta_estructura"]) && $permisos["egresos_consulta_estructura"] == 1 ? ' checked="checked" ' : '';?>/>Consulta de partidas
                                                        </label>
                                                    </li>
                                                </ul>
                                        </ul>
                                    </li>
                                </ul>
                                <!-- Grupo Ciclo -->
                                <ul style="list-style: none; ">
                                    <li>
                                        <label class="checkbox-inline">
                                            <input name="datos[ciclo_maestro]" type="checkbox" id="ciclo_maestro" <?= isset($permisos["ciclo_maestro"]) && $permisos["ciclo_maestro"] == 1 ? ' checked="checked" ' : ' ';?>/>Ciclo
                                        </label>
                                    </li>
                                    <li>
                                        <ul style="list-style: none;">
                                            <li>
                                                <label class="checkbox-inline">
                                                    <input name="datos[precompromiso]" type="checkbox" id="precompromiso" <?= isset($permisos["precompromiso"]) && $permisos["precompromiso"] == 1 ? ' checked="checked" ' : '';?> />Precompromiso
                                                </label>
                                                <ul style="list-style: none;">
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[agregar_precompromiso]" type="checkbox" id="agregar_precompromiso" <?= isset($permisos["agregar_precompromiso"]) && $permisos["agregar_precompromiso"] == 1 ? ' checked="checked" ' : '';?>/>Agregar Precompromiso
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[editar_precompromiso]" type="checkbox" id="editar_precompromiso" <?= isset($permisos["editar_precompromiso"]) && $permisos["editar_precompromiso"] == 1 ? ' checked="checked" ' : '';?>/>Editar Precompromiso
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[ver_precompromiso]" type="checkbox" id="ver_precompromiso" <?= isset($permisos["ver_precompromiso"]) && $permisos["ver_precompromiso"] == 1 ? ' checked="checked" ' : '';?>/>Ver Precompromiso
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[terminar_precompromiso]" type="checkbox" id="terminar_precompromiso" <?= isset($permisos["terminar_precompromiso"]) && $permisos["terminar_precompromiso"] == 1 ? ' checked="checked" ' : '';?>/>Terminar Precompromiso
                                                        </label>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <label class="checkbox-inline">
                                                    <input name="datos[compromiso]" type="checkbox" id="compromiso" <?= isset($permisos["compromiso"]) && $permisos["compromiso"] == 1 ? ' checked="checked" ' : '';?>/>Compromiso
                                                </label>
                                                <ul style="list-style: none;">
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[agregar_compromiso]" type="checkbox" id="agregar_compromiso" <?= isset($permisos["agregar_compromiso"]) && $permisos["agregar_compromiso"] == 1 ? ' checked="checked" ' : '';?>/>Agregar Compromiso
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[editar_compromiso]" type="checkbox" id="editar_compromiso" <?= isset($permisos["editar_compromiso"]) && $permisos["editar_compromiso"] == 1 ? ' checked="checked" ' : '';?>/>Editar Compromiso
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[ver_compromiso]" type="checkbox" id="ver_compromiso" <?= isset($permisos["ver_compromiso"]) && $permisos["ver_compromiso"] == 1 ? ' checked="checked" ' : '';?>/>Ver Compromiso
                                                        </label>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <label class="checkbox-inline">
                                                    <input name="datos[contrarecibo]" type="checkbox" id="contrarecibo" <?= isset($permisos["contrarecibo"]) && $permisos["contrarecibo"] == 1 ? ' checked="checked" ' : '';?>/>Contra recibo de pago
                                                </label>
                                                <ul style="list-style: none;">
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[agregar_contrarecibo]" type="checkbox" id="agregar_contrarecibo" <?= isset($permisos["agregar_contrarecibo"]) && $permisos["agregar_contrarecibo"] == 1 ? ' checked="checked" ' : '';?>/>Agregar Contra Recibo de Pago
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[editar_contrarecibo]" type="checkbox" id="editar_contrarecibo" <?= isset($permisos["editar_contrarecibo"]) && $permisos["editar_contrarecibo"] == 1 ? ' checked="checked" ' : '';?>/>Editar Contra Recibo de Pago
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[ver_contrarecibo]" type="checkbox" id="ver_contrarecibo" <?= isset($permisos["ver_contrarecibo"]) && $permisos["ver_contrarecibo"] == 1 ? ' checked="checked" ' : '';?>/>Ver Contra Recibo de Pago
                                                        </label>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <label class="checkbox-inline">
                                                    <input name="datos[tesoreria]" type="checkbox" id="tesoreria" <?= isset($permisos["tesoreria"]) && $permisos["tesoreria"] == 1 ? ' checked="checked" ' : '';?> />Tesorería
                                                </label>
                                                <ul style="list-style: none;">
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[movimientos_bancarios]" type="checkbox" id="movimientos_bancarios" <?= isset($permisos["movimientos_bancarios"]) && $permisos["movimientos_bancarios"] == 1 ? ' checked="checked" ' : '';?>/>Movimientos Bancarios
                                                        </label>
                                                        <ul style="list-style: none;">
                                                            <li>
                                                                <label class="checkbox-inline">
                                                                    <input name="datos[agregar_movimientos_bancarios]" type="checkbox" id="agregar_movimientos_bancarios" <?= isset($permisos["agregar_movimientos_bancarios"]) && $permisos["agregar_movimientos_bancarios"] == 1 ? ' checked="checked" ' : '';?>/>Agregar Movimiento Bancario
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="checkbox-inline">
                                                                    <input name="datos[editar_movimientos_bancarios]" type="checkbox" id="editar_movimientos_bancarios" <?= isset($permisos["editar_movimientos_bancarios"]) && $permisos["editar_movimientos_bancarios"] == 1 ? ' checked="checked" ' : '';?>/>Editar Movimiento Bancario
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="checkbox-inline">
                                                                    <input name="datos[ver_movimientos_bancarios]" type="checkbox" id="ver_movimientos_bancarios" <?= isset($permisos["ver_movimientos_bancarios"]) && $permisos["ver_movimientos_bancarios"] == 1 ? ' checked="checked" ' : '';?>/>Ver Movimiento Bancario
                                                                </label>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[conciliacion_bancaria]" type="checkbox" id="conciliacion_bancaria" <?= isset($permisos["conciliacion_bancaria"]) && $permisos["conciliacion_bancaria"] == 1 ? ' checked="checked" ' : '';?>/>Conciliación Bancaria
                                                        </label>
                                                        <ul style="list-style: none;">
                                                            <li>
                                                                <label class="checkbox-inline">
                                                                    <input name="datos[agregar_conciliacion_bancaria]" type="checkbox" id="agregar_conciliacion_bancaria" <?= isset($permisos["agregar_conciliacion_bancaria"]) && $permisos["agregar_conciliacion_bancaria"] == 1 ? ' checked="checked" ' : '';?>/>Agregar Conciliación Bancaria
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="checkbox-inline">
                                                                    <input name="datos[editar_conciliacion_bancaria]" type="checkbox" id="editar_conciliacion_bancaria" <?= isset($permisos["editar_conciliacion_bancaria"]) && $permisos["editar_conciliacion_bancaria"] == 1 ? ' checked="checked" ' : '';?>/>Editar Conciliación Bancaria
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="checkbox-inline">
                                                                    <input name="datos[ver_conciliacion_bancaria]" type="checkbox" id="ver_conciliacion_bancaria" <?= isset($permisos["ver_conciliacion_bancaria"]) && $permisos["ver_conciliacion_bancaria"] == 1 ? ' checked="checked" ' : '';?>/>Ver Conciliación Bancaria
                                                                </label>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                                <!-- Grupo Recaudación -->
                                <ul style="list-style: none; ">
                                    <li>
                                        <label class="checkbox-inline">
                                            <input name="datos[recaudacion]" type="checkbox" id="recaudacion" <?= isset($permisos["recaudacion"]) && $permisos["recaudacion"] == 1 ? ' checked="checked" ' : '';?>/>Recaudación
                                        </label>
                                    </li>
                                    <li>
                                        <ul style="list-style: none;">
                                            <li>
                                                <label class="checkbox-inline">
                                                    <input name="datos[devengado]" type="checkbox" id="devengado" <?= isset($permisos["devengado"]) && $permisos["devengado"] == 1 ? ' checked="checked" ' : '';?> />Devengado
                                                </label>
                                                <ul style="list-style: none;">
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[agregar_devengado]" type="checkbox" id="agregar_devengado" <?= isset($permisos["agregar_devengado"]) && $permisos["agregar_devengado"] == 1 ? ' checked="checked" ' : '';?> />Agregar Devengado
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[subir_devengado]" type="checkbox" id="subir_devengado" <?= isset($permisos["subir_devengado"]) && $permisos["subir_devengado"] == 1 ? ' checked="checked" ' : '';?> />Subir Devengado
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[editar_devengado]" type="checkbox" id="editar_devengado" <?= isset($permisos["editar_devengado"]) && $permisos["editar_devengado"] == 1 ? ' checked="checked" ' : '';?> />Editar Devengado
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[ver_devengado]" type="checkbox" id="ver_devengado" <?= isset($permisos["ver_devengado"]) && $permisos["ver_devengado"] == 1 ? ' checked="checked" ' : '';?> />Ver Devengado
                                                        </label>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <label class="checkbox-inline">
                                                    <input name="datos[recaudado]" type="checkbox" id="recaudado" <?= isset($permisos["recaudado"]) && $permisos["recaudado"] == 1 ? ' checked="checked" ' : '';?> />Recaudado
                                                </label>
                                                <ul style="list-style: none;">
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[agregar_recaudado]" type="checkbox" id="agregar_recaudado" <?= isset($permisos["agregar_recaudado"]) && $permisos["agregar_recaudado"] == 1 ? ' checked="checked" ' : '';?> />Agregar Recaudado
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[subir_recaudado]" type="checkbox" id="subir_recaudado" <?= isset($permisos["subir_recaudado"]) && $permisos["subir_recaudado"] == 1 ? ' checked="checked" ' : '';?> />Subir Recaudado
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[editar_recaudado]" type="checkbox" id="editar_recaudado" <?= isset($permisos["editar_recaudado"]) && $permisos["editar_recaudado"] == 1 ? ' checked="checked" ' : '';?> />Editar Recaudado
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[ver_recaudado]" type="checkbox" id="ver_recaudado" <?= isset($permisos["ver_recaudado"]) && $permisos["ver_recaudado"] == 1 ? ' checked="checked" ' : '';?> />Ver Recaudado
                                                        </label>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>

                                <!-- Grupo Contabilidad -->
                                <ul style="list-style: none; ">
                                    <li>
                                        <label class="checkbox-inline">
                                            <input name="datos[contabilidad]" type="checkbox" id="contabilidad" onclick="if(this.checked)document.getElementById('plan_cuentas').checked=true;document.getElementById('agregar_datos_cuentas').checked=true;document.getElementById('asientos_polizas').checked=true;document.getElementById('agregar_polizas').checked=true; document.getElementById('editar_polizas').checked=true; document.getElementById('ver_polizas').checked=true;document.getElementById('consulta_balanza').checked=true;document.getElementById('contabilidad_electronica').checked=true;document.getElementById('matriz_conversion').checked=true;document.getElementById('agregar_matriz').checked=true;document.getElementById('agregar_datos_matriz').checked=true;document.getElementById('auxiliar').checked=true;document.getElementById('periodos_contables').checked=true;document.getElementById('agregar_periodo').checked=true; document.getElementById('agregar_periodo_anual').checked=true;" <?= isset($permisos["contabilidad"]) && $permisos["contabilidad"] == 1 ? ' checked="checked" ' : '';?> />Contabilidad
                                        </label>
                                        <ul style="list-style: none;">
                                            <li>
                                                <label class="checkbox-inline">
                                                    <input name="datos[asientos_polizas]" type="checkbox" id="asientos_polizas" onclick="if(this.checked)document.getElementById('contabilidad').checked=true;" <?= isset($permisos["asientos_polizas"]) && $permisos["asientos_polizas"] == 1 ? ' checked="checked" ' : '';?>/>Asientos / Pólizas
                                                </label>
                                                <ul style="list-style: none;">
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[agregar_polizas]" type="checkbox" id="agregar_polizas" onclick="if(this.checked)document.getElementById('contabilidad').checked=true;" <?= isset($permisos["agregar_polizas"]) && $permisos["agregar_polizas"] == 1 ? ' checked="checked" ' : '';?>/>Agregar Pólizas
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[editar_polizas]" type="checkbox" id="editar_polizas" onclick="if(this.checked)document.getElementById('contabilidad').checked=true;" <?= isset($permisos["editar_polizas"]) && $permisos["editar_polizas"] == 1 ? ' checked="checked" ' : '';?>/>Editar Pólizas
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[ver_polizas]" type="checkbox" id="ver_polizas" onclick="if(this.checked)document.getElementById('contabilidad').checked=true;" <?= isset($permisos["ver_polizas"]) && $permisos["ver_polizas"] == 1 ? ' checked="checked" ' : '';?>/>Ver Pólizas
                                                        </label>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <label class="checkbox-inline">
                                                    <input name="datos[consulta_balanza]" type="checkbox" id="consulta_balanza" onclick="if(this.checked)document.getElementById('contabilidad').checked=true;" <?= isset($permisos["consulta_balanza"]) && $permisos["consulta_balanza"] == 1 ? ' checked="checked" ' : '';?>/>Consulta Balanza
                                                </label>
                                            </li>
                                            <li>
                                                <label class="checkbox-inline">
                                                    <input name="datos[auxiliar]" type="checkbox" id="auxiliar" onclick="if(this.checked)document.getElementById('contabilidad').checked=true;" <?= isset($permisos["auxiliar"]) && $permisos["auxiliar"] == 1 ? ' checked="checked" ' : '';?>/>Auxiliar
                                                </label>
                                            </li>
                                            <li>
                                                <label class="checkbox-inline">
                                                    <input name="datos[matriz_conversion]" type="checkbox" id="matriz_conversion" onclick="if(this.checked)document.getElementById('contabilidad').checked=true;" <?= isset($permisos["matriz_conversion"]) && $permisos["matriz_conversion"] == 1 ? ' checked="checked" ' : '';?>/>Matriz de Conversión
                                                </label>
                                                <ul style="list-style: none;">
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[agregar_matriz]" type="checkbox" id="agregar_matriz" onclick="if(this.checked)document.getElementById('contabilidad').checked=true;" <?= isset($permisos["agregar_matriz"]) && $permisos["agregar_matriz"] == 1 ? ' checked="checked" ' : '';?>/>Agregar Mátriz
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[agregar_datos_matriz]" type="checkbox" id="agregar_datos_matriz" onclick="if(this.checked)document.getElementById('contabilidad').checked=true;" <?= isset($permisos["agregar_datos_matriz"]) && $permisos["agregar_datos_matriz"] == 1 ? ' checked="checked" ' : '';?>/>Agregar, Editar y Eliminar Datos Tabla Matriz
                                                        </label>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <label class="checkbox-inline">
                                                    <input name="datos[plan_cuentas]" type="checkbox" id="plan_cuentas" onclick="if(this.checked)document.getElementById('contabilidad').checked=true;" <?= isset($permisos["plan_cuentas"]) && $permisos["plan_cuentas"] == 1 ? ' checked="checked" ' : '';?>/>Plan de cuentas
                                                </label>
                                                <ul style="list-style: none;">
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[agregar_datos_cuentas]" type="checkbox" id="agregar_datos_cuentas" onclick="if(this.checked)document.getElementById('contabilidad').checked=true;" <?= isset($permisos["agregar_datos_cuentas"]) && $permisos["agregar_datos_cuentas"] == 1 ? ' checked="checked" ' : '';?>/>Agregar, Editar y Eliminar Datos Tabla Cuentas
                                                        </label>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <label class="checkbox-inline">
                                                    <input name="datos[periodos_contables]" type="checkbox" id="periodos_contables" onclick="if(this.checked)document.getElementById('contabilidad').checked=true;" <?= isset($permisos["periodos_contables"]) && $permisos["periodos_contables"] == 1 ? ' checked="checked" ' : '';?>/>Períodos Contables
                                                </label>
                                                <ul style="list-style: none;">
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[agregar_periodo]" type="checkbox" id="agregar_periodo" onclick="if(this.checked)document.getElementById('contabilidad').checked=true;" <?= isset($permisos["agregar_periodo"]) && $permisos["agregar_periodo"] == 1 ? ' checked="checked" ' : '';?>/>Agregar Periodo
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[agregar_periodo_anual]" type="checkbox" id="agregar_periodo_anual" onclick="if(this.checked)document.getElementById('contabilidad').checked=true;" <?= isset($permisos["agregar_periodo_anual"]) && $permisos["agregar_periodo_anual"] == 1 ? ' checked="checked" ' : '';?>/>Agregar Periodo Anual
                                                        </label>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <label class="checkbox-inline">
                                                    <input name="datos[contabilidad_electronica]" type="checkbox" id="contabilidad_electronica" onclick="if(this.checked)document.getElementById('contabilidad').checked=true;" <?= isset($permisos["contabilidad_electronica"]) && $permisos["contabilidad_electronica"] == 1 ? ' checked="checked" ' : '';?>/>Contabilidad Electrónica
                                                </label>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                                <!-- Grupo Patrimonio -->
                                <ul style="list-style: none; ">
                                    <li>
                                        <label class="checkbox-inline">
                                            <input name="datos[patrimonio]" type="checkbox" id="patrimonio" <?= isset($permisos["patrimonio"]) && $permisos["patrimonio"] == 1 ? ' checked="checked" ' : '';?>/>Patrimonio
                                        </label>
                                        <ul style="list-style: none;">
                                            <li>
                                                <label class="checkbox-inline">
                                                    <input name="datos[nota_entrada]" type="checkbox" id="nota_entrada" <?= isset($permisos["nota_entrada"]) && $permisos["nota_entrada"] == 1 ? ' checked="checked" ' : '';?>/>Nota de Entrada
                                                </label>
                                                <ul style="list-style: none;">
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[agregar_nota_entrada]" type="checkbox" id="agregar_nota_entrada" <?= isset($permisos["agregar_nota_entrada"]) && $permisos["agregar_nota_entrada"] == 1 ? ' checked="checked" ' : '';?>/>Agregar Nota de Entrada
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[editar_nota_entrada]" type="checkbox" id="editar_nota_entrada" <?= isset($permisos["editar_nota_entrada"]) && $permisos["editar_nota_entrada"] == 1 ? ' checked="checked" ' : '';?>/>Editar Nota de Entrada
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[ver_nota_entrada]" type="checkbox" id="ver_nota_entrada" <?= isset($permisos["ver_nota_entrada"]) && $permisos["ver_nota_entrada"] == 1 ? ' checked="checked" ' : '';?>/>Ver Nota de Entrada
                                                        </label>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <label class="checkbox-inline">
                                                    <input name="datos[nota_salida]" type="checkbox" id="nota_salida" <?= isset($permisos["nota_salida"]) && $permisos["nota_salida"] == 1 ? ' checked="checked" ' : '';?>/>Nota de Salida
                                                </label>
                                                <ul style="list-style: none;">
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[agregar_nota_salida]" type="checkbox" id="agregar_nota_salida" <?= isset($permisos["agregar_nota_salida"]) && $permisos["agregar_nota_salida"] == 1 ? ' checked="checked" ' : '';?>/>Agregar Nota de Salida
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[editar_nota_salida]" type="checkbox" id="editar_nota_salida" <?= isset($permisos["editar_nota_salida"]) && $permisos["editar_nota_salida"] == 1 ? ' checked="checked" ' : '';?>/>Editar Nota de Salida
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[ver_nota_salida]" type="checkbox" id="ver_nota_salida" <?= isset($permisos["ver_nota_salida"]) && $permisos["ver_nota_salida"] == 1 ? ' checked="checked" ' : '';?>/>Ver Nota de Salida
                                                        </label>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <label class="checkbox-inline">
                                                    <input name="datos[kardex]" type="checkbox" id="kardex" <?= isset($permisos["kardex"]) && $permisos["kardex"] == 1 ? ' checked="checked" ' : '';?>/>Kardex de Existencias
                                                </label>
                                            </li>
                                            <li>
                                                <label class="checkbox-inline">
                                                    <input name="datos[inventarios]" type="checkbox" id="inventarios" <?= isset($permisos["inventarios"]) && $permisos["inventarios"] == 1 ? ' checked="checked" ' : '';?>/>Consulta Inventarios
                                                </label>
                                            </li>
                                            <li>
                                                <label class="checkbox-inline">
                                                    <input name="datos[clasificador_productos]" type="checkbox" id="clasificador_productos" <?= isset($permisos["clasificador_productos"]) && $permisos["clasificador_productos"] == 1 ? ' checked="checked" ' : '';?>/>Clasificador de Productos
                                                </label>
                                                <ul style="list-style: none;">
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[agregar_producto]" type="checkbox" id="agregar_producto" <?= isset($permisos["agregar_producto"]) && $permisos["agregar_producto"] == 1 ? ' checked="checked" ' : '';?>/>Agregar Producto
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[editar_producto]" type="checkbox" id="editar_producto" <?= isset($permisos["editar_producto"]) && $permisos["editar_producto"] == 1 ? ' checked="checked" ' : '';?>/>Editar Producto
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[ver_producto]" type="checkbox" id="ver_producto" <?= isset($permisos["ver_producto"]) && $permisos["ver_producto"] == 1 ? ' checked="checked" ' : '';?>/>Ver Producto
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[eliminar_producto]" type="checkbox" id="eliminar_producto" <?= isset($permisos["eliminar_producto"]) && $permisos["eliminar_producto"] == 1 ? ' checked="checked" ' : '';?>/>Eliminar Producto
                                                        </label>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                                <!-- Grupo Anteproyecto -->
                                <ul style="list-style: none; ">
                                    <li>
                                        <label class="checkbox-inline">
                                            <input name="datos[anteproyecto]" type="checkbox" id="anteproyecto" <?= isset($permisos["anteproyecto"]) && $permisos["anteproyecto"] == 1 ? ' checked="checked" ' : '';?>/>Anteproyecto
                                        </label>
                                        <ul style="list-style: none;">
                                            <li>
                                                <label class="checkbox-inline">
                                                    <input name="datos[presupuesto_aprobado]" type="checkbox" id="presupuesto_aprobado" <?= isset($permisos["presupuesto_aprobado"]) && $permisos["presupuesto_aprobado"] == 1 ? ' checked="checked" ' : '';?>/>Anteproyecto 2016
                                                </label>
                                                <ul style="list-style: none; ">
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[subir_anio_anteproyecto]" type="checkbox" id="subir_anio_anteproyecto" <?= isset($permisos["subir_anio_anteproyecto"]) && $permisos["subir_anio_anteproyecto"] == 1 ? ' checked="checked" ' : '';?>/>Subir Año
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[subir_fondos_anteproyecto]" type="checkbox" id="subir_fondos_anteproyecto" <?= isset($permisos["subir_fondos_anteproyecto"]) && $permisos["subir_fondos_anteproyecto"] == 1 ? ' checked="checked" ' : '';?>/>Subir Fondos
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[aprobado_anteproyecto]" type="checkbox" id="aprobado_anteproyecto" <?= isset($permisos["aprobado_anteproyecto"]) && $permisos["aprobado_anteproyecto"] == 1 ? ' checked="checked" ' : '';?>/>Aprobado
                                                        </label>
                                                    </li>
                                                </ul>
                                            <li>
                                                <label class="checkbox-inline">
                                                    <input name="datos[indicadores_anteproyecto]" type="checkbox" id="indicadores_anteproyecto" <?= isset($permisos["indicadores_anteproyecto"]) && $permisos["indicadores_anteproyecto"] == 1 ? ' checked="checked" ' : '';?>/>Indicadores
                                                </label>
                                            </li>
                                            <li>
                                                <label class="checkbox-inline">
                                                    <input name="datos[servicios_subrogados_anteproyecto]" type="checkbox" id="servicios_subrogados_anteproyecto" <?= isset($permisos["servicios_subrogados_anteproyecto"]) && $permisos["servicios_subrogados_anteproyecto"] == 1 ? ' checked="checked" ' : '';?>/>Servicios Subrogados
                                                </label>
                                            </li>
                                            <li>
                                                <label class="checkbox-inline">
                                                    <input name="datos[catalogos_anteproyecto]" type="checkbox" id="catalogos_anteproyecto" <?= isset($permisos["catalogos_anteproyecto"]) && $permisos["catalogos_anteproyecto"] == 1 ? ' checked="checked" ' : '';?>/>Catalogos
                                                </label>
                                                <ul style="list-style: none;" >
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[botones_cc]" type="checkbox" id="botones_cc" <?= isset($permisos["botones_cc"]) && $permisos["botones_cc"] == 1 ? ' checked="checked" ' : '';?>/>Botones CC (Nuevo/Editar/Borrar)
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[botones_partidas]" type="checkbox" id="botones_partidas" <?= isset($permisos["botones_partidas"]) && $permisos["botones_partidas"] == 1 ? ' checked="checked" ' : '';?>/>Botones Partidas (Nuevo/Editar/Borrar)
                                                        </label>
                                                    </li>
                                                </ul>
                                            </li>
                                    </li>
                                </ul>

                                    <li>
                                        <label class="checkbox-inline">
                                            <input name="datos[reportes]" type="checkbox" id="reportes" <?= isset($permisos["reportes"]) && $permisos["reportes"] == 1 ? ' checked="checked" ' : '';?>/>Reportes
                                        </label>
                                    </li>
                                    <li>
                                        <label class="checkbox-inline">
                                            <input name="datos[catalogos]" type="checkbox" id="catalogos"<?= isset($permisos["catalogos"]) && $permisos["catalogos"] == 1 ? ' checked="checked" ' : '';?> />Catálogos
                                        </label>
                                    </li>
                                    <li>
                                        <label class="checkbox-inline">
                                            <input name="datos[politicas_manuales]" type="checkbox" id="politicas_manuales" <?= isset($permisos["politicas_manuales"]) && $permisos["politicas_manuales"] == 1 ? ' checked="checked" ' : '';?>/>Políticas / Manuales
                                        </label>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                            <!--Firmas-->
                            <label>¿El usuario firma?</label>
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <input type="radio" name="firma" value="1" <?= $firma == 1 ? ' checked="checked" disabled' : '';?>>&nbsp;&nbsp;Firma 1<br>
                                    <input type="radio" name="firma" value="2" <?= $firma == 2 ? ' checked="checked" disabled' : '';?>>&nbsp;&nbsp;Firma 2<br>
                                    <input type="radio" name="firma" value="3" <?= $firma == 3 ? ' checked="checked" disabled' : '';?>>&nbsp;&nbsp;Firma 3
                                </div>
                            </div>

                            <div class="col-lg-12 center" style="margin-bottom: 4%;">
                                <div class="text-center" id="resultado_insertar_caratula"></div>
                                <a class="btn btn-default" href="<?= base_url("administrador/usuarios") ?>" ><i class="fa fa-reply ic-color"></i> Regresar</a>
                                <?php echo form_submit( array("id" => "guardar_privilegios", "name" => "guardar_privilegios", "class" => "btn btn-green" ), 'Guardar Cambios'); ?>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                        <div class="col-lg-2"></div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>
<!-- /.row -->

</div>
<!-- /#page-wrapper -->

<!-- <?php echo form_open($this->uri->uri_string()); ?> -->
