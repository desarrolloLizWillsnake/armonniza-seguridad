<?php
$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
$fecha = $meses[date('n')-1]." ".date('d');
$a�o = date("Y");
$hora = date('h:i:s A');
?>
<h3 class="page-header center"><i class="fa fa-laptop"></i> Panel Administrador</h3>
<div id="page-wrapper">
    <div class="cont-menu-admin left">
        <a class="menu-admin center" href="<?= base_url("administrador/empresa") ?>">
            <i class="fa fa-building fa-2x"></i><br><span>Empresa</span>
        </a>
        <a class="menu-admin center" href="<?= base_url("administrador/usuarios") ?>">
            <i class="fa fa-users fa-2x"></i><br><span>Usuarios</span>
        </a>
        <a class="menu-admin center" href="<?= base_url("administrador/catalogos") ?>">
            <i class="fa fa-cubes fa-2x"></i><br><span>Cat&aacute;logos</span>
        </a>
        <a class="menu-admin center" href="<?= base_url("administrador/autorizaciones") ?>">
            <i class="fa fa-check-square  fa-2x"></i><br><span>Autorizaciones</span>
        </a>
        <a class="menu-admin center" href="<?= base_url("administrador/config_estructuras") ?>">
            <i class="fa fa-bar-chart fa-2x"></i><br><span>Estructuras</span>
        </a>
        <a class="menu-admin center" href="<?= base_url("administrador/config_contabilidad") ?>">
            <i class="fa fa-dollar fa-2x"></i><br><span>Contabilidad</span>
        </a>
        <div class="menu-admin-calendar-ic center">
            <i class="fa fa-calendar  fa-2x"></i><br><span><?= $a�o ?></span>
        </div>
        <div class="menu-admin-calendar center">
            <span><b><?= $fecha ?></b></span><br><?= $hora ?>
        </div>
    </div>
    <div class="row add-pre">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <h3 class="page-header sub-page center"><i class="fa fa-check-circle"></i> Cat&aacute;logo Autorizadores <span style="font-size: .7em;">Ciclo Administrativo</span></h3>
                <div class="panel-body">
                    <form class="forma_usuarios" role="form">
                        <div class="row">
                            <div class="col-lg-4"></div>
                            <div class="col-lg-4">

                                <input type="hidden" name="id_autorizador" id="id_autorizador" value="3">
                                <h5> Nombre</h5>
                                <input type="text" class="form-control" name="nombre_autorizador" id="nombre_autorizador" <?= $nombre != NULL ? ' value="'.$nombre.'"' : '';?>  required/>
                                <h5> Puesto</h5>
                                <input type="text" class="form-control" name="puesto_autorizador" id="puesto_autorizador" <?= $puesto != NULL ? ' value="'.$puesto.'"' : '';?>  required/>
                            </div>
                            <div class="col-lg-4"></div>
                        </div>
                        <div class="btns-finales text-center">
                            <div class="text-center" id="resultado_insertar_caratula"></div>
                            <a class="btn btn-default" href="<?= base_url("administrador/autorizaciones_ciclo") ?>" ><i class="fa fa-reply ic-color"></i> Regresar</a>
                            <input type="submit" id="guardar_autorizacion" class="btn btn-green" name="guardar_autorizacion" value="Actualizar Datos"/>
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>

</div>
</div>
