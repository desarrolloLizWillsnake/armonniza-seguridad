<?php
$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
$fecha = $meses[date('n')-1]." ".date('d');
$año = date("Y");
$hora = date('h:i:s A');
?>
<h3 class="page-header center"><i class="fa fa-laptop"></i> Panel Administrador</h3>
<div id="page-wrapper">
    <div class="cont-menu-admin left">
        <a class="menu-admin center" href="<?= base_url("administrador/empresa") ?>">
            <i class="fa fa-building fa-2x"></i><br><span>Empresa</span>
        </a>
        <a class="menu-admin center" href="<?= base_url("administrador/usuarios") ?>">
            <i class="fa fa-users fa-2x"></i><br><span>Usuarios</span>
        </a>
        <a class="menu-admin center" href="<?= base_url("administrador/catalogos") ?>">
            <i class="fa fa-cubes fa-2x"></i><br><span>Catálogos</span>
        </a>
        <a class="menu-admin center" href="<?= base_url("administrador/autorizaciones") ?>">
            <i class="fa fa-check-square  fa-2x"></i><br><span>Autorizaciones</span>
        </a>
        <a class="menu-admin center" href="<?= base_url("administrador/config_estructuras") ?>">
            <i class="fa fa-bar-chart fa-2x"></i><br><span>Estructuras</span>
        </a>
        <a class="menu-admin center" href="<?= base_url("administrador/config_contabilidad") ?>">
            <i class="fa fa-dollar fa-2x"></i><br><span>Contabilidad</span>
        </a>
        <div class="menu-admin-calendar-ic center">
            <i class="fa fa-calendar  fa-2x"></i><br><span><?= $año ?></span>
        </div>
        <div class="menu-admin-calendar center">
            <span><b><?= $fecha ?></b></span><br><?= $hora ?>
        </div>
    </div>
    <div class="cont-admin">
        <div class="row center cont">
            <div class="col-lg-12">
                <a href="<?= base_url("auth/register") ?>" class="btn btn-default"><i class="fa fa-plus-circle circle ic-color"></i> Agregar Usuario</a>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body table-gral">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover datos_tabla">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th width="23%">Nombre</th>
                                    <th width="15%">Puesto</th>
                                    <th width="19%">Correo</th>
                                    <th width="10%">F. Registro</th>
                                    <th width="10%">Estatus</th>
                                    <th width="25%">Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Modal Activar Baja de Usuario-->
<div class="modal fade modal_baja" tabindex="-1" role="dialog" aria-labelledby="modal_baja" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-user-times ic-modal"></i> Baja Usuario</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="message-text" id="mensaje_baja" class="control-label">¿Realmente desea dar de baja el usuario seleccionado?</label>
                        <input type="hidden" value="" name="baja_usuario_id" id="baja_usuario_id" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-reply ic-color"></i> Regresar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_baja">Aceptar</button>
            </div>
        </div>
    </div>
</div>
<!--Modal Activar Usuario-->
<div class="modal fade modal_activar" tabindex="-1" role="dialog" aria-labelledby="modal_activar" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-user ic-modal"></i> Activar Usuario</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="message-text" id="mensaje_activar" class="control-label">¿Realmente desea activar el usuario seleccionado?</label>
                        <input type="hidden" value="" name="activar_usuario_id" id="activar_usuario_id" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-reply ic-color"></i> Regresar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_activar">Aceptar</button>
            </div>
        </div>
    </div>
</div>
<!--Modal Bloqueo Usuario-->
<div class="modal fade modal_bloquear" tabindex="-1" role="dialog" aria-labelledby="modal_bloquear" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-times-circle-o ic-modal"></i> Bloquear Usuario</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="message-text" id="mensaje_bloquear" class="control-label">¿Realmente desea bloquear el usuario seleccionado?</label>
                        <input type="hidden" value="" name="bloquear_usuario_id" id="bloquear_usuario_id" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-reply ic-color"></i> Regresar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_bloquear">Aceptar</button>
            </div>
        </div>
    </div>
</div>
<!--Modal Desbloqueo Usuario-->
<div class="modal fade modal_desbloquear" tabindex="-1" role="dialog" aria-labelledby="modal_desbloquear" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-check-circle-o ic-modal"></i> Desbloquear Usuario</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="message-text" id="mensaje_desbloquear" class="control-label">¿Realmente desea desbloquear el usuario seleccionado?</label>
                        <input type="hidden" value="" name="desbloquear_usuario_id" id="desbloquear_usuario_id" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-reply ic-color"></i> Regresar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_desbloquear">Aceptar</button>
            </div>
        </div>
    </div>
</div>
<!--Modal Restaurar Contraseña Usuario-->
<div class="modal fade modal_restaurar_contraseña" tabindex="-1" role="dialog" aria-labelledby="modal_restaurar_contraseña" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-refresh ic-modal"></i> Restaurar Contraseña</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="message-text" id="mensaje_restaurar" class="control-label">¿Realmente desea restaurar la contraseña del usuario seleccionado?</label>
                        <input type="hidden" value="" name="restaurar_usuario_id" id="restaurar_usuario_id" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-reply ic-color"></i> Regresar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_restaurar">Aceptar</button>
            </div>
        </div>
    </div>
</div>
<!--Modal Resultado de Restaurar Contraseña Usuario-->
<div class="modal fade resultado_restaurar_contraseña" tabindex="-1" role="dialog" aria-labelledby="resultado_restaurar_contraseña" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-refresh ic-modal"></i> Resultado Restaurar Contraseña</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="message-text" class="control-label"><p>El resultado ha sido éxitoso, la contraseña ha sido restaurada.</p></label>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-green" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>