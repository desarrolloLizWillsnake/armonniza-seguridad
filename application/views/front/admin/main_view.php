    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row cont-btns-c center">
                <div class="col-lg-12">
                    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#subirArchivo" data-whatever="Subir"><i class="fa fa-upload" style="color: #B6CE33;"></i> Subir Archivo de Compromisos</button>
                    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#subirArchivoPolizas" data-whatever="Subir"><i class="fa fa-upload" style="color: #B6CE33;"></i> Subir Archivo de P&oacute;lizas</button>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->

    <div class="modal fade" id="subirArchivo" tabindex="-1" role="dialog" aria-labelledby="subirArchivo" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="exampleModalLabel"><span class="glyphicon glyphicon-indent-left"></span> Subir Archivo de Compromiso</h4>
                </div>
                <div class="modal-body">
                    <?php
                    $attributes = array(
                        'role' => 'form',
                    );

                    echo(form_open_multipart('administrador/subir_archivo', $attributes));
                    ?>
                    <div class="form-group center">
                        <div class="fileUpload btn btn-default btn-size">
                            <span>Selecciona Archivo</span>
                            <?php
                            $data = array(
                                'name'        => 'archivoSubir',
                                'id'          => 'archivoSubir',
                                'class'       => 'upload',
                                'accept' => 'application/vnd.ms-excel',
                                'required' => 'required',
                            );

                            echo(form_upload($data));
                            ?>
                        </div>
                        <p class="help-block">El archivo a subir debe de tener la extensi&oacute;n .XLS</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <?php
                    $atributos_submit = array(
                        'class' => 'btn btn-default',
                        'required' => 'required',
                    );

                    echo(form_submit($atributos_submit, 'Subir Archivo'));
                    ?>
                    <?php echo(form_close()); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="subirArchivoPrecompro" tabindex="-1" role="dialog" aria-labelledby="subirArchivoPrecompro" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="exampleModalLabel"><span class="glyphicon glyphicon-indent-left"></span> Subir Archivo de Precompromisos</h4>
                </div>
                <div class="modal-body">
                    <?php
                    $attributes = array(
                        'role' => 'form',
                    );

                    echo(form_open_multipart('administrador/subir_archivo_precompromiso', $attributes));
                    ?>
                    <div class="form-group center">
                        <div class="fileUpload btn btn-default btn-size">
                            <span>Selecciona Archivo</span>
                            <?php
                            $data = array(
                                'name'        => 'archivoSubir',
                                'id'          => 'archivoSubir',
                                'class'       => 'upload',
                                'accept' => 'application/vnd.ms-excel',
                                'required' => 'required',
                            );

                            echo(form_upload($data));
                            ?>
                        </div>
                        <p class="help-block">El archivo a subir debe de tener la extensión .XLS</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <?php
                    $atributos_submit = array(
                        'class' => 'btn btn-default',
                        'required' => 'required',
                    );

                    echo(form_submit($atributos_submit, 'Subir Archivo'));
                    ?>
                    <?php echo(form_close()); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="subirArchivoPolizas" tabindex="-1" role="dialog" aria-labelledby="subirArchivoPolizas" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="exampleModalLabel"><span class="glyphicon glyphicon-indent-left"></span> Subir Archivo de Pólizas</h4>
                </div>
                <div class="modal-body">
                    <?php
                    $attributes = array(
                        'role' => 'form',
                    );

                    echo(form_open_multipart('administrador/subir_archivo_polizas', $attributes));
                    ?>
                    <div class="form-group center">
                        <div class="fileUpload btn btn-default btn-size">
                            <span>Selecciona Archivo</span>
                            <?php
                            $data = array(
                                'name'        => 'archivoSubir',
                                'id'          => 'archivoSubir',
                                'class'       => 'upload',
                                'accept' => 'application/vnd.ms-excel',
                                'required' => 'required',
                            );

                            echo(form_upload($data));
                            ?>
                        </div>
                        <p class="help-block">El archivo a subir debe de tener la extensión .XLS</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <?php
                    $atributos_submit = array(
                        'class' => 'btn btn-default',
                        'required' => 'required',
                    );

                    echo(form_submit($atributos_submit, 'Subir Archivo'));
                    ?>
                    <?php echo(form_close()); ?>
                </div>
            </div>
        </div>
    </div>