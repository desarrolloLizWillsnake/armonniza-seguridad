<?php
$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
$fecha = $meses[date('n')-1]." ".date('d');
$año = date("Y");
$hora = date('h:i:s A');

$imagen = 'logo.png';
?>
<h3 class="page-header center"><i class="fa fa-laptop"></i> Panel Administrador</h3>
<div id="page-wrapper" style="padding-bottom: 5%;" >
    <div class="cont-menu-admin left">
        <a class="menu-admin center" href="<?= base_url("administrador/empresa") ?>">
            <i class="fa fa-building fa-2x"></i><br><span>Empresa</span>
        </a>
        <a class="menu-admin center" href="<?= base_url("administrador/usuarios") ?>">
            <i class="fa fa-users fa-2x"></i><br><span>Usuarios</span>
        </a>
        <a class="menu-admin center" href="<?= base_url("administrador/catalogos") ?>">
            <i class="fa fa-cubes fa-2x"></i><br><span>Catálogos</span>
        </a>
        <a class="menu-admin center" href="<?= base_url("administrador/autorizaciones") ?>">
            <i class="fa fa-check-square  fa-2x"></i><br><span>Autorizaciones</span>
        </a>
        <a class="menu-admin center" href="<?= base_url("administrador/config_estructuras") ?>">
            <i class="fa fa-bar-chart fa-2x"></i><br><span>Estructuras</span>
        </a>
        <a class="menu-admin center" href="<?= base_url("administrador/config_contabilidad") ?>">
            <i class="fa fa-dollar fa-2x"></i><br><span>Contabilidad</span>
        </a>
        <div class="menu-admin-calendar-ic center">
            <i class="fa fa-calendar  fa-2x"></i><br><span><?= $año ?></span>
        </div>
        <div class="menu-admin-calendar center">
            <span><b><?= $fecha ?></b></span><br><?= $hora ?>
        </div>
    </div>
    <div class="cont-admin">
        <div class="panel panel-default">
            <div class="panel-footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="row cont-admin-img">
                                <div class="col-lg-3"></div>
                                <div class="col-lg-6 center">
                                    <img id="imagen_logotipo" src="<?=base_url()?>img/<?=$imagen?>" />
                                </div>
                                <div class="col-lg-3"></div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2"></div>
                                <div class="col-lg-8">
                                    <div class="btn-perfil center">
                                        <h5>Tamaño Máx.: 320 x 480</h5>
                                        <h5>Formatos Permitidos: .png / .jpg  / .gif</h5>
                                        <br>
                                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#subirArchivo" data-whatever="Subir"><i class="fa fa-refresh ic-color"></i> Actualizar Logotipo</button>
                                    </div>
                                </div>
                                <div class="col-lg-2"></div>
                            </div>

                        </div>
                        <div class="col-lg-6">
                            <?php
                            if(isset($mensaje)){ ?>
                                <div class="alert alert-success alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <?= $mensaje ?>
                                </div>
                            <?php } ?>
                            <form role="form" action="<?= base_url("administrador/insertar_datos_empresa") ?>" name="datos_empresa" method="POST">
                                <div class="form-group">
                                    <input type="hidden" name="id_datos_usuario" id="id_datos_usuario"/>
                                    <input type="hidden" name="id_usuario" id="id_usuario" />
                                    <div class="row cont">
                                        <div class="col-lg-4"><label>Nombre Empresa</label></div>
                                        <div class="col-lg-8"><input class="form-control" name="nombre_empresa" id="nombre_empresa" placeholder="Nombre Empresa" <?= $nombre != NULL ? ' value="'.$nombre.'"' : '';?> required/></div>
                                    </div>
                                    <div class="row cont">
                                        <div class="col-lg-4"> <label>R.F.C.</label></div>
                                        <div class="col-lg-8"><input class="form-control" name="rfc" id="rfc"  placeholder="RFC" <?= $rfc != NULL ? ' value="'.$rfc.'"' : '';?> required/></div>
                                    </div>
                                    <div class="row cont">
                                        <div class="col-lg-4"> <label>Dirección</label></div>
                                        <div class="col-lg-8"><input class="form-control" name="calle" id="calle"  placeholder="Calle" <?= $calle != NULL ? ' value="'.$calle.'"' : '';?> required/></div>
                                    </div>
                                    <div class="row cont">
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-4"><input class="form-control" name="no_ext" id="no_ext"  placeholder="No. Ext." <?= $no_ext != NULL ? ' value="'.$no_ext.'"' : '';?> required/></div>
                                        <div class="col-lg-4"><input class="form-control" name="no_int" id="no_int"  placeholder="No. Int." <?= $no_int != NULL ? ' value="'.$no_int.'"' : '';?> required/></div>
                                    </div>
                                    <div class="row cont">
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-8"><input class="form-control" name="colonia" id="colonia"  placeholder="Colonia" <?= $colonia != NULL ? ' value="'.$colonia.'"' : '';?> required/></div>
                                    </div>
                                    <div class="row cont">
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-4"><input class="form-control" name="cp" id="cp"  placeholder="C.P." <?= $cp != NULL ? ' value="'.$cp.'"' : '';?> required/></div>
                                        <div class="col-lg-4"><input class="form-control" name="delegacion" id="delegacion"  placeholder="Delegación" <?= $delegacion != NULL ? ' value="'.$delegacion.'"' : '';?> required/></div>
                                    </div>
                                    <div class="row cont">
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-8"><input class="form-control" name="ciudad" id="ciudad"  placeholder="Ciudad" <?= $ciudad != NULL ? ' value="'.$ciudad.'"' : '';?> required/></div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 center"></div>
                            <div class="col-lg-6 center" style="margin-top:3%;">
                                <div class="text-center" id="resultado_actualizacion"></div>
                                <button type="submit" id="actualizar_info"  name="actualizar_info" class="btn btn-green">Actualizar Información</button>
                            </div>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="subirArchivo" tabindex="-1" role="dialog" aria-labelledby="subirArchivo" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-refresh ic-color"></i> Actualizar Logotipo</h4>
                </div>
                <div class="modal-body">
                    <form method="POST" class="imagen_archivo" id="imagen_archivo" accept-charset="utf-8" enctype="multipart/form-data">
                        <div class="form-group center">
                            <div class="fileUpload btn btn-default btn-size">
                                <span>Selecciona Imagen</span>
                                <input type="file" name="userfile" id="userfile" class="upload" required="required" required/>
                            </div>
                            <div id="mensaje_resultado_imagen"></div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" id="refresh" data-dismiss="modal"><i class="fa fa-reply ic-color"></i> Regresar</button>
                            <input type="button" value="Continuar" class="btn btn-green" required="required" onclick="submitFile();"  />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>