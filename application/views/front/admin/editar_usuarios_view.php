<?php
$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
$fecha = $meses[date('n')-1]." ".date('d');
$año = date("Y");
$hora = date('h:i:s A');
?>
<h3 class="page-header center"><i class="fa fa-laptop"></i> Panel Administrador</h3>
<div id="page-wrapper">
    <div class="cont-menu-admin left">
        <a class="menu-admin center" href="<?= base_url("administrador/empresa") ?>">
            <i class="fa fa-building fa-2x"></i><br><span>Empresa</span>
        </a>
        <a class="menu-admin center" href="<?= base_url("administrador/usuarios") ?>">
            <i class="fa fa-users fa-2x"></i><br><span>Usuarios</span>
        </a>
        <a class="menu-admin center" href="<?= base_url("administrador/catalogos") ?>">
            <i class="fa fa-cubes fa-2x"></i><br><span>Catálogos</span>
        </a>
        <a class="menu-admin center" href="<?= base_url("administrador/autorizaciones") ?>">
            <i class="fa fa-check-square  fa-2x"></i><br><span>Autorizaciones</span>
        </a>
        <a class="menu-admin center" href="<?= base_url("administrador/config_estructuras") ?>">
            <i class="fa fa-bar-chart fa-2x"></i><br><span>Estructuras</span>
        </a>
        <a class="menu-admin center" href="<?= base_url("administrador/config_contabilidad") ?>">
            <i class="fa fa-dollar fa-2x"></i><br><span>Contabilidad</span>
        </a>
        <div class="menu-admin-calendar-ic center">
            <i class="fa fa-calendar  fa-2x"></i><br><span><?= $año ?></span>
        </div>
        <div class="menu-admin-calendar center">
            <span><b><?= $fecha ?></b></span><br><?= $hora ?>
        </div>
    </div>

    <div class="row add-pre">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <h3 class="page-header sub-page center"><i class="fa fa-edit"></i> Editar Usuario</h3>
                <div class="panel-body">
                <form class="forma_usuarios" role="form">
                    <div class="row">
                        <!--Primera Columna-->
                        <div class="col-lg-4">
                            <!---No. Usuario-->
                            <div class="row">
                                <div class="col-lg-6"><label>No. Usuario</label></div>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" name="no_usuario" id="no_usuario" placeholder="No. Usuario" <?= $id_datos_usuario != NULL ? ' value="'.$id_datos_usuario.'"' : '';?> required disabled/>
                                </div>
                            </div>
                            <!---Nombre Usuario-->
                            <div class="row">
                                <div class="col-lg-6"><label>Nombre Usuario</label></div>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" name="nombre_usuario" id="nombre_usuario" placeholder="Nombre Usuario" <?= $username != NULL ? ' value="'.$username.'"' : '';?> required />
                                </div>
                            </div>
                            <!---Estatus-->
                            <div class="row">
                                <div class="col-lg-6"><label>Estatus</label></div>
                                <div class="col-lg-6">
                                    <select class="form-control" id="activo" name="activo" required>
                                        <option value="1"<?= $activated == '1' ? ' selected="selected"' : '';?>>Activo</option>
                                        <option value="0"<?= $activated == '0' ? ' selected="selected"' : '';?>>Inactivo</option>
                                    </select>
                                </div>
                            </div>
                            <!---Estatus-->
                            <div class="row">
                                <div class="col-lg-6"></div>
                                <div class="col-lg-6">
                                    <select class="form-control" id="bloqueado" name="bloqueado" required>
                                        <option value="1"<?= $banned == '1' ? ' selected="selected"' : '';?>>Bloqueado</option>
                                        <option value="0"<?= $banned == '0' ? ' selected="selected"' : '';?>>Sin Bloquear</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!--Fin Primera Columna-->
                        <!--Segunda Columna-->
                        <div class="col-lg-4">
                            <!--Nombre-->
                            <div class="form-group forma_compromis_dato">
                                <label>Nombre</label>
                                <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Nombre" <?= $nombre != NULL ? ' value="'.$nombre.'"' : '';?> required />
                            </div>
                            <!--Apellido Paterno-->
                            <div class="form-group forma_compromis_dato">
                                <input type="text" class="form-control" name="apellido_paterno" id="apellido_paterno" placeholder="Apellido Paterno" <?= $apellido_paterno != NULL ? ' value="'.$apellido_paterno.'"' : '';?> required />
                            </div>
                            <!--Apellido Materno-->
                            <div class="form-group forma_compromis_dato">
                                <input type="text" class="form-control" name="apellido_materno" id="apellido_materno" placeholder="Apellido Materno" <?= $apellido_materno != NULL ? ' value="'.$apellido_materno.'"' : '';?> required />
                            </div>
                            <!--Puesto-->
                            <div class="form-group forma_compromis_dato">
                                <label>Puesto</label>
                                <input type="text" class="form-control" name="puesto" id="puesto" placeholder="Puesto" <?= $puesto != NULL ? ' value="'.$puesto.'"' : '';?> required />
                            </div>
                        </div>
                        <!--Fin Segunda Columna-->
                        <!--Tercer Columna-->
                        <div class="col-lg-4">
                            <!--Fecha Solicitud-->
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-6"><label class="label-f">Fecha Creación</label></div>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" name="fecha" id="fecha" placeholder="Fecha Creación" <?= $created != NULL ? ' value="'.$created.'"' : '';?> required disabled/>
                                    </div>
                                </div>
                            </div>
                            <!--Correo-->
                            <div class="form-group forma_compromis_dato">
                                <label>Correo Electrónico</label>
                                <input type="text" class="form-control" name="correo" id="correo" placeholder="Correo Electrónico" <?= $email != NULL ? ' value="'.$email.'"' : '';?> required/>
                            </div>
                        </div>
                        <!--Fin Tercer Columna-->
                    </div>
                </div>
                <div class="btns-finales text-center">
                    <div class="text-center" id="resultado_insertar_caratula"></div>
                    <a class="btn btn-default" href="<?= base_url("administrador/usuarios") ?>" ><i class="fa fa-reply ic-color"></i> Regresar</a>
                    <input type="submit" id="guardar_usuario" class="btn btn-green" name="guardar_usuario" value="Actualizar Datos"/>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>

</div>
<!-- /.row -->

</div>
<!-- /#page-wrapper -->