<?php
$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
$fecha = $meses[date('n')-1]." ".date('d');
$año = date("Y");
$hora = date('h:i:s A');
?>
<h3 class="page-header center"><i class="fa fa-laptop"></i> Panel Administrador</h3>
<div id="page-wrapper">
    <div class="cont-menu-admin left">
        <a class="menu-admin center" href="<?= base_url("administrador/empresa") ?>">
            <i class="fa fa-building fa-2x"></i><br><span>Empresa</span>
        </a>
        <a class="menu-admin center" href="<?= base_url("administrador/usuarios") ?>">
            <i class="fa fa-users fa-2x"></i><br><span>Usuarios</span>
        </a>
        <a class="menu-admin center" href="<?= base_url("administrador/catalogos") ?>">
            <i class="fa fa-cubes fa-2x"></i><br><span>Catálogos</span>
        </a>
        <a class="menu-admin center" href="<?= base_url("administrador/autorizaciones") ?>">
            <i class="fa fa-check-square  fa-2x"></i><br><span>Autorizaciones</span>
        </a>
        <a class="menu-admin center" href="<?= base_url("administrador/config_estructuras") ?>">
            <i class="fa fa-bar-chart fa-2x"></i><br><span>Estructuras</span>
        </a>
        <a class="menu-admin center" href="<?= base_url("administrador/config_contabilidad") ?>">
            <i class="fa fa-dollar fa-2x"></i><br><span>Contabilidad</span>
        </a>
        <div class="menu-admin-calendar-ic center">
            <i class="fa fa-calendar  fa-2x"></i><br><span><?= $año ?></span>
        </div>
        <div class="menu-admin-calendar center">
            <span><b><?= $fecha ?></b></span><br><?= $hora ?>
        </div>
    </div>
</div>