<?php
$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
$fecha = $meses[date('n')-1]." ".date('d');
$a�o = date("Y");
$hora = date('h:i:s A');
?>
<h3 class="page-header center"><i class="fa fa-laptop"></i> Panel Administrador</h3>
<div id="page-wrapper">
    <div class="cont-menu-admin left">
        <a class="menu-admin center" href="<?= base_url("administrador/empresa") ?>">
            <i class="fa fa-building fa-2x"></i><br><span>Empresa</span>
        </a>
        <a class="menu-admin center" href="<?= base_url("administrador/usuarios") ?>">
            <i class="fa fa-users fa-2x"></i><br><span>Usuarios</span>
        </a>
        <a class="menu-admin center" href="<?= base_url("administrador/catalogos") ?>">
            <i class="fa fa-cubes fa-2x"></i><br><span>Cat�logos</span>
        </a>
        <a class="menu-admin center" href="<?= base_url("administrador/autorizaciones") ?>">
            <i class="fa fa-check-square  fa-2x"></i><br><span>Autorizaciones</span>
        </a>
        <a class="menu-admin center" href="<?= base_url("administrador/config_estructuras") ?>">
            <i class="fa fa-bar-chart fa-2x"></i><br><span>Estructuras</span>
        </a>
        <a class="menu-admin center" href="<?= base_url("administrador/config_contabilidad") ?>">
            <i class="fa fa-dollar fa-2x"></i><br><span>Contabilidad</span>
        </a>
        <div class="menu-admin-calendar-ic center">
            <i class="fa fa-calendar  fa-2x"></i><br><span><?= $a�o ?></span>
        </div>
        <div class="menu-admin-calendar center">
            <span><b><?= $fecha ?></b></span><br><?= $hora ?>
        </div>
    </div>

        <div class="cont-admin">
            <div class="row menu-catalogos menu-reportes">
                <div class="col-xs-4 col-sm-4">
                    <a href="<?= base_url("administrador/firmas_contabilidad") ?>" class="ref">
                        <div class="catalogo-caja text-center">
                            <i class="fa fa-check-circle"></i>
                            <h5>Cat&aacute;logo de Personas <br> Elabor&aacute;n <br> (Contabilidad)</h5>
                        </div>
                    </a>
                </div>
                <div class="col-xs-4 col-sm-4">
                    <a href="<?= base_url("administrador/firmas_vobo_contabilidad") ?>" class="ref">
                        <div class="catalogo-caja text-center">
                            <i class="fa fa-check-circle"></i>
                            <h5>Cat&aacute;logo de Persona <br> Vo. Bo. <br> (Contabilidad)</h5>
                        </div>
                    </a>
                </div>
                <div class="col-xs-4 col-sm-4">
                    <a href="<?= base_url("administrador/firmas_autorizador_contabilidad") ?>" class="ref">
                        <div class="catalogo-caja text-center">
                            <i class="fa fa-check-circle"></i>
                            <h5>Cat&aacute;logo de Persona <br> Autoriza <br> (Contabilidad)</h5>
                        </div>
                    </a>
                </div>
            </div>
        </div>

