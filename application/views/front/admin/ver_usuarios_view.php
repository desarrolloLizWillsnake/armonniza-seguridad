<?php
$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
$fecha = $meses[date('n')-1]." ".date('d');
$año = date("Y");
$hora = date('h:i:s A');
?>
<h3 class="page-header center"><i class="fa fa-laptop"></i> Panel Administrador</h3>
<div id="page-wrapper">
    <div class="cont-menu-admin left">
        <a class="menu-admin center" href="<?= base_url("administrador/empresa") ?>">
            <i class="fa fa-building fa-2x"></i><br><span>Empresa</span>
        </a>
        <a class="menu-admin center" href="<?= base_url("administrador/usuarios") ?>">
            <i class="fa fa-users fa-2x"></i><br><span>Usuarios</span>
        </a>
        <a class="menu-admin center" href="<?= base_url("administrador/catalogos") ?>">
            <i class="fa fa-cubes fa-2x"></i><br><span>Catálogos</span>
        </a>
        <a class="menu-admin center" href="<?= base_url("administrador/autorizaciones") ?>">
            <i class="fa fa-check-square  fa-2x"></i><br><span>Autorizaciones</span>
        </a>
        <a class="menu-admin center" href="<?= base_url("administrador/config_estructuras") ?>">
            <i class="fa fa-bar-chart fa-2x"></i><br><span>Estructuras</span>
        </a>
        <a class="menu-admin center" href="<?= base_url("administrador/config_contabilidad") ?>">
            <i class="fa fa-dollar fa-2x"></i><br><span>Contabilidad</span>
        </a>
        <div class="menu-admin-calendar-ic center">
            <i class="fa fa-calendar  fa-2x"></i><br><span><?= $año ?></span>
        </div>
        <div class="menu-admin-calendar center">
            <span><b><?= $fecha ?></b></span><br><?= $hora ?>
        </div>
    </div>

    <div class="row add-pre">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <h3 class="page-header sub-page center"><i class="fa fa-eye"></i> Ver Usuario</h3>
                <div class="panel-body">
                    <div class="row">
                        <!--Primera Columna-->
                        <div class="col-lg-4">
                            <!---No. Usuario-->
                            <div class="row">
                                <div class="col-lg-6"><label>No. Usuario</label></div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <?php if(isset($id_datos_usuario)) { ?>
                                            <p class="form-control-static input_view"><?= $id_datos_usuario ?></p>
                                        <?php } else { ?>
                                            <p class="form-control-static input_view2"></p>
                                        <?php }  ?>
                                    </div>
                                </div>
                            </div>
                            <!---Nombre Usuario-->
                            <div class="row">
                                <div class="col-lg-6"><label>Nombre Usuario</label></div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <?php if(isset($username)) { ?>
                                            <p class="form-control-static input_view"><?= $username ?></p>
                                        <?php } else { ?>
                                            <p class="form-control-static input_view2"></p>
                                        <?php }  ?>
                                    </div>
                                </div>
                            </div>
                            <!---Estatus-->
                            <div class="row">
                                <div class="col-lg-6"><label>Estatus</label></div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <?php if(isset($activated)) { ?>
                                            <?php if($activated == 1) { ?>
                                                <p class="form-control-static input_view">Activo</p>
                                            <?php } else { ?>
                                                <p class="form-control-static input_view">Inactivo</p>
                                            <?php }  ?>
                                        <?php } else { ?>
                                            <p class="form-control-static input_view"></p>
                                        <?php }  ?>
                                    </div>
                                </div>
                            </div>
                            <!---Estatus-->
                            <div class="row">
                                <div class="col-lg-6"></div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <?php if(isset($banned)) { ?>
                                            <?php if($banned == 0) { ?>
                                                <p class="form-control-static input_view">Sin bloquear</p>
                                            <?php } else { ?>
                                                <p class="form-control-static input_view">Bloqueado</p>
                                            <?php }  ?>
                                        <?php } else { ?>
                                            <p class="form-control-static input_view"></p>
                                        <?php }  ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                            <!--Fin Primera Columna-->
                            <!--Segunda Columna-->
                            <div class="col-lg-4">
                                <!--Nombre-->
                                <div class="form-group forma_compromis_dato">
                                    <label>Nombre</label>
                                    <?php if(isset($nombre)) { ?>
                                        <p class="form-control-static input_view" ><?= $nombre ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view" ></p>
                                    <?php }  ?>
                                </div>
                                <!--Apellido Paterno-->
                                <div class="form-group forma_compromis_dato">
                                    <label>Apellido Paterno</label>
                                    <?php if(isset($apellido_paterno)) { ?>
                                        <p class="form-control-static input_view" ><?= $apellido_paterno ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view" ></p>
                                    <?php }  ?>
                                </div>
                                <!--Apellido Materno-->
                                <div class="form-group forma_compromis_dato">
                                    <label>Apellido Materno</label>
                                    <?php if(isset($apellido_materno)) { ?>
                                        <p class="form-control-static input_view" ><?= $apellido_materno ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view" ></p>
                                    <?php }  ?>
                                </div>
                                <!--Puesto-->
                                <div class="form-group forma_compromis_dato">
                                    <label>Puesto</label>
                                    <?php if(isset($puesto)) { ?>
                                        <p class="form-control-static input_view" ><?= $puesto ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view" ></p>
                                    <?php }  ?>
                                </div>
                            </div>
                            <!--Fin Segunda Columna-->
                            <!--Tercer Columna-->
                            <div class="col-lg-4">
                                <!--Fecha Solicitud-->
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-6"><label class="label-f">Fecha Solicitud</label></div>
                                        <div class="col-lg-6">
                                            <?php if(isset($created)) { ?>
                                                <p class="form-control-static input_view"><?= $created ?></p>
                                            <?php } else { ?>
                                                <p class="form-control-static input_view"></p>
                                            <?php }  ?>
                                        </div>
                                    </div>
                                </div>
                                <!--Correo-->
                                <div class="form-group forma_compromis_dato">
                                    <label>Correo Electrónico</label>
                                    <?php if(isset($email)) { ?>
                                        <p class="form-control-static input_view" ><?= $email ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view" ></p>
                                    <?php }  ?>
                                </div>
                            </div>
                            <!--Fin Tercer Columna-->
                        </div>
                        <div class="btns-finales text-center">
                            <a class="btn btn-default" href="<?= base_url("administrador/usuarios") ?>" ><i class="fa fa-reply ic-color"></i> Regresar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>

</div>
<!-- /.row -->

</div>
<!-- /#page-wrapper -->