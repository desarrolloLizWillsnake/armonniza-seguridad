<?php
$query_articulo = "SELECT * FROM articulos;";
$resultado_articulos = $this->db->query($query_articulo);
$articulos = $resultado_articulos->result();

$query_lugar = "SELECT * FROM lugar_de_entrega;";
$resultado_lugar = $this->db->query($query_lugar);
$lugar_de_entrega = $resultado_lugar->result();

?>
<h3 class="page-header center"><i class="fa fa-plus-circle"></i> Agregar Precompromiso</h3>
<div id="page-wrapper">
    <form class="forma_precompromiso" role="form">
        <div class="row add-pre error-gral">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        General
                    </div>
                    <div class="panel-body">
                        <input type="hidden" name="ultimo_pre" id="ultimo_pre" value="<?= $ultimo ?>">
                        <div class="row">
                            <!--Primer Columna-->
                            <div class="col-lg-4">
                                <!---No. Precompromiso-->
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-6"><label>No. Precompromiso</label></div>
                                        <div class="col-lg-6"><p class="form-control-static input_ver"><?= $ultimo ?></p></div>
                                    </div>
                                </div>

                                <select class="form-control" id="tipo_precompromiso" name="tipo_precompromiso">
                                    <option value="">Tipo de Precompromiso</option>
                                    <option value="Bienes">Bienes</option>
                                    <option value="Servicio">Servicio</option>
                                    <option value="Obra">Obra</option>
                                    <option value="Honorarios">Honorarios</option>
                                    <option value="Fondo Revolvente">Fondo Revolvente</option>
                                    <option value="Gastos a Comprobar">Gastos a Comprobar</option>
                                    <option value="Viáticos Nacionales">Viáticos Nacionales</option>
                                    <option value="Viáticos Internacionales">Viáticos Internacionales</option>
                                </select>

                                <!--Proveedor-->
                                <div class="form-group input-group forma_caratula_normal">
                                    <input type="hidden" name="id_proveedor" id="id_proveedor" value=""/>
                                    <input type="text" class="form-control" name="proveedor" id="proveedor" placeholder="Proveedor Sugerido" />
                                    <span class="input-group-btn ic-buscar-btn">
                                        <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_proveedores"><i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                </div>

                                <!--<label>Tipo de Garantía</label>-->
                                <select class="form-control forma_caratula_normal" id="tipo_garantia" name="tipo_garantia">
                                    <option value="">Tipo de Garantía</option>
                                    <option value="Fianza por Anticipo">Fianza por Anticipo</option>
                                    <option value="Fianza por Cumplimiento de Contrato">Fianza por Cumplimiento de Contrato</option>
                                    <option value="No aplica">No aplica</option>
                                </select>

                                <!--<label>Porcentaje de Garantía</label>-->
                                <select class="form-control forma_caratula_normal" id="porciento_garantia" name="porciento_garantia">
                                    <option value="">Porcentaje de Garantía</option>
                                    <option value="No aplica">No aplica</option>
                                    <option value="100">100%</option>
                                    <option value="10">10%</option>
                                </select>

                                <!--<label>Importe de Responsabilidad Civil</label>-->
                                <div class="form-group input-group forma_caratula_normal">
                                    <span class="input-group-btn ic-peso-btn">
                                        <button class="btn btn-default sg-dollar" ><i class="fa fa-dollar"></i></button>
                                    </span>
                                    <input style="margin-top: .5%;" type="text" pattern="[0-9]+([\.|,][0-9]+)?" class="form-control dinero" name="importe_civil" id="importe_civil" placeholder="Importe de Responsabilidad Civil">
                                </div>

                                <!--<label>Lugar de Adquisición de bienes y/o ejecución de servicios</label>-->
                                <textarea class="form-control forma_caratula_normal" id="lugar_adquisicion" name="lugar_adquisicion" style="width: 100%;" placeholder="Lugar de Adquisición de bienes y/o ejecución de servicios"></textarea>

                                <!--<label>Existencia en Almacén</label>-->
                                <textarea class="form-control forma_caratula_normal" id="almacen" name="almacen" style="width: 100%;" placeholder="Existencia en Almacén"></textarea>

                            </div>
                            <!--Fin Primer Columna-->
                            <!--Segunda Columna-->
                            <div class="col-lg-5" style="padding-left: 3%;">
                                <label class="forma_caratula_normal">Tipo de Gastos</label>
                                <div class="radio forma_caratula_normal">
                                    <label>
                                        <input type="radio" name="tipo_radio" id="optionsRadios1" value="1" checked>Gasto Corriente
                                    </label>
                                </div>
                                <div class="radio forma_caratula_normal">
                                    <label>
                                        <input type="radio" name="tipo_radio" id="optionsRadios2" value="2">Gasto de Capital
                                    </label>
                                </div>
                                <div class="radio forma_caratula_normal">
                                    <label>
                                        <input type="radio" name="tipo_radio" id="optionsRadios3" value="3">Amortización de la deuda y disminución de pasivos
                                    </label>
                                </div>
                                <div class="radio forma_caratula_normal">
                                    <label>
                                        <input type="radio" name="tipo_radio" id="optionsRadios4" value="4">Pensiones y Jubilaciones
                                    </label>
                                </div>
                                <div class="radio forma_caratula_normal">
                                    <label>
                                        <input type="radio" name="tipo_radio" id="optionsRadios5" value="5">Participaciones
                                    </label>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <!--<label>Ley aplicable</label>-->
                                        <select class="form-control forma_caratula_normal" id="ley" name="ley">
                                            <option value="">Ley Aplicable</option>
                                            <option value="LOPSRM">LOPSRM</option>
                                            <option value="LAASSP">LAASSP</option>
                                            <option value="N/A">N/A</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-6">
                                        <!--<label>Artículo</label>-->
                                        <select class="form-control forma_caratula_normal" id="articulo" name="articulo">
                                            <option value="">Artículo</option>
                                            <option value="N/A">N/A</option>
                                            <?php foreach($articulos as $fila) {?>
                                                <option class="<?= $fila->ley ?>" value="<?= $fila->articulo ?>"><?= $fila->articulo ?></option>
                                            <?php }?>
                                        </select>
                                    </div>
                                </div>

                                <!--<label>Plurianualidad</label>-->
                                <select class="form-control forma_caratula_normal" id="plurianualidad" name="plurianualidad" style="margin-top:1%;">
                                    <option value="">Plurianualidad</option>
                                    <option value="Si">Si aplica</option>
                                    <option value="No">No aplica</option>
                                </select>

                                <!--<label>Normas / Niveles de Inspección</label>-->
                                <textarea class="form-control forma_caratula_normal" id="normas" name="normas" style="width: 100%;" placeholder="Normas / Niveles de Inspección"></textarea>

                                <!--<label>Registros Sanitarios</label>-->
                                <textarea class="form-control forma_caratula_normal" id="registros" name="registros" style="width: 100%;" placeholder="Registros Sanitarios"></textarea>

                                <!--<label>Capacitación</label>-->
                                <textarea class="form-control forma_caratula_normal" id="capacitacion" name="capacitacion" style="width: 100%;" placeholder="Capacitación"></textarea>

                            </div>
                            <!--Fin Segunda Columna-->
                            <!--Tercera Columna-->
                            <div class="col-lg-3">
                                <!--<label>Fecha de Solicitud</label>-->
                                <input type="text" class="form-control ic-calendar" name="fecha_solicitud" id="fecha_solicitud" placeholder="Fecha de Solicitud" >

                                <!--<label>Fecha de Autorizacion</label>-->
                                <?php if($this->utilerias->get_firmas() != NULL || $this->utilerias->get_grupo() == 1) { ?>
                                <input type="text" class="form-control ic-calendar" name="fecha_autoriza" id="fecha_autoriza" placeholder="Fecha de Autorización">
                                <?php } else { ?>
                                <input type="hidden" name="fecha_autoriza" id="fecha_autoriza" value="0" />
                                <?php } ?>

                                <!--<label>Fecha de Entrega</label>-->
                                <input type="text" class="form-control ic-calendar forma_caratula_normal" name="fecha_entrega" id="fecha_entrega" placeholder="Fecha de Entrega" >

                                <!--<label>Lugar de Entrega</label>-->
                                <select class="form-control forma_caratula_normal" id="lugar_entrega" name="lugar_entrega">
                                    <option value="">Lugar de Entrega</option>
                                    <?php foreach($lugar_de_entrega as $lugar) {?>
                                        <option value="<?= $lugar->lugar_de_entrega ?>"><?= $lugar->lugar_de_entrega ?></option>
                                    <?php }?>
                                </select>

                                <label class="forma_caratula_normal" style="margin-top:6%;">Plazo</label>
                                <div class="row">
                                    <div class="col-lg-5 forma_caratula_normal">
                                        <select class="form-control" id="numero_plazo" name="numero_plazo">
                                            <option value="">0</option>
                                            <?php for($i = 1; $i <= 31; $i++) { ?>
                                                <option value="<?= $i ?>"><?= $i ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="col-lg-7 forma_caratula_normal">
                                        <select class="form-control" id="tipo_plazo" name="tipo_plazo">
                                            <option value="">Seleccione</option>
                                            <option value="Dia">Día</option>
                                            <option value="Dias">Días</option>
                                            <option value="Mes">Mes</option>
                                            <option value="Meses">Meses</option>
                                            <option value="Año">Año</option>
                                            <option value="Años">Años</option>
                                        </select>
                                    </div>
                                </div>

                                <label style="margin-top:6%;">Anexos</label>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="anexos" id="anexos1" value="1">Sí
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="anexos" id="anexo2" value="0" checked>No
                                    </label>
                                </div>

                            </div>
                            <!--Fin Tecera Columna-->
                        </div>

                        <hr/>

                        <!--Datos para viáticos, fondo revolvente y gastos a comprobar-->
                        <div class="row forma_fondo" style="display: none;">
                            <div class="col-lg-12">
                                <h5 class="center"><b>Datos del Comisionado o Beneficiario</b></h5>
                            </div>
                            <div class="col-lg-4">
                                <input type="hidden" name="id_persona" id="id_persona" value="0" />
                                <!-- Beneficiario -->
                                <div class="form-group input-group">
                                    <input type="text" class="form-control" name="nombre_persona" id="nombre_persona" placeholder="Nombre de la Persona" required/>
                                    <span class="input-group-btn ic-buscar-btn">
                                        <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_beneficiarios"><i class="fa fa-search"></i></button>
                                    </span>
                                </div>
                                <input type="text" class="form-control" name="no_empleado" id="no_empleado" placeholder="No. Empleado" disabled required/>
                            </div>
                            <div class="col-lg-4">
                                <input type="text" class="form-control" name="puesto" id="puesto" placeholder="Puesto o Categoría" disabled required />
                                <input type="text" class="form-control" name="area" id="area" placeholder="Área de Adscripción" disabled required />
                            </div>
                            <div class="col-lg-4">
                                <!-- Centro de Costos -->
                                <div class="form-group input-group">
                                    <input type="text" class="form-control" name="clave" id="clave" placeholder="Centro de Costos" required/>
                                    <span class="input-group-btn ic-buscar-btn">
                                        <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_centro_costo"><i class="fa fa-search"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <!--Datos para viáticos -->
                        <div class="row forma_viaticos" style="display: none;">
                            <div class="col-lg-4">
                                <select class="form-control" id="forma_pago" name="forma_pago" disabled required>
                                    <option value="">Forma de Pago</option>
                                    <option value="Cheque">Cheque</option>
                                    <option value="Transferencia">Transferencia</option>
                                </select>

                                <select class="form-control" id="transporte" name="transporte" disabled required>
                                    <option value="">Medio de Transporte</option>
                                    <option value="Autobús">Autobús</option>
                                    <option value="Avión">Avión</option>
                                    <option value="Auto Particular"> Auto Particular</option>
                                    <option value="Auto de la Empresa">Auto de la Empresa</option>
                                </select>

                                <select class="form-control" id="grupo_jerarquico" name="grupo_jerarquico" disabled required>
                                    <option value="">Grupos Jerárquicos</option>
                                    <option value="K hasta G">K hasta G</option>
                                    <option value="P hasta L">P hasta L</option>
                                    <option value="Personal Operativo"> Personal Operativo</option>
                                </select>
                            </div>
                            <div class="col-lg-4">
                            </div>
                            <div class="col-lg-4">
                                <label>Tipo de Zona</label>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="zona_marginada_check" id="zona_marginada_check" disabled >Zona Marginada</label>
                                </div>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="zona_mas_economica_check" id="zona_mas_economica_check" disabled >Zona más Económica</label>
                                </div>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="zona_menos_economica_check" id="zona_menos_economica_check" disabled >Zona menos Económica</label>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <!--Descripción y firmas precompromiso-->
                        <div class="row">
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="concepto_especifico" id="concepto_especifico" placeholder="Concepto Específico" maxlength="120" required />
                                <textarea class="form-control" id="descripcion_general" name="descripcion_general" placeholder="Justificación del Precompromiso" style="height: 7em;" required></textarea>
                            </div>
                            <div class="col-lg-3">
                                <!--Firmas-->
                                <div class="form-group">
                                    <input type="hidden" name="firma1" id="firma1" value="0" />
                                    <input type="hidden" name="firma2" id="firma2" value="0" />
                                    <input type="hidden" name="firma3" id="firma3" value="0" />
                                    <label style="margin-top:6%;"> Firmas de Autorización</label>
                                    <?php

                                    $grupo = $this->utilerias->get_grupo();
                                    $firmas = $this->utilerias->get_firmas_arreglo();

                                    if($firmas) {
                                        foreach ($firmas as $key) {
                                            if($key->firma == 1) { ?>
                                                <div class="checkbox">
                                                    <label><input type="checkbox" name="firma1_check" id="firma1_check" >Programación y Evaluación</label>
                                                </div>
                                            <?php }
                                            if($key->firma == 2) { ?>
                                                <div class="checkbox">
                                                    <label><input type="checkbox" name="firma2_check" id="firma2_check" >Recursos Materiales</label>
                                                </div>
                                            <?php }
                                            if($key->firma == 3) { ?>
                                                <div class="checkbox">
                                                    <label><input type="checkbox" name="firma3_check" id="firma3_check" >Gerencia Técnica Administrativa</label>
                                                </div>
                                            <?php }
                                        }
                                    } elseif($grupo == 1) { ?>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="firma1_check" id="firma1_check" >Programación y Evaluación</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="firma2_check" id="firma2_check" >Recursos Materiales</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="firma3_check" id="firma3_check" >Gerencia Técnica Administrativa</label>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Tabla Detalle-->
        <div class="row add-pre error-detalle forma_viaticos" style="display: none;">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Detalle Viáticos
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-4">
                                <input type="text" class="form-control" name="lugar_comision" id="lugar_comision" placeholder="Lugar de Comisión" disabled />
                                <!--Cuota Diaria-->
                                <div class="form-group input-group">
                                    <span class="input-group-btn ic-peso-btn">
                                        <button class="btn btn-default sg-dollar" ><i class="fa fa-dollar"></i></button>
                                    </span>
                                    <input style="margin-top: .5%;" type="text" pattern="[0-9]+([\.|,][0-9]+)?" class="form-control dinero" name="cuota_diaria" id="cuota_diaria" placeholder="Cuota Diaria" disabled />
                                </div>
                            </div>
                            <div  class="col-lg-4"></div>
                            <div class="col-lg-4">
                                <input type="text" class="form-control ic-calendar" name="fecha_inicio" id="fecha_inicio" placeholder="Fecha Inicio" disabled />
                                <input type="text" class="form-control ic-calendar" name="fecha_termina" id="fecha_termina" placeholder="Fecha Termino" disabled />
                                <input type="text" class="form-control dias" name="dias" id="dias" placeholder="Dias" value="1.0" disabled />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <button type="button" class="btn btn-green btn-block btn-gd" id="guardar_precompromiso_viaticos_detalle">Guardar Detalle Viáticos</button>
                                <div id="resultado_insertar_viaticos_detalle"></div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-body table-gral">
                                        <div class="table-responsive">
                                            <h4 id="suma_total_viaticos" class="text-center"></h4>
                                            <input type="hidden" value="" name="total_viaticos_hidden" id="total_viaticos_hidden" />

                                            <table class="table table-striped table-bordered table-hover" id="tabla_datos_viaticos_precompromiso">
                                                <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Lugar de Comisión</th>
                                                    <th>Fecha Inicio</th>
                                                    <th>Fecha Término</th>
                                                    <th>Días</th>
                                                    <th>Cuota Diaria</th>
                                                    <th>Importe Total</th>
                                                    <th>Acciones</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row add-pre error-detalle">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Detalles
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-2 niveles-pc">
                                <!--Estructura Administrativa de Egresos-->
                                <?= $niveles ?>
                                <a href="#modal_estructura" class="btn btn-default" data-toggle="modal" data-target="#modal_estructura">¿No conoces la  <br/>estructura?</a>
                            </div>
                            <div class="col-lg-6">
                                <div id="mes_grafica" class="text-center text-muted"></div>
                                <div id="grafica_partida"></div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group input-group">
                                    <input type="text" class="form-control" name="gasto" id="gasto" placeholder="Gasto"/>
                                    <span class="input-group-btn ic-buscar-btn">
                                        <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_gasto"><i class="fa fa-search"></i></button>
                                    </span>
                                </div>

                                <input id="titulo_gasto" class="form-control" name="titulo_gasto" placeholder="Título"/>

                                <input type="text" class="form-control" name="u_medida" id="u_medida" placeholder="Unidad de Medida" />

                                <input type="number" pattern="[0-9]+" class="form-control" name="cantidad" id="cantidad" placeholder="Cantidad">

                                <div class="form-group input-group">
                                    <span class="input-group-btn ic-peso-btn">
                                        <button class="btn btn-default sg-dollar" ><i class="fa fa-dollar"></i></button>
                                    </span>
                                    <input style="margin-top: .5%;" type="text" pattern="[0-9]+([\.|,][0-9]+)?" class="form-control dinero" name="precio" id="precio" placeholder="Precio Unitario">
                                </div>
                                <!--Iva Detalle-->
                                <label class="label-iva">IVA</label>
                                <div class="iva">
                                    <div class="radio">
                                        <label> <input type="radio" name="iva" id="iva1" value="1">Sí </label>
                                    </div>
                                    <div class="radio">
                                        <label> <input type="radio" name="iva" id="iva2" value="0" checked>No </label>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <textarea class="form-control" id="descripcion_detalle" name="descripcion_detalle" placeholder="Descripción Detalle" style="height: 7em;"></textarea>
<!--                                <div class="btn-toolbar toolbar-editor" data-role="editor-toolbar" data-target="#editor">-->
<!--                                    <div class="btn-group">-->
<!--                                        <a class="btn" data-edit="bold" title="Negrita (Ctrl/Cmd+B)"><i class="fa fa-bold"></i></a>-->
<!--                                        <a class="btn" data-edit="italic" title="Cursica (Ctrl/Cmd+I)"><i class="fa fa-italic"></i></a>-->
<!--                                        <a class="btn" data-edit="underline" title="Subrayado (Ctrl/Cmd+U)"><i class="fa fa-underline"></i></a>-->
<!--                                    </div>-->
<!--                                    <div class="btn-group">-->
<!--                                        <a class="btn" data-edit="insertunorderedlist" title="Lista Desordenada"><i class="fa fa-list-ul"></i></a>-->
<!--                                        <a class="btn" data-edit="insertorderedlist" title="Lista Ordenada"><i class="fa fa-list-ol"></i></a>-->
<!--                                        <a class="btn" data-edit="outdent" title="Quitar sangría (Shift+Tab)"><i class="fa fa-outdent"></i></a>-->
<!--                                        <a class="btn" data-edit="indent" title="Insertar sagnría (Tab)"><i class="fa fa-indent"></i></a>-->
<!--                                    </div>-->
<!--                                    <div class="btn-group">-->
<!--                                        <a class="btn" data-edit="justifyleft" title="Alinear Izquierda (Ctrl/Cmd+L)"><i class="fa fa-align-left"></i></a>-->
<!--                                        <a class="btn" data-edit="justifycenter" title="Alinear Centro (Ctrl/Cmd+E)"><i class="fa fa-align-center"></i></a>-->
<!--                                        <a class="btn" data-edit="justifyright" title="Alinear Derecha (Ctrl/Cmd+R)"><i class="fa fa-align-right"></i></a>-->
<!--                                        <a class="btn" data-edit="justifyfull" title="Justificado (Ctrl/Cmd+J)"><i class="fa fa-align-justify"></i></a>-->
<!--                                    </div>-->
<!---->
<!--                                    <div class="btn-group">-->
<!--                                        <a class="btn" data-edit="undo" title="Deshacer (Ctrl/Cmd+Z)"><i class="fa fa-undo"></i></a>-->
<!--                                        <a class="btn" data-edit="redo" title="Rehacer (Ctrl/Cmd+Y)"><i class="fa fa-repeat"></i></a>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="editor" id="descripcion_detalle">Descripción Detalle</div>-->
                            </div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-lg-12">
                                <button type="button" class="btn btn-green btn-block btn-gd" id="guardar_precompromiso_detalle">Guardar Partida</button>
                                <div id="resultado_insertar_detalle"></div>
                            </div>
                        </div>

                        <div class="row">
                             <div class="col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-body table-gral">
                                        <div class="table-responsive">
                                            <h4 id="suma_total" class="text-center"></h4>
                                            <input type="hidden" value="" name="subtotal_hidden" id="subtotal_hidden" />
                                            <input type="hidden" value="" name="total_hidden" id="total_hidden" />

                                            <table class="table table-striped table-bordered table-hover" id="tabla_datos_precompromiso">
                                                <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>F. F.</th>
                                                    <th>P. F.</th>
                                                    <th>C. C.</th>
                                                    <th>Capítulo</th>
                                                    <th>Concepto</th>
                                                    <th>Partida</th>
                                                    <th>Gasto</th>
                                                    <th>U/M</th>
                                                    <th>Cantidad</th>
                                                    <th>Precio U.</th>
                                                    <th>Subtotal</th>
                                                    <th>IVA</th>
                                                    <th>Total</th>
                                                    <th>Título</th>
                                                    <th>Descripción</th>
                                                    <th>Acciones</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <input type="hidden" value="" name="borrar_precompromiso_hidden" id="borrar_precompromiso_hidden" />
                                        </div>
                                    </div>
                                </div>
                             </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-body text-center">
                <div class="form-group c-firme">
                    <h3>¿Precompromiso en Firme?</h3>
                    <label class="checkbox-inline">
                        <input type="checkbox" id="check_firme" name="check_firme" disabled/>Sí
                    </label>
                </div>
            </div>
        </div>

        <div class="btns-finales text-center">
            <input type="submit" id="verificar_presupuesto" name="verificar_presupuesto" class="btn btn-default" value="Verificar Presupuesto" />
            <div class="text-center msj" id="resultado_verificado"></div>
        </div>

        <div class="btns-finales text-center">
            <div class="text-center" id="resultado_insertar_caratula"></div>
            <a href="<?= base_url("ciclo/precompromiso") ?>" class="btn btn-default"><i class="fa fa-reply ic-color"></i> Regresar</a>
            <input type="submit" id="guardar_precompromiso" name="guardar_precompromiso" class="btn btn-green" style="border:none;" value="Guardar Precompromiso" disabled/>
        </div>

    </form>
</div>


<!-- Modal estructura -->
<div class="modal fade" id="modal_estructura" tabindex="-1" role="dialog" aria-labelledby="modal_estructura" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-indent-left"></span>Estructura de Egresos</h4>
            </div>
            <div class="modal-body text-center">
                <?= $niveles_modal ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" disabled id="elegir_ultimoNivel">Elegir</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Proveedores -->
<div class="modal fade" id="modal_proveedores" tabindex="-1" role="dialog" aria-labelledby="modal_proveedores" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-users ic-modal"></i> Proveedores</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-3">
                <div class="table-responsive">
                    <input type="hidden" name="hidden_clave_proveedor" id="hidden_clave_proveedor" value="" />
                    <table class="table table-striped table-bordered table-hover" id="tabla_proveedores">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Clave</th>
                            <th>Nombre Comercial</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Gasto -->
<div class="modal fade" id="modal_gasto" tabindex="-1" role="dialog" aria-labelledby="modal_gasto" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-usd" style="color:#25B7BC;"></span>Gastos</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-gasto">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tabla_gastos">
                        <thead>
                        <tr>
                            <th width="13%">Clave</th>
                            <th width="15%">Clasificación</th>
                            <th width="45%">Descripción</th>
                            <th width="7%">U/M</th>
                            <th width="10%">Existencia</th>
                            <th  width="13%">Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Borrar Detalle -->
<div class="modal fade modal_borrar" tabindex="-1" role="dialog" aria-labelledby="modal_borrar" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-trash ic-modal"></i> Eliminar Partida</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="message-text" class="control-label">¿Realmente desea eliminar la partida seleccionada?</label>
                        <input type="hidden" value="" name="borrar_precompromiso_hidden" id="borrar_precompromiso_hidden" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_cancelar_precompromiso">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Borrar Detalle Viáticos -->
<div class="modal fade modal_borrar_detalle_viaticos" tabindex="-1" role="dialog" aria-labelledby="modal_borrar_detalle_viaticos" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-trash ic-modal"></i> Eliminar Viático</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="message-text" class="control-label">¿Realmente desea eliminar el viático seleccionado?</label>
                        <input type="hidden" value="" name="borrar_viatico_precompromiso_hidden" id="borrar_viatico_precompromiso_hidden" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_borrar_viatico">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Centros de Costo -->
<div class="modal fade" id="modal_centro_costo" tabindex="-1" role="dialog" aria-labelledby="modal_centro_costo" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-users ic-modal"></i> Centro de Costo</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-3">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tabla_centro_costos">
                        <thead>
                        <tr>
                            <th width="15%">Centro de Costo</th>
                            <th width="75%">Nombre</th>
                            <th width="10%">Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Beneficiarios -->
<div class="modal fade" id="modal_beneficiarios" tabindex="-1" role="dialog" aria-labelledby="modal_beneficiarios" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-users ic-modal"></i> Beneficiarios</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-5">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tabla_beneficiarios">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>No. Empleado</th>
                            <th>Nombre</th>
                            <th>Adscripción</th>
                            <th>Cargo</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Ver Detalle -->
<div class="modal fade modal_ver" tabindex="-1" role="dialog" aria-labelledby="modal_ver" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-eye ic-modal"></i> Detalle de Partida</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <p id="detalle_partida"></p>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-green" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Editar Detalle -->
<div class="modal fade modal_editar" tabindex="-1" role="dialog" aria-labelledby="modal_editar" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-edit ic-modal"></i> Editar Partida</h4>
            </div>
            <div class="modal-body modal_edit_detalle">
                <form role="form">
                    <div class="form-group" style="margin: 0 auto; width: 60%;">
                        <label for="message-text" class="control-label">Cantidad</label>
                        <input class="form-control" type="text" value="" name="editar_cantidad_precompromiso" id="editar_cantidad_precompromiso" />

                        <!--Iva Detalle-->

                        <label class="label-iva">IVA</label>
                        <div class="iva">
                            <div class="radio">
                                <label> <input type="radio" name="iva_editar" id="iva1" value="1" >Sí </label>
                            </div>
                            <div class="radio">
                                <label> <input type="radio" name="iva_editar" id="iva2" value="0" checked>No </label>
                            </div>
                        </div>

                        <label for="message-text" class="control-label">Precio Unitario</label>
                        <div class="form-group input-group">
                            <span class="input-group-btn ic-peso-btn">
                                <button class="btn btn-default sg-dollar" style="margin-top: 1%;" disabled><i class="fa fa-dollar"></i></button>
                            </span>
                            <input class="form-control dinero" type="text" pattern="[0-9]+([\.|,][0-9]+)?"  value="" name="editar_precio_precompromiso" id="editar_precio_precompromiso" required/>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Regresar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_editar_precompromiso">Aceptar</button>
            </div>
        </div>
    </div>
</div>

</div>
<!-- /#page-wrapper -->