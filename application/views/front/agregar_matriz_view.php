<h3 class="page-header title center"><i class="fa fa-area-chart"></i> Agregar Matriz</h3>
<div id="page-wrapper">
    <form class="forma_compromiso" role="form">
        <div class="row add-pre">
            <div class="col-lg-12">
                <div class="panel panel-default" style="margin-top: 2%;">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-7">
                                <!--Botones de Selección de Clave-->

                                <div class="row">
                                    <div class="col-lg-5 text-center" style="margin-bottom: 2%;">
                                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal_cog" style="margin-right: 2%;"><i class="fa fa-search"></i> COG</button>
                                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal_cri"><i class="fa fa-search"></i> CRI</button>
                                    </div>
                                    <div class="col-lg-7"></div>
                                </div>


                                <div class="row">
                                    <div class="col-lg-5"> <input type="text" class="form-control" name="clave" id="clave" placeholder="No. Clave" disabled></div>
                                    <div class="col-lg-7"> <input type="text" class="form-control" name="titulo_clave" id="titulo_clave" placeholder="Descripción Clave" disabled></div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-5">
                                        <div class="form-group input-group">
                                            <input type="hidden" name="" id="" value="">
                                            <input type="text" class="form-control" name="cuenta_cargo" id="cuenta_cargo" placeholder="No. Cuenta Cargo" disabled>
                                            <span class="input-group-btn ic-buscar-btn">
                                                 <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal_cuenta_cargo"><i class="fa fa-search"></i></button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-lg-7">
                                        <input type="text" class="form-control" name="descripcion_cuenta_cargo" id="descripcion_cuenta_cargo" placeholder="Descripción Cuenta Cargo" disabled>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-5">
                                        <div class="form-group input-group">
                                            <input type="hidden" name="" id="" value="">
                                            <input type="text" class="form-control" name="cuenta_abono" id="cuenta_abono" placeholder="No. Cuenta Abono" disabled>
                                            <span class="input-group-btn ic-buscar-btn">
                                                 <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal_cuenta_abono"><i class="fa fa-search"></i></button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-lg-7">
                                        <input type="text" class="form-control" name="descripcion_cuenta_abono" id="descripcion_cuenta_abono" placeholder="Descripción Cuenta Abono" disabled>
                                    </div>
                                </div>

                            </div>


                            <div class="col-lg-5" style="padding-top: 4.3%;">
                                <!--- Destino -->
                                <select class="form-control" id="destino" name="destino">
                                    <option value="">Destino</option>
                                    <option value="Devengado Gasto">Devengado Gastos</option>
                                    <option value="Pagado Gasto">Pagado Gasto</option>
                                    <option value="Ingreso Devengado">Ingreso Devengado</option>
                                    <option value="Ingreso Recaudado">Ingreso Recaudado</option>
                                </select>
                                <!-- Tipo de Gastos -->
                                <label>Tipo de Gasto</label>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="tipo_gasto" id="optionsRadios1" value="1" checked>Gasto Corriente
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="tipo_gasto" id="optionsRadios2" value="2">Gasto de Capital
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="tipo_gasto" id="optionsRadios3" value="3">Amortización de la cuenta y disminución de pasivos
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <textarea style="height: 7em;" class="form-control" id="caracteristicas" name="caracteristicas" placeholder="Características"></textarea>
                                <textarea style="height: 7em;" class="form-control" id="medio" name="medio" placeholder="Medio"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="btns-finales text-center">
            <a class="btn btn-default" href="<?= base_url("contabilidad/matriz") ?>" >Cancelar</a>
            <a id="guardar_matriz" class="btn btn-green">Guardar Matriz</a>
        </div>
    </form>
</div>

<!-- Modal COG -->
<div class="modal fade" id="modal_cog" tabindex="-1" role="dialog" aria-labelledby="modal_cog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-file ic-modal"></i> COG</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-pre">
                <input type="hidden" name="no_cog" id="no_cog" />
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tabla_cog">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Clave</th>
                            <th>Capítulo</th>
                            <th>Descripción</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal CRI-->
<div class="modal fade" id="modal_cri" tabindex="-1" role="dialog" aria-labelledby="modal_cri" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-file ic-modal"></i> CRI</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-pre">
                <input type="hidden" name="no_cog" id="no_cog" />
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tabla_cri">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Clave</th>
                            <th>Titulo</th>
                            <th>Descripción</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Cuenta Cargo-->
<div class="modal fade" id="modal_cuenta_cargo" tabindex="-1" role="dialog" aria-labelledby="modal_cuenta_cargo" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-file ic-modal"></i> Catálogo de Cuentas</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-pre">
                <input type="hidden" name="hidden_cuenta_cargo" id="hidden_cuenta_cargo" />
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tabla_cuenta_cargo">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>No. Cuenta</th>
                                <th>Descripción</th>
                                <th>Tipo</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Cuenta Abono-->
<div class="modal fade" id="modal_cuenta_abono" tabindex="-1" role="dialog" aria-labelledby="modal_cuenta_abono" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-file ic-modal"></i> Catálogo de Cuentas</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-pre">
                <input type="hidden" name="hidden_cuenta_abono" id="hidden_cuenta_abono" />
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tabla_cuenta_abono">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>No. Cuenta</th>
                            <th>Descripción</th>
                            <th>Tipo</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>