<h3 class="page-header title center" xmlns="http://www.w3.org/1999/html"><i class="fa fa-cubes fa-fw"></i> Catálogos / Clasificadores </h3>
<div id="page-wrapper">
    <div class="row menu-catalogos menu-reportes" style="padding-top: 6%;">
        <div class="col-xs-4 col-sm-4">
            <a href="<?= base_url("catalogos/proveedores") ?>" class="ref">
                <div class="catalogo-caja text-center">
                    <i class="fa fa-folder-open"></i>
                    <h5>Catálogo <br/> Proveedores</h5>
                </div>
            </a>
        </div>
        <div class="col-xs-4 col-sm-4">
            <a href="<?= base_url("catalogos/beneficiarios") ?>" class="ref">
                <div class="catalogo-caja text-center">
                    <i class="fa fa-folder-open"></i>
                    <h5>Catálogo <br/> Beneficiarios</h5>
                </div>
            </a>
        </div>
        <div class="col-xs-4 col-sm-4">
            <a href="<?= base_url("catalogos/cucop") ?>" class="ref">
                <div class="catalogo-caja text-center">
                    <i class="fa fa-folder-open"></i>
                    <h5>Clasificador de Bienes, <br/> Servicios y Obras [CUCoP]</h5>
                </div>
            </a>
        </div>
        <div class="col-xs-4 col-sm-4">
            <a href="<?= base_url("catalogos/medicamentos") ?>" class="ref">
                <div class="catalogo-caja text-center">
                    <i class="fa fa-folder-open"></i>
                    <h5>Catálogo de Medicamentos</h5>
                </div>
            </a>
        </div>
        <div class="col-xs-4 col-sm-4">
            <a href="<?= base_url("catalogos/bancos") ?>" class="ref">
                <div class="catalogo-caja text-center">
                    <i class="fa fa-folder-open"></i>
                    <h5>Catálogo <br/> Bancos</h5>
                </div>
            </a>
        </div>
        <div class="col-xs-4 col-sm-4">
            <a href="<?= base_url("catalogos/conceptos_bancarios") ?>" class="ref">
                <div class="catalogo-caja text-center">
                    <i class="fa fa-folder-open"></i>
                    <h5>Clave Concepto <br/> Bancario</h5>
                </div>
            </a>
        </div>
    </div>
</div>