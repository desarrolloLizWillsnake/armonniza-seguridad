<h3 class="page-header title center"><i class="fa fa-files-o fa-fw"></i> Reportes</h3>
<div id="page-wrapper">
    <!--Reportes | Categorías -->
    <div id="reportes-administracion">
        <div class="row">
            <div class="col-lg-12 text-left">
                <a class="btn btn-default btn-reportes" href="<?= base_url("reportes/reportesContabilidad") ?>">Contabilidad / CONAC</a>
                <a class="btn btn-default btn-reportes" href="<?= base_url("reportes/reportesPresupuestos") ?>">Presupuestos / CONAC</a>
                <a class="btn btn-default btn-reportes active" href="<?= base_url("reportes/reportesAdministracion") ?>">Administración</a>
            </div>
        </div>
        <div class="row menu-catalogos menu-reportes">
            <div class="col-xs-4 col-sm-4">
                <div class="catalogo-caja text-center">
                    <a href="<?= base_url("reportes/reporte_diot") ?>" data-tooltip="Consulta"><i class="fa fa-file-pdf-o"></i></a>
                    <a style="margin-left: 8%;" href="<?= base_url("reportes/reporte_diot_excel") ?>" data-tooltip="Exportar"><i class="fa fa-file-excel-o"></i></a>
                    <h5>Declaración Informativa de Operaciones con Terceros [DIOT]</h5>
                </div>
            </div>
            <div class="col-xs-4 col-sm-4">
                <div class="catalogo-caja text-center">
                    <a href="<?= base_url("reportes/consultaProveedores") ?>" data-tooltip="Consulta"><i class="fa fa-file-pdf-o"></i></a>
                    <a href="<?= base_url("reportes/consultaProveedoresExcel") ?>" data-tooltip="Exportar"><i class="fa fa-file-excel-o margin"></i></a>
                    <h5>Consulta de Proveedores</h5>
                </div>
            </div>
            <div class="col-xs-4 col-sm-4">
                <div class="catalogo-caja text-center">
                    <a href="<?= base_url("reportes/consultaViaticosNacionales") ?>" data-tooltip="Consulta"><i class="fa fa-file-pdf-o"></i></a>
                    <a href="<?= base_url("reportes/consultaViaticosNacionalesExcel") ?>" data-tooltip="Exportar"><i class="fa fa-file-excel-o margin"></i></a>
                    <h5>Consulta de Viáticos <br> Nacionales</h5>
                </div>
            </div>
            <div class="col-xs-4 col-sm-4">
                <div class="catalogo-caja text-center">
                    <a href="<?= base_url("reportes/consultaViaticosInternacionales") ?>" data-tooltip="Consulta"><i class="fa fa-file-pdf-o"></i></a>
                    <a href="<?= base_url("reportes/consultaViaticosInternacionalesExcel") ?>" data-tooltip="Exportar"><i class="fa fa-file-excel-o margin"></i></a>
                    <h5>Consulta de Viáticos Internacionales</h5>
                </div>
            </div>
            <div class="col-xs-4 col-sm-4">
                <div class="catalogo-caja text-center">
                    <a href="<?= base_url("reportes/consultaGastosComprobar") ?>" data-tooltip="Consulta"><i class="fa fa-file-pdf-o"></i></a>
                    <a href="<?= base_url("reportes/consultaGastosComprobarExcel") ?>" data-tooltip="Exportar"><i class="fa fa-file-excel-o margin"></i></a>
                    <h5>Consulta de Gastos a <br>  Comprobar</h5>
                </div>
            </div>
            <div class="col-xs-4 col-sm-4">
                <div class="catalogo-caja text-center">
                    <a href="<?= base_url("reportes/consultaFondoRevolvente") ?>" data-tooltip="Consulta"><i class="fa fa-file-pdf-o"></i></a>
                    <a href="<?= base_url("reportes/consultaFondoRevolventeExcel") ?>" data-tooltip="Exportar"><i class="fa fa-file-excel-o margin"></i></a>
                    <h5>Consulta de Fondos<br>  Revolvente</h5>
                </div>
            </div>
            <div class="col-xs-4 col-sm-4">
                <div class="catalogo-caja text-center">
                    <a href="<?= base_url("reportes/consultaSeguimientoProceso") ?>" data-tooltip="Exportar"><i class="fa fa-file-excel-o"></i></a>
                    <h5>Seguimiento de Proceso <br> Ciclo Administrativo</h5>
                </div>
            </div>
        </div>
    </div>
</div>

</div>
</div>

