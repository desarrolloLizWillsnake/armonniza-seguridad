<h3 class="page-header title center"><i class="fa fa-files-o fa-fw"></i> Reportes </h3>
<div id="page-wrapper">
    <div id="reportes-principal">
        <div class="row menu-catalogos menu-reportes-p" style="padding-top: 6%;">
            <div class="col-xs-4 col-sm-4">
                <a id="btn-conta" href="<?= base_url("reportes/reportesContabilidad") ?>" class="ref">
                    <div class="catalogo-caja text-center">
                        <i class="fa fa-folder-open"></i>
                        <h5>Contabilidad / CONAC</h5>
                    </div>
                </a>
            </div>
            <div class="col-xs-4 col-sm-4">
                <a id="btn-presu" href="<?= base_url("reportes/reportesPresupuestos") ?>" class="ref">
                    <div class="catalogo-caja text-center">
                        <i class="fa fa-folder-open"></i>
                        <h5>Presupuestos / CONAC</h5>
                    </div>
                </a>
            </div>
            <div class="col-xs-4 col-sm-4">
                <a  id="btn-admin" href="<?= base_url("reportes/reportesAdministracion") ?>" class="ref">
                    <div class="catalogo-caja text-center">
                        <i class="fa fa-folder-open"></i>
                        <h5>Administración</h5>
                    </div>
                </a>
            </div>
        </div>
    </div>
    </div>

</div>
</div>

