<h3 class="page-header center"><i class="fa fa-plus-circle"></i> Generar Movimiento Bancario</h3>
<div id="page-wrapper">
    <form class="forma_movimiento" role="form">
        <div class="row add-pre error-gral">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        General
                    </div>
                    <div class="panel-body">
                        <input type="hidden" name="movimiento" id="movimiento" value="<?= $ultimo ?>">
                        <input type="hidden" name="hidden_id_cuenta" id="hidden_id_cuenta" value="<?= $id_cuenta ?>">
                        <input type="hidden" name="hidden_cuenta" id="hidden_cuenta" value="<?= $cuenta ?>">
                        <input type="hidden" name="tipo_hidden" id="tipo_hidden" value="<?= $tipo ?>">
                        <div class="row">
                            <!--Primera Columna-->
                            <div class="col-lg-4">
                                <!---No. Movimiento Bancario-->
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-6"><label>No. Mov. Bancario</label></div>
                                        <div class="col-lg-6"><p class="form-control-static input_ver"><?= $ultimo ?></p></div>
                                    </div>
                                </div>
                                <!-- Timpo de Movimiento -->
                                <select class="form-control" id="tipo_movimiento" name="tipo_movimiento" required>
                                    <option value="">Tipo de Movimiento</option>
                                    <option value="Cheque">Cheque</option>
                                    <option value="En línea">En línea</option>
                                    <option value="SPEI">SPEI</option>
                                    <option value="Nómina">Nómina</option>
                                </select>

                                <div class="form-group input-group">
                                    <input type="hidden" name="hidden_clave_concepto" id="hidden_clave_concepto" value="" />
                                    <input type="text" class="form-control" name="concepto" id="concepto" placeholder="Concepto"ss disabled/>
                                    <span class="input-group-btn ic-buscar-btn">
                                        <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_bancos_concepto">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                </div>
                                <input type="text" class="form-control" name="cheque" id="cheque" placeholder="No. Cheque">
                                <input type="text" class="form-control" name="movimiento_bancario" id="movimiento_bancario" placeholder="No. Movimiento Bancario">

                                <div class="form-group input-group">
                                    <span class="input-group-btn ic-peso-btn">
                                        <button class="btn btn-default sg-dollar"><i class="fa fa-dollar"></i></button>
                                    </span>
                                    <input type="text" class="form-control ic-peso-input dinero" name="importe" id="importe"  pattern="[0-9]+([\.|,][0-9]+)?" placeholder="Importe">
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <div class="form-group input-group">
                                    <input type="hidden" name="id_proveedor" id="id_proveedor" />
                                    <input type="text" class="form-control ic-buscar-input" name="proveedor" id="proveedor" placeholder="Proveedor" />
                                    <span class="input-group-btn ic-buscar-btn">
                                        <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_proveedores">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                </div>
                                <textarea style="height:4em;" class="form-control" name="concepto_especifico" id="concepto_especifico" placeholder="Concepto Específico" maxlength="120" required/></textarea>

                                <textarea style="height: 8em;" class="form-control" id="descripcion_general" name="descripcion_general" placeholder="Descripción Movimiento"></textarea>
                            </div>
                            <div class="col-lg-3">
                                <input type="text" class="form-control ic-calendar" name="fecha_emision" id="fecha_emision" placeholder="Fecha de Emisión">

                                <input type="time" class="form-control" name="tiempo_movimiento" placeholder="hrs:mins" value="<?= date("h:m") ?>" />

                                <input type="text" class="form-control ic-calendar" name="fecha_pago" id="fecha_pago" placeholder="Fecha de Pago">
                            </div>
                            <!--Fin Primera Columna-->
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row add-pre error-gral">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body text-center">
                        <div class="form-group c-firme">
                            <h3>¿Movimiento en Firme?</h3>
                            <label class="checkbox-inline">
                                <input type="checkbox" id="check_firme" name="check_firme" />Sí
                            </label>
                        </div>
                    </div>
                </div>

                <div class="btns-finales text-center">
                    <div class="text-center" id="resultado_insertar_caratula"></div>
                    <a class="btn btn-default" onclick="window.history.back();"><i class="fa fa-reply ic-color"></i> Regresar</a>
                    <a id="guardar_movimiento" class="btn btn-green">Guardar Movimiento</a>
                </div>
            </div>
        </div>
    </form>
</div>

<!-- Modal Proveedores -->
<div class="modal fade" id="modal_proveedores" tabindex="-1" role="dialog" aria-labelledby="modal_proveedores" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-users ic-modal"></i> Proveedores</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-3">
                <div class="table-responsive">
                    <input type="hidden" name="hidden_clave_proveedor" id="hidden_clave_proveedor" value="" />
                    <table class="table table-striped table-bordered table-hover" id="tabla_proveedores">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Clave</th>
                            <th>Nombre Comercial</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Bancos Concepto -->
<div class="modal fade" id="modal_bancos_concepto" tabindex="-1" role="dialog" aria-labelledby="modal_bancos_concepto" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-book ic-modal"></i> Conceptos Bancarios</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-3">
                <input type="hidden" name="hidden_clave_proveedor" id="hidden_clave_proveedor" value="" />
                <table class="table table-striped table-bordered table-hover" id="tabla_bancos_concepto">
                    <thead>
                    <tr>
                        <th width="50%">Clave</th>
                        <th>Concepto</th>
                        <th width="100%">Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

</div>
<!-- /#page-wrapper -->