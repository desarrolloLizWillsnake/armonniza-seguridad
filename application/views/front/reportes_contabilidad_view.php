<h3 class="page-header title center"><i class="fa fa-files-o fa-fw"></i> Reportes</h3>
<div id="page-wrapper">
    <!--Reportes | Categorías -->
    <div id="reportes-contabilidad">
        <div class="row">
            <div class="col-lg-12 text-left">
                <a class="btn btn-default btn-reportes active" href="<?= base_url("reportes/reportesContabilidad") ?>">Contabilidad / CONAC</a>
                <a class="btn btn-default btn-reportes" href="<?= base_url("reportes/reportesPresupuestos") ?>">Presupuestos / CONAC</a>
                <a class="btn btn-default btn-reportes" href="<?= base_url("reportes/reportesAdministracion") ?>">Administración</a>
            </div>
        </div>
        <div class="row menu-catalogos menu-reportes">
            <div class="col-xs-4 col-sm-4">
                <div class="catalogo-caja text-center">
                <!--    <a href="<?= base_url("reportes/libroDiario") ?>" data-tooltip="Consulta"><i class="fa fa-file-pdf-o"></i></a>    -->
                    <a href="<?= base_url("reportes/libroDiarioExcel") ?>" data-tooltip="Exportar"><i class="fa fa-file-excel-o"></i></a>
                    <h5>Libro de Contabilidad <br/> Libro Diario</h5>
                </div>
            </div>
            <div class="col-xs-4 col-sm-4">
                <div class="catalogo-caja text-center">
                    <a href="<?= base_url("reportes/libroMayor") ?>" data-tooltip="Consulta"><i class="fa fa-file-pdf-o"></i></a>
                    <a style="margin-left: 8%;" href="<?= base_url("reportes/libroMayorExcel") ?>" data-tooltip="Exportar"><i class="fa fa-file-excel-o"></i></a>
                    <h5>Libro de Contabilidad <br/> Libro Mayor</h5>
                </div>
            </div>

            <! ----------- Formatos Personalizados EDUCAL  ----------- ->

            <div class="col-xs-4 col-sm-4">
                <div class="catalogo-caja text-center">
                    <a href="<?= base_url("reportes/estadoFinancieroEducal") ?>" data-tooltip="Consulta"><i class="fa fa-file-pdf-o"></i></a>
                    <a style="margin-left: 8%;" href="<?= base_url("reportes/estadoFinancieroEducalExcel") ?>" data-tooltip="Exportar"><i class="fa fa-file-excel-o"></i></a>
                    <h5>Estado de Situación <br/> Financiera</h5>
                </div>
            </div>
            <div class="col-xs-4 col-sm-4">
                <div class="catalogo-caja text-center">
                    <a href="<?= base_url("reportes/estadoResultados") ?>" data-tooltip="Consulta"><i class="fa fa-file-pdf-o"></i></a>
                    <a style="margin-left: 8%;" href="<?= base_url("reportes/estadoResultadosExcel") ?>" data-tooltip="Exportar"><i class="fa fa-file-excel-o"></i></a>
                    <h5>Estado de Resultados</h5>
                </div>
            </div>
            <div class="col-xs-4 col-sm-4">
                <div class="catalogo-caja text-center">
                    <a href="<?= base_url("reportes/estadoActividadesEducal") ?>" data-tooltip="Consulta"><i class="fa fa-file-pdf-o"></i></a>
                    <a style="margin-left: 8%;" href="<?= base_url("reportes/estadoActividadesEducalExcel") ?>" data-tooltip="Exportar"><i class="fa fa-file-excel-o"></i></a>
                    <h5>Estado de Actividades</h5>
                </div>
            </div>
            <div class="col-xs-4 col-sm-4">
                <div class="catalogo-caja text-center">
                    <a href="<?= base_url("reportes/estadoAnaliticoActivoEducal") ?>" data-tooltip="Consulta"><i class="fa fa-file-pdf-o"></i></a>
                    <a style="margin-left: 8%;" href="<?= base_url("reportes/estadoAnaliticoActivoEducalExcel") ?>" data-tooltip="Exportar"><i class="fa fa-file-excel-o"></i></a>
                    <h5>Estado Analítico del <br/> Activo</h5>
                </div>
            </div>
            <div class="col-xs-4 col-sm-4">
                <div class="catalogo-caja text-center">
                    <a href="<?= base_url("reportes/estadoAnaliticoPasivoEducal") ?>" data-tooltip="Consulta"><i class="fa fa-file-pdf-o"></i></a>
                    <a style="margin-left: 8%;" href="<?= base_url("reportes/estadoAnaliticoPasivoEducalExcel") ?>" data-tooltip="Exportar"><i class="fa fa-file-excel-o"></i></a>
                    <h5>Estado Analítico del <br/> Pasivo</h5>
                </div>
            </div>
            <div class="col-xs-4 col-sm-4">
                <div class="catalogo-caja text-center">
                    <a href="<?= base_url("reportes/estadoSituacionFinancieraEducal") ?>" data-tooltip="Consulta"><i class="fa fa-file-pdf-o"></i></a>
                    <a style="margin-left: 8%;" href="<?= base_url("reportes/estadoSituacionFinancieraEducalExcel") ?>" data-tooltip="Exportar"><i class="fa fa-file-excel-o"></i></a>
                    <h5>Estado de Cambios en la Situación Financiera</h5>
                </div>
            </div>
            <div class="col-xs-4 col-sm-4">
                <div class="catalogo-caja text-center">
                    <!--<a href="<?= base_url("reportes/estadoVariaciones") ?>" data-tooltip="Consulta"><i class="fa fa-file-pdf-o"></i></a>-->
                    <a href="<?= base_url("reportes/estadoVariacionesExcel") ?>" data-tooltip="Exportar"><i class="fa fa-file-excel-o"></i></a>
                    <h5>Estado de Variación en la Hacienda Pública</h5>
                </div>
            </div>
            <div class="col-xs-4 col-sm-4">
                <div class="catalogo-caja text-center">
                    <!--<a href="<?= base_url("reportes/estadoFlujoEfectivo") ?>" data-tooltip="Consulta"><i class="fa fa-file-pdf-o"></i></a>-->
                    <a href="<?= base_url("reportes/estadoFlujoEfectivoExcel") ?>" data-tooltip="Exportar"><i class="fa fa-file-excel-o"></i></a>
                    <h5>Estado de Flujos de <br/> Efectivo</h5>
                </div>
            </div>
            <! ----------- Formatos CONAC  ----------- ->
            <!--
            <div class="col-xs-4 col-sm-4">
                <div class="catalogo-caja text-center">
                    <a href="<?= base_url("reportes/estadoFinanciero") ?>" data-tooltip="Consulta"><i class="fa fa-file-pdf-o"></i></a>
                    <a style="margin-left: 8%;" href="<?= base_url("reportes/estadoFinancieroExcel") ?>" data-tooltip="Exportar"><i class="fa fa-file-excel-o"></i></a>
                    <h5>Estado de Situación <br/> Financiera</h5>
                </div>
            </div>
            <div class="col-xs-4 col-sm-4">
                <div class="catalogo-caja text-center">
                    <a href="<?= base_url("reportes/estadoActividades") ?>" data-tooltip="Consulta"><i class="fa fa-file-pdf-o"></i></a>
                    <h5>Estado de Actividades</h5>
                </div>
            </div>
            <div class="col-xs-4 col-sm-4">
                <div class="catalogo-caja text-center">
                    <a href="<?= base_url("reportes/estadoAnaliticoActivo") ?>" data-tooltip="Consulta"><i class="fa fa-file-pdf-o"></i></a>
                    <h5>Estado Analítico del <br/> Activo</h5>
                </div>
            </div>
             <div class="col-xs-4 col-sm-4">
                <div class="catalogo-caja text-center">
                    <a href="<?= base_url("reportes/estadoAnaliticoDeuda") ?>" data-tooltip="Consulta"><i class="fa fa-file-pdf-o"></i></a>
                    <h5>Estado Analítico de la Deuda y Otros Pasivos</h5>
                </div>
            </div>

            <div class="col-xs-4 col-sm-4">
                <div class="catalogo-caja text-center">
                    <a href="<?= base_url("reportes/estadoSituacionFinanciera") ?>" data-tooltip="Consulta"><i class="fa fa-file-pdf-o"></i></a>
                    <h5>Estado de Cambios en la Situación Financiera</h5>
                </div>
            </div>
            -->
        </div>
    </div>
</div>

</div>
</div>
