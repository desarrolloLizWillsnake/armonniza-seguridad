<h3 class="page-header center"><i class="fa fa-file-text-o"></i> Compromisos</h3>
<div id="page-wrapper">
    <!--Opción 2-->
    <!--<h3 class="page-header center"><i class="fa fa-file-o"></i> compromisos</h3>-->
    <div class="row cont-btns-c center">
        <div class="col-lg-12">
            <?php if($this->utilerias->get_permisos("agregar_compromiso") || $this->utilerias->get_grupo() == 1){ ?>
                <a href="<?= base_url("ciclo/agregar_compromiso") ?>" class="btn btn-default"><i class="fa fa-plus circle ic-color"></i> Agregar Compromiso</a>
            <?php } ?>
            <a href="<?= base_url("plantillas/plantilla_compromiso.xls") ?>" class="btn btn-default"><i class="fa fa-download ic-color"></i> Descargar Plantilla</a>
            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#subirArchivo" data-whatever="Subir"><i class="fa fa-upload ic-color"></i> Subir Archivo</button>

        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <ul class="nav nav-tabs">
                <li id= "btn-todas" class="active"><a onclick="BtnTodas()">Todos</a></li>
                <li id= "btn-aprobado" role="presentation"><a onclick="BtnAprobado()">Aprobado</a></li>
                <li id= "btn-pendiente" role="presentation"><a onclick="BtnPendiente()">Pendiente</a></li>
                <li id= "btn-autorizar" role="presentation"><a onclick="BtnAutorizar()">Por Autorizar</a></li>
                <li id= "btn-cancelado" role="presentation"><a onclick="BtnCancelado()">Cancelado</a></li>
            </ul>
            <div class="panel panel-default" id="tab-todas">
                <div class="panel-body table-gral">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover datos_tabla ajuste">
                            <thead>
                            <tr>
                                <th width="5.5%">No.</th>
                                <th width="12%">Precompromiso</th>
                                <th width="11%">F. Autorizado</th>
                                <th width="16%">Proveedor</th>
                                <th width="12%">Importe</th>
                                <th width="13%">Creado Por</th>
                                <th width="6.5%">Firme</th>
                                <th width="7%">Estatus</th>
                                <th width="12%">Acciones</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="panel panel-default" id="tab-aprobado" style="display: none;">
                <div class="panel-body table-gral">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover datos_tabla_aprobado ajuste" >
                            <thead>
                            <tr>
                                <th width="5.5%">No.</th>
                                <th width="12%">Precompromiso</th>
                                <th width="11%">F. Autorizado</th>
                                <th width="16%">Proveedor</th>
                                <th width="12%">Importe</th>
                                <th width="13%">Creado Por</th>
                                <th width="6.5%">Firme</th>
                                <th width="7%">Estatus</th>
                                <th width="12%">Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="panel panel-default" id="tab-pendiente" style="display: none;">
                <div class="panel-body table-gral">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover datos_tabla_pendiente ajuste">
                            <thead>
                            <tr>
                                <th width="5.5%">No.</th>
                                <th width="12%">Precompromiso</th>
                                <th width="11%">F. Autorizado</th>
                                <th width="16%">Proveedor</th>
                                <th width="12%">Importe</th>
                                <th width="13%">Creado Por</th>
                                <th width="6.5%">Firme</th>
                                <th width="7%">Estatus</th>
                                <th width="12%">Acciones</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="panel panel-default" id="tab-autorizar" style="display: none;">
                <div class="panel-body table-gral">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover datos_tabla_autorizar ajuste">
                            <thead>
                            <tr>
                                <th width="5.5%">No.</th>
                                <th width="12%">Precompromiso</th>
                                <th width="11%">F. Autorizado</th>
                                <th width="16%">Proveedor</th>
                                <th width="12%">Importe</th>
                                <th width="13%">Creado Por</th>
                                <th width="6.5%">Firme</th>
                                <th width="7%">Estatus</th>
                                <th width="12%">Acciones</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="panel panel-default" id="tab-cancelado" style="display: none;">
                <div class="panel-body table-gral">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover datos_tabla_cancelado ajuste">
                            <thead>
                            <tr>
                                <th width="5.5%">No.</th>
                                <th width="12%">Precompromiso</th>
                                <th width="11%">F. Autorizado</th>
                                <th width="16%">Proveedor</th>
                                <th width="12%">Importe</th>
                                <th width="13%">Creado Por</th>
                                <th width="6.5%">Firme</th>
                                <th width="7%">Estatus</th>
                                <th width="12%">Acciones</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="modal fade modal_borrar" tabindex="-1" role="dialog" aria-labelledby="modal_borrar" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-remove ic-modal"></i> Cancelar Compromiso</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="message-text" class="control-label">¿Realmente desea cancelar el compromiso seleccionado?</label>
                        <input type="hidden" value="" name="cancelar_compromiso" id="cancelar_compromiso" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-reply ic-color"></i> Regresar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_cancelar_compromiso">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_resultado_borrar" tabindex="-1" role="dialog" aria-labelledby="modal_resultado_borrar" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-remove ic-modal"></i> Cancelar Compromiso</h4>
            </div>
            <div class="modal-body">
                <div id="resultado_borrar"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-green" data-dismiss="modal" >Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="subirArchivo" tabindex="-1" role="dialog" aria-labelledby="subirArchivo" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><span class="glyphicon glyphicon-indent-left" style="color:#25B7BC;"></span> Subir Archivo(s) de Compromiso</h4>
            </div>
            <div class="modal-body">
                <?php
                $attributes = array(
                    'role' => 'form',
                );

                echo(form_open_multipart('ciclo/subir_compromiso', $attributes));
                ?>
                <div class="form-group center">
                    <div class="fileUpload btn btn-default btn-size">
                        <span>Selecciona Archivo(s)</span>
                        <?php
                        $data = array(
                            'name'        => 'archivoSubir[]',
                            'id'          => 'archivoSubir',
                            'class'       => 'upload',
                            'accept' => 'application/vnd.ms-excel',
                            'required' => 'required',
                            'multiple' => 'multiple',
                        );

                        echo(form_upload($data));
                        ?>
                    </div>
                    <p class="help-block">El archivo(s) a subir debe(n) de tener la extensión .XLS</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-reply ic-color"></i> Regresar</button>
                <?php
                $atributos_submit = array(
                    'class' => 'btn btn-green',
                    'required' => 'required',
                );

                echo(form_submit($atributos_submit, 'Subir Archivo(s)'));
                ?>
                <?php echo(form_close()); ?>
            </div>
        </div>
    </div>
</div>

</div>
<!-- /.row -->
</div>
<!-- /#page-wrapper -->