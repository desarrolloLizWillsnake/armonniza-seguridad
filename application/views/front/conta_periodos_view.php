<h3 class="page-header title center"><i class="fa fa-calendar-check-o"></i> Períodos Contables</h3>
<div id="page-wrapper">
    <div class="periodos_contables">
    <div class="row cont-btns-c center">
        <div class="col-lg-12">
            <?php if($this->utilerias->get_permisos("agregar_periodo") || $this->utilerias->get_grupo() == 1){ ?>
                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#agregarPeriodo"><i class="fa fa-plus-circle circle ic-color"></i> Agregar Período</button>
            <?php } ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body table-gral">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover datos_tabla table-prec">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th width="6%">Año</th>
                                <th width="11%">Fecha Inicio</th>
                                <th width="11%">Fecha Cierre</th>
                                <th width="13%">Saldo Anterior</th>
<!--                                <th width="13%">Debe</th>-->
<!--                                <th width="13%">Haber</th>-->
                                <th width="13%">Saldo Actual</th>
                                <th width="8%">Estatus</th>
                                <th width="9%">Acciones</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Registro Cierre Anual -->
    <div>
            <div class="panel panel-default">
                <div class="panel-body table-gral">
                    <div class="row center">
                        <div class="col-lg-12">
                            <button type="button" class="btn btn-green btn-conta-cierre" id="cerrarPeriodoAnualBoton" data-toggle="modal" data-target="#cerrarperiodoanual"><i class="fa fa-calendar-check-o"></i> Registro Cierre Anual</button>
                        </div>
                        <div class="col-lg-12" id="botones_exportar" style="display:none;">
<!--                        <div class="col-lg-12" id="#cerrar_periodo_anual" style="display: none;">-->
                            <a href="<?= base_url("contabilidad/exportar_cierre_anual_detalle") ?>" class="btn btn-default btn-conta-cierre"><i class="fa fa-download ic-color"></i> Póliza Cierre del Ejercicio</a>
                            <br>
                            <a href="<?= base_url("contabilidad/exportar_cierre_anual_totales") ?>" class="btn btn-default btn-conta-cierre"><i class="fa fa-download ic-color"></i> Póliza Registro de Pérdida o Ganancia</a>
                            <br>
                            <a href="<?= base_url("contabilidad/exportar_balanza_cierre") ?>" class="btn btn-default btn-conta-cierre"><i class="fa fa-download ic-color"></i> Exportar Balanza Final</a>
                            <br>
                            <p class="btn btn-purple" id="resultado-cierre"><i class="fa fa-check-circle fa-2x ic-msj" style="padding: 0% 2% 0% 0%;"></i> Cierre Contable Anual generado con éxito.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="agregarPeriodo" tabindex="-1" role="dialog" aria-labelledby="agregarPeriodo" aria-hidden="true" style="z-index: 1; padding-top: 5%;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-plus-circle circle ic-color"></i> Agregar Período Contable</h4>
                </div>

                <div class="modal-body">
                    <form role="form" id="forma_periodo">
                        <div class="row c_periodo">
                            <div class="col-lg-5">
                                <label for="year" class="control-label">Año</label>
                            </div>
                            <div class="col-lg-7">
                                <div class="form-group">
                                    <input type="text" class="form-control" value="2015" name="year" id="year" required/>
                                </div>
                            </div>
                        </div>
                        <div class="row c_periodo">
                            <div class="col-lg-5">
                                <label for="fecha_inicio" class="control-label">Fecha Inicio</label>
                            </div>
                            <div class="col-lg-7">
                                <div class="form-group">
                                    <input type="text" class="form-control ic-calendar" name="fecha_inicio" id="fecha_inicio" placeholder="aaaa-mm-dd" required="required"/>
                                </div>
                            </div>
                        </div>
                        <div class="row c_periodo">
                            <div class="col-lg-5">
                                <label for="fecha_termina" class="control-label">Fecha Cierre </label>
                            </div>
                            <div class="col-lg-7">
                                <div class="form-group">
                                    <input type="text" class="form-control ic-calendar" name="fecha_termina" id="fecha_termina" placeholder="aaaa-mm-dd" required="required"/>
                                </div>
                            </div>
                            <div class="col-lg-12 center">
                                <div id="imagen_espera" style="width: 70%; margin: 0% 25%; padding-top: 5%;"></div>
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-reply ic-color"></i> Regresar</button>
                    <input type="submit" class="btn btn-green" id="agregar_periodo" value="Aceptar"/>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="cerrarperiodoanual" tabindex="-1" role="dialog" aria-labelledby="cerrarperiodoanual" aria-hidden="true" style="z-index: 1; margin-top: 5%;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-calendar-check-o ic-modal"></i> Registro Cierre Anual</h4>
                </div>
                <div class="modal-body center">
                    <h5>Cierre del Ejercicio</h5>
                    <div id="logo_espera" style="width:50%; margin: 1% 25%;">
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-green" id="cerrar_periodo_anual" value="Cerrar"/>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="resultado_mensaje" tabindex="-1" role="dialog" aria-labelledby="resultado_mensaje" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content close_off">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-calendar-check-o ic-modal"></i> Resultado</h4>
                </div>
                <div class="modal-body center">
                    <p id="resultado_periodo"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-green" data-dismiss="modal">Aceptar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Editar Detalle -->
    <div class="modal fade modal_editar" tabindex="-1" role="dialog" aria-labelledby="modal_editar" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-edit ic-modal"></i> Editar Período Contable</h4>
                </div>
                <div class="modal-body modal_edit_detalle">
                    <form role="form">
                        <div class="form-group center">
                            <input type="hidden" name="id_periodo_editar" id="id_periodo_editar" value="" />
                            <span>¿Desea modificar el estatus actual de período?</span>
                            <div class="iva">
                                <div class="radio">
                                    <label> <input type="radio" name="periodo" id="periodo1" value="1">Cerrar </label>
                                </div>
                                <div class="radio">
                                    <label> <input type="radio" name="periodo" id="periodo2" value="0">Abrir </label>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-reply ic-color"></i> Regresar</button>
                    <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_editar_periodo">Aceptar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Cuentas Afectadas -->
    <div class="modal fade" id="modal_cuentas_afectadas" tabindex="-1" role="dialog" aria-labelledby="modal_cuentas_afectadas" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel"><i class="fa fa-users ic-modal"></i> Cuentas Afectadas</h4>
                </div>

                <div class="modal-body table-gral modal-action modal-3 modal-body-cuentas">
                    <div class="table-responsive">
                        <div class="center">
                            <div id="imagen_espera_cuentas" style="width: 100%;"></div>
                        </div>
                        <table class="table table-striped table-bordered table-hover table-prec display" id="tabla_cuentas_afectadas">
                            <thead>
                            <tr>
                                <th>Fecha Inicial</th>
                                <th>Fecha Final</th>
                                <th>Cuenta</th>
                                <th>Descripción de Cuenta</th>
                                <th>Saldo Inicial</th>
                                <th>Saldo Final</th>
                            </tr>
                            </thead>
                            <tbody id="cuerpo_tabla_cuentas_afectadas">
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-reply ic-color"></i> Regresar</button>
                </div>
            </div>
        </div>
    </div>

    </div>

</div>