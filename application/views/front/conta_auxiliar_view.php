<h3 class="page-header title center"><i class="fa fa-area-chart"></i> Auxiliar</h3>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-6">
            <form action="<?= base_url("contabilidad/imprimir_reporte_auxiliar") ?>" method="POST" id="datos_auxiliar" class="datos_auxiliar" id="imprimir_auxiliar" role="form">
                <div class="row add-pre error-gral text-center">
                    <div class="col-lg-12">
                        <div class="panel panel-default" style="margin: 0 auto; margin-top: 4%;">
                            <h4 style="margin-top: 6%;"><i class="fa fa-print ic-color"></i> Imprimir Auxiliar</h4>
                            <div class="panel-body">
                                <!--Período Tiempo -->
                                <div class="row" style="margin-top: 2%;">
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control ic-calendar" name="fecha_inicial" id="fecha_inicial" placeholder="Fecha Inicial" >
                                    </div>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control ic-calendar" name="fecha_final" id="fecha_final" placeholder="Fecha Final" >
                                    </div>
                                </div>
                                <!--Rango Cuentas -->
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group input-group marg_sup2">
                                            <input type="text" class="form-control marg_sup1" name="cuenta_inicial" id="cuenta_inicial" placeholder="No. Cuenta Inicial" required/>
                                            <span class="input-group-btn ic-buscar-btn">
                                                <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_cuenta_inicial"><i class="fa fa-search"></i></button>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control marg_sup2" name="nombre_cuenta" id="nombre_cuenta" placeholder="Nombre Cuenta" required/>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group input-group marg_sup2">
                                            <input type="text" class="form-control marg_sup1" name="cuenta_final" id="cuenta_final" placeholder="No. Cuenta Final" required/>
                                            <span class="input-group-btn ic-buscar-btn">
                                                <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_cuenta_final"><i class="fa fa-search"></i></button>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control marg_sup2" name="nombre_cuenta_final" id="nombre_cuenta_final" placeholder="Nombre Cuenta" required/>
                                    </div>
                                </div>

                                <div class="btns-finales text-center">
                                    <input class="btn btn-green" type="submit" id="consultar_reporte" value="Aceptar"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-lg-6">
            <form action="<?= base_url("contabilidad/exportar_reporte_auxiliar") ?>" method="POST" id="datos_auxiliar" class="datos_auxiliar" id="exportar_auxiliar" role="form">
                <div class="row add-pre error-gral text-center">
                    <div class="col-lg-12">
                        <div class="panel panel-default" style="margin: 0 auto; margin-top: 4%;">
                            <h4 style="margin-top: 6%;"><i class="fa fa-download ic-color"></i> Exportar Auxiliar</h4>
                            <div class="panel-body">
                                <!--Período Tiempo -->
                                <div class="row" style="margin-top: 2%;">
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control ic-calendar" name="fecha_inicial" id="fecha_inicial_exportar" placeholder="Fecha Inicial" >
                                    </div>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control ic-calendar" name="fecha_final" id="fecha_final_exportar" placeholder="Fecha Final" >
                                    </div>
                                </div>
                                <!--Rango Cuentas -->
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group input-group marg_sup2">
                                            <input type="text" class="form-control marg_sup1" name="cuenta_inicial_exportar" id="cuenta_inicial_exportar" placeholder="No. Cuenta Inicial" required/>
                                            <span class="input-group-btn ic-buscar-btn">
                                                <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_cuenta_inicial"><i class="fa fa-search"></i></button>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control marg_sup2" name="nombre_cuenta_exportar" id="nombre_cuenta_exportar" placeholder="Nombre Cuenta" required/>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group input-group marg_sup2">
                                            <input type="text" class="form-control marg_sup1" name="cuenta_final_exportar" id="cuenta_final_exportar" placeholder="No. Cuenta Final" required/>
                                            <span class="input-group-btn ic-buscar-btn">
                                                <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_cuenta_final"><i class="fa fa-search"></i></button>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control marg_sup2" name="nombre_cuenta_final_exportar" id="nombre_cuenta_final_exportar" placeholder="Nombre Cuenta" required/>
                                    </div>
                                </div>

                                <div class="btns-finales text-center">
                                    <input class="btn btn-green" type="submit" id="consultar_reporte" value="Aceptar"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Plan Cuentas Contables-->
<div class="modal fade" id="modal_cuenta_inicial" tabindex="-1" role="dialog" aria-labelledby="modal_cuenta_inicial" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-book" style="color: #25B7BC;"></span> Plan de Cuentas Contables</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-4">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tabla_cuenta_inicial">
                        <thead>
                        <tr>
                            <th>Cuenta</th>
                            <th>Nombre</th>
                            <th>Tipo</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_cuenta_final" tabindex="-1" role="dialog" aria-labelledby="modal_cuenta_final" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-book" style="color: #25B7BC;"></span> Plan de Cuentas Contables</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-4">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tabla_cuenta_final">
                        <thead>
                        <tr>
                            <th>Cuenta</th>
                            <th>Nombre</th>
                            <th>Tipo</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

