<h3 class="page-header center"><i class="fa fa-plus-circle"></i> Agregar Contra Recibo</h3>
<div id="page-wrapper">
    <form class="forma_contrarecibo" role="form">
        <div class="row add-pre error-gral">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <!--Apartado General-->
                    <div class="panel-heading">General</div>
                    <div class="panel-body">
                        <input type="hidden" name="ultimo_contrarecibo" id="ultimo_contrarecibo" value="<?= $ultimo ?>">
                        <div class="row">
                            <div class="col-lg-4">
                                <!--No. Contra Recibo-->
                                <div class="row">
                                    <div class="col-lg-6"><label>No. Contra Recibo</label></div>
                                    <div class="col-lg-6"><p class="form-control-static input_ver"><?= $ultimo ?></p></div>
                                </div>
                                <!--No. Compromiso-->
                                <div class="form-group input-group">
                                    <input type="text" class="form-control" name="no_compromiso" id="no_compromiso" placeholder="No. Compromiso" />
                                    <span class="input-group-btn ic-buscar-btn">
                                        <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_compromiso"><i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                </div>
                                <!--Destino-->
                                <input type="text" class="form-control" name="destino" id="destino" placeholder="Destino" disabled required/>
                                <!--Concepto Específico-->
                                <textarea style="height: 6.8em;" class="form-control" id="concepto_especifico" name="concepto_especifico" placeholder="Concepto Específico" disabled required></textarea>
                                <!--Concepto-->
                                <textarea style="height: 8em;" class="form-control" id="concepto" name="concepto" placeholder="Concepto" disabled required></textarea>

                            </div>
                            <div class="col-lg-4">
                                <!--Proveedor-->
                                <div class="form-group input-group forma_normal">
                                    <input type="hidden" name="hidden_clave_proveedor" id="hidden_clave_proveedor" value="" />
                                    <input type="text" class="form-control" name="proveedor" id="proveedor" placeholder="Proveedor" required/>
                                    <span class="input-group-btn ic-buscar-btn">
                                        <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_proveedores">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                </div>

                                <!--Beneficiario-->
                                <div class="form-group forma_diferente" style="display: none;">
                                    <input type="hidden" name="hidden_id_persona" id="hidden_id_persona" value="0" />
                                    <input type="hidden" name="hidden_tipo_impresion" id="hidden_tipo_impresion" />
                                    <input type="text" class="form-control" name="nombre_completo" id="nombre_completo" placeholder="Beneficiario" required/>
                                </div>

                                <!--Tipo Documento-->
                                <select class="form-control" id="tipo_documento" name="tipo_documento" required>
                                    <option value="">Comprobante Fiscal</option><!--Tipo de documento-->
                                    <option value="Factura">Factura</option>
                                    <option value="Honorario">Honorario</option>
                                    <option value="Nota de Crédito">Nota de Crédito</option>
                                    <option value="Nómina">Nómina</option>
                                </select>
                                <!--Documento-->
                                <input type="text" class="form-control" name="documento" id="documento" placeholder="No. Comprobante Fiscal" required/>
                                <!--Descripción Contra Recibo de Pago-->
                                <textarea style="height: 15.2em;" class="form-control" id="descripcion_general" name="descripcion_general" placeholder="Descripción del Contrarecibo" required></textarea>

                            </div>
                            <div class="col-lg-4">
                                <!--Fecha Solicitud-->
                                <input type="text" class="form-control ic-calendar" name="fecha_solicitud" id="fecha_solicitud" placeholder="Fecha de Solicitud" required>
                                <!--Fecha Posible de Pago-->
                                <input type="text" class="form-control ic-calendar" name="fecha_p_pago" id="fecha_p_pago" placeholder="Fecha Probable de Pago" required>
                                <!--Documentación Anexa-->
                                <textarea style="height: 5em;" class="form-control" id="documentacion_anexa" name="documentacion_anexa" placeholder="Documentación Anexa" required></textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row add-pre" id="desglose_honorarios">

                </div>

                <div class="row add-pre error-detalle">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Detalles
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="panel panel-default">
                                            <div class="panel-body table-gral">
                                                <div class="table-responsive">
                                                    <h3 id="suma_total" class="text-center"></h3>
                                                    <input type="hidden" value="" name="total_hidden" id="total_hidden" />
                                                    <table class="table table-striped table-bordered table-hover" id="tabla_datos_contrarecibo">
                                                        <thead>
                                                        <tr>
                                                            <th>ID</th>
                                                            <th width="15%">No. Compromiso</th>
                                                            <th width="18%">Tipo de Compromiso</th>
                                                            <th width="13%">Subtotal</th>
                                                            <th width="13%">IVA</th>
                                                            <th width="13%">Total</th>
                                                            <th>Descripción</th>
                                                            <th>Acciones</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-body text-center">
                        <div class="form-group c-firme">
                            <h3>¿Contra Recibo en Firme?</h3>
                            <label class="checkbox-inline">
                                <input type="checkbox" id="check_firme" name="check_firme" />Sí
                            </label>
                        </div>
                    </div>
                </div>

                <div class="btns-finales text-center">
                    <div class="text-center" id="resultado_insertar_caratula"></div>
                    <a href="<?= base_url("ciclo/contrarecibos") ?>" class="btn btn-default"><i class="fa fa-reply ic-color"></i> Regresar</a>
                    <input type="submit" name="guardar_contrarecibo" id="guardar_contrarecibo" class="btn btn-green" value="Guardar Contra Recibo" />
                </div>
            </div>
        </div>
    </form>
</div>
</div>
<!-- /.row -->

<!-- Modal Compromiso -->
<div class="modal fade" id="modal_compromiso" tabindex="-1" role="dialog" aria-labelledby="modal_compromiso" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-file-text-o ic-modal"></i> Compromisos</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-10">
                <input type="hidden" name="hidden_no_compromiso" id="hidden_no_compromiso" />
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tabla_compromiso">
                        <thead>
                        <tr><th width="6%">No.</th>
                            <th width="13%">Precompromiso</th>
                            <th width="12%">F. Autorizado</th>
                            <th width="10%">Tipo</th>
                            <th width="11%">Proveedor</th>
                            <th width="11%">Total</th>
                            <th width="12%">Creado Por</th>
                            <th width="7%">Firme</th>
                            <th width="9%">Estatus</th>
                            <th width="9%">Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Proveedores -->
<div class="modal fade" id="modal_proveedores" tabindex="-1" role="dialog" aria-labelledby="modal_proveedores" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-users ic-modal"></i> Proveedores</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-3">
                <div class="table-responsive">
                    <input type="hidden" name="hidden_clave_proveedor" id="hidden_clave_proveedor" value="" />
                    <table class="table table-striped table-bordered table-hover" id="tabla_proveedores">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Clave</th>
                            <th>Nombre Comercial</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Borrar Detalle -->
<div class="modal fade modal_borrar" tabindex="-1" role="dialog" aria-labelledby="modal_borrar" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-trash ic-modal"></i> Eliminar Partida</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="message-text" class="control-label">¿Realmente desea eliminar la partida seleccionada?</label>
                        <input type="hidden" value="" name="borrar_contrarecibo_hidden" id="borrar_contrarecibo_hidden" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_cancelar_contrarecibo">Aceptar</button>
            </div>
        </div>
    </div>
</div>

</div>
<!-- /#page-wrapper -->