<h3 class="page-header title center"><i class="fa fa-book"></i> Catálogo Conceptos Bancarios</h3>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body table-gral">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover datos_tabla">
                            <thead>
                            <tr>
                                <th>Clave</th>
                                <th>Concepto</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</div>


</div>
<!-- /.row -->
</div>
<!-- /#page-wrapper -->