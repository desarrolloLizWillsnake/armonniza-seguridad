<h3 class="page-header title center"><i class="fa fa-print"></i> Impresión Recaudado</h3>
<div id="page-wrapper">
    <div class="row center">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <div class="list-group error-completar">
                <?php if(isset($mensaje)) { ?>
                    <div class="alert alert-danger">
                        <?= $mensaje ?>
                    </div>
                    <div class="text-center">
                        <div class="btns-finales">
                            <a class="btn btn-default" href="<?= base_url("recaudacion/recaudado") ?>"><i class="fa fa-reply ic-color"></i> Regresar</a>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-8">
                            <div class="panel panel-default datos-requeridos">
                                <div class="panel-body">
                                    <form class="" action="<?= base_url("recaudacion/imprimir_recaudado_formato") ?>" method="POST" role="form">
                                        <!--Rango Fechas-->
                                        <div class="row marg_inf">
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control ic-calendar" name="fecha_inicial" id="fecha_inicial" placeholder="Fecha Inicial" value="2015-01-01">
                                            </div>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control ic-calendar" name="fecha_final" id="fecha_final" placeholder="Fecha Final" value="2015-12-31">
                                            </div>
                                        </div>
                                        <!-- Centro de Costos -->
                                        <div class="form-group input-group marg_inf">
                                            <input type="text" class="form-control marg_sup0" name="centro_recaudacion" id="centro_recaudacion" placeholder="Centro de Recaudación"/>
                                            <span class="input-group-btn ic-buscar-btn">
                                                <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_centro_recaudacion"><i class="fa fa-search"></i></button>
                                            </span>
                                        </div>
                                        <!--Clasificación-->
                                        <select class="form-control marg_inf" id="clasificacion" name="clasificacion">
                                            <option value="">Clasificación</option>
                                            <option value="Impuestos">Impuestos</option>
                                            <option value="Derechos">Derechos</option>
                                            <option value="Productos y/o Bienes">Productos y/o Bienes</option>
                                            <option value="Servicios">Servicios</option>
                                            <option value="Aprovechamientos">Aprovechamientos</option>
                                            <option value="Participaciones">Participaciones</option>
                                            <option value="Aportaciones">Aportaciones</option>
                                            <option value="Financiamientos">Financiamientos</option>
                                            <option value="Presupuesto">Presupuesto</option>
                                            <option value="Transferencias y/o Subsidios">Transferencias y/o Subsidios</option>
                                            <option value="Otros">Otros</option>
                                        </select>
                                        <!--No. Factura-->
                                        <input type="text" class="form-control marg_inf" name="num_movimiento" id="num_movimiento" placeholder="No. Factura"/>

                                        <div class="text-center">
                                            <div class="btns-finales text-center">
                                                <a class="btn btn-default" href="<?= base_url("recaudacion/recaudado") ?>"><i class="fa fa-reply ic-color"></i> Regresar</a>
                                                <button type="submit" class="btn btn-green">Continuar</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

</div>

<!-- Modal Centros de Costo -->
<div class="modal fade" id="modal_centro_recaudacion" tabindex="-1" role="dialog" aria-labelledby="modal_centro_recaudacion" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-building ic-modal"></i> Centros de Recaudación</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-3">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tabla_centro_recaudacion">
                        <thead>
                        <tr>
                            <th width="25%">Centro de Recaudación</th>
                            <th width="65%">Nombre</th>
                            <th width="10%">Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal Cliente-->
<div class="modal fade" id="modal_cliente" tabindex="-1" role="dialog" aria-labelledby="modal_cliente" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-users ic-modal"></i> Clientes</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-3">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tabla_cliente">
                        <thead>
                        <tr>
                            <th width="25%">Clave Cliente</th>
                            <th width="65%">Nombre</th>
                            <th width="10%">Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>