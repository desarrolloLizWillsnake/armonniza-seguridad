</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="<?= base_url("assets/templates/front/js/jquery.js") ?>"></script>

<script src="<?= base_url("assets/templates/front/js/plugins/external/jquery.hotkeys.js") ?>"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?= base_url("assets/templates/front/js/bootstrap.min.js") ?>"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="<?= base_url("assets/templates/front/js/plugins/metisMenu/metisMenu.min.js") ?>"></script>

<?php if(isset($graficas)) { ?>
    <!-- Morris Charts JavaScript -->
    <script src="<?= base_url("assets/templates/front/js/plugins/morris/raphael.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/morris/morris.min.js") ?>"></script>
<?php } ?>

<script src="<?= base_url("assets/templates/front/js/sb-admin-2.js") ?>"></script>

<script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>

<script>
    $('.dinero').number( true, 2 );
    $('.dias').number( true, 1 );
</script>

<?php if(isset($tablas)) { ?>
    <!-- DataTables JavaScript -->
    <script src="<?= base_url("assets/datatables/media/js/jquery.dataTables.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/dataTables/dataTables.bootstrap.js") ?>"></script>
<?php } ?>

<?php if(isset($exportar_tablas)) { ?>
    <!-- DataTables JavaScript -->
    <script src="<?= base_url("assets/datatables/media/js/jquery.dataTables.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/dataTables/dataTables.bootstrap.js") ?>"></script>
    <script src="<?= base_url("assets/datatables/extensions/TableTools/js/dataTables.tableTools.min.js") ?>"></script>
    <script src="<?= base_url("assets/datatables/extensions/Editor-1.3.3/js/dataTables.editor.min.js") ?>"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/3.5.2/select2.min.js"></script>
<?php } ?>

<?php if(isset($efectos)) { ?>
    <script src="<?= base_url("assets/templates/front/js/egresos.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
<?php } ?>

<?php if(isset($main)) { ?>
    <script src="<?= base_url("assets/templates/front/js/main.js") ?>"></script>
<?php } ?>

<?php if(isset($ingresos)) { ?>
    <script src="<?= base_url("assets/templates/front/js/ingresos.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
<?php } ?>

<?php if(isset($adminEstructuras)) { ?>
    <script src="<?= base_url("assets/templates/front/js/adminEstructuras.js") ?>"></script>
<?php } ?>

<?php if(isset($adecuaciones_ingresos)) { ?>
    <script src="<?= base_url("assets/templates/front/js/adecuacionesIngresos.js") ?>"></script>
<?php } ?>

<?php if(isset($adecuaciones_ingresos_agregar_transferencia)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/agregarIngresosTransferencia.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/external/google-code-prettify/prettify.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/bootstrap-wysiwyg.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.validate.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
<?php } ?>

<?php if(isset($editar_ingresos_agregar_transferencia)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/editarIngresosTransferencia.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/external/google-code-prettify/prettify.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/bootstrap-wysiwyg.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.validate.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
<?php } ?>

<?php if(isset($ver_ingresos_agregar_transferencia)) { ?>
    <script src="<?= base_url("assets/templates/front/js/verIngresosTransferencia.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/external/google-code-prettify/prettify.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/bootstrap-wysiwyg.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.validate.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
<?php } ?>

<?php if(isset($adecuaciones_ingresos_agregar_ampliacion)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/agregarIngresosAmpliacion.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/external/google-code-prettify/prettify.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/bootstrap-wysiwyg.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.validate.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
<?php } ?>

<?php if(isset($editar_ingresos_agregar_ampliacion)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/editarIngresosAmpliacion.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/external/google-code-prettify/prettify.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/bootstrap-wysiwyg.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.validate.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
<?php } ?>

<?php if(isset($ver_ingresos_agregar_ampliacion)) { ?>
    <script src="<?= base_url("assets/templates/front/js/verIngresosAmpliacion.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/external/google-code-prettify/prettify.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/bootstrap-wysiwyg.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.validate.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
<?php } ?>

<?php if(isset($adecuaciones_ingresos_agregar_reduccion)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/agregarIngresosReduccion.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/external/google-code-prettify/prettify.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/bootstrap-wysiwyg.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.validate.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
<?php } ?>

<?php if(isset($editar_ingresos_agregar_reduccion)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/editarIngresosReduccion.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/external/google-code-prettify/prettify.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/bootstrap-wysiwyg.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.validate.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
<?php } ?>

<?php if(isset($ver_ingresos_agregar_reduccion)) { ?>
    <script src="<?= base_url("assets/templates/front/js/verIngresosReduccion.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/external/google-code-prettify/prettify.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/bootstrap-wysiwyg.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.validate.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
<?php } ?>

<?php if(isset($adecuaciones_egresos)) { ?>
    <script src="<?= base_url("assets/templates/front/js/adecuacionesEgresos.js") ?>"></script>
<?php } ?>

<?php if(isset($adecuaciones_egresos_agregar_transferencia)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/agregarEgresosTransferencia.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/external/google-code-prettify/prettify.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/bootstrap-wysiwyg.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.validate.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
<?php } ?>

<?php if(isset($editar_egresos_agregar_transferencia)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/editarEgresosTransferencia.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/external/google-code-prettify/prettify.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/bootstrap-wysiwyg.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.validate.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
<?php } ?>

<?php if(isset($ver_egresos_agregar_transferencia)) { ?>
    <script src="<?= base_url("assets/templates/front/js/verEgresosTransferencia.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/external/google-code-prettify/prettify.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/bootstrap-wysiwyg.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.validate.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
<?php } ?>

<?php if(isset($adecuaciones_egresos_agregar_ampliacion)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/agregarEgresosAmpliacion.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/external/google-code-prettify/prettify.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/bootstrap-wysiwyg.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.validate.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
<?php } ?>

<?php if(isset($editar_egresos_agregar_ampliacion)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/editarEgresosAmpliacion.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/external/google-code-prettify/prettify.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/bootstrap-wysiwyg.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.validate.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
<?php } ?>

<?php if(isset($ver_egresos_agregar_ampliacion)) { ?>
    <script src="<?= base_url("assets/templates/front/js/verEgresosAmpliacion.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/external/google-code-prettify/prettify.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/bootstrap-wysiwyg.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.validate.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
<?php } ?>

<?php if(isset($adecuaciones_egresos_agregar_reduccion)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/agregarEgresosReduccion.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/external/google-code-prettify/prettify.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/bootstrap-wysiwyg.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.validate.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
<?php } ?>

<?php if(isset($editar_egresos_agregar_reduccion)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/editarEgresosReduccion.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/external/google-code-prettify/prettify.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/bootstrap-wysiwyg.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.validate.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
<?php } ?>

<?php if(isset($ver_egresos_agregar_reduccion)) { ?>
    <script src="<?= base_url("assets/templates/front/js/verEgresosReduccion.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/external/google-code-prettify/prettify.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/bootstrap-wysiwyg.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.validate.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
<?php } ?>

<?php if(isset($agregar_precompromiso)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/agregarPrecompromiso.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/external/google-code-prettify/prettify.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/bootstrap-wysiwyg.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.validate.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
<?php } ?>

<?php if(isset($agregar_compromiso)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/agregarCompromiso.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/external/google-code-prettify/prettify.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/bootstrap-wysiwyg.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.validate.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
<?php } ?>

<?php if(isset($editar_precompromiso)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/editarPrecompromiso.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/external/google-code-prettify/prettify.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/bootstrap-wysiwyg.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.validate.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
<?php } ?>

<?php if(isset($ver_precompromiso)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/verPrecompromiso.js") ?>"></script>
<?php } ?>

<?php if(isset($terminar_precompromiso)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/terminarPrecompromiso.js") ?>"></script>
<?php } ?>

<?php if(isset($precompromiso)) { ?>
    <script src="<?= base_url("assets/templates/front/js/precompromiso.js") ?>"></script>
<?php } ?>

<?php if(isset($compromiso)) { ?>
    <script src="<?= base_url("assets/templates/front/js/compromiso.js") ?>"></script>
<?php } ?>

<?php if(isset($editar_compromiso)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/editarCompromiso.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/external/google-code-prettify/prettify.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/bootstrap-wysiwyg.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.validate.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
<?php } ?>

<?php if(isset($ver_compromiso)) { ?>
    <script src="<?= base_url("assets/templates/front/js/verCompromiso.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
<?php } ?>

<?php if(isset($contrarecibos)) { ?>
    <script src="<?= base_url("assets/templates/front/js/contrarecibos.js") ?>"></script>
<?php } ?>

<?php if(isset($agregar_contrarecibo)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/agregarContrarecibo.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/external/google-code-prettify/prettify.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/bootstrap-wysiwyg.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.validate.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
<?php } ?>

<?php if(isset($agregar_movimiento)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/agregarMovimiento.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/external/google-code-prettify/prettify.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/bootstrap-wysiwyg.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.validate.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
<?php } ?>

<?php if(isset($agregar_movimiento_conciliacion)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/agregarMovimientoConciliacion.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/external/google-code-prettify/prettify.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/bootstrap-wysiwyg.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.validate.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
<?php } ?>

<?php if(isset($polizas)) { ?>
    <script src="<?= base_url("assets/templates/front/js/indicePoliza.js") ?>"></script>
<?php } ?>

<?php if(isset($agregar_editar_poliza)) { ?>
    <script src="<?= base_url("assets/templates/front/js/agregarEditarPoliza.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/external/google-code-prettify/prettify.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/bootstrap-wysiwyg.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.validate.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
<?php } ?>

<?php if(isset($exportarPolizas)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/exportarPolizas.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/external/google-code-prettify/prettify.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/bootstrap-wysiwyg.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.validate.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
<?php } ?>

<?php if(isset($agregarPolizas)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/agregarPoliza.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.validate.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
    <script>
        <?php if(isset($fecha_minima) && $fecha_minima != NULL) {
        $fecha_minima = date('Y-m-d', strtotime($fecha_minima. ' + 2 days'));
        ?>
        $( "#fecha" ).datepicker({
            dateFormat: 'yy-mm-dd',
            minDate: new Date("<?= $fecha_minima ?>")
        });
        <?php } else { ?>
        $( "#fecha" ).datepicker({
            dateFormat: 'yy-mm-dd'
        });
        <?php } ?>
    </script>
<?php } ?>

<?php if(isset($editarPolizas)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/editarPoliza.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/external/google-code-prettify/prettify.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/bootstrap-wysiwyg.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.validate.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
    <script>
        <?php if(isset($fecha_minima) && $fecha_minima != NULL) {
        $fecha_minima = date('Y-m-d', strtotime($fecha_minima. ' + 2 days'));
        ?>
        $( "#fecha" ).datepicker({
            dateFormat: 'yy-mm-dd',
            minDate: new Date("<?= $fecha_minima ?>")
        });
        <?php } else { ?>
        $( "#fecha" ).datepicker({
            dateFormat: 'yy-mm-dd'
        });
        <?php } ?>
    </script>
<?php } ?>

<?php if(isset($verPolizas)) { ?>
    <script src="<?= base_url("assets/templates/front/js/verPoliza.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
<?php } ?>

<?php if(isset($polizas_firme)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/polizasFirme.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.validate.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
<?php } ?>

<?php if(isset($editar_movimiento)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/editarMovimiento.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/external/google-code-prettify/prettify.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/bootstrap-wysiwyg.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.validate.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
<?php } ?>

<?php if(isset($ver_movimiento)) { ?>
    <script src="<?= base_url("assets/templates/front/js/verMovimiento.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
<?php } ?>

<?php if(isset($editar_contrarecibo)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/editarContrarecibo.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/external/google-code-prettify/prettify.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/bootstrap-wysiwyg.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.validate.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
<?php } ?>

<?php if(isset($ver_contrarecibo)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/verContrarecibo.js") ?>"></script>
<?php } ?>

<?php if(isset($detalle_cuenta)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/detalle_cuenta.js") ?>"></script>
<?php } ?>

<?php if(isset($detalle_cuenta_conciliacion)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery.validate.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/detalle_cuenta_conciliacion.js") ?>"></script>
<?php } ?>

<?php if(isset($matrizjs)) { ?>
    <?php if($this->utilerias->get_permisos("agregar_datos_matriz") || $this->utilerias->get_grupo() == 1){ ?>
        <script src="<?= base_url("assets/templates/front/js/matriz.js") ?>"></script>
    <?php } else { ?>
        <script src=" <?= base_url("assets/templates/front/js/matriz_privilegios.js") ?>"></script>
    <?php } ?>
<?php } ?>

<?php if(isset($agregar_matriz)) { ?>
    <script src="<?= base_url("assets/templates/front/js/agregarMatriz.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/external/google-code-prettify/prettify.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/bootstrap-wysiwyg.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.validate.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
<?php } ?>

<?php if(isset($editar_matriz)) { ?>
    <script src="<?= base_url("assets/templates/front/js/editarMatriz.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/external/google-code-prettify/prettify.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/bootstrap-wysiwyg.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.validate.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
<?php } ?>
<?php if(isset($balanza)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/balanza.js") ?>"></script>
<?php } ?>
<?php if(isset($consulta_balanza)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/consulta_balanza.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
<?php } ?>

<?php if(isset($manuales)) { ?>
    <script src="<?= base_url("assets/templates/front/js/manuales.js") ?>"></script>
<?php } ?>

<?php if(isset($plan_cuentas)) { ?>
    <script src="<?= base_url("assets/templates/front/js/plancuenta.js") ?>"></script>
<?php } ?>

<?php if(isset($auxiliar)) { ?>
    <script src="<?= base_url("assets/templates/front/js/auxiliar.js") ?>"></script>
<?php } ?>

<?php if(isset($catalogo_proveedores)) { ?>
    <script src="<?= base_url("assets/templates/front/js/catalogoProveedores.js") ?>"></script>
<?php } ?>

<?php if(isset($catalogo_beneficiarios)) { ?>
    <script src="<?= base_url("assets/templates/front/js/catalogoBeneficiarios.js") ?>"></script>
<?php } ?>

<?php if(isset($catalogo_bancos)) { ?>
    <script src="<?= base_url("assets/templates/front/js/catalogoBancos.js") ?>"></script>
<?php } ?>

<?php if(isset($catalogo_artiulos)) { ?>
    <script src="<?= base_url("assets/templates/front/js/catalogoArticulos.js") ?>"></script>
<?php } ?>

<?php if(isset($catalogo_librerias)) { ?>
    <script src="<?= base_url("assets/templates/front/js/catalogoLibrerias.js") ?>"></script>
<?php } ?>

<?php if(isset($catalogo_medicamentos)) { ?>
    <script src="<?= base_url("assets/templates/front/js/catalogoMedicamentos.js") ?>"></script>
<?php } ?>

<?php if(isset($clasificador_cucop)) { ?>
    <script src="<?= base_url("assets/templates/front/js/clasificador_cucop.js") ?>"></script>
<?php } ?>

<?php if(isset($catalogo_cuentas)) { ?>
    <?php if($this->utilerias->get_permisos("agregar_datos_cuentas") || $this->utilerias->get_grupo() == 1){ ?>
        <script src="<?= base_url("assets/templates/front/js/catalogoCuentas.js") ?>"></script>
    <?php } else { ?>
        <script src="<?= base_url("assets/templates/front/js/catalogoCuentasPrivilegios.js") ?>"></script>
    <?php } ?>
<?php } ?>

<?php if(isset($catalogo_conceptos)) { ?>
    <script src="<?= base_url("assets/templates/front/js/catalogoConceptos.js") ?>"></script>
<?php } ?>

<?php if(isset($catalogo_medidas)) { ?>
    <script src="<?= base_url("assets/templates/front/js/catalogoMedidas.js") ?>"></script>
<?php } ?>

<?php if(isset($firmas_ciclo)) { ?>
    <script src="<?= base_url("assets/templates/front/js/firmasCiclo.js") ?>"></script>
<?php } ?>

<?php if(isset($firmas_contabilidad)) { ?>
    <script src="<?= base_url("assets/templates/front/js/firmasContabilidad.js") ?>"></script>
<?php } ?>

<?php if(isset($menureportes)) { ?>
    <script src="<?= base_url("assets/templates/front/js/reportes.js") ?>"></script>
<?php } ?>

<?php if(isset($reporte_ingresos_adecuaciones)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/reporteIngresosAdecuaciones.js") ?>"></script>
<?php } ?>

<?php if(isset($reporte_egresos_adecuaciones)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/reporteEgresosAdecuaciones.js") ?>"></script>
<?php } ?>

<?php if(isset($reporte_diot)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/reporteDiot.js") ?>"></script>
<?php } ?>

<?php if(isset($devengado)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/devengado.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.validate.min.js") ?>"></script>
<?php } ?>

<?php if(isset($agregar_devengado)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/agregarDevengado.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.validate.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
<?php } ?>
<?php if(isset($ver_devengado)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/verDevengado.js") ?>"></script>
<?php } ?>
<?php if(isset($editar_devengado)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/editarDevengado.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.validate.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
<?php } ?>
<?php if(isset($recaudado)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/recaudado.js") ?>"></script>
<?php } ?>
<?php if(isset($agregar_recaudado)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/agregarRecaudado.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.validate.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
<?php } ?>
<?php if(isset($editar_recaudado)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/editarRecaudado.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.validate.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
<?php } ?>
<?php if(isset($ver_recaudado)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/verRecaudado.js") ?>"></script>
<?php } ?>
<?php if(isset($nota_entrada)) { ?>
    <script src="<?= base_url("assets/templates/front/js/patrimonio_nota_entrada.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/bootstrap-wysiwyg.js") ?>"></script>
<?php } ?>
<?php if(isset($nota_salida)) { ?>
    <script src="<?= base_url("assets/templates/front/js/patrimonio_nota_salida.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/bootstrap-wysiwyg.js") ?>"></script>
<?php } ?>
<?php if(isset($agregar_nota_entrada)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/agregarNotaEntrada.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/external/google-code-prettify/prettify.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/bootstrap-wysiwyg.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.validate.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
<?php } ?>

<?php if(isset($editar_nota_entrada)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/editarNotaEntrada.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/external/google-code-prettify/prettify.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/bootstrap-wysiwyg.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.validate.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
<?php } ?>

<?php if(isset($ver_nota_entrada)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/verNotaEntrada.js") ?>"></script>
<?php } ?>

<?php if(isset($agregar_nota_salida)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/agregarNotaSalida.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/external/google-code-prettify/prettify.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/bootstrap-wysiwyg.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.validate.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
<?php } ?>

<?php if(isset($editar_nota_salida)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/external/google-code-prettify/prettify.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/bootstrap-wysiwyg.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.validate.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/editarNotaSalida.js") ?>"></script>
<?php } ?>
<?php if(isset($ver_nota_salida)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/verNotaSalida.js") ?>"></script>
<?php } ?>
<?php if(isset($empresa)) { ?>
    <script src="<?= base_url("assets/templates/front/js/adminEmpresa.js") ?>"></script>
<?php } ?>

<?php if(isset($usuarios)) { ?>
    <script src="<?= base_url("assets/templates/front/js/adminUsuarios.js") ?>"></script>
<?php } ?>

<?php if(isset($privilegios_usuarios)) { ?>
    <script src="<?= base_url("assets/templates/front/js/editarPrivilegios.js") ?>"></script>
<?php } ?>

<?php if(isset($editar_usuarios)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/editarUsuarios.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/external/google-code-prettify/prettify.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/bootstrap-wysiwyg.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.validate.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
<?php } ?>

<?php if(isset($editar_autorizador)) { ?>
    <script src="<?= base_url("assets/templates/front/js/editar_autorizador.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/bootstrap-wysiwyg.js") ?>"></script>
<?php } ?>

<?php if(isset($periodos_contables)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/periodosContables.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.validate.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
<?php } ?>

<?php if(isset($movimiento)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/movimiento.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/plugins/bootstrap-wysiwyg.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.validate.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
<?php } ?>

<?php if(isset($kardex_index)) { ?>
    <script src="<?= base_url("assets/templates/front/js/kardexIndex.js") ?>"></script>
<?php } ?>

<?php if(isset($catalogo_inventario)) { ?>
    <script src="<?= base_url("assets/templates/front/js/InvetarioIndex.js") ?>"></script>
<?php } ?>

<?php if(isset($catalogo_inventario_agregar_producto)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.validate.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.form.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/catalogoInventarioAgregarProducto.js") ?>"></script>
<?php } ?>

<?php if(isset($catalogo_inventario_editar_producto)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.validate.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.number.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/jquery.form.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/catalogoInventarioEditarProducto.js") ?>"></script>
<?php } ?>

<?php if(isset($consulta_inventarios)) { ?>
    <script src="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.js") ?>"></script>
    <script src="<?= base_url("assets/templates/front/js/consultaInventarios.js") ?>"></script>
<?php } ?>

<?php if(isset($nuevas_tablas)) { ?>
    <script src="<?= base_url("assets/nuevo_datatables/media/js/jquery.dataTables.min.js") ?>"></script>
<?php } ?>

<?php if(isset($nuevas_tablas_autofill)) { ?>
    <script src="<?= base_url("assets/nuevo_datatables/extensions/AutoFill/js/dataTables.autoFill.min.js") ?>"></script>
<?php } ?>

<?php if(isset($nuevas_tablas_buttons)) { ?>
    <script src="<?= base_url("assets/nuevo_datatables/extensions/Buttons/js/dataTables.buttons.min.js") ?>"></script>
<?php } ?>

<?php if(isset($nuevas_tablas_select)) { ?>
    <script src="<?= base_url("assets/nuevo_datatables/extensions/Select/js/dataTables.select.min.js") ?>"></script>
<?php } ?>

<?php if(isset($nuevas_tablas_colreorder)) { ?>
    <script src="<?= base_url("assets/nuevo_datatables/extensions/ColReorder/js/dataTables.colReorder.min.js") ?>"></script>
<?php } ?>

<?php if(isset($nuevas_tablas_fixedcolumns)) { ?>
    <script src="<?= base_url("assets/nuevo_datatables/extensions/FixedColumns/js/dataTables.fixedColumns.min.js") ?>"></script>
<?php } ?>

<?php if(isset($nuevas_tablas_fixedheader)) { ?>
    <script src="<?= base_url("assets/nuevo_datatables/extensions/FixedHeader/js/dataTables.fixedHeader.min.js") ?>"></script>
<?php } ?>

<?php if(isset($nuevas_tablas_keytable)) { ?>
    <script src="<?= base_url("assets/nuevo_datatables/extensions/KeyTable/js/dataTables.keyTable.min.js") ?>"></script>
<?php } ?>

<?php if(isset($nuevas_tablas_responsive)) { ?>
    <script src="<?= base_url("assets/nuevo_datatables/extensions/Responsive/js/dataTables.responsive.min.js") ?>"></script>
<?php } ?>

<?php if(isset($nuevas_tablas_rowreorder)) { ?>
    <script src="<?= base_url("assets/nuevo_datatables/extensions/RowReorder/js/dataTables.rowReorder.min.js") ?>"></script>
<?php } ?>

<?php if(isset($nuevas_tablas_scroller)) { ?>
    <script src="<?= base_url("assets/nuevo_datatables/extensions/Scroller/js/dataTables.scroller.min.js") ?>"></script>
<?php } ?>

<?php if(isset($nuevas_tablas_editor)) { ?>
    <script src="<?= base_url("assets/nuevo_datatables/extensions/Editor/js/dataTables.editor.min.js") ?>"></script>
<?php } ?>

<!--<?php if(isset($catalogo_partidas)&& $this->utilerias->get_permisos("botones_partidas") || $this->utilerias->get_grupo() == 1){ ?>
    <script src="<?= base_url("assets/templates/front/js/anteproyecto/catalogoPartidasBotones.js") ?>"></script>
<?php } else {?>
    <script src="<?= base_url("assets/templates/front/js/anteproyecto/catalogoPartidas.js") ?>"></script>
<?php } ?>

<?php if(isset($catalogo_usuarios) && $this->utilerias->get_permisos("botones_cc") || $this->utilerias->get_grupo() == 1){ ?>
    <script src="<?= base_url("assets/templates/front/js/anteproyecto/catalogoUsuariosBotones.js") ?>"></script>
<?php } else { ?>
    <script src="<?= base_url("assets/templates/front/js/anteproyecto/catalogoUsuarios.js") ?>"></script>
<?php } ?>
-->
<?php if(isset($catalogo_partidas)) { ?>
    <?php if($this->utilerias->get_permisos("botones_partidas") || $this->utilerias->get_grupo() == 1){ ?>
        <script src="<?= base_url("assets/templates/front/js/anteproyecto/catalogoPartidasBotones.js") ?>"></script>
    <?php } else {?>
        <script src="<?= base_url("assets/templates/front/js/anteproyecto/catalogoPartidas.js") ?>"></script>
    <?php } ?>
<?php } ?>

<?php if(isset($catalogo_usuarios)) { ?>
    <?php if($this->utilerias->get_permisos("botones_cc") || $this->utilerias->get_grupo() == 1){ ?>
        <script src="<?= base_url("assets/templates/front/js/anteproyecto/catalogoUsuariosBotones.js") ?>"></script>
    <?php } else { ?>
        <script src="<?= base_url("assets/templates/front/js/anteproyecto/catalogoUsuarios.js") ?>"></script>
    <?php } ?>
<?php } ?>

<?php if(isset($presupuesto_aprobado)) { ?>
    <script src="<?= base_url("assets/templates/front/js/anteproyecto/presupuestoAprobado.js") ?>"></script>
<?php } ?>

<?php if(isset($servicios_subrogados)) { ?>
    <script src="<?= base_url("assets/templates/front/js/anteproyecto/serviciosSubrogados.js") ?>"></script>
<?php } ?>

<?php if(isset($exportar_aprobado)) { ?>
    <script src="<?= base_url("assets/templates/front/js/anteproyecto/exportarAprobado.js") ?>"></script>
<?php } ?>

<?php if(isset($presupuesto_busqueda)) { ?>
    <script src="<?= base_url("assets/templates/front/js/anteproyecto/presupuestoBusqueda.js") ?>"></script>
<?php } ?>

<?php if(isset($indice_empleados)) { ?>
    <script src="<?= base_url("assets/templates/front/js/empleados.js") ?>"></script>
<?php } ?>

<script>
    $('.editor').wysiwyg();
</script>

</body>

</html>