<h3 class="page-header title center"><span class="glyphicon glyphicon-indent-left"></span> Estructura Administrativa de Egresos</h3>
<div id="page-wrapper">
    <div class="panel-body" id="estructura_egresos_contenido">
        <div class="row cont-btns center">
            <div class="col-lg-12">
                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#agregar_nivel" data-whatever="Agregar"><i class="fa fa-plus-circle circle ic-color"></i> Agregar Nivel</button>
                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#editar_nivel" data-whatever="Editar"><i class="fa fa-edit ic-color"></i> Editar Nivel</button>
<!--                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#exampleModal" data-whatever="Borrar"><i class="fa fa-trash-o ic-color"></i> Borrar Nivel</button>-->
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <form>
                    <?= $niveles ?>
                </form>
            </div>
        </div>

        <div class="row cont_grafica">
            <div class="col-lg-12">
                <div id="grafica_partida"></div>
            </div>
        </div>
    </div>

</div>

</div>
<!-- /#page-wrapper -->

<div class="modal fade" id="agregar_nivel" tabindex="-1" role="dialog" aria-labelledby="agregar_nivel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><span class="glyphicon glyphicon-indent-left"></span> Agregar Nivel Estructura Administrativa de Egresos</h4>
            </div>
            <div class="modal-body">
                <form role="form" id="form_agregar_partida" action="">
                    <div class="form-group">
                        <select class="form-control" id="egresos_elegir_agregar_nivel">
                            <option value="">Elegir Nivel</option>
                            <?php
                            $nivel = 1;
                            foreach($nombres_egresos as $fila){
                                echo('<option value="'.$nivel.'">'.ucfirst(strtolower($fila)).'</option>');
                                $nivel += 1;
                            }
                            ?>
                        </select>
                        <div>
                            <select class="form-control" name="fuente_financiamiento_agregar_select" id="fuente_financiamiento_agregar_select" style="display: none;"  /></select>
                            <select class="form-control" name="programa_financiamiento_agregar_select" id="programa_financiamiento_agregar_select" style="display: none;"  /></select>
                            <select class="form-control" name="centro_de_costos_agregar_select" id="centro_de_costos_agregar_select" style="display: none;" /></select>
                            <select class="form-control" name="capitulo_agregar_select" id="capitulo_agregar_select" style="display: none;" /></select>
                            <select class="form-control" name="concepto_agregar_select" id="concepto_agregar_select" style="display: none;" /></select>
                            <div id="egresos_modal_agregar_contenido" class="right"></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="editar_nivel" tabindex="-1" role="dialog" aria-labelledby="editar_nivel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><span class="glyphicon glyphicon-indent-left"></span> Editar Nivel Estructura Administrativa de Egresos</h4>
            </div>
            <div class="modal-body">
                <form role="form" id="form_editar_partida" action="">
                    <div class="form-group">
                        <select class="form-control" id="egresos_elegir_editar_nivel">
                            <option value="">Elegir Nivel</option>
                            <?php
                            $nivel = 1;
                            foreach($nombres_egresos as $fila){
                                echo('<option value="'.$nivel.'">'.ucfirst(strtolower($fila)).'</option>');
                                $nivel += 1;
                            }
                            ?>
                        </select>
                        <div>
                            <select name="fuente_financiamiento_editar_select" id="fuente_financiamiento_editar_select" style="display: none;"  /></select>
                            <select name="programa_financiamiento_editar_select" id="programa_financiamiento_editar_select" style="display: none;"  /></select>
                            <select name="centro_de_costos_editar_select" id="centro_de_costos_editar_select" style="display: none;" /></select>
                            <select name="capitulo_editar_select" id="capitulo_editar_select" style="display: none;" /></select>
                            <select name="concepto_editar_select" id="concepto_editar_select" style="display: none;" /></select>
                            <select name="partida_editar_select" id="partida_editar_select" style="display: none;" /></select>
                            <input type="hidden" name="clave_anterior" id="clave_anterior" value="" />
                            <input type="hidden" name="descripcion_anterior" id="descripcion_anterior" value="" />
                            <div id="egresos_modal_editar_contenido">

                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>