<h3 class="page-header center"><i class="fa fa-edit"></i> Editar Reducción</h3>
<div id="page-wrapper">
    <form class="forma_reduccion" role="form">
        <div class="row add-pre error-gral">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        General
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <input type="hidden" name="ultimo" id="ultimo" value="<?= $numero ?>">
                        <div class="row">
                            <!--Primera Columna-->
                            <div class="col-lg-4">
                                <!--Clasificación-->
                                <select class="form-control" id="clasificacion" name="clasificacion" required>
                                    <option value="">Clasificación</option>
                                    <option value="Otros" <?= $clasificacion == 'Internas' ? ' selected="selected"' : '';?>>Internas</option>
                                    <option value="Otros" <?= $clasificacion == 'Externas' ? ' selected="selected"' : '';?>>Externas</option>
                                    <option value="Impuestos" <?= $clasificacion == 'Impuestos' ? ' selected="selected"' : '';?>>Impuestos</option>
                                    <option value="Derechos" <?= $clasificacion == 'Derechos' ? ' selected="selected"' : '';?>>Derechos</option>
                                    <option value="Productos y/o Bienes" <?= $clasificacion == 'Productos y/o Bienes' ? ' selected="selected"' : '';?>>Productos y/o Bienes</option>
                                    <option value="Servicios" <?= $clasificacion == 'Servicios' ? ' selected="selected"' : '';?>>Servicios</option>
                                    <option value="Aprovechamientos" <?= $clasificacion == 'Aprovechamientos' ? ' selected="selected"' : '';?>>Aprovechamientos</option>
                                    <option value="Participaciones" <?= $clasificacion == 'Participaciones' ? ' selected="selected"' : '';?>>Participaciones</option>
                                    <option value="Aportaciones" <?= $clasificacion == 'Aportaciones' ? ' selected="selected"' : '';?>>Aportaciones</option>
                                    <option value="Financiamientos" <?= $clasificacion == 'Financiamientos' ? ' selected="selected"' : '';?>>Financiamientos</option>
                                    <option value="Presupuesto" <?= $clasificacion == 'Presupuesto' ? ' selected="selected"' : '';?>>Presupuesto</option>
                                    <option value="Transferencias y/o Subsidios" <?= $clasificacion == 'Transferencias y/o Subsidios' ? ' selected="selected"' : '';?>>Transferencias y/o Subsidios</option>
                                    <option value="Otros" <?= $clasificacion == 'Otros' ? ' selected="selected"' : '';?>>Otros</option>
                                </select>
                                <!--Justificación-->
                                <textarea style="height: 9em;" class="form-control" id="descripcion" name="descripcion" placeholder="Justificación" <?= $descripcion != NULL ? '> '.$descripcion.'</textarea>' : '> </textarea>' ;?>
                            </div>
                            <!--Fin Primera Columna-->
                            <!--Segunda Columna-->
                            <div class="col-lg-5" style="padding-left: 3%;">
                                <!-- Tipo de Gasto-->
                                <label>Tipo de Gastos</label>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="tipo_gasto" id="optionsRadios1" value="1" <?= $tipo_gasto == '1' ? ' checked="checked"' : '';?>>Gasto Corriente
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="tipo_gasto" id="optionsRadios2" value="2" <?= $tipo_gasto == '2' ? ' checked="checked"' : '';?>>Gasto de Capital
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="tipo_gasto" id="optionsRadios3" value="3" <?= $tipo_gasto == '3' ? ' checked="checked"' : '';?>>Amortización de la cuenta y disminución de pasivos
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="tipo_gasto" id="optionsRadios4" value="4" <?= $tipo_gasto == '4' ? ' checked="checked"' : '';?>>Pensiones y Jubilaciones
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="tipo_gasto" id="optionsRadios5" value="5" <?= $tipo_gasto == '5' ? ' checked="checked"' : '';?>>Participaciones
                                    </label>
                                </div>
                            </div>
                            <!--Fin Segunda Columna-->
                            <!--Tercer Columna-->
                            <div class="col-lg-3">
                                <!--Fecha Solicitud-->
                                <input ype="text" class="form-control ic-calendar" name="fecha_solicitud" id="fecha_solicitud" placeholder="Fecha de Solicitud" <?= $fecha_solicitud != NULL ? ' value="'.$fecha_solicitud.'"' : '';?>  disabled/>
                                <!--Fecha-->
                                <input ype="text" class="form-control ic-calendar" name="fecha_aplicar_caratula" id="fecha_aplicar_caratula" placeholder="Fecha Aplicada" <?= $fecha_aplicacion != NULL ? ' value="'.$fecha_aplicacion.'"' : '';?>  required/>
                            </div>
                            <!--Fin Tercer Columna-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row add-pre error-detalle">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Información de Partida
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <!--Primera Parte Partida Inicio-->
                        <div class="row">
                            <div class="col-lg-2 niveles-pc">
                                <?= $nivel_superior ?>
                                <a href="#modal_estructura_superior" class="btn btn-default" data-toggle="modal" data-target="#modal_estructura_superior">¿No conoces la
                                    <br/>estructura?</a>
                            </div>

                            <div class="col-lg-10">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <label>Enero</label>
                                        <input type="text" class="form-control" style="margin-top: .3%;" name="enero_superior" id="enero_superior" disabled/>
                                    </div>
                                    <div class="col-lg-4">
                                        <label>Febrero</label>
                                        <input type="text" class="form-control" style="margin-top: .3%;" name="febrero_superior" id="febrero_superior" disabled/>
                                    </div>
                                    <div class="col-lg-4">
                                        <label>Marzo</label>
                                        <input type="text" class="form-control" style="margin-top: .3%;" name="marzo_superior" id="marzo_superior" disabled/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <label>Abril</label>
                                        <input type="text" class="form-control" style="margin-top: .3%;" name="abril_superior" id="abril_superior" disabled/>
                                    </div>
                                    <div class="col-lg-4">
                                        <label>Mayo</label>
                                        <input type="text" class="form-control" style="margin-top: .3%;" name="mayo_superior" id="mayo_superior" disabled/>
                                    </div>
                                    <div class="col-lg-4">
                                        <label>Junio</label>
                                        <input type="text" class="form-control" style="margin-top: .3%;" name="junio_superior" id="junio_superior" disabled/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <label>Julio</label>
                                        <input type="text" class="form-control" style="margin-top: .3%;" name="julio_superior" id="julio_superior" disabled/>
                                    </div>
                                    <div class="col-lg-4">
                                        <label>Agosto</label>
                                        <input type="text" class="form-control" style="margin-top: .3%;" name="agosto_superior" id="agosto_superior" disabled/>
                                    </div>
                                    <div class="col-lg-4">
                                        <label>Septiembre</label>
                                        <input type="text" class="form-control" style="margin-top: .3%;" name="septiembre_superior" id="septiembre_superior" disabled/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <label>Octubre</label>
                                        <input type="text" class="form-control" style="margin-top: .3%;" name="octubre_superior" id="octubre_superior" disabled/>
                                    </div>
                                    <div class="col-lg-4">
                                        <label>Noviembre</label>
                                        <input type="text" class="form-control" style="margin-top: .3%;" name="noviembre_superior" id="noviembre_superior" disabled/>
                                    </div>
                                    <div class="col-lg-4">
                                        <label>Diciembre</label>
                                        <input type="text" class="form-control" style="margin-top: .3%;" name="diciembre_superior" id="diciembre_superior" disabled/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-2"></div>
                            <div class="col-lg-10 text-center">
                                <h4 id="total_superior"></h4>
                            </div>
                        </div>
                        <!--Primera Parte Partida Inicio-->
                    </div>
                </div>
            </div>
        </div>
        <div class="row add-pre error-detalle">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Reducción de Partida
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <!--Segunda Parte Partida Inicio-->
                        <div class="row">
                            <div class="col-lg-4 niveles-pc">
                                <!--Estructura Administrativa de ingresos-->
                                <?= $nivel_inferior ?>
                                <a href="#modal_estructura_inferior" class="btn btn-default" data-toggle="modal" data-target="#modal_estructura_inferior" style="margin-top: 3%;">¿No conoces la
                                    <br/>estructura?</a>
                            </div>

                            <div class="col-lg-8">
                                <div class="row">
                                    <div class="col-lg-8">
                                        <input type="hidden" name="total_hidden" id="total_hidden" />
                                        <select class="form-control" name="mes_destino" required>
                                            <option value="">Mes en que se realizará la reducción</option>
                                            <option value="enero">Enero</option>
                                            <option value="febrero">Febrero</option>
                                            <option value="marzo">Marzo</option>
                                            <option value="abril">Abril</option>
                                            <option value="mayo">Mayo</option>
                                            <option value="junio">Junio</option>
                                            <option value="julio">Julio</option>
                                            <option value="agosto">Agosto</option>
                                            <option value="septiembre">Septiembre</option>
                                            <option value="octubre">Octubre</option>
                                            <option value="noviembre">Noviembre</option>
                                            <option value="diciembre">Diciembre</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control ic-calendar" name="fecha_aplicar_detalle" id="fecha_aplicar_detalle" placeholder="Fecha Aplicación" />
                                    </div>
                                </div>
                                <div class="form-group input-group">
                                            <span class="input-group-btn ic-peso-btn">
                                                <button class="btn btn-default sg-dollar"><i class="fa fa-dollar"></i></button>
                                            </span>
                                    <input style="margin-top: 1.4%;" type="text" pattern="[0-9]+([\.|,][0-9]+)?" class="form-control dinero" name="cantidad" placeholder="Importe" />
                                </div>
                            </div>
                        </div>
                        <!-- Fin Segunda Parte Partida Inicio-->
                        <!-- Tercer Parte Partida Inicio-->
                        <div class="row">
                            <div class="col-lg-12">
                                <button type="button" class="btn btn-green btn-block btn-gd" id="guardar_reduccion_detalle">Guardar Adecuación</button>
                                <div id="resutado_insertar_detalle"></div>
                            </div>
                        </div>
                        <!-- Fin Tercer Parte Partida Inicio-->
                        <!--Tabla Detalle-->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-body table-gral">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover" id="tabla_datos_reduccion">
                                                <thead>
                                                <tr>
                                                    <th>Id</th>
                                                    <th width="13%">Gerencia</th>
                                                    <th width="13%">C. Recaudación</th>
                                                    <th width="7%">Rubro</th>
                                                    <th width="6%">Tipo</th>
                                                    <th width="8%">Clase</th>
                                                    <th>Importe</th>
                                                    <th width="13%">Fecha Aplicada</th>
                                                    <th>Descripción</th>
                                                    <th width="11%">Movimiento</th>
                                                    <th width="11%">Acciones</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Fin Tabla Detalle-->
                    </div>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-body text-center">
                <div class="form-group c-firme">
                    <h3>¿Adecuación en Firme?</h3>
                    <label class="checkbox-inline">
                        <input type="checkbox" id="check_firme" name="check_firme" <?= $enfirme == 1 ? ' checked="checked" disabled' : '';?> />Sí
                    </label>
                </div>
            </div>
        </div>

        <div class="btns-finales text-center">
            <a class="btn btn-default" href="<?= base_url("ingresos/adecuaciones_presupuestarias") ?>" ><i class="fa fa-reply ic-color"></i> Regresar</a>
            <a id="guardar_reduccion" class="btn btn-green">Guardar Adecuación</a>
        </div>
    </form>
</div>


<!-- Modal estructura superior-->
<div class="modal fade" id="modal_estructura_superior" tabindex="-1" role="dialog" aria-labelledby="modal_estructura_superior" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-indent-left"></span> Estructura de Ingresos</h4>
            </div>
            <div class="modal-body text-center">
                <?= $niveles_modal_superior ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" disabled id="elegir_ultimoNivel_superior">Elegir</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal estructura inferior-->
<div class="modal fade" id="modal_estructura_inferior" tabindex="-1" role="dialog" aria-labelledby="modal_estructura_inferior" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-indent-left"></span> Estructura de Ingresos</h4>
            </div>
            <div class="modal-body text-center">
                <?= $niveles_modal_inferior ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" disabled id="elegir_ultimoNivel_inferior">Elegir</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Borrar Detalle -->
<div class="modal fade modal_borrar" tabindex="-1" role="dialog" aria-labelledby="modal_borrar" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-trash ic-modal"></i> Eliminar Reducción</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="message-text" class="control-label">¿Realmente desea eliminar el movimiento seleccionado?</label>
                        <input type="hidden" value="" name="borrar_reduccion_hidden" id="borrar_reduccion_hidden" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_cancelar_reduccion">Aceptar</button>
            </div>
        </div>
    </div>
</div>

</div>
<!-- /#page-wrapper -->