    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <?php
                    if(isset($mensaje)){
                        echo($mensaje);
                    }
                    ?>
                </div>
                <!-- /.col-lg-12 -->
                <div class="col-lg-12">
                    <a class="btn btn-default" onclick="window.history.back();">Regresar</a>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->