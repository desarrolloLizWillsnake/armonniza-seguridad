<h3 class="page-header title center"><i class="fa fa-th"></i> Matriz de Conversión</h3>
<div id="page-wrapper">
    <!--Opción 2-->
    <!--<h3 class="page-header center"><i class="fa fa-file-o"></i> compromisos</h3>-->
    <div class="row cont-btns-c center">
        <div class="col-lg-12">
            <?php if($this->utilerias->get_permisos("agregar_matriz") || $this->utilerias->get_grupo() == 1){ ?>
                <!--<a href="<?= base_url("contabilidad/agregar_matriz")?>" class="btn btn-default"><i class="fa fa-plus-circle circle ic-color"></i> Agregar Matriz</a>-->
                <button type="button" class="btn btn-default" id="descargar_plantilla"><i class="fa fa-download ic-color"></i> Descargar Plantilla</button>
                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#subirArchivo" data-whatever="Subir"><i class="fa fa-upload ic-color"></i> Subir Archivo</button>
            <?php } ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <!--<div class="panel-heading">
                    compromisos
                </div>-->
                <div class="panel-body table-gral">
                    <div class="">
                        <table class="table table-striped table-bordered table-hover datos_tabla">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Partida</th>
                                <th>Descripción</th>
                                <th>Cuenta Cargo</th>
                                <th>Descripción</th>
                                <th>Cuenta Abono</th>
                                <th>Descripción</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="modal fade" id="subirArchivo" tabindex="-1" role="dialog" aria-labelledby="subirArchivo" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-th ic-modal"></i> Matriz de Conversión</h4>
            </div>
            <div class="modal-body">
                <?php
                $attributes = array(
                    'role' => 'form',
                );

                echo(form_open_multipart('contabilidad/subir_matriz', $attributes));
                ?>
                <div class="form-group center">
                    <div class="fileUpload btn btn-default btn-size">
                        <span>Selecciona Archivo</span>
                        <?php
                        $data = array(
                            'name'        => 'archivoSubir',
                            'id'          => 'archivoSubir',
                            'class'       => 'upload',
                            'accept' => 'application/vnd.ms-excel',
                            'required' => 'required',
                        );

                        echo(form_upload($data));
                        ?>
                    </div>
                    <p class="help-block">El archivo a subir debe de tener la extensión .XLS</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-reply ic-color"></i> Regresar</button>
                <?php
                $atributos_submit = array(
                    'class' => 'btn btn-green',
                    'required' => 'required',
                );

                echo(form_submit($atributos_submit, 'Subir Archivo'));
                ?>
                <?php echo(form_close()); ?>
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal_borrar" tabindex="-1" role="dialog" aria-labelledby="modal_borrar" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-remove ic-modal"></i> Borrar matriz</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="message-text" class="control-label">¿Realmente desea borrar la matriz seleccionada?</label>
                        <input type="hidden" value="" name="borrar_matriz" id="borrar_matriz" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_borrar_matriz">Aceptar</button>
            </div>
        </div>
    </div>
</div>

</div>
<!-- /.row -->
</div>
<!-- /#page-wrapper -->