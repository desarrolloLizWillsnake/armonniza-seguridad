<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?= base_url('img/favicon.ico') ?>">

    <title>{titulo_pagina}</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?= base_url("assets/templates/front/css/bootstrap.css") ?>" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?= base_url("assets/templates/front/css/plugins/metisMenu/metisMenu.min.css") ?>" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="<?= base_url("assets/templates/front/css/plugins/timeline.css") ?>" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?= base_url("assets/templates/front/css/sb-admin-2.css") ?>" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="<?= base_url("assets/templates/front/css/plugins/morris.css") ?>" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?= base_url("assets/templates/front/font-awesome-4.4.0/css/font-awesome.min.css") ?>" rel="stylesheet" type="text/css">

    <?php if(isset($tablas)) { ?>
    <!-- DataTables CSS -->
    <link href="<?= base_url("assets/templates/front/css/plugins/dataTables.bootstrap.css") ?>" rel="stylesheet">
    <?php } ?>

    <?php if(isset($exportar_tabla)) { ?>
    <!-- DataTables CSS -->
    <link href="<?= base_url("assets/datatables/extensions/TableTools/css/dataTables.tableTools.min.css") ?>" rel="stylesheet">
    <?php } ?>

    <?php if(isset($editar_tabla)) { ?>
    <!-- DataTables CSS -->
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/3.5.2/select2.min.css" rel="stylesheet">
    <link href="<?= base_url("assets/templates/front/css/plugins/dataTables.bootstrap.css") ?>" rel="stylesheet">
    <link href="<?= base_url("assets/datatables/extensions/TableTools/css/dataTables.tableTools.min.css") ?>" rel="stylesheet">
    <link href="<?= base_url("assets/datatables/extensions/Editor-1.3.3/css/dataTables.editor.min.css") ?>" rel="stylesheet">
    <?php } ?>

    <?php if(isset($precompromisocss)) { ?>
    <link rel="stylesheet" href="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.css") ?>" />
    <link rel="stylesheet" href="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.theme.css") ?>" />
    <link rel="stylesheet" href="<?= base_url("assets/templates/front/js/plugins/external/google-code-prettify/prettify.css") ?>" />
    <?php } ?>

    <?php if(isset($compromisocss)) { ?>
    <link rel="stylesheet" href="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.css") ?>" />
    <link rel="stylesheet" href="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.theme.css") ?>" />
    <?php } ?>

    <?php if(isset($contrarecibocss)) { ?>
    <link rel="stylesheet" href="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.css") ?>" />
    <link rel="stylesheet" href="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.theme.css") ?>" />
    <?php } ?>

    <?php if(isset($recaudacioncss)) { ?>
        <link rel="stylesheet" href="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.css") ?>" />
        <link rel="stylesheet" href="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.theme.css") ?>" />
    <?php } ?>

    <?php if(isset($reportescss)) { ?>
        <link rel="stylesheet" href="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.css") ?>" />
        <link rel="stylesheet" href="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.theme.css") ?>" />
    <?php } ?>

    <?php if(isset($usuariocss)) { ?>
        <link rel="stylesheet" href="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.min.css") ?>" />
        <link rel="stylesheet" href="<?= base_url("assets/templates/front/js/jquery-ui/jquery-ui.theme.css") ?>" />
    <?php } ?>

    <?php if(isset($nuevas_tablas)) { ?>
        <link href="<?= base_url("assets/nuevo_datatables/media/css/jquery.dataTables.min.css") ?>" rel="stylesheet" />
    <?php } ?>

    <?php if(isset($nuevas_tablas_autofill)) { ?>
        <link href="<?= base_url("assets/nuevo_datatables/extensions/AutoFill/css/autofill.dataTables.min.css") ?>" rel="stylesheet" />
        <link href="<?= base_url("assets/nuevo_datatables/extensions/AutoFill/css/autofill.bootstrap.min.css") ?>" rel="stylesheet" />
        <link href="<?= base_url("assets/nuevo_datatables/extensions/AutoFill/css/autofill.jqueryui.min.css") ?>" rel="stylesheet" />
    <?php } ?>

    <?php if(isset($nuevas_tablas_buttons)) { ?>
        <link href="<?= base_url("assets/nuevo_datatables/extensions/Buttons/css/buttons.dataTables.min.css") ?>" rel="stylesheet" />
    <?php } ?>

    <?php if(isset($nuevas_tablas_colreorder)) { ?>
        <link href="<?= base_url("assets/nuevo_datatables/extensions/ColReorder/css/colReorder.dataTables.min.css") ?>" rel="stylesheet" />
        <link href="<?= base_url("assets/nuevo_datatables/extensions/ColReorder/css/colReorder.bootstrap.min.css") ?>" rel="stylesheet" />
        <link href="<?= base_url("assets/nuevo_datatables/extensions/ColReorder/css/colReorder.jqueryui.min.css") ?>" rel="stylesheet" />
    <?php } ?>

    <?php if(isset($nuevas_tablas_fixedcolumns)) { ?>
        <link href="<?= base_url("assets/nuevo_datatables/extensions/FixedColumns/css/fixedColumns.dataTables.min.css") ?>" rel="stylesheet" />
        <link href="<?= base_url("assets/nuevo_datatables/extensions/FixedColumns/css/fixedColumns.bootstrap.min.css") ?>" rel="stylesheet" />
        <link href="<?= base_url("assets/nuevo_datatables/extensions/FixedColumns/css/fixedColumns.jqueryui.min.css") ?>" rel="stylesheet" />
    <?php } ?>

    <?php if(isset($nuevas_tablas_fixedheader)) { ?>
        <link href="<?= base_url("assets/nuevo_datatables/extensions/FixedHeader/css/fixedHeader.dataTables.min.css") ?>" rel="stylesheet" />
        <link href="<?= base_url("assets/nuevo_datatables/extensions/FixedHeader/css/fixedHeader.bootstrap.min.css") ?>" rel="stylesheet" />
        <link href="<?= base_url("assets/nuevo_datatables/extensions/FixedHeader/css/fixedHeader.jqueryui.min.css") ?>" rel="stylesheet" />
    <?php } ?>

    <?php if(isset($nuevas_tablas_keytable)) { ?>
        <link href="<?= base_url("assets/nuevo_datatables/extensions/KeyTable/css/keyTable.dataTables.min.css") ?>" rel="stylesheet" />
    <?php } ?>

    <?php if(isset($nuevas_tablas_responsive)) { ?>
        <link href="<?= base_url("assets/nuevo_datatables/extensions/Responsive/css/responsive.dataTables.min.css") ?>" rel="stylesheet" />
        <link href="<?= base_url("assets/nuevo_datatables/extensions/Responsive/css/responsive.bootstrap.min.css") ?>" rel="stylesheet" />
        <link href="<?= base_url("assets/nuevo_datatables/extensions/Responsive/css/responsive.jqueryui.min.css") ?>" rel="stylesheet" />
    <?php } ?>

    <?php if(isset($nuevas_tablas_rowreorder)) { ?>
        <link href="<?= base_url("assets/nuevo_datatables/extensions/RowReorder/css/rowreorder.dataTables.min.css") ?>" rel="stylesheet" />
        <link href="<?= base_url("assets/nuevo_datatables/extensions/RowReorder/css/rowreorder.bootstrap.min.css") ?>" rel="stylesheet" />
        <link href="<?= base_url("assets/nuevo_datatables/extensions/RowReorder/css/rowreorder.jqueryui.min.css") ?>" rel="stylesheet" />
    <?php } ?>

    <?php if(isset($nuevas_tablas_scroller)) { ?>
        <link href="<?= base_url("assets/nuevo_datatables/extensions/Scroller/css/scroller.dataTables.min.css") ?>" rel="stylesheet" />
        <link href="<?= base_url("assets/nuevo_datatables/extensions/Scroller/css/scroller.bootstrap.min.css") ?>" rel="stylesheet" />
        <link href="<?= base_url("assets/nuevo_datatables/extensions/Scroller/css/scroller.jqueryui.min.css") ?>" rel="stylesheet" />
    <?php } ?>

    <?php if(isset($nuevas_tablas_select)) { ?>
        <link href="<?= base_url("assets/nuevo_datatables/extensions/Select/css/select.dataTables.min.css") ?>" rel="stylesheet" />
    <?php } ?>

    <?php if(isset($nuevas_tablas_editor)) { ?>
        <link href="<?= base_url("assets/nuevo_datatables/extensions/Editor/css/editor.dataTables.min.css") ?>" rel="stylesheet" />
    <?php } ?>

    <style>
        .editor {overflow:scroll; max-height:300px; min-height: 300px;}
    </style>



    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript">
        var js_site_url = function( urlText ){
            var urlTmp = "<?= site_url('" + urlText + "'); ?>";
            return urlTmp;
        };
        var js_base_url = function( urlText ){
            var urlTmp = "<?= base_url('" + urlText + "'); ?>";
            return urlTmp;
        };
    </script>

</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0;">
    <div class="navbar-header" style="width: 70%;">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="<?= base_url() ?>">
            <img class="logo1" src="<?= base_url('img/logo.png') ?>">
        </a>
    </div>
    <!-- /.navbar-header -->

    <ul class="nav navbar-top-links navbar-right">
    <li class="dropdown">
        <!--<a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <i class="fa fa-envelope fa-fw"></i> Mensajes <i class="fa fa-caret-down"></i>
        </a>-->
        <ul class="dropdown-menu dropdown-messages">
            <li>
                <a href="#">
                    <div>
                        <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                    </div>
                    <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                </a>
            </li>
            <li class="divider"></li>
            <li>
                <a href="#">
                    <div>
                        <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                    </div>
                    <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                </a>
            </li>
            <li class="divider"></li>
            <li>
                <a href="#">
                    <div>
                        <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                    </div>
                    <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                </a>
            </li>
            <li class="divider"></li>
            <li>
                <a class="text-center" href="#">
                    <strong>Leer más mensajes</strong>
                    <i class="fa fa-angle-right"></i>
                </a>
            </li>
        </ul>
        <!-- /.dropdown-messages -->
    </li>
    <li class="dropdown cont-user">
        <a class="dropdown-toggle right" data-toggle="dropdown" href="#">
            <i class="fa fa-user fa-fw"></i> {usuario} <i class="fa fa-caret-down"></i>
        </a>
        <ul class="dropdown-menu dropdown-user">
            <li><a href="<?= base_url("usuario") ?>"><i class="fa fa-user fa-fw"></i> Perfil </a></li>
            <?php if($this->utilerias->get_grupo() == 1) { ?>
                <li><a href="<?= base_url("administrador/perfil") ?>"><i class="fa fa-laptop"></i> Panel Administrador </a></li>
            <?php } ?>
            <li class="divider"></li>
            <li><a href="<?= base_url("auth/logout") ?>"><i class="fa fa-sign-out fa-fw"></i> Cerrar Sesión </a></li>
        </ul>
        <!-- /.dropdown-user -->
    </li>
    <!-- /.dropdown -->
    </ul>
    <!-- /.navbar-top-links -->
    <!-- Inicio Menú Lateral   -->
    <div class="navbar-default sidebar nav-side" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                <?php if($this->utilerias->get_permisos("estructuras") || $this->utilerias->get_grupo() == 1) { ?>
                <li class="first" <?php if( strpos($this->uri->uri_string(), "ingresos") !== false || strpos($this->uri->uri_string(), "egresos") !== false ){ ?> class="active" <?php }; ?>>
                        <a href="<?= base_url("administrativos") ?>"><i class="fa fa-bar-chart fa-fw"></i> Estructuras </br><span style="padding-left: 18%;">Administrativas</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <?php if($this->utilerias->get_permisos("ingresos_maestro") || $this->utilerias->get_grupo() == 1) { ?>


                                <li <?php if( strpos($this->uri->uri_string(), "ingresos") !== false){ ?> class="active" <?php }; ?>>
                                    <a href="<?= base_url("ingresos") ?>"><i class="fa fa-sort-amount-asc fa-fw"></i> Ingresos<span class="fa arrow"></span></a>
                                    <ul class="nav nav-second-level">
                                        <?php if($this->utilerias->get_permisos("ingresos_estructura") || $this->utilerias->get_grupo() == 1) { ?>
                                        <li><a <?php if( strpos($this->uri->uri_string(), "ingresos/estructura_rubros") !== false ){ ?> class="active" <?php }; ?> href="<?= base_url("ingresos/estructura_rubros") ?>">Estructura Administrativa</a></li>
                                        <?php } ?>
                                        <?php if($this->utilerias->get_permisos("ingresos_adecuaciones") || $this->utilerias->get_grupo() == 1) { ?>
                                        <li><a <?php if(strpos($this->uri->uri_string(), "ingresos/adecuaciones_presupuestarias") !== false || strpos($this->uri->uri_string(), "egresos/agregar_ampliacion") !== false || strpos($this->uri->uri_string(), "egresos/agregar_reduccion") !== false || strpos($this->uri->uri_string(), "egresos/agregar_transferencia") !== false || strpos($this->uri->uri_string(), "egresos/editar_ampliacion") !== false || strpos($this->uri->uri_string(), "egresos/editar_reduccion") !== false || strpos($this->uri->uri_string(), "egresos/editar_transferencia") !== false || strpos($this->uri->uri_string(), "egresos/ver_ampliacion") !== false || strpos($this->uri->uri_string(), "egresos/ver_reduccion") !== false || strpos($this->uri->uri_string(), "egresos/ver_transferencia") !== false){ ?> class="active" <?php }; ?> href="<?= base_url("ingresos/adecuaciones_presupuestarias") ?>">Adecuaciones Presupuestarias</a></li>
                                        <?php } ?>
                                        <?php if($this->utilerias->get_permisos("ingresos_consulta_estructura") || $this->utilerias->get_grupo() == 1) { ?>
                                        <li><a <?php if( strpos($this->uri->uri_string(), "ingresos/consulta_presupuesto") !== false ){ ?> class="active" <?php }; ?> href="<?= base_url("ingresos/consulta_presupuesto") ?>">Consulta Partida</a></li>
                                        <?php } ?>
                                    </ul>
                                    <!-- /.nav-second-level -->
                                </li>
                            <?php } ?>
                            <?php if($this->utilerias->get_permisos("egresos_maestro") || $this->utilerias->get_grupo() == 1) { ?>
                                <li <?php if( strpos($this->uri->uri_string(), "egresos") !== false ){ ?> class="active" <?php }; ?>>
                                    <a href="#"><i class="fa fa-sort-amount-desc fa-fw"></i> Egresos<span class="fa arrow"></span></a>
                                    <ul class="nav nav-second-level">
                                        <?php if($this->utilerias->get_permisos("egresos_estructura") || $this->utilerias->get_grupo() == 1) { ?>
                                        <li><a <?php if( strpos($this->uri->uri_string(), "egresos/estructura_rubros") !== false ){ ?> class="active" <?php }; ?> href="<?= base_url("egresos/estructura_rubros") ?>">Estructura Administrativa</a></li>
                                        <?php } ?>
                                        <?php if($this->utilerias->get_permisos("egresos_adecuaciones") || $this->utilerias->get_grupo() == 1) { ?>
                                        <li><a <?php if( $this->uri->uri_string() == 'egresos/adecuaciones_presupuestarias' ){ ?> class="active" <?php }; ?> href="<?= base_url("egresos/adecuaciones_presupuestarias") ?>">Adecuaciones Presupuestarias</a></li>
                                        <?php } ?>
                                        <?php if($this->utilerias->get_permisos("egresos_consulta_estructura") || $this->utilerias->get_grupo() == 1) { ?>
                                        <li><a <?php if( strpos($this->uri->uri_string(), "egresos/consulta_presupuesto") !== false ){ ?> class="active" <?php }; ?> href="<?= base_url("egresos/consulta_presupuesto") ?>">Consulta Partida</a></li>
                                        <?php } ?>
                                    </ul>
                                    <!-- /.nav-second-level -->
                                </li>
                            <?php } ?>
                        </ul>
                        <!-- /.nav-second-level -->
                </li>
                <?php } ?>
                <?php if($this->utilerias->get_permisos("ciclo_maestro") || $this->utilerias->get_grupo() == 1) { ?>
                <li <?php if( strpos($this->uri->uri_string(), "ciclo") !== false ){ ?> class="active" <?php }; ?>>
                        <a href="#"><i class="fa fa-history fa-fw"></i> Ciclo Administrativo<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <?php if($this->utilerias->get_permisos("precompromiso") || $this->utilerias->get_grupo() == 1) { ?>
                                <li><a <?php if( strpos($this->uri->uri_string(), "ciclo/precompromiso") !== false || strpos($this->uri->uri_string(), "ciclo/ver_precompromiso") !== false || strpos($this->uri->uri_string(), "ciclo/agregar_precompromiso") !== false || strpos($this->uri->uri_string(), "ciclo/editar_precompromiso") !== false){ ?> class="active" <?php }; ?> href="<?= base_url("ciclo/precompromiso") ?>">Pre Compromisos</a></li>
                            <?php } ?>
                            <?php if($this->utilerias->get_permisos("compromiso") || $this->utilerias->get_grupo() == 1) { ?>
                                <li><a <?php if( strpos($this->uri->uri_string(), "ciclo/compromiso") !== false || strpos($this->uri->uri_string(), "ciclo/ver_compromiso") !== false || strpos($this->uri->uri_string(), "ciclo/agregar_compromiso") !== false || strpos($this->uri->uri_string(), "ciclo/editar_compromiso") !== false){ ?> class="active" <?php }; ?> href="<?= base_url("ciclo/compromiso") ?>">Compromisos</a></li>
                            <?php } ?>
                            <?php if($this->utilerias->get_permisos("contrarecibo") || $this->utilerias->get_grupo() == 1) { ?>
                                <li><a <?php if( strpos($this->uri->uri_string(), "ciclo/contrarecibos") !== false || strpos($this->uri->uri_string(), "ciclo/ver_contrarecibo") !== false || strpos($this->uri->uri_string(), "ciclo/agregar_contrarecibo") !== false || strpos($this->uri->uri_string(), "ciclo/editar_contrarecibo") !== false){ ?> class="active" <?php }; ?> href="<?= base_url("ciclo/contrarecibos") ?>">Contra Recibos de Pago</a></li>
                            <?php } ?>
                            <?php if($this->utilerias->get_permisos("tesoreria") || $this->utilerias->get_grupo() == 1) { ?>
                                <li>
                                    <a <?php if( strpos($this->uri->uri_string(), "ciclo/movbancarios") !== false || strpos($this->uri->uri_string(), "ciclo/conciliacion") !== false || strpos($this->uri->uri_string(), "ciclo/cuenta_bancaria") !== false || strpos($this->uri->uri_string(), "ciclo/agregar_movimiento") !== false || strpos($this->uri->uri_string(), "ciclo/editar_movimiento") !== false || strpos($this->uri->uri_string(), "ciclo/ver_movimiento") !== false){ ?> class="active" <?php }; ?> href="<?= base_url("") ?>">Tesorería <span class="fa arrow" style="padding-right: 4%;"></span></a>
                                    <ul class="nav nav-second-level">
                                        <?php if($this->utilerias->get_permisos("movimientos_bancarios") || $this->utilerias->get_grupo() == 1) { ?>
                                            <li><a <?php if( strpos($this->uri->uri_string(), "ciclo/movbancarios") !== false || strpos($this->uri->uri_string(), "ciclo/agregar_movimiento") !== false || strpos($this->uri->uri_string(), "ciclo/editar_movimiento") !== false || strpos($this->uri->uri_string(), "ciclo/ver_movimiento") !== false ){ ?> class="active" <?php }; ?> href="<?= base_url("ciclo/movbancarios") ?>">Movimientos Bancarios</a></li>
                                        <?php } ?>
                                        <?php if($this->utilerias->get_permisos("conciliacion_bancaria") || $this->utilerias->get_grupo() == 1) { ?>
                                            <li><a <?php if( strpos($this->uri->uri_string(), "ciclo/conciliacion") !== false ){ ?> class="active" <?php }; ?> href="<?= base_url("ciclo/conciliacion") ?>">Conciliación Bancaria</a></li>
                                        <?php } ?>

                                    </ul>
                                </li>
                            <?php } ?>
                        </ul>
                        <!-- /.nav-second-level -->
                </li>
                <?php } ?>
                <?php if($this->utilerias->get_permisos("recaudacion") || $this->utilerias->get_grupo() == 1) { ?>
                    <li <?php if( strpos($this->uri->uri_string(), "recaudacion") !== false ){ ?> class="active" <?php }; ?>>
                        <a href="#"><i class="fa fa-shopping-cart"></i> Recaudación<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <?php if($this->utilerias->get_permisos("devengado") || $this->utilerias->get_grupo() == 1) { ?>
                                <li><a <?php if( $this->uri->uri_string() == 'recaudacion/devengado' ){ ?> class="active" <?php }; ?> href="<?= base_url("recaudacion/devengado") ?>"> Devengado</a></li>
                            <?php } ?>
                            <?php if($this->utilerias->get_permisos("recaudado") || $this->utilerias->get_grupo() == 1) { ?>
                                <li><a <?php if( $this->uri->uri_string() == 'recaudacion/recaudado' ){ ?> class="active" <?php }; ?> href="<?= base_url("recaudacion/recaudado") ?>"> Recaudado</a></li>
                            <?php } ?>
                        </ul>
                    </li>
                <?php } ?>
                <?php if($this->utilerias->get_permisos("contabilidad") || $this->utilerias->get_grupo() == 1) { ?>
                    <li <?php if( strpos($this->uri->uri_string(), "contabilidad") !== false ){ ?> class="active" <?php }; ?>>
                        <a href="#"><i class="fa fa-dollar fa-fw"></i> Contabilidad<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <?php if($this->utilerias->get_permisos("asientos_polizas") || $this->utilerias->get_grupo() == 1) { ?>
                            <li><a <?php if( $this->uri->uri_string() == 'contabilidad/polizas' ){ ?> class="active" <?php }; ?> href="<?= base_url("contabilidad/polizas") ?>"> Asientos / Pólizas</a></li>
                            <?php } ?>
                            <?php if($this->utilerias->get_permisos("consulta_balanza") || $this->utilerias->get_grupo() == 1) { ?>
                            <li><a <?php if( $this->uri->uri_string() == 'contabilidad/balanza' ){ ?> class="active" <?php }; ?> href="<?= base_url("contabilidad/balanza") ?>"> Consulta Balanza</a></li>
                            <?php } ?>
                            <?php if($this->utilerias->get_permisos("auxiliar") || $this->utilerias->get_grupo() == 1) { ?>
                                <li><a <?php if( $this->uri->uri_string() == 'contabilidad/auxiliar' ){ ?> class="active" <?php }; ?> href="<?= base_url("contabilidad/auxiliar") ?>"> Auxiliar</a></li>
                            <?php } ?>
                            <?php if($this->utilerias->get_permisos("matriz_conversion") || $this->utilerias->get_grupo() == 1) { ?>
                            <li><a <?php if( strpos($this->uri->uri_string(),'matriz') ){ ?> class="active" <?php }; ?> href="<?= base_url("contabilidad/matriz") ?>"> Matriz de Conversión</a></li>
                            <?php } ?>
                            <?php if($this->utilerias->get_permisos("plan_cuentas") || $this->utilerias->get_grupo() == 1) { ?>
                                <li><a <?php if( $this->uri->uri_string() == 'contabilidad/plancuentas' ){ ?> class="active" <?php }; ?> href="<?= base_url("contabilidad/plancuentas") ?>"> Plan de Cuentas</a></li>
                            <?php } ?>
                            <?php if($this->utilerias->get_permisos("periodos_contables") || $this->utilerias->get_grupo() == 1) { ?>
                                <li><a <?php if( $this->uri->uri_string() == 'contabilidad/periodoscontables' ){ ?> class="active" <?php }; ?> href="<?= base_url("contabilidad/periodoscontables") ?>">Períodos Contables</a></li>
                            <?php } ?>
                            <!--
                            <?php if($this->utilerias->get_permisos("contabilidad_electronica") || $this->utilerias->get_grupo() == 1) { ?>
                            <li><a <?php if( $this->uri->uri_string() == 'contabilidad/contaelectronica' ){ ?> class="active" <?php }; ?> href="<?= base_url("contabilidad/contaelectronica") ?>"> Contabilidad Electrónica</a></li>
                            <?php } ?>
                            -->
                         </ul>
                        <!-- /.nav-second-level -->
                    </li>
                <?php } ?>

                <?php if($this->utilerias->get_permisos("patrimonio") || $this->utilerias->get_grupo() == 1) { ?>
                    <li <?php if( strpos($this->uri->uri_string(), "patrimonio") !== false ){ ?> class="active" <?php }; ?>>
                        <a href="#"><i class="fa fa-tasks fa-fw"></i> Patrimonio<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <?php if($this->utilerias->get_permisos("nota_entrada") || $this->utilerias->get_grupo() == 1) { ?>
                                <li><a <?php if( $this->uri->uri_string() == 'patrimonio/nota_entrada' ){ ?> class="active" <?php }; ?> href="<?= base_url("patrimonio/nota_entrada") ?>"> Nota de Entrada</a></li>
                            <?php } ?>
                            <?php if($this->utilerias->get_permisos("nota_salida") || $this->utilerias->get_grupo() == 1) { ?>
                                <li><a <?php if( $this->uri->uri_string() == 'patrimonio/nota_salida' ){ ?> class="active" <?php }; ?> href="<?= base_url("patrimonio/nota_salida") ?>"> Nota de Salida</a></li>
                            <?php } ?>
                            <?php if($this->utilerias->get_permisos("kardex") || $this->utilerias->get_grupo() == 1) { ?>
                                <li><a <?php if( $this->uri->uri_string() == 'patrimonio/kardex' ){ ?> class="active" <?php }; ?> href="<?= base_url("patrimonio/kardex") ?>"> Kardex de Existencias</a></li>
                            <?php } ?>
                            <?php if($this->utilerias->get_permisos("inventarios") || $this->utilerias->get_grupo() == 1) { ?>
                                <li><a <?php if( $this->uri->uri_string() == 'patrimonio/inventarios' ){ ?> class="active" <?php }; ?> href="<?= base_url("patrimonio/inventarios") ?>"> Consulta Inventarios</a></li>
                            <?php } ?>
                            <?php if($this->utilerias->get_permisos("clasificador_productos") || $this->utilerias->get_grupo() == 1) { ?>
                                <li><a <?php if( $this->uri->uri_string() == 'patrimonio/catalogo_inventarios' ){ ?> class="active" <?php }; ?> href="<?= base_url("patrimonio/catalogo_inventarios") ?>"> Clasificador de Productos</a></li>
                            <?php } ?>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                <?php } ?>

                <?php if($this->utilerias->get_permisos("almacenes") || $this->utilerias->get_grupo() == 1) { ?>
                    <li <?php if( strpos($this->uri->uri_string(), "almacenes") !== false ){ ?> class="active" <?php }; ?>>
                        <a <?php if( $this->uri->uri_string() == 'almacenes/indicealmacenes' ){ ?> class="active" <?php }; ?> href=""><i class="fa fa-list-alt fa-fw"></i> Almacenes<span class="fa arrow"></span></a>
                    </li>
                <?php } ?>

                <?php if($this->utilerias->get_permisos("nomina") || $this->utilerias->get_grupo() == 1) { ?>

                <li <?php if( strpos($this->uri->uri_string(), "nomina") !== false ){ ?> class="active" <?php }; ?>>
                    <a href="#"><i class="fa fa-users"></i> Recursos Humanos<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <?php if($this->utilerias->get_permisos("empleados") || $this->utilerias->get_grupo() == 1) { ?>
                            <li><a <?php if( $this->uri->uri_string() == 'nomina/empleados' ){ ?> class="active" <?php }; ?> href="<?= base_url("nomina/empleados") ?>">Empleados</a></li>
                        <?php } ?>
                    </ul>
                    <ul class="nav nav-second-level">
                        <?php if($this->utilerias->get_permisos("nominas") || $this->utilerias->get_grupo() == 1) { ?>
                            <li><a <?php if( $this->uri->uri_string() == 'nomina/nominas' ){ ?> class="active" <?php }; ?> href="<?= base_url("nomina/nominas") ?>">N&oacute;mina</a></li>
                        <?php } ?>
                    </ul>
                    <ul class="nav nav-second-level">
                        <?php if($this->utilerias->get_permisos("lista_empleados") || $this->utilerias->get_grupo() == 1) { ?>
                            <li><a <?php if( $this->uri->uri_string() == 'nomina/consulta_empleados' ){ ?> class="active" <?php }; ?> href="<?= base_url("nomina/consulta_empleados") ?>">Consulta Empleados</a></li>
                        <?php } ?>
                    </ul>
                    <ul class="nav nav-second-level">
                        <?php if($this->utilerias->get_permisos("lista_nomina") || $this->utilerias->get_grupo() == 1) { ?>
                            <li><a <?php if( $this->uri->uri_string() == 'nomina/consulta_nomina' ){ ?> class="active" <?php }; ?> href="<?= base_url("nomina/consulta_nomina") ?>">Consulta N&oacute;minas</a></li>
                        <?php } ?>
                    </ul>
                    <!--<ul class="nav nav-second-level">
                            <?php if($this->utilerias->get_permisos("consulta_nomina") || $this->utilerias->get_grupo() == 1) { ?>
                                <li><a <?php if( $this->uri->uri_string() == 'nomina/consulta_nomina' ){ ?> class="active" <?php }; ?> href="<?= base_url("nomina/consulta_nomina") ?>">Consultas</a></li>
                            <?php } ?>
                        </ul>-->
                    </li>
                <?php } ?>

                <?php if($this->utilerias->get_permisos("anteproyecto") || $this->utilerias->get_grupo() == 1) { ?>
                    <li <?php if( strpos($this->uri->uri_string(), "anteproyecto") !== false ){ ?> class="active" <?php }; ?>>
                        <a href="#"><i class="fa fa-history fa-fw"></i> Anteproyecto<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <!--                            --><?php //if($this->utilerias->get_permisos("catalogo_partidas") || $this->utilerias->get_grupo() == 1) { ?>
                            <!--                                <li><a --><?php //if( strpos($this->uri->uri_string(), "anteproyecto/catalogo_partidas") !== false ){ ?><!-- class="active" --><?php //}; ?><!-- href="--><?//= base_url("anteproyecto/catalogo_partidas") ?><!--">Catálogo Partidas</a></li>-->
                            <!--                            --><?php //} ?>
                            <!--                            --><?php //if($this->utilerias->get_permisos("catalogo_usuarios_anteproyecto") || $this->utilerias->get_grupo() == 1) { ?>
                            <!--                                <li><a --><?php //if( strpos($this->uri->uri_string(), "anteproyecto/catalogo_usuarios") !== false ){ ?><!-- class="active" --><?php //}; ?><!-- href="--><?//= base_url("anteproyecto/catalogo_usuarios") ?><!--">Catálogo Centro de Costos</a></li>-->
                            <!--                            --><?php //} ?>

                            <?php if($this->utilerias->get_permisos("presupuesto_aprobado") || $this->utilerias->get_grupo() == 1) { ?>
                                <li><a <?php if( strpos($this->uri->uri_string(), "anteproyecto/presupuesto_aprobado") !== false ){ ?> class="active" <?php }; ?> href="<?= base_url("anteproyecto/presupuesto_aprobado") ?>">Anteproyecto 2016</a></li>
                            <?php } ?>
                            <!--                            --><?php // if($this->utilerias->get_permisos("indicadores_anteproyecto") || $this->utilerias->get_grupo() == 1) { ?>
                            <!--                                   <li><a --><?php //if( strpos($this->uri->uri_string(), "anteproyecto/indicadores") !== false ){ ?><!-- class="active" --><?php //}; ?><!--href="--><?//= base_url("anteproyecto/indicadores") ?><!--">Indicadores</a></li>-->
                            <!--                            --><?php //} ?>
                            <?php if($this->utilerias->get_permisos("servicios_subrogados_anteproyecto") || $this->utilerias->get_grupo() == 1) { ?>
                                <li><a <?php if( strpos($this->uri->uri_string(), "anteproyecto/servicios_subrogados") !== false ){ ?> class="active" <?php }; ?> href="<?= base_url("anteproyecto/servicios_subrogados") ?>">Servicios Subrogados</a></li>
                            <?php } ?>
                            <?php if($this->utilerias->get_permisos("catalogos_anteproyecto") || $this->utilerias->get_grupo() == 1) { ?>
                                <li><a <?php if( strpos($this->uri->uri_string(), "anteproyecto/catalogos_anteproyecto") !== false ){ ?> class="active" <?php }; ?> href="<?= base_url("anteproyecto/catalogos_anteproyecto") ?>">Catalogos</a></li>
                            <?php } ?>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                <?php } ?>

                <?php if($this->utilerias->get_permisos("reportes") || $this->utilerias->get_grupo() == 1) { ?>
                    <li <?php if( strpos($this->uri->uri_string(), "reportes") !== false ){ ?> class="active" <?php }; ?>>
                        <a <?php if( $this->uri->uri_string() == 'reportes/menureportes' ){ ?> class="active" <?php }; ?> href="<?= base_url("reportes/menureportes") ?>"><i class="fa fa-files-o fa-fw"></i> Reportes<span class="fa arrow"></span></a>
                    </li>
                <?php } ?>
                <?php if($this->utilerias->get_permisos("catalogos") || $this->utilerias->get_grupo() == 1) { ?>
                    <li <?php if( strpos($this->uri->uri_string(), "catalogos") !== false ){ ?> class="active" <?php }; ?>>
                        <a <?php if( $this->uri->uri_string() == 'catalogos/indicecatalogos' ){ ?> class="active" <?php }; ?> href="<?= base_url("catalogos/indicecatalogos") ?>"><i class="fa fa-cubes fa-fw"></i> Catálogos<span class="fa arrow"></span></a>
                    </li>
                <?php } ?>

                <?php if($this->utilerias->get_permisos("politicas_manuales") || $this->utilerias->get_grupo() == 1) { ?>
                    <li <?php if( strpos($this->uri->uri_string(), "manuales") !== false ){ ?> class="active" <?php }; ?>>
                        <a <?php if( $this->uri->uri_string() == 'manuales/manualespoliticas' ){ ?> class="active" <?php }; ?> href="<?= base_url("manuales/manualespoliticas") ?>"><i class="fa fa-bookmark fa-fw"></i> Guías de Usuario<span class="fa arrow"></span></a>
                    </li>
                <?php } ?>

            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
    </nav>


    <!-- Fin de la sección del menú   -->