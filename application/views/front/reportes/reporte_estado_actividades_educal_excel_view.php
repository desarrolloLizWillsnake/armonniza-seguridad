<?php
$this->db->select('*')->from('cat_polizas_firmas');
$query = $this->db->get();
$resultados = $query->result();
?>
<h3 class="page-header title center"><i class="fa fa-files-o"></i> Estado de Actividades</h3>
<div id="page-wrapper">
    <div class="row center">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <div class="list-group error-completar">
                <?php if(isset($mensaje)) { ?>
                    <div class="alert alert-danger">
                        <?= $mensaje ?>
                    </div>
                    <div class="text-center">
                        <div class="btns-finales">
                            <a class="btn btn-default" href="<?= base_url("reportes/reportesContabilidad") ?>"><i class="fa fa-reply ic-color"></i> Regresar</a>
                        </div>
                    </div>
                <?php } else { ?>
                    <form class="" action="<?= base_url("reportes/exportar_reporte_estadoActividadesEducal") ?>" method="POST" id="datos_impresion" role="form">
                        <div class="row add-pre error-gral">
                            <div class="col-lg-12">
                                <div class="panel panel-default" style="margin: 0 auto; margin-top: 2%; width: 80%;">
                                    <div class="panel-body">
                                        <!--Tipo de Consulta-->
                                        <label style="margin-top: 1%;">Seleccione el tipo de consulta</label>
                                        <br>
                                        <div class="row">
                                            <div class="col-lg-6" >
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="opciones" id="acumulativa" value="acumulativa" checked>Acumulativo
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="opciones" id="mensual" value="mensual">Mensual
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <!--Rango Fechas-->
                                        <div class="row" id="consulta_acumulativa" name="consulta_acumulativa">
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control ic-calendar" name="fecha_inicial" id="fecha_inicial" placeholder="Fecha Inicial" >
                                            </div>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control ic-calendar" name="fecha_final" id="fecha_final" placeholder="Fecha Final" >
                                            </div>
                                        </div>
                                        <!--Rango Mes-->
                                        <div class="row" id="consulta_mensual" name="consulta_mensual" style="display: none;">
                                            <div class="col-lg-12">
                                                <select class="form-control" id="mes" name="mes">
                                                    <option value="1">Enero</option>
                                                    <option value="2">Febrero</option>
                                                    <option value="3">Marzo</option>
                                                    <option value="4">Abril</option>
                                                    <option value="5">Mayo</option>
                                                    <option value="6">Junio</option>
                                                    <option value="7">Julio</option>
                                                    <option value="8">Agosto</option>
                                                    <option value="9">Septiembre</option>
                                                    <option value="10">Octubre</option>
                                                    <option value="11">Noviembre</option>
                                                    <option value="12">Diciembre</option>
                                                </select>
                                            </div>
                                        </div>
                                        <!--Elabor�-->
                                        <div class="form-group">
                                            <label class="label_sig center">Elabor&oacute;</label>
                                            <select class="form-control" name="persona_elaboro">
                                                <?php
                                                foreach($resultados as $row) { ?>
                                                    <option value="<?= $row->id_persona ?>"><?= $row->grado_estudio ?> <?= $row->nombre ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>

                                        <div class="btns-finales text-center">
                                            <a class="btn btn-default" href="<?= base_url("reportes/reportesContabilidad") ?>"><i class="fa fa-reply" style="color: #B6CE33;"></i> Regresar</a>
                                            <input class="btn btn-green" type="submit" value="Continuar"/>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                <?php } ?>
            </div>
        </div>
    </div>
</div>