<h3 class="page-header title center"><i class="fa fa-files-o"></i> Consulta de Proveedores</h3>
<div id="page-wrapper">
    <form action="<?= base_url("reportes/imprimir_reporte_consultaProveedores") ?>" method="POST" id="datos_impresion" role="form">
        <div class="row add-pre error-gral">
            <div class="col-lg-12">
                <div class="panel panel-default" style="margin: 0 auto; margin-top: 2%; width: 50%;">
                    <div class="panel-body">
                        <!--Tipo de Consulta-->
                        <label style="margin-top: 1%;">Seleccione el tipo de consulta</label>
                        <div class="row">
                            <div class="col-lg-6" >
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="opciones" id="general" value="gral" checked>General
                                    </label>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="opciones" id="especifica" value="espec">Específica
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="margin-top: 1%;">
                            <div class="col-lg-6">
                                <input type="text" class="form-control ic-calendar" name="fecha_inicial" id="fecha_inicial" placeholder="Fecha Inicial" >
                            </div>
                            <div class="col-lg-6">
                                <input type="text" class="form-control ic-calendar" name="fecha_final" id="fecha_final" placeholder="Fecha Final" >
                            </div>
                        </div>


                        <div class="row niveles-pc" id="consulta_especifica" name="consulta_especifica" style="margin-top: 3%; display: none;">
                            <div class="col-lg-12">
                                <div class="form-group input-group">
                                    <input type="hidden" name="id_proveedor" id="id_proveedor" value="">
                                    <input type="text" class="form-control" name="proveedor" id="proveedor" placeholder="Proveedor" disabled="">
                                    <span class="input-group-btn ic-buscar-btn">
                                        <button class="btn btn-default b-espec" type="button" data-toggle="modal" data-target="#modal_proveedores"><i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="btns-finales text-center">
                            <a class="btn btn-default" href="<?= base_url("reportes/reportesAdministracion") ?>"><i class="fa fa-reply" style="color: #B6CE33;"></i> Regresar</a>
                            <input class="btn btn-green" type="submit" id="consultar_reporte" value="Continuar" />
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<!-- Modal Proveedores -->
<div class="modal fade" id="modal_proveedores" tabindex="-1" role="dialog" aria-labelledby="modal_proveedores" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-users ic-modal"></i> Proveedores</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-prove">
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-4 center">
                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal_proveedores_armonniza">Armonniza</button>
                    </div>
                    <div class="col-md-4 center">
                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal_proveedores_educal">EDUCAL</button>
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_proveedores_armonniza" tabindex="-1" role="dialog" aria-labelledby="modal_proveedores_armonniza" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-users ic-modal"></i> Proveedores Armonniza</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-3">
                <div class="table-responsive">
                    <input type="hidden" name="hidden_clave_proveedor" id="hidden_clave_proveedor" value="" />
                    <table class="table table-striped table-bordered table-hover" id="tabla_proveedores">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Clave</th>
                            <th>Nombre Comercial</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_proveedores_educal" tabindex="-1" role="dialog" aria-labelledby="modal_proveedores_educal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-users ic-modal"></i> Proveedores EDUCAL</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-3">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tabla_proveedores_educal">
                        <thead>
                        <tr>
                            <th>Clave</th>
                            <th>Nombre Comercial</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>