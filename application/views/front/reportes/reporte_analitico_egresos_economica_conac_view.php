<h3 class="page-header title center"><i class="fa fa-files-o"></i> Estado Analítico de Egresos</h3>
<div id="page-wrapper">
    <form class="" action="<?= base_url("reportes/imprimir_reporte_analiticoregresoseconomica") ?>" method="POST" id="datos_impresion" role="form">
        <div class="row add-pre error-gral">
            <div class="col-lg-12">
                <div class="panel panel-default" style="margin: 0 auto; margin-top: 3%; width: 50%;">
                    <div class="panel-body">
                        <h4 class="text-center">Clasificación Económica </h4>
                        <h5 class="text-center">Tipo de Gasto</h5>

                        <!--Tipo Adeacuación-->
                        <select class="form-control" id="tipo_gasto" name="tipo_gasto" style="margin-top: 5%;">
                            <option value="">Tipo de Gasto</option>
                            <option value="1">Gasto Corriente</option>
                            <option value="2">Gasto de Capital</option>
                            <option value="3">Amortización de la cuenta y disminución de pasivos</option>
                            <option value="1,2,3">Todo</option>
                        </select>

                        <div class="btns-finales text-center">
                            <a class="btn btn-default" href="<?= base_url("reportes/reportesPresupuestos") ?>"><i class="fa fa-reply" style="color: #B6CE33;"></i> Regresar</a>
                            <input class="btn btn-green" type="submit" value="Continuar"/>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
