<h3 class="page-header title center"><i class="fa fa-files-o"></i> Consulta Afectación Presupuestaria Egresos</h3>
<div id="page-wrapper">
    <form class="forma_adecuaciones_egresos" action="<?= base_url("reportes/imprimir_reporte_adecuacionesegresos") ?>" name="form" method="POST" id="datos_impresion" role="form">
        <div class="row add-pre error-gral">
            <div class="col-lg-12">
                <div class="panel panel-default" style="margin: 0 auto; margin-top: 2%; width: 50%;">
                    <div class="panel-body">
                        <!--Tipo Adeacuación-->
                        <select class="form-control" id="tipo_adecuacion" name="tipo_adecuacion">
                            <option value="">Tipo de Adecuación</option>
                            <option value="Ampliación">Ampliación</option>
                            <option value="Reducción">Reducción</option>
                            <option value="Transferencia">Transferencia</option>
                            <option value="">Todo</option>
                        </select>

                        <!--Tipo de Consulta-->
                        <label style="margin-top: 1%;">Seleccione el tipo de consulta</label>
                        <div class="row">
                            <div class="col-lg-6" >
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="opciones" id="general" value="gral" checked>General
                                    </label>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="opciones" id="especifica" value="espec">Específica
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="margin-top: 1%;">
                            <div class="col-lg-6">
                                <input type="text" class="form-control ic-calendar" name="fecha_inicial" id="fecha_inicial" placeholder="Fecha Inicial" >
                            </div>
                            <div class="col-lg-6">
                                <input type="text" class="form-control ic-calendar" name="fecha_final" id="fecha_final" placeholder="Fecha Final" >
                            </div>
                        </div>


                        <div class="row niveles-pc" id="consulta_especifica" name="consulta_especifica" style="margin-top: 3%; display: none;">
                            <div class="col-lg-6">
                                <!--Estructura Administrativa de egresos-->
                                <?= $niveles ?>
                            </div>
                            <div class="col-lg-6">
                                <a href="#modal_estructura_egresos" class="btn btn-default" data-toggle="modal" data-target="#modal_estructura_egresos" style="margin-top: 0;">¿No conoces la
                                    <br/>estructura?</a>
                            </div>
                        </div>
                        <div class="btns-finales text-center">
                            <a class="btn btn-default" href="<?= base_url("reportes/reportesPresupuestos") ?>"><i class="fa fa-reply" style="color: #B6CE33;"></i> Regresar</a>
                            <input class="btn btn-green" type="submit" id="consultar_reporte" value="Continuar"/>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<!-- Modal estructura Egresos-->
<div class="modal fade" id="modal_estructura_egresos" tabindex="-1" role="dialog" aria-labelledby="modal_estructura_egresos" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-indent-left"></span> Estructura de Egresos</h4>
            </div>
            <div class="modal-body text-center">
                <?= $niveles_modal ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" disabled id="elegir_ultimoNivel">Elegir</button>
            </div>
        </div>
    </div>
</div>