<h3 class="page-header title center"><i class="fa fa-files-o"></i> Estado Analítico de Ingresos</h3>
<div id="page-wrapper">
    <form class="" action="<?= base_url("reportes/exportar_reporte_analiticoIngresos") ?>" name="form" method="POST" id="datos_impresion" role="form">
        <div class="row add-pre error-gral">
            <div class="col-lg-12">
                <div class="panel panel-default" style="margin: 0 auto; margin-top: 2%; width: 50%;">
                    <div class="panel-body">

                        <div class="row" style="margin-top: 1%;">
                            <div class="col-lg-6">
                                <select class="form-control" id="mes_inicial" name="mes_inicial">
                                    <option value="1">Enero</option>
                                    <option value="2">Febrero</option>
                                    <option value="3">Marzo</option>
                                    <option value="4">Abril</option>
                                    <option value="5">Mayo</option>
                                    <option value="6">Junio</option>
                                    <option value="7">Julio</option>
                                    <option value="8">Agosto</option>
                                    <option value="9">Septiembre</option>
                                    <option value="10">Octubre</option>
                                    <option value="11">Noviembre</option>
                                    <option value="12">Diciembre</option>
                                </select>
                            </div>
                            <div class="col-lg-6">
                                <select class="form-control" id="mes_final" name="mes_final">
                                    <option value="1">Enero</option>
                                    <option value="2">Febrero</option>
                                    <option value="3">Marzo</option>
                                    <option value="4">Abril</option>
                                    <option value="5">Mayo</option>
                                    <option value="6">Junio</option>
                                    <option value="7">Julio</option>
                                    <option value="8">Agosto</option>
                                    <option value="9">Septiembre</option>
                                    <option value="10">Octubre</option>
                                    <option value="11">Noviembre</option>
                                    <option value="12">Diciembre</option>
                                </select>
                            </div>
                        </div>

                        <div class="row niveles-pc" id="consulta_especifica" name="consulta_especifica" style="margin-top: 3%;">
                            <div class="col-lg-6">
                                <!--Estructura Administrativa de ingresos-->
                                <?= $niveles ?>
                            </div>
                            <div class="col-lg-6">
                                <a href="#modal_estructura_ingresos" class="btn btn-default" data-toggle="modal" data-target="#modal_estructura_ingresos" style="margin-top: 0;">¿No conoces la
                                    <br/>estructura?</a>
                            </div>
                        </div>
                        <div class="btns-finales text-center">
                            <a class="btn btn-default" href="<?= base_url("reportes/reportesPresupuestos") ?>"><i class="fa fa-reply" style="color: #B6CE33;"></i> Regresar</a>
                            <input class="btn btn-green" type="submit" id="consultar_reporte" value="Continuar"/>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<!-- Modal estructura Ingresos-->
<div class="modal fade" id="modal_estructura_ingresos" tabindex="-1" role="dialog" aria-labelledby="modal_estructura_ingresos" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-indent-left"></span> Estructura de Ingresos</h4>
            </div>
            <div class="modal-body text-center">
                <?= $niveles_modal ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" disabled id="elegir_ultimoNivel">Elegir</button>
            </div>
        </div>
    </div>
</div>