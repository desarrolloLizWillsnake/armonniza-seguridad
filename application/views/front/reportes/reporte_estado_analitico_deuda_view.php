<?php
$this->db->select('*')->from('cat_polizas_firmas');
$query = $this->db->get();
$resultados = $query->result();
?>
<h3 class="page-header title center"><i class="fa fa-files-o"></i>Estado Analítico de la Deuda y Otros Pasivos</h3>
<div id="page-wrapper">
    <form class="" action="<?= base_url("reportes/imprimir_reporte_estadoAnaliticoDeuda") ?>"method="POST" id="datos_impresion" role="form">
        <div class="row add-pre error-gral">
            <div class="col-lg-12">
                <div class="panel panel-default" style="margin: 0 auto; margin-top: 2%; width: 50%;">
                    <div class="panel-body">
                        <!--Rango Fechas-->
                        <div class="row">
                            <div class="col-lg-6">
                                <input type="text" class="form-control ic-calendar" name="fecha_inicial" id="fecha_inicial" placeholder="Fecha Inicial" >
                            </div>
                            <div class="col-lg-6">
                                <input type="text" class="form-control ic-calendar" name="fecha_final" id="fecha_final" placeholder="Fecha Final" >
                            </div>
                        </div>
                        <!--Elaboró-->
                        <div class="form-group">
                            <label class="label_sig center">Elaboró</label>
                            <select class="form-control" name="persona_elaboro">
                                <?php
                                foreach($resultados as $row) { ?>
                                    <option value="<?= $row->id_persona ?>"><?= $row->grado_estudio ?> <?= $row->nombre ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="btns-finales text-center">
                            <a class="btn btn-default" href="<?= base_url("reportes/reportesContabilidad") ?>"><i class="fa fa-reply" style="color: #B6CE33;"></i> Regresar</a>
                            <input class="btn btn-green" type="submit" value="Continuar"/>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
</div>