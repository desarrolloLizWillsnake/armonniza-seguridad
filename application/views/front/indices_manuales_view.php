<h3 class="page-header title center"><i class="fa fa-bookmark"></i> Guías de usuario Armonniza</h3>
<div id="page-wrapper" style="padding-bottom: 2%;">
    <div class="manuales">
        <i class="fa fa-leanpub ic-manual" style="font-size: 12em; color: #25B7BC;  position: absolute; left: 75%; top: 70%; z-index: 5;"></i>
        <div class="list-group cont-manuales">

            <!--Manual Usuario 1-->
            <a href="<?= base_url("documentos/Guía_General_Programa_Armonniza.pdf") ?>" class="list-group-item">
                <div class="row">
                    <div class="col-lg-2"><img class="ic-armonniza" src="<?= base_url('img/logo_small.png') ?>"/></div>
                    <div class="col-lg-10 text-left">Guía General Programa Armonniza</div>
                </div>
            </a>
            <!--Manual Usuario 2-->
            <a href="<?= base_url("documentos/Guia_de_Registro_y_Captura_Estructurra_Administrativa_Ingresos.pdf") ?>" class="list-group-item">
                <div class="row">
                    <div class="col-lg-2"><img class="ic-armonniza" src="<?= base_url('img/logo_small.png') ?>"/></div>
                    <div class="col-lg-10 text-left">Guía de Registro, Captura e Implementación Estructura Administrativa Ingresos</div>
                </div>
            </a>

            <!--Manual Usuario 3-->
            <a href="<?= base_url("documentos/Guia_de_Registro_y_Captura_Estructura_Administrativa_Egresos.pdf") ?>" class="list-group-item">
                <div class="row">
                    <div class="col-lg-2"><img class="ic-armonniza" src="<?= base_url('img/logo_small.png') ?>"/></div>
                    <div class="col-lg-10 text-left">Guía de Registro, Captura e Implementación Estructura Administrativa Egresos</div>
                </div>
            </a>
            <!--Manual Usuario 4-->
            <a href="<?= base_url("documentos/Guía_de_Registro_y_Captura_Adecuaciones_Presupuestarias_de_Ingresos_y_Egresos.pdf") ?>" class="list-group-item">
                <div class="row">
                    <div class="col-lg-2"><img class="ic-armonniza" src="<?= base_url('img/logo_small.png') ?>"/></div>
                    <div class="col-lg-10 text-left">Guía de Registro y Captura Adecuaciones Presupuestarias de Ingresos y Egresos</div>
                </div>
            </a>

            <!--Manual Usuario 5-->
            <a href="<?= base_url("documentos/Guia_de_Registro_y_Captura_de_PreCompromisos.pdf") ?>" class="list-group-item">
                <div class="row">
                    <div class="col-lg-2"><img class="ic-armonniza"src="<?= base_url('img/logo_small.png') ?>"/></div>
                    <div class="col-lg-10 text-left">Guía de Registro y Captura de Pre Compromisos</div>
                </div>
            </a>
            <!--Manual Usuario 6-->
            <a href="<?= base_url("documentos/Guia_de_Registro_y_Captura_de_Compromisos.pdf") ?>" class="list-group-item">
                <div class="row">
                    <div class="col-lg-2"><img class="ic-armonniza" src="<?= base_url('img/logo_small.png') ?>"/></div>
                    <div class="col-lg-10 text-left">Guía de Registro y Captura de Compromisos</div>
                </div>
            </a>
            <!--Manual Usuario 7-->
            <a href="<?= base_url("documentos/Guia_de_Registro_y_Captura_de_Contra_Recibos_de_Pago.pdf") ?>" class="list-group-item">
                <div class="row" style="background: transparent;">
                    <div class="col-lg-2"><img class="ic-armonniza" src="<?= base_url('img/logo_small.png') ?>"/></div>
                    <div class="col-lg-10 text-left">Guía de Registro y Captura de Contra Recibos de Pago</div>
                </div>
            </a>
            <!--Manual Usuario 8-->
            <a href="<?= base_url("documentos/Guía_de_Registro_y_Captura_de_Movimientos_Bancarios.pdf") ?>" class="list-group-item">
                <div class="row">
                    <div class="col-lg-2"><img class="ic-armonniza" src="<?= base_url('img/logo_small.png') ?>"/></div>
                    <div class="col-lg-10 text-left">Guía de Registro y Captura de Movimientos Bancarios</div>
                </div>
            </a>
            <!--Manual Usuario 9-->
            <a href="<?= base_url("documentos/Guía_de_Registro_y_Captura_de_Conciliaciones_Bancarias.pdf") ?>" class="list-group-item">
                <div class="row">
                    <div class="col-lg-2"><img class="ic-armonniza" src="<?= base_url('img/logo_small.png') ?>"/></div>
                    <div class="col-lg-10 text-left">Guia de Registro y Captura de Conciliaciones Bancarias</div>
                </div>
            </a>
            <!--Manual Usuario 10-->
            <a href="<?= base_url("documentos/Guía_de_Registro_y_Consulta_de_Ingresos_Devengado_y_Recaudado.pdf") ?>" class="list-group-item">
                <div class="row">
                    <div class="col-lg-2"><img class="ic-armonniza" src="<?= base_url('img/logo_small.png') ?>"/></div>
                    <div class="col-lg-10 text-left">Guía de Registro y Consulta de Ingresos Devengado y Recaudado</div>
                </div>
            </a>
            <!--Manual Usuario 11-->
            <a href="<?= base_url("documentos/Guía_de_Registro_Captura_de_Contabilidad.pdf") ?>" class="list-group-item">
                <div class="row">
                    <div class="col-lg-2"><img class="ic-armonniza" src="<?= base_url('img/logo_small.png') ?>"/></div>
                    <div class="col-lg-10 text-left">Guía de Registro y Captura de Contabilidad</div>
                </div>
            </a>
            <!--Manual Usuario 12-->
            <a href="<?= base_url("documentos/Guía_de_Uso_de_la_plataforma_de_ayuda.pdf") ?>" class="list-group-item">
                <div class="row">
                    <div class="col-lg-2"><img class="ic-armonniza" src="<?= base_url('img/logo_small.png') ?>"/></div>
                    <div class="col-lg-10 text-left">Guía de Uso de la Plataforma soporte.armonniza.com</div>
                </div>
            </a>
            <!--Manual Usuario 13-->
            <a href="<?= base_url("documentos/Guía_de_registro_y_captura_de_Patrimonio.pdf") ?>" class="list-group-item">
                <div class="row">
                    <div class="col-lg-2"><img class="ic-armonniza" src="<?= base_url('img/logo_small.png') ?>"/></div>
                    <div class="col-lg-10 text-left">Guía de Registro, Captura y Consulta de Patrimonio</div>
                </div>
            </a>
            <!--Manual Usuario 14-->
            <a href="<?= base_url("documentos/Guía_de_uso_de_Panel_Administrador.pdf") ?>" class="list-group-item">
                <div class="row">
                    <div class="col-lg-2"><img class="ic-armonniza" src="<?= base_url('img/logo_small.png') ?>"/></div>
                    <div class="col-lg-10 text-left">Guía de Uso de Panel Administrador</div>
                </div>
            </a>
            <!--Manual Usuario 15-->
            <a href="<?= base_url("documentos/Guía_Traslado_de_Información_del_Ejercicio_Fiscal.pdf") ?>" class="list-group-item">
                <div class="row">
                    <div class="col-lg-2"><img class="ic-armonniza" src="<?= base_url('img/logo_small.png') ?>"/></div>
                    <div class="col-lg-10 text-left">Guía de Traslado de Información del Ejercicio Fiscal</div>
                </div>
            </a>


        </div>
    </div>

</div>