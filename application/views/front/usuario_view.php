<!-- Page Content -->
<h3 class="page-header title center"><i class="fa fa-user"></i> Perfil Usuario</h3>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row perfil-usuario">
            <div class="col-lg-12">
                <?php
                if(isset($mensaje)){ ?>
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?= $mensaje ?>
                    </div>
                <?php } ?>
                <form role="form" action="<?= base_url("usuario/actualizar_datos") ?>" method="POST">
                    <div class="form-group">
                        <input type="hidden" name="id_datos_usuario" id="id_datos_usuario" value="<?= $id_datos_usuario ?>" />
                        <input type="hidden" name="id_usuario" id="id_usuario" value="<?= $id_usuario ?>" />
                        <div class="row cont">
                            <div class="col-lg-4"><label >Nombre</label></div>
                            <div class="col-lg-8"><input class="form-control" name="nombre" id="nombre" value="<?= utf8_decode($nombre) ?>" required="required"/></div>
                        </div>
                        <div class="row cont">
                            <div class="col-lg-4"> <label>Apellido Paterno</label></div>
                            <div class="col-lg-8"><input class="form-control" name="a_paterno" id="a_paterno" value="<?= utf8_decode($apellido_paterno) ?>" required="required"/></div>
                        </div>
                        <div class="row cont">
                            <div class="col-lg-4"> <label>Apellido Materno</label></div>
                            <div class="col-lg-8"><input class="form-control" name="a_materno" id="a_materno" value="<?= utf8_decode($apellido_materno) ?>" required="required"/></div>
                        </div>
                        <div class="row cont">
                            <div class="col-lg-4"> <label>Puesto</label></div>
                            <div class="col-lg-8"><input class="form-control" name="puesto" id="puesto" value="<?= utf8_decode($puesto) ?>" required="required"/></div>
                        </div>

                        <div class="btn-perfil text-center">
                            <a href="<?= base_url("auth/change_password") ?>" class="btn btn-default">Cambiar Contraseña</a>
                            <button type="submit" class="btn btn-green">Actualizar Información</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->