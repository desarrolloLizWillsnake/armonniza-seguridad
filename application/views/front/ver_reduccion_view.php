<h3 class="page-header center"><i class="fa fa-eye"></i> Ver Reducción</h3>
<div id="page-wrapper">
    <form class="forma_reduccion" role="form">
        <div class="row add-pre error-gral">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        General
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="row">
                            <input type="hidden" name="ultimo" id="ultimo" value="<?= $numero ?>">
                            <!--Primera Columna-->
                            <div class="col-lg-3">
                                <!--No. Ampliación-->
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-7"><label class="label-f">No. Reducción</label></div>
                                        <div class="col-lg-5">
                                            <?php if(isset($numero)) { ?>
                                                <p class="form-control-static input_view"><?= $numero ?></p>
                                            <?php } else { ?>
                                                <p class="form-control-static input_view"></p>
                                            <?php }  ?>
                                        </div>
                                    </div>
                                </div>
                                <!--Clasificación-->
                                <div class="form-group">
                                    <label>Clasificación</label>
                                    <?php if(isset($clasificacion)) { ?>
                                        <p class="form-control-static input_view"><?= $clasificacion ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>
                            </div>
                            <!--Fin Primera Columna-->

                            <!--Segunda Columna-->
                            <div class="col-lg-5" style="padding-left: 3%;">
                                <!--Tipo de Gasto-->
                                <div class="form-group">
                                    <label>Tipo de Gasto</label>
                                    <?php if(isset($tipo_gasto) && $tipo_gasto == 1) { ?>
                                        <p class="form-control-static input_view">Gasto Corriente</p>
                                    <?php } elseif(isset($tipo_gasto) && $tipo_gasto == 2) { ?>
                                        <p class="form-control-static input_view">Gasto de Capital</p>
                                    <?php } elseif(isset($tipo_gasto) && $tipo_gasto == 3) { ?>
                                        <p class="form-control-static input_view">Amortización de la cuenta y disminución de pasivos</p>
                                    <?php } elseif(isset($tipo_gasto) && $tipo_gasto == 4) { ?>
                                        <p class="form-control-static input_view">Pensiones y Jubilaciones</p>
                                    <?php } elseif(isset($tipo_gasto) && $tipo_gasto == 5) { ?>
                                        <p class="form-control-static input_view">Participaciones</p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>
                                <!--Justificación-->
                                <div class="form-group">
                                    <label>Justificación</label>
                                    <?php if(isset($descripcion)) { ?>
                                        <p class="form-control-static input_view"><?= $descripcion ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>

                            </div>
                            <!--Fin Segunda Columna-->

                            <!--Tercera Columna-->
                            <div class="col-lg-4">
                                <!--Fecha Solicitud-->
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-7"><label class="label-f">Fecha Solicitud</label></div>
                                        <div class="col-lg-5">
                                            <?php if(isset($fecha_solicitud)) { ?>
                                                <p class="form-control-static input_view"><?= $fecha_solicitud ?></p>
                                            <?php } else { ?>
                                                <p class="form-control-static input_view"></p>
                                            <?php }  ?>
                                        </div>
                                    </div>
                                </div>

                                <!--Fecha Aplicación-->
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-7"><label class="label-f">Fecha Aplicación</label></div>
                                        <div class="col-lg-5">
                                            <?php if(isset($fecha_aplicacion)) { ?>
                                                <p class="form-control-static input_view"><?= $fecha_aplicacion ?></p>
                                            <?php } else { ?>
                                                <p class="form-control-static input_view"></p>
                                            <?php }  ?>
                                        </div>
                                    </div>
                                </div>

                                <!--Ampliación en Firme-->
                                <div class="form-group c-firme" style="margin-top: 5%;">
                                    <div class="form-group">
                                        <label>¿Ampliación en Firme?</label>
                                        <?php if(isset($enfirme) && $enfirme == 1) { ?>
                                            <p class="form-control-static"><i class="fa fa-check-circle i-firmesi"></i></p>
                                        <?php } else { ?>
                                            <p class="form-control-static"><i class="fa fa-times-circle i-firmeno"></i></p>
                                        <?php }  ?>
                                    </div>
                                </div>

                            </div>
                            <!--Fin Tercera Columna-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row add-pre">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Adeacuaciones | Reducción
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body table-gral">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="tabla_datos_reduccion">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>F.F.</th>
                                <th>P.F.</th>
                                <th>C.C.</th>
                                <th>Capítulo</th>
                                <th>Concepto</th>
                                <th>Partida</th>
                                <th>Importe</th>
                                <th>Fecha Aplicada</th>
                                <th>Descripción</th>
                                <th>Movimiento</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                 </div>
            </div>
        </div>
    </div>
    <div class="btns-finales text-center">
        <a class="btn btn-default" href="<?= base_url("egresos/adecuaciones_presupuestarias") ?>" ><i class="fa fa-reply ic-color"></i> Regresar</a>
    </div>
</form>

</div>
<!-- /#page-wrapper -->