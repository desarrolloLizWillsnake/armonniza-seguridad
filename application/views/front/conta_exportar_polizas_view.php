<h3 class="page-header title center"><i class="fa fa-files-o"></i> Exportar P&oacute;lizas a Excel</h3>
<div id="page-wrapper">
    <form action="<?= base_url("contabilidad/exportar_polizas_excel") ?>" method="POST" id="datos_impresion" role="form">
        <div class="row add-pre error-gral">
            <div class="col-lg-12">
                <div class="panel panel-default" style="margin: 0 auto; margin-top: 2%; width: 50%;">
                    <div class="panel-body">
                        <div class="row" style="margin-top: 1%;">
                            <div class="col-lg-6">
                                <input type="text" class="form-control ic-calendar" name="fecha_inicial" id="fecha_inicial" placeholder="Fecha Inicial" required />
                            </div>
                            <div class="col-lg-6">
                                <input type="text" class="form-control ic-calendar" name="fecha_final" id="fecha_final" placeholder="Fecha Final" required />
                            </div>
                        </div>
                        <select class="form-control" name="tipo_poliza" required>
                            <option value="">Tipo de P&oacute;liza</option>
                            <option value="Diario">Diario</option>
                            <option value="Egresos">Egresos</option>
                            <option value="Ingresos">Ingresos</option>
                        </select>

                        <select class="form-control" name="estatus_poliza" required>
                            <option value="">Estatus de las p&oacute;lizas</option>
                            <option value="activo">En firme</option>
                            <option value="espera">Espera</option>
                            <option value="cancelada">Canceladas</option>
                        </select>
                        <div class="btns-finales text-center">
                            <a class="btn btn-default" href="<?= base_url("contabilidad/polizas") ?>"><i class="fa fa-reply ic-color"></i> Regresar</a>
                            <input class="btn btn-green" type="submit" value="Continuar" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>