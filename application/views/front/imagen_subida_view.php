<h3 class="page-header title center"><i class="fa fa-list-alt fa-fw"></i> Almacenes </h3>
<div id="page-wrapper">
    <!--mostramos la informaci�n de la imagen-->
    <h3>Tu imagen fu� subida correctamente!</h3>
    <div id="info_subida">
        <ul>
            <?php foreach ($upload_data as $item => $value):?>
                <li><?php echo $item;?>: <?php echo $value;?></li>
            <?php endforeach; ?>
        </ul>
    </div>
    <p><?php echo anchor('upload', 'Sigue subiendo archivos!!'); ?></p>
    <!--mostramos la imagen subida-->
    <div id="imagenes">
        <h3><?=$titulo?></h3>
        <img src="<?=base_url()?>img/<?=$imagen?>" />
    </div>
</div>