<h3 class="page-header title center"><i class="fa fa-edit"></i> Editar Nota de Entrada</h3>
<div id="page-wrapper">
    <form class="forma_nota" role="form">
        <div class="row add-pre error-gral">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        General
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <input type="hidden" name="ultima_nota" id="ultima_nota" value="<?= $numero_movimiento ?>">
                        <div class="row">
                            <!--Primera Columna-->
                            <div class="col-lg-3">
                                <!--No. Nota de Entrada-->
                                <div class="row">
                                    <div class="col-lg-6"><label>No. Nota Entrada</label></div>
                                    <div class="col-lg-6"><p class="form-control-static input_ver"><?= $numero_movimiento ?></p></div>
                                </div>
                                <!-- No. Contra Recibo de pago -->
                                <div class="row">
                                    <div class="col-lg-6" style="padding-right: 0px;"><label>No. Contra Recibo</label></div>
                                    <div class="col-lg-6">
                                        <div class="form-group input-group" >
                                            <input type="hidden" name="id_contrarecibo" id="id_contrarecibo" />
                                            <input type="text" class="form-control" name="no_contrarecibo" id="no_contrarecibo" style="margin-top: 1%;" <?= $contrarecibo != NULL ? ' value="'.$contrarecibo.'"' : '';?> disabled/>
                                            <span class="input-group-btn ic-buscar-btn">
                                                <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_contrarecibo" style="margin-top: .5%;"><i class="fa fa-search"></i></button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <!-- No. Compromiso -->
                                <div class="row" style="margin-top: 1.5%; margin-bottom: 1.5%;">
                                    <div class="col-lg-6"><label>No. Compromiso</label></div>
                                    <div class="col-lg-6">
                                        <input type="hidden" name="id_compromiso" id="id_compromiso" />
                                        <input type="text" class="form-control" name="compromiso" id="compromiso"  <?= $compromiso_nota_entrada != NULL ? ' value="'.$compromiso_nota_entrada.'"' : '';?> disabled/>
                                    </div>
                                </div>
                                <!--Concepto de Entrada-->
                                <select class="form-control" id="tipo_poliza" name="tipo_poliza" required>
                                    <option value="">Concepto de Entrada</option>
                                    <option value="Proceso" <?= $tipo_poliza == 'Proceso' ? ' selected="selected"' : '';?>>Proceso</option>
                                    <option value="Inicial" <?= $tipo_poliza == 'Inicial' ? ' selected="selected"' : '';?>>Inicial</option>
                                    <option value="Directa" <?= $tipo_poliza == 'Directa' ? ' selected="selected"' : '';?>>Directa</option>
                                </select>

                            </div>
                            <!--Fin Primera Columna-->

                            <!--Segunda Columna-->
                            <div class="col-lg-6">

                                <div class="row">
                                    <div class="col-lg-3">
                                        <!--Clave-->
                                        <input type="text" class="form-control" name="clave_proveedor" id="clave_proveedor" placeholder="Clave"  <?= $id_proveedor != NULL ? ' value="'.$id_proveedor.'"' : '';?> disabled/>
                                    </div>
                                    <div class="col-lg-9">
                                        <!-- Nombre Proveedor -->
                                        <div class="form-group input-group">
                                            <input type="hidden" name="id_proveedor" id="id_proveedor" />
                                            <input type="text" class="form-control" name="proveedor" id="proveedor" placeholder="Proveedor"  <?= $proveedor != NULL ? ' value="'.$proveedor.'"' : '';?> disabled/>
                                        <span class="input-group-btn ic-buscar-btn">
                                            <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_proveedores"><i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                        </div>
                                    </div>
                                </div>

                                <!-- Condiciones de Entrega -->
                                <textarea style="height: 8em; margin-top: .5%;" class="form-control" id="observaciones" name="observaciones" placeholder="Descripción / Observaciones" <?= $observaciones != NULL ? '>'.$observaciones.'</textarea>' : '></textarea>' ;?>

                            </div>
                            <!--Fin Segunda Columna-->
                            <!--Tercera Columna-->
                            <div class="col-lg-3">
                                <!-- Fecha -->
                                <input type="text" class="form-control ic-calendar" name="fecha" id="fecha" placeholder="Fecha Emisión"  <?= $fecha_emision != NULL ? ' value="'.$fecha_emision.'"' : '';?>>
                                <!--Tipo Documento-->
                                <select class="form-control" id="tipo_documento" name="tipo_documento" required>
                                    <option value="">Comprobante Fiscal</option><!--Tipo de documento-->
                                    <option value="Factura" <?= $documento == 'Factura' ? ' selected="selected"' : '';?>>Factura</option>
                                    <option value="Nota de Crédito" <?= $documento == 'Nota de Crédito' ? ' selected="selected"' : '';?>>Nota de Crédito</option>
                                </select>
                                <!-- No. Documento -->
                                <textarea style="height: 5em; margin-top: .5%;" class="form-control" name="documento" id="documento" placeholder="No. Comprobante Fiscal" <?= $no_documento != NULL ? '>'.$no_documento.'</textarea>' : '></textarea>' ;?>
                            </div>
                            <!--Fin Tercera Columna-->

                        </div>
                        <!--                        <div class="row">-->
                        <!--                            <div class="col-lg-5">-->
                        <!--                                <div class="form-group input-group">-->
                        <!--                                    <input type="text" class="form-control ic-buscar-input" name="cuenta_cargo" id="cuenta_cargo" placeholder="Cuenta Cargo" required="required"/>-->
                        <!--                                    <span class="input-group-btn ic-buscar-btn">-->
                        <!--                                        <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_cuenta_cargo">-->
                        <!--                                            <i class="fa fa-search"></i>-->
                        <!--                                        </button>-->
                        <!--                                    </span>-->
                        <!--                                </div>-->
                        <!---->
                        <!--                                <input type="text" class="form-control" name="descripcion_cuenta_cargo" id="descripcion_cuenta_cargo" placeholder="Descripción Cuenta Cargo" required="required"/>-->
                        <!--                            </div>-->
                        <!---->
                        <!--                            <div class="col-lg-2"></div>-->
                        <!---->
                        <!--                            <div class="col-lg-5">-->
                        <!--                                <div class="form-group input-group">-->
                        <!--                                    <input type="text" class="form-control ic-buscar-input" name="cuenta_abono" id="cuenta_abono" placeholder="Cuenta Abono" required="required"/>-->
                        <!--                                    <span class="input-group-btn ic-buscar-btn">-->
                        <!--                                        <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_cuenta_abono">-->
                        <!--                                            <i class="fa fa-search"></i>-->
                        <!--                                        </button>-->
                        <!--                                    </span>-->
                        <!--                                </div>-->
                        <!---->
                        <!--                                <input type="text" class="form-control" name="descripcion_cuenta_abono" id="descripcion_cuenta_abono" placeholder="Descripción Cuenta Abono" required="required"/>-->
                        <!--                            </div>-->
                        <!--                        </div>-->
                    </div>
                </div>
            </div>
        </div>

        <div class="row add-pre error-gral">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Detalle Nota de Entrada
                    </div>
                    <div class="panel-body">
                        <div class="text-center">
                            <a id="btn_detalle" class="btn btn-default"><i class="fa fa-plus-square ic-color"></i> Agregar Detalle</a>
                        </div>
                        <br>
                        <div id="detalle" style="display: none;">
                            <div class="row">
                                <div class="col-lg-3 niveles-pc">
                                    <!--Estructura Administrativa de Egresos-->
                                    <?= $niveles ?>
                                    <a href="#modal_estructura" class="btn btn-default" data-toggle="modal" data-target="#modal_estructura">¿No conoces la
                                        <br/>estructura?</a>
                                </div>
                                <div class="col-lg-5"></div>
                                <div class="col-lg-4">
                                    <div class="form-group input-group">
                                        <input type="text" class="form-control" style="margin-top: -.5%;" name="gasto" id="gasto" placeholder="Gasto" required/>
                                        <span class="input-group-btn ic-buscar-btn" >
                                            <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_gasto"><i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                    </div>

                                    <input id="titulo_gasto" class="form-control" name="titulo_gasto" placeholder="Título" required />

                                    <input type="text" class="form-control" name="u_medida" id="u_medida" placeholder="Unidad de Medida" required />

                                    <input type="text" pattern="[0-9]+" class="form-control" name="cantidad" id="cantidad" placeholder="Cantidad" required>

                                    <div class="form-group input-group">
                                        <span class="input-group-btn ic-peso-btn">
                                            <button class="btn btn-default sg-dollar" ><i class="fa fa-dollar"></i>
                                            </button>
                                        </span>
                                        <input type="text" pattern="[0-9]+([\.|,][0-9]+)?" class="form-control dinero" name="precio" id="precio" placeholder="Precio Unitario" required />
                                    </div>

                                    <textarea class="form-control" id="descripcion_detalle" name="descripcion_detalle" placeholder="Descripción" required></textarea>

                                    <!-- style="height: 6em;" -->
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <button type="button" class="btn btn-green btn-block btn-gd" id="guardar_nota_detalle">Guardar Detalle de Nota</button>
                                </div>
                            </div>
                        </div>
                        </form>
                        <div class="row">
                            <div class="col-lg-12">
                                <div id="resultado_insertar_detalle"></div>
                                <div class="panel panel-default">
                                    <div class="panel-body table-gral">
                                        <div class="table-responsive">

                                            <h4 id="suma_total" class="text-center"></h4>
                                            <input type="hidden" value="" name="importe_total" id="importe_total" />

                                            <table class="table table-striped table-bordered table-hover" id="tabla_detalle">
                                                <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>F.F.</th>
                                                    <th>P.F.</th>
                                                    <th>C.C.</th>
                                                    <th>Capítulo</th>
                                                    <th>Concepto</th>
                                                    <th>Partida</th>
                                                    <th>Artículo / CUCOP</th>
                                                    <th>U/M</th>
                                                    <th>Cantidad</th>
                                                    <th>Precio U.</th>
                                                    <th>Subtotal</th>
                                                    <th>Importe</th>
                                                    <th>Título</th>
                                                    <th>Acciones</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    <div class="panel panel-default">
        <div class="panel-body text-center">
            <div class="form-group c-firme">
                <h3>¿Nota de Entrada en Firme?</h3>
                <label class="checkbox-inline">
                    <input type="checkbox" id="check_firme" name="check_firme" <?= $enfirme == 1 ? ' checked="checked" disabled' : '';?> />Sí
                </label>
            </div>
        </div>
    </div>
    <div class="btns-finales text-center">
        <div class="text-center" id="resultado_insertar_caratula"></div>
        <a class="btn btn-default" href="<?= base_url("patrimonio/nota_entrada") ?>"><i class="fa fa-reply ic-color"></i> Regresar</a>
        <a id="guardar_nota" class="btn btn-green">Guardar Nota de Entrada</a>
    </div>


</div>
</div>
<!-- Modal Gasto -->
<div class="modal fade" id="modal_gasto" tabindex="-1" role="dialog" aria-labelledby="modal_gasto" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-usd"></span> Gastos</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-gasto">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tabla_gastos">
                        <thead>
                        <tr>
                            <th>Clave</th>
                            <th>Clasificación</th>
                            <th>Descripción</th>
                            <th>U/M</th>
                            <th>Existencia</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" disabled>Elegir</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Proveedores -->
<div class="modal fade" id="modal_proveedores" tabindex="-1" role="dialog" aria-labelledby="modal_proveedores" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-users ic-modal"></i> Proveedores</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-3">
                <div class="table-responsive">
                    <input type="hidden" name="hidden_clave_proveedor" id="hidden_clave_proveedor" value="" />
                    <table class="table table-striped table-bordered table-hover" id="tabla_proveedores">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Clave</th>
                            <th>Nombre Comercial</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

</div>
<!-- /.row -->
</div>

<!-- Modal estructura -->
<div class="modal fade" id="modal_estructura" tabindex="-1" role="dialog" aria-labelledby="modal_estructura" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-indent-left"></span> Estructura de Egresos</h4>
            </div>
            <div class="modal-body">
                <?= $niveles_modal ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" disabled id="elegir_ultimoNivel">Elegir</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Contra Recibo -->
<div class="modal fade" id="modal_contrarecibo" tabindex="-1" role="dialog" aria-labelledby="modal_contrarecibo" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-clipboard ic-color"></i> Contra Recibos de Pago</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-10">
                <div class="table-responsive">
                    <input type="hidden" name="hidden_no_contrarecibo" id="hidden_no_contrarecibo" value="" />
                    <input type="hidden" name="hidden_no_compromiso" id="hidden_no_compromiso" value="" />
                    <table class="table table-striped table-bordered table-hover" id="tabla_contrarecibo">
                        <thead>
                        <tr>
                            <th>No. Contra Recibo</th>
                            <th>No. compromiso</th>
                            <th>Fecha Autorizado</th>
                            <th>Tipo</th>
                            <th>Proveedor</th>
                            <th>Importe</th>
                            <th>Creado Por</th>
                            <th>Enfirme</th>
                            <th>Estatus</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Cuenta Cargo -->
<div class="modal fade" id="modal_cuenta_cargo" tabindex="-1" role="dialog" aria-labelledby="modal_cuenta_cargo" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-arrow-circle-down ic-modal"></i> Cuenta Cargo</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-4">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tabla_cuenta_cargo">
                        <thead>
                        <tr>
                            <th>Cuenta</th>
                            <th>Nombre</th>
                            <th>Tipo</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Cuenta Abono -->
<div class="modal fade" id="modal_cuenta_abono" tabindex="-1" role="dialog" aria-labelledby="modal_cuenta_abono" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-arrow-circle-up ic-modal"></i> Cuenta Abono</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-4">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tabla_cuenta_abono">
                        <thead>
                        <tr>
                            <th>Cuenta</th>
                            <th>Nombre</th>
                            <th>Tipo</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Editar Detalle -->
<div class="modal fade modal_editar" tabindex="-1" role="dialog" aria-labelledby="modal_editar" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-edit ic-modal"></i> Editar Detalle</h4>
            </div>
            <div class="modal-body modal_edit_detalle">
                <div class="form-group" style="margin: 0 auto; width: 60%;">

                    <label for="message-text" class="control-label">Centro Costos</label>
                    <input class="form-control" type="text" value="" name="editar_cc_nota" id="editar_cc_nota" />

                    <label for="message-text" class="control-label">Artículo</label>
                    <input class="form-control" type="text" value="" name="editar_articulo_nota" id="editar_articulo_nota" />

                    <input class="form-control" type="hidden" value="" name="borrar_nota_hidden" id="borrar_nota_hidden" />
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-reply ic-color"></i> Regresar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_editar_nota_detalle">Aceptar</button>
            </div>
        </div>
    </div>
</div>