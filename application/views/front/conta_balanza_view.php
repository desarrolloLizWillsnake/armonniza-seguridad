<h3 class="page-header title center"><i class="fa fa-pie-chart"></i> Consulta Balanza</h3>
<div id="page-wrapper">
    <form action="<?= base_url("contabilidad/tabla_balanza") ?>" method="POST" id="datos_balanza" role="form">
        <div class="row add-pre error-gral text-center">
            <div class="col-lg-12">
                <div class="panel panel-default" style="margin: 0 auto; margin-top: 2%; width: 50%;">
                    <div class="panel-body">
                        <div class="row" style="margin-top: 1%;">
                            <div class="col-lg-12">
                                <!--Subsidio-->
                                <div class="form-group input-group">
                                    <input type="text" class="form-control" name="subsidio" id="subsidio" style="margin-top: .5%;" placeholder="Subsidio"/>
                                        <span class="input-group-btn ic-buscar-btn">
                                            <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_subsidio"><i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                </div>
                            </div>
                        </div>
                        <!--Centro de Costos-->
                        <div class="row" style="margin-top: 1%;">
                            <div class="col-lg-12">
                                <div class="form-group input-group">
                                    <input type="text" class="form-control" name="centro_costos" id="centro_costos" style="margin-top: .5%;" placeholder="Centro de Costos"/>
                                        <span class="input-group-btn ic-buscar-btn">
                                            <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_centro_costos"><i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                </div>
                            </div>
                        </div>
                        <!--Período Tiempo -->
                        <div class="row" style="margin-top: 2%;">
                            <div class="col-lg-6">
                                <input type="text" class="form-control ic-calendar" name="fecha_inicial" id="fecha_inicial" placeholder="Fecha Inicial" >
                            </div>
                            <div class="col-lg-6">
                                <input type="text" class="form-control ic-calendar" name="fecha_final" id="fecha_final" placeholder="Fecha Final" >
                            </div>
                        </div>
                        <!--Rango Cuentas -->
                        <div class="row" style="margin-top: 2%;">
                            <div class="col-lg-6">
                                <div class="form-group input-group">
                                    <input type="text" class="form-control" name="cuenta_inicial" id="cuenta_inicial" placeholder="No. Cuenta Inicial" style="margin-top: -1.5%;"/>
                                        <span class="input-group-btn ic-buscar-btn">
                                            <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_cuenta_inicial"><i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group input-group">
                                    <input type="text" class="form-control" name="cuenta_final" id="cuenta_final" placeholder="No. Cuenta Final" style="margin-top: -1.5%;"/>
                                        <span class="input-group-btn ic-buscar-btn">
                                            <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_cuenta_final"><i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                </div>
                            </div>
                        </div>
                        <!--Tipo Nivel de cuenta-->
                        <select class="form-control" id="nivel_cuenta" name="nivel_cuenta" style="margin-top: 2%;" required>
                            <option value="">Seleccione Nivel de Cuenta</option>
                            <option value="1">Género</option>
                            <option value="2">Grupo</option>
                            <option value="3">Rubro</option>
                            <option value="4">Cuenta</option>
                            <option value="5">Subcuenta</option>
                            <option value="6">SubSubcuenta</option>
                            <option value="7">SubSubSubcuenta</option>
                            <option value="8">SubSubSubSubcuenta</option>
                            <option value="9">SubSubSubSubSubcuenta</option>
                            <option value="99">Todos</option>
                        </select>

                        <div class="btns-finales text-center">
                            <div id="espera"></div>
                            <input class="btn btn-green" type="submit" id="consultar_balanza" value="Aceptar"/>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<!-- Modal Subsidio-->
<div class="modal fade" id="modal_subsidio" tabindex="-1" role="dialog" aria-labelledby="modal_subsidio" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-indent-left" style="color: #25B7BC;"></span> Subsidio</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-3">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tabla_subsidio">
                        <thead>
                        <tr>
                            <th>Código</th>
                            <th>Fuentes de Financiamiento</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Plan Cuentas Contables | Cuenta Inicial-->
<div class="modal fade" id="modal_cuenta_inicial" tabindex="-1" role="dialog" aria-labelledby="modal_cuenta_inicial" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-book" style="color: #25B7BC;"></span> Plan de Cuentas Contables</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-4">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tabla_cuenta_inicial">
                        <thead>
                        <tr>
                            <th>Cuenta</th>
                            <th>Nombre</th>
                            <th>Tipo</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal Plan Cuentas Contables | Cuenta Final-->
<div class="modal fade" id="modal_cuenta_final" tabindex="-1" role="dialog" aria-labelledby="modal_cuenta_final" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-book" style="color: #25B7BC;"></span> Plan de Cuentas Contables</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-4">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tabla_cuenta_final">
                        <thead>
                        <tr>
                            <th>Cuenta</th>
                            <th>Nombre</th>
                            <th>Tipo</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Centros de Costos -->
<div class="modal fade" id="modal_centro_costos" tabindex="-1" role="dialog" aria-labelledby="modal_centro_costos" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-building ic-modal"></i> Centros de Costos</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-3">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tabla_centro_costos">
                        <thead>
                        <tr>
                            <th>Centro de Costos</th>
                            <th>Descripción</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

