<h3 class="page-header title center"><i class="fa fa-files-o fa-fw"></i> Reportes</h3>
<div id="page-wrapper">
    <div id="reportes-presupuestos">
        <div class="row">
            <div class="col-lg-12 text-left">
                <a class="btn btn-default btn-reportes" href="<?= base_url("reportes/reportesContabilidad") ?>">Contabilidad / CONAC</a>
                <a class="btn btn-default btn-reportes active" href="<?= base_url("reportes/reportesPresupuestos") ?>">Presupuestos / CONAC</a>
                <a class="btn btn-default btn-reportes" href="<?= base_url("reportes/reportesAdministracion") ?>">Administración</a>
            </div>
        </div>
        <div class="row menu-catalogos menu-reportes">
            <div class="col-xs-4 col-sm-4">
                <div class="catalogo-caja text-center">
                    <a href="<?= base_url("reportes/exportar_reporte_presupuestoingresos") ?>" download="Ley de Ingresos Aprobada 2015" data-tooltip="Exportar"><i class="fa fa-file-excel-o"></i></a>
                    <h5>Ley de Ingresos <br/> Aprobado</h5>
                </div>
            </div>
            <div class="col-xs-4 col-sm-4">
                <div class="catalogo-caja text-center">
                    <a href="<?= base_url("reportes/adecuacionesIngresos") ?>" data-tooltip="Consulta"><i class="fa fa-file-pdf-o"></i></a>
                    <a href="<?= base_url("reportes/adecuacionesIngresosExcel") ?>" data-tooltip="Exportar"><i class="fa fa-file-excel-o margin"></i></a>
                    <h5>Consulta de Afectación Presupuestaria de Ingresos</h5>
                </div>
            </div>
            <div class="col-xs-4 col-sm-4">
                <div class="catalogo-caja text-center">
                    <a href="<?= base_url("reportes/estadoAuxiliarIngresos") ?>" data-tooltip="Consulta"><i class="fa fa-file-pdf-o"></i></a>
                    <a href="<?= base_url("reportes/estadoAuxiliarIngresosExcel") ?>" data-tooltip="Exportar"><i class="fa fa-file-excel-o margin"></i></a>
                    <h5>Estado Auxiliar de Ingresos Presupuesto</h5>
                </div>
            </div>
            <div class="col-xs-4 col-sm-4">

                <div class="catalogo-caja text-center">
                    <a href="<?= base_url("reportes/exportar_reporte_presupuestoegresos") ?>" download="Presupuesto Egresos Aprobado 2015" data-tooltip="Exportar"><i class="fa fa-file-excel-o"></i></a>
                    <h5>Presupuesto de Egresos <br/> Aprobado</h5>
                </div>
            </div>
            <div class="col-xs-4 col-sm-4">
                <div class="catalogo-caja text-center">
                    <a href="<?= base_url("reportes/adecuacionesEgresos") ?>" data-tooltip="Consulta"><i class="fa fa-file-pdf-o"></i></a>
                    <a href="<?= base_url("reportes/adecuacionesEgresosExcel") ?>" data-tooltip="Exportar"><i class="fa fa-file-excel-o margin"></i></a>
                    <h5>Consulta de Afectación Presupuestaria de Egresos </h5>
                </div>
            </div>
            <div class="col-xs-4 col-sm-4">
                <div class="catalogo-caja text-center">
                    <a href="<?= base_url("reportes/estadoAuxiliarEgresos") ?>" data-tooltip="Consulta"><i class="fa fa-file-pdf-o"></i></a>
                    <a href="<?= base_url("reportes/estadoAuxiliarEgresosExcel") ?>" data-tooltip="Exportar"><i class="fa fa-file-excel-o margin"></i></a>
                    <h5>Estado Auxiliar de Egresos Presupuesto</h5>
                </div>
            </div>
            <div class="col-xs-4 col-sm-4">
                <div class="catalogo-caja text-center">
                    <a href="<?= base_url("reportes/precompromisosAutorizados") ?>" data-tooltip="Consulta"><i class="fa fa-file-pdf-o"></i></a>
                    <a href="<?= base_url("reportes/precompromisosAutorizadosExcel") ?>" data-tooltip="Exportar"><i class="fa fa-file-excel-o margin"></i></a>
                    
                    <h5>Presupuesto <br> Precompromiso vs. Compromiso</h5>
                </div>
            </div>
            <div class="col-xs-4 col-sm-4">
                <div class="catalogo-caja text-center">
                    <a href="<?= base_url("reportes/analiticoIngresos") ?>" data-tooltip="Consulta"><i class="fa fa-file-pdf-o"></i></a>
                    <a href="<?= base_url("reportes/analiticoIngresosExcel") ?>" data-tooltip="Exportar"><i class="fa fa-file-excel-o margin"></i></a>
                    <h5>Estado Analítico <br> Ingresos</h5>
                </div>
            </div>

            <div class="col-xs-4 col-sm-4">
                <div class="catalogo-caja text-center">

                    <a href="<?= base_url("reportes/analiticoEgresosObjetoGasto") ?>" data-tooltip="Consulta"><i class="fa fa-file-pdf-o"></i></a>
                    <a href="<?= base_url("reportes/analiticoEgresosObjetoGastoExcel") ?>" data-tooltip="Exportar"><i class="fa fa-file-excel-o margin"></i></a>
                    <h5>Estado Analítico Egresos <br> Clasificación por Objeto de Gasto </h5>
                </div>
            </div>
            <!--
            <div class="col-xs-4 col-sm-4">
                <div class="catalogo-caja text-center">
                    <a href="<?= base_url("reportes/analiticoEgresosEconomica") ?>"data-tooltip="Consulta"><i class="fa fa-file-pdf-o"></i></a>
                    <a href="<?= base_url("reportes/analiticoEgresosEconomicaExcel") ?>" data-tooltip="Exportar"><i class="fa fa-file-excel-o margin"></i></a>
                    <h5>Estado Analítico Egresos <br> Clasificación Económica</h5>
                </div>
            </div>
            -->
            <div class="col-xs-4 col-sm-4">
                <div class="catalogo-caja text-center">
                    <a href="<?= base_url("reportes/analiticoEgresosAdministrativa") ?>" data-tooltip="Consulta"><i class="fa fa-file-pdf-o"></i></a>
                    <a href="<?= base_url("reportes/analiticoEgresosAdministrativaExcel") ?>" data-tooltip="Exportar"><i class="fa fa-file-excel-o margin"></i></a>
                    <h5>Estado Analítico Egresos <br> Clasificación Administrativa</h5>
                </div>
            </div>

            <div class="col-xs-4 col-sm-4">
                <div class="catalogo-caja text-center">
                    <a href="<?= base_url("reportes/analiticoEgresosGasto") ?>" data-tooltip="Consulta"><i class="fa fa-file-pdf-o"></i></a>
                    <a href="<?= base_url("reportes/analiticoEgresosGastoExcel") ?>" data-tooltip="Exportar"><i class="fa fa-file-excel-o margin"></i></a>
                    <h5>Estado Analítico del Ejercido Egresos <br> Capítulo del Gasto</h5>
                </div>
            </div>
            <!--<div class="col-xs-4 col-sm-4">
                <div class="catalogo-caja text-center">
                    <a href="<?/*= base_url("reportes/reporte_movimientos_presupuesto_egresos") */?>" data-tooltip="Exportar"><i class="fa fa-file-excel-o"></i></a>
                    <h5>Reporte de Movimientos Presupuestarios <br> Egresos</h5>
                </div>
            </div>-->
            <div class="col-xs-4 col-sm-4">
                <div class="catalogo-caja text-center">
                    <a href="<?= base_url("reportes/reporte_movimientos_presupuesto_egresos_anual") ?>" data-tooltip="Exportar"><i class="fa fa-file-excel-o"></i></a>
                    <h5>Reporte de Movimientos Presupuestarios Anual <br> Egresos</h5>
                </div>
            </div>
            <div class="col-xs-4 col-sm-4">
                <div class="catalogo-caja text-center">
                    <a href="<?= base_url("reportes/reporte_movimientos_presupuesto_ingresos_anual") ?>" data-tooltip="Exportar"><i class="fa fa-file-excel-o"></i></a>
                    <h5>Reporte de Movimientos Presupuestarios Anual <br> Ingresos</h5>
                </div>
            </div>
        </div>
    </div>
</div>

</div>
</div>
