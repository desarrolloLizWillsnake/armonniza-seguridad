<h3 class="page-header title center"><i class="fa fa-clipboard"></i> Contra Recibo de Pago</h3>
<div id="page-wrapper">
    <div class="row cont-btns-c center">
        <div class="col-lg-12">
            <?php if($this->utilerias->get_permisos("agregar_contrarecibo") || $this->utilerias->get_grupo() == 1) { ?>
                <a href="<?= base_url("ciclo/generar_contrarecibo") ?>" class="btn btn-default"><i class="fa fa-plus-circle circle ic-color"></i> Generar Contra Recibo de Pago</a>
            <?php } ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <ul class="nav nav-tabs">
                <li id= "btn-todas" class="active"><a onclick="BtnTodas()">Todos</a></li>
                <li id= "btn-aprobado" role="presentation"><a onclick="BtnAprobado()">Aprobado</a></li>
                <li id= "btn-pendiente" role="presentation"><a onclick="BtnPendiente()">Pendiente</a></li>
                <li id= "btn-cancelado" role="presentation"><a onclick="BtnCancelado()">Cancelado</a></li>
            </ul>
            <div class="panel panel-default" id="tab-todas">
                <div class="panel-body table-gral">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover datos_tabla table-cpago ajuste">
                            <thead>
                            <tr>
                                <th width="6%">No.</th>
                                <th width="9.5%">Compromiso</th>
                                <th width="19%">Proveedor</th>
                                <th width="9%">F. Emitido</th>
                                <th width="9%">F. Pago</th>
                                <th width="11%">Importe</th>
                                <th width="5.5%">Pago</th>
                                <th width="12%">Creado Por</th>
                                <th width="6%">Firme</th>
                                <th >Estatus</th>
                                <th width="20%">Acciones</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="panel panel-default" id="tab-aprobado" style="display: none;">
                <div class="panel-body table-gral">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover datos_tabla_aprobado table-cpago ajuste" >
                            <thead>
                            <tr>
                                <th width="6%">No.</th>
                                <th width="7%">Compromiso</th>
                                <th width="21%">Proveedor</th>
                                <th width="9%">F. Emitido</th>
                                <th width="9%">F. Pago</th>
                                <th width="11%">Importe</th>
                                <th width="5.5%">Pago</th>
                                <th width="12%">Creado Por</th>
                                <th width="6%">Firme</th>
                                <th width="8%">Estatus</th>
                                <th width="20%">Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="panel panel-default" id="tab-pendiente" style="display: none;">
                <div class="panel-body table-gral">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover datos_tabla_pendiente table-cpago ajuste">
                            <thead>
                            <tr>
                                <th width="6%">No.</th>
                                <th width="7%">Compromiso</th>
                                <th width="21%">Proveedor</th>
                                <th width="9%">F. Emitido</th>
                                <th width="9%">F. Pago</th>
                                <th width="11%">Importe</th>
                                <th width="5.5%">Pago</th>
                                <th width="12%">Creado Por</th>
                                <th width="6%">Firme</th>
                                <th width="8%">Estatus</th>
                                <th width="20%">Acciones</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="panel panel-default" id="tab-cancelado" style="display: none;">
                <div class="panel-body table-gral">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover datos_tabla_cancelado table-cpago ajuste">
                            <thead>
                            <tr>
                                <th width="6%">No.</th>
                                <th width="7%">Compromiso</th>
                                <th width="21%">Proveedor</th>
                                <th width="9%">F. Emitido</th>
                                <th width="9%">F. Pago</th>
                                <th width="11%">Importe</th>
                                <th width="5.5%">Pago</th>
                                <th width="12%">Creado Por</th>
                                <th width="6%">Firme</th>
                                <th width="8%">Estatus</th>
                                <th width="20%">Acciones</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="modal fade modal_borrar" tabindex="-1" role="dialog" aria-labelledby="modal_borrar" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-remove ic-modal"></i> Cancelar Contra Recibo</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="message-text" class="control-label">¿Realmente desea cancelar el contra recibo seleccionado?</label>
                        <input type="hidden" value="" name="cancelar_contrarecibo" id="cancelar_contrarecibo" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_cancelar_contrarecibo">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal_poliza" tabindex="-1" role="dialog" aria-labelledby="modal_poliza" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-leanpub ic-modal"></i> Póliza</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="message-text" class="control-label">Se generara Póliza de Diario correspondiente al Contra Recibo de Pago No. <span id="numero_contrarecibo_poliza"></span></label>
                        <input type="hidden" value="" name="numero_contrarecibo_poliza_hidden" id="numero_contrarecibo_poliza_hidden" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-reply ic-color"></i> Regresar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_generar_poliza">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade resultado_poliza" tabindex="-1" role="dialog" aria-labelledby="resultado_poliza" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-leanpub ic-modal"></i> Resultado Póliza</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="message-text" class="control-label"><span id="mensaje_resultado_poliza"></span></label>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-green" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_resultado_borrar" tabindex="-1" role="dialog" aria-labelledby="modal_resultado_borrar" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-remove ic-modal"></i> Cancelar Contrarecibo</h4>
            </div>
            <div class="modal-body">
                <div id="resultado_borrar"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-green" data-dismiss="modal" >Aceptar</button>
            </div>
        </div>
    </div>
</div>