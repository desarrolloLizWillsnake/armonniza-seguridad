<h3 class="page-header title center"><i class="fa fa-files-o"></i> Exportar Aprobado</h3>
<div id="page-wrapper">
    <form class="" action="<?= base_url("anteproyecto/exportar_aprobado_excel") ?>" name="form" method="POST" id="datos_impresion" role="form">
        <div class="row add-pre error-gral">
            <div class="col-lg-12">
                <div class="panel panel-default" style="margin: 0 auto; margin-top: 2%; width: 50%;">
                    <div class="panel-body">
                        <!-- Centro de Costos -->
                        <div class="form-group input-group">
                            <input type="text" class="form-control" name="centro_costos" id="centro_costos" placeholder="Centro de Costos" />
                            <span class="input-group-btn ic-buscar-btn">
                                <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_centro_costos"><i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>

                        <!-- Partida -->
                        <div class="form-group input-group">
                            <input type="text" class="form-control" name="partida" id="partida" placeholder="Partida" />
                            <span class="input-group-btn ic-buscar-btn">
                                <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_partida"><i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>

                        <div class="btns-finales text-center">
                            <a class="btn btn-default" href="presupuesto_aprobado"><i class="fa fa-reply ic-color"></i> Regresar</a>
                            <input class="btn btn-green" type="submit" value="Continuar"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<!-- Modal Centro de Costos -->
<div class="modal fade" id="modal_centro_costos" tabindex="-1" role="dialog" aria-labelledby="modal_centro_costos" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-building ic-modal"></i> Centro de Costos</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-3">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tabla_centro_costos">
                        <thead>
                        <tr>
                            <th>Centro de Costos</th>
                            <th>Descripci&oacute;n</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Partida -->
<div class="modal fade" id="modal_partida" tabindex="-1" role="dialog" aria-labelledby="modal_partida" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-indent-left" style="color:#25B7BC;"></span> Partidas</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-3">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tabla_partidas">
                        <thead>
                        <tr>
                            <th width="15%">Partida</th>
                            <th width="70%">Descripci&oacute;n</th>
                            <th width="15%">Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

