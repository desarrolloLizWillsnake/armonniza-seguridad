<h3 class="page-header title center" xmlns="http://www.w3.org/1999/html"><i class="fa fa-files-o"></i> B&uacute;squeda Avanzada Presupuesto Aprobado</h3>
<div id="page-wrapper">
    <?php

    $attributes = array(
        'name' => 'busqueda',
        'id' => 'busqueda',
        'role' => 'form',
    );

    echo validation_errors();
    echo form_open('anteproyecto/presupuesto_aprobado_busqueda', $attributes);
    ?>
        <div class="row add-pre error-gral">
            <div class="col-lg-12">
                <div class="panel panel-default" style="margin: 0 auto; margin-top: 2%; width: 50%;">
                    <div class="panel-body">

                        <!-- Centro de Costos -->
                        <div class="form-group input-group">
                            <input type="text" class="form-control" name="centro_costos" id="centro_costos" placeholder="Centro de Costos" value="<?= set_value('centro_costos'); ?>" />
                            <span class="input-group-btn ic-buscar-btn">
                                <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_centro_costos"><i class="fa fa-search"></i></button>
                            </span>
                        </div>

                        <!-- Partida -->
                        <div class="form-group input-group">
                            <input type="text" class="form-control" name="partida" id="partida" placeholder="Partida" value="<?= set_value('partida'); ?>" />
                            <span class="input-group-btn ic-buscar-btn">
                                <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_partida"><i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>

                        <div class="row">
                          <form name="f1">
                            <div class="col-lg-12">
                                <h4>Meses</h4>
                                <input type="checkbox" name="meses" id="meses" onclick="if(this.checked){document.getElementById('enero').checked=true;document.getElementById('febrero').checked=true;document.getElementById('marzo').checked=true;document.getElementById('abril').checked=true;document.getElementById('mayo').checked=true;document.getElementById('junio').checked=true;document.getElementById('julio').checked=true;document.getElementById('agosto').checked=true;document.getElementById('septiembre').checked=true;document.getElementById('octubre').checked=true;document.getElementById('noviembre').checked=true;document.getElementById('diciembre').checked=true;}else{document.getElementById('enero').checked=false;document.getElementById('febrero').checked=false;document.getElementById('marzo').checked=false;document.getElementById('abril').checked=false;document.getElementById('mayo').checked=false;document.getElementById('junio').checked=false;document.getElementById('julio').checked=false;document.getElementById('agosto').checked=false;document.getElementById('septiembre').checked=false;document.getElementById('octubre').checked=false;document.getElementById('noviembre').checked=false;document.getElementById('diciembre').checked=false;}"><label class="margen" style="color: #919999;">Seleccionar/Quitar</label>
                                <div style="color: black;">
                                    <?php
                                    if(isset($meses)) {
                                        if (in_array("enero", $meses)) { ?>
                                            <input type="checkbox" name="meses[]" id="enero" value="enero" onclick="if(this.checked)document.getElementById('meses').checked=true" checked="true" /><label class="margen-meses" >Enero</label>
                                        <?php } else { ?>
                                            <input type="checkbox" name="meses[]" id="enero" value="enero" onclick="if(this.checked)document.getElementById('meses').checked=true" /><label class="margen-meses" >Enero</label>
                                        <?php }

                                        if (in_array("febrero", $meses)) { ?>
                                            <input type="checkbox" name="meses[]" id="febrero" value="febrero" onclick="if(this.checked)document.getElementById('meses').checked=true" checked="true" /><label class="margen-meses" >Febrero</label>
                                        <?php } else { ?>
                                            <input type="checkbox" name="meses[]" id="febrero" value="febrero" onclick="if(this.checked)document.getElementById('meses').checked=true" /><label class="margen-meses" >Febrero</label>
                                        <?php }

                                        if (in_array("marzo", $meses)) { ?>
                                            <input type="checkbox" name="meses[]" id="marzo" value="marzo" onclick="if(this.checked)document.getElementById('meses').checked=true" checked="true" /><label class="margen-meses" >Marzo</label>
                                        <?php } else { ?>
                                            <input type="checkbox" name="meses[]" id="marzo" value="marzo" onclick="if(this.checked)document.getElementById('meses').checked=true" /><label class="margen-meses" >Marzo</label>
                                        <?php }

                                        if (in_array("abril", $meses)) { ?>
                                            <input type="checkbox" name="meses[]" id="abril" value="abril" onclick="if(this.checked)document.getElementById('meses').checked=true" checked="true" /><label class="margen-meses" >Abril</label><br />
                                        <?php } else { ?>
                                            <input type="checkbox" name="meses[]" id="abril" value="abril" onclick="if(this.checked)document.getElementById('meses').checked=true" /><label class="margen-meses" >Abril</label><br />
                                        <?php }

                                        if (in_array("mayo", $meses)) { ?>
                                            <input type="checkbox" name="meses[]" id="mayo" value="mayo" onclick="if(this.checked)document.getElementById('meses').checked=true" checked="true" /><label class="margen-meses" >Mayo</label>
                                        <?php } else { ?>
                                            <input type="checkbox" name="meses[]" id="mayo" value="mayo" onclick="if(this.checked)document.getElementById('meses').checked=true" /><label class="margen-meses" >Mayo</label>
                                        <?php }

                                        if (in_array("junio", $meses)) { ?>
                                            <input type="checkbox" name="meses[]" id="junio" value="junio" onclick="if(this.checked)document.getElementById('meses').checked=true" checked="true" /><label class="margen-meses" >Junio</label>
                                        <?php } else { ?>
                                            <input type="checkbox" name="meses[]" id="junio" value="junio" onclick="if(this.checked)document.getElementById('meses').checked=true" /><label class="margen-meses" >Junio</label>
                                        <?php }

                                        if (in_array("julio", $meses)) { ?>
                                            <input type="checkbox" name="meses[]" id="julio" value="julio" onclick="if(this.checked)document.getElementById('meses').checked=true" checked="true" /><label class="margen-meses" >Julio</label>
                                        <?php } else { ?>
                                            <input type="checkbox" name="meses[]" id="julio" value="julio" onclick="if(this.checked)document.getElementById('meses').checked=true" /><label class="margen-meses" >Julio</label>
                                        <?php }

                                        if (in_array("agosto", $meses)) { ?>
                                            <input type="checkbox" name="meses[]" id="agosto" value="agosto" onclick="if(this.checked)document.getElementById('meses').checked=true" checked="true" /><label class="margen-meses" >Agosto</label><br />
                                        <?php } else { ?>
                                            <input type="checkbox" name="meses[]" id="agosto" value="agosto" onclick="if(this.checked)document.getElementById('meses').checked=true" /><label class="margen-meses" >Agosto</label><br />
                                        <?php }

                                        if (in_array("septiembre", $meses)) { ?>
                                            <input type="checkbox" name="meses[]" id="septiembre" value="septiembre" onclick="if(this.checked)document.getElementById('meses').checked=true" checked="true" /><label class="margen-meses" >Sept</label>
                                        <?php } else { ?>
                                            <input type="checkbox" name="meses[]" id="septiembre" value="septiembre" onclick="if(this.checked)document.getElementById('meses').checked=true" /><label class="margen-meses" >Sept</label>
                                        <?php }

                                        if (in_array("octubre", $meses)) { ?>
                                            <input type="checkbox" name="meses[]" id="octubre" value="octubre" onclick="if(this.checked)document.getElementById('meses').checked=true" checked="true" /><label class="margen-meses" >Octubre</label>
                                        <?php } else { ?>
                                            <input type="checkbox" name="meses[]" id="octubre" value="octubre" onclick="if(this.checked)document.getElementById('meses').checked=true" /><label class="margen-meses" >Octubre</label>
                                        <?php }

                                        if (in_array("noviembre", $meses)) { ?>
                                            <input type="checkbox" name="meses[]" id="noviembre" value="noviembre" onclick="if(this.checked)document.getElementById('meses').checked=true" checked="true" /><label class="margen-meses" >Nov</label>
                                        <?php } else { ?>
                                            <input type="checkbox" name="meses[]" id="noviembre" value="noviembre" onclick="if(this.checked)document.getElementById('meses').checked=true" /><label class="margen-meses" >Nov</label>
                                        <?php }

                                        if (in_array("diciembre", $meses)) { ?>
                                            <input type="checkbox" name="meses[]" id="diciembre" value="diciembre" onclick="if(this.checked)document.getElementById('meses').checked=true" checked="true" /><label class="margen-meses" >Dic</label><br /><br />
                                        <?php } else { ?>
                                            <input type="checkbox" name="meses[]" id="diciembre" value="diciembre" onclick="if(this.checked)document.getElementById('meses').checked=true" /><label class="margen-meses" >Dic</label><br /><br />
                                        <?php }  ?>

                                    <?php } else { ?>
                                     <div class="row">
                                         <div class="col-lg-3">
                                             <input type="checkbox" name="meses[]" id="enero" value="enero" onclick="if(this.checked)document.getElementById('meses').checked=true" /><label class="margen-meses" >Enero</label><br>
                                             <input type="checkbox" name="meses[]" id="febrero" value="febrero" onclick="if(this.checked)document.getElementById('meses').checked=true"  /><label class="margen-meses">Febrero</label><br>
                                             <input type="checkbox" name="meses[]" id="marzo" value="marzo" onclick="if(this.checked)document.getElementById('meses').checked=true" /><label class="margen-meses">Marzo</label><br>
                                         </div>
                                         <div class="col-lg-3">
                                             <input type="checkbox" name="meses[]" id="abril" value="abril" onclick="if(this.checked)document.getElementById('meses').checked=true" /><label class="margen-meses">Abril</label><br>
                                             <input type="checkbox" name="meses[]" id="mayo" value="mayo" onclick="if(this.checked)document.getElementById('meses').checked=true" /><label class="margen-meses">Mayo</label><br>
                                             <input type="checkbox" name="meses[]" id="junio" value="junio" onclick="if(this.checked)document.getElementById('meses').checked=true" /><label class="margen-meses">Junio</label><br>
                                        </div>
                                        <div class="col-lg-3">
                                             <input type="checkbox" name="meses[]" id="julio" value="julio" onclick="if(this.checked)document.getElementById('meses').checked=true" /><label class="margen-meses">Julio</label><br>
                                             <input type="checkbox" name="meses[]" id="agosto" value="agosto" onclick="if(this.checked)document.getElementById('meses').checked=true" /><label class="margen-meses">Agosto</label><br>
                                             <input type="checkbox" name="meses[]" id="septiembre" value="septiembre" onclick="if(this.checked)document.getElementById('meses').checked=true" /><label class="margen-meses">Sept</label><br>
                                        </div>
                                         <div class="col-lg-3">
                                             <input type="checkbox" name="meses[]" id="octubre" value="octubre" onclick="if(this.checked)document.getElementById('meses').checked=true" /><label class="margen-meses">Octubre</label><br>
                                             <input type="checkbox" name="meses[]" id="noviembre" value="noviembre" onclick="if(this.checked)document.getElementById('meses').checked=true" /><label class="margen-meses">Nov</label><br>
                                             <input type="checkbox" name="meses[]" id="diciembre" value="diciembre" onclick="if(this.checked)document.getElementById('meses').checked=true" /><label class="margen-meses">Dic</label><br>
                                        </div>
                                     </div>
                                     <?php } ?>
                                </div>
                            </div>
                          </form>
                            <div class="col-lg-12">
                                <h4>Tipo de Presupuesto</h4>
                                <input type="checkbox" id="presupuesto" onclick="if(this.checked){document.getElementById('anio').checked=true;document.getElementById('fondos').checked=true;document.getElementById('solicitado').checked=true;document.getElementById('aprobado').checked=true;}else{document.getElementById('anio').checked=false;document.getElementById('fondos').checked=false;document.getElementById('solicitado').checked=false;document.getElementById('aprobado').checked=false;}"><label class="margen" style="color: #919999;">Seleccionar/Quitar</label>
                              <div style="color: black;">
                                  <?php
                                  if(isset($tipo_presupuesto)) {
                                      if (in_array("anio", $tipo_presupuesto)) { ?>
                                          <input type="checkbox" name="tipo_presupuesto[]" id="anio" value="anio" onclick="if(this.checked)document.getElementById('presupuesto').checked=true" checked="true"><label class="margen">2015</label>
                                      <?php } else { ?>
                                          <input type="checkbox" name="tipo_presupuesto[]" id="anio" value="anio" onclick="if(this.checked)document.getElementById('presupuesto').checked=true" ><label class="margen">2015</label>
                                      <?php }

                                      if (in_array("fondos", $tipo_presupuesto)) { ?>
                                          <input type="checkbox" name="tipo_presupuesto[]" id="fondos" value="fondos" onclick="if(this.checked)document.getElementById('presupuesto').checked=true" checked="true"><label class="margen">Fondos</label>
                                      <?php } else { ?>
                                          <input type="checkbox" name="tipo_presupuesto[]" id="fondos" value="fondos" onclick="if(this.checked)document.getElementById('presupuesto').checked=true" ><label class="margen">Fondos</label>
                                      <?php }

                                      if (in_array("solicitado", $tipo_presupuesto)) { ?>
                                          <input type="checkbox" name="tipo_presupuesto[]" id="solicitado" value="solicitado" onclick="if(this.checked)document.getElementById('presupuesto').checked=true" checked="true"><label class="margen">Solicitado</label>
                                      <?php } else { ?>
                                          <input type="checkbox" name="tipo_presupuesto[]" id="solicitado" value="solicitado" onclick="if(this.checked)document.getElementById('presupuesto').checked=true" ><label class="margen">Solicitado</label>
                                      <?php }

                                      if (in_array("aprobado", $tipo_presupuesto)) { ?>
                                          <input type="checkbox" name="tipo_presupuesto[]" id="aprobado" value="aprobado" onclick="if(this.checked)document.getElementById('presupuesto').checked=true" checked="true"><label class="margen">Aprobado</label><br><br>
                                      <?php } else { ?>
                                          <input type="checkbox" name="tipo_presupuesto[]" id="aprobado" value="aprobado" onclick="if(this.checked)document.getElementById('presupuesto').checked=true"><label class="margen">Aprobado</label><br><br>
                                      <?php } ?>


                                  <?php } else { ?>
                                <input type="checkbox" name="tipo_presupuesto[]" id="anio" value="anio" onclick="if(this.checked)document.getElementById('presupuesto').checked=true"><label class="margen">2015</label>
                                <input type="checkbox" name="tipo_presupuesto[]" id="fondos" value="fondos" onclick="if(this.checked)document.getElementById('presupuesto').checked=true"><label class="margen">Fondos</label>
                                <input type="checkbox" name="tipo_presupuesto[]" id="solicitado" value="solicitado" onclick="if(this.checked)document.getElementById('presupuesto').checked=true"><label class="margen">Solicitado</label>
                                <input type="checkbox" name="tipo_presupuesto[]" id="aprobado" value="aprobado" onclick="if(this.checked)document.getElementById('presupuesto').checked=true"><label class="margen">Aprobado</label><br><br>
                                  <?php } ?>
                              </div>
                            </div>
                        </div>
                        <center>
                        <div class="row">
                           <div class="col-lg-12"class="btns-finales text-center">
                                <a class="btn btn-default" href="presupuesto_aprobado"><i class="fa fa-reply ic-color"></i> Regresar</a>
                                <input class="btn btn-green" type="submit" value="Continuar"/>
                           </div>
                        </div>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    <br><br>
        <?php
        if(isset($tabla_lista)) { ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body table-gral">
                            <div class="table-responsive">
                              <center> <form action="<?= base_url("anteproyecto/exportar_busqueda_excel") ?>" name="form" method="post" id="datos_busqueda" role="form">
                                    <input type="hidden" name="jquery_tabla" id="query_tabla" value="<?= $query ?>">
                                    <input clas="btn-busqueda" type="submit" value="Exportar" class="btn btn-default">
                                </form> </center>
                                <?= $tabla_lista ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </form>
</div>



<!-- Modal Centro de Costos -->
<div class="modal fade" id="modal_centro_costos" tabindex="-1" role="dialog" aria-labelledby="modal_centro_costos" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-building ic-color"></i> Centro de Costos</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-3">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tabla_cc_busqueda">
                        <thead>
                        <tr>
                            <th>Centro de Costos</th>
                            <th>Descripci&oacute;n</th>
                            <th>Selecciona</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Partida -->
<div class="modal fade" id="modal_partida" tabindex="-1" role="dialog" aria-labelledby="modal_partida" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-indent-left" style="color:#25B7BC;"></span> Partidas</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-3">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tabla_partidas_busqueda">
                        <thead>
                        <tr>
                            <th width="15%">Partida</th>
                            <th width="70%">Descripci&oacute;n</th>
                            <th width="15%">Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>








