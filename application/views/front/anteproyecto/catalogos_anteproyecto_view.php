<h3 class="page-header title center" xmlns="http://www.w3.org/1999/html"><i class="fa fa-cubes fa-fw"></i> Cat&aacute;logos  </h3>
<div id="page-wrapper">
    <div class="row menu-catalogos menu-reportes" style="padding-top: 6%;">
        <div class="col-xs-6 col-sm-6">
            <a href="<?= base_url("anteproyecto/catalogo_usuarios") ?>" class="ref">
                <div class="catalogo-caja text-center">
                    <i class="fa fa-folder-open"></i>
                    <h5>Cat&aacute;logo <br/> Centro Costos</h5>
                </div>
            </a>
        </div>
        <div class="col-xs-6 col-sm-6">
            <a href="<?= base_url("anteproyecto/catalogo_partidas") ?>" class="ref">
                <div class="catalogo-caja text-center">
                    <i class="fa fa-folder-open"></i>
                    <h5>Cat&aacute;logo <br/> Partidas</h5>
                </div>
            </a>
        </div>
    </div>
</div>