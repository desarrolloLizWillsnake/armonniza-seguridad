<h3 class="page-header title center"><i class="fa fa-book"></i>Aprobar Presupuesto</h3>
<div id="page-wrapper">

    <div class="row cont-btns-c center">
        <div class="col-lg-12">
            <a href="<?= base_url("plantillas/plantilla_anteproyecto.xls") ?>" class="btn btn-default"><i class="fa fa-download ic-color"></i> Descargar Plantilla</a>
            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#subirArchivoSolicitado" data-whatever="SubirArchivo"><i class="fa fa-upload ic-color"></i> Subir Solicitado</button>
            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#subirArchivoAprobado" data-whatever="SubirArchivo"><i class="fa fa-upload ic-color"></i> Subir Aprobado</button>

            <?php if($this->utilerias->get_permisos("subir_anio_anteproyecto") || $this->utilerias->get_grupo() == 1){ ?>
            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#subirArchivo2015" data-whatever="SubirArchivo"><i class="fa fa-upload ic-color"></i> Subir 2015</button>
            <?php } ?>

            <?php if($this->utilerias->get_permisos("subir_fondos_anteproyecto") || $this->utilerias->get_grupo() == 1){ ?>
            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#subirArchivoFondos" data-whatever="SubirArchivo"><i class="fa fa-upload ic-color"></i> Subir Fondos</button>
            <?php } ?>

            <a href="<?= base_url("anteproyecto/exportar_aprobado") ?>" class="btn btn-default"><i class="fa fa-download ic-color"></i> Exportar</a>

            <br><br>
            <?php if($this->utilerias->get_permisos("aprobado_anteproyecto") || $this->utilerias->get_grupo() == 1){?>
            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#aprobado" data-whatever="Aprobado"><i class="fa fa-check  ic-color"></i> Aprobado</button>
            <?php } ?>

            <a href="<?= base_url("plantillas/hoja_membretada.pdf") ?>" class="btn btn-default"><i class="fa fa-file-pdf-o ic-color"></i> Generar Documento</a>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body table-gral">
                    <div class="table-responsive">
                        <a href="<?= base_url("anteproyecto/presupuesto_aprobado_busqueda") ?>" class="btn btn-default" style="float: right"><i class="fa fa-search ic-color"></i> B&uacute;squeda Avanzada</a>
                        <br><br>
                        <table class="display" id="datos_tabla">
                            <thead>
                            <tr>
                                <th style="border-bottom: 1px dotted #959496"></th>
                                <th style="border-bottom: 1px dotted #959496"></th>
                                <th style="border-bottom: 1px dotted #959496"></th>
                                <th style="border-bottom: 1px dotted #959496"></th>
                                <th colspan="4" style="text-align: center; border-bottom: 1px dotted #959496">Enero</th>
                                <th colspan="4" style="text-align: center; border-bottom: 1px dotted #959496">Febrero</th>
                                <th colspan="4" style="text-align: center; border-bottom: 1px dotted #959496">Marzo</th>
                                <th colspan="4" style="text-align: center; border-bottom: 1px dotted #959496">Abril</th>
                                <th colspan="4" style="text-align: center; border-bottom: 1px dotted #959496">Mayo</th>
                                <th colspan="4" style="text-align: center; border-bottom: 1px dotted #959496">Junio</th>
                                <th colspan="4" style="text-align: center; border-bottom: 1px dotted #959496">Julio</th>
                                <th colspan="4" style="text-align: center; border-bottom: 1px dotted #959496">Agosto</th>
                                <th colspan="4" style="text-align: center; border-bottom: 1px dotted #959496">Septiembre</th>
                                <th colspan="4" style="text-align: center; border-bottom: 1px dotted #959496">Octubre</th>
                                <th colspan="4" style="text-align: center; border-bottom: 1px dotted #959496">Noviembre</th>
                                <th colspan="4" style="text-align: center; border-bottom: 1px dotted #959496">Diciembre</th>
                                <th style="border-bottom: 1px dotted #959496"></th>
                                <th style="border-bottom: 1px dotted #959496"></th>
                                <th style="border-bottom: 1px dotted #959496"></th>
                                <th style="border-bottom: 1px dotted #959496"></th>
                            </tr>
                            <tr>
                                <th class="border-left">CC</th>
                                <th></th>
                                <th class="border-left">Partida</th>
                                <th></th>
                                <th class="border-left">2015</th>
                                <th>Fondos 2015</th>
                                <th>Solicitado 2016</th>
                                <th class="border-right">Aprobado 2016</th>
                                <th>2015</th>
                                <th>Fondos 2015</th>
                                <th>Solicitado 2016</th>
                                <th class="border-right">Aprobado 2016</th>
                                <th>2015</th>
                                <th>Fondos 2015</th>
                                <th>Solicitado 2016</th>
                                <th class="border-right">Aprobado 2016</th>
                                <th>2015</th>
                                <th>Fondos 2015</th>
                                <th>Solicitado 2016</th>
                                <th class="border-right">Aprobado 2016</th>
                                <th>2015</th>
                                <th>Fondos 2015</th>
                                <th>Solicitado 2016</th>
                                <th class="border-right">Aprobado 2016</th>
                                <th>2015</th>
                                <th>Fondos 2015</th>
                                <th>Solicitado 2016</th>
                                <th class="border-right">Aprobado 2016</th>
                                <th>2015</th>
                                <th>Fondos 2015</th>
                                <th>Solicitado 2016</th>
                                <th class="border-right">Aprobado 2016</th>
                                <th>2015</th>
                                <th>Fondos 2015</th>
                                <th>Solicitado 2016</th>
                                <th class="border-right">Aprobado 2016</th>
                                <th>2015</th>
                                <th>Fondos 2015</th>
                                <th>Solicitado 2016</th>
                                <th class="border-right">Aprobado 2016</th>
                                <th>2015</th>
                                <th>Fondos 2015</th>
                                <th>Solicitado 2016</th>
                                <th class="border-right">Aprobado 2016</th>
                                <th>2015</th>
                                <th>Fondos 2015</th>
                                <th>Solicitado 2016</th>
                                <th class="border-right">Aprobado 2016</th>
                                <th>2015</th>
                                <th>Fondos 2015</th>
                                <th>Solicitado 2016</th>
                                <th class="border-right">Aprobado 2016</th>
                                <th class="border-right">Total 2015</th>
                                <th class="border-right">Total Fondos</th>
                                <th class="border-right">Total Solicitado</th>
                                <th class="border-right">Total Aprobado</th>
                            </tr>
                            </thead>
                            <tbody class="color">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</div>


</div>
<!-- /.row -->
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- Modal Solicitado -->
<div class="modal fade" id="subirArchivoSolicitado" tabindex="-1" role="dialog" aria-labelledby="subirArchivoSolicitado" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-upload ic-color"></i> Subir Archivo Solicitado</h4>
            </div>
            <div class="modal-body">
                <?php
                $attributes = array(
                    'role' => 'form',
                );

                echo(form_open_multipart('anteproyecto/subir_solicitado', $attributes));
                ?>
                <div class="form-group center">
                    <div class="fileUpload btn btn-default btn-size">
                        <span>Selecciona Archivo</span>
                        <?php
                        //echo(form_label('', 'cammpoArchivo'));
                        $data = array(
                            'name'        => 'archivoSubir',
                            'id'          => 'archivoSubir',
                            'class'       => 'upload',
                            'accept' => 'application/vnd.ms-excel',
                            'required' => 'required',
                        );

                        echo(form_upload($data));
                        ?>
                    </div>
                    <p class="help-block">El archivo a subir debe de tener la extensi&oacute;n .XLS</p>
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-reply ic-color"></i> Regresar</button>
                <?php
                $atributos_submit = array(
                    'class' => 'btn btn-green',
                    'required' => 'required',
                );

                echo(form_submit($atributos_submit, 'Subir Archivo'));
                ?>
                <?php echo(form_close()); ?>
            </div>
        </div>
    </div>
</div>

<!-- Modal Aprobado -->
<div class="modal fade" id="subirArchivoAprobado" tabindex="-1" role="dialog" aria-labelledby="subirArchivoAprobado" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-upload ic-color"></i> Subir Archivo Aprobado</h4>
            </div>
            <div class="modal-body">
                <?php
                $attributes = array(
                    'role' => 'form',
                );

                echo(form_open_multipart('anteproyecto/subir_aprobado', $attributes));
                ?>
                <div class="form-group center">
                    <div class="fileUpload btn btn-default btn-size">
                        <span>Selecciona Archivo</span>
                        <?php
                        //echo(form_label('', 'cammpoArchivo'));
                        $data = array(
                            'name'        => 'archivoSubir',
                            'id'          => 'archivoSubir',
                            'class'       => 'upload',
                            'accept' => 'application/vnd.ms-excel',
                            'required' => 'required',
                        );

                        echo(form_upload($data));
                        ?>
                    </div>
                    <p class="help-block">El archivo a subir debe de tener la extensi&oacute;n .XLS</p>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-reply ic-color"></i> Regresar</button>
                <?php
                $atributos_submit = array(
                    'class' => 'btn btn-green',
                    'required' => 'required',
                );

                echo(form_submit($atributos_submit, 'Subir Archivo'));
                ?>
                <?php echo(form_close()); ?>
            </div>
        </div>
    </div>
</div>

<!-- Modal Subir 2015 -->
<div class="modal fade" id="subirArchivo2015" tabindex="-1" role="dialog" aria-labelledby="subirArchivo2015" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-upload ic-color"></i> Subir Archivo</h4>
            </div>
            <div class="modal-body">
                <?php
                $attributes = array(
                    'role' => 'form',
                );

                echo(form_open_multipart('anteproyecto/subir_2015', $attributes));
                ?>
                <div class="form-group center">
                    <div class="fileUpload btn btn-default btn-size">
                        <span>Selecciona Archivo</span>
                        <?php
                        //echo(form_label('', 'cammpoArchivo'));
                        $data = array(
                            'name'        => 'archivoSubir',
                            'id'          => 'archivoSubir',
                            'class'       => 'upload',
                            'accept' => 'application/vnd.ms-excel',
                            'required' => 'required',
                        );

                        echo(form_upload($data));
                        ?>
                    </div>
                    <p class="help-block">El archivo a subir debe de tener la extensi&oacute;n .XLS</p>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-reply ic-color"></i> Regresar</button>
                <?php
                $atributos_submit = array(
                    'class' => 'btn btn-green',
                    'required' => 'required',
                );

                echo(form_submit($atributos_submit, 'Subir Archivo'));
                ?>
                <?php echo(form_close()); ?>
            </div>
        </div>
    </div>
</div>

<!-- Modal Subir Fondos -->
<div class="modal fade" id="subirArchivoFondos" tabindex="-1" role="dialog" aria-labelledby="subirArchivoFondos" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-upload ic-color"></i> Subir Archivo</h4>
            </div>
            <div class="modal-body">
                <?php
                $attributes = array(
                    'role' => 'form',
                );

                echo(form_open_multipart('anteproyecto/subir_fondos', $attributes));
                ?>
                <div class="form-group center">
                    <div class="fileUpload btn btn-default btn-size">
                        <span>Selecciona Archivo</span>
                        <?php
                        //echo(form_label('', 'cammpoArchivo'));
                        $data = array(
                            'name'        => 'archivoSubir',
                            'id'          => 'archivoSubir',
                            'class'       => 'upload',
                            'accept' => 'application/vnd.ms-excel',
                            'required' => 'required',
                        );

                        echo(form_upload($data));
                        ?>
                    </div>
                    <p class="help-block">El archivo a subir debe de tener la extensi&oacute;n .XLS</p>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-reply ic-color"></i> Regresar</button>
                <?php
                $atributos_submit = array(
                    'class' => 'btn btn-green',
                    'required' => 'required',
                );

                echo(form_submit($atributos_submit, 'Subir Archivo'));
                ?>
                <?php echo(form_close()); ?>
            </div>
        </div>
    </div>
</div>

<!-- Modal Ver Detalle -->
<div class="modal fade modal_ver_detalle" tabindex="-1" role="dialog" aria-labelledby="modal_ver_detalle" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-eye ic-color"></i> Detalle de Fila</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-8">
                        <form role="form">
                            <div class="form-group">
                                <label>Centro de Costos</label>
                                <p class="input_view" id="detalle_centro_costos"></p>
                                <label>Partida</label>
                                <p class="input_view" id="no_partida"></p>
                                <p class="input_view" id="detalle_partida"></p>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-2"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-gray" data-dismiss="modal"><i class="fa fa-reply ic-color"></i> Regresar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Aprobado -->
<div class="modal fade" id="aprobado" tabindex="-1" role="dialog" aria-labelledby="aprobado" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-check  ic-color"></i> Aprobar Presupuesto</h4>
            </div>
            <div class="modal-body">

                <div class="form-group center">
                    <p>&#191;Est&aacute; usted seguro que desea aprobar el presupuesto?</p>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" id="boton_cerrar_presupuesto" class="btn btn-default" data-dismiss="modal">S&iacute;</button>
                <button type="button" id="boton_abrir_presupuesto" class="btn btn-default" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>






