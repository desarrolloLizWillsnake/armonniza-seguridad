    <h3 class="page-header title center"><i class="fa fa-files-o"></i> Seleccionar Centro de Costos</h3>
    <div id="page-wrapper">
        <form class="" action="<?= base_url("anteproyecto/datos_centro_de_costos") ?>" name="form" method="POST" id="datos_impresion" role="form">
            <div class="row add-pre error-gral">
                <div class="col-lg-12">
                    <div class="panel panel-default" style="margin: 0 auto; margin-top: 2%; width: 50%;">
                        <div class="panel-body">

                            <!--Tipo de Consulta-->
                            <label style="margin-top: 1%;">Seleccione las fechas para la consulta</label>
                            <!--Rango Fechas-->
                            <div class="row" style="margin-top: 1%;">
                                <div class="col-lg-6">
                                    <input type="text" class="form-control ic-calendar" name="fecha_inicial" id="fecha_inicial" placeholder="Fecha Inicial" >
                                </div>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control ic-calendar" name="fecha_final" id="fecha_final" placeholder="Fecha Final" >
                                </div>
                            </div>

                            <div class="btns-finales text-center">
                                <input class="btn btn-green" type="submit" id="consultar_reporte" value="Continuar"/>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>