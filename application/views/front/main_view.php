<div id="page-wrapper" style="background:#FFF;">
    <div class="row page-main">
        <div class="col-lg-12 center" style="margin-top: 5%;">
            <img class="logo-main" src="<?= base_url('img/logo_michoacan.png') ?>">
        </div>
        <div class="col-lg-12 center" style="margin-top: 5%;">
            <h1><b style="color: #585858;">Sistema Integral de Gesti&oacute;n</b></h1>
            <h2 style="margin-top: -1%; color:#848484;"><b>Armonniza</b></h2>
        </div>
    </div>
    <!-- === Vista 1 ===-->
    <!--
    <div class="row vst-1 vista">
        <!--Pasos Ciclo-->
    <!--
        <div class="col-lg-6">
            <div class="panel panel-default">
                <h2 class="center">Ciclo Administrativo</h2>
                <p class="c-ciclo"><button type="button" class="btn btn-inicio1 btn-circle">1</button>
                    <a href="<?= base_url("ciclo/precompromiso") ?>">Pre Compromiso</a>
                </p>
                <p class="c-ciclo"><button type="button" class="btn btn-inicio2 btn-circle">2</button>
                    <a href="<?= base_url("ciclo/compromiso") ?>">Compromisos</a>
                </p>
                <p class="c-ciclo"><button type="button" class="btn btn-inicio3 btn-circle">3</button>
                    <a href="<?= base_url("ciclo/contrarecibos") ?>">Contra Recibos de Pago</a>
                </p>
                <p class="c-ciclo"><button type="button" class="btn btn-inicio4 btn-circle">4</button>
                    <a href="<?= base_url("ciclo/movbancarios") ?>">Tesorer&iacute;a</a>
                </p>
            </div>
        </div>
        <!-- /.Pasos Ciclo-->
        <!--Adecuaciones-->
    <!--
        <div  class="col-lg-6">
            <div class="panel panel-default">
                <h2 class="center">Estructuras Administrativas</h2>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6"><h4 class="center" style="color: #585858;"><i class="fa fa-sort-amount-asc fa-fw" style="color: #B6CE33; margin-right: 3%;"></i> Ingresos</h4></div>
                        <div class="col-lg-6"><h4 class="center" style="color: #585858;"><i class="fa fa-sort-amount-desc fa-fw" style="color: #B6CE33; margin-right: 3%;"></i>Egresos</h4></div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-8">
                            <p class="c-conta c-estructura"><i style="color: #CDDD72 !important;" class="fa fa-bar-chart fa-2x"></i> Estructura Administrativa </p>
                            <p class="c-conta c-estructura"><i style="color: #CDDD72 !important;" class="fa fa-exchange fa-2x"></i> Adecuaciones Presupuestales </p>
                            <p class="c-conta c-estructura"><i style="color: #DEE9A2 !important;" class="fa fa-search fa-2x"></i> Consulta Partida </p>
                            <!-- <p class="c-conta c-estructura"><i style="color: #DEE9A2 !important;" class="fa fa-search fa-2x"></i> <a href="<?= base_url("contabilidad/auxiliar") ?>">Consulta Partida</a> </p> -->
      <!--
                        </div>
                        <div class="col-lg-2"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /. Adecuaciones-->
    <!--
    </div>
    <div class="row vst-1">
        <!--Contabilidad-->
    <!--
        <div class="col-lg-6">
            <div class="panel panel-default">
                <h2 class="center">Contabilidad</h2>
                <p class="c-conta"><i style="color: #B6CE33 !important;" class="fa fa-list-alt fa-2x"></i> <a href="<?= base_url("contabilidad/polizas") ?>">Asientos / P&oacute;lizas</a> </p>
                <p class="c-conta"><i style="color: #C1D653 !important;" class="fa fa-pie-chart fa-2x"></i> <a href="<?= base_url("contabilidad/balanza") ?>">Consulta Balanza</a> </p>
                <p class="c-conta"><i style="color: #CDDD72 !important;" class="fa fa-area-chart fa-2x"></i> <a href="<?= base_url("contabilidad/auxiliar") ?>">Auxiliar</a> </p>
                <p class="c-conta"><i style="color: #D8E592 !important;" class="fa fa-th fa-2x"></i> <a href="<?= base_url("contabilidad/matriz") ?>">Matriz de Conversi&oacute;n</a> </p>
                <p class="c-conta"><i style="color: #DEE9A2 !important;" class="fa fa-book fa-2x"></i> <a href="<?= base_url("contabilidad/plancuentas") ?>">Plan de Cuentas</a> </p>
                <p class="c-conta"><i style="color: #E3ECB2 !important;" class="fa fa-calendar-check-o fa-2x"></i> <a href="<?= base_url("contabilidad/periodoscontables") ?>">Per&oacute;dos Contables</a> </p>
                <p class="c-conta"><i style="color: #EFF4D1 !important;" class="fa fa-desktop fa-2x"></i> <a href="<?= base_url("contabilidad/contaelectronica") ?>">Contabilidad Electr&oacute;nico</a> </p>
            </div>
        </div>
        <!-- /.Contabilidad-->
        <!-- General -->
    <!--
        <div class="col-lg-6">
            <div class="panel panel-default">
                <h2 class="center">General</h2>
                <div class="panel-body">
                    <p class="c-gral"><button type="button" class="btn btn-inicio1"></button><a href="<?= base_url("catalogos/indicecatalogos") ?>">Cat&aacute;logos</a></p>
                    <p class="c-gral"><button type="button" class="btn btn-inicio2"></button><a href="<?= base_url("catalogos/indicecatalogos") ?>">Clasificadores</a></p>
                    <p class="c-gral"><button type="button" class="btn btn-inicio3"></button><a href="<?= base_url("reportes/menureportes") ?>">Reportes<a></a></p>
                    <p class="c-gral"><button type="button" class="btn btn-inicio4"></button><a href="<?= base_url("manuales/manualespoliticas") ?>">Gu&iacute;as de Usuario</a></p>
                </div>
            </div>
        </div>
        <!-- /. General -->
    <!--
    </div>
    <!-- === /.Vista 1 ===-->
    <!-- Carrusel Imagenes -->
    <!--
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicadores -->
        <!--
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
            <li data-target="#myCarousel" data-slide-to="3"></li>
            <li data-target="#myCarousel" data-slide-to="4"></li>
            <li data-target="#myCarousel" data-slide-to="5"></li>
            <li data-target="#myCarousel" data-slide-to="6"></li>
        </ol>

        <!-- Contenedor Imagen -->
        <!--
        <div class="carousel-inner" role="listbox">

            <div class="item active">
                <img src="<?= base_url('img/slider-armonniza/0.png') ?>" style="width:30%; margin-top: 10%; margin-bottom: 12%;">
            </div>

            <div class="item">
                <img src="<?= base_url('img/slider-armonniza/6.png') ?>">
            </div>

            <div class="item">
                <img src="<?= base_url('img/slider-armonniza/5.png') ?>">
            </div>

            <div class="item">
                <img src="<?= base_url('img/slider-armonniza/4.png') ?>">
            </div>

            <div class="item">
                <img src="<?= base_url('img/slider-armonniza/3.png') ?>">
            </div>

            <div class="item">
                <img src="<?= base_url('img/slider-armonniza/2.png') ?>">
            </div>

            <div class="item">
                <img src="<?= base_url('img/slider-armonniza/1.png') ?>" style="width:38%;">
            </div>
        </div>
    </div>
</div>-->

</div>
<!-- /.row -->
</div>
<!-- /#page-wrapper -->