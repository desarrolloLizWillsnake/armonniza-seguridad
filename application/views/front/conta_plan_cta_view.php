<h3 class="page-header title center"><i class="fa fa-book"></i> Plan de Cuentas Contables</h3>
<div id="page-wrapper">
    <div class="row cont-btns-c center">
        <div class="col-lg-12">
            <a href="#" class="btn btn-default" data-toggle="modal" data-target="#modal_agregar"><i class="fa fa-plus circle" style="color: #B6CE33;" ></i> Agregar Cuenta Contable</a>
            <a href="#" class="btn btn-default"><i class="fa fa-print circle" style="color: #B6CE33;" ></i> Imprimir</a>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body table-gral">
                    <div class="table-responsive">
                        <div id="dataTables-example_wrapper" class="dataTables_wrapper form-inline" role="grid">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="dataTables_length" id="dataTables-example_length"><label>Mostrar <select name="dataTables-example_length" aria-controls="dataTables-example" class="form-control input-sm"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> registros</label></div></div><div class="col-sm-6"><div id="dataTables-example_filter" class="dataTables_filter"><label>Buscar  <input type="search" class="form-control input-sm" aria-controls="dataTables-example"></label></div></div></div><table class="table table-striped table-bordered table-hover dataTable no-footer" id="dataTables-example" aria-describedby="dataTables-example_info">
                                <thead>
                                <tr role="row">
                                    <th class="sorting_asc" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" aria-sort="ascending" style="width: 100px;">No. Cuenta</th>
                                    <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 350px;">Nombre Cuenta</th>
                                    <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 100px;">Tipo Cuenta</th>
                                    <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 100px;">Grupo</th>
                                    <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 120px;">Acciones</th></tr>
                                </thead>
                                <tbody>
                                <tr class="gradeA odd">
                                    <td class="sorting_1"></td>
                                    <td class=" "> </td>
                                    <td class="center "> </td>
                                    <td class="center "> </td>
                                    <td class="center "> </td>
                                </tr>
                                </tbody>
                            </table><div class="row"><div class="col-sm-6"><div class="dataTables_info" id="dataTables-example_info" role="alert" aria-live="polite" aria-relevant="all">Mostrando del 0 al 0 de un total de 0 registros</div></div><div class="col-sm-6"><div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate"><ul class="pagination"><li class="paginate_button previous disabled" aria-controls="dataTables-example" tabindex="0" id="dataTables-example_previous"><a href="#">Anterior</a></li><li class="paginate_button active" aria-controls="dataTables-example" tabindex="0"><a href="#">1</a></li><li class="paginate_button " aria-controls="dataTables-example" tabindex="0"><a href="#">2</a></li><li class="paginate_button next" aria-controls="dataTables-example" tabindex="0" id="dataTables-example_next"><a href="#">Siguiente</a></li></ul></div></div></div></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal_agregar" tabindex="-1" role="dialog" aria-labelledby="modal_agregar" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-plus-circle ic-modal"></i> Agregar Plan de Cuenta</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-3 col-md-3"></div>
                    <div class="col-xs-6 col-md-6">
                        <h4 class="text-center">Asientos Contables</h4>

                        <!--No. Cuenta-->
                        <input type="text" class="form-control" name="num_cuenta" id="num_cuenta" placeholder="No. Cuenta" style="margin: 5% 0%;">

                        <!--Nombre Cuenta-->
                        <input type="text" class="form-control" name="nom_cuenta" id="nom_cuenta" placeholder="Nombre Cuenta" >

                        <!--Grupo Cuenta-->
                        <!--<select class="form-control" id="grupo_cta" name="grupo_cta">
                            <option value="">Grupo Cuenta</option>
                            <option value="">Grupo 1</option>
                            <option value="">Grupo 2</option>
                        </select>-->

                        <!--Descripción Cuenta-->
                        <textarea class="form-control" name="" id="" cols="10" rows="10" style="margin: 5% 0%;" placeholder="Descripción"></textarea>

                        <!--Tipo Cuenta-->
                        <label>Tipo Cuenta</label>
                        <div class="radio">
                            <label>
                                <input type="radio" name="tipo_radio" id="optionsRadios1" value="1" checked>Acumulativa
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="tipo_radio" id="optionsRadios2" value="2">Detalle
                            </label>
                        </div>
                    </div>
                    <div class="col-xs-3 col-md-3"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_cancelar_precompromiso">Aceptar</button>
            </div>
        </div>
    </div>
</div>



</div>
<!-- /.row -->
</div>
<!-- /#page-wrapper -->