<h3 class="page-header center"><i class="fa fa-edit"></i> Editar Compromiso</h3>
<div id="page-wrapper">
    <form class="forma_compromiso" role="form">
        <div class="row add-pre error-gral">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        General
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <input type="hidden" name="ultimo_compromiso" id="ultimo_compromiso" value="<?= $ultimo ?>" />
                        <div class="row">
                            <div class="col-lg-12" id="resutado_insertar_superior_detalle"></div>
                            <!--Primera Columna-->
                            <div class="col-lg-4">
                                <!---No. Compromiso-->
                                <div class="form-group" style="margin-bottom: 1%;">
                                    <div class="row">
                                        <div class="col-lg-6"><label>No. Compromiso</label></div>
                                        <div class="col-lg-6"><p class="form-control-static input_ver"><?= $ultimo ?></p></div>
                                    </div>
                                </div>
                                <!--No. Precompromiso-->
                                <div class="row marg_sup2">
                                    <div class="col-lg-6"><label>No. Precompromiso</label></div>
                                    <div class="col-lg-6">
                                        <div class="form-group input-group">
                                            <input type="text" class="form-control ic-buscar-input" name="no_precompromiso" id="no_precompromiso" placeholder="No. Precompromiso"<?= $num_precompromiso != NULL ? ' value="'.$num_precompromiso.'"' : '';?> style="margin-top:-5%;"/>
                                            <span class="input-group-btn ic-buscar-btn">
                                                <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_precompromiso"><i class="fa fa-search"></i></button>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <!--Tipo de Compromiso-->
                                <select class="form-control marg_sup1" id="tipo_compromiso" name="tipo_compromiso">
                                    <option value="">Tipo de Precompromiso</option>
                                    <option value="Bienes"<?= $tipo_compromiso == 'Bienes' ? ' selected="selected"' : '';?>>Bienes</option>
                                    <option value="Servicio"<?= $tipo_compromiso == 'Servicio' ? ' selected="selected"' : '';?>>Servicio</option>
                                    <option value="Obra"<?= $tipo_compromiso == 'Obra' ? ' selected="selected"' : '';?>>Obra</option>
                                    <option value="Honorarios"<?= $tipo_compromiso == 'Honorarios' ? ' selected="selected"' : '';?>>Honorarios</option>
                                    <option value="Contrato Pedido"<?= $tipo_compromiso == 'Contrato Pedido' ? ' selected="selected"' : '';?>>Contrato Pedido</option>
                                    <option value="Fondo Revolvente"<?= $tipo_compromiso == 'Fondo Revolvente' ? ' selected="selected"' : '';?>>Fondo Revolvente</option>
                                    <option value="Gastos a Comprobar"<?= $tipo_compromiso == 'Gastos a Comprobar' ? ' selected="selected"' : '';?>>Gastos a Comprobar</option>
                                    <option value="Viáticos Nacionales"<?= $tipo_compromiso == 'Viáticos Nacionales' ? ' selected="selected"' : '';?>>Viáticos Nacionales</option>
                                    <option value="Viáticos Internacionales"<?= $tipo_compromiso == 'Viáticos Internacionales' ? ' selected="selected"' : '';?>>Viáticos Internacionales</option>
                                </select>

                                <!-- Concepto Específico -->
                                <input type="text" class="form-control" name="concepto_especifico" id="concepto_especifico" placeholder="Concepto Específico" <?= $concepto_especifico != NULL ? ' value="'.$concepto_especifico.'"' : '';?> maxlength="120" required="required" />

                                <!--Descripción Compromiso-->
                                <textarea class="form-control" id="descripcion_general" name="descripcion_general" placeholder="Justificación del Precompromiso" style="height: 7em;" <?= $descripcion_general != NULL ? '> '.$descripcion_general.'</textarea>' : '> </textarea>' ;?>

                            </div>
                            <!--Fin Primera Columna-->
                            <!--Segunda Columna-->
                            <div class="col-lg-5">
                                <!-- Proveedor -->
                                <div class="form-group input-group forma_caratula_normal">
                                    <input type="hidden" name="id_proveedor" id="id_proveedor" <?= $id_proveedor != NULL ? ' value="'.$id_proveedor.'"' : '';?> />
                                    <input type="text" class="form-control" name="proveedor" id="proveedor" placeholder="Proveedor" <?= $nombre_proveedor != NULL ? ' value="'.$nombre_proveedor.'"' : '';?> />
                                    <span class="input-group-btn ic-buscar-btn">
                                        <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_proveedores"><i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                </div>

                                <!-- Lugar de Entrega-->
                                <input type="text" class="form-control forma_caratula_normal" name="lugar_entrega" id="lugar_entrega" placeholder="Lugar de Entrega" <?= $lugar_entrega != NULL ? ' value="'.$lugar_entrega.'"' : '';?> />

                                <!-- Condición de Entrega-->
                                <input type="text" class="form-control forma_caratula_normal" name="condicion_entrega" id="condicion_entrega" placeholder="Condición de Entrega" <?= $condicion_entrega != NULL ? ' value="'.$condicion_entrega.'"' : '';?> />

                                <!--Tipo de Formato Impresión-->
                                <input type="hidden" name="id_tipo_impresion" id="id_tipo_impresion" value="<?= $num_impresion ?>" />
                                <select class="form-control forma_caratula_normal" id="tipo_impresion" name="tipo_impresion" required="required">
                                    <option value="">Formato de Impresión</option>
                                    <option value="Orden de Compra"<?= $tipo_impresion == 'Orden de Compra' ? ' selected="selected"' : '';?>>Orden de Compra</option>
                                    <option value="Orden de Servicio"<?= $tipo_impresion == 'Orden de Servicio' ? ' selected="selected"' : '';?>>Orden de Servicio</option>
                                    <option value="Orden de Trabajo"<?= $tipo_impresion == 'Orden de Trabajo' ? ' selected="selected"' : '';?>>Orden de Trabajo</option>
                                    <option value="Contrato Pedido"<?= $tipo_impresion == 'Contrato Pedido' ? ' selected="selected"' : '';?>>Contrato Pedido</option>
                                </select>

                                <!-- Tipo de Gasto-->
                                <label class="forma_caratula_normal">Tipo de Gastos</label>
                                <div class="radio forma_caratula_normal">
                                    <label>
                                        <input type="radio" name="tipo_radio" id="optionsRadios1" value="1" <?= $tipo_gasto == '1' ? ' checked="checked"' : '';?>>Gasto Corriente
                                    </label>
                                </div>
                                <div class="radio forma_caratula_normal">
                                    <label>
                                        <input type="radio" name="tipo_radio" id="optionsRadios2" value="2" <?= $tipo_gasto == '2' ? ' checked="checked"' : '';?>>Gasto de Capital
                                    </label>
                                </div>
                                <div class="radio forma_caratula_normal">
                                    <label>
                                        <input type="radio" name="tipo_radio" id="optionsRadios3" value="3" <?= $tipo_gasto == '3' ? ' checked="checked"' : '';?>>Amortización de la cuenta y disminución de pasivos
                                    </label>
                                </div>

                            </div>
                            <!--Fin Segunda Columna-->
                            <!--Tercer Columna-->
                            <div class="col-lg-3">
                                <!--<label>Fecha de Solicitud</label>-->
                                <input ype="text" class="form-control ic-calendar" name="fecha_solicitud" id="fecha_solicitud" placeholder="Fecha de Solicitud" <?= $fecha_emision != NULL ? ' value="'.$fecha_emision.'"' : '';?> />

                                <!--<label>Fecha de Autorizacion</label>-->
                                <?php if($this->utilerias->get_grupo() == 1) { ?>
                                <input type="text" class="form-control ic-calendar" name="fecha_autoriza" id="fecha_autoriza" placeholder="Fecha de Autorización" <?= $fecha_autoriza != NULL ? ' value="'.$fecha_autoriza.'"' : '';?> />
                                <?php } else { ?>
                                <input type="hidden" name="fecha_autoriza" id="fecha_autoriza" value="0" />
                                <?php } ?>

                                <!--<label>Fecha de Entrega</label>-->
                                <input type="text" class="form-control ic-calendar forma_caratula_normal" name="fecha_entrega" id="fecha_entrega" placeholder="Fecha de Entrega" <?= $fecha_programada != NULL ? ' value="'.$fecha_programada.'"' : '';?> />

                                <!--Firmas-->
                                <div class="form-group">
                                    <input type="hidden" name="firma1" id="firma1" value="<?= $firma1 ?>" />
                                    <input type="hidden" name="firma2" id="firma2" value="<?= $firma2 ?>" />
                                    <input type="hidden" name="firma3" id="firma3" value="<?= $firma3 ?>" />
                                    <label style="margin-top:6%;"> Firmas de Autorización</label>
                                    <?php
                                    
                                    $grupo = $this->utilerias->get_grupo();
                                    $firmas = $this->utilerias->get_firmas_arreglo();

                                    if($firmas) {
                                    foreach ($firmas as $key) {
                                        if($key->firma == 1) { ?>
                                            <div class="checkbox">
                                                <label><input type="checkbox" name="firma1_check" id="firma1_check" <?= $firma1 == 1 ? ' checked="checked" disabled' : '';?> >Programación y Evaluación</label>
                                            </div>
                                        <?php }
                                        if($key->firma == 2) { ?>
                                            <div class="checkbox">
                                                <label><input type="checkbox" name="firma2_check" id="firma2_check" <?= $firma2 == 1 ? ' checked="checked" disabled' : '';?> >Recursos Materiales</label>
                                            </div>
                                        <?php }
                                        if($key->firma == 3) { ?>
                                            <div class="checkbox">
                                                <label><input type="checkbox" name="firma3_check" id="firma3_check" <?= $firma3 == 1 ? ' checked="checked" disabled' : '';?> >Gerencia Técnica Administrativa</label>
                                            </div>
                                        <?php }
                                        }
                                    } elseif($grupo == 1) { ?>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="firma1_check" id="firma1_check" <?= $firma1 == 1 ? ' checked="checked" disabled' : '';?> >Programación y Evaluación</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="firma2_check" id="firma2_check" <?= $firma2 == 1 ? ' checked="checked" disabled' : '';?> >Recursos Materiales</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="firma3_check" id="firma3_check" <?= $firma3 == 1 ? ' checked="checked" disabled' : '';?> >Gerencia Técnica Administrativa</label>
                                        </div>
                                    <?php } ?>

                                </div>
                            </div>
                            <!--Fin Tercer Columna-->
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row add-pre error-detalle forma_fondo" style="display: none;">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Datos para Viáticos, Fondo Revolvente y Gastos a Comprobar
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <h5 class="center"><b>Datos del Comisionado o Beneficiario</b></h5>
                            </div>
                            <div class="row" style="margin: 1% 0%;">
                                <div class="col-lg-4">
                                    <input type="hidden" name="id_persona" id="id_persona" <?= $id_persona != NULL ? ' value="'.$id_persona.'"' : 'value="0"';?> />
                                    <!-- Beneficiario -->
                                    <div class="form-group input-group">
                                        <input type="text" class="form-control" name="nombre_persona" id="nombre_persona" placeholder="Nombre de la Persona" <?= $nombre_completo != NULL ? ' value="'.$nombre_completo.'"' : '';?> required="required" />
                                        <span class="input-group-btn ic-buscar-btn">
                                            <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_beneficiarios"><i class="fa fa-search"></i></button>
                                        </span>
                                    </div>
                                    <input type="text" class="form-control" name="no_empleado" id="no_empleado" placeholder="No. Empleado" <?= $no_empleado != NULL ? ' value="'.$no_empleado.'"' : '';?> disabled required="required" />
                                </div>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control" name="puesto" id="puesto" placeholder="Puesto o Categoría" <?= $puesto != NULL ? ' value="'.$puesto.'"' : '';?> disabled required="required" />
                                    <input type="text" class="form-control" name="area" id="area" placeholder="Área de Adscripción" <?= $area != NULL ? ' value="'.$area.'"' : '';?> disabled required="required" />
                                </div>
                                <div class="col-lg-4">
                                    <!-- Centro de Costos -->
                                    <div class="form-group input-group">
                                        <input type="text" class="form-control" name="clave" id="clave" placeholder="Centro de Costos" <?= $clave != NULL ? ' value="'.$clave.'"' : '';?> disabled required="required" />
                                        <span class="input-group-btn ic-buscar-btn">
                                            <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_centro_costo"><i class="fa fa-search"></i></button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row add-pre error-detalle forma_viaticos" style="display: none;">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Detalle de Viáticos
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-4">
                                <select class="form-control" id="forma_pago" name="forma_pago" disabled required="required">
                                    <option value="">Forma de Pago</option>
                                    <option value="Cheque"<?= $forma_pago == 'Cheque' ? ' selected="selected"' : '';?>>Cheque</option>
                                    <option value="Transferencia"<?= $forma_pago == 'Transferencia' ? ' selected="selected"' : '';?>>Transferencia</option>
                                </select>

                                <select class="form-control" id="transporte" name="transporte" disabled required="required">
                                    <option value="">Medio de Transporte</option>
                                    <option value="Autobús"<?= $transporte == 'Autobús' ? ' selected="selected"' : '';?>>Autobús</option>
                                    <option value="Avión"<?= $transporte == 'Avión' ? ' selected="selected"' : '';?>>Avión</option>
                                    <option value="Auto Particular"<?= $transporte == 'Auto Particular' ? ' selected="selected"' : '';?>>Auto Particular</option>
                                    <option value="Auto de la Empresa"<?= $transporte == 'Auto de la Empresa' ? ' selected="selected"' : '';?>>Auto de la Empresa</option>
                                </select>

                                <select class="form-control" id="grupo_jerarquico" name="grupo_jerarquico" disabled required="required">
                                    <option value="">Grupos Jerárquicos</option>
                                    <option value="K hasta G"<?= $grupo_jerarquico == 'K hasta G' ? ' selected="selected"' : '';?>>K hasta G</option>
                                    <option value="P hasta L"<?= $grupo_jerarquico == 'P hasta L' ? ' selected="selected"' : '';?>>P hasta L</option>
                                    <option value="Personal Operativo"<?= $grupo_jerarquico == 'Personal Operativo' ? ' selected="selected"' : '';?>>Personal Operativo</option>
                                </select>
                            </div>
                            <div class="col-lg-4">
                            </div>
                            <div class="col-lg-4">
                                <label>Tipo de Zona</label>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="zona_marginada_check" id="zona_marginada_check" <?= $zona_marginada == 1 ? ' checked="checked" disabled' : '';?> />Zona Marginada</label>
                                </div>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="zona_mas_economica_check" id="zona_mas_economica_check" <?= $zona_mas_economica == 1 ? ' checked="checked" disabled' : '';?> />Zona más Económica</label>
                                </div>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="zona_menos_economica_check" id="zona_menos_economica_check" <?= $zona_menos_economica == 1 ? ' checked="checked" disabled' : '';?> />Zona menos Económica</label>
                                </div>
                            </div>
                        </div>

                        <hr />

                        <div class="row">
                            <div class="col-lg-4">
                                <input type="text" class="form-control" name="lugar_comision" id="lugar_comision" placeholder="Lugar de Comisión" disabled />
                                <!--Cuota Diaria-->
                                <div class="form-group input-group">
                                    <span class="input-group-btn ic-peso-btn">
                                        <button class="btn btn-default sg-dollar" ><i class="fa fa-dollar"></i></button>
                                    </span>
                                    <input style="margin-top: .5%;" type="text" pattern="[0-9]+([\.|,][0-9]+)?" class="form-control dinero" name="cuota_diaria" id="cuota_diaria" placeholder="Cuota Diaria" disabled />
                                </div>
                            </div>
                            <div  class="col-lg-4"></div>
                            <div class="col-lg-4">
                                <input type="text" class="form-control ic-calendar" name="fecha_inicio" id="fecha_inicio" placeholder="Fecha Inicio" disabled />
                                <input type="text" class="form-control ic-calendar" name="fecha_termina" id="fecha_termina" placeholder="Fecha Termino" disabled />
                                <input type="text" class="form-control dias" name="dias" id="dias" placeholder="Dias" value="1.0" disabled />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <button type="button" class="btn btn-green btn-block btn-gd" id="guardar_compromiso_viaticos_detalle">Guardar Detalle Viáticos</button>
                                <div id="resultado_insertar_viaticos_detalle"></div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-body table-gral">
                                        <div class="table-responsive">
                                            <h4 id="suma_total_viaticos" class="text-center"></h4>
                                            <input type="hidden" value="" name="total_viaticos_hidden" id="total_viaticos_hidden" />

                                            <table class="table table-striped table-bordered table-hover" id="tabla_datos_viaticos_compromiso">
                                                <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Lugar de Comisión</th>
                                                    <th>Fecha Inicio</th>
                                                    <th>Fecha Término</th>
                                                    <th>Días</th>
                                                    <th>Cuota Diaria</th>
                                                    <th>Importe Total</th>
                                                    <th>Acciones</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row add-pre error-detalle">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Detalles
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="text-center">
                            <a id="btn_detalle" class="btn btn-default"><i class="fa fa-plus-square ic-color"></i> Agregar Detalle</a>
                        </div>
                        <br>
                        <div id="detalle" style="display: none;">
                            <div class="row">
                                <div class="col-lg-2 niveles-pc">
                                    <!--Estructura Administrativa de Egresos-->
                                    <?= $niveles ?>
                                    <a href="#modal_estructura" class="btn btn-default" data-toggle="modal" data-target="#modal_estructura">¿No conoces la
                                        <br/>estructura?</a>
                                </div>
                                <div class="col-lg-6">
                                    <div id="mes_grafica" class="text-center text-muted"></div>
                                    <div id="grafica_partida"></div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group input-group">
                                        <input type="text" class="form-control" style="margin-top: .3%;" name="gasto" id="gasto" placeholder="Gasto" />
                                        <span class="input-group-btn ic-buscar-btn">
                                            <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_gasto"><i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                    </div>

                                    <input id="titulo_gasto" class="form-control" name="titulo_gasto" placeholder="Título"  />

                                    <input type="text" class="form-control" name="u_medida" id="u_medida" placeholder="Unidad de Medida" />

                                    <input type="number" pattern="[0-9]+" class="form-control" name="cantidad" id="cantidad" placeholder="Cantidad" />

                                    <div class="form-group input-group">
                                        <span class="input-group-btn ic-peso-btn">
                                            <button class="btn btn-default sg-dollar" ><i class="fa fa-dollar"></i>
                                            </button>
                                        </span>
                                        <input type="text" pattern="[0-9]+([\.|,][0-9]+)?" class="form-control dinero" name="precio" id="precio" placeholder="Precio Unitario"  />
                                    </div>

                                    <!--Iva Detalle-->
                                    <label class="label-iva">IVA</label>
                                    <div class="iva">
                                        <div class="radio">
                                            <label> <input type="radio" name="iva" id="iva1" value="1">Sí </label>
                                        </div>
                                        <div class="radio">
                                            <label> <input type="radio" name="iva" id="iva2" value="0" checked>No </label>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <textarea class="form-control" id="descripcion_detalle" name="descripcion_detalle" placeholder="Descripción Detalle" style="height: 7em;"></textarea>
<!--                                    <div class="btn-toolbar toolbar-editor" data-role="editor-toolbar" data-target="#editor">-->
<!--                                        <div class="btn-group">-->
<!--                                            <a class="btn" data-edit="bold" title="Negrita (Ctrl/Cmd+B)"><i class="fa fa-bold"></i></a>-->
<!--                                            <a class="btn" data-edit="italic" title="Cursica (Ctrl/Cmd+I)"><i class="fa fa-italic"></i></a>-->
<!--                                            <a class="btn" data-edit="underline" title="Subrayado (Ctrl/Cmd+U)"><i class="fa fa-underline"></i></a>-->
<!--                                        </div>-->
<!--                                        <div class="btn-group">-->
<!--                                            <a class="btn" data-edit="insertunorderedlist" title="Lista Desordenada"><i class="fa fa-list-ul"></i></a>-->
<!--                                            <a class="btn" data-edit="insertorderedlist" title="Lista Ordenada"><i class="fa fa-list-ol"></i></a>-->
<!--                                            <a class="btn" data-edit="outdent" title="Quitar sangría (Shift+Tab)"><i class="fa fa-outdent"></i></a>-->
<!--                                            <a class="btn" data-edit="indent" title="Insertar sagnría (Tab)"><i class="fa fa-indent"></i></a>-->
<!--                                        </div>-->
<!--                                        <div class="btn-group">-->
<!--                                            <a class="btn" data-edit="justifyleft" title="Alinear Izquierda (Ctrl/Cmd+L)"><i class="fa fa-align-left"></i></a>-->
<!--                                            <a class="btn" data-edit="justifycenter" title="Alinear Centro (Ctrl/Cmd+E)"><i class="fa fa-align-center"></i></a>-->
<!--                                            <a class="btn" data-edit="justifyright" title="Alinear Derecha (Ctrl/Cmd+R)"><i class="fa fa-align-right"></i></a>-->
<!--                                            <a class="btn" data-edit="justifyfull" title="Justificado (Ctrl/Cmd+J)"><i class="fa fa-align-justify"></i></a>-->
<!--                                        </div>-->
<!---->
<!--                                        <div class="btn-group">-->
<!--                                            <a class="btn" data-edit="undo" title="Deshacer (Ctrl/Cmd+Z)"><i class="fa fa-undo"></i></a>-->
<!--                                            <a class="btn" data-edit="redo" title="Rehacer (Ctrl/Cmd+Y)"><i class="fa fa-repeat"></i></a>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                    <div class="editor" id="descripcion_detalle">Descripción Detalle</div>-->
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <button type="button" class="btn btn-green btn-block btn-gd" id="guardar_compromiso_detalle">Guardar Partida</button>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-body table-gral">
                                        <div class="table-responsive">
                                            <h4 id="suma_total" class="text-center"></h4>
                                            <div id="resultado_insertar_detalle"></div>
                                            <input type="hidden" value="" name="subtotal_hidden" id="subtotal_hidden" />
                                            <input type="hidden" value="" name="total_hidden" id="total_hidden" />
                                            <table class="table table-striped table-bordered table-hover" id="tabla_datos_compromiso">
                                                <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th width="6%">F.F.</th>
                                                    <th>P.F.</th>
                                                    <th width="6%">C.C.</th>
                                                    <th>Capítulo</th>
                                                    <th>Concepto</th>
                                                    <th>Partida</th>
                                                    <th>Gasto</th>
                                                    <th>U/M</th>
                                                    <th>Cant.</th>
                                                    <th>Precio U.</th>
                                                    <th>Subtotal</th>
                                                    <th>IVA</th>
                                                    <th>Total</th>
                                                    <th>Título</th>
                                                    <th>Descripción</th>
                                                    <th>Acciones</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-body text-center">
                <div class="form-group c-firme">
                    <h3>¿Compromiso en Firme?</h3>
                    <label class="checkbox-inline">
                        <input type="checkbox" id="check_firme" name="check_firme" <?= $enfirme == 1 ? ' checked="checked" disabled' : 'disabled';?> />Sí
                    </label>
                </div>
            </div>
        </div>

        <div class="btns-finales text-center">
            <input type="submit" id="verificar_presupuesto" name="verificar_presupuesto" class="btn btn-default" value="Verificar Presupuesto" />
            <br />
            <div class="text-center" id="resultado_verificado"></div>
        </div>

        <div class="btns-finales text-center">
            <div class="text-center" id="resultado_insertar_caratula"></div>
            <a class="btn btn-default" href="<?= base_url("ciclo/compromiso") ?>"><i class="fa fa-reply ic-color"></i> Regresar</a>
            <input type="submit" id="guardar_compromiso" class="btn btn-green" name="guardar_compromiso" value="Guardar Compromiso" disabled/>
        </div>
    </form>


</div>

</div>
<!-- /.row -->

<!-- Modal Precompromiso -->
<div class="modal fade" id="modal_precompromiso" tabindex="-1" role="dialog" aria-labelledby="modal_precompromiso" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-file ic-modal"></i> Precompromisos</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-pre">
                <input type="hidden" name="hidden_no_precompromiso" id="hidden_no_precompromiso" />
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tabla_precompromiso">
                        <thead>
                        <tr>
                            <th width="10%">No.</th>
                            <th width="15%">Tipo</th>
                            <th width="10%">F. Emitido</th>
                            <th width="10%">F. Entrega</th>
                            <th width="15%">Importe</th>
                            <th width="10%">Creado Por</th>
                            <th width="10%">Firme</th>
                            <th width="10%">Estatus</th>
                            <th width="10%">Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Proveedores -->
<!--<div class="modal fade" id="modal_proveedores" tabindex="-1" role="dialog" aria-labelledby="modal_proveedores" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-users ic-modal"></i> Proveedores</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-prove">
                <div class="table-responsive">
                    <input type="hidden" name="hidden_clave_proveedor" id="hidden_clave_proveedor" value="" />
                    <table class="table table-striped table-bordered table-hover" id="tabla_proveedores">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Clave</th>
                            <th>Nombre Comercial</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>-->

<!--Parte provisional-->

<div class="modal fade" id="modal_proveedores" tabindex="-1" role="dialog" aria-labelledby="modal_proveedores" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-users ic-modal"></i> Proveedores</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-prove">
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-4 center">
                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal_proveedores_armonniza">Armonniza</button>
                    </div>
                    <div class="col-md-4 center">
                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal_proveedores_educal">EDUCAL</button>
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-reply ic-color"></i> Regresar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_proveedores_armonniza" tabindex="-1" role="dialog" aria-labelledby="modal_proveedores_armonniza" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-users ic-modal"></i> Proveedores Armonniza</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-3">
                <div class="table-responsive">
                    <input type="hidden" name="hidden_clave_proveedor" id="hidden_clave_proveedor" value="" />
                    <table class="table table-striped table-bordered table-hover" id="tabla_proveedores">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Clave</th>
                            <th>Nombre Comercial</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_proveedores_educal" tabindex="-1" role="dialog" aria-labelledby="modal_proveedores_educal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-users ic-modal"></i> Proveedores EDUCAL</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-3">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tabla_proveedores_educal">
                        <thead>
                        <tr>
                            <th>Clave</th>
                            <th>Nombre Comercial</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Fin de parte provisional-->

<!-- Modal estructura -->
<div class="modal fade" id="modal_estructura" tabindex="-1" role="dialog" aria-labelledby="modal_estructura" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-indent-left" style="color: #25B7BC;"></span> Estructura de Egresos</h4>
            </div>
            <div class="modal-body">
                <?= $niveles_modal ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" disabled id="elegir_ultimoNivel">Elegir</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Gasto -->
<div class="modal fade" id="modal_gasto" tabindex="-1" role="dialog" aria-labelledby="modal_gasto" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-usd" style="color: #25B7BC;"></span> Gastos</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-gasto">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tabla_gastos">
                        <thead>
                        <tr>
                            <th width="13%">Clave</th>
                            <th width="15%">Clasificación</th>
                            <th width="45%">Descripción</th>
                            <th width="7%">U/M</th>
                            <th width="10%">Existencia</th>
                            <th  width="13%">Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" disabled>Elegir</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Borrar Detalle -->
<div class="modal fade modal_borrar" tabindex="-1" role="dialog" aria-labelledby="modal_borrar" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-trash ic-modal"></i> Eliminar Partida</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="message-text" class="control-label">¿Realmente desea eliminar la partida seleccionada?</label>
                        <input type="hidden" value="" name="borrar_compromiso_hidden" id="borrar_compromiso_hidden" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_cancelar_compromiso">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Borrar Detalle Viáticos -->
<div class="modal fade modal_borrar_detalle_viaticos" tabindex="-1" role="dialog" aria-labelledby="modal_borrar_detalle_viaticos" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-trash ic-modal"></i> Eliminar Viático</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="message-text" class="control-label">¿Realmente desea eliminar el viático seleccionado?</label>
                        <input type="hidden" value="" name="borrar_viatico_compromiso_hidden" id="borrar_viatico_compromiso_hidden" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_borrar_viatico">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Editar Detalle -->
<div class="modal fade modal_editar" tabindex="-1" role="dialog" aria-labelledby="modal_editar" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-edit ic-modal"></i> Editar Partida</h4>
            </div>
            <div class="modal-body modal_edit_detalle">
                <form role="form">
                    <div class="form-group" style="margin: 0 auto; width: 60%;">
                        <label for="message-text" class="control-label">Cantidad</label>
                        <input class="form-control" type="text" value="" name="editar_cantidad_compromiso" id="editar_cantidad_compromiso" />

                        <!--Iva Detalle-->
                        <label class="label-iva">IVA</label>
                        <div class="iva">
                            <div class="radio">
                                <label> <input type="radio" name="iva_editar" id="iva_editar1" value="1">Sí </label>
                            </div>
                            <div class="radio">
                                <label> <input type="radio" name="iva_editar" id="iva_editar2" value="0" checked>No </label>
                            </div>
                        </div>

                        <label for="message-text" class="control-label">Precio Unitario</label>
                        <div class="form-group input-group">
                            <span class="input-group-btn ic-peso-btn">
                                <button class="btn btn-default sg-dollar" style="margin-top: 1%;" disabled><i class="fa fa-dollar"></i></button>
                            </span>
                            <input class="form-control dinero" type="text" pattern="[0-9]+([\.|,][0-9]+)?"  value="" name="editar_precio_compromiso" id="editar_precio_compromiso" required="required"/>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Regresar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_editar_compromiso">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Centros de Costo -->
<div class="modal fade" id="modal_centro_costo" tabindex="-1" role="dialog" aria-labelledby="modal_centro_costo" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-users ic-modal"></i> Centro de Costo</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-3">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tabla_centro_costos">
                        <thead>
                        <tr>
                            <th width="15%">Centro de Costo</th>
                            <th width="75%">Nombre</th>
                            <th width="10%">Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Beneficiarios -->
<div class="modal fade" id="modal_beneficiarios" tabindex="-1" role="dialog" aria-labelledby="modal_beneficiarios" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-users ic-modal"></i> Beneficiarios</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-5">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tabla_beneficiarios">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>No. Empleado</th>
                            <th>Nombre</th>
                            <th>Adscripción</th>
                            <th>Cargo</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Ver Detalle -->
<div class="modal fade modal_ver" tabindex="-1" role="dialog" aria-labelledby="modal_ver" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-eye ic-modal"></i> Detalle de Partida</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <p id="detalle_partida"></p>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-green" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

</div>
<!-- /#page-wrapper -->