<h3 class="page-header title center"><i class="fa fa-shopping-cart fa-fw"></i> Canales de Venta </h3>
<div id="page-wrapper">
    <div class="row cont_canales">
        <div class="col-lg-4 text-center">
            <h3><i class="fa fa-building-o"></i> Librerías</h3>
            <div class="cont_detalles" style="margin-top: 8%;"><h4>Corporativo</h4></div>
            <div class="cont_detalles"><h4>Zona Sur</h4></div>
            <div class="cont_detalles"><h4>Zona Centro</h4></div>
            <div class="cont_detalles"><h4>Zona Norte</h4></div>
            <div class="cont_detalles"><h4>Librobus</h4></div>
            <div class="cont_detalles"><h4>Ferias del Libro</h4></div>
        </div>
        <div class="col-lg-4 text-center">
            <h3><i class="fa fa-tasks"></i> Patrimonio</h3>
            <div class="cont_detalles" style="margin-top: 8%;"><h4>Inventario</h4></div>
        </div>
        <div class="col-lg-4 text-center">
            <h3><i class="fa fa-inbox"></i> Tesorería</h3>
            <div class="cont_detalles" style="margin-top: 8%;"><h4>Fondo Revolvente</h4></div>

        </div>
    </div>
</div>