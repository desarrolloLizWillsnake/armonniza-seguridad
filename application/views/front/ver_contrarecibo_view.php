<h3 class="page-header center"><i class="fa fa-eye"></i> Ver Contra Recibo</h3>
<div id="page-wrapper">
    <form class="forma_contrarecibo" role="form">
        <div class="row add-pre">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        General
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <input type="hidden" name="ultimo_contrarecibo" id="ultimo_contrarecibo" value="<?= $ultimo ?>">
                        <div class="row">
                            <!--Primera Columna-->
                            <div class="col-lg-4">
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-lg-6"><label>No. Contra Recibo</label></div>
                                        <div class="col-lg-6">
                                            <?php if(isset($id_contrarecibo_caratula)) { ?>
                                                <p class="form-control-static input_view"><?= $id_contrarecibo_caratula ?></p>
                                            <?php } else { ?>
                                                 <p class="form-control-static input_view"></p>
                                            <?php }  ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-lg-6"><label>No. Compromiso</label></div>
                                        <div class="col-lg-6">
                                            <?php if(isset($numero_compromiso)) { ?>
                                                <p class="form-control-static input_view"><?= $numero_compromiso ?></p>
                                            <?php } else { ?>
                                                <p class="form-control-static input_view"></p>
                                            <?php }  ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Destino</label>
                                    <?php if(isset($destino)) { ?>
                                        <p class="form-control-static input_view" id="destino"><?= $destino ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view" id="destino"></p>
                                    <?php }  ?>
                                </div>
                                <div class="form-group">
                                    <label>Concepto Específico</label>
                                    <?php if(isset($concepto_especifico)) { ?>
                                        <textarea style="width: 100%; height: 6.8em" class="form-control-static input_view" id="concepto_especifico"><?= $concepto_especifico ?></textarea>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view" id="concepto_especifico"></p>
                                    <?php }  ?>
                                </div>
                                <div class="form-group">
                                    <label>Concepto</label>
                                    <?php if(isset($concepto)) { ?>
                                        <p class="form-control-static input_view"><?= $concepto ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>

                            </div>
                            <!--Fin Primera Columna-->
                            <!--Segunda Columna-->
                            <div class="col-lg-4">
                                <label>Nombre de Proveedor o Beneficiario</label>
                                <div class="form-group forma_normal">
                                    <?php if(isset($proveedor)) { ?>
                                        <p class="form-control-static input_view"><?= $proveedor ?></p>
                                    <?php } elseif(isset($nombre_completo)) { ?>
                                        <p class="form-control-static input_view"><?= $nombre_completo ?></p>
                                    <?php }  ?>
                                </div>

                                <div class="form-group">
                                    <label>No. Comprobante Fiscal</label>
                                    <?php if(isset($documento)) { ?>
                                        <textarea style="width: 100%; height: 6.8em" class="form-control-static input_view"><?= $documento ?></textarea>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>

                                <div class="form-group">
                                    <label>Comprobante Fiscal</label>
                                    <?php if(isset($tipo_documento)) { ?>
                                        <p class="form-control-static input_view"><?= $tipo_documento ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>
                                <div class="form-group">
                                    <label>Descripción</label>
                                    <?php if(isset($descripcion)) { ?>
                                        <p class="form-control-static input_view"><?= $descripcion ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>

                            </div>
                            <!--Fin Segunda Columna-->
                            <!--Tercer Columna-->
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Fecha Emitido</label>
                                    <?php if(isset($fecha_emision)) { ?>
                                        <p class="form-control-static input_view"><?= $fecha_emision ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>

                                <div class="form-group">
                                    <label>Fecha de Pago</label>
                                    <?php if(isset($fecha_pago)) { ?>
                                        <p class="form-control-static input_view"><?= $fecha_pago ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>

                                    <label>Anexos</label>
                                    <?php if(isset($documentacion_anexa)) { ?>
                                        <p class="form-control-static input_view"><?= $documentacion_anexa ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>

                                    <div class="form-group c-firme" style="margin-top: 8%;">
                                        <div class="form-group">
                                            <label>¿Contra Recibo en Firme?</label>
                                            <?php if(isset($enfirme) && $enfirme == 1) { ?>
                                                <p class="form-control-static"><i class="fa fa-check-circle i-firmesi"></i></p>
                                            <?php } else { ?>
                                                    <p class="form-control-static"><i class="fa fa-times-circle i-firmeno"></i></p>
                                            <?php  } ?>
                                        </div>
                                    </div>
                                </div>


                                </div>
                            <!--Fin Tercer Columna-->

                    </div>

                </div>
            </div>

                <div class="row add-pre" id="desglose_honorarios">

                </div>

<div class="row add-pre">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Detalles
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-body table-gral">
                                <div class="table-responsive">
                                    <h4 id="suma_total" class="text-center"></h4>
                                    <input type="hidden" value="" name="total_hidden" id="total_hidden" />
                                    <table class="table table-striped table-bordered table-hover" id="tabla_datos_contrarecibo">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th width="15%">No. Compromiso</th>
                                            <th width="18%">Tipo de Compromiso</th>
                                            <th width="13%">Subtotal</th>
                                            <th width="13%">IVA</th>
                                            <th width="13%">Total</th>
                                            <th>Descripción</th>
                                            <th>Acciones</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

    <div class="btns-finales text-center">
        <a class="btn btn-default" href="<?= base_url("ciclo/contrarecibos") ?>"><i class="fa fa-reply ic-color"></i>  Regresar</a>
    </div>
</form>


</div>

</div>
<!-- /.row -->

</div>
<!-- /#page-wrapper -->