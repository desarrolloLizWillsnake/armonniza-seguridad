<h3 class="page-header center"><i class="fa fa-archive"></i> Terminar Precompromiso</h3>
<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row add-pre">

                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Precompromiso
                                <input type="hidden" name="precompromiso" id="precompromiso" value="<?= $numero_pre ?>" />
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-3">
                                        <label style="font-weight: normal;">No. Precompromiso</label>
                                        <?php if(isset($numero_pre)) { ?>
                                            <p class="form-control-static input_view"><?= $numero_pre ?></p>
                                        <?php } else { ?>
                                            <p class="form-control-static input_view"></p>
                                        <?php }  ?>
                                        <label style="font-weight: normal;">Fecha de Autorizacion</label>
                                        <?php if(isset($fecha_autoriza)) { ?>
                                            <p class="form-control-static input_view"><?= $fecha_autoriza ?></p>
                                        <?php } else { ?>
                                            <p class="form-control-static input_view"></p>
                                        <?php }  ?>
                                        <label style="font-weight: normal;">Importe Total</label>
                                        <?php if(isset($total)) { ?>
                                            <p class="form-control-static input_view">$ <?= $this->cart->format_number($total)?></p>
                                        <?php } else { ?>
                                            <p class="form-control-static input_view"></p>
                                        <?php }  ?>
                                    </div>
                                    <div class="col-lg-9">
                                        <table class="table table-striped table-bordered table-hover" id="tabla_precompromiso">
                                            <thead>
                                            <tr>
                                                <th>F.F.</th>
                                                <th>P.F.</th>
                                                <th>C.C.</th>
                                                <th>Capitulo</th>
                                                <th>Concepto</th>
                                                <th>Partida</th>
                                                <th>Importe</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-lg-3"><h4>Compromisos Asociados</h4></div>
                                    <div class="col-lg-9">
                                    <table class="table table-striped table-bordered table-hover" id="tabla_compromiso">
                                        <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>F.F.</th>
                                            <th>P.F.</th>
                                            <th>C.C.</th>
                                            <th>Capitulo</th>
                                            <th>Concepto</th>
                                            <th>Partida</th>
                                            <th width="13%">Fecha</th>
                                            <th>Importe</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>

                <div class="row add-pre">
                    <div class="col-lg-12">
                        <div class="btns-finales text-center">
                            <div class="text-center" id="resultado_terminar"></div>
                            <a href="<?= base_url("ciclo/precompromiso") ?>" id="borrar_cambios" class="btn btn-default"><i class="fa fa-reply ic-color"></i> Regresar</a>
                            <input data-toggle="modal" data-target="#modal_terminar" type="submit" id="terminar_precompromiso" name="terminar_precompromiso" class="btn btn-green" style="border:none;" value="Terminar Precompromiso" />
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <!-- Modal Terminar -->
        <div class="modal fade" id="modal_terminar" tabindex="-1" role="dialog" aria-labelledby="modal_terminar" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-archive ic-modal"></i> Terminar Precompromiso</h4>
                    </div>
                    <div class="modal-body text-center">
                        <label for="message-text" class="control-label">¿Realmente desea terminar el precompromiso No. <?= $numero_pre ?>?</label>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_terminar">Terminar</button>
                    </div>
                </div>
            </div>
        </div>