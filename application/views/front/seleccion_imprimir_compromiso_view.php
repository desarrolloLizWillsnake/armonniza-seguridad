<?php
$this->db->select('*')->from('cat_personas_firma');
$query = $this->db->get();
$resultados = $query->result();

$this->db->select('*')->from('datos_empresa')->where('id_datos_empresa', 1);
$query = $this->db->get();
$datos_empresa = $query->row();
?>
<h3 class="page-header title center" xmlns="http://www.w3.org/1999/html"><i class="fa fa-print"></i> Impresión Compromiso</h3>
<div id="page-wrapper">
    <div class="row center">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <div class="list-group error-completar">
                <?php if(isset($mensaje)) { ?>
                <div class="alert alert-danger">
                    <?= $mensaje ?>
                </div>
                <div class="text-center">
                    <div class="btns-finales">
                        <a class="btn btn-default" href="<?= base_url("ciclo/compromiso") ?>"><i class="fa fa-reply ic-color"></i> Regresar</a>
                    </div>
                </div>
            </div>
            <?php } else { ?>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default datos-requeridos">
                            <div class="panel-body">
                                <form class="" action="<?= base_url("ciclo/imprimir_compromiso_formato") ?>" method="POST" role="form">
                                    <input type="hidden" name="compromiso" value="<?= $compromiso ?>" id="compromiso" />
                                    <?php if($tipo_impresion == "Orden de Compra" || $tipo_impresion == "Orden de Servicio" || $tipo_impresion == "Orden de Trabajo") { ?>
                                        <!--Tipo Formato Impresión-->
                                    <div id="orden">
                                        <!--Elaboró-->
                                        <div class="form-group">
                                            <label>Persona que elabora</label>
                                            <input class="form-control" type="text" id="persona" name="persona" placeholder="Persona que elaboró" required/>
                                        </div>
                                        <div class="form-group">
                                            <input class="form-control" type="text" id="persona_puesto" name="persona_puesto" placeholder="Puesto" required/>
                                        </div>
                                        <div class="form-group">
                                            <label>Vo. Bo.</label>
                                            <select class="form-control" name="vobo">
                                                <?php
                                                foreach($resultados as $row) { ?>
                                                    <option value="<?= $row->id_personas_firma ?>"><?= $row->grado_estudio ?> <?= $row->nombre ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    <!--Nota-->
                                    <div class="form-group">
                                        <textarea class="form-control textarea-xl-big" name="texto">Facturar a: <?= $datos_empresa->nombre?> Registro Federal de Contribuyentes:  <?= $datos_empresa->rfc?> Dirección:  <?= $datos_empresa->calle?>  <?= $datos_empresa->no_ext?> <?= $datos_empresa->no_int?>, Col. <?= $datos_empresa->colonia?>, Del. <?= $datos_empresa->delegacion?>, <?= $datos_empresa->ciudad?> <?= $datos_empresa->cp?>
Autorización del Presupuesto: Mediante Requisición RQ-2014100029, solicitada por el Jefe del Departamento de Almacén, a la cual se le asignó la suficiencia Presupuestal el día 27 de enero del 2014.
Procedimiento de Adjudicación: Adjudicación Directa de conformidad con lo dispuesto en el Artículo 42 de la Ley de Adquisiciones, Arrendamientos y Servicios del Sector Público.
Plazo, Lugar y Condiciones de Entrega: El bien objeto de dicha orden, será entregado a más tardar el día jueves 06 de febrero del año en curso, en un horario de 09:00 a 13:00 horas, en oficinas centrales de EDUCAL, en Av. Ceylán No. 450, Col Euzkadi, Delegación Azcapotzalco, C.P. 02660.
Condiciones de Pago: De conformidad con el Artículo 51 de la citada Ley, el pago se efectuará contra entrega del material en la Tesorería de EDUCAL, S.A. DE C.V., ubicada en el planta baja del citado domicilio, a la presentación de la factura respectiva con el Vo. Bo. de la Coordinación de Adquisiciones.
Penalizaciones: De conformidad con el Articulo 53 de la Ley de Adquisiciones, Arrendamientos y Servicios del Sector Público se aplicará una pena convencional en caso de incumplimiento de las fechas establecidas para la adquisición por el 4% diario respecto del valor del bien entregado oportunamente, sin exceder de un 20%, salvo que por causas excepcionales y justificadas y a solicitud por escrito DEL PROVEEDOR dentro del plazo establecido para la dicho servicio, EDUCAL, S.A. DE C.V. haya otorgado un plazo mayor, debidamente especificadas las condiciones del mismo, a partir del 6° día, EDUCAL, S.A. DE C.V. determinará se inicia el procedimiento de rescisión correspondiente.
Garantía: De conformidad con el penúltimo párrafo del Artículo 48 de Ley de Adquisiciones, Arrendamientos y Servicios del Sector Público, se exenta AL PROVEEDOR de presentar la Garantía de cumplimiento.
Precio Fijo: La presente orden se celebra bajo la modalidad de precio fijo y total de conformidad con la cotización DEL PROVEEDOR, por lo que el monto se mantendrá fijo, sin realizar algún ajuste.
Otros: La factura se entregará en el Departamento de Recursos Materiales, ubicada en la planta baja del domicilio antes referido.
                                        </textarea>
                                    </div>
                                        <!--Botón-->
                                        <div class="btns-finales text-center">
                                            <button type="submit" class="btn btn-green">Continuar</button>
                                        </div>
                                    </div>
                                    <?php } elseif($tipo_impresion == "Contrato Pedido") { ?>
                                        <div id="contrato">
                                            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="false">
                                                <!-- Indicators -->
                                               <!-- <ol class="carousel-indicators">
                                                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                                </ol>
                                                -->
                                                <!-- Wrapper for slides -->
                                                <div class="carousel-inner">
                                                    <div class="item active">
                                                        <div class="form-group">
                                                            <input class="form-control" type="text" id="objeto" name="objeto" placeholder="Objeto del Contrato-Pedido"/>
                                                        </div>
                                                        <div class="form-group">
                                                            <input class="form-control" type="text" id="transporte" name="transporte" placeholder="Transporte"/>
                                                        </div>
                                                        <textarea class="form-control" id="tipo_adjuticacion" name="tipo_adjuticacion" style="height: 8px;" placeholder="Tipo de Adjudicación"></textarea>

                                                        <textarea class="form-control textarea-big" id="clausulas" name="clausulas" placeholder="Cláusulas Generales"></textarea>

                                                        <div class="btns-finales text-right">
                                                            <!-- Controls -->
                                                            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                                                <butom class="btn btn-green">Siguiente <i class="fa fa-angle-double-right"></i></butom>
                                                            </a>
                                                        </div>
                                                    </div>

                                                    <div class="item">
                                                        <label class="label_sig">Datos Proveedor</label>
                                                        <div class="form-group">
                                                            <input class="form-control" type="text" id="representante" name="representante" placeholder="Nombre del representante legal"/>
                                                        </div>
                                                        <!--Nota-->
                                                        <div class="form-group">
                                                            <textarea class="form-control" name="identificacion">Documento de identificación del "Proveedor": Credencial de Elector Folio:</textarea>

                                                            <textarea class="form-control textarea-big" name="texto">
Sociedad debidamente constituida ante la fe del Lic. Raúl Name Neme Notario Público No. 79 del Estado de México, con residencia en la Paz, mediante Escritura No. 75, 277 de fecha 17/Enero/2005 y teniendo Folio de inscripción en el Registro Público de Comercio No. 327, 458. Registrada ante hacienda con el R.F.C. LCO0501174JA.
Representada por el C. Mario Juan Contreras Garduño, poder otorgado mediante instrumento notarial número 75,279 pasado ante la fe del notario antes referido de fecha 17/Enero/2005.
                                                            </textarea>
                                                        </div>
                                                        <div class="btns-finales">
                                                            <!-- Controls -->
                                                            <div class="row">
                                                                <div class="col-lg-6" style="text-align:left;">
                                                                    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                                                        <butom class="btn btn-green"><i class="fa fa-angle-double-left"></i> Anterior</butom>
                                                                    </a>
                                                                </div>
                                                                <div class="col-lg-6" style="text-align:right;">
                                                                    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                                                        <butom class="btn btn-green">Siguiente <i class="fa fa-angle-double-right"></i></butom>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="item">
                                                        <div class="form-group">
                                                            <label class="label_sig">Elaboró</label>
                                                            <div class="form-group">
                                                                <input class="form-control" type="text" id="persona_elaboro" name="persona_elaboro" placeholder="Nombre de la persona que elaboró"/>
                                                            </div>
                                                            <div class="form-group">
                                                                <input class="form-control" type="text" id="puesto_elaboro" name="puesto_elaboro" placeholder="Puesto de la persona que elaboró"/>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="label_sig">Vo. Bo.</label>
                                                            <select class="form-control" name="vobo">
                                                                <?php
                                                                foreach($resultados as $row) { ?>
                                                                    <option value="<?= $row->id_personas_firma ?>"><?= $row->grado_estudio ?> <?= $row->nombre ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                        <div class="btns-finales text-center">
                                                            <button type="submit" class="btn btn-green">Continuar</button>
                                                        </div>
                                                        <div class="btns-finales text-left">
                                                            <!-- Controls -->
                                                            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                                                <butom class="btn btn-green"><i class="fa fa-angle-double-left"></i> Anterior</butom>
                                                            </a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div> <!-- Carousel -->

                                        </div>
                                    <?php } else { ?>
                                        <div id="otros">
                                            <div class="form-group">
                                                <label>Vo.Bo.</label>
                                                <select class="form-control" name="persona">
                                                    <?php
                                                    foreach($resultados as $row) { ?>
                                                        <option value="<?= $row->id_personas_firma ?>"><?= $row->grado_estudio ?> <?= $row->nombre ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <!--<label class="imp-total">Total Compromiso</label>
                                            <p class="imp-total-cant">$<?= $this->cart->format_number($total) ?></p>
                                            <div class="form-group">
                                                <input class="form-control" type="text" id="importe_letra" name="importe_letra" placeholder="Importe con Letra"/>
                                            </div>-->
                                            <!--Botón-->
                                            <div class="btns-finales text-center">
                                                <button type="submit" class="btn btn-green">Continuar</button>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </form>
                            </div>
                        </div>
                    </div>
               </div>
                <?php } ?>
            </div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>



</div>