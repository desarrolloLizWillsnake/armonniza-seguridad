<h3 class="page-header center"><i class="fa fa-plus-circle"></i> Agregar Ampliación</h3>
<div id="page-wrapper">
    <form class="forma_ampliacion" role="form">
        <div class="row add-pre error-gral">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        General
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="row">
                            <input type="hidden" name="ultimo" id="ultimo" value="<?= $ultimo ?>">
                            <!--Primera Columna-->
                            <div class="col-lg-4">
                                <!--Clasificación-->
                                <select class="form-control" id="clasificacion" name="clasificacion" required>
                                    <option value="">Clasificación</option>
                                    <option value="Internas">Internas</option>
                                    <option value="Externas">Externas</option>
                                    <option value="Impuestos">Impuestos</option>
                                    <option value="Derechos">Derechos</option>
                                    <option value="Productos y/o Bienes">Productos y/o Bienes</option>
                                    <option value="Servicios">Servicios</option>
                                    <option value="Aprovechamientos">Aprovechamientos</option>
                                    <option value="Participaciones">Participaciones</option>
                                    <option value="Aportaciones">Aportaciones</option>
                                    <option value="Financiamientos">Financiamientos</option>
                                    <option value="Presupuesto">Presupuesto</option>
                                    <option value="Transferencias y/o Subsidios">Transferencias y/o Subsidios</option>
                                    <option value="Otros">Otros</option>
                                </select>

                                <!--Justificación-->
                                <textarea style="height: 9em;" class="form-control" id="descripcion" name="descripcion" placeholder="Justificación" required></textarea>


                            </div>
                            <!--Fin Primer Columna-->
                            <!--Segunda Columna -->
                            <div class="col-lg-5" style="padding-left: 3%;">

                                <!-- Tipo de Gastos -->
                                <label>Tipo de Gasto</label>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="tipo_gasto" id="optionsRadios1" value="1" checked>Gasto Corriente
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="tipo_gasto" id="optionsRadios2" value="2">Gasto de Capital
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="tipo_gasto" id="optionsRadios3" value="3">Amortización de la deuda y disminución de pasivos
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="tipo_gasto" id="optionsRadios4" value="4">Pensiones y Jubilaciones
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="tipo_gasto" id="optionsRadios5" value="5">Participaciones
                                    </label>
                                </div>
                            </div>
                            <!--Fin Segunda Columna-->
                            <!--Tercer Columna-->
                            <div class="col-lg-3 error-detalle">
                                <!--Fecha Solicitud-->
                                <input type="text" class="form-control ic-calendar" name="fecha_solicitud" id="fecha_solicitud" placeholder="Fecha de Solicitud" disabled />
                                <!--Fecha-->
                                <input type="text" class="form-control ic-calendar" name="fecha_aplicar_caratula" id="fecha_aplicar_caratula" placeholder="Fecha Aplicada" required>

                            </div>
                            <!--Fin Tercera Columna-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row add-pre error-detalle">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Información de Partida
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <!--Primera Parte Partida Inicio-->
                        <div class="row">
                            <div class="col-lg-2 niveles-pc">
                                <?= $nivel_superior ?>
                                <a href="#modal_estructura_superior" class="btn btn-default" data-toggle="modal" data-target="#modal_estructura_superior">¿No conoces la
                                    <br/>estructura?</a>
                            </div>
                            <div class="col-lg-10">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <label>Enero</label>
                                        <input type="text" class="form-control disabled-color" name="enero_superior" id="enero_superior" disabled/>
                                    </div>
                                    <div class="col-lg-4">
                                        <label>Febrero</label>
                                        <input type="text" class="form-control disabled-color"  name="febrero_superior" id="febrero_superior" disabled/>
                                    </div>
                                    <div class="col-lg-4">
                                        <label>Marzo</label>
                                        <input type="text" class="form-control disabled-color" name="marzo_superior" id="marzo_superior" disabled/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <label>Abril</label>
                                        <input type="text" class="form-control disabled-color" name="abril_superior" id="abril_superior" disabled/>
                                    </div>
                                    <div class="col-lg-4">
                                        <label>Mayo</label>
                                        <input type="text" class="form-control disabled-color" name="mayo_superior" id="mayo_superior" disabled/>
                                    </div>
                                    <div class="col-lg-4">
                                        <label>Junio</label>
                                        <input type="text" class="form-control disabled-color" name="junio_superior" id="junio_superior" disabled/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <label>Julio</label>
                                        <input type="text" class="form-control disabled-color" name="julio_superior" id="julio_superior" disabled/>
                                    </div>
                                    <div class="col-lg-4">
                                        <label>Agosto</label>
                                        <input type="text" class="form-control disabled-color" name="agosto_superior" id="agosto_superior" disabled/>
                                    </div>
                                    <div class="col-lg-4">
                                        <label>Septiembre</label>
                                        <input type="text" class="form-control disabled-color" name="septiembre_superior" id="septiembre_superior" disabled/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <label>Octubre</label>
                                        <input type="text" class="form-control disabled-color" name="octubre_superior" id="octubre_superior" disabled/>
                                    </div>
                                    <div class="col-lg-4">
                                        <label>Noviembre</label>
                                        <input type="text" class="form-control disabled-color" name="noviembre_superior" id="noviembre_superior" disabled/>
                                    </div>
                                    <div class="col-lg-4">
                                        <label>Diciembre</label>
                                        <input type="text" class="form-control disabled-color" name="diciembre_superior" id="diciembre_superior" disabled/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-2"></div>
                            <div class="col-lg-10 text-center">
                                <h4 id="total_superior"></h4>
                            </div>
                        </div>
                        <!--Fin Primera Parte Partida Inicio-->
                    </div>
                </div>
            </div>
        </div>
        <div class="row add-pre error-detalle">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Ampliación de Partida
                    </div>

                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <!--Segunda Parte Partida Inicio-->
                        <div class="row">
                            <div class="col-lg-4 niveles-pc">
                                <!--Estructura Administrativa de ingresos-->
                                <?= $nivel_inferior ?>
                                <a href="#modal_estructura_inferior" class="btn btn-default" data-toggle="modal" data-target="#modal_estructura_inferior" style="margin-top: 3%;">¿No conoces la
                                    <br/>estructura?</a>
                            </div>
                            <div class="col-lg-8">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="row">
                                            <div class="col-lg-8">
                                                <input type="hidden" name="total_hidden" id="total_hidden" />
                                                <select class="form-control" name="mes_destino" required>
                                                    <option value="">Mes en que se realizará la ampliación</option>
                                                    <option value="enero">Enero</option>
                                                    <option value="febrero">Febrero</option>
                                                    <option value="marzo">Marzo</option>
                                                    <option value="abril">Abril</option>
                                                    <option value="mayo">Mayo</option>
                                                    <option value="junio">Junio</option>
                                                    <option value="julio">Julio</option>
                                                    <option value="agosto">Agosto</option>
                                                    <option value="septiembre">Septiembre</option>
                                                    <option value="octubre">Octubre</option>
                                                    <option value="noviembre">Noviembre</option>
                                                    <option value="diciembre">Diciembre</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-4">
                                                <input type="text" class="form-control ic-calendar" name="fecha_aplicar_detalle" id="fecha_aplicar_detalle" placeholder="Fecha en que se aplicará el movimiento" />
                                            </div>
                                        </div>
                                        <div class="form-group input-group">
                                            <span class="input-group-btn ic-peso-btn">
                                                <button class="btn btn-default sg-dollar"><i class="fa fa-dollar"></i></button>
                                            </span>
                                            <input style="margin-top: 1.4%;" type="text" pattern="[0-9]+([\.|,][0-9]+)?" class="form-control dinero" name="cantidad" placeholder="Importe" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- Fin Segunda Parte Partida Inicio-->
                        <!-- Tercer Parte Partida Inicio-->
                        <div class="row">
                            <div class="col-lg-12">
                                <button type="button" class="btn btn-green btn-block btn-gd" id="guardar_ampliacion_detalle">Guardar Adecuación</button>
                                <div id="resutado_insertar_detalle"></div>
                            </div>
                        </div>
                        <!-- Fin Tercer Parte Partida Inicio-->
                        <!--Tabla Detalle-->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-body table-gral">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover" id="tabla_datos_ampliacion">
                                                <thead>
                                                <tr>
                                                    <th>Id</th>
                                                    <th width="13%">Gerencia</th>
                                                    <th width="13%">C. Recaudación</th>
                                                    <th width="7%">Rubro</th>
                                                    <th width="6%">Tipo</th>
                                                    <th width="8%">Clase</th>
                                                    <th>Importe</th>
                                                    <th width="13%">Fecha Aplicada</th>
                                                    <th>Descripción</th>
                                                    <th width="11%">Movimiento</th>
                                                    <th width="11%">Acciones</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Fin Tabla Detalle-->
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-body text-center">
                <div class="form-group c-firme">
                    <h3>¿Adecuación en Firme?</h3>
                    <label class="checkbox-inline">
                        <input type="checkbox" id="check_firme" name="check_firme" />Sí
                    </label>
                </div>
            </div>
        </div>

        <div class="btns-finales text-center">
            <a class="btn btn-default" href="<?= base_url("ingresos/adecuaciones_presupuestarias") ?>" id="borrar_cambios"><i class="fa fa-reply ic-color"></i> Regresar</a>
            <a id="guardar_ampliacion" class="btn btn-green">Guardar Adecuación</a>
        </div>
    </form>
</div>


<!-- Modal estructura superior-->
<div class="modal fade" id="modal_estructura_superior" tabindex="-1" role="dialog" aria-labelledby="modal_estructura_superior" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-indent-left"></span> Estructura de Ingresos</h4>
            </div>
            <div class="modal-body text-center">
                <?= $niveles_modal_superior ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" disabled id="elegir_ultimoNivel_superior">Elegir</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal estructura inferior-->
<div class="modal fade" id="modal_estructura_inferior" tabindex="-1" role="dialog" aria-labelledby="modal_estructura_inferior" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-indent-left"></span> Estructura de Ingresos</h4>
            </div>
            <div class="modal-body text-center">
                <?= $niveles_modal_inferior ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" disabled id="elegir_ultimoNivel_inferior">Elegir</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Borrar Detalle -->
<div class="modal fade modal_borrar" tabindex="-1" role="dialog" aria-labelledby="modal_borrar" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-trash ic-modal"></i> Eliminar Ampliación</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="message-text" class="control-label">¿Realmente desea eliminar el movimiento seleccionado?</label>
                        <input type="hidden" value="" name="borrar_ampliacion_hidden" id="borrar_ampliacion_hidden" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_cancelar_ampliacion">Aceptar</button>
            </div>
        </div>
    </div>
</div>

</div>
<!-- /#page-wrapper -->