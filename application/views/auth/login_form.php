<?php
$login = array(
	'name'	=> 'login',
	'class'	=> 'form-control c-center',
	'id'	=> 'login',
	'value' => set_value('login'),
	'maxlength'	=> 80,
	'placeholder'	=> "Usuario",
);
if ($login_by_username AND $login_by_email) {
	$login_label = 'Email or login';
} else if ($login_by_username) {
	$login_label = 'Login';
} else {
	$login_label = 'Email';
}
$password = array(
	'name'	=> 'password',
    'class'	=> 'form-control c-center',
    'placeholder'	=> "Contraseña",
	'id'	=> 'password',
);
$remember = array(
	'name'	=> 'remember',
	'id'	=> 'remember',
	'value'	=> 1,
	'checked'	=> set_value('remember'),
	'style' => 'margin:0;padding:0',
);
$captcha = array(
	'name'	=> 'captcha',
	'id'	=> 'captcha',
	'maxlength'	=> 8,
);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Armonniza</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?= base_url("assets/templates/front/css/bootstrap.min.css") ?>" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?= base_url("assets/templates/front/css/plugins/metisMenu/metisMenu.min.css") ?>" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?= base_url("assets/templates/front/css/sb-admin-2.css") ?>" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?= base_url("assets/templates/front/font-awesome-4.1.0/css/font-awesome.min.css") ?>" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="login">
<div class="login-container">
    <div class="logo">
        <img class="logo1" src="<?= base_url('img/logo.png') ?>">
        <!--<img class="logo2" src="<?= base_url('img/logo_edo.png') ?>">-->
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 cont-gral-form">
                <div class="font-cont-form c-center"></div>
                <div class="cont-form c-center">
                    <h2 class="center c-center">Iniciar Sesión</h2>
                        <?php echo form_open($this->uri->uri_string(), array("role" => "form", "id" => "forma_login")); ?>
                            <fieldset class="center c-center">
                                <div class="form-group">
                                    <!--<?php echo form_label($login_label, $login['id']); ?>-->
                                    <?php echo form_input($login); ?>
                                </div>
                                <div class="form-group">
                                    <!--<?php echo form_label('Password', $password['id']); ?>-->
                                    <?php echo form_password($password); ?>
                                </div>
                                <div class="checkbox">
                                    <?php echo anchor('/auth/forgot_password/', '¿Olvido su contraseña?'); ?>
                                    <!--<?php if ($this->config->item('allow_registration', 'tank_auth')) echo anchor('/auth/register/', 'Register'); ?>-->
                                </div>
                            </fieldset>
                            <!-- Change this to a button or input when using this as a form -->
                            <!--<a href="index.html" class="btn btn-lg btn-success btn-block">Login</a>-->
                            <div class="row c-center">
                                <div class="col-sm-8 col-md-8 text-center" id="mensajes">
                                    <?php echo form_error($login['name']); ?><?php echo isset($errors[$login['name']])?$errors[$login['name']]:''; ?>
                                    <?php echo form_error($password['name']); ?><?php echo isset($errors[$password['name']])?$errors[$password['name']]:''; ?>
                                </div>
                                <div class="col-sm-4 col-md-4">
                                    <?php echo form_submit( array("name" => "submit", "class" => "btn btn-lg btn-default" ), 'Entrar'); ?>
                                </div>
                            </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-12 logo_arm text-right">
                <img src="<?= base_url('img/armonniza.png') ?>">
            </div>
        </div>
</div>
    <!--<hr style="margin-top: 1%; border: 2px solid #B6CE33; "/>
    <hr style="border-color: #B6CE33;margin-top: -1%; "/>-->
</div>
<!-- jQuery -->
<script src="<?= base_url("assets/templates/front/js/jquery.js") ?>"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?= base_url("assets/templates/front/js/bootstrap.min.js") ?>"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="<?= base_url("assets/templates/front/js/plugins/metisMenu/metisMenu.min.js") ?>"></script>

<!-- Custom Theme JavaScript -->
<script src="<?= base_url("assets/templates/front/js/sb-admin-2.js") ?>"></script>

<script>
    $("#forma_login").on("submit", function() {
        $("#mensajes").html('<img src="<?= base_url("img/ajax-loader.gif") ?>" style="width: 50px;">');
    });
</script>

</body>

</html>