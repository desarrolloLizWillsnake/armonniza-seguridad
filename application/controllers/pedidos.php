<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pedidos extends CI_Controller {
	
    function __construct() {
        parent::__construct();
        // $this->methods['api_pedidos_post']['limit'] = 5000; //100 requests per hour per user/key
    }

    public function prueba_pedido() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha ingresado al modulo de pedidos');
        $respuesta = array();

        if(!$this->input->post("pedido")) {
            $respuesta["status"] = 400;
            $respuesta["mensaje"] = "No hay valor ingresado";
            echo(json_encode($respuesta));
            return FALSE;
        } else {
            $pedido = $this->input->post('pedido');

            //        Se llama la funcion del modelo de egresos encargado de contar los niveles que existen
            $total_egresos = $this->egresos_model->contar_egresos_elementos();
            //        Se llama a la funcion del model de egresos encargada de tomar los nombres delos niveles que existen
            $nombres_egresos = $this->egresos_model->obtener_nombre_niveles();
            //        En este arreglo se van a guardar los nombres de los niveles
            $nombre = array();

            //        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
            foreach ($nombres_egresos as $fila) {
                array_push($nombre, $fila->descripcion);
            }

            //        Se carga los datos de conexión de la base de datos de pedidos y se guarda en la variable $DB1
            $DB1 = $this->load->database('pedidos', TRUE);

            //        Se toma el numero del ultimo precompromiso y se aumenta en uno
            $last = 0;
            $ultimo = $this->ciclo_model->ultimo_precompromiso();

            if ($ultimo) {
                $last = $ultimo->ultimo + 1;
            } else {
                $last = 1;
            }

            $sql_caratula = "SELECT npedido, cvelib, pedido01.cveedi, nomedi, tipo, pedido01.status, entrega
                                FROM pedido01
                                LEFT JOIN editor
                                ON (pedido01.cveedi = editor.cveedi)
                                WHERE fecha >= '2015-01-01'
                                AND tipo = 1
                                AND pedido01.status = 'a'
                                AND pedido01.npedido = ?";
            $query_caratula = $DB1->query($sql_caratula, array($pedido));
            $result_caratula = $query_caratula->row();

            //        Se toma el detalle del pedido
            $sql = "SELECT p.codigo, titulo.titulo, p.codigo ||' '|| titulo.titulo AS detalle, c_sol, p.precio, p.iva, p.educal, (c_sol * p.precio) AS importe, titulo.cveprod AS clave_producto
                    FROM pedido02 p
                    LEFT JOIN titulo
                    ON (p.codigo = titulo.codigo)
                    WHERE npedido = ?;";

            $query = $DB1->query($sql, array($pedido));

            //         Devuelve un arreglo con los datos
            $result_detalle = $query->result();

            $total = 0;

            foreach ($result_detalle as $row) {
                if ($row->iva == "S") {
                    $total = round($row->importe * 1.16, 2);
                } //            De lo contrario, se crea pasa el valor del subtotal al importe
                else {
                    $total = round($row->importe, 2);
                }
            }

//            Revisar si hay dinero

            $centro_costo = $this->ciclo_model->get_centro_costo_API($result_caratula->cvelib);

            $sql_existe_estructura = "SELECT id_niveles
                            FROM cat_niveles
                            WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) = 4
                            AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) = 'E016'
                            AND COLUMN_GET(nivel, 'centro_de_costos' as char) = ?
                            AND COLUMN_GET(nivel, 'capitulo' as char) = 2000
                            AND COLUMN_GET(nivel, 'concepto' as char) = 238
                            AND COLUMN_GET(nivel, 'partida' as char) = 23801";
            $query_estructura = $this->query($sql, array($centro_costo));
            $existe = $query_estructura->row();

            if ($existe->id_niveles) {

                $mes_actual = $this->utilerias->convertirFechaAMes(date("Y-m-d"));

                //            Se tiene que comprobar si hay dinero para hacer el precompromiso
                $query_revisar_dinero = "SELECT COLUMN_GET(nivel, 'total_anual' as char) AS total_anual,
                                        COLUMN_GET(nivel, '" . $mes_actual . "_precompromiso' as char) AS mes_precompromiso,
                                        COLUMN_GET(nivel, 'precomprometido_anual' as char) AS precomprometido_anual
                                        FROM cat_niveles WHERE id_niveles = ?";

                $resultado_dinero = $this->ciclo_model->revisarDinero($existe->id_niveles, $query_revisar_dinero);


                //            Se calcula el valor de la cantidad precomprometida anualmente mas el importe
                $check = $resultado_dinero->precomprometido_anual + $total;

                //            Se pregunta si el total anual es menor a 0, y si el valor de la cantidad precomprometida mas la precomprometida anual es menor o igual al total anual del presupuesto
                if ($resultado_dinero->total_anual > 0 && $check <= $resultado_dinero->total_anual) {
                    $respuesta["status"] = 200;
                    $respuesta["mensaje"] = 'La información se insertó correctamente.';
                } else {
                    $respuesta["status"] = 300;
                    $respuesta["mensaje"] = 'No hay suficiente dinero en la partida durante el mes de ' . ucfirst($mes_actual) . '.';
                }

            } else {
                $respuesta["status"] = 400;
                $respuesta["mensaje"] = 'La estructura introducida no existe.';
            }


            foreach ($result_detalle as $fila) {
                $gasto = "";
                $titulo_gasto = "";

                $subtotal = 0;
                $iva = 0;
                $importe = 0;

                if($fila->iva == "S") {
                    $subtotal = round($fila->importe, 2);
                    $iva = round($fila->importe * 0.16, 2);
                    $importe = round($fila->importe * 1.16, 2);
                }
//            De lo contrario, se crea pasa el valor del subtotal al importe
                else {
                    $subtotal = round($fila->importe, 2);
                    $iva = 0;
                    $importe = round($fila->importe, 2);
                }

                switch($fila->clave_producto) {
                    case 0:
                        $gasto = "23801000-0201";
                        $titulo_gasto = "Material Bibliográfico";
                        break;
                    case 1:
                        $gasto = "23801000-0301";
                        $titulo_gasto = "Tarjetas para su comercialización";
                        break;
                    case 2:
                        $gasto = "23801000-0302";
                        $titulo_gasto = "Audio y video para su comercialización";
                        break;
                    case 3:
                        $gasto = "23801000-0303";
                        $titulo_gasto = "Reproducciones para su comercialización";
                        break;
                    case 4:
                        $gasto = "23801000-0304";
                        $titulo_gasto = "Producto infantil para su comercialización";
                        break;
                    case 5:
                        $gasto = "23801000-0101";
                        $titulo_gasto = "Materiales Culturales para su comercialización";
                        break;
                    case 6:
                        $gasto = "23801000-0305";
                        $titulo_gasto = "Promocionales y/o productos desarrollados para su comercialización";
                        break;
                    case 7:
                        $gasto = "23801000-0306";
                        $titulo_gasto = "Papeleria";
                        break;
                    case 8:
                        $gasto = "23801000-0307";
                        $titulo_gasto = "Publicaciones periodicas para su comercialización";
                        break;
                    case 9:
                        $gasto = "23801000-0308";
                        $titulo_gasto = "Artesanias para su comercialización";
                        break;
                    case 10:
                        $gasto = "23801000-0309";
                        $titulo_gasto = "Joyeria para su comercialización";
                        break;
                    case 11:
                        $gasto = "23801000-0310";
                        $titulo_gasto = "Ropa para su comercialización";
                        break;
                    case 12:
                        $gasto = "23801000-0311";
                        $titulo_gasto = "Articulos decorativos para su comercialización";
                        break;
                    case 13:
                        $gasto = "23801000-0312";
                        $titulo_gasto = "Alimentos para su comercialización";
                        break;
                    case 14:
                        $gasto = "23801000-0313";
                        $titulo_gasto = "Excentos de comisiones";
                        break;
                }

                $sql_insertar = "INSERT INTO mov_precompromiso_detalle
                                    (numero_pre, id_nivel, gasto, unidad_medida, cantidad, p_unitario, subtotal, iva, importe, titulo, mov_precompromiso_detalle.year, especificaciones, nivel )
                                    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, COLUMN_CREATE('fuente_de_financiamiento', '4', 'programa_de_financiamiento', 'E016', 'centro_de_costos', ?, 'capitulo', '2000', 'concepto', '238', 'partida', '23801'));";

                $query_insertar = $this->query($sql_insertar, array(
                    $last,
                    $existe->id_niveles,
                    $gasto,
                    "PZA",
                    $fila->c_sol,
                    $fila->precio,
                    $subtotal,
                    $iva,
                    $importe,
                    $titulo_gasto,
                    date("Y"),
                    $fila->detalle,
                    $centro_costo));

                $this->query($query_insertar);

            }
            echo(json_encode($respuesta));
            return FALSE;
        }

    }
    
    function hacer_pedido() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha hecho un pedido');
        $respuesta = array(
            'status' => "",
            'mensaje' => "",
            );

        if(!$this->input->post("nopedido")) {
            $respuesta["status"] = 400;
            $respuesta["mensaje"] = "No hay valor ingresado";
            echo(json_decode($respuesta));
            return FALSE;
        }

        $pedido = $this->input->post('nopedido');



        
    	
    }/*

    function user_get() {
        if(!$this->get('id'))
        {
            $this->response(NULL, 400);
        }

        // $user = $this->some_model->getSomething( $this->get('id') );
        $users = array(
            1 => array('id' => 1, 'name' => 'Some Guy', 'email' => 'example1@example.com', 'fact' => 'Loves swimming'),
            2 => array('id' => 2, 'name' => 'Person Face', 'email' => 'example2@example.com', 'fact' => 'Has a huge face'),
            3 => array('id' => 3, 'name' => 'Scotty', 'email' => 'example3@example.com', 'fact' => 'Is a Scott!', array('hobbies' => array('fartings', 'bikes'))),
        );
        
        $user = @$users[$this->get('id')];
        
        if($user)
        {
            $this->response($user, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'User could not be found'), 404);
        }
    }
    
    function user_post() {
        //$this->some_model->updateUser( $this->get('id') );
        $message = array('id' => $this->get('id'), 'name' => $this->post('name'), 'email' => $this->post('email'), 'message' => 'ADDED!');
        
        $this->response($message, 200); // 200 being the HTTP response code
    }
    
    function user_delete()
    {
    	//$this->some_model->deletesomething( $this->get('id') );
        $message = array('id' => $this->get('id'), 'message' => 'DELETED!');
        
        $this->response($message, 200); // 200 being the HTTP response code
    }
    
    function users_get()
    {
        //$users = $this->some_model->getSomething( $this->get('limit') );
        $users = array(
			array('id' => 1, 'name' => 'Some Guy', 'email' => 'example1@example.com'),
			array('id' => 2, 'name' => 'Person Face', 'email' => 'example2@example.com'),
			3 => array('id' => 3, 'name' => 'Scotty', 'email' => 'example3@example.com', 'fact' => array('hobbies' => array('fartings', 'bikes'))),
		);
        
        if($users)
        {
            $this->response($users, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Couldn\'t find any users!'), 404);
        }
    }


	public function send_post()
	{
		var_dump($this->request->body);
	}


	public function send_put()
	{
		var_dump($this->put('foo'));
	}*/
}