<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Usuario extends CI_Controller {

    /**
     * Se revisa si el usuario esta logueado, si no esta logueado, se reenvia a la pantalla de login
     */
    function __construct() {
        parent::__construct();
        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        }
    }

    function index($mensaje = NULL) {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha ingresado al modulo de usuarios');
        $datos = $this->usuario_model->get_datos_usuario();
        $datos->mensaje = $mensaje;
//        $this->debugeo->imprimir_pre($datos);
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Usuario",
            "usuario" => $this->tank_auth->get_username(),
        );
        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/usuario_view', $datos);
        $this->load->view('front/footer_main_view', array("graficas" => TRUE));
    }

    function actualizar_datos() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha actualizado los datos');
        $datos = $this->input->post();
        $resultado = $this->usuario_model->actualizar_datos($datos);
        if($resultado) {
            $mensaje = "Sus datos han sido actualizados correctamente.";
            $this->index($mensaje);
        } else {
            $mensaje = "Ha ocurrido un error al actualizar sus datos.";
            $this->index($mensaje);
        }
//        $this->debugeo->imprimir_pre($datos);
    }
}