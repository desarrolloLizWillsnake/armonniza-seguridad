<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Este controlador es el encargado de realizar las funciones de los ingresos del sistema
 */

class Ingresos extends CI_Controller {

    /**
     * Esta funcion se encarga de revisar si el usuario esta logueado, si no lo está, lo regresa a la pantalla de login
     */
    function __construct() {
        parent::__construct();
        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        }
    }

    /**
     * Esta funcion se encarga de preparar el encabezado de las paginas, donde se muestra el nombre de usuario y el titulo de la pagina
     * El parametro que necesita es el titulo de la pagina, y devuelve un arreglo con el titulo de la pagina y el nombre de usuario
     * @param $titulo_pagina
     * @return array
     */
    function prep_datos($titulo_pagina) {
        $datos = array(
            "titulo_pagina" => $titulo_pagina,
            "usuario" => $this->tank_auth->get_username(),
        );
        return $datos;
    }
    /**
     * Esta es la funcion base de los ingresos, donde se muestra la pantalla principal de los ingresos
     */
    function index() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha ingresado al modulo de ingresos');
        $datos_header = $this->prep_datos("Armonniza Ingresos");

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/main_view');
        $this->load->view('front/footer_main_view');
    }

    /**
     * Esta funcion se encarga de mostrar la pagina de la estructura de los rubros de los ingresos
     */
    function estructura_rubros() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha ingresado en la estructura de los rubros');

//        Se llama la funcion del modelo de ingresos encargado de contar los niveles que existen
        $total_ingresos = $this->ingresos_model->contar_ingresos_elementos();
//        Se llama a la funcion del model de ingresos encargada de tomar los nombres delos niveles que existen
        $nombres_ingresos = $this->ingresos_model->obtener_nombre_niveles();

//        $this->debugeo->imprimir_pre($nombres_ingresos);

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_ingresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

        $nombre_primerNivel = $this->ingresos_model->datos_primerNivel($nombre[0]);

//        $this->debugeo->imprimir_pre($nombre_primerNivel);

        $niveles = '<div class="row" id="ingresos_estrucura">
                        <div class="col-lg-4">
                            <form role="form">
                                <div class="form-group" id="forma_nivel1">
                                    <div class="title-form center">
                                        <h5>Nivel 1</h5>
                                        <label>'.$nombre[0].'</label>
                                    </div>
                                    <input type="text" class="form-control" id="buscar_nivel1" placeholder="Buscar...">
                                    <select multiple class="form-control formas" id="select_nivel1">';
        foreach($nombre_primerNivel as $fila) {
            $niveles .= '<option value="'.$fila->gerencia.'" title="'.$fila->gerencia.' - '.$fila->descripcion.'">'.$fila->gerencia.' - '.$fila->descripcion.'</option>';
        }

        $niveles .= '                </select>
                                </div>
                            </form>
                        </div>';

        $a = 1;
        $num = 2;

        for($i = 1; $i < $total_ingresos->conteo; $i++){
            $niveles .= '<div class="col-lg-4">
                            <form role="form">
                                <div class="form-group" id="forma_nivel'.$num.'">
                                    <div class="title-form center">
                                        <h5>Nivel '.$num.'</h5>
                                        <label>'.$nombre[$a].'</label>
                                    </div>
                                    <input type="text" class="form-control" id="buscar_nivel'.$num.'" placeholder="Buscar..." disabled>
                                    <select multiple class="form-control formas" id="select_nivel'.$num.'">
                                    </select>
                                </div>
                            </form>
                        </div>';
            $a++;
            $num++;
        }

        $niveles .= '</div>';

//        Se llama a la funcion que se encarga de preparar los datos para el encabezado de la pagina
        $datos_header = $this->prep_datos("Armonniza Estructura Clasificación por Rubros");

//        Se crea un arreglo con los niveles existentes de la tabla de ingresos
        $datos_vista = array(
            "niveles" => $niveles,
            "nombres_ingresos" => $nombre,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/ingresos_estructura_rubros_view', $datos_vista);
        $this->load->view('front/footer_main_view', array("graficas" => TRUE, "ingresos" => TRUE));
    }

    /**
     * Esta funcion se encarga de hacer la adecuaciones presupuestarias para los ingresos
     */
    function adecuaciones_presupuestarias() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha ingresado adecuaciones presupuestarias ');
        $datos_header = $this->prep_datos("Armonniza Adecuaciones Presupuestarias");

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/ingresos_adecuacion_view');
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "adecuaciones_ingresos" => TRUE,
        ));
    }

    function tabla_indice_adecuaciones() {
        $resultado = $this->ingresos_model->get_datos_adecuaciones();

//        $this->debugeo->imprimir_pre($resultado);

//        $resultado = $this->egresos_model->get_datos_adecuaciones();

//        $this->debugeo->imprimir_pre($resultado);

        $output = array("data" => "");
        foreach($resultado as $fila) {

            $firme = '';
            $destino = '';

            if($fila->enfirme == 0) {
                $firme = '<button type="button" class="btn btn-circle btn-firmec" disabled><i class="fa fa-times"></i></button>';
            }
            elseif($fila->enfirme == 1){
                $firme = '<button type="button" class="btn btn-circle btn-firmea" disabled><i class="fa fa-check"></i></button>';
            }

            if($fila->tipo == "Ampliación") {
                $destino = '_ampliacion';
            } elseif($fila->tipo == "Reducción"){
                $destino = '_reduccion';
            } elseif($fila->tipo == "Transferencia"){
                $destino = '_transferencia';
            }

            if($fila->estatus == "activo") {
                $estatus = "success";
                if($fila->enfirme == 0 || $this->utilerias->get_grupo() == 1) {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_ingresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("ingresos/ver".$destino."/".$fila->id_adecuaciones_ingresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                        <a href="'.base_url("ingresos/editar".$destino."/".$fila->id_adecuaciones_ingresos_caratula).'" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                         <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>'
                    );
                }
                elseif($fila->enfirme == 1 && $this->utilerias->get_grupo() == 1) {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_ingresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("ingresos/ver".$destino."/".$fila->id_adecuaciones_ingresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                        <a href="'.base_url("ingresos/editar".$destino."/".$fila->id_adecuaciones_ingresos_caratula).'" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                         <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>'
                    );
                }
                else {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_ingresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("ingresos/ver".$destino."/".$fila->id_adecuaciones_ingresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>'
                    );

                }

            }
            elseif($fila->estatus == "espera"){
                $estatus = "warning";

                if($fila->enfirme == 0 || $this->utilerias->get_firmas() != NULL) {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_ingresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("ingresos/ver".$destino."/".$fila->id_adecuaciones_ingresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                         <a href="'.base_url("ingresos/editar".$destino."/".$fila->id_adecuaciones_ingresos_caratula).'" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                         <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>'
                    );
                }
                elseif($fila->enfirme == 1 && $this->utilerias->get_grupo() == 1) {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_ingresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("ingresos/ver".$destino."/".$fila->id_adecuaciones_ingresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                         <a href="'.base_url("ingresos/editar".$destino."/".$fila->id_adecuaciones_ingresos_caratula).'" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                         <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>'
                    );
                }
                else {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_ingresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("ingresos/ver".$destino."/".$fila->id_adecuaciones_ingresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>'
                    );

                }
            }
            else{
                $estatus = "danger";

                if($fila->enfirme == 0 || $this->utilerias->get_firmas() != NULL) {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_ingresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("ingresos/ver".$destino."/".$fila->id_adecuaciones_ingresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>'
                    );
                }
                elseif($fila->enfirme == 1 && $this->utilerias->get_grupo() == 1) {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_ingresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("ingresos/ver".$destino."/".$fila->id_adecuaciones_ingresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>'
                    );
                }
                else {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_ingresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("ingresos/ver".$destino."/".$fila->id_adecuaciones_ingresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>'
                    );
                }
            }
        }
//        $this->debugeo->imprimir_pre($output);
        echo json_encode($output);
    }

    function tabla_indice_adecuaciones_ampliaciones() {
        $sql = "SELECT * FROM mov_adecuaciones_ingresos_caratula WHERE tipo = 'Ampliación' ORDER BY numero DESC;";
        $resultado = $this->ingresos_model->get_datos_adecuaciones_tipo($sql);

//      $this->debugeo->imprimir_pre($resultado);

        $output = array("data" => "");
        foreach($resultado as $fila) {

            $firme = '';
            $destino = '';

            if($fila->enfirme == 0) {
                $firme = '<button type="button" class="btn btn-circle btn-firmec" disabled><i class="fa fa-times"></i></button>';
            }
            elseif($fila->enfirme == 1){
                $firme = '<button type="button" class="btn btn-circle btn-firmea" disabled><i class="fa fa-check"></i></button>';
            }

            if($fila->tipo == "Ampliación") {
                $destino = '_ampliacion';
            } elseif($fila->tipo == "Reducción"){
                $destino = '_reduccion';
            } elseif($fila->tipo == "Transferencia"){
                $destino = '_transferencia';
            }

            if($fila->estatus == "activo") {
                $estatus = "success";
                if($fila->enfirme == 0 || $this->utilerias->get_grupo() == 1) {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_ingresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("ingresos/ver".$destino."/".$fila->id_adecuaciones_ingresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                        <a href="'.base_url("ingresos/editar".$destino."/".$fila->id_adecuaciones_ingresos_caratula).'" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                         <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>'
                    );
                }
                elseif($fila->enfirme == 1 && $this->utilerias->get_grupo() == 1) {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_ingresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("ingresos/ver".$destino."/".$fila->id_adecuaciones_ingresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                        <a href="'.base_url("ingresos/editar".$destino."/".$fila->id_adecuaciones_ingresos_caratula).'" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                         <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>'
                    );
                }
                else {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_ingresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("ingresos/ver".$destino."/".$fila->id_adecuaciones_ingresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>'
                    );

                }

            }
            elseif($fila->estatus == "espera"){
                $estatus = "warning";

                if($fila->enfirme == 0 || $this->utilerias->get_firmas() != NULL) {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_ingresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("ingresos/ver".$destino."/".$fila->id_adecuaciones_ingresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                         <a href="'.base_url("ingresos/editar".$destino."/".$fila->id_adecuaciones_ingresos_caratula).'" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                         <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>'
                    );
                }
                elseif($fila->enfirme == 1 && $this->utilerias->get_grupo() == 1) {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_ingresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("ingresos/ver".$destino."/".$fila->id_adecuaciones_ingresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                         <a href="'.base_url("ingresos/editar".$destino."/".$fila->id_adecuaciones_ingresos_caratula).'" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                         <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>'
                    );
                }
                else {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_ingresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("ingresos/ver".$destino."/".$fila->id_adecuaciones_ingresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>'
                    );

                }
            }
            else{
                $estatus = "danger";

                if($fila->enfirme == 0 || $this->utilerias->get_firmas() != NULL) {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_ingresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("ingresos/ver".$destino."/".$fila->id_adecuaciones_ingresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>'
                    );
                }
                elseif($fila->enfirme == 1 && $this->utilerias->get_grupo() == 1) {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_ingresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("ingresos/ver".$destino."/".$fila->id_adecuaciones_ingresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>'
                    );
                }
                else {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_ingresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("ingresos/ver".$destino."/".$fila->id_adecuaciones_ingresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>'
                    );
                }
            }
        }
//        $this->debugeo->imprimir_pre($output);
        echo json_encode($output);
    }

    function tabla_indice_adecuaciones_reducciones() {
        $sql = "SELECT * FROM mov_adecuaciones_ingresos_caratula WHERE tipo = 'Reducción' ORDER BY numero DESC;";
        $resultado = $this->ingresos_model->get_datos_adecuaciones_tipo($sql);

//      $this->debugeo->imprimir_pre($resultado);

        $output = array("data" => "");
        foreach($resultado as $fila) {

            $firme = '';
            $destino = '';

            if($fila->enfirme == 0) {
                $firme = '<button type="button" class="btn btn-circle btn-firmec" disabled><i class="fa fa-times"></i></button>';
            }
            elseif($fila->enfirme == 1){
                $firme = '<button type="button" class="btn btn-circle btn-firmea" disabled><i class="fa fa-check"></i></button>';
            }

            if($fila->tipo == "Ampliación") {
                $destino = '_ampliacion';
            } elseif($fila->tipo == "Reducción"){
                $destino = '_reduccion';
            } elseif($fila->tipo == "Transferencia"){
                $destino = '_transferencia';
            }

            if($fila->estatus == "activo") {
                $estatus = "success";
                if($fila->enfirme == 0 || $this->utilerias->get_grupo() == 1) {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_ingresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("ingresos/ver".$destino."/".$fila->id_adecuaciones_ingresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                        <a href="'.base_url("ingresos/editar".$destino."/".$fila->id_adecuaciones_ingresos_caratula).'" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                         <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>'
                    );
                }
                elseif($fila->enfirme == 1 && $this->utilerias->get_grupo() == 1) {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_ingresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("ingresos/ver".$destino."/".$fila->id_adecuaciones_ingresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                        <a href="'.base_url("ingresos/editar".$destino."/".$fila->id_adecuaciones_ingresos_caratula).'" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                         <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>'
                    );
                }
                else {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_ingresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("ingresos/ver".$destino."/".$fila->id_adecuaciones_ingresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>'
                    );

                }

            }
            elseif($fila->estatus == "espera"){
                $estatus = "warning";

                if($fila->enfirme == 0 || $this->utilerias->get_firmas() != NULL) {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_ingresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("ingresos/ver".$destino."/".$fila->id_adecuaciones_ingresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                         <a href="'.base_url("ingresos/editar".$destino."/".$fila->id_adecuaciones_ingresos_caratula).'" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                         <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>'
                    );
                }
                elseif($fila->enfirme == 1 && $this->utilerias->get_grupo() == 1) {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_ingresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("ingresos/ver".$destino."/".$fila->id_adecuaciones_ingresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                         <a href="'.base_url("ingresos/editar".$destino."/".$fila->id_adecuaciones_ingresos_caratula).'" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                         <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>'
                    );
                }
                else {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_ingresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("ingresos/ver".$destino."/".$fila->id_adecuaciones_ingresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>'
                    );

                }
            }
            else{
                $estatus = "danger";

                if($fila->enfirme == 0 || $this->utilerias->get_firmas() != NULL) {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_ingresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("ingresos/ver".$destino."/".$fila->id_adecuaciones_ingresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>'
                    );
                }
                elseif($fila->enfirme == 1 && $this->utilerias->get_grupo() == 1) {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_ingresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("ingresos/ver".$destino."/".$fila->id_adecuaciones_ingresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>'
                    );
                }
                else {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_ingresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("ingresos/ver".$destino."/".$fila->id_adecuaciones_ingresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>'
                    );
                }
            }
        }
//        $this->debugeo->imprimir_pre($output);
        echo json_encode($output);
    }

    function tabla_indice_adecuaciones_transferencias() {
        $sql = "SELECT * FROM mov_adecuaciones_ingresos_caratula WHERE tipo = 'Transferencia' ORDER BY numero DESC;";
        $resultado = $this->ingresos_model->get_datos_adecuaciones_tipo($sql);

//      $this->debugeo->imprimir_pre($resultado);

        $output = array("data" => "");
        foreach($resultado as $fila) {

            $firme = '';
            $destino = '';

            if($fila->enfirme == 0) {
                $firme = '<button type="button" class="btn btn-circle btn-firmec" disabled><i class="fa fa-times"></i></button>';
            }
            elseif($fila->enfirme == 1){
                $firme = '<button type="button" class="btn btn-circle btn-firmea" disabled><i class="fa fa-check"></i></button>';
            }

            if($fila->tipo == "Ampliación") {
                $destino = '_ampliacion';
            } elseif($fila->tipo == "Reducción"){
                $destino = '_reduccion';
            } elseif($fila->tipo == "Transferencia"){
                $destino = '_transferencia';
            }

            if($fila->estatus == "activo") {
                $estatus = "success";
                if($fila->enfirme == 0 || $this->utilerias->get_grupo() == 1) {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_ingresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("ingresos/ver".$destino."/".$fila->id_adecuaciones_ingresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                        <a href="'.base_url("ingresos/editar".$destino."/".$fila->id_adecuaciones_ingresos_caratula).'" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                         <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>'
                    );
                }
                elseif($fila->enfirme == 1 && $this->utilerias->get_grupo() == 1) {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_ingresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("ingresos/ver".$destino."/".$fila->id_adecuaciones_ingresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                        <a href="'.base_url("ingresos/editar".$destino."/".$fila->id_adecuaciones_ingresos_caratula).'" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                         <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>'
                    );
                }
                else {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_ingresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("ingresos/ver".$destino."/".$fila->id_adecuaciones_ingresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>'
                    );

                }

            }
            elseif($fila->estatus == "espera"){
                $estatus = "warning";

                if($fila->enfirme == 0 || $this->utilerias->get_firmas() != NULL) {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_ingresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("ingresos/ver".$destino."/".$fila->id_adecuaciones_ingresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                         <a href="'.base_url("ingresos/editar".$destino."/".$fila->id_adecuaciones_ingresos_caratula).'" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                         <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>'
                    );
                }
                elseif($fila->enfirme == 1 && $this->utilerias->get_grupo() == 1) {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_ingresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("ingresos/ver".$destino."/".$fila->id_adecuaciones_ingresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                         <a href="'.base_url("ingresos/editar".$destino."/".$fila->id_adecuaciones_ingresos_caratula).'" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                         <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>'
                    );
                }
                else {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_ingresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("ingresos/ver".$destino."/".$fila->id_adecuaciones_ingresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>'
                    );

                }
            }
            else{
                $estatus = "danger";

                if($fila->enfirme == 0 || $this->utilerias->get_firmas() != NULL) {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_ingresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("ingresos/ver".$destino."/".$fila->id_adecuaciones_ingresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>'
                    );
                }
                elseif($fila->enfirme == 1 && $this->utilerias->get_grupo() == 1) {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_ingresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("ingresos/ver".$destino."/".$fila->id_adecuaciones_ingresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>'
                    );
                }
                else {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_ingresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("ingresos/ver".$destino."/".$fila->id_adecuaciones_ingresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>'
                    );
                }
            }
        }
//        $this->debugeo->imprimir_pre($output);
        echo json_encode($output);
    }

    function consulta_presupuesto() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha consultado el presupuesto');
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Consulta Presupuestaria",
            "usuario" => $this->tank_auth->get_username(),
            "precompromisocss" => TRUE,
            "tablas" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/consulta_presupuestaria_ingresos_view', array(
            "nivel_superior" => $this->input_nivelsuperior(),
            "niveles_modal_superior" => $this->preparar_estructura_superior(),
        ));
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "adecuaciones_ingresos_agregar_transferencia" => TRUE,
        ));
    }

    /**
     * Seccion Transferencia
     */

    function agregar_transferencia() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha agregado una transferencia');
        $last = 0;
//        Se toma el numero del ultimo precompromiso
        $ultimo = $this->ingresos_model->ultimo_adecuaciones();

        if($ultimo) {
            $last = $ultimo->ultimo + 1;
        }
        else {
            $last = 1;
        }

        $this->ingresos_model->apartarAdecuacion($last, "Transferencia");

        $datos_header = array(
            "titulo_pagina" => "Armonniza | Agregar Transferencia",
            "usuario" => $this->tank_auth->get_username(),
            "precompromisocss" => TRUE,
            "tablas" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/agregar_transferencia_ingresos_view', array(
            "ultimo" => $last,
            "nivel_superior" => $this->input_nivelsuperior(),
            "nivel_inferior" => $this->input_nivelinferior(),
            "niveles_modal_superior" => $this->preparar_estructura_superior(),
            "niveles_modal_inferior" => $this->preparar_estructura_inferior(),
        ));
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "adecuaciones_ingresos_agregar_transferencia" => TRUE,
        ));
    }

    function insertar_transferencia() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha insertado una transferencia');
        $datos = $this->input->post();
        $datos["tipo"] = "Transferencia";

//        $this->debugeo->imprimir_pre($datos);

        if($datos["check_firme"] == 1) {
            $datos["estatus"] = "activo";
            $datos["fecha_autoriza"] = date("Y-m-d");
            $this->actualizar_saldo_transferencia($datos["ultimo"]);
        }
        else {
            $datos["estatus"] = "espera";
        }

        $resultado_insertar = $this->ingresos_model->insertarCaratula($datos);

        echo(json_encode($resultado_insertar));
    }

    private function insertar_transferencia_archivo($datos) {
        $datos["tipo"] = "Transferencia";

        $fecha_solicitud = ($datos['D'] - 25569) * 86400;
        $datos["D"] = date('Y-m-d', $fecha_solicitud);

        $fecha_aplicar = ($datos['E'] - 25569) * 86400;
        $datos["E"] = date('Y-m-d', $fecha_aplicar);

        $resultado_insertar = $this->ingresos_model->insertarCaratulaArchivo($datos);

        return $resultado_insertar;
    }

    function insertar_transferencia_detalle() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha insertado un detalle a la tranferencia');
        $datos = $this->input->post();

        $caracteres = array("$", ",");

        foreach($datos as $key=>$value){
            $datos[$key]=str_replace($caracteres,"",$value);
        }

//        Se llama a la funcion del model de ingresos encargada de tomar los nombres delos niveles que existen
        $nombres_ingresos = $this->ingresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_ingresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

        $datos["enlace"] = rand();

//       Se toman los datos de la estructura superior
        $datos_partida_superior = $this->ingresos_model->datos_de_partida(array("nivel1" => $datos["nivel1_superior"], "nivel2" => $datos["nivel2_superior"], "nivel3" => $datos["nivel3_superior"], "nivel4" => $datos["nivel4_superior"], "nivel5" => $datos["nivel5_superior"]), $nombre);
        $estructura_superior = NULL;
        $id_superior = 0;
        foreach($datos_partida_superior as $fila) {
            $estructura_superior = json_decode($fila->estructura);
            $id_superior = $fila->ID;
//            $this->debugeo->imprimir_pre($estructura_superior);
        }

//       Se toman los datos de la estructura inferior
        $datos_partida_inferior = $this->ingresos_model->datos_de_partida(array("nivel1" => $datos["nivel1_inferior"], "nivel2" => $datos["nivel2_inferior"], "nivel3" => $datos["nivel3_inferior"], "nivel4" => $datos["nivel4_inferior"], "nivel5" => $datos["nivel5_inferior"]), $nombre);
        $estructura_inferior = NULL;
        $id_inferior = 0;
        foreach($datos_partida_inferior as $fila) {
            $estructura_inferior = json_decode($fila->estructura);
            $id_inferior = $fila->ID;
//            $this->debugeo->imprimir_pre($estructura_inferior);
        }

        $resultado = FALSE;

        if($datos["operacion"] == "+") {
            $datos["tipo"] = "Transferencia";
            $datos["texto"] = "Ampliación";
            $resultado = $this->ingresos_model->insertarDetalle($datos);

            if($resultado) {
                $mensaje["mensaje"] = "ok";
                $mensaje["detalle"] = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-check-circle fa-2x ic-msj"></i> Éxito al generar la Ampliación.</div>';
            } else {
                $mensaje["mensaje"] = "error";
                $mensaje["detalle"] = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-times-circle fa-2x ic-msj"></i> Hubo un error al insertar la Amplicación.</div>';
            }

        } elseif($datos["operacion"] == "-") {
            $datos["tipo"] = "Transferencia";
            $datos["texto"] = "Reducción";

//            Se toma el saldo del mes y del año
            $saldo_mes = $this->ingresos_model->get_saldo_partida($datos["mes_destino"], $id_inferior );

            $operacion = $saldo_mes->mes_saldo - $datos["cantidad"];

            if($operacion >= 0) {
                $resultado = $this->ingresos_model->insertarDetalle($datos);
                if($resultado) {
                    $mensaje["mensaje"] = "ok";
                    $mensaje["detalle"] = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-check-circle fa-2x ic-msj"></i> Éxito al generar la Reducción.</div>';
                } else {
                    $mensaje["mensaje"] = "error";
                    $mensaje["detalle"] = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-times-circle fa-2x ic-msj"></i> Hubo un error al insertar la Reducción.</div>';
                }
            } else {
                $mensaje["mensaje"] = "sin_dinero";
                $mensaje["detalle"] = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-times-circle fa-2x ic-msj"></i> No hay dinero suficiente para realizar la Reducción.</div>';
            }
        }

//        $this->debugeo->imprimir_pre($datos);

        echo(json_encode($mensaje));

    }

    private function insertar_transferencia_detalle_archivo($datos) {

        $datos["nivel1_inferior"] = $datos["A"];
        $datos["nivel2_inferior"] = $datos["B"];
        $datos["nivel3_inferior"] = $datos["C"];
        $datos["nivel4_inferior"] = $datos["D"];

        $fecha = ($datos['H'] - 25569) * 86400;
        $fecha = date('Y-m-d', $fecha);

        $datos["fecha_aplicar_detalle"] = $fecha;

        $datos["mes_destino"] = strtolower($datos["E"]);

        $datos["cantidad"] = $datos["G"];

        $caracteres = array("$", ",");

        foreach($datos as $key=>$value){
            $datos[$key]=str_replace($caracteres,"",$value);
        }

//        Se llama a la funcion del model de ingresos encargada de tomar los nombres delos niveles que existen
        $nombres_ingresos = $this->ingresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_ingresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

        $datos["enlace"] = rand();

//       Se toman los datos de la estructura inferior
        $datos_partida_inferior = $this->ingresos_model->datos_de_partida(array("nivel1" => $datos["nivel1_inferior"], "nivel2" => $datos["nivel2_inferior"], "nivel3" => $datos["nivel3_inferior"], "nivel4" => $datos["nivel4_inferior"]), $nombre);
        $estructura_inferior = NULL;
        $id_inferior = 0;
        foreach($datos_partida_inferior as $fila) {
            $estructura_inferior = json_decode($fila->estructura);
            $id_inferior = $fila->ID;
//            $this->debugeo->imprimir_pre($estructura_inferior);
        }

        $resultado = FALSE;

        if($datos["operacion"] == "+") {
            $datos["tipo"] = "Transferencia";
            $datos["texto"] = "Ampliación";
            $resultado = $this->ingresos_model->insertarDetalle($datos);

            if($resultado) {
                $mensaje = TRUE;
            } else {
                $mensaje = FALSE;
            }

        } elseif($datos["operacion"] == "-") {
            $datos["tipo"] = "Transferencia";
            $datos["texto"] = "Reducción";

//            Se toma el saldo del mes y del año
            $saldo_mes = $this->ingresos_model->get_saldo_partida($datos["mes_destino"], $id_inferior );

            $operacion = $saldo_mes->mes_saldo - $datos["cantidad"];

            if($operacion >= 0) {
                $resultado = $this->ingresos_model->insertarDetalle($datos);
                if($resultado) {
                    $mensaje = TRUE;
                } else {
                    $mensaje = FALSE;
                }
            } else {
                $mensaje = "no_dinero";
            }
        }

//        $this->debugeo->imprimir_pre($datos);

        return $mensaje;

    }

    private function actualizar_saldo_transferencia($id) {
        $datos_caratula = $this->ingresos_model->get_datos_caratula($id);
        $datos_detalle = $this->ingresos_model->get_datos_detalle_numero($datos_caratula->numero);

        foreach($datos_detalle as $fila) {
            if($fila->texto == "Reducción") {
                $saldo_mes = $this->ingresos_model->get_saldo_partida($fila->mes_destino, $fila->id_nivel);

                $nuevo_saldo_mes = $saldo_mes->mes_saldo - $fila->total;

                $nuevo_saldo_year = $saldo_mes->total_anual - $fila->total;

                $this->ingresos_model->actualizar_saldo_partida($fila->mes_destino, $nuevo_saldo_mes, $nuevo_saldo_year, $fila->id_nivel);

            } elseif($fila->texto == "Ampliación") {
                $saldo_mes = $this->ingresos_model->get_saldo_partida($fila->mes_destino, $fila->id_nivel);

                $nuevo_saldo_mes = $saldo_mes->mes_saldo + $fila->total;

                $nuevo_saldo_year = $saldo_mes->total_anual + $fila->total;

                $this->ingresos_model->actualizar_saldo_partida($fila->mes_destino, $nuevo_saldo_mes, $nuevo_saldo_year, $fila->id_nivel);

            }
        }

    }

    function tabla_detalle_transferencia() {
        $transferencia = $this->input->post("transferencia", TRUE);

        $query = "SELECT *, COLUMN_JSON(estructura) as estructura FROM mov_adecuaciones_ingresos_detalle WHERE numero_adecuacion = ?;";

        $resultado = $this->ingresos_model->datos_transferenciaDetalle($query, $transferencia);
//        $this->debugeo->imprimir_pre($resultado);

        $output = array();

        foreach($resultado as $fila) {
            $estructura = json_decode($fila->estructura);

            $output[] = array(
                $fila->id_adecuaciones_ingresos_detalle,
                $estructura->gerencia,
                $estructura->centro_de_recaudacion,
                $estructura->rubro,
                $estructura->tipo,
                $estructura->clase,
                $fila->total,
                $fila->fecha_aplicacion,
                $fila->titulo,
                $fila->texto,
                '<a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Eliminar"><i class="fa fa-remove"></i></a>'
            );
        }
//        $this->debugeo->imprimir_pre($output);
        echo json_encode($output);
    }

    function borrar_detalle_transferencia() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha borrado un detalle de tranferencia');

        $transferencia = $this->input->post("transferencia", TRUE);

        $resultado = $this->ingresos_model->borrar_adecuacion_detalle($transferencia);
        if($resultado) {
            $mensaje["mensaje"] = "ok";
        } else {
            $mensaje["mensaje"] = "error";
        }

        echo(json_encode($mensaje));

    }

    function editar_transferencia($transferencia = NULL) {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha editado una tranferencia');
        $resultados = $this->ingresos_model->get_datos_caratula($transferencia);

//        $this->debugeo->imprimir_pre($resultados);

        $datos_header = array(
            "titulo_pagina" => "Armonniza | Editar Transferencia",
            "usuario" => $this->tank_auth->get_username(),
            "precompromisocss" => TRUE,
            "tablas" => TRUE,
        );

        $resultados->nivel_superior = $this->input_nivelsuperior();
        $resultados->nivel_inferior = $this->input_nivelinferior();
        $resultados->niveles_modal_superior = $this->preparar_estructura_superior();
        $resultados->niveles_modal_inferior = $this->preparar_estructura_inferior();

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/editar_transferencia_ingresos_view', $resultados);
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "editar_ingresos_agregar_transferencia" => TRUE,
        ));
    }

    function ver_transferencia($transferencia = NULL) {
        $resultados = $this->ingresos_model->get_datos_caratula($transferencia);

//        $this->debugeo->imprimir_pre($resultados);

        $datos_header = array(
            "titulo_pagina" => "Armonniza | Ver Transferencia",
            "usuario" => $this->tank_auth->get_username(),
            "precompromisocss" => TRUE,
            "tablas" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/ver_transferencia_ingresos_view', $resultados);
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "ver_ingresos_agregar_transferencia" => TRUE,
        ));
    }

    /**
     * Seccion Ampliación
     */

    function agregar_ampliacion() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha agregado una ampliación');
        $last = 0;
//        Se toma el numero del ultimo precompromiso
        $ultimo = $this->ingresos_model->ultimo_adecuaciones();

        if($ultimo) {
            $last = $ultimo->ultimo + 1;
        }
        else {
            $last = 1;
        }

        $this->ingresos_model->apartarAdecuacion($last, "Ampliación");

        $datos_header = array(
            "titulo_pagina" => "Armonniza | Agregar Ampliación",
            "usuario" => $this->tank_auth->get_username(),
            "precompromisocss" => TRUE,
            "tablas" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/agregar_ampliacion_ingresos_view', array(
            "ultimo" => $last,
            "nivel_superior" => $this->input_nivelsuperior(),
            "nivel_inferior" => $this->input_nivelinferior(),
            "niveles_modal_superior" => $this->preparar_estructura_superior(),
            "niveles_modal_inferior" => $this->preparar_estructura_inferior(),
        ));
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "adecuaciones_ingresos_agregar_ampliacion" => TRUE,
        ));
    }

    function insertar_ampliacion() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha insertado una ampliación');
        $datos = $this->input->post();
        $datos["tipo"] = "Ampliación";

        if($datos["check_firme"] == 1) {
            $datos["estatus"] = "activo";
            $datos["fecha_autoriza"] = date("Y-m-d");
            $this->actualizar_saldo_ampliacion($datos["ultimo"]);
        }
        else {
            $datos["estatus"] = "espera";
        }

        $resultado_insertar = $this->ingresos_model->insertarCaratula($datos);

        echo(json_encode($resultado_insertar));
    }

    private function insertar_ampliacion_archivo($datos) {
        $datos["tipo"] = "Ampliación";

        $fecha_solicitud = ($datos['D'] - 25569) * 86400;
        $datos["D"] = date('Y-m-d', $fecha_solicitud);

        $fecha_aplicar = ($datos['E'] - 25569) * 86400;
        $datos["E"] = date('Y-m-d', $fecha_aplicar);

        $resultado_insertar = $this->ingresos_model->insertarCaratulaArchivo($datos);

        return $resultado_insertar;
    }

    function insertar_ampliacion_detalle() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha insertado ampliación de detalle.');
        $datos = $this->input->post();
        $datos["tipo"] = "Ampliación";
        $datos["texto"] = "Ampliación";
        $datos["enlace"] = 0;

        $caracteres = array("$", ",");

        foreach($datos as $key=>$value){
            $datos[$key]=str_replace($caracteres,"",$value);
        }

//        Se llama a la funcion del model de ingresos encargada de tomar los nombres delos niveles que existen
        $nombres_ingresos = $this->ingresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_ingresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

//       Se toman los datos de la estructura superior
        $datos_partida_superior = $this->ingresos_model->datos_de_partida(array("nivel1" => $datos["nivel1_superior"], "nivel2" => $datos["nivel2_superior"], "nivel3" => $datos["nivel3_superior"], "nivel4" => $datos["nivel4_superior"], "nivel5" => $datos["nivel5_superior"]), $nombre);
        $estructura_superior = NULL;
        $id_superior = 0;
        foreach($datos_partida_superior as $fila) {
            $estructura_superior = json_decode($fila->estructura);
            $id_superior = $fila->ID;
//            $this->debugeo->imprimir_pre($estructura_superior);
        }

//       Se toman los datos de la estructura inferior
        $datos_partida_inferior = $this->ingresos_model->datos_de_partida(array("nivel1" => $datos["nivel1_inferior"], "nivel2" => $datos["nivel2_inferior"], "nivel3" => $datos["nivel3_inferior"], "nivel4" => $datos["nivel4_inferior"], "nivel5" => $datos["nivel4_inferior"]), $nombre);
        $estructura_inferior = NULL;
        $id_inferior = 0;
        foreach($datos_partida_inferior as $fila) {
            $estructura_inferior = json_decode($fila->estructura);
            $id_inferior = $fila->ID;
//            $this->debugeo->imprimir_pre($estructura_inferior);
        }

        $mensaje = array("mensaje" => "");

        $resultado = $this->ingresos_model->insertarDetalle($datos);

        if($resultado) {
            $mensaje["mensaje"] = "ok";
            $mensaje["detalle"] = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-check-circle fa-2x ic-msj"></i> Éxito al generar la Ampliación.</div>';
        } else {
            $mensaje["mensaje"] = "error";
            $mensaje["detalle"] = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-times-circle fa-2x ic-msj"></i> Hubo un error al insertar la Amplicación.</div>';
        }

        echo(json_encode($mensaje));

    }

    private function insertar_ampliacion_detalle_archivo($datos) {

        $datos["nivel1_inferior"] = $datos["A"];
        $datos["nivel2_inferior"] = $datos["B"];
        $datos["nivel3_inferior"] = $datos["C"];
        $datos["nivel4_inferior"] = $datos["D"];
        $datos["nivel5_inferior"] = $datos["E"];

        $fecha = ($datos['H'] - 25569) * 86400;
        $fecha = date('Y-m-d', $fecha);

        $datos["fecha_aplicar_detalle"] = $fecha;

        $datos["mes_destino"] = strtolower($datos["F"]);

        $datos["cantidad"] = $datos["H"];

        $datos["tipo"] = "Ampliación";
        $datos["texto"] = "Ampliación";
        $datos["enlace"] = 0;

        $caracteres = array("$", ",");

        foreach($datos as $key=>$value){
            $datos[$key]=str_replace($caracteres,"",$value);
        }

//        Se llama a la funcion del model de ingresos encargada de tomar los nombres delos niveles que existen
        $nombres_ingresos = $this->ingresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_ingresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

//       Se toman los datos de la estructura inferior
        $datos_partida_inferior = $this->ingresos_model->datos_de_partida(array("nivel1" => $datos["nivel1_inferior"], "nivel2" => $datos["nivel2_inferior"], "nivel3" => $datos["nivel3_inferior"], "nivel4" => $datos["nivel4_inferior"], "nivel5" => $datos["nivel5_inferior"]), $nombre);
        $estructura_inferior = NULL;
        $id_inferior = 0;
        foreach($datos_partida_inferior as $fila) {
            $estructura_inferior = json_decode($fila->estructura);
            $id_inferior = $fila->ID;
//            $this->debugeo->imprimir_pre($estructura_inferior);
        }

        $mensaje = array("mensaje" => "");

//            Se toma el saldo del mes y del año
        $saldo_mes = $this->ingresos_model->get_saldo_partida_inicial($datos["mes_destino"], $id_inferior );

        $resultado = $this->ingresos_model->insertarDetalle($datos);

        if($resultado) {
            $mensaje = TRUE;
        } else {
            $mensaje = FALSE;
        }

        return $mensaje;

    }

    private function actualizar_saldo_ampliacion($id) {
        $datos_caratula = $this->ingresos_model->get_datos_caratula($id);
        $datos_detalle = $this->ingresos_model->get_datos_detalle_numero($datos_caratula->numero);

        foreach($datos_detalle as $fila) {
//            Se toma el saldo del mes y del año
            $saldo_mes = $this->ingresos_model->get_saldo_partida_inicial($fila->mes_destino, $fila->id_nivel);

            if($saldo_mes["mes_saldo"] == 0) {
                $nuevo_saldo_mes = $saldo_mes["mes_inicial"] + $fila->total;
                $nuevo_saldo_year = $saldo_mes["total_anual"] + $fila->total;
            } else {
                $nuevo_saldo_mes = $saldo_mes["mes_saldo"] + $fila->total;
                $nuevo_saldo_year = $saldo_mes["total_anual"] + $fila->total;
            }

//            Se genera el movimiento
            $this->ingresos_model->actualizar_saldo_partida($fila->mes_destino, $nuevo_saldo_mes, $nuevo_saldo_year, $fila->id_nivel);
        }

    }

    function tabla_detalle_ampliacion() {
        $ampliacion = $this->input->post("ampliacion", TRUE);

        $query = "SELECT *, COLUMN_JSON(estructura) as estructura FROM mov_adecuaciones_ingresos_detalle WHERE numero_adecuacion = ?;";

        $resultado = $this->ingresos_model->datos_transferenciaDetalle($query, $ampliacion);
//        $this->debugeo->imprimir_pre($resultado);

        $output = array();

        foreach($resultado as $fila) {
            $estructura = json_decode($fila->estructura);

            $output[] = array(
                $fila->id_adecuaciones_ingresos_detalle,
                $estructura->gerencia,
                $estructura->centro_de_recaudacion,
                $estructura->rubro,
                $estructura->tipo,
                $estructura->clase,
                $fila->total,
                $fila->fecha_aplicacion,
                $fila->titulo,
                $fila->texto,
                '<a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Eliminar"><i class="fa fa-remove"></i></a>'
            );
        }
//        $this->debugeo->imprimir_pre($output);
        echo json_encode($output);
    }

    function borrar_detalle_ampliacion() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha borrado el detalle de la ampliación');
        $ampliacion = $this->input->post("ampliacion", TRUE);

        $resultado = $this->ingresos_model->borrar_adecuacion_detalle($ampliacion);
        if($resultado) {
            $mensaje["mensaje"] = "ok";
        } else {
            $mensaje["mensaje"] = "error";
        }

        echo(json_encode($mensaje));

    }

    function editar_ampliacion($ampliacion = NULL) {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha editado una ampliación');
        $resultados = $this->ingresos_model->get_datos_caratula($ampliacion);

//        $this->debugeo->imprimir_pre($resultados);

        $datos_header = array(
            "titulo_pagina" => "Armonniza | Editar Ampliación",
            "usuario" => $this->tank_auth->get_username(),
            "precompromisocss" => TRUE,
            "tablas" => TRUE,
        );

        $resultados->nivel_superior = $this->input_nivelsuperior();
        $resultados->nivel_inferior = $this->input_nivelinferior();
        $resultados->niveles_modal_superior = $this->preparar_estructura_superior();
        $resultados->niveles_modal_inferior = $this->preparar_estructura_inferior();

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/editar_ampliacion_ingresos_view', $resultados);
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "editar_ingresos_agregar_ampliacion" => TRUE,
        ));
    }

    function ver_ampliacion($ampliacion = NULL) {
        $resultados = $this->ingresos_model->get_datos_caratula($ampliacion);

//        $this->debugeo->imprimir_pre($resultados);

        $datos_header = array(
            "titulo_pagina" => "Armonniza | Ver Ampliación",
            "usuario" => $this->tank_auth->get_username(),
            "precompromisocss" => TRUE,
            "tablas" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/ver_ampliacion_ingresos_view', $resultados);
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "ver_ingresos_agregar_ampliacion" => TRUE,
        ));
    }

    /**
     * Seccion de Reducción
     */

    function agregar_reduccion() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha agregado una reducción');
        $last = 0;
//        Se toma el numero del ultimo precompromiso
        $ultimo = $this->ingresos_model->ultimo_adecuaciones();

        if($ultimo) {
            $last = $ultimo->ultimo + 1;
        }
        else {
            $last = 1;
        }

        $this->ingresos_model->apartarAdecuacion($last, "Reducción");

        $datos_header = array(
            "titulo_pagina" => "Armonniza | Agregar Reducción",
            "usuario" => $this->tank_auth->get_username(),
            "precompromisocss" => TRUE,
            "tablas" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/agregar_reduccion_ingresos_view', array(
            "ultimo" => $last,
            "nivel_superior" => $this->input_nivelsuperior(),
            "nivel_inferior" => $this->input_nivelinferior(),
            "niveles_modal_superior" => $this->preparar_estructura_superior(),
            "niveles_modal_inferior" => $this->preparar_estructura_inferior(),
        ));
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "adecuaciones_ingresos_agregar_reduccion" => TRUE,
        ));
    }

    function insertar_reduccion() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha insertado una reducción');
        $datos = $this->input->post();
        $datos["tipo"] = "Reducción";

        if($datos["check_firme"] == 1) {
            $datos["estatus"] = "activo";
            $datos["fecha_autoriza"] = date("Y-m-d");
            $this->actualizar_saldo_reduccion($datos["ultimo"]);
        }
        else {
            $datos["estatus"] = "espera";
        }

        $resultado_insertar = $this->ingresos_model->insertarCaratula($datos);

        echo(json_encode($resultado_insertar));
    }

    private function insertar_reduccion_archivo($datos) {

        $datos["tipo"] = "Reducción";

        $fecha_solicitud = ($datos['D'] - 25569) * 86400;
        $datos["D"] = date('Y-m-d', $fecha_solicitud);

        $fecha_aplicar = ($datos['E'] - 25569) * 86400;
        $datos["E"] = date('Y-m-d', $fecha_aplicar);

        $resultado_insertar = $this->ingresos_model->insertarCaratulaArchivo($datos);

        return $resultado_insertar;
    }

    function insertar_reduccion_detalle() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha insertado un detalle a la reducción');
        $datos = $this->input->post();
        $datos["tipo"] = "Reducción";
        $datos["texto"] = "Reducción";
        $datos["enlace"] = 0;

        $caracteres = array("$", ",");

        foreach($datos as $key=>$value){
            $datos[$key]=str_replace($caracteres,"",$value);
        }

//        Se llama a la funcion del model de ingresos encargada de tomar los nombres delos niveles que existen
        $nombres_ingresos = $this->ingresos_model->obtener_nombre_niveles();

//        $this->debugeo->imprimir_pre($nombres_ingresos);

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_ingresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

////       Se toman los datos de la estructura superior
//        $datos_partida_superior = $this->ingresos_model->datos_de_partida(array("nivel1" => $datos["nivel1_superior"], "nivel2" => $datos["nivel2_superior"], "nivel3" => $datos["nivel3_superior"], "nivel4" => $datos["nivel4_superior"], "nivel5" => $datos["nivel5_superior"]), $nombre);
//        $estructura_superior = NULL;
//        $id_superior = 0;
//        foreach($datos_partida_superior as $fila) {
//            $estructura_superior = json_decode($fila->estructura);
//            $id_superior = $fila->ID;
////            $this->debugeo->imprimir_pre($estructura_superior);
//        }

//       Se toman los datos de la estructura inferior
        $datos_partida_inferior = $this->ingresos_model->datos_de_partida(array("nivel1" => $datos["nivel1_inferior"], "nivel2" => $datos["nivel2_inferior"], "nivel3" => $datos["nivel3_inferior"], "nivel4" => $datos["nivel4_inferior"], "nivel5" => $datos["nivel5_inferior"]), $nombre);
        $estructura_inferior = NULL;
        $id_inferior = 0;
        foreach($datos_partida_inferior as $fila) {
            $estructura_inferior = json_decode($fila->estructura);
            $id_inferior = $fila->ID;
//            $this->debugeo->imprimir_pre($estructura_inferior);
        }

        $mensaje = array("mensaje" => "");

//            Se toma el saldo del mes y del año
        $saldo_mes = $this->ingresos_model->get_saldo_partida($datos["mes_destino"], $id_inferior );

        $operacion = $saldo_mes->mes_saldo - $datos["cantidad"];

        if($operacion >= 0) {
            $resultado = $this->ingresos_model->insertarDetalle($datos);
            if($resultado) {
                $mensaje["mensaje"] = "ok";
                $mensaje["detalle"] = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-check-circle fa-2x ic-msj"></i> Éxito al generar la Reducción.</div>';
            } else {
                $mensaje["mensaje"] = "error";
                $mensaje["detalle"] = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-times-circle fa-2x ic-msj"></i> Hubo un error al insertar la Reducción.</div>';
            }
        } else {
            $mensaje["mensaje"] = "sin_dinero";
            $mensaje["detalle"] = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-times-circle fa-2x ic-msj"></i> No hay dinero suficiente para realizar la Reducción.</div>';
        }

        echo(json_encode($mensaje));
    }

    private function insertar_reduccion_detalle_archivo($datos) {

        $datos["nivel1_inferior"] = $datos["A"];
        $datos["nivel2_inferior"] = $datos["B"];
        $datos["nivel3_inferior"] = $datos["C"];
        $datos["nivel4_inferior"] = $datos["D"];
        $datos["nivel5_inferior"] = $datos["E"];

        $fecha = ($datos['H'] - 25569) * 86400;
        $fecha = date('Y-m-d', $fecha);

        $datos["fecha_aplicar_detalle"] = $fecha;

        $datos["mes_destino"] = strtolower($datos["F"]);

        $datos["cantidad"] = $datos["H"];

        $datos["tipo"] = "Reducción";
        $datos["texto"] = "Reducción";
        $datos["enlace"] = 0;

        $caracteres = array("$", ",");

        foreach($datos as $key=>$value){
            $datos[$key]=str_replace($caracteres,"",$value);
        }

//        Se llama a la funcion del model de ingresos encargada de tomar los nombres delos niveles que existen
        $nombres_ingresos = $this->ingresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_ingresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

//       Se toman los datos de la estructura inferior
        $datos_partida_inferior = $this->ingresos_model->datos_de_partida(array("nivel1" => $datos["nivel1_inferior"], "nivel2" => $datos["nivel2_inferior"], "nivel3" => $datos["nivel3_inferior"], "nivel4" => $datos["nivel4_inferior"], "nivel5" => $datos["nivel5_inferior"]), $nombre);
        $estructura_inferior = NULL;
        $id_inferior = 0;
        foreach($datos_partida_inferior as $fila) {
            $estructura_inferior = json_decode($fila->estructura);
            $id_inferior = $fila->ID;
//            $this->debugeo->imprimir_pre($estructura_inferior);
        }

        $mensaje = array("mensaje" => "");

        $saldo_mes = $this->ingresos_model->get_saldo_partida_inicial($datos["mes_destino"], $id_inferior );

        $resultado = $this->ingresos_model->insertarDetalle($datos);

        if($resultado) {
            $mensaje = TRUE;
        } else {
            $mensaje = FALSE;
        }

        return $mensaje;
    }

    private function actualizar_saldo_reduccion($id) {
        $datos_caratula = $this->ingresos_model->get_datos_caratula($id);
        $datos_detalle = $this->ingresos_model->get_datos_detalle_numero($datos_caratula->numero);

        foreach($datos_detalle as $fila) {
//            Se toma el saldo del mes y del año
            $saldo_mes = $this->ingresos_model->get_saldo_partida_inicial($fila->mes_destino, $fila->id_nivel);

            if($saldo_mes["mes_saldo"] == 0) {
                $nuevo_saldo_mes = $saldo_mes["mes_inicial"] - $fila->total;
                $nuevo_saldo_year = $saldo_mes["total_anual"] - $fila->total;
            } else {
                $nuevo_saldo_mes = $saldo_mes["mes_saldo"] - $fila->total;
                $nuevo_saldo_year = $saldo_mes["total_anual"] - $fila->total;
            }

//            Se genera el movimiento
            $this->ingresos_model->actualizar_saldo_partida($fila->mes_destino, $nuevo_saldo_mes, $nuevo_saldo_year, $fila->id_nivel);
        }

    }

    function tabla_detalle_reduccion() {
        $reduccion = $this->input->post("reduccion", TRUE);

        $query = "SELECT *, COLUMN_JSON(estructura) as estructura FROM mov_adecuaciones_ingresos_detalle WHERE numero_adecuacion = ?;";

        $resultado = $this->ingresos_model->datos_transferenciaDetalle($query, $reduccion);
//        $this->debugeo->imprimir_pre($resultado);

        $output = array();

        foreach($resultado as $fila) {
            $estructura = json_decode($fila->estructura);

            $output[] = array(
                $fila->id_adecuaciones_ingresos_detalle,
                $estructura->gerencia,
                $estructura->centro_de_recaudacion,
                $estructura->rubro,
                $estructura->tipo,
                $estructura->clase,
                $fila->total,
                $fila->fecha_aplicacion,
                $fila->titulo,
                $fila->texto,
                '<a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Eliminar"><i class="fa fa-remove"></i></a>'
            );
        }
//        $this->debugeo->imprimir_pre($output);
        echo json_encode($output);
    }

    function borrar_detalle_reduccion() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha borrado un detalle a la reducción');
        $reduccion = $this->input->post("reduccion", TRUE);

        $datos = $this->ingresos_model->get_datos_detalle($reduccion);
//        $this->debugeo->imprimir_pre($datos);


        $mensaje = array("mensaje" => "");

        $resultado = $this->ingresos_model->borrar_adecuacion_detalle($reduccion);
        if($resultado) {
            $mensaje["mensaje"] = "ok";
        } else {
            $mensaje["mensaje"] = "error";
        }

        echo(json_encode($mensaje));
    }

    function editar_reduccion($ampliacion = NULL) {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha editado una reducción');
        $resultados = $this->ingresos_model->get_datos_caratula($ampliacion);

//        $this->debugeo->imprimir_pre($resultados);

        $datos_header = array(
            "titulo_pagina" => "Armonniza | Editar Reducción",
            "usuario" => $this->tank_auth->get_username(),
            "precompromisocss" => TRUE,
            "tablas" => TRUE,
        );

        $resultados->nivel_superior = $this->input_nivelsuperior();
        $resultados->nivel_inferior = $this->input_nivelinferior();
        $resultados->niveles_modal_superior = $this->preparar_estructura_superior();
        $resultados->niveles_modal_inferior = $this->preparar_estructura_inferior();

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/editar_reduccion_ingresos_view', $resultados);
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "editar_ingresos_agregar_reduccion" => TRUE,
        ));
    }

    function ver_reduccion($ampliacion = NULL) {
        $resultados = $this->ingresos_model->get_datos_caratula($ampliacion);

//        $this->debugeo->imprimir_pre($resultados);

        $datos_header = array(
            "titulo_pagina" => "Armonniza | Ver Reducción",
            "usuario" => $this->tank_auth->get_username(),
            "precompromisocss" => TRUE,
            "tablas" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/ver_reduccion_ingresos_view', $resultados);
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "ver_ingresos_agregar_reduccion" => TRUE,
        ));
    }

    function cancelar_adecuacion() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha cancelado una adecuación');
        $adecuacion = $this->input->post("adecuacion", TRUE);

        $datos_caratula = $this->ingresos_model->get_datos_caratula_cancelar($adecuacion);
        $datos_detalle = $this->ingresos_model->get_datos_detalle_numero($adecuacion);

        if($datos_caratula->enfirme == 1) {
            foreach($datos_detalle as $fila) {
                if($fila->tipo == "Reducción") {
                    $saldo_mes = $this->ingresos_model->get_saldo_partida($fila->mes_destino, $fila->id_nivel);

                    $nuevo_saldo_mes = $saldo_mes->mes_saldo + $fila->total;

                    $nuevo_saldo_year = $saldo_mes->total_anual + $fila->total;

                    $this->ingresos_model->actualizar_saldo_partida($fila->mes_destino, $nuevo_saldo_mes, $nuevo_saldo_year, $fila->id_nivel);

                } elseif($fila->tipo == "Ampliación") {
                    $saldo_mes = $this->ingresos_model->get_saldo_partida($fila->mes_destino, $fila->id_nivel);

                    $nuevo_saldo_mes = $saldo_mes->mes_saldo - $fila->total;

                    $nuevo_saldo_year = $saldo_mes->total_anual - $fila->total;

                    $this->ingresos_model->actualizar_saldo_partida($fila->mes_destino, $nuevo_saldo_mes, $nuevo_saldo_year, $fila->id_nivel);

                }
            }

            $this->ingresos_model->cancelarAdecuacion($adecuacion);
        } else {
            $this->ingresos_model->cancelarAdecuacion($adecuacion);
        }


        echo(json_encode(array("mensaje" => "ok")));
    }

    function preparar_query() {
//        Se llama la funcion del modelo de ingresos encargado de contar los niveles que existen
        $total_ingresos = $this->ingresos_model->contar_ingresos_elementos();
//        Se llama a la funcion del model de ingresos encargada de tomar los nombres delos niveles que existen
        $nombres_ingresos = $this->ingresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_ingresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

        $query_insertar = " COLUMN_CREATE(";

        for($i = 0; $i < $total_ingresos->conteo; $i++){
            $query_insertar .= "'".strtolower(str_replace(' ', '_', $nombre[$i]))."', ?, ";
        }

        $query_insertar .= "'id_nivel', ?), ";

        echo($query_insertar);
    }

    function obtenerDatosPartida() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha obtenido los datos de las partias');
//        Se llama a la funcion del model de ingresos encargada de tomar los nombres delos niveles que existen
        $nombres_ingresos = $this->ingresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_ingresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

//        Se toman los valores del primer, segundo, tercer, cuarto, quinto y sexto nivel que son enviados por POST
        $datos = $this->input->post();

//        Se llama a la libreria de utilirerias, para preparar el mes actual
        $mes_actual = $this->utilerias->preparar_mes_actual();

//        Se llama a la funcion que se encarga de tomar los datos de la partida para desplegar la informacion necesaria
        $datos_nivel = $this->ingresos_model->datos_de_partida($datos, $nombre);

//        Se devuelven los valores en formato JSON para poder ingresarlos a la grafica de balance de dinero por mes
        foreach($datos_nivel as $row) {
            $estructura = json_decode($row->estructura);
            echo(json_encode($estructura));
        }
    }

    /**
     * Sección para subir una adecuación
     */
    function subir_adecuacion() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha subido una adecución');

//        Se prepara la configuracion de la libreria "Upload"
        $config['upload_path'] = './application/archivos/';
        $config['allowed_types'] = '*';

//        Se carga la libreria con sus configuraciones correspondientes
        $this->load->library('upload', $config);

//        Si el server no logra subir el archivo, despliega un mensaje de error
        if ( ! $this->upload->do_upload('archivoSubir')) {
//            Se capturan los errores en una variable y se imprimen para debug
            $error = array('error' => $this->upload->display_errors());
            $this->debugeo->imprimir_pre($error);
        }
        else {
            ini_set('memory_limit', '-1');
//            load our new PHPExcel library
            $this->load->library('excel');
//            Si el server logra subir el archivo, se toman los datos del archivo
            $data = $this->upload->data();

//            Se toma la ruta del archivo junto con su nombre y extension
            $file = $data["full_path"];

//            read file from path
            $objPHPExcel = PHPExcel_IOFactory::load($file);

//            get only the Cell Collection
            $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();


            try {

//                extract to a PHP readable array format
                foreach ($cell_collection as $cell) {
                    $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
                    $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
                    $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();

//                    header will/should be in row 1 only. of course this can be modified to suit your need.
                    if ($row == 1 || $row == 3) {
                        continue;
                    } elseif ($row == 2) {
                        if(empty($data_value)) {
                            throw new Exception('Ha ocurrido un error en el encabezado del documento, por favor revíselo.');
                        } else {
                            $datos_caratula[$row][$column] = trim($data_value);
                        }
                    } else {
                        if(empty($data_value)) {
                            throw new Exception('Ha ocurrido un error en el detalle del documento, por favor revíselo.');
                        } else {
                            $datos_detalle[$row][$column] = trim($data_value);
                        }

                    }
                }

                $last = 0;
                $tipo_movimiento = "";
                $total = 0;
                $ampliacion = 0;
                $reduccion = 0;

//                Se toma el numero de la última adecuacion
                $ultimo = $this->ingresos_model->ultimo_adecuaciones();

                if($ultimo) {
                    $last = $ultimo->ultimo + 1;
                }
                else {
                    $last = 1;
                }

                foreach($datos_caratula as $fila) {
                    $tipo_movimiento = strtoupper($fila["A"]);
                }

//                $this->debugeo->imprimir_pre($datos_detalle);

                foreach($datos_detalle as $fila) {
                    if($tipo_movimiento == "Transferencia" || $tipo_movimiento == "TRANSFERENCIA") {
                        if(strtoupper($fila["H"]) == "Ampliación" || strtoupper($fila["H"]) == "AMPLIACIÓN" || strtoupper($fila["H"]) == "Ampliacion" || strtoupper($fila["H"]) == "AMPLIACION" || strtoupper($fila["H"]) == "AMPLIACIóN") {
                            $ampliacion += $fila["I"];

                        } elseif(strtoupper($fila["H"]) == "Reducción" || strtoupper($fila["H"]) == "REDUCCIÓN" || strtoupper($fila["H"]) == "Reduccion" || strtoupper($fila["H"]) == "REDUCCION" || strtoupper($fila["H"]) == "REDUCCIóN") {
                            $reduccion += $fila["I"];
                        }
                    }
                }

                $ampliacion_cadena = (string)round($ampliacion, 2);
                $reduccion_cadena = (string)round($reduccion, 2);

//                $this->debugeo->imprimir_pre($ampliacion_cadena);
//                $this->debugeo->imprimir_pre($reduccion_cadena);

                if($ampliacion_cadena != $reduccion_cadena) {
//                    goto fin;
                    throw new Exception('Las suma de las cantidades entre ampliaciones y reducciones no son iguales, por favor revise su documento.');
                }

                $numero_fila = 1;

                foreach($datos_detalle as $fila) {
//                    $this->debugeo->imprimir_pre($fila);
                    $fila["ultimo"] = $last;
                    $total += $fila["H"];

                    $existe = $this->revisar_estructura_existe($fila);

                    if($existe) {

                        if($tipo_movimiento == "Ampliación" || $tipo_movimiento == "AMPLIACIÓN" || $tipo_movimiento == "Ampliacion" || $tipo_movimiento == "AMPLIACION" || $tipo_movimiento == "AMPLIACIóN") {
                            $respuesta["detalle"] = $this->insertar_ampliacion_detalle_archivo($fila);
                            if(!$respuesta["detalle"]) {
                                $this->db->delete('mov_adecuaciones_ingresos_detalle', array('numero_adecuacion' => $fila["ultimo"]));
                                throw new Exception('Ha ocurrido un error al insertar los datos, por favor intentelo de nuevo.');
                            }
                        } elseif($tipo_movimiento == "Reducción" || $tipo_movimiento == "REDUCCIÓN" || $tipo_movimiento == "Reduccion" || $tipo_movimiento == "REDUCCION" || $tipo_movimiento == "REDUCCIóN") {
                            $respuesta["detalle"] = $this->insertar_reduccion_detalle_archivo($fila);
                            if($respuesta["detalle"] == FALSE) {
                                $this->db->delete('mov_adecuaciones_ingresos_detalle', array('numero_adecuacion' => $fila["ultimo"]));
                                throw new Exception('Ha ocurrido un error al insertar los datos, por favor intentelo de nuevo.');
                            } elseif($respuesta["detalle"] === "no_dinero") {
                                $this->db->delete('mov_adecuaciones_ingresos_detalle', array('numero_adecuacion' => $fila["ultimo"]));
                                throw new Exception('No hay suficiencia presupuestaria en la línea '.$numero_fila.'.');
                            }
                        } elseif($tipo_movimiento == "Transferencia" || $tipo_movimiento == "TRANSFERENCIA") {

                            if(strtoupper($fila["G"]) == "Ampliación" || strtoupper($fila["G"]) == "AMPLIACIÓN" || strtoupper($fila["G"]) == "Ampliacion" || strtoupper($fila["G"]) == "AMPLIACION" || strtoupper($fila["G"]) == "AMPLIACIóN") {
                                $fila["operacion"] = "+";
                                $respuesta["detalle"] = $this->insertar_transferencia_detalle_archivo($fila);
                                if(!$respuesta["detalle"]) {
                                    $this->db->delete('mov_adecuaciones_ingresos_detalle', array('numero_adecuacion' => $fila["ultimo"]));
                                    throw new Exception('Ha ocurrido un error al insertar los datos, por favor intentelo de nuevo.');
                                }
                            } elseif(strtoupper($fila["G"]) == "Reducción" || strtoupper($fila["G"]) == "REDUCCIÓN" || strtoupper($fila["G"]) == "Reduccion" || strtoupper($fila["G"]) == "REDUCCION" || strtoupper($fila["G"]) == "REDUCCIóN") {
                                $fila["operacion"] = "-";
                                $respuesta["detalle"] = $this->insertar_transferencia_detalle_archivo($fila);
                                if(!$respuesta["detalle"]) {
                                    $this->db->delete('mov_adecuaciones_ingresos_detalle', array('numero_adecuacion' => $fila["ultimo"]));
                                    throw new Exception('Ha ocurrido un error al insertar los datos, por favor intentelo de nuevo.');
                                } elseif($respuesta["detalle"] === "no_dinero") {
                                    $this->db->delete('mov_adecuaciones_ingresos_detalle', array('numero_adecuacion' => $fila["ultimo"]));
                                    throw new Exception('No hay suficiencia presupuestaria en la línea '.$numero_fila.'.');
                                }
                            }
                        }
                    } else {
                        throw new Exception('La estructura en la fila '.$numero_fila.' no existe, por favor revísela.');
                    }

                    $numero_fila += 1;
                }

                $respuesta_caratula = FALSE;

                foreach($datos_caratula as $row) {
                    $row["ultimo"] = $last;
                    $row["total"] = $total;

                    if($tipo_movimiento == "Ampliación" || $tipo_movimiento == "AMPLIACIÓN" || $tipo_movimiento == "Ampliacion" || $tipo_movimiento == "AMPLIACION" || $tipo_movimiento == "AMPLIACIóN") {
                        $respuesta_caratula = $this->insertar_ampliacion_archivo($row);
                    } elseif($tipo_movimiento == "Reducción" || $tipo_movimiento == "REDUCCIÓN" || $tipo_movimiento == "Reduccion" || $tipo_movimiento == "REDUCCION" || $tipo_movimiento == "REDUCCIóN") {
                        $respuesta_caratula = $this->insertar_reduccion_archivo($row);
                    } elseif($tipo_movimiento == "Transferencia" || $tipo_movimiento == "TRANSFERENCIA") {
                        $row["total"] = $total / 2;
                        $respuesta_caratula = $this->insertar_transferencia_archivo($row);
                    };

                }

                if(!$respuesta_caratula) {
                    $this->db->delete('mov_adecuaciones_ingresos_caratula', array('numero' => $last));
                    throw new Exception('Ha ocurrido un error al insertar los datos, en la caratula por favor intentelo de nuevo.');
                }

                $datos_header = array(
                    "titulo_pagina" => "Armonniza | Resultado Archivo",
                    "usuario" => $this->tank_auth->get_username(),
                    "precompromisocss" => TRUE,
                );

                $this->parser->parse('front/header_main_view', $datos_header);
                $this->load->view('front/resultado_archivo_view', array(
                    "mensaje" => '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>La adecucación fue insertada correctamente.</div>',
                ));
                $this->load->view('front/footer_main_view');


            } catch (Exception $e) {
                $datos_header = array(
                    "titulo_pagina" => "Armonniza | Resultado Archivo",
                    "usuario" => $this->tank_auth->get_username(),
                    "precompromisocss" => TRUE,
                );

                $this->parser->parse('front/header_main_view', $datos_header);
                $this->load->view('front/resultado_archivo_view', array(
                    "mensaje" => '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$e->getMessage().'</div>',
                ));
                $this->load->view('front/footer_main_view');

            }

        }

    }

    /**
     * Esta funcion es para mostrar los datos de la consulta presupuestaria de los ingresos
     */
    function consulta_presupuestaria() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha consultado los datos presupuestarios');
        $datos_header = $this->prep_datos("Armonniza Consulta de Afectación Presupuestaria");

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/ingresos_consulta_view');
        $this->load->view('front/footer_main_view');
    }

    /**
     * Esta funcion se encarga de obtener los datos del segundo nivel de la estructura de egresos
     */
    function obtener_nivel1() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha obtenido los datos del nivel 1');
//        Se llama a la funcion del model de ingresos encargada de tomar los nombres delos niveles que existen
        $nombres_ingresos = $this->ingresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_ingresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

//        Se llamam a la funcion del modelo que se encarga de buscar los programas de financiamiento conforme a la variable recibida
//        Y se guarda los resultados en una variable
        $nuevo_nivel = $this->ingresos_model->datos_nivel1($nombre[0]);

//        Se crea un ciclo para imprimir los datos obtenidos en los elementos de una lista
        foreach($nuevo_nivel as $fila) {
            echo("<option title='".$fila->gerencia." - ".$fila->descripcion."' value='".$fila->gerencia."' >".$fila->gerencia." -  ".$fila->descripcion."</option>");
        }
    }

    /**
     * Esta funcion se encarga de obtener los datos del segundo nivel de la estructura de ingresos
     */
    function obtener_nivel2() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha obtenido los datos del nivel 2');
//        Se llama a la funcion del model de ingresos encargada de tomar los nombres delos niveles que existen
        $nombres_ingresos = $this->ingresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_ingresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

//        Se toma la variable que se manda por POST
        $nivel1 = $this->input->post("nivel1");

//        Se llamam a la funcion del modelo que se encarga de buscar los programas de financiamiento conforme a la variable recibida
//        Y se guarda los resultados en una variable
        $nuevo_nivel = $this->ingresos_model->datos_nivel2($nivel1, $nombre);

//        Se crea un ciclo para imprimir los datos obtenidos en los elementos de una lista
        foreach($nuevo_nivel as $fila) {
            echo("<option title='".$fila->centro_de_recaudacion." - ".$fila->descripcion."' value='".$fila->centro_de_recaudacion."' >".$fila->centro_de_recaudacion." -  ".$fila->descripcion."</option>");
        }
    }

    /**
     * Esta funcion se encarga de obtener los datos del tercer nivel de la estructura de ingresos
     */
    function obtener_nivel3() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha obtenido los datos del nivel 3');
//        Se llama a la funcion del model de ingresos encargada de tomar los nombres delos niveles que existen
        $nombres_ingresos = $this->ingresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_ingresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

//        Se toman los valores del primer y segundo nivel que son enviados por POST
        $nivel1 = $this->input->post("nivel1", TRUE);
        $nivel2 = $this->input->post("nivel2", TRUE);

//        Se llamam a la funcion del modelo que se encarga de buscar el tercer nivel conforme a la estructura de ingresos
//        Y se guarda los resultados en una variable
        $nuevo_nivel = $this->ingresos_model->datos_nivel3($nivel1, $nivel2, $nombre);

//        Se crea un ciclo para imprimir los datos obtenidos en los elementos de una lista
        foreach($nuevo_nivel as $row) {
            echo("<option title='".$row->rubro." - ".$row->descripcion."' value='".$row->rubro."' >".$row->rubro." -  ".$row->descripcion."</option>");
        }
    }

    /**
     * Esta funcion se encarga de obtener los datos del cuarto nivel de la estructura de ingresos
     */
    function obtener_nivel4() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha obtenido los datos del nivel 4');
//        Se llama a la funcion del model de ingresos encargada de tomar los nombres delos niveles que existen
        $nombres_ingresos = $this->ingresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_ingresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

//        Se toman los valores del primer, segundo y tercer nivel que son enviados por POST
        $nivel1 = $this->input->post("nivel1");
        $nivel2 = $this->input->post("nivel2");
        $nivel3 = $this->input->post("nivel3");
//        Se llamam a la funcion del modelo que se encarga de buscar el cuarto nivel conforme a la estructura de ingresos
//        Y se guarda los resultados en una variable
        $nuevo_nivel = $this->ingresos_model->datos_nivel4($nivel1, $nivel2, $nivel3, $nombre);
//        Se crea un ciclo para imprimir los datos obtenidos en los elementos de una lista
        foreach($nuevo_nivel as $row) {
            echo("<option title='".$row->tipo." - ".$row->descripcion."' value='".$row->tipo."' >".$row->tipo." -  ".$row->descripcion."</option>");
        }
    }

    /**
     * Esta funcion se encarga de obtener los datos del quinto nivel de la estructura de ingresos
     */
    function obtener_nivel5() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha obtenido los datos del nivel 5');
//        Se llama a la funcion del model de ingresos encargada de tomar los nombres delos niveles que existen
        $nombres_ingresos = $this->ingresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_ingresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

//        Se toman los valores del primer, segundo, tercer y cuarto nivel que son enviados por POST
        $nivel1 = $this->input->post("nivel1");
        $nivel2 = $this->input->post("nivel2");
        $nivel3 = $this->input->post("nivel3");
        $nivel4 = $this->input->post("nivel4");

//        Se llamam a la funcion del modelo que se encarga de buscar el quinto nivel conforme a la estructura de ingresos
//        Y se guarda los resultados en una variable
        $nuevo_nivel = $this->ingresos_model->datos_nivel5($nivel1, $nivel2, $nivel3, $nivel4, $nombre);
//        Se crea un ciclo para imprimir los datos obtenidos en los elementos de una lista
        foreach($nuevo_nivel as $row) {
            echo("<option title='".$row->clase." - ".$row->descripcion."' value='".$row->clase."' >".$row->clase." -  ".$row->descripcion."</option>");
        }
    }

    /**
     * Esta funcion se encarga de obtener los datos del sexto nivel de la estructura de ingresos
     */
    function obtener_nivel6() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha obtenido los datos del nivel 6');
//        Se llama a la funcion del model de ingresos encargada de tomar los nombres delos niveles que existen
        $nombres_ingresos = $this->ingresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_ingresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

//        Se toman los valores del primer, segundo, tercer, cuarto y quinto nivel que son enviados por POST
        $nivel1 = $this->input->post("nivel1");
        $nivel2 = $this->input->post("nivel2");
        $nivel3 = $this->input->post("nivel3");
        $nivel4 = $this->input->post("nivel4");
        $nivel5 = $this->input->post("nivel5");

//        Se llamam a la funcion del modelo que se encarga de buscar el sexto nivel conforme a la estructura de ingresos
//        Y se guarda los resultados en una variable
        $nuevo_nivel = $this->ingresos_model->datos_nivel6($nivel1, $nivel2, $nivel3, $nivel4, $nivel5, $nombre);

//        Se crea un ciclo para imprimir los datos obtenidos en los elementos de una lista
        foreach($nuevo_nivel as $row) {
            echo("<option title='".$row->partida." - ".$row->descripcion."' value='".$row->partida."' >".$row->partida." -  ".$row->descripcion."</option>");
        }
    }

    /**
     * Esta funcion se encarga de buscar la informacion del ultimo nivel conforme a la estructura de ingresos
     */
    function obtener_datosUltimoNivel() {
//        Se llama a la funcion del model de ingresos encargada de tomar los nombres delos niveles que existen
        $nombres_ingresos = $this->ingresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_ingresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

//        Se toman los valores del primer, segundo, tercer, cuarto, quinto y sexto nivel que son enviados por POST
        $nivel1 = $this->input->post("nivel1");
        $nivel2 = $this->input->post("nivel2");
        $nivel3 = $this->input->post("nivel3");
        $nivel4 = $this->input->post("nivel4");
        $nivel5 = $this->input->post("nivel5");


//        Se llama a la libreria de utilirerias, para preparar el mes actual
        $mes_actual = $this->utilerias->preparar_mes_actual();

//        $this->debugeo->imprimir_pre($mes_actual);

//        $this->debugeo->imprimir_pre($nombre);

//        Se llama a la funcion que se encarga de tomar los datos de la partida para desplegar la informacion necesaria
        $datos_nivel = $this->ingresos_model->datos_de_ultimo_nivel($nivel1, $nivel2, $nivel3, $nivel4, $nivel5, $mes_actual, $nombre);

//        $this->debugeo->imprimir_pre($datos_nivel);

//        Se devuelven los valores en formato JSON para poder ingresarlos a la grafica de balance de dinero por mes
        foreach($datos_nivel as $row) {
            echo(json_encode($row));
        }

    }

    /**
     * Esta funcion sirve para transformar el archivo .CSV que sube el usuario, para poder insertarlos a la base de datos en el formato adecuado
     * NOTA: Revisar porque cuando el archivo es muy grande, no se insertan todas las filas
     */
    function subir_niveles() {

//        Se prepara la configuracion de la libreria "Upload"
        $config['upload_path'] = './application/archivos/';
        $config['allowed_types'] = '*';

//        Se carga la libreria con sus configuraciones correspondientes
        $this->load->library('upload', $config);

//        Si el server no logra subir el archivo, despliega un mensaje de error
        if ( ! $this->upload->do_upload('archivoSubir')) {
//            Se capturan los errores en una variable y se imprimen para debug
            $error = array('error' => $this->upload->display_errors());
            $this->debugeo->imprimir_pre($error);
        }
        else {
            ini_set('memory_limit', '-1');
//            load our new PHPExcel library
            $this->load->library('excel');
//            Si el server logra subir el archivo, se toman los datos del archivo
            $data = $this->upload->data();

//            Se toma la ruta del archivo junto con su nombre y extension
            $file = $data["full_path"];

//            read file from path
            $objPHPExcel = PHPExcel_IOFactory::load($file);

//            get only the Cell Collection
            $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();

//            extract to a PHP readable array format
            foreach ($cell_collection as $cell) {
                $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
                $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
                $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();

//                header will/should be in row 1 only. of course this can be modified to suit your need.
                if ($row == 1) {
                    continue;
                } else {
                    $arr_data[$row][$column] = $data_value;
                }
            }

//            Se llama a la funcion del model de ingresos encargada de tomar los nombres delos niveles que existen
            $nombres_ingresos = $this->ingresos_model->obtener_nombre_niveles();

//            En este arreglo se van a guardar los nombres de los niveles
            $nombre = array();

//            En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
            foreach($nombres_ingresos as $fila) {
                array_push($nombre, $fila->descripcion);
            }

            $conteo = 0;

            foreach($arr_data as $datos) {
                $resultado = $this->ingresos_model->insertar_niveles_ingresos($datos, $nombre);
                $conteo += 1;
            }

//            Si hay un resultado positivo se redirecciona a la pagina de la estructura de ingresos
            if($resultado) {
                redirect(base_url("ingresos/estructura_rubros"));
            }
//            De lo contrario se imprime un error
            else {
                echo("Hubo un error al insertar los datos.");
            }
        }

    }

    /**
     * Esta funcion es para hacer pruebas con los datos NoSQL
     */
    function checar_datos() {
//        $sql = "SELECT COLUMN_JSON(nivel) FROM cat_niveles;";
        $sql = "SELECT COUNT(id_niveles) AS conteo, COLUMN_JSON(nivel) FROM cat_niveles
                WHERE COLUMN_GET(nivel, 'centro_de_recaudacion' as char) = 101
                AND COLUMN_GET(nivel, 'rubro' as char) = 7
                AND COLUMN_GET(nivel, 'tipo' as char) = 72
                AND COLUMN_GET(nivel, 'clase' as char) = 72001;";
//        $sql = "SELECT COLUMN_JSON(nivel) FROM cat_niveles
//                WHERE COLUMN_GET(nivel, 'centro_de_recaudacion' as char) = 108;";
//        $mes = $this->utilerias->preparar_mes_actual();
//        $sql = "SELECT COLUMN_GET(nivel, '".$mes."_inicial' as double) as saldo_mes_inicial,
//        COLUMN_GET(nivel, '".$mes."_precompromiso' as double) as saldo_mes_precompromiso,
//        COLUMN_GET(nivel, '".$mes."_compromiso' as double) as saldo_mes_compromiso,
//        COLUMN_GET(nivel, 'saldo' as double) as saldo
//        FROM cat_niveles WHERE COLUMN_GET(nivel, 'centro_de_recaudacion' as char) = '1' AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) = 'E016' AND COLUMN_GET(nivel, 'centro_de_costos' as char) = '100' AND COLUMN_GET(nivel, 'capitulo' as char) = '1000' AND COLUMN_GET(nivel, 'concepto' as char) = '113' AND COLUMN_GET(nivel, 'partida' as char) = '11301'";
        $query = $this->db->query($sql);
        $resultado = $query->result();
        foreach($resultado as $fila) {
            $this->debugeo->imprimir_pre($fila);
//            $myArray = json_decode($fila->datos, true);
//            $this->debugeo->imprimir_pre($myArray);
        }
//        $this->debugeo->imprimir_pre($this->utilerias->preparar_mes_actual());
//        $this->debugeo->imprimir_pre($resultado);
    }

    function buscar_nivel1() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha obtenido los datos del nivel 1');
//        Se llama a la funcion del model de ingresos encargada de tomar los nombres delos niveles que existen
        $nombres_ingresos = $this->ingresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_ingresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

//        Se toma la variable que se manda por POST
        $nivel_buscar1 = $this->input->post("nivel_buscar1", TRUE);

//        Se llamam a la funcion del modelo que se encarga de buscar los programas de financiamiento conforme a la variable recibida
//        Y se guarda los resultados en una variable
        $resultados = $this->ingresos_model->buscar_nivel1($nivel_buscar1, $nombre);

//        Se crea un ciclo para imprimir los datos obtenidos en los elementos de una lista
        foreach($resultados as $fila) {
            echo("<option title='".$fila->centro_de_recaudacion." - ".$fila->descripcion."' value='".$fila->centro_de_recaudacion."' >".$fila->centro_de_recaudacion." -  ".$fila->descripcion."</option>");
        }
    }

    function buscar_nivel2() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha obtenido los datos del nivel 2');
//        Se llama a la funcion del model de ingresos encargada de tomar los nombres delos niveles que existen
        $nombres_ingresos = $this->ingresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_ingresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

//        Se toman los valores del primer y segundo nivel que son enviados por POST
        $nivel_buscar1 = $this->input->post("nivel_buscar1", TRUE);
        $nivel_buscar2 = $this->input->post("nivel_buscar2", TRUE);

//        Se llamam a la funcion del modelo que se encarga de buscar el tercer nivel conforme a la estructura de ingresos
//        Y se guarda los resultados en una variable
        $resultados = $this->ingresos_model->buscar_nivel2($nivel_buscar1, $nivel_buscar2, $nombre);

//        Se crea un ciclo para imprimir los datos obtenidos en los elementos de una lista
        foreach($resultados as $row) {
            echo("<option title='".$row->rubro." - ".$row->descripcion."' value='".$row->rubro."' >".$row->rubro." -  ".$row->descripcion."</option>");
        }
    }

    function buscar_nivel3() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha obtenido los datos del nivel 3');
//        Se llama a la funcion del model de ingresos encargada de tomar los nombres delos niveles que existen
        $nombres_ingresos = $this->ingresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_ingresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

//        Se toman los valores del primer, segundo y tercer nivel que son enviados por POST
        $nivel_buscar1 = $this->input->post("nivel_buscar1", TRUE);
        $nivel_buscar2 = $this->input->post("nivel_buscar2", TRUE);
        $nivel_buscar3 = $this->input->post("nivel_buscar3", TRUE);

//        Se llamam a la funcion del modelo que se encarga de buscar el cuarto nivel conforme a la estructura de ingresos
//        Y se guarda los resultados en una variable
        $resultados = $this->ingresos_model->buscar_nivel3($nivel_buscar1, $nivel_buscar2, $nivel_buscar3, $nombre);

//        Se crea un ciclo para imprimir los datos obtenidos en los elementos de una lista
        foreach($resultados as $row) {
            echo("<option title='".$row->tipo." - ".$row->descripcion."' value='".$row->tipo."' >".$row->tipo." -  ".$row->descripcion."</option>");
        }
    }

    function buscar_nivel4() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha obtenido los datos del nivel 4');
//        Se llama a la funcion del model de ingresos encargada de tomar los nombres delos niveles que existen
        $nombres_ingresos = $this->ingresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_ingresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

//        Se toman los valores del primer, segundo y tercer nivel que son enviados por POST
        $nivel_buscar1 = $this->input->post("nivel_buscar1", TRUE);
        $nivel_buscar2 = $this->input->post("nivel_buscar2", TRUE);
        $nivel_buscar3 = $this->input->post("nivel_buscar3", TRUE);
        $nivel_buscar4 = $this->input->post("nivel_buscar4", TRUE);

//        Se llamam a la funcion del modelo que se encarga de buscar el cuarto nivel conforme a la estructura de ingresos
//        Y se guarda los resultados en una variable
        $resultados = $this->ingresos_model->buscar_nivel4($nivel_buscar1, $nivel_buscar2, $nivel_buscar3, $nivel_buscar4, $nombre);

//        Se crea un ciclo para imprimir los datos obtenidos en los elementos de una lista
        foreach($resultados as $row) {
            echo("<option title='".$row->clase." - ".$row->descripcion."' value='".$row->clase."' >".$row->clase." -  ".$row->descripcion."</option>");
        }
    }

    function buscar_nivel5() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha obtenido los datos del nivel 5');
//        Se llama a la funcion del model de ingresos encargada de tomar los nombres delos niveles que existen
        $nombres_ingresos = $this->ingresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_ingresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

//        Se toman los valores del primer, segundo y tercer nivel que son enviados por POST
        $nivel_buscar1 = $this->input->post("nivel_buscar1", TRUE);
        $nivel_buscar2 = $this->input->post("nivel_buscar2", TRUE);
        $nivel_buscar3 = $this->input->post("nivel_buscar3", TRUE);
        $nivel_buscar4 = $this->input->post("nivel_buscar4", TRUE);
        $nivel_buscar5 = $this->input->post("nivel_buscar5", TRUE);

//        Se llamam a la funcion del modelo que se encarga de buscar el cuarto nivel conforme a la estructura de ingresos
//        Y se guarda los resultados en una variable
        $resultados = $this->ingresos_model->buscar_nivel5($nivel_buscar1, $nivel_buscar2, $nivel_buscar3, $nivel_buscar4, $nivel_buscar5, $nombre);

//        Se crea un ciclo para imprimir los datos obtenidos en los elementos de una lista
        foreach($resultados as $row) {
            echo("<option title='".$row->concepto." - ".$row->descripcion."' value='".$row->concepto."' >".$row->concepto." -  ".$row->descripcion."</option>");
        }
    }

    function buscar_nivel6() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha obtenido los datos del nivel 6');
//        Se llama a la funcion del model de ingresos encargada de tomar los nombres delos niveles que existen
        $nombres_ingresos = $this->ingresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_ingresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

//        Se toman los valores del primer, segundo y tercer nivel que son enviados por POST
        $nivel_buscar1 = $this->input->post("nivel_buscar1", TRUE);
        $nivel_buscar2 = $this->input->post("nivel_buscar2", TRUE);
        $nivel_buscar3 = $this->input->post("nivel_buscar3", TRUE);
        $nivel_buscar4 = $this->input->post("nivel_buscar4", TRUE);
        $nivel_buscar5 = $this->input->post("nivel_buscar5", TRUE);
        $nivel_buscar6 = $this->input->post("nivel_buscar6", TRUE);

//        Se llamam a la funcion del modelo que se encarga de buscar el cuarto nivel conforme a la estructura de ingresos
//        Y se guarda los resultados en una variable
        $resultados = $this->ingresos_model->buscar_nivel6($nivel_buscar1, $nivel_buscar2, $nivel_buscar3, $nivel_buscar4, $nivel_buscar5, $nivel_buscar6, $nombre);

//        Se crea un ciclo para imprimir los datos obtenidos en los elementos de una lista
        foreach($resultados as $row) {
            echo("<option title='".$row->partida." - ".$row->descripcion."' value='".$row->partida."' >".$row->partida." -  ".$row->descripcion."</option>");
        }
    }

    private function preparar_estructura_superior() {
//        Se llama la funcion del modelo de ingresos encargado de contar los niveles que existen
        $total_ingresos = $this->ingresos_model->contar_ingresos_elementos();
//        Se llama a la funcion del model de ingresos encargada de tomar los nombres delos niveles que existen
        $nombres_ingresos = $this->ingresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_ingresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

        $nombre_primerNivel = $this->ingresos_model->datos_primerNivel($nombre[0]);

//        $this->debugeo->imprimir_pre($nombre_primerNivel);

        $niveles = '<div class="row" id="ingresos_estrucura_superior">
                        <div class="col-lg-4">
                            <form role="form">
                                <div class="form-group" id="forma_nivel1_superior">
                                    <h5>Nivel 1</h5>
                                    <label>'.$nombre[0].'</label>
                                    <input type="text" class="form-control" id="buscar_nivel1_superior" placeholder="Buscar...">
                                    <select multiple class="form-control formas">';
        foreach($nombre_primerNivel as $fila) {
            $niveles .= '<option value="'.$fila->gerencia.'" title="'.$fila->gerencia.' - '.$fila->descripcion.'">'.$fila->gerencia.' - '.$fila->descripcion.'</option>';
        }

        $niveles .= '                </select>
                                </div>
                            </form>
                        </div>';

        $a = 1;
        $num = 2;

        for($i = 1; $i < $total_ingresos->conteo; $i++){
            $niveles .= '<div class="col-lg-4">
                            <form role="form">
                                <div class="form-group" id="forma_nivel'.$num.'_superior">
                                     <h5>Nivel '.$num.'</h5>
                                    <label>'.$nombre[$a].'</label>
                                    <input type="text" class="form-control" id="buscar_nivel'.$num.'_superior" placeholder="Buscar..." disabled>
                                    <select multiple class="form-control formas" id="select_nivel'.$num.'_superior">
                                    </select>
                                </div>
                            </form>
                        </div>';
            $a++;
            $num++;
        }

        $niveles .= '</div>';

        return $niveles;
    }

    private function preparar_estructura_inferior() {
//        Se llama la funcion del modelo de ingresos encargado de contar los niveles que existen
        $total_ingresos = $this->ingresos_model->contar_ingresos_elementos();
//        Se llama a la funcion del model de ingresos encargada de tomar los nombres delos niveles que existen
        $nombres_ingresos = $this->ingresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_ingresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

        $nombre_primerNivel = $this->ingresos_model->datos_primerNivel($nombre[0]);

//        $this->debugeo->imprimir_pre($nombre_primerNivel);

        $niveles = '<div class="row" id="ingresos_estrucura_inferior">
                        <div class="col-lg-4">
                            <form role="form">
                                <div class="form-group" id="forma_nivel1_inferior">
                                    <h5>Nivel 1</h5>
                                    <label>'.$nombre[0].'</label>
                                    <input type="text" class="form-control" id="buscar_nivel1_inferior" placeholder="Buscar...">
                                    <select multiple class="form-control formas">';
        foreach($nombre_primerNivel as $fila) {
            $niveles .= '<option value="'.$fila->gerencia.'" title="'.$fila->gerencia.' - '.$fila->descripcion.'">'.$fila->gerencia.' - '.$fila->descripcion.'</option>';
        }

        $niveles .= '                </select>
                                </div>
                            </form>
                        </div>';

        $a = 1;
        $num = 2;

        for($i = 1; $i < $total_ingresos->conteo; $i++){
            $niveles .= '<div class="col-lg-4">
                            <form role="form">
                                <div class="form-group" id="forma_nivel'.$num.'_inferior">
                                     <h5>Nivel '.$num.'</h5>
                                    <label>'.$nombre[$a].'</label>
                                    <input type="text" class="form-control" id="buscar_nivel'.$num.'_inferior" placeholder="Buscar..." disabled>
                                    <select multiple class="form-control formas" id="select_nivel'.$num.'_inferior">
                                    </select>
                                </div>
                            </form>
                        </div>';
            $a++;
            $num++;
        }

        $niveles .= '</div>';

        return $niveles;
    }

    private function input_nivelsuperior() {
//        Se llama la funcion del modelo de ingresos encargado de contar los niveles que existen
        $total_ingresos = $this->ingresos_model->contar_ingresos_elementos();
//        Se llama a la funcion del model de ingresos encargada de tomar los nombres delos niveles que existen
        $nombres_ingresos = $this->ingresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_ingresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

        $niveles = '';
        $num = 1;

        for($i = 0; $i < $total_ingresos->conteo; $i++){
            $nom_niv = ucwords(strtolower($nombre[$i]));
            $nom_niv1 = str_replace( "De" , "" , $nom_niv);
            $nom_niv2 = str_replace( "Recaudacion" , "Recaudación" , $nom_niv1);
            $nom_niv_f = str_replace( "Centro" , "C." , $nom_niv2);
            $niveles .= '
                                <!-- <h5>Nivel '.$num.'</h5>
                                <label>'.$nombre[$i].'</label>-->
                                <input type="text" class="form-control" id="input_nivel'.$num.'_superior" placeholder="'.$nom_niv_f.'" >
                        ';
            $num++;
        }

        return $niveles;
    }

    private function input_nivelinferior() {
//        Se llama la funcion del modelo de ingresos encargado de contar los niveles que existen
        $total_ingresos = $this->ingresos_model->contar_ingresos_elementos();
//        Se llama a la funcion del model de ingresos encargada de tomar los nombres delos niveles que existen
        $nombres_ingresos = $this->ingresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_ingresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

        $niveles = '';
        $num = 1;

        for($i = 0; $i < $total_ingresos->conteo; $i++){
            $nom_niv = ucwords(strtolower($nombre[$i]));
            $nom_niv1 = str_replace( "De" , "" , $nom_niv);
            $nom_niv2 = str_replace( "Recaudacion" , "Recaudación" , $nom_niv1);
            $nom_niv_f = str_replace( "Centro" , "C." , $nom_niv2);
            $niveles .= '
                                <!-- <h5>Nivel '.$num.'</h5>
                                <label>'.$nombre[$i].'</label>-->
                                <input type="text" class="form-control" id="input_nivel'.$num.'_inferior" placeholder="'.$nom_niv_f.'" >
                        ';
            $num++;
        }

        return $niveles;
    }

    private function revisar_estructura_existe($datos) {

        $fecha = ($datos['I'] - 25569) * 86400;
        $fecha = date('Y-m-d', $fecha);

        $datos["fecha_aplicar_detalle"] = $fecha;

        $datos["mes_destino"] = strtolower($datos["F"]);

        $datos["cantidad"] = $datos["H"];

        $datos["tipo"] = "Ampliación";
        $datos["texto"] = "Ampliación";
        $datos["enlace"] = 0;

        $caracteres = array("$", ",");

        foreach($datos as $key=>$value){
            $datos[$key]=str_replace($caracteres,"",$value);
        }

//        Se llama a la funcion del model de egresos encargada de tomar los nombres delos niveles que existen
        $nombres_ingresos = $this->ingresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_ingresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

//       Se toman los datos de la estructura inferior
        $datos_partida_inferior = $this->ingresos_model->datos_de_partida(array(
            "nivel1" => $datos["A"],
            "nivel2" => $datos["B"],
            "nivel3" => $datos["C"],
            "nivel4" => $datos["D"],
            "nivel5" => $datos["E"],),
            $nombre);

        if($datos_partida_inferior) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function agregar_nivel() {
        $datos = $this->input->post();

        foreach ($datos as $key => $value) {
            $datos[$key] = trim($value);
        }

        try {

            $total_ingresos = $this->ingresos_model->contar_ingresos_elementos();
//        Se llama a la funcion del model de ingresos encargada de tomar los nombres delos niveles que existen
            $nombres_ingresos = $this->ingresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
            $nombres = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
            foreach($nombres_ingresos as $fila) {
                array_push($nombres, $fila->descripcion);
            }

            $sql = "";

            if(isset($datos["nivel2"]) && $datos["nivel2"] == "") {

                $sql = "INSERT INTO cat_niveles (id_niveles,  nivel) VALUES (NULL, COLUMN_CREATE('" . strtolower(str_replace(' ', '_', $nombres[0])) . "','" . $datos["clave"] . "',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[1])) . "', '',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[2])) . "', '',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[3])) . "', '',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[4])) . "', '',
                                                                    'descripcion', '" . $datos["descripcion"] . "'));";

            }
            elseif(isset($datos["nivel3"]) && $datos["nivel3"] == "") {

                $sql = "INSERT INTO cat_niveles (id_niveles,  nivel) VALUES (NULL, COLUMN_CREATE('" . strtolower(str_replace(' ', '_', $nombres[0])) . "','" . $datos["nivel2"] . "',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[1])) . "', '" . $datos["clave"] . "',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[2])) . "', '',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[3])) . "', '',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[4])) . "', '',
                                                                    'descripcion', '" . $datos["descripcion"] . "'));";

            }
            elseif(isset($datos["nivel4"]) && $datos["nivel4"] == "") {

                $sql = "INSERT INTO cat_niveles (id_niveles,  nivel) VALUES (NULL, COLUMN_CREATE('" . strtolower(str_replace(' ', '_', $nombres[0])) . "','" . $datos["nivel2"] . "',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[1])) . "', '" . $datos["nivel3"] . "',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[2])) . "', '" . $datos["clave"] . "',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[3])) . "', '',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[4])) . "', '',
                                                                    'descripcion', '" . $datos["descripcion"] . "'));";

            }
            elseif(isset($datos["nivel5"]) && $datos["nivel5"] == "") {

                $sql = "INSERT INTO cat_niveles (id_niveles,  nivel) VALUES (NULL, COLUMN_CREATE('" . strtolower(str_replace(' ', '_', $nombres[0])) . "','" . $datos["nivel2"] . "',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[1])) . "', '" . $datos["nivel3"] . "',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[2])) . "', '" . $datos["nivel4"] . "',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[3])) . "', '" . $datos["clave"] . "',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[4])) . "', '',
                                                                    'descripcion', '" . $datos["descripcion"] . "'));";

            }
            else {

                $sql_fuente = "SELECT descripcion FROM cat_clasificador_fuentes_financia WHERE codigo = ?;";
                $query = $this->db->query($sql_fuente, array($datos["nivel2"]));
                $resultado = $query->row_array();

                $sql = "INSERT INTO cat_niveles (id_niveles,  nivel) VALUES (NULL, COLUMN_CREATE('" . strtolower(str_replace(' ', '_', $nombres[0])) . "','" . $datos["nivel2"] . "',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[1])) . "', '" . $datos["nivel3"] . "',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[2])) . "', '" . $datos["nivel4"] . "',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[3])) . "', '" . $datos["nivel5"] . "',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[4])) . "', '" . $datos["clave"] . "',
                                                                    'descripcion', '" . $datos['descripcion'] . "',
                                                                    'enero_inicial', 0.0,
                                                                    'febrero_inicial', 0.0,
                                                                    'marzo_inicial', 0.0,
                                                                    'abril_inicial', 0.0,
                                                                    'mayo_inicial', 0.0,
                                                                    'junio_inicial', 0.0,
                                                                    'julio_inicial', 0.0,
                                                                    'agosto_inicial', 0.0,
                                                                    'septiembre_inicial', 0.0,
                                                                    'octubre_inicial', 0.0,
                                                                    'noviembre_inicial', 0.0,
                                                                    'diciembre_inicial', 0.0,
                                                                    'enero_devengado', 0.0,
                                                                    'febrero_devengado', 0.0,
                                                                    'marzo_devengado', 0.0,
                                                                    'abril_devengado', 0.0,
                                                                    'mayo_devengado', 0.0,
                                                                    'junio_devengado', 0.0,
                                                                    'julio_devengado', 0.0,
                                                                    'agosto_devengado', 0.0,
                                                                    'septiembre_devengado', 0.0,
                                                                    'octubre_devengado', 0.0,
                                                                    'noviembre_devengado', 0.0,
                                                                    'diciembre_devengado', 0.0,
                                                                    'enero_recaudado', 0.0,
                                                                    'febrero_recaudado', 0.0,
                                                                    'marzo_recaudado', 0.0,
                                                                    'abril_recaudado', 0.0,
                                                                    'mayo_recaudado', 0.0,
                                                                    'junio_recaudado', 0.0,
                                                                    'julio_recaudado', 0.0,
                                                                    'agosto_recaudado', 0.0,
                                                                    'septiembre_recaudado', 0.0,
                                                                    'octubre_recaudado', 0.0,
                                                                    'noviembre_recaudado', 0.0,
                                                                    'diciembre_recaudado', 0.0,
                                                                    'enero_saldo', 0.0,
                                                                    'febrero_saldo', 0.0,
                                                                    'marzo_saldo', 0.0,
                                                                    'abril_saldo', 0.0,
                                                                    'mayo_saldo', 0.0,
                                                                    'junio_saldo', 0.0,
                                                                    'julio_saldo', 0.0,
                                                                    'agosto_saldo', 0.0,
                                                                    'septiembre_saldo', 0.0,
                                                                    'octubre_saldo', 0.0,
                                                                    'noviembre_saldo', 0.0,
                                                                    'diciembre_saldo', 0.0,
                                                                    'total_anual', 0.0,
                                                                    'devengado_anual', 0.0,
                                                                    'recaudado_anual', 0.0,
                                                                    'clave_financiamiento', 'Ingresos Propios'));";
            }

            $insertar_nivel = $this->ingresos_model->insertar_nivel($sql);

            if($insertar_nivel) {
                $respuesta = array(
                    "mensaje" => '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Se ha insertado el nuevo nivel con éxito.</div>',
                );
            } else {
                throw new Exception('Ha ocurrido un error al tratar de insertar el nivel, por favor contacte a su administrador.');
            }

            echo(json_encode($respuesta));

        } catch (Exception $e) {

            $respuesta = array(
                "mensaje" => '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$e->getMessage().'</div>',
            );

            echo(json_encode($respuesta));
        }
    }

    function editar_nivel() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha editado un nivel ');
        $datos = $this->input->post();

        foreach ($datos as $key => $value) {
            $datos[$key] = trim($value);
        }

        try {

            $total_egresos = $this->ingresos_model->contar_egresos_elementos();
//        Se llama a la funcion del model de egresos encargada de tomar los nombres delos niveles que existen
            $nombres_egresos = $this->ingresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
            $nombres = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
            foreach($nombres_egresos as $fila) {
                array_push($nombres, $fila->descripcion);
            }

            $sql = "";

            if(isset($datos["nivel3"]) && $datos["nivel3"] == "") {

                $sql = "UPDATE cat_niveles SET nivel = COLUMN_ADD(nivel, '" . strtolower(str_replace(' ', '_', $nombres[0])) . "','" . $datos["clave"] . "',
                                                                    'descripcion', '" . $datos["descripcion"] . "')
                                                                    WHERE COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[0])) . "' as char) = '" . $datos["clave_anterior"] . "'
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[1])) . "' as char) = ''
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[2])) . "' as char) = ''
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[3])) . "' as char) = ''
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[4])) . "' as char) = ''
                                                                    AND COLUMN_GET(nivel, 'descripcion' as char) = '". $datos["descripcion_anterior"] ."' ;";

                $sql .= "UPDATE cat_niveles SET nivel = COLUMN_ADD(nivel, '" . strtolower(str_replace(' ', '_', $nombres[0])) . "','" . $datos["clave"] . "')
                                                                    WHERE COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[0])) . "' as char) = '" . $datos["clave_anterior"] . "' ;";

            }
            elseif(isset($datos["nivel4"]) && $datos["nivel4"] == "") {

                $sql = "UPDATE cat_niveles SET nivel = COLUMN_ADD(nivel, '" . strtolower(str_replace(' ', '_', $nombres[1])) . "','" . $datos["clave"] . "',
                                                                    'descripcion', '" . $datos["descripcion"] . "')
                                                                    WHERE COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[0])) . "' as char) = " . $datos["nivel2"] . "'
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[1])) . "' as char) = '" . $datos["nivel3"] . "'
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[2])) . "' as char) = ''
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[3])) . "' as char) = ''
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[4])) . "' as char) = ''
                                                                    AND COLUMN_GET(nivel, 'descripcion' as char) = '". $datos["descripcion_anterior"] ."' ;";

            }
            elseif(isset($datos["nivel5"]) && $datos["nivel5"] == "") {

                $sql = "UPDATE cat_niveles SET nivel = COLUMN_ADD(nivel, '" . strtolower(str_replace(' ', '_', $nombres[2])) . "','" . $datos["clave"] . "',
                                                                    'descripcion', '" . $datos["descripcion"] . "')
                                                                    WHERE COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[0])) . "' as char) = '" . $datos["nivel2"] . "'
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[1])) . "' as char) = '" . $datos["nivel3"] . "'
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[2])) . "' as char) = '" . $datos["nivel4"] . "'
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[3])) . "' as char) = ''
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[4])) . "' as char) = ''
                                                                    AND COLUMN_GET(nivel, 'descripcion' as char) = '". $datos["descripcion_anterior"] ."' ;";

            }
            elseif(isset($datos["nivel6"]) && $datos["nivel6"] == "") {

                $sql = "UPDATE cat_niveles SET nivel = COLUMN_ADD(nivel, '" . strtolower(str_replace(' ', '_', $nombres[3])) . "','" . $datos["clave"] . "',
                                                                    'descripcion', '" . $datos["descripcion"] . "')
                                                                    WHERE COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[0])) . "' as char) = '" . $datos["nivel2"] . "'
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[1])) . "' as char) = '" . $datos["nivel3"] . "'
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[2])) . "' as char) = '" . $datos["nivel4"] . "'
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[3])) . "' as char) = '" . $datos["nivel5"] . "'
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[4])) . "' as char) = ''
                                                                    AND COLUMN_GET(nivel, 'descripcion' as char) = '". $datos["descripcion_anterior"] ."' ;";

            }
            elseif(isset($datos["nivel7"]) && $datos["nivel7"] == "") {

                $sql = "UPDATE cat_niveles SET nivel = COLUMN_ADD(nivel, '" . strtolower(str_replace(' ', '_', $nombres[4])) . "','" . $datos["clave"] . "',
                                                                    'descripcion', '" . $datos["descripcion"] . "')
                                                                    WHERE COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[0])) . "' as char) = '" . $datos["nivel2"] . "'
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[1])) . "' as char) = '" . $datos["nivel3"] . "'
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[2])) . "' as char) = '" . $datos["nivel4"] . "'
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[3])) . "' as char) = '" . $datos["nivel5"] . "'
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[4])) . "' as char) = '" . $datos["nivel6"] . "'
                                                                    AND COLUMN_GET(nivel, 'descripcion' as char) = '". $datos["descripcion_anterior"] ."' ;";

            }
            else {

                $sql = "UPDATE cat_niveles SET nivel = COLUMN_ADD(nivel, '" . strtolower(str_replace(' ', '_', $nombres[5])) . "','" . $datos["clave"] . "',
                                                                    'descripcion', '" . $datos["descripcion"] . "')
                                                                    WHERE COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[0])) . "' as char) = '" . $datos["nivel2"] . "'
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[1])) . "' as char) = '" . $datos["nivel3"] . "'
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[2])) . "' as char) = '" . $datos["nivel4"] . "'
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[3])) . "' as char) = '" . $datos["nivel5"] . "'
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[4])) . "' as char) = '" . $datos["nivel6"] . "'
                                                                    AND COLUMN_GET(nivel, 'descripcion' as char) = '". $datos["descripcion_anterior"] ."' ;";
            }

//            $this->debugeo->imprimir_pre($sql);

            $insertar_nivel = $this->ingresos_model->editar_nivel($sql);

            if($insertar_nivel) {
                $respuesta = array(
                    "mensaje" => '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Se ha editado el nivel con éxito.</div>',
                );
            } else {
                throw new Exception('Ha ocurrido un error al tratar de editar el nivel, por favor contacte a su administrador.');
            }

            echo(json_encode($respuesta));

        } catch (Exception $e) {

            $respuesta = array(
                "mensaje" => '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$e->getMessage().'</div>',
            );

            echo(json_encode($respuesta));
        }

    }

}