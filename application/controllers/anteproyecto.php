<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require( './assets/datatables/scripts/ssp.class.php' );

class Anteproyecto extends CI_Controller
{

    /**
     * Se revisa si el usuario esta logueado, si no esta logueado, se reenvia a la pantalla de login
     */
    function __construct()
    {
        parent::__construct();
        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        }
    }

    /**
     * Esta funcion es la principal, donde se muestra la pantalla del Anteproyecto
     */
    function index()
    {
        if ($this->utilerias->get_permisos("anteproyecto") || $this->utilerias->get_grupo() == 1) {
            $datos_header = array(
                "titulo_pagina" => "Armonniza | Anteproyecto",
                "precompromisocss" => $this->tank_auth->get_username(),
                "nuevas_tablas" => TRUE,
            );
            $this->parser->parse('front/header_main_view', $datos_header);
            $this->load->view('front/anteproyecto/main_admin_view');
            $this->load->view('front/footer_main_view');
        } else {
            redirect('/auth/login');
        }

    }

    function catalogos_anteproyecto()
    {
        if ($this->utilerias->get_permisos("catalogos_anteproyecto") || $this->utilerias->get_grupo() == 1) {
            $datos_header = array(
                "titulo_pagina" => "Armonniza | Catalogos",
                "usuario" => $this->tank_auth->get_username(),
                "tablas" => TRUE,
            );

            $this->parser->parse('front/header_main_view', $datos_header);
            $this->load->view('front/anteproyecto/catalogos_anteproyecto_view');
            $this->load->view('front/footer_main_view', array("tablas" => TRUE, "catalogos_anteproyecto" => TRUE));
        } else {
            redirect('/auth/login');
        }

    }

    function catalogo_partidas() {

//        $this->debugeo->imprimir_pre($this->session->userdata('user_id'));

        $datos_header = array(
            "titulo_pagina" => "Armonniza | Cat&aacute;logo Partidas",
            "usuario" => $this->tank_auth->get_username(),
            "nuevas_tablas" => TRUE,
            "nuevas_tablas_editor" => TRUE,
            "nuevas_tablas_buttons" => TRUE,
            "nuevas_tablas_select" => TRUE,
            "catalogo_partidas" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/anteproyecto/catalogo_partidas_view');
        $this->load->view('front/footer_main_view');
    }

    function tabla_catalogo_partidas() {
        include("./assets/nuevo_datatables/extensions/Editor/php/DataTables.php");

        DataTables\Editor::inst($db, 'partidas_anteproyecto')
            ->pkey('id_partida')
            ->fields(
                DataTables\Editor\Field::inst('partida')->validator('DataTables\Editor\Validate::notEmpty'),
                DataTables\Editor\Field::inst('descripcion')->validator('DataTables\Editor\Validate::notEmpty')
            )
//            ->where( "id_estructura", 1, '='  )
            ->process($_POST)
            ->json();

//        $this->debugeo->imprimir_pre($resultado);

    }

    function catalogo_usuarios()
    {

        $datos_header = array(
            "titulo_pagina" => "Armonniza | Cat&aacute;logo Centro de Costos",
            "usuario" => $this->tank_auth->get_username(),
            "nuevas_tablas" => TRUE,
            "nuevas_tablas_editor" => TRUE,
            "nuevas_tablas_buttons" => TRUE,
            "nuevas_tablas_select" => TRUE,
            "catalogo_usuarios" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/anteproyecto/catalogo_usuarios_view');
        $this->load->view('front/footer_main_view');
    }

    function tabla_catalogo_usuarios()
    {
        include("./assets/nuevo_datatables/extensions/Editor/php/DataTables.php");

        DataTables\Editor::inst($db, 'centro_costos_anteproyecto')
            ->pkey('id_centro_costos')
            ->fields(
                DataTables\Editor\Field::inst('centro_costos')->validator('DataTables\Editor\Validate::notEmpty'),
                DataTables\Editor\Field::inst('descripcion')->validator('DataTables\Editor\Validate::notEmpty')
            )
            ->process($_POST)
            ->json();

//        $this->debugeo->imprimir_pre($resultado);

    }

    function select_usuarios()
    {
        $this->db->select('id')->from('users');
        $query = $this->db->get();
        echo(json_encode($query->result_array()));
    }

    function select_partidas()
    {
        $this->db->select('partida')->from('anteproyecto_estructura');
        $query = $this->db->get();
        echo(json_encode($query->result_array()));
    }

    function presupuesto_aprobado()
    {
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Presupuesto Aprobado",
            "usuario" => $this->tank_auth->get_username(),
            "nuevas_tablas" => TRUE,
            "nuevas_tablas_editor" => TRUE,
            "nuevas_tablas_buttons" => TRUE,
            "nuevas_tablas_select" => TRUE,
            "nuevas_tablas_keytable" => TRUE,
            "presupuesto_aprobado" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/anteproyecto/aprobar_presupuesto_view');
        $this->load->view('front/footer_main_view');
    }

    function tabla_presupuesto_aprobado() {
        include("./assets/nuevo_datatables/extensions/Editor/php/DataTables.php");

        DataTables\Editor::inst($db, 'meses_aprobado')
            ->pkey('id_aprobado')
            ->fields(
                DataTables\Editor\Field::inst('centro_costos')->validator('DataTables\Editor\Validate::notEmpty'),
                DataTables\Editor\Field::inst('descripcion_centro_costos')->validator('DataTables\Editor\Validate::notEmpty'),
                DataTables\Editor\Field::inst('partida')->validator('DataTables\Editor\Validate::notEmpty'),
                DataTables\Editor\Field::inst('descripcion_partida')->validator('DataTables\Editor\Validate::notEmpty'),

                DataTables\Editor\Field::inst('enero_anio')->validator('DataTables\Editor\Validate::numeric'),
                DataTables\Editor\Field::inst('enero_fondos')->validator('DataTables\Editor\Validate::numeric'),
                DataTables\Editor\Field::inst('enero_solicitado')->validator('DataTables\Editor\Validate::numeric'),
                DataTables\Editor\Field::inst('enero_aprobado')->validator('DataTables\Editor\Validate::numeric'),

                DataTables\Editor\Field::inst('febrero_anio')->validator('DataTables\Editor\Validate::numeric'),
                DataTables\Editor\Field::inst('febrero_fondos')->validator('DataTables\Editor\Validate::numeric'),
                DataTables\Editor\Field::inst('febrero_solicitado')->validator('DataTables\Editor\Validate::numeric'),
                DataTables\Editor\Field::inst('febrero_aprobado')->validator('DataTables\Editor\Validate::numeric'),

                DataTables\Editor\Field::inst('marzo_anio')->validator('DataTables\Editor\Validate::numeric'),
                DataTables\Editor\Field::inst('marzo_fondos')->validator('DataTables\Editor\Validate::numeric'),
                DataTables\Editor\Field::inst('marzo_solicitado')->validator('DataTables\Editor\Validate::numeric'),
                DataTables\Editor\Field::inst('marzo_aprobado')->validator('DataTables\Editor\Validate::numeric'),

                DataTables\Editor\Field::inst('abril_anio')->validator('DataTables\Editor\Validate::numeric'),
                DataTables\Editor\Field::inst('abril_fondos')->validator('DataTables\Editor\Validate::numeric'),
                DataTables\Editor\Field::inst('abril_solicitado')->validator('DataTables\Editor\Validate::numeric'),
                DataTables\Editor\Field::inst('abril_aprobado')->validator('DataTables\Editor\Validate::numeric'),

                DataTables\Editor\Field::inst('mayo_anio')->validator('DataTables\Editor\Validate::numeric'),
                DataTables\Editor\Field::inst('mayo_fondos')->validator('DataTables\Editor\Validate::numeric'),
                DataTables\Editor\Field::inst('mayo_solicitado')->validator('DataTables\Editor\Validate::numeric'),
                DataTables\Editor\Field::inst('mayo_aprobado')->validator('DataTables\Editor\Validate::numeric'),


                DataTables\Editor\Field::inst('junio_anio')->validator('DataTables\Editor\Validate::numeric'),
                DataTables\Editor\Field::inst('junio_fondos')->validator('DataTables\Editor\Validate::numeric'),
                DataTables\Editor\Field::inst('junio_solicitado')->validator('DataTables\Editor\Validate::numeric'),
                DataTables\Editor\Field::inst('junio_aprobado')->validator('DataTables\Editor\Validate::numeric'),

                DataTables\Editor\Field::inst('julio_anio')->validator('DataTables\Editor\Validate::numeric'),
                DataTables\Editor\Field::inst('julio_fondos')->validator('DataTables\Editor\Validate::numeric'),
                DataTables\Editor\Field::inst('julio_solicitado')->validator('DataTables\Editor\Validate::numeric'),
                DataTables\Editor\Field::inst('julio_aprobado')->validator('DataTables\Editor\Validate::numeric'),

                DataTables\Editor\Field::inst('agosto_anio')->validator('DataTables\Editor\Validate::numeric'),
                DataTables\Editor\Field::inst('agosto_fondos')->validator('DataTables\Editor\Validate::numeric'),
                DataTables\Editor\Field::inst('agosto_solicitado')->validator('DataTables\Editor\Validate::numeric'),
                DataTables\Editor\Field::inst('agosto_aprobado')->validator('DataTables\Editor\Validate::numeric'),

                DataTables\Editor\Field::inst('septiembre_anio')->validator('DataTables\Editor\Validate::numeric'),
                DataTables\Editor\Field::inst('septiembre_fondos')->validator('DataTables\Editor\Validate::numeric'),
                DataTables\Editor\Field::inst('septiembre_solicitado')->validator('DataTables\Editor\Validate::numeric'),
                DataTables\Editor\Field::inst('septiembre_aprobado')->validator('DataTables\Editor\Validate::numeric'),

                DataTables\Editor\Field::inst('octubre_anio')->validator('DataTables\Editor\Validate::numeric'),
                DataTables\Editor\Field::inst('octubre_fondos')->validator('DataTables\Editor\Validate::numeric'),
                DataTables\Editor\Field::inst('octubre_solicitado')->validator('DataTables\Editor\Validate::numeric'),
                DataTables\Editor\Field::inst('octubre_aprobado')->validator('DataTables\Editor\Validate::numeric'),

                DataTables\Editor\Field::inst('noviembre_anio')->validator('DataTables\Editor\Validate::numeric'),
                DataTables\Editor\Field::inst('noviembre_fondos')->validator('DataTables\Editor\Validate::numeric'),
                DataTables\Editor\Field::inst('noviembre_solicitado')->validator('DataTables\Editor\Validate::numeric'),
                DataTables\Editor\Field::inst('noviembre_aprobado')->validator('DataTables\Editor\Validate::numeric'),

                DataTables\Editor\Field::inst('diciembre_anio')->validator('DataTables\Editor\Validate::numeric'),
                DataTables\Editor\Field::inst('diciembre_fondos')->validator('DataTables\Editor\Validate::numeric'),
                DataTables\Editor\Field::inst('diciembre_solicitado')->validator('DataTables\Editor\Validate::numeric'),
                DataTables\Editor\Field::inst('diciembre_aprobado')->validator('DataTables\Editor\Validate::numeric'),

                DataTables\Editor\Field::inst('total_anio')->validator('DataTables\Editor\Validate::numeric'),
                DataTables\Editor\Field::inst('total_fondos')->validator('DataTables\Editor\Validate::numeric'),
                DataTables\Editor\Field::inst('total_solicitado')->validator('DataTables\Editor\Validate::numeric'),
                DataTables\Editor\Field::inst('total_aprobado')->validator('DataTables\Editor\Validate::numeric')
            )
            ->process($_POST)
            ->json();

//        $this->debugeo->imprimir_pre($resultado);

    }

    function subir_solicitado()
    {

//        Se prepara la configuracion de la libreria "Upload"
        $config['upload_path'] = './application/archivos/';
        $config['allowed_types'] = '*';

//        Se carga la libreria con sus configuraciones correspondientes
        $this->load->library('upload', $config);

//        Si el server no logra subir el archivo, despliega un mensaje de error
        if (!$this->upload->do_upload('archivoSubir')) {
//            Se capturan los errores en una variable y se imprimen para debug
            $error = array('error' => $this->upload->display_errors());
            $this->debugeo->imprimir_pre($error);
        } else {
            try {
                ini_set('memory_limit', '-1');
//            load our new PHPExcel library
                $this->load->library('excel');
//            Si el server logra subir el archivo, se toman los datos del archivo
                $data = $this->upload->data();

//            Se toma la ruta del archivo junto con su nombre y extension
                $file = $data["full_path"];

//            read file from path
                $objPHPExcel = PHPExcel_IOFactory::load($file);

//            get only the Cell Collection
                $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();

//            extract to a PHP readable array format
                foreach ($cell_collection as $cell) {
                    $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
                    $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
                    $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();

//                header will/should be in row 1 only. of course this can be modified to suit your need.
                    if ($row == 9) {
                        $datos_centro_costos[$row][$column] = $data_value;
                    } elseif ($row >= 12 && $row <= 392) {
                        $datos_detalle[$row][$column] = $data_value;
                    }
                }

//                $this->debugeo->imprimir_pre($datos_centro_costos);
//                $this->debugeo->imprimir_pre($datos_detalle);

                $this->db->trans_begin();

                if (!isset($datos_centro_costos[9]["G"]) || $datos_centro_costos[9]["G"] == "" || $datos_centro_costos[9]["G"] == NULL) {
                    throw new Exception('Al archivo le falta el centro de costos.');
                }

                $linea = 12;

                foreach ($datos_detalle as $key => $value) {

                    if (!isset($value["A"])) {
                        throw new Exception('Al archivo le falta la partida en la l�nea ' . $linea);
                    }

//                    $this->debugeo->imprimir_pre($value);

                    if (!isset($value["C"]) || !isset($value["D"]) || !isset($value["E"]) || !isset($value["F"]) || !isset($value["G"]) || !isset($value["H"]) || !isset($value["I"]) || !isset($value["J"]) || !isset($value["K"]) || !isset($value["L"]) || !isset($value["M"]) || !isset($value["N"])) {
                        throw new Exception('Falta un monto dentro del presupuesto solicitado en la l�nea ' . $linea);
                    }

                    $total = $value["C"] + $value["D"] + $value["E"] + $value["F"] + $value["G"] + $value["H"] + $value["I"] + $value["J"] + $value["K"] + $value["L"] + $value["M"] + $value["N"];

                    $datos_insertar = array(
                        'enero_solicitado' => $value["C"],
                        'febrero_solicitado' => $value["D"],
                        'marzo_solicitado' => $value["E"],
                        'abril_solicitado' => $value["F"],
                        'mayo_solicitado' => $value["G"],
                        'junio_solicitado' => $value["H"],
                        'julio_solicitado' => $value["I"],
                        'agosto_solicitado' => $value["J"],
                        'septiembre_solicitado' => $value["K"],
                        'octubre_solicitado' => $value["L"],
                        'noviembre_solicitado' => $value["M"],
                        'diciembre_solicitado' => $value["N"],
                        'total_solicitado' => $total,
                    );

                    $this->db->where('partida', $value["A"]);
                    $this->db->where('centro_costos', $datos_centro_costos[9]["G"]);
                    $this->db->update('meses_aprobado', $datos_insertar);

                    $linea += 1;

                }

                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    throw new Exception('Ha ocurrido un error al insertar el archivo, por favor comun&iacute;quese con su administrador.');
                } else {
                    $this->db->trans_commit();
                    $respuesta = array(
                        "mensaje" => '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>El archivo ha subido correctamente.</div>',
                    );
                }

            } catch (Exception $e) {
                $this->db->trans_rollback();

                $respuesta = array(
                    "mensaje" => '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' . $e->getMessage() . '</div>',
                );
            }

            $datos_header = array(
                "titulo_pagina" => "Armonniza | Resultado Archivo",
                "usuario" => $this->tank_auth->get_username(),
                "precompromisocss" => TRUE,
            );

            $this->parser->parse('front/header_main_view', $datos_header);
            $this->load->view('front/resultado_archivo_view', array(
                "mensaje" => $respuesta["mensaje"],
            ));
            $this->load->view('front/footer_main_view');
        }

    }

    function subir_aprobado()
    {

//        Se prepara la configuracion de la libreria "Upload"
        $config['upload_path'] = './application/archivos/';
        $config['allowed_types'] = '*';

//        Se carga la libreria con sus configuraciones correspondientes
        $this->load->library('upload', $config);

//        Si el server no logra subir el archivo, despliega un mensaje de error
        if (!$this->upload->do_upload('archivoSubir')) {
//            Se capturan los errores en una variable y se imprimen para debug
            $error = array('error' => $this->upload->display_errors());
            $this->debugeo->imprimir_pre($error);
        } else {
            try {
                ini_set('memory_limit', '-1');
//            load our new PHPExcel library
                $this->load->library('excel');
//            Si el server logra subir el archivo, se toman los datos del archivo
                $data = $this->upload->data();

//            Se toma la ruta del archivo junto con su nombre y extension
                $file = $data["full_path"];

//            read file from path
                $objPHPExcel = PHPExcel_IOFactory::load($file);

//            get only the Cell Collection
                $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();

//            extract to a PHP readable array format
                foreach ($cell_collection as $cell) {
                    $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
                    $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
                    $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();

//                header will/should be in row 1 only. of course this can be modified to suit your need.

                    if ($row >= 7) {
                        $datos_detalle[$row][$column] = $data_value;
                    }
                }

//                $this->debugeo->imprimir_pre($datos_centro_costos);
//                $this->debugeo->imprimir_pre($datos_detalle);

                $this->db->trans_begin();

                $linea = 7;

                foreach ($datos_detalle as $key => $value) {

                    if (!isset($value["B"])) {
                        throw new Exception('Al archivo le falta la partida en la l�nea ' . $linea);
                    }

//                   $this->debugeo->imprimir_pre($value);

                    if (!isset($value["F"]) || !isset($value["J"]) || !isset($value["N"]) || !isset($value["R"]) || !isset($value["V"]) || !isset($value["Z"]) || !isset($value["AD"]) || !isset($value["AH"]) || !isset($value["AL"]) || !isset($value["AP"]) || !isset($value["AT"]) || !isset($value["AX"])) {
                        throw new Exception('Falta un monto dentro del presupuesto solicitado en la l�nea ' . $linea);
                    }

                    $total = $value["F"] + $value["J"] + $value["N"] + $value["R"] + $value["V"] + $value["Z"] + $value["AD"] + $value["AH"] + $value["AL"] + $value["AP"] + $value["AT"] + $value["AX"];

                    $datos_insertar = array(
                        'enero_aprobado' => $value["F"],
                        'febrero_aprobado' => $value["J"],
                        'marzo_aprobado' => $value["N"],
                        'abril_aprobado' => $value["R"],
                        'mayo_aprobado' => $value["V"],
                        'junio_aprobado' => $value["Z"],
                        'julio_aprobado' => $value["AD"],
                        'agosto_aprobado' => $value["AH"],
                        'septiembre_aprobado' => $value["AL"],
                        'octubre_aprobado' => $value["AP"],
                        'noviembre_aprobado' => $value["AT"],
                        'diciembre_aprobado' => $value["AX"],
                        'total_aprobado' => $total,
                    );

                    $this->db->where('partida', $value["B"]);
                    $this->db->where('centro_costos', $value["A"]);
                    $this->db->update('meses_aprobado', $datos_insertar);

                    $linea += 1;

                }

                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    throw new Exception('Ha ocurrido un error al insertar el archivo, por favor comun&iacute;quese con su administrador.');
                } else {
                    $this->db->trans_commit();
                    $respuesta = array(
                        "mensaje" => '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>El archivo ha subido correctamente.</div>',
                    );
                }

            } catch (Exception $e) {
                $this->db->trans_rollback();

                $respuesta = array(
                    "mensaje" => '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' . $e->getMessage() . '</div>',
                );
            }

            $datos_header = array(
                "titulo_pagina" => "Armonniza | Resultado Archivo",
                "usuario" => $this->tank_auth->get_username(),
                "precompromisocss" => TRUE,
            );

            $this->parser->parse('front/header_main_view', $datos_header);
            $this->load->view('front/resultado_archivo_view', array(
                "mensaje" => $respuesta["mensaje"],
            ));
            $this->load->view('front/footer_main_view');
        }

    }

    function subir_2015() {

//        Se prepara la configuracion de la libreria "Upload"
        $config['upload_path'] = './application/archivos/';
        $config['allowed_types'] = '*';

//        Se carga la libreria con sus configuraciones correspondientes
        $this->load->library('upload', $config);

//        Si el server no logra subir el archivo, despliega un mensaje de error
        if (!$this->upload->do_upload('archivoSubir')) {
//            Se capturan los errores en una variable y se imprimen para debug
            $error = array('error' => $this->upload->display_errors());
            $this->debugeo->imprimir_pre($error);
        } else {
            try {
                ini_set('memory_limit', '-1');
//            load our new PHPExcel library
                $this->load->library('excel');
//            Si el server logra subir el archivo, se toman los datos del archivo
                $data = $this->upload->data();

//            Se toma la ruta del archivo junto con su nombre y extension
                $file = $data["full_path"];

//            read file from path
                $objPHPExcel = PHPExcel_IOFactory::load($file);

//            get only the Cell Collection
                $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();

//            extract to a PHP readable array format
                foreach ($cell_collection as $cell) {
                    $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
                    $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
                    $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();

//                header will/should be in row 1 only. of course this can be modified to suit your need.

                    if ($row >= 2 && $row <= 2732) {
                        $datos_detalle[$row][$column] = $data_value;
                    }
                }

//                $this->debugeo->imprimir_pre($datos_centro_costos);
//                $this->debugeo->imprimir_pre($datos_detalle);

                $this->db->trans_begin();

                $linea = 2;

                foreach ($datos_detalle as $key => $value) {

                    if (!isset($value["B"])) {
                        throw new Exception('Al archivo le falta la partida en la l�nea ' . $linea);
                    }

//                   $this->debugeo->imprimir_pre($value);

                    if (!isset($value["D"]) || !isset($value["E"]) || !isset($value["F"]) || !isset($value["G"]) || !isset($value["H"]) || !isset($value["I"]) || !isset($value["J"]) || !isset($value["K"]) || !isset($value["L"]) || !isset($value["M"]) || !isset($value["N"]) || !isset($value["O"])) {
                        throw new Exception('Falta un monto dentro del presupuesto solicitado en la l�nea ' . $linea);
                    }

                    $total = $value["D"] + $value["E"] + $value["F"] + $value["G"] + $value["H"] + $value["I"] + $value["J"] + $value["K"] + $value["L"] + $value["M"] + $value["N"] + $value["O"];

                    if ($value["A"] == 0 || !isset($value["A"]) || $value["A"] == NULL) {
                        continue;
                    }

                    $this->db->select('descripcion')->from('centro_costos_anteproyecto')->where('centro_costos', $value["A"]);
                    $query_centro_costos = $this->db->get();
                    $centro_costos = $query_centro_costos->row_array();

                    $this->db->select('descripcion')->from('partidas_anteproyecto')->where('partida', $value["B"]);
                    $query_partida = $this->db->get();
                    $partida = $query_partida->row_array();

                    $datos_insertar = array(
                        'centro_costos' => $value["A"],
                        'descripcion_centro_costos' => $centro_costos["descripcion"],
                        'partida' => $value["B"],
                        'descripcion_partida' => $partida["descripcion"],
                        'enero_anio' => $value["D"],
                        'febrero_anio' => $value["E"],
                        'marzo_anio' => $value["F"],
                        'abril_anio' => $value["G"],
                        'mayo_anio' => $value["H"],
                        'junio_anio' => $value["I"],
                        'julio_anio' => $value["J"],
                        'agosto_anio' => $value["K"],
                        'septiembre_anio' => $value["L"],
                        'octubre_anio' => $value["M"],
                        'noviembre_anio' => $value["N"],
                        'diciembre_anio' => $value["O"],
                        'total_anio' => $total,
                    );

                    $this->db->where('partida', $value["B"]);
                    $this->db->where('centro_costos', $value["A"]);
                    $this->db->update('meses_aprobado', $datos_insertar);

                    $linea += 1;

                }

                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    throw new Exception('Ha ocurrido un error al insertar el archivo, por favor comun&iacute;quese con su administrador.');
                } else {
                    $this->db->trans_commit();
                    $respuesta = array(
                        "mensaje" => '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>El archivo ha subido correctamente.</div>',
                    );
                }

            } catch (Exception $e) {
                $this->db->trans_rollback();

                $respuesta = array(
                    "mensaje" => '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' . $e->getMessage() . '</div>',
                );
            }

            $datos_header = array(
                "titulo_pagina" => "Armonniza | Resultado Archivo",
                "usuario" => $this->tank_auth->get_username(),
                "precompromisocss" => TRUE,
            );

            $this->parser->parse('front/header_main_view', $datos_header);
            $this->load->view('front/resultado_archivo_view', array(
                "mensaje" => $respuesta["mensaje"],
            ));
            $this->load->view('front/footer_main_view');
        }

    }

    function subir_fondos()
    {

//        Se prepara la configuracion de la libreria "Upload"
        $config['upload_path'] = './application/archivos/';
        $config['allowed_types'] = '*';

//        Se carga la libreria con sus configuraciones correspondientes
        $this->load->library('upload', $config);

//        Si el server no logra subir el archivo, despliega un mensaje de error
        if (!$this->upload->do_upload('archivoSubir')) {
//            Se capturan los errores en una variable y se imprimen para debug
            $error = array('error' => $this->upload->display_errors());
            $this->debugeo->imprimir_pre($error);
        } else {
            try {
                ini_set('memory_limit', '-1');
//            load our new PHPExcel library
                $this->load->library('excel');
//            Si el server logra subir el archivo, se toman los datos del archivo
                $data = $this->upload->data();

//            Se toma la ruta del archivo junto con su nombre y extension
                $file = $data["full_path"];

//            read file from path
                $objPHPExcel = PHPExcel_IOFactory::load($file);

//            get only the Cell Collection
                $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();

//            extract to a PHP readable array format
                foreach ($cell_collection as $cell) {
                    $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
                    $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
                    $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();

//                header will/should be in row 1 only. of course this can be modified to suit your need.

                    if ($row >= 2 && $row <= 6632) {
                        $datos_detalle[$row][$column] = $data_value;
                    }
                }

//                $this->debugeo->imprimir_pre($datos_centro_costos);
//                $this->debugeo->imprimir_pre($datos_detalle);

                $this->db->trans_begin();

                $linea = 2;

                foreach ($datos_detalle as $key => $value) {

                    if (!isset($value["B"])) {
                        throw new Exception('Al archivo le falta la partida en la l�nea ' . $linea);
                    }

//                   $this->debugeo->imprimir_pre($value);

                    if (!isset($value["C"]) || !isset($value["D"]) || !isset($value["E"]) || !isset($value["F"]) || !isset($value["G"]) || !isset($value["H"]) || !isset($value["I"]) || !isset($value["J"]) || !isset($value["K"]) || !isset($value["L"]) || !isset($value["M"]) || !isset($value["N"])) {
                        throw new Exception('Falta un monto dentro del presupuesto solicitado en la l�nea ' . $linea);
                    }

                    $total = $value["C"] + $value["D"] + $value["E"] + $value["F"] + $value["G"] + $value["H"] + $value["I"] + $value["J"] + $value["K"] + $value["L"] + $value["M"] + $value["N"];

                    $datos_insertar = array(
                        'enero_fondos' => $value["C"],
                        'febrero_fondos' => $value["D"],
                        'marzo_fondos' => $value["E"],
                        'abril_fondos' => $value["F"],
                        'mayo_fondos' => $value["G"],
                        'junio_fondos' => $value["H"],
                        'julio_fondos' => $value["I"],
                        'agosto_fondos' => $value["J"],
                        'septiembre_fondos' => $value["K"],
                        'octubre_fondos' => $value["L"],
                        'noviembre_fondos' => $value["M"],
                        'diciembre_fondos' => $value["N"],
                        'total_fondos' => $total,
                    );

                    $this->db->where('partida', $value["B"]);
                    $this->db->where('centro_costos', $value["A"]);
                    $this->db->update('meses_aprobado', $datos_insertar);

                    $linea += 1;

                }

                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    throw new Exception('Ha ocurrido un error al insertar el archivo, por favor comun&iacute;quese con su administrador.');
                } else {
                    $this->db->trans_commit();
                    $respuesta = array(
                        "mensaje" => '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>El archivo ha subido correctamente.</div>',
                    );
                }

            } catch (Exception $e) {
                $this->db->trans_rollback();

                $respuesta = array(
                    "mensaje" => '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' . $e->getMessage() . '</div>',
                );
            }

            $datos_header = array(
                "titulo_pagina" => "Armonniza | Resultado Archivo",
                "usuario" => $this->tank_auth->get_username(),
                "precompromisocss" => TRUE,
            );

            $this->parser->parse('front/header_main_view', $datos_header);
            $this->load->view('front/resultado_archivo_view', array(
                "mensaje" => $respuesta["mensaje"],
            ));
            $this->load->view('front/footer_main_view');
        }

    }

    function exportar_aprobado() {
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Exportar Aprobado",
            "usuario" => $this->tank_auth->get_username(),
            "compromisocss" => TRUE,
            "tablas" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/anteproyecto/exportar_aprobado_filtro_view');
        $this->load->view('front/footer_main_view', array(
            "exportar_aprobado" => TRUE,
        ));
    }

    function tabla_partidas()
    {
//        Se toman todos los datos de la caratula de precompromisos
        $this->db->select('partida, descripcion')->from('partidas_anteproyecto');
        $query = $this->db->get();
        $resultado = $query->result_array();

//        Se crea el arreglo de datos donde se va a guardar la informaci�n
        $output = array("data" => "");

//        Se recorre el arreglo fila por fila
        foreach ($resultado as $key => $value) {

            $output["data"][] = array(
                $value["partida"],
                $value["descripcion"],
                '<a data-dismiss="modal"><i class="fa fa-check"></i></a>',
            );
        }

        echo json_encode($output);
    }

    function tabla_centro_costos()
    {
//        Se toman todos los datos de la caratula de precompromisos
        $this->db->select('centro_costos, descripcion')->from('centro_costos_anteproyecto');
        $query = $this->db->get();
        $resultado = $query->result_array();

//        Se crea el arreglo de datos donde se va a guardar la informaci�n
        $output = array("data" => "");

//        Se recorre el arreglo fila por fila
        foreach ($resultado as $key => $value) {

            $output["data"][] = array(
                $value["centro_costos"],
                $value["descripcion"],
                '<a data-dismiss="modal"><i class="fa fa-check"></i></a>',
            );
        }

        echo json_encode($output);
    }

    function exportar_aprobado_excel() {

        $datos = $this->input->post();
        /**
         * $datos = array(
         * "centro_costos" => "01",
         * "partida" => "21101",
         * );
         * **/

        foreach ($datos as $key => $value) {
            $datos[$key] = trim($value);
        }

        //$this->benchmark->mark('code_start');

//        $this->debugeo->imprimir_pre($datos);

        //$this->load->library('excel');

        //activate worksheet number 1
        //$this->excel->setActiveSheetIndex(0);
        //name the worksheet
        //$this->excel->getActiveSheet()->setTitle('Reporte Aprobado');


        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
            'font' => array(
                'bold' => true,
            )
        );
        $bold = array(
            'font' => array(
                'bold' => true,
            )
        );
        $style_center = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );
        $style_left = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            )
        );
        $style_header = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'F0FEFC')
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );

        $style_color1 = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'EBCAF0')
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );
        $style_color2 = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'F8ECE1')
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );
        $style_color3 = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'EFF8E1')
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );
        $style_color4 = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'DDD6E3')
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );
        $style_color5 = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'E4DEE3')
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );
        $style_color6 = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'DFE1F8')
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );


        $this->excel->getActiveSheet()->mergeCells('A1:M1')->getStyle("A1")->applyFromArray($style);
        $this->excel->getActiveSheet()->setCellValue('A1', 'SECRETARIA DE SALUD MICHOACAN');
        $this->excel->getActiveSheet()->mergeCells('A2:M2')->getStyle("A2")->applyFromArray($style);
        $this->excel->getActiveSheet()->setCellValue('A2', 'Reporte de Aprobado');

        //set cell A1 content with some text

        $this->excel->getActiveSheet()->setCellValue('A6', 'Centro de Costos')->getColumnDimension("A")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('B6', 'Partida')->getColumnDimension("B")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('C6', 'Enero 2015')->getColumnDimension("C")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('D6', 'Enero 2015 Fondos')->getColumnDimension("D")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('E6', 'Enero 2016 Solicitado')->getColumnDimension("E")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('F6', 'Enero 2016 Aprobado')->getColumnDimension("F")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('G6', 'Febrero 2015')->getColumnDimension("G")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('H6', 'Febrero 2015 Fondos')->getColumnDimension("H")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('I6', 'Febrero 2016 Solicitado')->getColumnDimension("I")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('J6', 'Febrero 2016 Aprobado')->getColumnDimension("J")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('K6', 'Marzo 2015')->getColumnDimension("K")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('L6', 'Marzo 2015 Fondos')->getColumnDimension("L")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('M6', 'Marzo 2016 Solicitado')->getColumnDimension("M")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('N6', 'Marzo 2016 Aprobado')->getColumnDimension("N")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('O6', 'Abril 2015')->getColumnDimension("O")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('P6', 'Abril 2015 Fondos')->getColumnDimension("P")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('Q6', 'Abril 2016 Solicitado')->getColumnDimension("Q")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('R6', 'Abril 2016 Aprobado')->getColumnDimension("R")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('S6', 'Mayo 2015')->getColumnDimension("S")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('T6', 'Mayo 2015 Fondos')->getColumnDimension("T")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('U6', 'Mayo 2016 Solicitado')->getColumnDimension("U")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('V6', 'Mayo 2016 Aprobado')->getColumnDimension("V")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('W6', 'Junio 2015')->getColumnDimension("W")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('X6', 'Junio 2015 Fondos')->getColumnDimension("X")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('Y6', 'Junio 2016 Solicitado')->getColumnDimension("Y")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('Z6', 'Junio 2016 Aprobado')->getColumnDimension("Z")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('AA6', 'Julio 2015')->getColumnDimension("AA")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('AB6', 'Julio 2015 Fondos')->getColumnDimension("AB")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('AC6', 'Julio 2016 Solicitado')->getColumnDimension("AC")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('AD6', 'Julio 2016 Aprobado')->getColumnDimension("AD")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('AE6', 'Agosto 2015')->getColumnDimension("AE")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('AF6', 'Agosto 2015 Fondos')->getColumnDimension("AF")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('AG6', 'Agosto 2016 Solicitado')->getColumnDimension("AG")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('AH6', 'Agosto 2016 Aprobado')->getColumnDimension("AH")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('AI6', 'Septiembre 2015')->getColumnDimension("AI")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('AJ6', 'Septiembre 2015 Fondos')->getColumnDimension("AJ")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('AK6', 'Septiembre 2016 Solicitado')->getColumnDimension("AK")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('AL6', 'Septiembre 2016 Aprobado')->getColumnDimension("AL")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('AM6', 'Octubre 2015')->getColumnDimension("AM")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('AN6', 'Octubre 2015 Fondos')->getColumnDimension("AN")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('AO6', 'Octubre 2016 Solicitado')->getColumnDimension("AO")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('AP6', 'Octubre 2016 Aprobado')->getColumnDimension("AP")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('AQ6', 'Noviembre 2015')->getColumnDimension("AQ")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('AR6', 'Noviembre 2015 Fondos')->getColumnDimension("AR")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('AS6', 'Noviembre 2016 Solicitado')->getColumnDimension("AS")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('AT6', 'Noviembre 2016 Aprobado')->getColumnDimension("AT")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('AU6', 'Diciembre 2015')->getColumnDimension("AU")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('AV6', 'Diciembre 2015 Fondos')->getColumnDimension("AV")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('AW6', 'Diciembre 2016 Solicitado')->getColumnDimension("AW")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('AX6', 'Diciembre 2016 Aprobado')->getColumnDimension("AX")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('AY6', 'Total 2015')->getColumnDimension("AY")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('AZ6', 'Total Fondos')->getColumnDimension("AY")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('BA6', 'Total Solicitado')->getColumnDimension("BA")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('BB6', 'Total Aprobado')->getColumnDimension("BB")->setWidth(20);

        $this->excel->getActiveSheet()->setCellValue('A6', 'Centro de Costos')->getStyle('A6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('B6', 'Partida')->getStyle('B6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('C6', 'Enero 2015')->getStyle('C6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('D6', 'Enero 2015 Fondos')->getStyle('D6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('E6', 'Enero 2016 Solicitado')->getStyle('E6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('F6', 'Enero 2016 Aprobado')->getStyle('F6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('G6', 'Febrero 2015')->getStyle('G6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('H6', 'Febrero 2015 Fondos')->getStyle('H6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('I6', 'Febrero 2016 Solicitado')->getStyle('I6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('J6', 'Febrero 2016 Aprobado')->getStyle('J6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('K6', 'Marzo 2015')->getStyle('K6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('L6', 'Marzo 2015 Fondos')->getStyle('L6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('M6', 'Marzo 2016 Solicitado')->getStyle('M6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('N6', 'Marzo 2016 Aprobado')->getStyle('N6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('O6', 'Abril 2015')->getStyle('O6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('P6', 'Abril 2015 Fondos')->getStyle('P6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('Q6', 'Abril 2016 Solicitado')->getStyle('Q6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('R6', 'Abril 2016 Aprobado')->getStyle('R6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('S6', 'Mayo 2015')->getStyle('S6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('T6', 'Mayo 2015 Fondos')->getStyle('T6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('U6', 'Mayo 2016 Solicitado')->getStyle('U6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('V6', 'Mayo 2016 Aprobado')->getStyle('V6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('W6', 'Junio 2015')->getStyle('W6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('X6', 'Junio 2015 Fondos')->getStyle('X6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('Y6', 'Junio 2016 Solicitado')->getStyle('Y6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('Z6', 'Junio 2016 Aprobado')->getStyle('Z6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('AA6', 'Julio 2015')->getStyle('AA6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('AB6', 'Julio 2015 Fondos')->getStyle('AB6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('AC6', 'Julio 2016 Solicitado')->getStyle('AC6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('AD6', 'Julio 2016 Aprobado')->getStyle('AD6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('AE6', 'Agosto 2015')->getStyle('AE6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('AF6', 'Agosto 2015 Fondos')->getStyle('AF6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('AG6', 'Agosto 2016 Solicitado')->getStyle('AG6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('AH6', 'Agosto 2016 Aprobado')->getStyle('AH6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('AI6', 'Septiembre 2015')->getStyle('AI6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('AJ6', 'Septiembre 2015 Fondos')->getStyle('AJ6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('AK6', 'Septiembre 2016 Solicitado')->getStyle('AK6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('AL6', 'Septiembre 2016 Aprobado')->getStyle('AL6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('AM6', 'Octubre 2015')->getStyle('AM6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('AN6', 'Octubre 2015 Fondos')->getStyle('AN6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('AO6', 'Octubre 2016 Solicitado')->getStyle('AO6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('AP6', 'Octubre 2016 Aprobado')->getStyle('AP6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('AQ6', 'Noviembre 2015')->getStyle('AQ6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('AR6', 'Noviembre 2015 Fondos')->getStyle('AR6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('AS6', 'Noviembre 2016 Solicitado')->getStyle('AS6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('AT6', 'Noviembre 2016 Aprobado')->getStyle('AT6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('AU6', 'Diciembre 2015')->getStyle('AU6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('AV6', 'Diciembre 2015 Fondos')->getStyle('AV6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('AW6', 'Diciembre 2016 Solicitado')->getStyle('AW6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('AX6', 'Diciembre 2016 Aprobado')->getStyle('AX6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('AY6', 'Total 2015')->getStyle('AY6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('AZ6', 'Total Fondos')->getStyle('AZ6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('BA6', 'Total Solicitado')->getStyle('BA6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('BB6', 'Total Aprobado')->getStyle('BB6')->applyFromArray($style_header);

        $arreglo_like = array('partida' => $datos["partida"], 'centro_costos' => $datos["centro_costos"]);
        $this->db->select('*')->from('meses_aprobado')->like($arreglo_like);
        $query = $this->db->get();
        $resultado = $query->result_array();

//        $this->debugeo->imprimir_pre($resultado);

        $row = 7;

        foreach ($resultado as $key => $value) {
            $this->excel->getActiveSheet()->setCellValue('A' . $row, $value["centro_costos"])->getStyle('A' . $row)->applyFromArray($style_color1);
            $this->excel->getActiveSheet()->setCellValue('B' . $row, $value["partida"])->getStyle('B' . $row)->applyFromArray($style_color2);

            $this->excel->getActiveSheet()->setCellValue('C' . $row, '$ ' . ($value["enero_anio"] == 0 || $value["enero_anio"] == NULL ? '0.00' : $this->cart->format_number($value["enero_anio"], 2)))->getStyle('C' . $row)->applyFromArray($style_color3);
            $this->excel->getActiveSheet()->setCellValue('D' . $row, '$ ' . ($value["enero_fondos"] == 0 || $value["enero_fondos"] == NULL ? '0.00' : $this->cart->format_number($value["enero_fondos"], 2)))->getStyle('D' . $row)->applyFromArray($style_color4);
            $this->excel->getActiveSheet()->setCellValue('E' . $row, '$ ' . ($value["enero_solicitado"] == 0 || $value["enero_solicitado"] == NULL ? '0.00' : $this->cart->format_number($value["enero_solicitado"], 2)))->getStyle('E' . $row)->applyFromArray($style_color5);
            $this->excel->getActiveSheet()->setCellValue('F' . $row, '$ ' . ($value["enero_aprobado"] == 0 || $value["enero_aprobado"] == NULL ? '0.00' : $this->cart->format_number($value["enero_aprobado"], 2)))->getStyle('F' . $row)->applyFromArray($style_color6);

            $this->excel->getActiveSheet()->setCellValue('G' . $row, '$ ' . ($value["febrero_anio"] == 0 || $value["febrero_anio"] == NULL ? '0.00' : $this->cart->format_number($value["febrero_anio"], 2)))->getStyle('G' . $row)->applyFromArray($style_color3);
            $this->excel->getActiveSheet()->setCellValue('H' . $row, '$ ' . ($value["febrero_fondos"] == 0 || $value["febrero_fondos"] == NULL ? '0.00' : $this->cart->format_number($value["febrero_fondos"], 2)))->getStyle('H' . $row)->applyFromArray($style_color4);
            $this->excel->getActiveSheet()->setCellValue('I' . $row, '$ ' . ($value["febrero_solicitado"] == 0 || $value["febrero_solicitado"] == NULL ? '0.00' : $this->cart->format_number($value["febrero_solicitado"], 2)))->getStyle('I' . $row)->applyFromArray($style_color5);
            $this->excel->getActiveSheet()->setCellValue('J' . $row, '$ ' . ($value["febrero_aprobado"] == 0 || $value["febrero_aprobado"] == NULL ? '0.00' : $this->cart->format_number($value["febrero_aprobado"], 2)))->getStyle('J' . $row)->applyFromArray($style_color6);

            $this->excel->getActiveSheet()->setCellValue('K' . $row, '$ ' . ($value["marzo_anio"] == 0 || $value["marzo_anio"] == NULL ? '0.00' : $this->cart->format_number($value["marzo_anio"], 2)))->getStyle('K' . $row)->applyFromArray($style_color3);
            $this->excel->getActiveSheet()->setCellValue('L' . $row, '$ ' . ($value["marzo_fondos"] == 0 || $value["marzo_fondos"] == NULL ? '0.00' : $this->cart->format_number($value["marzo_fondos"], 2)))->getStyle('L' . $row)->applyFromArray($style_color4);
            $this->excel->getActiveSheet()->setCellValue('M' . $row, '$ ' . ($value["marzo_solicitado"] == 0 || $value["marzo_solicitado"] == NULL ? '0.00' : $this->cart->format_number($value["marzo_solicitado"], 2)))->getStyle('M' . $row)->applyFromArray($style_color5);
            $this->excel->getActiveSheet()->setCellValue('N' . $row, '$ ' . ($value["marzo_aprobado"] == 0 || $value["marzo_aprobado"] == NULL ? '0.00' : $this->cart->format_number($value["marzo_aprobado"], 2)))->getStyle('N' . $row)->applyFromArray($style_color6);

            $this->excel->getActiveSheet()->setCellValue('O' . $row, '$ ' . ($value["abril_anio"] == 0 || $value["abril_anio"] == NULL ? '0.00' : $this->cart->format_number($value["abril_anio"], 2)))->getStyle('O' . $row)->applyFromArray($style_color3);
            $this->excel->getActiveSheet()->setCellValue('P' . $row, '$ ' . ($value["abril_fondos"] == 0 || $value["abril_fondos"] == NULL ? '0.00' : $this->cart->format_number($value["abril_fondos"], 2)))->getStyle('P' . $row)->applyFromArray($style_color4);
            $this->excel->getActiveSheet()->setCellValue('Q' . $row, '$ ' . ($value["abril_solicitado"] == 0 || $value["abril_solicitado"] == NULL ? '0.00' : $this->cart->format_number($value["abril_solicitado"], 2)))->getStyle('Q' . $row)->applyFromArray($style_color5);
            $this->excel->getActiveSheet()->setCellValue('R' . $row, '$ ' . ($value["abril_aprobado"] == 0 || $value["abril_aprobado"] == NULL ? '0.00' : $this->cart->format_number($value["abril_aprobado"], 2)))->getStyle('R' . $row)->applyFromArray($style_color6);

            $this->excel->getActiveSheet()->setCellValue('S' . $row, '$ ' . ($value["mayo_anio"] == 0 || $value["mayo_anio"] == NULL ? '0.00' : $this->cart->format_number($value["mayo_anio"], 2)))->getStyle('S' . $row)->applyFromArray($style_color3);
            $this->excel->getActiveSheet()->setCellValue('T' . $row, '$ ' . ($value["mayo_fondos"] == 0 || $value["mayo_fondos"] == NULL ? '0.00' : $this->cart->format_number($value["mayo_fondos"], 2)))->getStyle('T' . $row)->applyFromArray($style_color4);
            $this->excel->getActiveSheet()->setCellValue('U' . $row, '$ ' . ($value["mayo_solicitado"] == 0 || $value["mayo_solicitado"] == NULL ? '0.00' : $this->cart->format_number($value["mayo_solicitado"], 2)))->getStyle('U' . $row)->applyFromArray($style_color5);
            $this->excel->getActiveSheet()->setCellValue('V' . $row, '$ ' . ($value["mayo_aprobado"] == 0 || $value["mayo_aprobado"] == NULL ? '0.00' : $this->cart->format_number($value["mayo_aprobado"], 2)))->getStyle('V' . $row)->applyFromArray($style_color6);

            $this->excel->getActiveSheet()->setCellValue('W' . $row, '$ ' . ($value["junio_anio"] == 0 || $value["junio_anio"] == NULL ? '0.00' : $this->cart->format_number($value["junio_anio"], 2)))->getStyle('W' . $row)->applyFromArray($style_color3);
            $this->excel->getActiveSheet()->setCellValue('X' . $row, '$ ' . ($value["junio_fondos"] == 0 || $value["junio_fondos"] == NULL ? '0.00' : $this->cart->format_number($value["junio_fondos"], 2)))->getStyle('X' . $row)->applyFromArray($style_color4);
            $this->excel->getActiveSheet()->setCellValue('Y' . $row, '$ ' . ($value["junio_solicitado"] == 0 || $value["junio_solicitado"] == NULL ? '0.00' : $this->cart->format_number($value["junio_solicitado"], 2)))->getStyle('Y' . $row)->applyFromArray($style_color5);
            $this->excel->getActiveSheet()->setCellValue('Z' . $row, '$ ' . ($value["junio_aprobado"] == 0 || $value["junio_aprobado"] == NULL ? '0.00' : $this->cart->format_number($value["junio_aprobado"], 2)))->getStyle('Z' . $row)->applyFromArray($style_color6);

            $this->excel->getActiveSheet()->setCellValue('AA' . $row, '$ ' . ($value["julio_anio"] == 0 || $value["julio_anio"] == NULL ? '0.00' : $this->cart->format_number($value["julio_anio"], 2)))->getStyle('AA' . $row)->applyFromArray($style_color3);
            $this->excel->getActiveSheet()->setCellValue('AB' . $row, '$ ' . ($value["julio_fondos"] == 0 || $value["julio_fondos"] == NULL ? '0.00' : $this->cart->format_number($value["julio_fondos"], 2)))->getStyle('AB' . $row)->applyFromArray($style_color4);
            $this->excel->getActiveSheet()->setCellValue('AC' . $row, '$ ' . ($value["julio_solicitado"] == 0 || $value["julio_solicitado"] == NULL ? '0.00' : $this->cart->format_number($value["julio_solicitado"], 2)))->getStyle('AC' . $row)->applyFromArray($style_color5);
            $this->excel->getActiveSheet()->setCellValue('AD' . $row, '$ ' . ($value["julio_aprobado"] == 0 || $value["julio_aprobado"] == NULL ? '0.00' : $this->cart->format_number($value["julio_aprobado"], 2)))->getStyle('AD' . $row)->applyFromArray($style_color6);

            $this->excel->getActiveSheet()->setCellValue('AE' . $row, '$ ' . ($value["agosto_anio"] == 0 || $value["agosto_anio"] == NULL ? '0.00' : $this->cart->format_number($value["agosto_anio"], 2)))->getStyle('AE' . $row)->applyFromArray($style_color3);
            $this->excel->getActiveSheet()->setCellValue('AF' . $row, '$ ' . ($value["agosto_fondos"] == 0 || $value["agosto_fondos"] == NULL ? '0.00' : $this->cart->format_number($value["agosto_fondos"], 2)))->getStyle('AF' . $row)->applyFromArray($style_color4);
            $this->excel->getActiveSheet()->setCellValue('AG' . $row, '$ ' . ($value["agosto_solicitado"] == 0 || $value["agosto_solicitado"] == NULL ? '0.00' : $this->cart->format_number($value["agosto_solicitado"], 2)))->getStyle('AG' . $row)->applyFromArray($style_color5);
            $this->excel->getActiveSheet()->setCellValue('AH' . $row, '$ ' . ($value["agosto_aprobado"] == 0 || $value["agosto_aprobado"] == NULL ? '0.00' : $this->cart->format_number($value["agosto_aprobado"], 2)))->getStyle('AH' . $row)->applyFromArray($style_color6);

            $this->excel->getActiveSheet()->setCellValue('AI' . $row, '$ ' . ($value["septiembre_anio"] == 0 || $value["septiembre_anio"] == NULL ? '0.00' : $this->cart->format_number($value["septiembre_anio"], 2)))->getStyle('AI' . $row)->applyFromArray($style_color3);
            $this->excel->getActiveSheet()->setCellValue('AJ' . $row, '$ ' . ($value["septiembre_fondos"] == 0 || $value["septiembre_fondos"] == NULL ? '0.00' : $this->cart->format_number($value["septiembre_fondos"], 2)))->getStyle('AJ' . $row)->applyFromArray($style_color4);
            $this->excel->getActiveSheet()->setCellValue('AK' . $row, '$ ' . ($value["septiembre_solicitado"] == 0 || $value["septiembre_solicitado"] == NULL ? '0.00' : $this->cart->format_number($value["septiembre_solicitado"], 2)))->getStyle('AK' . $row)->applyFromArray($style_color5);
            $this->excel->getActiveSheet()->setCellValue('AL' . $row, '$ ' . ($value["septiembre_aprobado"] == 0 || $value["septiembre_aprobado"] == NULL ? '0.00' : $this->cart->format_number($value["septiembre_aprobado"], 2)))->getStyle('AL' . $row)->applyFromArray($style_color6);

            $this->excel->getActiveSheet()->setCellValue('AM' . $row, '$ ' . ($value["octubre_anio"] == 0 || $value["octubre_anio"] == NULL ? '0.00' : $this->cart->format_number($value["octubre_anio"], 2)))->getStyle('AM' . $row)->applyFromArray($style_color3);
            $this->excel->getActiveSheet()->setCellValue('AN' . $row, '$ ' . ($value["octubre_fondos"] == 0 || $value["octubre_fondos"] == NULL ? '0.00' : $this->cart->format_number($value["octubre_fondos"], 2)))->getStyle('AN' . $row)->applyFromArray($style_color4);
            $this->excel->getActiveSheet()->setCellValue('AO' . $row, '$ ' . ($value["octubre_solicitado"] == 0 || $value["octubre_solicitado"] == NULL ? '0.00' : $this->cart->format_number($value["octubre_solicitado"], 2)))->getStyle('AO' . $row)->applyFromArray($style_color5);
            $this->excel->getActiveSheet()->setCellValue('AP' . $row, '$ ' . ($value["octubre_aprobado"] == 0 || $value["octubre_aprobado"] == NULL ? '0.00' : $this->cart->format_number($value["octubre_aprobado"], 2)))->getStyle('AP' . $row)->applyFromArray($style_color6);

            $this->excel->getActiveSheet()->setCellValue('AQ' . $row, '$ ' . ($value["noviembre_anio"] == 0 || $value["noviembre_anio"] == NULL ? '0.00' : $this->cart->format_number($value["noviembre_anio"], 2)))->getStyle('AQ' . $row)->applyFromArray($style_color3);
            $this->excel->getActiveSheet()->setCellValue('AR' . $row, '$ ' . ($value["noviembre_fondos"] == 0 || $value["noviembre_fondos"] == NULL ? '0.00' : $this->cart->format_number($value["noviembre_fondos"], 2)))->getStyle('AR' . $row)->applyFromArray($style_color4);
            $this->excel->getActiveSheet()->setCellValue('AS' . $row, '$ ' . ($value["noviembre_solicitado"] == 0 || $value["noviembre_solicitado"] == NULL ? '0.00' : $this->cart->format_number($value["noviembre_solicitado"], 2)))->getStyle('AS' . $row)->applyFromArray($style_color5);
            $this->excel->getActiveSheet()->setCellValue('AT' . $row, '$ ' . ($value["noviembre_aprobado"] == 0 || $value["noviembre_aprobado"] == NULL ? '0.00' : $this->cart->format_number($value["noviembre_aprobado"], 2)))->getStyle('AT' . $row)->applyFromArray($style_color6);

            $this->excel->getActiveSheet()->setCellValue('AU' . $row, '$ ' . ($value["diciembre_anio"] == 0 || $value["diciembre_anio"] == NULL ? '0.00' : $this->cart->format_number($value["diciembre_anio"], 2)))->getStyle('AU' . $row)->applyFromArray($style_color3);
            $this->excel->getActiveSheet()->setCellValue('AV' . $row, '$ ' . ($value["diciembre_fondos"] == 0 || $value["diciembre_fondos"] == NULL ? '0.00' : $this->cart->format_number($value["diciembre_fondos"], 2)))->getStyle('AV' . $row)->applyFromArray($style_color4);
            $this->excel->getActiveSheet()->setCellValue('AW' . $row, '$ ' . ($value["diciembre_solicitado"] == 0 || $value["diciembre_solicitado"] == NULL ? '0.00' : $this->cart->format_number($value["diciembre_solicitado"], 2)))->getStyle('AW' . $row)->applyFromArray($style_color5);
            $this->excel->getActiveSheet()->setCellValue('AX' . $row, '$ ' . ($value["diciembre_aprobado"] == 0 || $value["diciembre_aprobado"] == NULL ? '0.00' : $this->cart->format_number($value["diciembre_aprobado"], 2)))->getStyle('AX' . $row)->applyFromArray($style_color6);

            $this->excel->getActiveSheet()->setCellValue('AY' . $row, '$ ' . ($value["total_anio"] == 0 || $value["total_anio"] == NULL ? '0.00' : $this->cart->format_number($value["total_anio"], 2)))->getStyle('AY' . $row)->applyFromArray($style_color3);
            $this->excel->getActiveSheet()->setCellValue('AZ' . $row, '$ ' . ($value["total_fondos"] == 0 || $value["total_fondos"] == NULL ? '0.00' : $this->cart->format_number($value["total_fondos"], 2)))->getStyle('AZ' . $row)->applyFromArray($style_color4);
            $this->excel->getActiveSheet()->setCellValue('BA' . $row, '$ ' . ($value["total_solicitado"] == 0 || $value["total_solicitado"] == NULL ? '0.00' : $this->cart->format_number($value["total_solicitado"], 2)))->getStyle('BA' . $row)->applyFromArray($style_color5);
            $this->excel->getActiveSheet()->setCellValue('BB' . $row, '$ ' . ($value["total_aprobado"] == 0 || $value["total_aprobado"] == NULL ? '0.00' : $this->cart->format_number($value["total_aprobado"], 2)))->getStyle('BB' . $row)->applyFromArray($style_color6);

            $row += 1;

        }

        //$this->benchmark->mark('code_end');

        //echo $this->benchmark->elapsed_time('code_start', 'code_end');

        $filename = "Reporte Aprobado.xls"; // Agregar fecha en que se gener�
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
//        save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
//        if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
//        force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');


    }

    function exportar_aprobado_csv () {

        $datos = $this->input->post();
//        $this->debugeo->imprimir_pre($datos);

        $this->load->dbutil();
        $this->load->helper('file');
        $this->load->helper('download');
        $delimiter = ",";
        $newline = "\r\n";
        $filename = "Reporte Aprobado.csv";

        if($datos) {


        } else if ($datos == "") {

            $query = "SELECT centro_costos AS 'Centro de Costos',
                             descripcion_centro_costos AS 'Descripcion CC',
                             partida AS 'Partida',
                             descripcion_partida AS 'Descripcion Partida',
                             enero_anio AS 'Enero',
                             febrero_anio AS 'Febrero',
                             marzo_anio AS 'Marzo',
                             abril_anio AS 'Abril',
                             mayo_anio AS 'Mayo',
                             junio_anio AS 'Junio',
                             julio_anio AS 'Julio',
                             agosto_anio AS 'Agosto',
                             septiembre_anio AS 'Septiembre',
                             octubre_anio AS 'Octubre',
                             noviembre_anio AS 'Noviembre',
                             diciembre_anio AS 'Diciembre',
                             enero_fondos AS 'Enero Fondos',
                             febrero_fondos AS 'Febrero Fondos',
                             marzo_fondos AS 'Marzo Fondos',
                             abril_fondos AS 'Abril Fondos',
                             mayo_fondos AS 'Mayo Fondos',
                             junio_fondos AS 'Junio Fondos',
                             julio_fondos AS 'Julio Fondos',
                             agosto_fondos AS 'Agosto Fondos',
                             septiembre_fondos AS 'Septiembre Fondos',
                             octubre_fondos AS 'Octubre Fondos',
                             noviembre_fondos AS 'Noviembre Fondos',
                             diciembre_fondos AS 'Diciembre Fondos',
                             enero_solicitado AS 'Enero Solicitado',
                             febrero_solicitado AS 'Febrero Solicitado',
                             marzo_solicitado AS 'Marzo Solicitado',
                             abril_solicitado AS 'Abril Solicitado',
                             mayo_solicitado AS 'Mayo Solicitado',
                             junio_solicitado AS 'Junio Solicitado',
                             julio_solicitado AS 'Julio Solicitado',
                             agosto_solicitado AS 'Agosto Solicitado',
                             septiembre_solicitado AS 'Septiembre Solicitado',
                             octubre_solicitado AS 'Octubre Solicitado',
                             noviembre_solicitado AS 'Noviembre Solicitado',
                             diciembre_solicitado AS 'Diciembre Solicitado',
                             enero_aprobado AS 'Enero Aprobado',
                             febrero_aprobado AS 'Febrero Aprobado',
                             marzo_aprobado AS 'Marzo Aprobado',
                             abril_aprobado AS 'Abril Aprobado',
                             mayo_aprobado AS 'Mayo Aprobado',
                             junio_aprobado AS 'Junio Aprobado',
                             julio_aprobado AS 'Julio Aprobado',
                             agosto_aprobado AS 'Agosto Aprobado',
                             septiembre_aprobado AS 'Septiembre Aprobado',
                             octubre_aprobado AS 'Octubre Aprobado',
                             noviembre_aprobado AS 'Noviembre Aprobado',
                             diciembre_aprobado AS 'Diciembre Aprobado',
                      ";


        }

        $result = $this->dbutil->csv_from_result($query, $delimiter, $newline);
        force_download($filename, $result);
    }

    function descripcion() {

        $partida = $this->input->post('partida');
        $centro_costos = $this->input->post('centro_costos');

        $this->db->where('partida', $partida);
        $this->db->where('centro_costos', $centro_costos);
        $this->db->select('descripcion_partida');
        $this->db->select('descripcion_centro_costos');
        $this->db->select('partida');
        $query = $this->db->get('meses_aprobado');
        $query = $query->result_array();

        header('Content-Type: application/json');
        echo json_encode($query[0]);

    }

    function generar_documento() {
//        $datos = $this->input->post();
        $datos = array(
            "partida" => "",
            "centro_costos" => 10,
        );

        foreach ($datos as $key => $value) {
            $datos[$key] = trim($value);
        }

        $this->load->library('Pdf');

        $pdf = new Pdf('L', 'cm', 'Letter', true, 'UTF-8', false);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Armonniza');
        $pdf->SetTitle('Presupuesto Aprobado');
        $pdf->SetSubject('Presupuesto Aprobado');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
//        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
//        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        // remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
//        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, 10, PDF_MARGIN_RIGHT);
//        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// ---------------------------------------------------------

// set font
        $pdf->SetFont('helvetica', '', 12);

// add a page
        $pdf->AddPage('L', 'Letter');

        $arreglo_like = array('partida' => $datos["partida"], 'centro_costos' => $datos["centro_costos"]);
        $this->db->select('*')->from('meses_aprobado')->like($arreglo_like);
        $query = $this->db->get();
        $resultado = $query->result_array();

        $tabla_estructura = '<table style="font-size: small;  border: 1px solid #BDBDBD;" cellpadding="4">
                            <tr>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" >Centro de Costos</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" >Descripcion</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" >Partida</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" >Descripcion</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" >Enero 2015</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" >Enero fondos 2105</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" >Enero 2016 solicitado</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" >Enero 2016 Aprobado</td>
                                
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" >Febrero 2015</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" >Febrero fondos 2105</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" >Febrero 2016 solicitado</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" >Febrero 2016 Aprobado</td>
                                
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" >Marzo 2015</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" >Marzo fondos 2105</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" >Marzo 2016 solicitado</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" >Marzo 2016 Aprobado</td>
                                
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" >Abril 2015</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" >Abril fondos 2105</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" >Abril 2016 solicitado</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" >Abril 2016 Aprobado</td>
                                
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" >Mayo 2015</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" >Mayo fondos 2105</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" >Mayo 2016 solicitado</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" >Mayo 2016 Aprobado</td>
                                
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" >Junio 2015</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" >Junio fondos 2105</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" >Junio 2016 solicitado</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" >Junio 2016 Aprobado</td>
                                
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" >Julio 2015</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" >Julio fondos 2105</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" >Julio 2016 solicitado</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" >Julio 2016 Aprobado</td>
                                
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" >Agosto 2015</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" >Agosto fondos 2105</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" >Agosto 2016 solicitado</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" >Agosto 2016 Aprobado</td>
                                
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" >Septiembre 2015</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" >Septiembre fondos 2105</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" >Septiembre 2016 solicitado</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" >Septiembre 2016 Aprobado</td>
                                
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" >Octubre 2015</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" >Octubre fondos 2105</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" >Octubre 2016 solicitado</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" >Octubre 2016 Aprobado</td>
                                
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" >Noviembre 2015</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" >Noviembre fondos 2105</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" >Noviembre 2016 solicitado</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" >Noviembre 2016 Aprobado</td>
                                
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" >Diciembre 2015</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" >Diciembre fondos 2105</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" >Diciembre 2016 solicitado</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" >Diciembre 2016 Aprobado</td>
                            </tr>';

        foreach ($resultado as $key => $value) {
            $tabla_estructura .= '<tr>';
            $tabla_estructura .= '<td align="left" >$</td>';
            $tabla_estructura .= '<td align="right" style="border-right: 1px solid #BDBDBD;" >' . ($value["enero"] == 0 || $value["enero"] == NULL ? '0.00' : $this->cart->format_number(round($value["enero"], 2))) . '</td>';
            $tabla_estructura .= '<td align="left" >$</td>';
            $tabla_estructura .= '<td align="right" style="border-right: 1px solid #BDBDBD;" >' . ($value["enero_fondos"] == 0 || $value["enero_fondos"] == NULL ? '0.00' : $this->cart->format_number(round($value["enero_fondos"], 2))) . '</td>';
            $tabla_estructura .= '<td align="left" >$</td>';
            $tabla_estructura .= '<td align="right" style="border-right: 1px solid #BDBDBD;" >' . ($value["enero_solicitado"] == 0 || $value["enero_solicitado"] == NULL ? '0.00' : $this->cart->format_number(round($value["enero_solicitado"], 2))) . '</td>';
            $tabla_estructura .= '<td align="left" >$</td>';
            $tabla_estructura .= '<td align="right" style="border-right: 1px solid #BDBDBD;" >' . ($value["enero_aprobado"] == 0 || $value["enero_aprobado"] == NULL ? '0.00' : $this->cart->format_number(round($value["enero_aprobado"], 2))) . '</td>';

            $tabla_estructura .= '<td align="left" >$</td>';
            $tabla_estructura .= '<td align="right" style="border-right: 1px solid #BDBDBD;" >' . ($value["febrero"] == 0 || $value["febrero"] == NULL ? '0.00' : $this->cart->format_number(round($value["febrero"], 2))) . '</td>';
            $tabla_estructura .= '<td align="left" >$</td>';
            $tabla_estructura .= '<td align="right" style="border-right: 1px solid #BDBDBD;" >' . ($value["febrero_fondos"] == 0 || $value["febrero_fondos"] == NULL ? '0.00' : $this->cart->format_number(round($value["febrero_fondos"], 2))) . '</td>';
            $tabla_estructura .= '<td align="left" >$</td>';
            $tabla_estructura .= '<td align="right" style="border-right: 1px solid #BDBDBD;" >' . ($value["febrero_solicitado"] == 0 || $value["febrero_solicitado"] == NULL ? '0.00' : $this->cart->format_number(round($value["febrero_solicitado"], 2))) . '</td>';
            $tabla_estructura .= '<td align="left" >$</td>';
            $tabla_estructura .= '<td align="right" style="border-right: 1px solid #BDBDBD;" >' . ($value["febrero_aprobado"] == 0 || $value["febrero_aprobado"] == NULL ? '0.00' : $this->cart->format_number(round($value["febrero_aprobado"], 2))) . '</td>';

            $tabla_estructura .= '<td align="left" >$</td>';
            $tabla_estructura .= '<td align="right" style="border-right: 1px solid #BDBDBD;" >' . ($value["marzo"] == 0 || $value["marzo"] == NULL ? '0.00' : $this->cart->format_number(round($value["marzo"], 2))) . '</td>';
            $tabla_estructura .= '<td align="left" >$</td>';
            $tabla_estructura .= '<td align="right" style="border-right: 1px solid #BDBDBD;" >' . ($value["marzo_fondos"] == 0 || $value["marzo_fondos"] == NULL ? '0.00' : $this->cart->format_number(round($value["marzo_fondos"], 2))) . '</td>';
            $tabla_estructura .= '<td align="left" >$</td>';
            $tabla_estructura .= '<td align="right" style="border-right: 1px solid #BDBDBD;" >' . ($value["marzo_solicitado"] == 0 || $value["marzo_solicitado"] == NULL ? '0.00' : $this->cart->format_number(round($value["marzo_solicitado"], 2))) . '</td>';
            $tabla_estructura .= '<td align="left" >$</td>';
            $tabla_estructura .= '<td align="right" style="border-right: 1px solid #BDBDBD;" >' . ($value["marzo_aprobado"] == 0 || $value["marzo_aprobado"] == NULL ? '0.00' : $this->cart->format_number(round($value["marzo_aprobado"], 2))) . '</td>';

            $tabla_estructura .= '<td align="left" >$</td>';
            $tabla_estructura .= '<td align="right" style="border-right: 1px solid #BDBDBD;" >' . ($value["abril"] == 0 || $value["abril"] == NULL ? '0.00' : $this->cart->format_number(round($value["abril"], 2))) . '</td>';
            $tabla_estructura .= '<td align="left" >$</td>';
            $tabla_estructura .= '<td align="right" style="border-right: 1px solid #BDBDBD;" >' . ($value["abril_fondos"] == 0 || $value["abril_fondos"] == NULL ? '0.00' : $this->cart->format_number(round($value["abril_fondos"], 2))) . '</td>';
            $tabla_estructura .= '<td align="left" >$</td>';
            $tabla_estructura .= '<td align="right" style="border-right: 1px solid #BDBDBD;" >' . ($value["abril_solicitado"] == 0 || $value["abril_solicitado"] == NULL ? '0.00' : $this->cart->format_number(round($value["abril_solicitado"], 2))) . '</td>';
            $tabla_estructura .= '<td align="left" >$</td>';
            $tabla_estructura .= '<td align="right" style="border-right: 1px solid #BDBDBD;" >' . ($value["abril_aprobado"] == 0 || $value["abril_aprobado"] == NULL ? '0.00' : $this->cart->format_number(round($value["abril_aprobado"], 2))) . '</td>';

            $tabla_estructura .= '<td align="left" >$</td>';
            $tabla_estructura .= '<td align="right" style="border-right: 1px solid #BDBDBD;" >' . ($value["mayo"] == 0 || $value["mayo"] == NULL ? '0.00' : $this->cart->format_number(round($value["mayo"], 2))) . '</td>';
            $tabla_estructura .= '<td align="left" >$</td>';
            $tabla_estructura .= '<td align="right" style="border-right: 1px solid #BDBDBD;" >' . ($value["mayo_fondos"] == 0 || $value["mayo_fondos"] == NULL ? '0.00' : $this->cart->format_number(round($value["mayo_fondos"], 2))) . '</td>';
            $tabla_estructura .= '<td align="left" >$</td>';
            $tabla_estructura .= '<td align="right" style="border-right: 1px solid #BDBDBD;" >' . ($value["mayo_solicitado"] == 0 || $value["mayo_solicitado"] == NULL ? '0.00' : $this->cart->format_number(round($value["mayo_solicitado"], 2))) . '</td>';
            $tabla_estructura .= '<td align="left" >$</td>';
            $tabla_estructura .= '<td align="right" style="border-right: 1px solid #BDBDBD;" >' . ($value["mayo_aprobado"] == 0 || $value["mayo_aprobado"] == NULL ? '0.00' : $this->cart->format_number(round($value["mayo_aprobado"], 2))) . '</td>';

            $tabla_estructura .= '<td align="left" >$</td>';
            $tabla_estructura .= '<td align="right" style="border-right: 1px solid #BDBDBD;" >' . ($value["junio"] == 0 || $value["junio"] == NULL ? '0.00' : $this->cart->format_number(round($value["junio"], 2))) . '</td>';
            $tabla_estructura .= '<td align="left" >$</td>';
            $tabla_estructura .= '<td align="right" style="border-right: 1px solid #BDBDBD;" >' . ($value["junio_fondos"] == 0 || $value["junio_fondos"] == NULL ? '0.00' : $this->cart->format_number(round($value["junio_fondos"], 2))) . '</td>';
            $tabla_estructura .= '<td align="left" >$</td>';
            $tabla_estructura .= '<td align="right" style="border-right: 1px solid #BDBDBD;" >' . ($value["junio_solicitado"] == 0 || $value["junio_solicitado"] == NULL ? '0.00' : $this->cart->format_number(round($value["junio_solicitado"], 2))) . '</td>';
            $tabla_estructura .= '<td align="left" >$</td>';
            $tabla_estructura .= '<td align="right" style="border-right: 1px solid #BDBDBD;" >' . ($value["junio_aprobado"] == 0 || $value["junio_aprobado"] == NULL ? '0.00' : $this->cart->format_number(round($value["junio_aprobado"], 2))) . '</td>';

            $tabla_estructura .= '<td align="left" >$</td>';
            $tabla_estructura .= '<td align="right" style="border-right: 1px solid #BDBDBD;" >' . ($value["julio"] == 0 || $value["julio"] == NULL ? '0.00' : $this->cart->format_number(round($value["julio"], 2))) . '</td>';
            $tabla_estructura .= '<td align="left" >$</td>';
            $tabla_estructura .= '<td align="right" style="border-right: 1px solid #BDBDBD;" >' . ($value["julio_fondos"] == 0 || $value["julio_fondos"] == NULL ? '0.00' : $this->cart->format_number(round($value["julio_fondos"], 2))) . '</td>';
            $tabla_estructura .= '<td align="left" >$</td>';
            $tabla_estructura .= '<td align="right" style="border-right: 1px solid #BDBDBD;" >' . ($value["julio_solicitado"] == 0 || $value["julio_solicitado"] == NULL ? '0.00' : $this->cart->format_number(round($value["julio_solicitado"], 2))) . '</td>';
            $tabla_estructura .= '<td align="left" >$</td>';
            $tabla_estructura .= '<td align="right" style="border-right: 1px solid #BDBDBD;" >' . ($value["julio_aprobado"] == 0 || $value["julio_aprobado"] == NULL ? '0.00' : $this->cart->format_number(round($value["julio_aprobado"], 2))) . '</td>';

            $tabla_estructura .= '<td align="left" >$</td>';
            $tabla_estructura .= '<td align="right" style="border-right: 1px solid #BDBDBD;" >' . ($value["agosto"] == 0 || $value["agosto"] == NULL ? '0.00' : $this->cart->format_number(round($value["agosto"], 2))) . '</td>';
            $tabla_estructura .= '<td align="left" >$</td>';
            $tabla_estructura .= '<td align="right" style="border-right: 1px solid #BDBDBD;" >' . ($value["agosto_fondos"] == 0 || $value["agosto_fondos"] == NULL ? '0.00' : $this->cart->format_number(round($value["agosto_fondos"], 2))) . '</td>';
            $tabla_estructura .= '<td align="left" >$</td>';
            $tabla_estructura .= '<td align="right" style="border-right: 1px solid #BDBDBD;" >' . ($value["agosto_solicitado"] == 0 || $value["agosto_solicitado"] == NULL ? '0.00' : $this->cart->format_number(round($value["agosto_solicitado"], 2))) . '</td>';
            $tabla_estructura .= '<td align="left" >$</td>';
            $tabla_estructura .= '<td align="right" style="border-right: 1px solid #BDBDBD;" >' . ($value["agosto_aprobado"] == 0 || $value["agosto_aprobado"] == NULL ? '0.00' : $this->cart->format_number(round($value["agosto_aprobado"], 2))) . '</td>';

            $tabla_estructura .= '<td align="left" >$</td>';
            $tabla_estructura .= '<td align="right" style="border-right: 1px solid #BDBDBD;" >' . ($value["septiembre"] == 0 || $value["septiembre"] == NULL ? '0.00' : $this->cart->format_number(round($value["septiembre"], 2))) . '</td>';
            $tabla_estructura .= '<td align="left" >$</td>';
            $tabla_estructura .= '<td align="right" style="border-right: 1px solid #BDBDBD;" >' . ($value["septiembre_fondos"] == 0 || $value["septiembre_fondos"] == NULL ? '0.00' : $this->cart->format_number(round($value["septiembre_fondos"], 2))) . '</td>';
            $tabla_estructura .= '<td align="left" >$</td>';
            $tabla_estructura .= '<td align="right" style="border-right: 1px solid #BDBDBD;" >' . ($value["septiembre_solicitado"] == 0 || $value["septiembre_solicitado"] == NULL ? '0.00' : $this->cart->format_number(round($value["septiembre_solicitado"], 2))) . '</td>';
            $tabla_estructura .= '<td align="left" >$</td>';
            $tabla_estructura .= '<td align="right" style="border-right: 1px solid #BDBDBD;" >' . ($value["septiembre_aprobado"] == 0 || $value["septiembre_aprobado"] == NULL ? '0.00' : $this->cart->format_number(round($value["septiembre_aprobado"], 2))) . '</td>';

            $tabla_estructura .= '<td align="left" >$</td>';
            $tabla_estructura .= '<td align="right" style="border-right: 1px solid #BDBDBD;" >' . ($value["octubre"] == 0 || $value["octubre"] == NULL ? '0.00' : $this->cart->format_number(round($value["octubre"], 2))) . '</td>';
            $tabla_estructura .= '<td align="left" >$</td>';
            $tabla_estructura .= '<td align="right" style="border-right: 1px solid #BDBDBD;" >' . ($value["octubre_fondos"] == 0 || $value["octubre_fondos"] == NULL ? '0.00' : $this->cart->format_number(round($value["octubre_fondos"], 2))) . '</td>';
            $tabla_estructura .= '<td align="left" >$</td>';
            $tabla_estructura .= '<td align="right" style="border-right: 1px solid #BDBDBD;" >' . ($value["octubre_solicitado"] == 0 || $value["octubre_solicitado"] == NULL ? '0.00' : $this->cart->format_number(round($value["octubre_solicitado"], 2))) . '</td>';
            $tabla_estructura .= '<td align="left" >$</td>';
            $tabla_estructura .= '<td align="right" style="border-right: 1px solid #BDBDBD;" >' . ($value["octubre_aprobado"] == 0 || $value["octubre_aprobado"] == NULL ? '0.00' : $this->cart->format_number(round($value["octubre_aprobado"], 2))) . '</td>';

            $tabla_estructura .= '<td align="left" >$</td>';
            $tabla_estructura .= '<td align="right" style="border-right: 1px solid #BDBDBD;" >' . ($value["noviembre"] == 0 || $value["noviembre"] == NULL ? '0.00' : $this->cart->format_number(round($value["noviembre"], 2))) . '</td>';
            $tabla_estructura .= '<td align="left" >$</td>';
            $tabla_estructura .= '<td align="right" style="border-right: 1px solid #BDBDBD;" >' . ($value["noviembre_fondos"] == 0 || $value["noviembre_fondos"] == NULL ? '0.00' : $this->cart->format_number(round($value["noviembre_fondos"], 2))) . '</td>';
            $tabla_estructura .= '<td align="left" >$</td>';
            $tabla_estructura .= '<td align="right" style="border-right: 1px solid #BDBDBD;" >' . ($value["noviembre_solicitado"] == 0 || $value["noviembre_solicitado"] == NULL ? '0.00' : $this->cart->format_number(round($value["noviembre_solicitado"], 2))) . '</td>';
            $tabla_estructura .= '<td align="left" >$</td>';
            $tabla_estructura .= '<td align="right" style="border-right: 1px solid #BDBDBD;" >' . ($value["noviembre_aprobado"] == 0 || $value["noviembre_aprobado"] == NULL ? '0.00' : $this->cart->format_number(round($value["noviembre_aprobado"], 2))) . '</td>';

            $tabla_estructura .= '<td align="left" >$</td>';
            $tabla_estructura .= '<td align="right" style="border-right: 1px solid #BDBDBD;" >' . ($value["diciembre"] == 0 || $value["diciembre"] == NULL ? '0.00' : $this->cart->format_number(round($value["diciembre"], 2))) . '</td>';
            $tabla_estructura .= '<td align="left" >$</td>';
            $tabla_estructura .= '<td align="right" style="border-right: 1px solid #BDBDBD;" >' . ($value["diciembre_fondos"] == 0 || $value["diciembre_fondos"] == NULL ? '0.00' : $this->cart->format_number(round($value["diciembre_fondos"], 2))) . '</td>';
            $tabla_estructura .= '<td align="left" >$</td>';
            $tabla_estructura .= '<td align="right" style="border-right: 1px solid #BDBDBD;" >' . ($value["diciembre_solicitado"] == 0 || $value["diciembre_solicitado"] == NULL ? '0.00' : $this->cart->format_number(round($value["diciembre_solicitado"], 2))) . '</td>';
            $tabla_estructura .= '<td align="left" >$</td>';
            $tabla_estructura .= '<td align="right" style="border-right: 1px solid #BDBDBD;" >' . ($value["diciembre_aprobado"] == 0 || $value["diciembre_aprobado"] == NULL ? '0.00' : $this->cart->format_number(round($value["diciembre_aprobado"], 2))) . '</td>';

            $tabla_estructura .= '</tr>';
        }
        $tabla_estructura .= '</table>';

        $html = '
        <style>
            .cont-general{
                border: 1px solid #eee;
                border-radius: 1%;
                margin: 2% 14%;
            }
            .cont-general2{
                border: 1px solid #BDBDBD;
            }
            .cont-general .borde-inf{
                border-bottom: 1px solid #eee;
            }
            .cont-general .borde-sup{
                border-top: 1px solid #eee;
            }
        </style>

        <table class="cont-general" border="0" cellspacing="3" style="font-size: small;" cellpadding="2">
            <tr cellspacing="10">
                <th width="2%"></th>
                <th align="left" width="20%"><img src="' . base_url("img/logo2.jpg") . '" /></th>
                <th align="center" width="76%" style="line-height: 7px;">
                    <h3>SECRETAR�A DE SALUD MICHOAC�N</h3>
                    <h5>Estado Auxiliar de Egresos</h5>
                </th>
                <th width="2%"></th>
            </tr>
            <tr>
                <td width="99%" style="font-size:10px;">' . $tabla_estructura . '</td>
            </tr>

            <tr><td class="borde-inf"  width="100%"></td></tr>
            <tr><td width="101%"></td></tr>
            <tr><td width="100%"></td></tr>

</table>';

// output the HTML content

        $pdf->writeHTML($html, false, false, true, false, 'top');

//Close and output PDF document
        $pdf->Output('Presupuesto Aprobado.pdf', 'I');

    }

    function cerrar_aprobado()
    {
//        Se inicializa la variable que contiene la respuesta
        $respuesta = array();

        $this->db->select('cerrado')->from('anteproyecto_cerrado');
        $query = $this->db->get();
        $anteproyecto_cerrado = $query->row_array();
        echo(json_encode($anteproyecto_cerrado["cerrado"]));
    }

    function cerrar_presupuesto_aprobado() {
        $data = array(
            'cerrado' => 1,
        );

        $query = $this->db->update('anteproyecto_cerrado', $data);
        echo(json_encode($query));

    }

    function abrir_presupuesto_aprobado()
    {
        $data = array(
            'cerrado' => 0,
        );

        $query = $this->db->update('anteproyecto_cerrado', $data);
        echo(json_encode($query));

    }

    function servicios_subrogados()
    {
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Servicios Subrogados",
            "usuario" => $this->tank_auth->get_username(),
            "nuevas_tablas" => TRUE,
            "nuevas_tablas_editor" => TRUE,
            "nuevas_tablas_buttons" => TRUE,
            "nuevas_tablas_select" => TRUE,
            "nuevas_tablas_keytable" => TRUE,
            "servicios_subrogados" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/anteproyecto/servicios_subrogados_view');
        $this->load->view('front/footer_main_view');
    }

    function tabla_servicios_subrogados()
    {
        include("./assets/nuevo_datatables/extensions/Editor/php/DataTables.php");

        DataTables\Editor::inst($db, 'servicios_subrogados')
            ->pkey('id_subrogado')
            ->fields(
                DataTables\Editor\Field::inst('centro_costos')->validator('DataTables\Editor\Validate::notEmpty'),
                DataTables\Editor\Field::inst('descripcion_centro_costos')->validator('DataTables\Editor\Validate::notEmpty'),
                DataTables\Editor\Field::inst('partida')->validator('DataTables\Editor\Validate::notEmpty'),
                DataTables\Editor\Field::inst('descripcion_partida')->validator('DataTables\Editor\Validate::notEmpty'),
                DataTables\Editor\Field::inst('enero')->validator('DataTables\Editor\Validate::numeric'),
                DataTables\Editor\Field::inst('febrero')->validator('DataTables\Editor\Validate::numeric'),
                DataTables\Editor\Field::inst('marzo')->validator('DataTables\Editor\Validate::numeric'),
                DataTables\Editor\Field::inst('abril')->validator('DataTables\Editor\Validate::numeric'),
                DataTables\Editor\Field::inst('mayo')->validator('DataTables\Editor\Validate::numeric'),
                DataTables\Editor\Field::inst('junio')->validator('DataTables\Editor\Validate::numeric'),
                DataTables\Editor\Field::inst('julio')->validator('DataTables\Editor\Validate::numeric'),
                DataTables\Editor\Field::inst('agosto')->validator('DataTables\Editor\Validate::numeric'),
                DataTables\Editor\Field::inst('septiembre')->validator('DataTables\Editor\Validate::numeric'),
                DataTables\Editor\Field::inst('octubre')->validator('DataTables\Editor\Validate::numeric'),
                DataTables\Editor\Field::inst('noviembre')->validator('DataTables\Editor\Validate::numeric'),
                DataTables\Editor\Field::inst('diciembre')->validator('DataTables\Editor\Validate::numeric'),
                DataTables\Editor\Field::inst('total')->validator('DataTables\Editor\Validate::numeric')

            )
            ->process($_POST)
            ->json();

//        $this->debugeo->imprimir_pre($resultado);

    }

    function exportar_subrogados_excel() {

        $query = 'SELECT * FROM servicios_subrogados;';
        $resultado = $this->anteproyecto_model->get_datos_meses_aprobados_busqueda($query);

        //$this->debugeo->imprimir_pre($resultado);

        //$this->benchmark->mark('code_start');

        //$this->debugeo->imprimir_pre($datos);

        //$this->load->library('excel');

        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Servicios Subrogados');


        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
            'font' => array(
                'bold' => true,
            )
        );
        $bold = array(
            'font' => array(
                'bold' => true,
            )
        );
        $style_center = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );
        $style_left = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            )
        );
        $style_header = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'F0FEFC')
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );

        $this->excel->getActiveSheet()->mergeCells('A1:M1')->getStyle("A1")->applyFromArray($style);
        $this->excel->getActiveSheet()->setCellValue('A1', 'SECRETARIA DE SALUD MICHOACAN');
        $this->excel->getActiveSheet()->mergeCells('A2:M2')->getStyle("A2")->applyFromArray($style);
        $this->excel->getActiveSheet()->setCellValue('A2', 'Servicios Subrogados');

        //set cell A1 content with some text

        $this->excel->getActiveSheet()->setCellValue('A6', 'Centro de Costos')->getColumnDimension("A")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('B6', 'Descripcion CC')->getColumnDimension("B")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('C6', 'Partida')->getColumnDimension("C")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('D6', 'Descripcion Partida')->getColumnDimension("D")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('E6', 'Enero')->getColumnDimension("E")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('F6', 'Febrero')->getColumnDimension("F")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('G6', 'Marzo')->getColumnDimension("G")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('H6', 'Abril')->getColumnDimension("H")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('I6', 'Mayo')->getColumnDimension("I")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('J6', 'Junio')->getColumnDimension("J")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('K6', 'Julio')->getColumnDimension("K")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('L6', 'Agosto')->getColumnDimension("L")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('M6', 'Septiembre')->getColumnDimension("M")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('N6', 'Octubre')->getColumnDimension("N")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('O6', 'Noviembre')->getColumnDimension("O")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('P6', 'Diciembre')->getColumnDimension("P")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('Q6', 'Total')->getColumnDimension("Q")->setWidth(20);

        $this->excel->getActiveSheet()->setCellValue('A6', 'Centro de Costos')->getStyle('A6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('B6', 'Descripcion CC')->getStyle('B6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('C6', 'Partida')->getStyle('C6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('D6', 'Descripcion Partida')->getStyle('D6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('E6', 'Enero')->getStyle('E6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('F6', 'Febrero')->getStyle('F6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('G6', 'Marzo')->getStyle('G6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('H6', 'Abril')->getStyle('H6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('I6', 'Mayo')->getStyle('I6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('J6', 'Junio')->getStyle('J6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('K6', 'Julio')->getStyle('K6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('L6', 'Agosto')->getStyle('L6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('M6', 'Septiembre')->getStyle('M6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('N6', 'Octubre')->getStyle('N6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('O6', 'Noviembre')->getStyle('O6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('P6', 'Diciembre')->getStyle('P6')->applyFromArray($style_header);
        $this->excel->getActiveSheet()->setCellValue('Q6', 'Total')->getStyle('Q6')->applyFromArray($style_header);

        /*    $arreglo_like = array('partida' => $datos["partida"], 'centro_costos' => $datos["centro_costos"]);
            $this->db->select('*')->from('servicios_subrogados')->like($arreglo_like);
            $query = $this->db->get();
            $resultado = $query->result_array(); */

        // $this->debugeo->imprimir_pre($resultado);

        $row = 7;

        foreach ($resultado as $value) {
            //  $this->debugeo->imprimir_pre($value);

            $this->excel->getActiveSheet()->setCellValue('A' . $row, $value["centro_costos"])->getStyle('A' . $row)->applyFromArray($style_center);
            $this->excel->getActiveSheet()->setCellValue('B' . $row, $value["descripcion_centro_costos"])->getStyle('B' . $row)->applyFromArray($style_center);
            $this->excel->getActiveSheet()->setCellValue('C' . $row, $value["partida"])->getStyle('C' . $row)->applyFromArray($style_center);
            $this->excel->getActiveSheet()->setCellValue('D' . $row, $value["descripcion_partida"])->getStyle('D' . $row)->applyFromArray($style_center);
            $this->excel->getActiveSheet()->setCellValue('E' . $row, '$ ' . ($value["enero"] == 0 || $value["enero"] == NULL ? '0.00' : $this->cart->format_number($value["enero"], 2)))->getStyle('E' . $row)->applyFromArray($style_center);
            $this->excel->getActiveSheet()->setCellValue('F' . $row, '$ ' . ($value["febrero"] == 0 || $value["febrero"] == NULL ? '0.00' : $this->cart->format_number($value["febrero"], 2)))->getStyle('F' . $row)->applyFromArray($style_center);
            $this->excel->getActiveSheet()->setCellValue('G' . $row, '$ ' . ($value["marzo"] == 0 || $value["marzo"] == NULL ? '0.00' : $this->cart->format_number($value["marzo"], 2)))->getStyle('G' . $row)->applyFromArray($style_center);
            $this->excel->getActiveSheet()->setCellValue('H' . $row, '$ ' . ($value["abril"] == 0 || $value["abril"] == NULL ? '0.00' : $this->cart->format_number($value["abril"], 2)))->getStyle('H' . $row)->applyFromArray($style_center);
            $this->excel->getActiveSheet()->setCellValue('I' . $row, '$ ' . ($value["mayo"] == 0 || $value["mayo"] == NULL ? '0.00' : $this->cart->format_number($value["mayo"], 2)))->getStyle('I' . $row)->applyFromArray($style_center);
            $this->excel->getActiveSheet()->setCellValue('J' . $row, '$ ' . ($value["junio"] == 0 || $value["junio"] == NULL ? '0.00' : $this->cart->format_number($value["junio"], 2)))->getStyle('J' . $row)->applyFromArray($style_center);
            $this->excel->getActiveSheet()->setCellValue('K' . $row, '$ ' . ($value["julio"] == 0 || $value["julio"] == NULL ? '0.00' : $this->cart->format_number($value["julio"], 2)))->getStyle('K' . $row)->applyFromArray($style_center);
            $this->excel->getActiveSheet()->setCellValue('L' . $row, '$ ' . ($value["agosto"] == 0 || $value["agosto"] == NULL ? '0.00' : $this->cart->format_number($value["agosto"], 2)))->getStyle('L' . $row)->applyFromArray($style_center);
            $this->excel->getActiveSheet()->setCellValue('M' . $row, '$ ' . ($value["septiembre"] == 0 || $value["septiembre"] == NULL ? '0.00' : $this->cart->format_number($value["septiembre"], 2)))->getStyle('M' . $row)->applyFromArray($style_center);
            $this->excel->getActiveSheet()->setCellValue('N' . $row, '$ ' . ($value["octubre"] == 0 || $value["octubre"] == NULL ? '0.00' : $this->cart->format_number($value["octubre"], 2)))->getStyle('N' . $row)->applyFromArray($style_center);
            $this->excel->getActiveSheet()->setCellValue('O' . $row, '$ ' . ($value["noviembre"] == 0 || $value["noviembre"] == NULL ? '0.00' : $this->cart->format_number($value["noviembre"], 2)))->getStyle('O' . $row)->applyFromArray($style_center);
            $this->excel->getActiveSheet()->setCellValue('P' . $row, '$ ' . ($value["diciembre"] == 0 || $value["diciembre"] == NULL ? '0.00' : $this->cart->format_number($value["diciembre"], 2)))->getStyle('P' . $row)->applyFromArray($style_center);
            $this->excel->getActiveSheet()->setCellValue('Q' . $row, '$ ' . ($value["total"] == 0 || $value["total"] == NULL ? '0.00' : $this->cart->format_->applyFronumber($value["total"], 2)))->getStyle('Q' . $row)->applyFromArray($style_center);

            $row += 1;

        }

        //$this->benchmark->mark('code_end');

        //echo $this->benchmark->elapsed_time('code_start', 'code_end');

        $filename = "Servicios Subrogados.xls"; // Agregar fecha en que se gener�
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
        //        save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //        if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //        force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }

    function centro_partidas()
    {

        $this->db->select('*')->from('centro_costos_anteproyecto');
        $centro_costos_query = $this->db->get();
        $centro_costos = $centro_costos_query->result_array();

        //$this->debugeo->imprimir_pre($centro_costos);

        foreach ($centro_costos as $key => $value) {

            $this->db->select('*')->from('partidas_anteproyecto');
            $partidas_query = $this->db->get();
            $partidas = $partidas_query->result_array();

            foreach ($partidas as $key_partida => $value_partida) {

                $datos = array(
                    'centro_costos' => $value["centro_costos"],
                    'descripcion_centro_costos' => $value["descripcion"],
                    'partida' => $value_partida["partida"],
                    'descripcion_partida' => $value_partida["descripcion"],

                );

                $this->db->insert('servicios_subrogados', $datos);
                //  $this->db->insert('meses_aprobado', $datos);

            }


        }

    }

    function presupuesto_aprobado_busqueda(){

        $query = "SELECT centro_costos, descripcion_centro_costos, partida, descripcion_partida, ";

        $datos_header = array(
            "titulo_pagina" => "Armonniza | Busqueda",
            "usuario" => $this->tank_auth->get_username(),
            "compromisocss" => TRUE,
            "tablas" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);

        $this->form_validation->set_rules('centro_costos', 'Centro de Costos', 'xss_clean');
        $this->form_validation->set_rules('partida', 'Partida', 'xss_clean');

        $datos = $this->input->post();

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('front/anteproyecto/presupuesto_aprobado_filtro_view', $datos);
        } else {

            $arreglo_meses = array(
                0 => "enero",
                1 => "febrero",
                2 => "marzo",
                3 => "abril",
                4 => "mayo",
                5 => "junio",
                6 => "julio",
                7 => "agosto",
                8 => "septiembre",
                9 => "octubre",
                10 => "noviembre",
                11 => "diciembre",
            );

            $arreglo_tipo_presupueto = array(
                0 => "anio",
                1 => "fondos",
                2 => "solicitado",
                3 => "aprobado",
            );

//            $this->debugeo->imprimir_pre($datos);

            $tabla = '<table class="table table-striped table-bordered table-hover display" id="datos_tabla_busqueda">
                        <thead >
                            <tr>
                                <th style="border:1px dotted #959496; text-align: center;">CC</th>
                                <th style="border:1px dotted #959496; text-align: center;">Descripcion CC</th>
                                <th style="border:1px dotted #959496; text-align: center;">Partida</th>
                                <th style="border:1px dotted #959496; text-align: center;">Descripcion Partida</th>';

            if (isset($datos["tipo_presupuesto"])) {

                foreach ($datos["tipo_presupuesto"] as $key_tp => $value_tp) {

                    if (isset($datos["meses"])) {

                        foreach ($datos["meses"] as $key_m => $value_m) {
                            $query .= $value_m . "_" . $value_tp . ", ";
                            $tabla .= "<th style='border:1px dotted #959496; text-align: center;'>" . ucfirst($value_m) . " " . ucfirst($value_tp) . "</th>";
                        }

                    } else {

                        foreach ($arreglo_meses as $key_m => $value_m) {
                            $query .= $value_m . "_" . $value_tp . ", ";
                            $tabla .= "<th>" . ucfirst($value_m) . " " . ucfirst($value_tp) . "</th>";
                        }

                    }

                }

            } else {
                $query .= "*, ";
            }

            $tabla .= '</tr>
                        </thead>
                        <tbody>';

            $posicion_coma = strrpos($query, ',', -1);
            $pedazo1 = substr($query, 0, $posicion_coma);
            $pedazo2 = substr($query, $posicion_coma + 1, -1);
            $query = $pedazo1 . $pedazo2 . " ";

            $query .= "FROM meses_aprobado ";


            $centro_costos = array();
            $partidas = array();

            if ($datos["centro_costos"] != NULL || $datos["centro_costos"] != "") {

                $centro_costos = explode(",", $datos["centro_costos"]);
                array_pop($centro_costos);

                foreach ($centro_costos as $key_cc => $value_cc) {
                    if (strpos($query, "WHERE") !== false) {
                        $query .= "OR centro_costos = '" . $value_cc . "' ";
                    } else {
                        $query .= "WHERE (centro_costos = '" . $value_cc . "' ";
                    }
                }
            } else {

                if (strpos($query, "WHERE") !== false) {
                    $query .= "OR centro_costos LIKE '%%' ";
                } else {
                    $query .= "WHERE (centro_costos LIKE '%%' ";
                }

            }

            $query .= ") ";

            $check_parentesis = TRUE;

            if ($datos["partida"] != NULL || $datos["partida"] != "") {

                $partidas = explode(",", $datos["partida"]);
                array_pop($partidas);

                foreach ($partidas as $key_p => $value_p) {

                    if ($check_parentesis) {
                        if (strpos($query, ")") !== false) {
                            $query .= "AND ( partida = '" . $value_p . "' ";
                        }

                        $check_parentesis = FALSE;
                    }

                    if (strpos($query, "WHERE") !== false) {
                        $query .= "OR partida = '" . $value_p . "' ";
                    } else {
                        $query .= "WHERE (partida = '" . $value_p . "' ";
                    }
                }
            } else {

                if (strpos($query, "WHERE") !== false) {
                    $query .= "OR partida LIKE '%%' ";
                } else {
                    $query .= "WHERE (partida LIKE '%%' ";
                }
            }

            $query .= ") ";

            $query .= ";";

            $resultado = $this->anteproyecto_model->get_datos_meses_aprobados_busqueda($query);

            unset($key_tp);
            unset($value_tp);
            unset($key_m);
            unset($value_m);

            foreach ($resultado as $key => $value) {

                $tabla .= "<tr>";
                $tabla .= "<td style='text-align: center;'>" . $value["centro_costos"] . "</td>";
                $tabla .= "<td>" . $value["descripcion_centro_costos"] . "</td>";
                $tabla .= "<td style='text-align: center;'>" . $value["partida"] . "</td>";
                $tabla .= "<td>" . $value["descripcion_partida"] . "</td>";

                foreach ($arreglo_tipo_presupueto as $key_tp => $value_tp) {

                    foreach ($arreglo_meses as $key_m => $value_m) {

                        if (isset($value[$value_m . "_" . $value_tp])) {
                            $tabla .= "<td style='text-align: center;'>" . $value[$value_m . "_" . $value_tp] . "";
                        } else {
                            continue;
                        }
                    }

                }

                $tabla .= "</tr>";
//                $this->debugeo->imprimir_pre($value);
            }

            $tabla .= "</tbody>
                    </table>";

//            $this->debugeo->imprimir_pre($tabla);

            $datos["tabla_lista"] = $tabla;
            $datos["query"] = $query;

            $this->load->view('front/anteproyecto/presupuesto_aprobado_filtro_view', $datos);
        }

        $this->load->view('front/footer_main_view', array(
            "presupuesto_busqueda" => TRUE,
        ));

    }

    function tabla_cc_busqueda()
    {
//        Se toman todos los datos de la caratula de precompromisos
        $this->db->select('centro_costos, descripcion')->from('centro_costos_anteproyecto');
        $query = $this->db->get();
        $resultado = $query->result_array();

//        Se crea el arreglo de datos donde se va a guardar la informaci�n
        $output = array("data" => "");

//        Se recorre el arreglo fila por fila
        foreach ($resultado as $key => $value) {

            $output["data"][] = array(
                $value["centro_costos"],
                $value["descripcion"],
                '<input type="checkbox" class="check_centro_costos" />'
            );
        }

        echo json_encode($output);
    }

    function tabla_partidas_busqueda()
    {
//        Se toman todos los datos de la caratula de precompromisos
        $this->db->select('partida, descripcion')->from('partidas_anteproyecto');
        $query = $this->db->get();
        $resultado = $query->result_array();

//        Se crea el arreglo de datos donde se va a guardar la informaci�n
        $output = array("data" => "");

//        Se recorre el arreglo fila por fila
        foreach ($resultado as $key => $value) {

            $output["data"][] = array(
                $value["partida"],
                $value["descripcion"],
                '<input type="checkbox" name="checkbox" value="1">',
            );
        }

        echo json_encode($output);
    }

    function exportar_busqueda_excel() {

        $datos = $this->input->post();

        //    $this->debugeo->imprimir_pre($datos["jquery_tabla"]);

        $query = $datos["jquery_tabla"];
        $resultado = $this->anteproyecto_model->get_datos_meses_aprobados_busqueda($query);

        //    $this->debugeo->imprimir_pre($resultado);


        //$this->benchmark->mark('code_start');


        //$this->load->library('excel');

        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Reporte Aprobado');


        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
            'font' => array(
                'bold' => true,
            )
        );

        $style_color1 = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'EBCAF0')
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );
        $style_color2 = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'F8ECE1')
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );

        $arreglo_header = array(
            0 => "A",
            1 => "B",
            2 => "C",
            3 => "D",
            4 => "E",
            5 => "F",
            6 => "G",
            7 => "H",
            8 => "I",
            9 => "J",
            10 => "K",
            11 => "L",
            12 => "M",
            13 => "N",
            14 => "O",
            15 => "P",
            16 => "Q",
            17 => "R",
            18 => "S",
            19 => "T",
            20 => "U",
            21 => "V",
            22 => "W",
            23 => "X",
            24 => "Y",
            25 => "Z",
            26 => "AA",
            27 => "AB",
            28 => "AC",
            29 => "AD",
            30 => "AE",
            31 => "AF",
            32 => "AG",
            33 => "AH",
            34 => "AI",
            35 => "AJ",
            36 => "AK",
            37 => "AL",
            38 => "AM",
            39 => "AN",
            40 => "AO",
            41 => "AP",
            42 => "AQ",
            43 => "AR",
            44 => "AS",
            45 => "AT",
            46 => "AU",
            47 => "AV",
            48 => "AW",
            49 => "AX",
            50 => "AY",
            51 => "AZ",
        );

        $posicion_header = 0;

        $this->excel->getActiveSheet()->mergeCells('A2:E2')->getStyle("A2")->applyFromArray($style);
        $this->excel->getActiveSheet()->setCellValue('A2', 'SECRETARIA DE SALUD MICHOACAN');
        $this->excel->getActiveSheet()->mergeCells('B4:D4')->getStyle("B4")->applyFromArray($style);
        $this->excel->getActiveSheet()->setCellValue('B4', 'Reporte de Aprobado');


        $index_from = strpos($query, "F");
        //  $this->debugeo->imprimir_pre($index_from);
        $subcadena = substr($query, 6, $index_from - 6);
        $subcadena = trim($subcadena);
        $resultado_cadena = explode(",", $subcadena);
             //$this->debugeo->imprimir_pre($resultado_cadena);

            foreach ($resultado_cadena as $key_header => $value_header) {
                //$this->debugeo->imprimir_pre($arreglo_header[$posicion_header]);
                //$this->debugeo->imprimir_pre($value_header);


              $this->excel->getActiveSheet()->setCellValue($arreglo_header[$posicion_header]."6", "".$value_header);

                $posicion_header += 1;
            }



        $row = 7;

        foreach ($resultado as $key => $value) {
            //    $this->debugeo->imprimir_pre($value);

            $this->excel->getActiveSheet()->setCellValue('A' . $row, $value["centro_costos"])->getStyle('A' . $row);
            $this->excel->getActiveSheet()->setCellValue('B' . $row, $value["descripcion_centro_costos"])->getStyle('B' . $row);
            $this->excel->getActiveSheet()->setCellValue('C' . $row, $value["partida"])->getStyle('C' . $row);
            $this->excel->getActiveSheet()->setCellValue('D' . $row, $value["descripcion_partida"])->getStyle('D' . $row);

            $meses = array(
                0 => "enero",
                1 => "febrero",
                2 => "marzo",
                3 => "abril",
                4 => "mayo",
                5 => "junio",
                6 => "julio",
                7 => "agosto",
                8 => "septiembre",
                9 => "octubre",
                10 => "noviembre",
                11 => "diciembre",
            );

            $tipo_presupuesto = array(
                0 => "anio",
                1 => "fondos",
                2 => "solicitado",
                3 => "aprobado",

            );

            $arreglo_meses = array(
                0 => "E",
                1 => "F",
                2 => "G",
                3 => "H",
                4 => "I",
                5 => "J",
                6 => "K",
                7 => "L",
                8 => "M",
                9 => "N",
                10 => "O",
                11 => "P",
                12 => "Q",
                13 => "R",
                14 => "S",
                15 => "T",
                16 => "U",
                17 => "V",
                18 => "W",
                19 => "X",
                20 => "Y",
                21 => "Z",
                22 => "AA",
                23 => "AB",
                24 => "AC",
                25 => "AD",
                26 => "AE",
                27 => "AF",
                28 => "AG",
                29 => "AH",
                30 => "AI",
                31 => "AJ",
                32 => "AK",
                33 => "AL",
                34 => "AM",
                35 => "AN",
                36 => "AO",
                37 => "AP",
                38 => "AQ",
                39 => "AR",
                40 => "AS",
                41 => "AT",
                42 => "AU",
                43 => "AV",
                44 => "AW",
                45 => "AX",
                46 => "AY",
                47 => "AZ",
            );

            $posicion = 0;


            foreach ($meses as $key_meses => $value_meses) {

                foreach ($tipo_presupuesto as $key_tipo => $value_tipo) {

                    if (isset($value[$value_meses . "_" . $value_tipo])) {
                        //$this->debugeo->imprimir_pre($value);
                        //$this->debugeo->imprimir_pre($arreglo_meses[$posicion]."".$row);

                        $this->excel->getActiveSheet()->setCellValue($arreglo_meses[$posicion]."".$row, $value[$value_meses . "_" . $value_tipo]);

                        $posicion += 1;

                    } else {
                        continue;
                    }
                }
            }
            $row += 1;
        }



        $filename="Reporte Aprobado.xls"; // Agregar fecha en que se gener�
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
//        save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
//        if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
//        force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');

    }

    function actualizar_cc () {

        $this->db->select('centro_costos, descripcion')->from('centro_costos_anteproyecto');
        $centro_costos_query = $this->db->get();
        $centro_costos = $centro_costos_query->result_array();

        //$this->debugeo->imprimir_pre($centro_costos);

        foreach ($centro_costos as $key => $value) {

            $data = array(
                'descripcion_centro_costos' => $value["descripcion"],
            );

            $this->db->where('centro_costos', $value["centro_costos"]);
            $this->db->update('meses_aprobado', $data);

        }
    }

    function actualizar_partidas () {

        $this->db->select('partida, descripcion')->from('partidas_anteproyecto');
        $partidas_query = $this->db->get();
        $partidas = $partidas_query->result_array();

        //$this->debugeo->imprimir_pre($centro_costos);

        foreach ($partidas as $key => $value) {

            $data = array(
                'descripcion_partida' => $value["descripcion"],
            );

            $this->db->where('partida', $value["partida"]);
            $this->db->update('meses_aprobado', $data);

        }
    }


}

