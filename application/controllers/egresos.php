<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Este controlador es el encargado de realizar las funciones de los egresos del sistema
 */

class Egresos extends CI_Controller {

    /**
     * Esta funcion se encarga de revisar si el usuario esta logueado, si no lo está, lo regresa a la pantalla de login
     */
    function __construct() {
        parent::__construct();
        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        }
    }

    /**
     * Esta funcion se encarga de preparar el encabezado de las paginas, donde se muestra el nombre de usuario y el titulo de la pagina
     * El parametro que necesita es el titulo de la pagina, y devuelve un arreglo con el titulo de la pagina y el nombre de usuario
     * @param $titulo_pagina
     * @return array
     */
    function prep_datos($titulo_pagina) {
        $datos = array(
            "titulo_pagina" => $titulo_pagina,
            "usuario" => $this->tank_auth->get_username(),
        );
        return $datos;
    }

    /**
     * Esta es la funcion base de los egresos, donde se muestra la pantalla principal de los egresos
     */
    function index() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha ingrresado a egresos');
        $datos_header = $this->prep_datos("Armonniza Egresos");

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/main_view');
        $this->load->view('front/footer_main_view');
    }

    /**
     * Esta funcion se encarga de mostrar la pagina de la estructura de los rubros de los egresos
     */
    function estructura_rubros() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha visto la estructura de rubros');
//        Se llama la funcion del modelo de egresos encargado de contar los niveles que existen
        $total_egresos = $this->egresos_model->contar_egresos_elementos();
//        Se llama a la funcion del model de egresos encargada de tomar los nombres delos niveles que existen
        $nombres_egresos = $this->egresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_egresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

        $nombre_primerNivel = $this->egresos_model->datos_primerNivel($nombre[0]);

//        $this->debugeo->imprimir_pre($nombre_primerNivel);

        $niveles = '<div class="row" id="egresos_estrucura">
                        <div class="col-lg-4">
                            <form role="form">
                                <div class="form-group" id="forma_nivel1">
                                    <div class="title-form center">
                                        <h5>Nivel 1</h5>
                                        <label>'.$nombre[0].'</label>
                                    </div>
                                    <input type="text" class="form-control" id="buscar_nivel1" placeholder="Buscar...">
                                    <select multiple class="form-control formas" id="select_nivel1">';
        foreach($nombre_primerNivel as $fila) {
            $niveles .= '<option value="'.$fila->fuente_de_financiamiento.'" title="'.$fila->fuente_de_financiamiento.' - '.$fila->descripcion.'">'.$fila->fuente_de_financiamiento.' - '.$fila->descripcion.'</option>';
        }

        $niveles .= '                </select>
                                </div>
                            </form>
                        </div>';

        $a = 1;
        $num = 2;

        for($i = 1; $i < $total_egresos->conteo; $i++){
            $niveles .= '<div class="col-lg-4">
                            <form role="form">
                                <div class="form-group" id="forma_nivel'.$num.'">
                                    <div class="title-form center">
                                        <h5>Nivel '.$num.'</h5>
                                        <label>'.$nombre[$a].'</label>
                                    </div>
                                    <input type="text" class="form-control" id="buscar_nivel'.$num.'" placeholder="Buscar..." disabled>
                                    <select multiple class="form-control formas" id="select_nivel'.$num.'">
                                    </select>
                                </div>
                            </form>
                        </div>';
            $a++;
            $num++;
        }

        $niveles .= '</div>';

//        Se llama a la funcion que se encarga de preparar los datos para el encabezado de la pagina
        $datos_header = $this->prep_datos("Armonniza Estructura Clasificación por Rubros");

//        Se crea un arreglo con los niveles existentes de la tabla de egresos
        $datos_vista = array(
            "niveles" => $niveles,
            "nombres_egresos" => $nombre,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/egresos_estructura_rubros_view', $datos_vista);
        $this->load->view('front/footer_main_view', array("graficas" => TRUE, "efectos" => TRUE));
    }

    /**
     * Aqui empieza la sección de Adecuaciones presupuestarias
     */

    /**
     * Esta funcion se encarga de hacer la adecuaciones presupuestarias para los egresos
     */
    function adecuaciones_presupuestarias() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha hecho una adecuación presupuestaria');
        $datos_header = $this->prep_datos("Armonniza Adecuaciones Presupuestarias");

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/egresos_adecuacion_view');
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "adecuaciones_egresos" => TRUE,
        ));
    }

    function tabla_indice_adecuaciones() {
        $resultado = $this->egresos_model->get_datos_adecuaciones();

//        $this->debugeo->imprimir_pre($resultado);

        $output = array("data" => "");
        foreach($resultado as $fila) {

            $firme = '';
            $destino = '';

            if($fila->enfirme == 0) {
                $firme = '<button type="button" class="btn btn-circle btn-firmec" disabled><i class="fa fa-times"></i></button>';
            }
            elseif($fila->enfirme == 1){
                $firme = '<button type="button" class="btn btn-circle btn-firmea" disabled><i class="fa fa-check"></i></button>';
            }

            if($fila->tipo == "Ampliación") {
                $destino = '_ampliacion';
            } elseif($fila->tipo == "Reducción"){
                $destino = '_reduccion';
            } elseif($fila->tipo == "Transferencia"){
                $destino = '_transferencia';
            }

            if($fila->estatus == "activo") {
                $estatus = "success";
                if($fila->enfirme == 0 || $this->utilerias->get_grupo() == 1) {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_egresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("egresos/ver".$destino."/".$fila->id_adecuaciones_egresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                        <a href="'.base_url("egresos/editar".$destino."/".$fila->id_adecuaciones_egresos_caratula).'" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                         <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>'
                    );
                }
                elseif($fila->enfirme == 1 && $this->utilerias->get_grupo() == 1) {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_egresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("egresos/ver".$destino."/".$fila->id_adecuaciones_egresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                        <a href="'.base_url("egresos/editar".$destino."/".$fila->id_adecuaciones_egresos_caratula).'" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                        <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>'
                    );
                }
                else {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_egresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("egresos/ver".$destino."/".$fila->id_adecuaciones_egresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                        <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>'
                    );

                }

            }
            elseif($fila->estatus == "espera"){
                $estatus = "warning";

                if($fila->enfirme == 0 || $this->utilerias->get_firmas() != NULL) {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_egresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("egresos/ver".$destino."/".$fila->id_adecuaciones_egresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                         <a href="'.base_url("egresos/editar".$destino."/".$fila->id_adecuaciones_egresos_caratula).'" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                         <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>'
                    );
                }
                elseif($fila->enfirme == 1 && $this->utilerias->get_grupo() == 1) {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_egresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("egresos/ver".$destino."/".$fila->id_adecuaciones_egresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                         <a href="'.base_url("egresos/editar".$destino."/".$fila->id_adecuaciones_egresos_caratula).'" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                         <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>'
                    );
                }
                else {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_egresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("egresos/ver".$destino."/".$fila->id_adecuaciones_egresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                        <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>'
                    );

                }
            }
            else{
                $estatus = "danger";

                if($fila->enfirme == 0 || $this->utilerias->get_firmas() != NULL) {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_egresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("egresos/ver".$destino."/".$fila->id_adecuaciones_egresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>'
                    );
                }
                elseif($fila->enfirme == 1 && $this->utilerias->get_grupo() == 1) {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_egresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("egresos/ver".$destino."/".$fila->id_adecuaciones_egresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>'
                    );
                }
                else {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_egresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("egresos/ver".$destino."/".$fila->id_adecuaciones_egresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>'
                    );
                }
            }
        }
//        $this->debugeo->imprimir_pre($output);
        echo json_encode($output);
    }

    function tabla_indice_adecuaciones_ampliaciones() {
        $sql = "SELECT * FROM mov_adecuaciones_egresos_caratula WHERE tipo = 'Ampliación' ORDER BY numero DESC;";
        $resultado = $this->ingresos_model->get_datos_adecuaciones_tipo($sql);

//        $this->debugeo->imprimir_pre($resultado);

        $output = array("data" => "");
        foreach($resultado as $fila) {

            $firme = '';
            $destino = '';

            if($fila->enfirme == 0) {
                $firme = '<button type="button" class="btn btn-circle btn-firmec" disabled><i class="fa fa-times"></i></button>';
            }
            elseif($fila->enfirme == 1){
                $firme = '<button type="button" class="btn btn-circle btn-firmea" disabled><i class="fa fa-check"></i></button>';
            }

            if($fila->tipo == "Ampliación") {
                $destino = '_ampliacion';
            } elseif($fila->tipo == "Reducción"){
                $destino = '_reduccion';
            } elseif($fila->tipo == "Transferencia"){
                $destino = '_transferencia';
            }

            if($fila->estatus == "activo") {
                $estatus = "success";
                if($fila->enfirme == 0 || $this->utilerias->get_grupo() == 1) {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_egresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("egresos/ver".$destino."/".$fila->id_adecuaciones_egresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                        <a href="'.base_url("egresos/editar".$destino."/".$fila->id_adecuaciones_egresos_caratula).'" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                         <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>'
                    );
                }
                elseif($fila->enfirme == 1 && $this->utilerias->get_grupo() == 1) {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_egresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("egresos/ver".$destino."/".$fila->id_adecuaciones_egresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                        <a href="'.base_url("egresos/editar".$destino."/".$fila->id_adecuaciones_egresos_caratula).'" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                        <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>'
                    );
                }
                else {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_egresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("egresos/ver".$destino."/".$fila->id_adecuaciones_egresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                        <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>'
                    );

                }

            }
            elseif($fila->estatus == "espera"){
                $estatus = "warning";

                if($fila->enfirme == 0 || $this->utilerias->get_firmas() != NULL) {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_egresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("egresos/ver".$destino."/".$fila->id_adecuaciones_egresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                         <a href="'.base_url("egresos/editar".$destino."/".$fila->id_adecuaciones_egresos_caratula).'" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                         <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>'
                    );
                }
                elseif($fila->enfirme == 1 && $this->utilerias->get_grupo() == 1) {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_egresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("egresos/ver".$destino."/".$fila->id_adecuaciones_egresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                         <a href="'.base_url("egresos/editar".$destino."/".$fila->id_adecuaciones_egresos_caratula).'" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                         <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>'
                    );
                }
                else {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_egresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("egresos/ver".$destino."/".$fila->id_adecuaciones_egresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                        <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>'
                    );

                }
            }
            else{
                $estatus = "danger";

                if($fila->enfirme == 0 || $this->utilerias->get_firmas() != NULL) {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_egresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("egresos/ver".$destino."/".$fila->id_adecuaciones_egresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>'
                    );
                }
                elseif($fila->enfirme == 1 && $this->utilerias->get_grupo() == 1) {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_egresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("egresos/ver".$destino."/".$fila->id_adecuaciones_egresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>'
                    );
                }
                else {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_egresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("egresos/ver".$destino."/".$fila->id_adecuaciones_egresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>'
                    );
                }
            }
        }
//        $this->debugeo->imprimir_pre($output);
        echo json_encode($output);
    }

    function tabla_indice_adecuaciones_reducciones() {
        $sql = "SELECT * FROM mov_adecuaciones_egresos_caratula WHERE tipo = 'Reducción' ORDER BY numero DESC;";
        $resultado = $this->ingresos_model->get_datos_adecuaciones_tipo($sql);

//        $this->debugeo->imprimir_pre($resultado);

        $output = array("data" => "");
        foreach($resultado as $fila) {

            $firme = '';
            $destino = '';

            if($fila->enfirme == 0) {
                $firme = '<button type="button" class="btn btn-circle btn-firmec" disabled><i class="fa fa-times"></i></button>';
            }
            elseif($fila->enfirme == 1){
                $firme = '<button type="button" class="btn btn-circle btn-firmea" disabled><i class="fa fa-check"></i></button>';
            }

            if($fila->tipo == "Ampliación") {
                $destino = '_ampliacion';
            } elseif($fila->tipo == "Reducción"){
                $destino = '_reduccion';
            } elseif($fila->tipo == "Transferencia"){
                $destino = '_transferencia';
            }

            if($fila->estatus == "activo") {
                $estatus = "success";
                if($fila->enfirme == 0 || $this->utilerias->get_grupo() == 1) {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_egresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("egresos/ver".$destino."/".$fila->id_adecuaciones_egresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                        <a href="'.base_url("egresos/editar".$destino."/".$fila->id_adecuaciones_egresos_caratula).'" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                         <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>'
                    );
                }
                elseif($fila->enfirme == 1 && $this->utilerias->get_grupo() == 1) {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_egresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("egresos/ver".$destino."/".$fila->id_adecuaciones_egresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                        <a href="'.base_url("egresos/editar".$destino."/".$fila->id_adecuaciones_egresos_caratula).'" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                        <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>'
                    );
                }
                else {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_egresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("egresos/ver".$destino."/".$fila->id_adecuaciones_egresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                        <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>'
                    );

                }

            }
            elseif($fila->estatus == "espera"){
                $estatus = "warning";

                if($fila->enfirme == 0 || $this->utilerias->get_firmas() != NULL) {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_egresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("egresos/ver".$destino."/".$fila->id_adecuaciones_egresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                         <a href="'.base_url("egresos/editar".$destino."/".$fila->id_adecuaciones_egresos_caratula).'" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                         <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>'
                    );
                }
                elseif($fila->enfirme == 1 && $this->utilerias->get_grupo() == 1) {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_egresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("egresos/ver".$destino."/".$fila->id_adecuaciones_egresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                         <a href="'.base_url("egresos/editar".$destino."/".$fila->id_adecuaciones_egresos_caratula).'" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                         <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>'
                    );
                }
                else {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_egresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("egresos/ver".$destino."/".$fila->id_adecuaciones_egresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                        <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>'
                    );

                }
            }
            else{
                $estatus = "danger";

                if($fila->enfirme == 0 || $this->utilerias->get_firmas() != NULL) {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_egresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("egresos/ver".$destino."/".$fila->id_adecuaciones_egresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>'
                    );
                }
                elseif($fila->enfirme == 1 && $this->utilerias->get_grupo() == 1) {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_egresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("egresos/ver".$destino."/".$fila->id_adecuaciones_egresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>'
                    );
                }
                else {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_egresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("egresos/ver".$destino."/".$fila->id_adecuaciones_egresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>'
                    );
                }
            }
        }
//        $this->debugeo->imprimir_pre($output);
        echo json_encode($output);
    }

    function tabla_indice_adecuaciones_transferencias() {
        $sql = "SELECT * FROM mov_adecuaciones_egresos_caratula WHERE tipo = 'Transferencia' ORDER BY numero DESC;";
        $resultado = $this->ingresos_model->get_datos_adecuaciones_tipo($sql);

//        $this->debugeo->imprimir_pre($resultado);

        $output = array("data" => "");
        foreach($resultado as $fila) {

            $firme = '';
            $destino = '';

            if($fila->enfirme == 0) {
                $firme = '<button type="button" class="btn btn-circle btn-firmec" disabled><i class="fa fa-times"></i></button>';
            }
            elseif($fila->enfirme == 1){
                $firme = '<button type="button" class="btn btn-circle btn-firmea" disabled><i class="fa fa-check"></i></button>';
            }

            if($fila->tipo == "Ampliación") {
                $destino = '_ampliacion';
            } elseif($fila->tipo == "Reducción"){
                $destino = '_reduccion';
            } elseif($fila->tipo == "Transferencia"){
                $destino = '_transferencia';
            }

            if($fila->estatus == "activo") {
                $estatus = "success";
                if($fila->enfirme == 0 || $this->utilerias->get_grupo() == 1) {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_egresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("egresos/ver".$destino."/".$fila->id_adecuaciones_egresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                        <a href="'.base_url("egresos/editar".$destino."/".$fila->id_adecuaciones_egresos_caratula).'" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                         <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>'
                    );
                }
                elseif($fila->enfirme == 1 && $this->utilerias->get_grupo() == 1) {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_egresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("egresos/ver".$destino."/".$fila->id_adecuaciones_egresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                        <a href="'.base_url("egresos/editar".$destino."/".$fila->id_adecuaciones_egresos_caratula).'" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                        <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>'
                    );
                }
                else {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_egresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("egresos/ver".$destino."/".$fila->id_adecuaciones_egresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                        <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>'
                    );

                }

            }
            elseif($fila->estatus == "espera"){
                $estatus = "warning";

                if($fila->enfirme == 0 || $this->utilerias->get_firmas() != NULL) {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_egresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("egresos/ver".$destino."/".$fila->id_adecuaciones_egresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                         <a href="'.base_url("egresos/editar".$destino."/".$fila->id_adecuaciones_egresos_caratula).'" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                         <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>'
                    );
                }
                elseif($fila->enfirme == 1 && $this->utilerias->get_grupo() == 1) {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_egresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("egresos/ver".$destino."/".$fila->id_adecuaciones_egresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                         <a href="'.base_url("egresos/editar".$destino."/".$fila->id_adecuaciones_egresos_caratula).'" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                         <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>'
                    );
                }
                else {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_egresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("egresos/ver".$destino."/".$fila->id_adecuaciones_egresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                        <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>'
                    );

                }
            }
            else{
                $estatus = "danger";

                if($fila->enfirme == 0 || $this->utilerias->get_firmas() != NULL) {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_egresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("egresos/ver".$destino."/".$fila->id_adecuaciones_egresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>'
                    );
                }
                elseif($fila->enfirme == 1 && $this->utilerias->get_grupo() == 1) {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_egresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("egresos/ver".$destino."/".$fila->id_adecuaciones_egresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>'
                    );
                }
                else {
                    $output["data"][] = array(
                        $fila->id_adecuaciones_egresos_caratula,
                        $fila->numero,
                        $fila->tipo,
                        $fila->fecha_solicitud,
                        $fila->fecha_aplicacion,
                        $fila->clasificacion,
                        '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                        $fila->creado_por,
                        $firme,
                        '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                        '<a href="'.base_url("egresos/ver".$destino."/".$fila->id_adecuaciones_egresos_caratula).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>'
                    );
                }
            }
        }
//        $this->debugeo->imprimir_pre($output);
        echo json_encode($output);
    }

    /**
     * Esta funcion se encarga de mostrar la consulta presupuestaria con todos su meses
     */
    function consulta_presupuesto() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha consultado un presupuesto');
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Consulta Presupuestaria",
            "usuario" => $this->tank_auth->get_username(),
            "precompromisocss" => TRUE,
            "tablas" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/consulta_presupuestaria_view', array(
            "nivel_superior" => $this->input_nivelsuperior(),
            "niveles_modal_superior" => $this->preparar_estructura_superior(),
        ));
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "adecuaciones_egresos_agregar_transferencia" => TRUE,
        ));
    }

    /**
     * Seccion Transferencia
     */

    function agregar_transferencia() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha agregado una tranferencia');
        $last = 0;
//        Se toma el numero del ultimo precompromiso
        $ultimo = $this->egresos_model->ultimo_adecuaciones();

        if($ultimo) {
            $last = $ultimo->ultimo + 1;
        }
        else {
            $last = 1;
        }

        $this->egresos_model->apartarAdecuacion($last, "Transferencia");

        $datos_header = array(
            "titulo_pagina" => "Armonniza | Agregar Transferencia",
            "usuario" => $this->tank_auth->get_username(),
            "precompromisocss" => TRUE,
            "tablas" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/agregar_transferencia_view', array(
            "ultimo" => $last,
            "nivel_superior" => $this->input_nivelsuperior(),
            "niveles_modal_superior" => $this->preparar_estructura_superior(),
        ));
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "adecuaciones_egresos_agregar_transferencia" => TRUE,
        ));
    }

    function insertar_transferencia() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha insertado una transferencia');
        $datos = $this->input->post();
        $datos["tipo"] = "Transferencia";

        $respuesta = array(
            "mensaje" => '',
        );

        try {

            $arreglo_datos = array();

//            Se toman las partidas de la transferencia y se agrupan por el id del nivel de la estructura
            $detalle_transferencia_partida = $this->egresos_model->get_datos_partida_detalle_numero($datos["ultimo"]);

//            Se recorre el arrelgo
            foreach($detalle_transferencia_partida as $value){
//                Se busca cada partida y la estructura
                $query_estructuras = "SELECT id_niveles, COLUMN_JSON(nivel) AS estructura FROM cat_niveles WHERE id_niveles = ?";
                $query = $this->db->query($query_estructuras, array($value["id_nivel"]));
                $result = $query->row_array();

//                Se guarda la estructura de la partida en un arreglo de dato
                $arreglo_datos[$result["id_niveles"]] = json_decode($result["estructura"], TRUE);
            }

//            Se vuelve a tomar el detalle de la transferencia
            $detalle_transferencia = $this->egresos_model->get_datos_detalle_numero($datos["ultimo"]);

            foreach($detalle_transferencia as $fila) {
                if($fila->texto == "Reducción") {
                    $arreglo_datos[$fila->id_nivel]["total_anual"] = round($arreglo_datos[$fila->id_nivel]["total_anual"], 2) - ((round($fila->total, 2) + round($arreglo_datos[$fila->id_nivel]["precomprometido_anual"], 2)));
                    if($arreglo_datos[$fila->id_nivel]["total_anual"] >= 0) {
//                        $this->debugeo->imprimir_pre($fila);
//                        $this->debugeo->imprimir_pre($arreglo_datos[$fila->id_nivel]["total_anual"]);
                        $arreglo_datos[$fila->id_nivel][$fila->mes_destino."_saldo"] = round($arreglo_datos[$fila->id_nivel][$fila->mes_destino."_saldo"], 2) - round($fila->total, 2);
//                        $this->debugeo->imprimir_pre($arreglo_datos[$fila->id_nivel][$fila->mes_destino."_saldo"]);
                        if($arreglo_datos[$fila->id_nivel][$fila->mes_destino."_saldo"] < 0) {
                            throw new Exception('Las reducciones en la partida '.$fila->partida.' durante el mes de '.ucfirst($fila->mes_destino).' dejan un saldo negativo.');
                        }
                    } else {
                        throw new Exception('El presupuesto está precomprometido.');
                    }
                }
            }

            if($datos["check_firme"] == 1) {
                $datos["estatus"] = "activo";
                $datos["fecha_autoriza"] = date("Y-m-d");
                $this->actualizar_saldo_transferencia($datos["ultimo"]);
            }
            else {
                $datos["estatus"] = "espera";
            }

            $respuesta = array(
                "mensaje" => '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-check-circle fa-2x ic-msj"></i> La Transferencia se ha guardado con éxito.</div>',
            );

        } catch (Exception $e) {
            $datos["check_firme"] = 0;
            $datos["fecha_autoriza"] = "0000-00-00";
            $datos["estatus"] = "espera";
            $respuesta = array(
                "mensaje" => '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-times-circle fa-2x ic-msj"></i> '.$e->getMessage().'</div>',
            );
        }

        $resultado_insertar = $this->egresos_model->insertarCaratula($datos);

        echo(json_encode($respuesta));
    }

    private function insertar_transferencia_archivo($datos) {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha insertado una trnsferencia en un archivo');
        $datos["tipo"] = "Transferencia";

        $fecha_solicitud = ($datos['D'] - 25569) * 86400;
        $datos["D"] = date('Y-m-d', $fecha_solicitud);

        $fecha_aplicar = ($datos['E'] - 25569) * 86400;
        $datos["E"] = date('Y-m-d', $fecha_aplicar);

        $resultado_insertar = $this->egresos_model->insertarCaratulaArchivo($datos);

        return $resultado_insertar;
    }

    function insertar_transferencia_detalle() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha insertado una transferencia de detalle');
        $datos = $this->input->post();

//        $this->debugeo->imprimir_pre($datos);

        $caracteres = array("$", ",");

        foreach($datos as $key=>$value){
            $datos[$key]=str_replace($caracteres,"",$value);
        }

        try {

            if($datos["mes_destino"] == "Mes del que se tomará/agregará el presupuesto") {
                throw new Exception('No se ingresó el mes destino.');
            }

//            Se llama a la funcion del model de egresos encargada de tomar los nombres delos niveles que existen
            $nombres_egresos = $this->egresos_model->obtener_nombre_niveles();

//          En este arreglo se van a guardar los nombres de los niveles
            $nombre = array();

//          En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
            foreach($nombres_egresos as $fila) {
                array_push($nombre, $fila->descripcion);
            }

            $datos["enlace"] = rand();

//          Se toman los datos de la estructura superior
            $datos_partida_superior = $this->egresos_model->datos_de_partida(array("nivel1" => $datos["nivel1_superior"], "nivel2" => $datos["nivel2_superior"], "nivel3" => $datos["nivel3_superior"], "nivel4" => $datos["nivel4_superior"], "nivel5" => $datos["nivel5_superior"], "nivel6" => $datos["nivel6_superior"],), $nombre);
            $estructura_superior = NULL;
            $id_superior = 0;
            foreach($datos_partida_superior as $fila) {
                $estructura_superior = json_decode($fila->estructura);
                $id_superior = $fila->ID;
//            $this->debugeo->imprimir_pre($estructura_superior);
            }

            if (!$datos_partida_superior) {
                throw new Exception('No existe la partida.');
            }

            $resultado = FALSE;

            if($datos["operacion"] == "+") {
                $datos["tipo"] = "Transferencia";
                $datos["texto"] = "Ampliación";
                $resultado = $this->egresos_model->insertarDetalleSuperior($datos);

                if($resultado) {
                    $respuesta = array(
                        "mensaje" => '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-check-circle fa-2x ic-msj"></i> La Ampliación se ha insertado correctamente.</div>',
                    );
                } else {
                    throw new Exception('Hubo un error al insertar la ampliación, por favor intentelo de nuevo.');
                }

            } elseif($datos["operacion"] == "-") {

                $datos["tipo"] = "Transferencia";
                $datos["texto"] = "Reducción";

//              Se toma el saldo del mes y del año
                $saldo_mes = $this->egresos_model->get_saldo_partida($datos["mes_destino"], $id_superior);

//                $this->debugeo->imprimir_pre($saldo_mes);

                $saldo = $saldo_mes->total_anual + $saldo_mes->comprometido_anual;

                $diferencia = round($saldo - $saldo_mes->precomprometido_anual, 2);

                $operacion = round($diferencia - $datos["cantidad"], 2);

                if($operacion >= 0) {
                    $resultado = $this->egresos_model->insertarDetalleSuperior($datos);
                    if($resultado) {
                        $respuesta = array(
                            "mensaje" => '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-check-circle fa-2x ic-msj"></i> La Reducción se ha insertado correctamente.</div>',
                        );
                    } else {
                        throw new Exception('Hubo un error al insertar la reducción, por favor intentelo de nuevo.');
                    }
                } else {
                    throw new Exception('El presupuesto ya está está precomprometido.');
                }
            }

            echo(json_encode($respuesta));

        } catch (Exception $e) {
            $respuesta = array(
                "mensaje" => '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-times-circle fa-2x ic-msj"></i> '.$e->getMessage().'</div>',
            );

            echo(json_encode($respuesta));
        }

    }

    private function insertar_transferencia_detalle_archivo($datos) {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha insertado transferencias de detalle del archivo');

//        $this->debugeo->imprimir_pre($datos);

        $datos["nivel1_superior"] = $datos["A"];
        $datos["nivel2_superior"] = $datos["B"];
        $datos["nivel3_superior"] = $datos["C"];
        $datos["nivel4_superior"] = $datos["D"];
        $datos["nivel5_superior"] = $datos["E"];
        $datos["nivel6_superior"] = $datos["F"];

        $fecha = ($datos['J'] - 25569) * 86400;
        $fecha = date('Y-m-d', $fecha);

        $datos["fecha_aplicar_detalle"] = $fecha;

        $datos["mes_destino"] = strtolower($datos["G"]);

        $datos["cantidad"] = $datos["I"];

        // $this->debugeo->imprimir_pre($datos);

//        Se llama a la funcion del model de egresos encargada de tomar los nombres delos niveles que existen
        $nombres_egresos = $this->egresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_egresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

        $datos["enlace"] = rand();

//       Se toman los datos de la estructura superior
        $datos_partida_superior = $this->egresos_model->datos_de_partida(array("nivel1" => $datos["nivel1_superior"], "nivel2" => $datos["nivel2_superior"], "nivel3" => $datos["nivel3_superior"], "nivel4" => $datos["nivel4_superior"], "nivel5" => $datos["nivel5_superior"], "nivel6" => $datos["nivel6_superior"],), $nombre);
        $estructura_superior = NULL;
        $id_superior = 0;
        foreach($datos_partida_superior as $fila) {
            $estructura_superior = json_decode($fila->estructura);
            $id_superior = $fila->ID;
//            $this->debugeo->imprimir_pre($estructura_superior);
        }

//        $this->debugeo->imprimir_pre($estructura_inferior);

        $resultado = FALSE;

        if($datos["operacion"] == "+") {
            $datos["tipo"] = "Transferencia";
            $datos["texto"] = "Ampliación";
            $resultado = $this->egresos_model->insertarDetalleSuperior($datos);

            if($resultado) {
                $mensaje = TRUE;
            } else {
                $mensaje = FALSE;
            }

        } elseif($datos["operacion"] == "-") {
            $datos["tipo"] = "Transferencia";
            $datos["texto"] = "Reducción";

//            Se toma el saldo del mes y del año
            $saldo_mes = $this->egresos_model->get_saldo_partida($datos["mes_destino"], $id_superior);

            // $this->debugeo->imprimir_pre($saldo_mes);

            $operacion = round($saldo_mes->mes_saldo, 2) - round($datos["cantidad"], 2);

            if($operacion >= 0) {
                $resultado = $this->egresos_model->insertarDetalleSuperior($datos);
                if($resultado) {
                    $mensaje = TRUE;
                } else {
                    $mensaje = FALSE;
                }
            } else {
                $mensaje = "no_dinero";
            }
        }

//        $this->debugeo->imprimir_pre($datos);

        return $mensaje;

    }

    private function actualizar_saldo_transferencia($id) {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha actualizado el saldo de transferencia');
        $datos_caratula = $this->egresos_model->get_datos_caratula($id);
//        $this->debugeo->imprimir_pre($datos_caratula);
        $datos_detalle = $this->egresos_model->get_datos_detalle_numero($datos_caratula->numero);
//        $this->debugeo->imprimir_pre($datos_detalle);

        foreach($datos_detalle as $fila) {
            if($fila->texto == "Reducción") {
//                $this->debugeo->imprimir_pre($fila);
                $saldo_mes = $this->egresos_model->get_saldo_partida($fila->mes_destino, $fila->id_nivel);

                $nuevo_saldo_mes = $saldo_mes->mes_saldo - $fila->total;

                $nuevo_saldo_year = $saldo_mes->total_anual - $fila->total;

                $this->egresos_model->actualizar_saldo_partida($fila->mes_destino, $nuevo_saldo_mes, $nuevo_saldo_year, $fila->id_nivel);

            } elseif($fila->texto == "Ampliación") {
//                $this->debugeo->imprimir_pre($fila);
                $saldo_mes = $this->egresos_model->get_saldo_partida($fila->mes_destino, $fila->id_nivel);

                $nuevo_saldo_mes = $saldo_mes->mes_saldo + $fila->total;

                $nuevo_saldo_year = $saldo_mes->total_anual + $fila->total;

                $this->egresos_model->actualizar_saldo_partida($fila->mes_destino, $nuevo_saldo_mes, $nuevo_saldo_year, $fila->id_nivel);

            }
        }

    }

    function tabla_detalle_transferencia() {
        $transferencia = $this->input->post("transferencia", TRUE);

        $query = "SELECT *, COLUMN_JSON(estructura) as estructura FROM mov_adecuaciones_egresos_detalle WHERE numero_adecuacion = ?;";

        $resultado = $this->egresos_model->datos_transferenciaDetalle($query, $transferencia);
//        $this->debugeo->imprimir_pre($resultado);

        $output = array();

        foreach($resultado as $fila) {
            $estructura = json_decode($fila->estructura);

            $output[] = array(
                $fila->id_adecuaciones_egresos_detalle,
                $estructura ->fuente_de_financiamiento,
                $estructura ->programa_de_financiamiento,
                $estructura ->centro_de_costos,
                $estructura ->capitulo,
                $estructura ->concepto,
                $estructura ->partida,
                $fila->total,
                $fila->fecha_aplicacion,
                $fila->titulo,
                $fila->texto,
                '<a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Eliminar"><i class="fa fa-remove"></i></a>'
            );
        }
//        $this->debugeo->imprimir_pre($output);
        echo json_encode($output);
    }

    function tomar_total_transferencia() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha tomado el total de transferencia');
        $transferencia = $this->input->post("transferencia", TRUE);

        $query = "SELECT *, COLUMN_JSON(estructura) as estructura FROM mov_adecuaciones_egresos_detalle WHERE numero_adecuacion = ?;";

        $resultado = $this->egresos_model->datos_transferenciaDetalle($query, $transferencia);
//        $this->debugeo->imprimir_pre($resultado);

        $output = array();
        $total_ampliacion = 0;
        $total_reduccion = 0;
        $total_final = 0;

        foreach($resultado as $fila) {

            if($fila->texto == "Ampliación") {
                $total_ampliacion += $fila->total;
            } elseif($fila->texto == "Reducción") {
                $total_reduccion += $fila->total;
            }

        }
        $total_final = ($total_ampliacion + $total_reduccion) / 2;
        $output["total_ampliacion"] = $total_ampliacion;
        $output["total_reduccion"] = $total_reduccion;
        $output["total_final"] = $total_final;
//        $this->debugeo->imprimir_pre($output);
        echo json_encode($output);
    }

    function borrar_detalle_transferencia() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha borrado el detalle de tranferencia');

        $transferencia = $this->input->post("transferencia", TRUE);

        $resultado = $this->egresos_model->borrar_adecuacion_detalle($transferencia);
        if($resultado) {
            $mensaje["mensaje"] = "ok";
        } else {
            $mensaje["mensaje"] = "error";
        }

        echo(json_encode($mensaje));

    }

    function editar_transferencia($transferencia = NULL) {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha editado la tranferencia');
        $resultados = $this->egresos_model->get_datos_caratula($transferencia);

//        $this->debugeo->imprimir_pre($resultados);

        $datos_header = array(
            "titulo_pagina" => "Armonniza | Editar Transferencia",
            "usuario" => $this->tank_auth->get_username(),
            "precompromisocss" => TRUE,
            "tablas" => TRUE,
        );

        $resultados->nivel_superior = $this->input_nivelsuperior();
        $resultados->nivel_inferior = $this->input_nivelinferior();
        $resultados->niveles_modal_superior = $this->preparar_estructura_superior();
        $resultados->niveles_modal_inferior = $this->preparar_estructura_inferior();

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/editar_transferencia_view', $resultados);
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "editar_egresos_agregar_transferencia" => TRUE,
        ));
    }

    function ver_transferencia($transferencia = NULL) {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha visto una tranferencia');
        $resultados = $this->egresos_model->get_datos_caratula($transferencia);

//        $this->debugeo->imprimir_pre($resultados);

        $datos_header = array(
            "titulo_pagina" => "Armonniza | Ver Transferencia",
            "usuario" => $this->tank_auth->get_username(),
            "precompromisocss" => TRUE,
            "tablas" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/ver_transferencia_view', $resultados);
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "ver_egresos_agregar_transferencia" => TRUE,
        ));
    }

    /**
     * Seccion Ampliación
     */

    function agregar_ampliacion() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha agregado una nueva ampliación ');
        $last = 0;
//        Se toma el numero del ultimo precompromiso
        $ultimo = $this->egresos_model->ultimo_adecuaciones();

        if($ultimo) {
            $last = $ultimo->ultimo + 1;
        }
        else {
            $last = 1;
        }

        $this->egresos_model->apartarAdecuacion($last, "Ampliación");

        $datos_header = array(
            "titulo_pagina" => "Armonniza | Agregar Ampliación",
            "usuario" => $this->tank_auth->get_username(),
            "precompromisocss" => TRUE,
            "tablas" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/agregar_ampliacion_view', array(
            "ultimo" => $last,
            "nivel_superior" => $this->input_nivelsuperior(),
            "nivel_inferior" => $this->input_nivelinferior(),
            "niveles_modal_superior" => $this->preparar_estructura_superior(),
            "niveles_modal_inferior" => $this->preparar_estructura_inferior(),
        ));
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "adecuaciones_egresos_agregar_ampliacion" => TRUE,
        ));
    }

    function insertar_ampliacion() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha insertado una nueva ampliación');
        $datos = $this->input->post();
        $datos["tipo"] = "Ampliación";

        if($datos["check_firme"] == 1) {
            $datos["estatus"] = "activo";
            $datos["fecha_autoriza"] = date("Y-m-d");
            $this->actualizar_saldo_ampliacion($datos["ultimo"]);
        }
        else {
            $datos["estatus"] = "espera";
        }

        $resultado_insertar = $this->egresos_model->insertarCaratula($datos);

        echo(json_encode($resultado_insertar));
    }

    private function insertar_ampliacion_archivo($datos) {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha insertado una ampliación de un archivo');
        $datos["tipo"] = "Ampliación";

        $fecha_solicitud = ($datos['D'] - 25569) * 86400;
        $datos["D"] = date('Y-m-d', $fecha_solicitud);

        $fecha_aplicar = ($datos['E'] - 25569) * 86400;
        $datos["E"] = date('Y-m-d', $fecha_aplicar);

        $resultado_insertar = $this->egresos_model->insertarCaratulaArchivo($datos);

        return $resultado_insertar;
    }
    
    function insertar_ampliacion_detalle() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha insertado un detalle a la ampliación.');
        $datos = $this->input->post();
        $datos["tipo"] = "Ampliación";
        $datos["texto"] = "Ampliación";
        $datos["enlace"] = 0;

        $caracteres = array("$", ",");

        foreach($datos as $key=>$value){
            $datos[$key]=str_replace($caracteres,"",$value);
        }

//        Se llama a la funcion del model de egresos encargada de tomar los nombres delos niveles que existen
        $nombres_egresos = $this->egresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_egresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

//       Se toman los datos de la estructura inferior
        $datos_partida_inferior = $this->egresos_model->datos_de_partida(array("nivel1" => $datos["nivel1_inferior"], "nivel2" => $datos["nivel2_inferior"], "nivel3" => $datos["nivel3_inferior"], "nivel4" => $datos["nivel4_inferior"], "nivel5" => $datos["nivel5_inferior"], "nivel6" => $datos["nivel6_inferior"],), $nombre);
        $estructura_inferior = NULL;
        $id_inferior = 0;
        foreach($datos_partida_inferior as $fila) {
            $estructura_inferior = json_decode($fila->estructura);
            $id_inferior = $fila->ID;
//            $this->debugeo->imprimir_pre($estructura_inferior);
        }

        $mensaje = array("mensaje" => "");

        $resultado = $this->egresos_model->insertarDetalle($datos);

        if($resultado) {
            $mensaje["mensaje"] = "ok";
            $mensaje["detalle"] = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-check-circle fa-2x ic-msj"></i> Éxito al generar la Ampliación.</div>';
        } else {
            $mensaje["mensaje"] = "error";
            $mensaje["detalle"] = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-times-circle fa-2x ic-msj"></i> Hubo un error al insertar la Ampliación.</div>';
        }

        echo(json_encode($mensaje));

    }

    private function revisar_estructura_existe($datos) {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha revisado una estructura ya existente');

        $fecha = ($datos['J'] - 25569) * 86400;
        $fecha = date('Y-m-d', $fecha);

        $datos["fecha_aplicar_detalle"] = $fecha;

        $datos["mes_destino"] = strtolower($datos["G"]);

        $datos["cantidad"] = $datos["I"];

        $datos["tipo"] = "Ampliación";
        $datos["texto"] = "Ampliación";
        $datos["enlace"] = 0;

        $caracteres = array("$", ",");

        foreach($datos as $key=>$value){
            $datos[$key]=str_replace($caracteres,"",$value);
        }

//        Se llama a la funcion del model de egresos encargada de tomar los nombres delos niveles que existen
        $nombres_egresos = $this->egresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_egresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

//       Se toman los datos de la estructura inferior
        $datos_partida_inferior = $this->egresos_model->datos_de_partida(array(
            "nivel1" => $datos["A"],
            "nivel2" => $datos["B"],
            "nivel3" => $datos["C"],
            "nivel4" => $datos["D"],
            "nivel5" => $datos["E"],
            "nivel6" => $datos["F"]),
            $nombre);

        if($datos_partida_inferior) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    private function insertar_ampliacion_detalle_archivo($datos) {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha insertado una ampliación de detalle en el archivo');
        $datos["nivel1_inferior"] = $datos["A"];
        $datos["nivel2_inferior"] = $datos["B"];
        $datos["nivel3_inferior"] = $datos["C"];
        $datos["nivel4_inferior"] = $datos["D"];
        $datos["nivel5_inferior"] = $datos["E"];
        $datos["nivel6_inferior"] = $datos["F"];

        $fecha = ($datos['J'] - 25569) * 86400;
        $fecha = date('Y-m-d', $fecha);

        $datos["fecha_aplicar_detalle"] = $fecha;

        $datos["mes_destino"] = strtolower($datos["G"]);

        $datos["cantidad"] = $datos["I"];

        $datos["tipo"] = "Ampliación";
        $datos["texto"] = "Ampliación";
        $datos["enlace"] = 0;

        $caracteres = array("$", ",");

        foreach($datos as $key=>$value){
            $datos[$key]=str_replace($caracteres,"",$value);
        }

//        Se llama a la funcion del model de egresos encargada de tomar los nombres delos niveles que existen
        $nombres_egresos = $this->egresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_egresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

//       Se toman los datos de la estructura inferior
        $datos_partida_inferior = $this->egresos_model->datos_de_partida(array(
            "nivel1" => $datos["A"],
            "nivel2" => $datos["B"],
            "nivel3" => $datos["C"],
            "nivel4" => $datos["D"],
            "nivel5" => $datos["E"],
            "nivel6" => $datos["F"]),
            $nombre);

        $estructura_inferior = NULL;
        $id_inferior = 0;
        foreach($datos_partida_inferior as $fila) {
            $estructura_inferior = json_decode($fila->estructura);
            $id_inferior = $fila->ID;
//            $this->debugeo->imprimir_pre($estructura_inferior);
        }

        $mensaje = FALSE;

        $resultado = $this->egresos_model->insertarDetalle($datos);

        if($resultado) {
            $mensaje = TRUE;
        } else {
            $mensaje = FALSE;
        }

        return $mensaje;

    }

    private function actualizar_saldo_ampliacion($id) {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha actulizado el saldo de la ampliación');
        $datos_caratula = $this->egresos_model->get_datos_caratula($id);
        $datos_detalle = $this->egresos_model->get_datos_detalle_numero($datos_caratula->numero);

        foreach($datos_detalle as $fila) {
//            Se toma el saldo del mes y del año
            $saldo_mes = $this->egresos_model->get_saldo_partida($fila->mes_destino, $fila->id_nivel);

            $nuevo_saldo_mes = $saldo_mes->mes_saldo + $fila->total;

            $nuevo_saldo_year = $saldo_mes->total_anual + $fila->total;

//            Se genera el movimiento
             $this->egresos_model->actualizar_saldo_partida($fila->mes_destino, $nuevo_saldo_mes, $nuevo_saldo_year, $fila->id_nivel);
        }

    }

    function tabla_detalle_ampliacion() {
        $ampliacion = $this->input->post("ampliacion", TRUE);

        $query = "SELECT *, COLUMN_JSON(estructura) as estructura FROM mov_adecuaciones_egresos_detalle WHERE numero_adecuacion = ?;";

        $resultado = $this->egresos_model->datos_transferenciaDetalle($query, $ampliacion);
//        $this->debugeo->imprimir_pre($resultado);

        $output = array();

        foreach($resultado as $fila) {
            $estructura = json_decode($fila->estructura);

            $output[] = array(
                $fila->id_adecuaciones_egresos_detalle,
                $estructura ->fuente_de_financiamiento,
                $estructura ->programa_de_financiamiento,
                $estructura ->centro_de_costos,
                $estructura ->capitulo,
                $estructura ->concepto,
                $estructura ->partida,
                $fila->total,
                $fila->fecha_aplicacion,
                $fila->titulo,
                $fila->texto,
                '<a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Eliminar"><i class="fa fa-remove"></i></a>'
            );
        }
//        $this->debugeo->imprimir_pre($output);
        echo json_encode($output);
    }

    function borrar_detalle_ampliacion() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha borrado el detalle de la ampliación');
        $ampliacion = $this->input->post("ampliacion", TRUE);

        $resultado = $this->egresos_model->borrar_adecuacion_detalle($ampliacion);
        if($resultado) {
            $mensaje["mensaje"] = "ok";
        } else {
            $mensaje["mensaje"] = "error";
        }

        echo(json_encode($mensaje));

    }

    function editar_ampliacion($ampliacion = NULL) {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha editado una ampliación');
        $resultados = $this->egresos_model->get_datos_caratula($ampliacion);

//        $this->debugeo->imprimir_pre($resultados);

        $datos_header = array(
            "titulo_pagina" => "Armonniza | Editar Ampliación",
            "usuario" => $this->tank_auth->get_username(),
            "precompromisocss" => TRUE,
            "tablas" => TRUE,
        );

        $resultados->nivel_superior = $this->input_nivelsuperior();
        $resultados->nivel_inferior = $this->input_nivelinferior();
        $resultados->niveles_modal_superior = $this->preparar_estructura_superior();
        $resultados->niveles_modal_inferior = $this->preparar_estructura_inferior();

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/editar_ampliacion_view', $resultados);
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "editar_egresos_agregar_ampliacion" => TRUE,
        ));
    }

    function ver_ampliacion($ampliacion = NULL) {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha visto una ampliación');
        $resultados = $this->egresos_model->get_datos_caratula($ampliacion);

//        $this->debugeo->imprimir_pre($resultados);

        $datos_header = array(
            "titulo_pagina" => "Armonniza | Ver Ampliación",
            "usuario" => $this->tank_auth->get_username(),
            "precompromisocss" => TRUE,
            "tablas" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/ver_ampliacion_view', $resultados);
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "ver_egresos_agregar_ampliacion" => TRUE,
        ));
    }

    /**
     * Seccion de Reducción
     */

    function agregar_reduccion() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha agregar una nueva reducción');
        $last = 0;
//        Se toma el numero del ultimo precompromiso
        $ultimo = $this->egresos_model->ultimo_adecuaciones();

        if($ultimo) {
            $last = $ultimo->ultimo + 1;
        }
        else {
            $last = 1;
        }

        $this->egresos_model->apartarAdecuacion($last, "Reducción");

        $datos_header = array(
            "titulo_pagina" => "Armonniza | Agregar Reducción",
            "usuario" => $this->tank_auth->get_username(),
            "precompromisocss" => TRUE,
            "tablas" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/agregar_reduccion_view', array(
            "ultimo" => $last,
            "nivel_superior" => $this->input_nivelsuperior(),
            "nivel_inferior" => $this->input_nivelinferior(),
            "niveles_modal_superior" => $this->preparar_estructura_superior(),
            "niveles_modal_inferior" => $this->preparar_estructura_inferior(),
        ));
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "adecuaciones_egresos_agregar_reduccion" => TRUE,
        ));
    }

    function insertar_reduccion() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha insertado una reducción');
        $datos = $this->input->post();
        $datos["tipo"] = "Reducción";

        if($datos["check_firme"] == 1) {
            $datos["estatus"] = "activo";
            $datos["fecha_autoriza"] = date("Y-m-d");
            $this->actualizar_saldo_reduccion($datos["ultimo"]);
        }
        else {
            $datos["estatus"] = "espera";
        }

        $resultado_insertar = $this->egresos_model->insertarCaratula($datos);

        echo(json_encode($resultado_insertar));
    }

    private function insertar_reduccion_archivo($datos) {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha insertado una reducción en el archivo');
        $datos["tipo"] = "Reducción";

        $fecha_solicitud = ($datos['D'] - 25569) * 86400;
        $datos["D"] = date('Y-m-d', $fecha_solicitud);

        $fecha_aplicar = ($datos['E'] - 25569) * 86400;
        $datos["E"] = date('Y-m-d', $fecha_aplicar);

        $resultado_insertar = $this->egresos_model->insertarCaratulaArchivo($datos);

        return $resultado_insertar;
    }

    function insertar_reduccion_detalle() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha insertado un detalle a la reducción');
        $datos = $this->input->post();
        $datos["tipo"] = "Reducción";
        $datos["texto"] = "Reducción";
        $datos["enlace"] = 0;

        $caracteres = array("$", ",");

        foreach($datos as $key=>$value){
            $datos[$key]=str_replace($caracteres,"",$value);
        }

        try {
//          Se llama a la funcion del model de egresos encargada de tomar los nombres delos niveles que existen
            $nombres_egresos = $this->egresos_model->obtener_nombre_niveles();

//            En este arreglo se van a guardar los nombres de los niveles
            $nombre = array();

//            En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
            foreach($nombres_egresos as $fila) {
                array_push($nombre, $fila->descripcion);
            }

//           Se toman los datos de la estructura inferior
            $datos_partida_inferior = $this->egresos_model->datos_de_partida(array("nivel1" => $datos["nivel1_inferior"], "nivel2" => $datos["nivel2_inferior"], "nivel3" => $datos["nivel3_inferior"], "nivel4" => $datos["nivel4_inferior"], "nivel5" => $datos["nivel5_inferior"], "nivel6" => $datos["nivel6_inferior"],), $nombre);
            $estructura_inferior = NULL;
            $id_inferior = 0;
            foreach($datos_partida_inferior as $fila) {
                $estructura_inferior = json_decode($fila->estructura);
                $id_inferior = $fila->ID;
//            $this->debugeo->imprimir_pre($estructura_inferior);
            }

            if (!$datos_partida_inferior) {
                throw new Exception('No existe la partida.');
            }

            $respuesta = array("mensaje" => "");

//              Se toma el saldo del mes y del año
            $saldo_mes = $this->egresos_model->get_saldo_partida($datos["mes_destino"], $id_inferior);

//                $this->debugeo->imprimir_pre($saldo_mes);

            $saldo = $saldo_mes->total_anual + $saldo_mes->comprometido_anual;

            $diferencia = round($saldo - $saldo_mes->precomprometido_anual, 2);

            $operacion = round($diferencia - $datos["cantidad"], 2);

            if($operacion >= 0) {
                $resultado = $this->egresos_model->insertarDetalle($datos);
                if($resultado) {
                    $respuesta = array(
                        "mensaje" => '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-check-circle fa-2x ic-msj"></i> La Reducción se ha insertado correctamente.</div>',
                    );
                } else {
                    throw new Exception('Hubo un error al insertar la reducción, por favor intentelo de nuevo.');
                }
            } else {
                throw new Exception('El presupuesto ya está está precomprometido.');
            }

            echo(json_encode($respuesta));

        } catch (Exception $e) {
            $respuesta = array(
                "mensaje" => '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-times-circle fa-2x ic-msj"></i> '.$e->getMessage().'</div>',
            );

            echo(json_encode($respuesta));
        }

    }

    private function insertar_reduccion_detalle_archivo($datos) {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha insertado un detalle a la reducción en el archivo');

        $datos["nivel1_inferior"] = $datos["A"];
        $datos["nivel2_inferior"] = $datos["B"];
        $datos["nivel3_inferior"] = $datos["C"];
        $datos["nivel4_inferior"] = $datos["D"];
        $datos["nivel5_inferior"] = $datos["E"];
        $datos["nivel6_inferior"] = $datos["F"];

        $fecha = ($datos['J'] - 25569) * 86400;
        $fecha = date('Y-m-d', $fecha);

        $datos["fecha_aplicar_detalle"] = $fecha;

        $datos["mes_destino"] = strtolower($datos["G"]);

        $datos["cantidad"] = $datos["I"];

        $datos["tipo"] = "Reducción";
        $datos["texto"] = "Reducción";
        $datos["enlace"] = 0;

        $caracteres = array("$", ",");

        foreach($datos as $key=>$value){
            $datos[$key]=str_replace($caracteres,"",$value);
        }

//        Se llama a la funcion del model de egresos encargada de tomar los nombres delos niveles que existen
        $nombres_egresos = $this->egresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_egresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

//       Se toman los datos de la estructura inferior
        $datos_partida_inferior = $this->egresos_model->datos_de_partida(array("nivel1" => $datos["nivel1_inferior"], "nivel2" => $datos["nivel2_inferior"], "nivel3" => $datos["nivel3_inferior"], "nivel4" => $datos["nivel4_inferior"], "nivel5" => $datos["nivel5_inferior"], "nivel6" => $datos["nivel6_inferior"],), $nombre);
        $estructura_inferior = NULL;
        $id_inferior = 0;
        foreach($datos_partida_inferior as $fila) {
            $estructura_inferior = json_decode($fila->estructura);
            $id_inferior = $fila->ID;
        }

//        $this->debugeo->imprimir_pre($datos);

        $mensaje = array("mensaje" => "");

//            Se toma el saldo del mes y del año
        $saldo_mes = $this->egresos_model->get_saldo_partida($datos["mes_destino"], $id_inferior );

        $operacion = $saldo_mes->mes_saldo - $datos["cantidad"];

//        $this->debugeo->imprimir_pre($operacion);

        if($operacion >= 0) {
            $resultado = $this->egresos_model->insertarDetalle($datos);
            if($resultado) {
                $mensaje = TRUE;
            } else {
                $mensaje = FALSE;
            }
        } else {
            $mensaje = "no_dinero";
        }

//        $this->debugeo->imprimir_pre($mensaje);

        return $mensaje;
    }

    private function actualizar_saldo_reduccion($id) {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha actualizado el saldo de reducción');
        $datos_caratula = $this->egresos_model->get_datos_caratula($id);
        $datos_detalle = $this->egresos_model->get_datos_detalle_numero($datos_caratula->numero);

        foreach($datos_detalle as $fila) {
//            Se toma el saldo del mes y del año
            $saldo_mes = $this->egresos_model->get_saldo_partida($fila->mes_destino, $fila->id_nivel);

            $nuevo_saldo_mes = $saldo_mes->mes_saldo - $fila->total;

            $nuevo_saldo_year = $saldo_mes->total_anual - $fila->total;

//            Se genera el movimiento
            $this->egresos_model->actualizar_saldo_partida($fila->mes_destino, $nuevo_saldo_mes, $nuevo_saldo_year, $fila->id_nivel);
        }

    }

    function tabla_detalle_reduccion() {
        $reduccion = $this->input->post("reduccion", TRUE);

        $query = "SELECT *, COLUMN_JSON(estructura) as estructura FROM mov_adecuaciones_egresos_detalle WHERE numero_adecuacion = ?;";

        $resultado = $this->egresos_model->datos_transferenciaDetalle($query, $reduccion);
//        $this->debugeo->imprimir_pre($resultado);

        $output = array();

        foreach($resultado as $fila) {
            $estructura = json_decode($fila->estructura);

            $output[] = array(
                $fila->id_adecuaciones_egresos_detalle,
                $estructura ->fuente_de_financiamiento,
                $estructura ->programa_de_financiamiento,
                $estructura ->centro_de_costos,
                $estructura ->capitulo,
                $estructura ->concepto,
                $estructura ->partida,
                $fila->total,
                $fila->fecha_aplicacion,
                $fila->titulo,
                $fila->texto,
                '<a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Eliminar"><i class="fa fa-remove"></i></a>'
            );
        }
//        $this->debugeo->imprimir_pre($output);
        echo json_encode($output);
    }

    function borrar_detalle_reduccion() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha borrado un detalle de la reducción');
        $reduccion = $this->input->post("reduccion", TRUE);

        $datos = $this->egresos_model->get_datos_detalle($reduccion);
//        $this->debugeo->imprimir_pre($datos);


        $mensaje = array("mensaje" => "");

        $resultado = $this->egresos_model->borrar_adecuacion_detalle($reduccion);
        if($resultado) {
            $mensaje["mensaje"] = "ok";
        } else {
            $mensaje["mensaje"] = "error";
        }

        echo(json_encode($mensaje));
    }

    function editar_reduccion($ampliacion = NULL) {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha editado un detalle a la reducción');
        $resultados = $this->egresos_model->get_datos_caratula($ampliacion);

//        $this->debugeo->imprimir_pre($resultados);

        $datos_header = array(
            "titulo_pagina" => "Armonniza | Editar Reducción",
            "usuario" => $this->tank_auth->get_username(),
            "precompromisocss" => TRUE,
            "tablas" => TRUE,
        );

        $resultados->nivel_superior = $this->input_nivelsuperior();
        $resultados->nivel_inferior = $this->input_nivelinferior();
        $resultados->niveles_modal_superior = $this->preparar_estructura_superior();
        $resultados->niveles_modal_inferior = $this->preparar_estructura_inferior();

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/editar_reduccion_view', $resultados);
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "editar_egresos_agregar_reduccion" => TRUE,
        ));
    }

    function ver_reduccion($ampliacion = NULL) {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha visto una reducción');
        $resultados = $this->egresos_model->get_datos_caratula($ampliacion);

//        $this->debugeo->imprimir_pre($resultados);

        $datos_header = array(
            "titulo_pagina" => "Armonniza | Ver Reducción",
            "usuario" => $this->tank_auth->get_username(),
            "precompromisocss" => TRUE,
            "tablas" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/ver_reduccion_view', $resultados);
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "ver_egresos_agregar_reduccion" => TRUE,
        ));
    }

    function cancelar_adecuacion() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha cancelado una reducción');
        $id = $this->input->post("adecuacion", TRUE);

        $datos_caratula = $this->egresos_model->get_datos_caratula($id);
        $datos_detalle = $this->egresos_model->get_datos_detalle_numero($datos_caratula->numero);

        if($datos_caratula->enfirme == 1) {
            foreach($datos_detalle as $fila) {
                if($fila->texto == "Reducción") {
                    $saldo_mes = $this->egresos_model->get_saldo_partida($fila->mes_destino, $fila->id_nivel);

                    $nuevo_saldo_mes = $saldo_mes->mes_saldo + $fila->total;

                    $nuevo_saldo_year = $saldo_mes->total_anual + $fila->total;

                    $nuevo_saldo_precompromiso_year = $saldo_mes->precomprometido_anual + $fila->total;

                    $this->egresos_model->actualizar_saldo_partida_completo($fila->mes_destino, $nuevo_saldo_mes, $nuevo_saldo_precompromiso_year, $nuevo_saldo_year, $fila->id_nivel);

                } elseif($fila->texto == "Ampliación") {
                    $saldo_mes = $this->egresos_model->get_saldo_partida($fila->mes_destino, $fila->id_nivel);

                    $nuevo_saldo_mes = $saldo_mes->mes_saldo - $fila->total;

                    $nuevo_saldo_year = $saldo_mes->total_anual - $fila->total;

                    $nuevo_saldo_precompromiso_year = $saldo_mes->precomprometido_anual - $fila->total;

                    $this->egresos_model->actualizar_saldo_partida_completo($fila->mes_destino, $nuevo_saldo_mes, $nuevo_saldo_precompromiso_year, $nuevo_saldo_year, $fila->id_nivel);

                }
            }

            $this->egresos_model->cancelarAdecuacion($id);
        } else {
            $this->egresos_model->cancelarAdecuacion($id);
        }


        echo(json_encode(array("mensaje" => "ok")));
    }

    function preparar_query() {
//        Se llama la funcion del modelo de egresos encargado de contar los niveles que existen
        $total_egresos = $this->egresos_model->contar_egresos_elementos();
//        Se llama a la funcion del model de egresos encargada de tomar los nombres delos niveles que existen
        $nombres_egresos = $this->egresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_egresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

        $query_insertar = " COLUMN_CREATE(";

        for($i = 0; $i < $total_egresos->conteo; $i++){
            $query_insertar .= "'".strtolower(str_replace(' ', '_', $nombre[$i]))."', ?, ";
        }

        $query_insertar .= "'id_nivel', ?), ";

        echo($query_insertar);
    }

    function obtenerDatosPartida() {
//        Se llama a la funcion del model de egresos encargada de tomar los nombres delos niveles que existen
        $nombres_egresos = $this->egresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_egresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

//        Se toman los valores del primer, segundo, tercer, cuarto, quinto y sexto nivel que son enviados por POST
        $datos = $this->input->post();

//        Se llama a la libreria de utilirerias, para preparar el mes actual
        $mes_actual = $this->utilerias->preparar_mes_actual();

//        Se llama a la funcion que se encarga de tomar los datos de la partida para desplegar la informacion necesaria
        $datos_nivel = $this->egresos_model->datos_de_partida($datos, $nombre);

//        Se devuelven los valores en formato JSON para poder ingresarlos a la grafica de balance de dinero por mes
        foreach($datos_nivel as $row) {
            $estructura = json_decode($row->estructura, TRUE);
            $estructura["enero_modificado"] = $estructura["enero_inicial"];
            $estructura["febrero_modificado"] = $estructura["febrero_inicial"];
            $estructura["marzo_modificado"] = $estructura["marzo_inicial"];
            $estructura["abril_modificado"] = $estructura["abril_inicial"];
            $estructura["mayo_modificado"] = $estructura["mayo_inicial"];
            $estructura["junio_modificado"] = $estructura["junio_inicial"];
            $estructura["julio_modificado"] = $estructura["julio_inicial"];
            $estructura["agosto_modificado"] = $estructura["agosto_inicial"];
            $estructura["septiembre_modificado"] = $estructura["septiembre_inicial"];
            $estructura["octubre_modificado"] = $estructura["octubre_inicial"];
            $estructura["noviembre_modificado"] = $estructura["noviembre_inicial"];
            $estructura["diciembre_modificado"] = $estructura["diciembre_inicial"];
            $estructura["total_modificado"] = $estructura["enero_inicial"]+$estructura["febrero_inicial"]+$estructura["marzo_inicial"]+$estructura["abril_inicial"]+$estructura["mayo_inicial"]+$estructura["junio_inicial"]+$estructura["julio_inicial"]+$estructura["agosto_inicial"]+$estructura["septiembre_inicial"]+$estructura["octubre_inicial"]+$estructura["noviembre_inicial"]+$estructura["diciembre_inicial"];
            $sql_adecuacion = "SELECT md.mes_destino, md.total, md.texto
                    FROM mov_adecuaciones_egresos_detalle md
                    JOIN mov_adecuaciones_egresos_caratula mc
                    ON md.numero_adecuacion = mc.numero
                    WHERE md.id_nivel = ?
                    AND mc.enfirme = 1
                    AND cancelada = 0;";
            $query_adecuacion = $this->db->query($sql_adecuacion, array($row->ID));
            $resultado_adecuacion = $query_adecuacion->result_array();
            foreach($resultado_adecuacion as $key => $value) {
                if($value["texto"] == "Ampliación") {
                    $estructura[$value["mes_destino"]."_modificado"] += $value["total"];
                    $estructura["total_modificado"] += $value["total"];
                } elseif($value["texto"] == "Reducción") {
                    $estructura[$value["mes_destino"]."_modificado"] -= $value["total"];
                    $estructura["total_modificado"] -= $value["total"];
                }
            }
            echo(json_encode($estructura));
        }
    }

    /**
     * Sección para subir una adecuación
     */
    function subir_adecuacion() {

        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha subido una adecuación');

//        Se prepara la configuracion de la libreria "Upload"
        $config['upload_path'] = './application/archivos/';
        $config['allowed_types'] = '*';

//        Se carga la libreria con sus configuraciones correspondientes
        $this->load->library('upload', $config);

//        Si el server no logra subir el archivo, despliega un mensaje de error
        if ( ! $this->upload->do_upload('archivoSubir')) {
//            Se capturan los errores en una variable y se imprimen para debug
            $error = array('error' => $this->upload->display_errors());
            $this->debugeo->imprimir_pre($error);
        }
        else {
            ini_set('memory_limit', '-1');
//            load our new PHPExcel library
            $this->load->library('excel');
//            Si el server logra subir el archivo, se toman los datos del archivo
            $data = $this->upload->data();

//            Se toma la ruta del archivo junto con su nombre y extension
            $file = $data["full_path"];

//            read file from path
            $objPHPExcel = PHPExcel_IOFactory::load($file);

//            get only the Cell Collection
            $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();

            try {

//                extract to a PHP readable array format
                foreach ($cell_collection as $cell) {
                    $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
                    $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
                    $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();

//                    header will/should be in row 1 only. of course this can be modified to suit your need.
                    if ($row == 1 || $row == 3) {
                        continue;
                    } elseif ($row == 2) {
                        if(empty($data_value)) {
                            throw new Exception('Ha ocurrido un error en el encabezado del documento, por favor revíselo.');
                        } else {
                            $datos_caratula[$row][$column] = trim($data_value);
                        }
                    } else {
                        if(empty($data_value)) {
                            throw new Exception('Ha ocurrido un error en el detalle del documento, por favor revíselo.');
                        } else {
                            $datos_detalle[$row][$column] = trim($data_value);
                        }

                    }
                }

                $last = 0;
                $tipo_movimiento = "";
                $total = 0;
                $ampliacion = 0;
                $reduccion = 0;

//                Se toma el numero de la última adecuacion
                $ultimo = $this->egresos_model->ultimo_adecuaciones();

                if($ultimo) {
                    $last = $ultimo->ultimo + 1;
                }
                else {
                    $last = 1;
                }

                foreach($datos_caratula as $fila) {
                    $tipo_movimiento = strtoupper($fila["A"]);
                }

//                $this->debugeo->imprimir_pre($datos_detalle);

                foreach($datos_detalle as $fila) {
                    if($tipo_movimiento == "Transferencia" || $tipo_movimiento == "TRANSFERENCIA") {
                        if(strtoupper($fila["H"]) == "Ampliación" || strtoupper($fila["H"]) == "AMPLIACIÓN" || strtoupper($fila["H"]) == "Ampliacion" || strtoupper($fila["H"]) == "AMPLIACION" || strtoupper($fila["H"]) == "AMPLIACIóN") {
                            $ampliacion += $fila["I"];

                        } elseif(strtoupper($fila["H"]) == "Reducción" || strtoupper($fila["H"]) == "REDUCCIÓN" || strtoupper($fila["H"]) == "Reduccion" || strtoupper($fila["H"]) == "REDUCCION" || strtoupper($fila["H"]) == "REDUCCIóN") {
                            $reduccion += $fila["I"];
                        }
                    }
                }

                $ampliacion_cadena = (string)round($ampliacion, 2);
                $reduccion_cadena = (string)round($reduccion, 2);

//                $this->debugeo->imprimir_pre($ampliacion_cadena);
//                $this->debugeo->imprimir_pre($reduccion_cadena);

                if($ampliacion_cadena != $reduccion_cadena) {
//                    goto fin;
                    throw new Exception('Las suma de las cantidades entre ampliaciones y reducciones no son iguales, por favor revise su documento.');
                }

                $numero_fila = 1;

                foreach($datos_detalle as $fila) {
//                    $this->debugeo->imprimir_pre($fila);
                    $fila["ultimo"] = $last;
                    $total += $fila["I"];

                    $existe = $this->revisar_estructura_existe($fila);

                    if($existe) {

                        if($tipo_movimiento == "Ampliación" || $tipo_movimiento == "AMPLIACIÓN" || $tipo_movimiento == "Ampliacion" || $tipo_movimiento == "AMPLIACION" || $tipo_movimiento == "AMPLIACIóN") {
                            $respuesta["detalle"] = $this->insertar_ampliacion_detalle_archivo($fila);
                            if(!$respuesta["detalle"]) {
                                $this->db->delete('mov_adecuaciones_egresos_detalle', array('numero_adecuacion' => $fila["ultimo"]));
                                throw new Exception('Ha ocurrido un error al insertar los datos, por favor intentelo de nuevo.');
                            }
                        } elseif($tipo_movimiento == "Reducción" || $tipo_movimiento == "REDUCCIÓN" || $tipo_movimiento == "Reduccion" || $tipo_movimiento == "REDUCCION" || $tipo_movimiento == "REDUCCIóN") {
                            $respuesta["detalle"] = $this->insertar_reduccion_detalle_archivo($fila);
                            if($respuesta["detalle"] == FALSE) {
                                $this->db->delete('mov_adecuaciones_egresos_detalle', array('numero_adecuacion' => $fila["ultimo"]));
                                throw new Exception('Ha ocurrido un error al insertar los datos, por favor intentelo de nuevo.');
                            } elseif($respuesta["detalle"] === "no_dinero") {
                                $this->db->delete('mov_adecuaciones_egresos_detalle', array('numero_adecuacion' => $fila["ultimo"]));
                                throw new Exception('No hay suficiencia presupuestaria en la línea '.$numero_fila.'.');
                            }
                        } elseif($tipo_movimiento == "Transferencia" || $tipo_movimiento == "TRANSFERENCIA") {

                            if(strtoupper($fila["H"]) == "Ampliación" || strtoupper($fila["H"]) == "AMPLIACIÓN" || strtoupper($fila["H"]) == "Ampliacion" || strtoupper($fila["H"]) == "AMPLIACION" || strtoupper($fila["H"]) == "AMPLIACIóN") {
                                $fila["operacion"] = "+";
                                $respuesta["detalle"] = $this->insertar_transferencia_detalle_archivo($fila);
                                if(!$respuesta["detalle"]) {
                                    $this->db->delete('mov_adecuaciones_egresos_detalle', array('numero_adecuacion' => $fila["ultimo"]));
                                    throw new Exception('Ha ocurrido un error al insertar los datos, por favor intentelo de nuevo.');
                                }
                            } elseif(strtoupper($fila["H"]) == "Reducción" || strtoupper($fila["H"]) == "REDUCCIÓN" || strtoupper($fila["H"]) == "Reduccion" || strtoupper($fila["H"]) == "REDUCCION" || strtoupper($fila["H"]) == "REDUCCIóN") {
                                $fila["operacion"] = "-";
                                $respuesta["detalle"] = $this->insertar_transferencia_detalle_archivo($fila);
                                if(!$respuesta["detalle"]) {
                                    $this->db->delete('mov_adecuaciones_egresos_detalle', array('numero_adecuacion' => $fila["ultimo"]));
                                    throw new Exception('Ha ocurrido un error al insertar los datos, por favor intentelo de nuevo.');
                                } elseif($respuesta["detalle"] === "no_dinero") {
                                    $this->db->delete('mov_adecuaciones_egresos_detalle', array('numero_adecuacion' => $fila["ultimo"]));
                                    throw new Exception('No hay suficiencia presupuestaria en la línea '.$numero_fila.'.');
                                }
                            }
                        }
                    } else {
                        throw new Exception('La estructura en la fila '.$numero_fila.' no existe, por favor revísela.');
                    }

                    $numero_fila += 1;
                }

                $respuesta_caratula = FALSE;

                foreach($datos_caratula as $row) {
                    $row["ultimo"] = $last;
                    $row["total"] = $total;

                    if($tipo_movimiento == "Ampliación" || $tipo_movimiento == "AMPLIACIÓN" || $tipo_movimiento == "Ampliacion" || $tipo_movimiento == "AMPLIACION" || $tipo_movimiento == "AMPLIACIóN") {
                        $respuesta_caratula = $this->insertar_ampliacion_archivo($row);
                    } elseif($tipo_movimiento == "Reducción" || $tipo_movimiento == "REDUCCIÓN" || $tipo_movimiento == "Reduccion" || $tipo_movimiento == "REDUCCION" || $tipo_movimiento == "REDUCCIóN") {
                        $respuesta_caratula = $this->insertar_reduccion_archivo($row);
                    } elseif($tipo_movimiento == "Transferencia" || $tipo_movimiento == "TRANSFERENCIA") {
                        $row["total"] = $total / 2;
                        $respuesta_caratula = $this->insertar_transferencia_archivo($row);
                    };

                }

                if(!$respuesta_caratula) {
                    $this->db->delete('mov_adecuaciones_egresos_caratula', array('numero' => $last));
                    throw new Exception('Ha ocurrido un error al insertar los datos, en la caratula por favor intentelo de nuevo.');
                }

                $datos_header = array(
                    "titulo_pagina" => "Armonniza | Resultado Archivo",
                    "usuario" => $this->tank_auth->get_username(),
                    "precompromisocss" => TRUE,
                );

                $this->parser->parse('front/header_main_view', $datos_header);
                $this->load->view('front/resultado_archivo_view', array(
                    "mensaje" => '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>La adecucación fue insertada correctamente.</div>',
                ));
                $this->load->view('front/footer_main_view');


            } catch (Exception $e) {
                $datos_header = array(
                    "titulo_pagina" => "Armonniza | Resultado Archivo",
                    "usuario" => $this->tank_auth->get_username(),
                    "precompromisocss" => TRUE,
                );

                $this->parser->parse('front/header_main_view', $datos_header);
                $this->load->view('front/resultado_archivo_view', array(
                    "mensaje" => '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$e->getMessage().'</div>',
                ));
                $this->load->view('front/footer_main_view');

            }

        }

    }

    /**
     * Esta funcion es para mostrar los datos de la consulta presupuestaria de los egresos
     */
    function consulta_presupuestaria() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha consultado una partida presupuestaria');
        $datos_header = $this->prep_datos("Armonniza Consulta de Afectación Presupuestaria");

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/egresos_consulta_view');
        $this->load->view('front/footer_main_view');
    }

    /**
     * Esta funcion se encarga de obtener los datos del segundo nivel de la estructura de egresos
     */
    function obtener_nivel1() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha seleccionado el nivel 1');
//        Se llama a la funcion del model de egresos encargada de tomar los nombres delos niveles que existen
        $nombres_egresos = $this->egresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_egresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

//        Se llamam a la funcion del modelo que se encarga de buscar los programas de financiamiento conforme a la variable recibida
//        Y se guarda los resultados en una variable
        $nuevo_nivel = $this->egresos_model->datos_primerNivel($nombre[0]);

//        Se crea un ciclo para imprimir los datos obtenidos en los elementos de una lista
        foreach($nuevo_nivel as $fila) {
            echo("<option title='".$fila->fuente_de_financiamiento." - ".$fila->descripcion."' value='".$fila->fuente_de_financiamiento."' >".$fila->fuente_de_financiamiento." -  ".$fila->descripcion."</option>");
        }
    }

    /**
     * Esta funcion se encarga de obtener los datos del segundo nivel de la estructura de egresos
     */
    function obtener_nivel2() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha seleccionado el nivel 2');
//        Se llama a la funcion del model de egresos encargada de tomar los nombres delos niveles que existen
        $nombres_egresos = $this->egresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_egresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

//        Se toma la variable que se manda por POST
        $nivel1 = $this->input->post("nivel1");

//        Se llamam a la funcion del modelo que se encarga de buscar los programas de financiamiento conforme a la variable recibida
//        Y se guarda los resultados en una variable
        $nuevo_nivel = $this->egresos_model->datos_nivel2($nivel1, $nombre);

//        Se crea un ciclo para imprimir los datos obtenidos en los elementos de una lista
        foreach($nuevo_nivel as $fila) {
            echo("<option title='".$fila->programa_de_financiamiento." - ".$fila->descripcion."' value='".$fila->programa_de_financiamiento."' >".$fila->programa_de_financiamiento." -  ".$fila->descripcion."</option>");
        }
    }

    /**
     * Esta funcion se encarga de obtener los datos del tercer nivel de la estructura de egresos
     */
    function obtener_nivel3() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha seleccionado el nivel 3');
//        Se llama a la funcion del model de egresos encargada de tomar los nombres delos niveles que existen
        $nombres_egresos = $this->egresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_egresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

//        Se toman los valores del primer y segundo nivel que son enviados por POST
        $nivel1 = $this->input->post("nivel1", TRUE);
        $nivel2 = $this->input->post("nivel2", TRUE);

//        Se llamam a la funcion del modelo que se encarga de buscar el tercer nivel conforme a la estructura de egresos
//        Y se guarda los resultados en una variable
        $nuevo_nivel = $this->egresos_model->datos_nivel3($nivel1, $nivel2, $nombre);

//        Se crea un ciclo para imprimir los datos obtenidos en los elementos de una lista
        foreach($nuevo_nivel as $row) {
            echo("<option title='".$row->centro_de_costos." - ".$row->descripcion."' value='".$row->centro_de_costos."' >".$row->centro_de_costos." -  ".$row->descripcion."</option>");
        }
    }

    /**
     * Esta funcion se encarga de obtener los datos del cuarto nivel de la estructura de egresos
     */
    function obtener_nivel4() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha seleccionado el nivel 4');
//        Se llama a la funcion del model de egresos encargada de tomar los nombres delos niveles que existen
        $nombres_egresos = $this->egresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_egresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

//        Se toman los valores del primer, segundo y tercer nivel que son enviados por POST
        $nivel1 = $this->input->post("nivel1");
        $nivel2 = $this->input->post("nivel2");
        $nivel3 = $this->input->post("nivel3");
//        Se llamam a la funcion del modelo que se encarga de buscar el cuarto nivel conforme a la estructura de egresos
//        Y se guarda los resultados en una variable
        $nuevo_nivel = $this->egresos_model->datos_nivel4($nivel1, $nivel2, $nivel3, $nombre);
//        Se crea un ciclo para imprimir los datos obtenidos en los elementos de una lista
        foreach($nuevo_nivel as $row) {
            echo("<option title='".$row->capitulo." - ".$row->descripcion."' value='".$row->capitulo."' >".$row->capitulo." -  ".$row->descripcion."</option>");
        }
    }

    /**
     * Esta funcion se encarga de obtener los datos del quinto nivel de la estructura de egresos
     */
    function obtener_nivel5() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha seleccionado el nivel 5');
//        Se llama a la funcion del model de egresos encargada de tomar los nombres delos niveles que existen
        $nombres_egresos = $this->egresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_egresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

//        Se toman los valores del primer, segundo, tercer y cuarto nivel que son enviados por POST
        $nivel1 = $this->input->post("nivel1");
        $nivel2 = $this->input->post("nivel2");
        $nivel3 = $this->input->post("nivel3");
        $nivel4 = $this->input->post("nivel4");

//        Se llamam a la funcion del modelo que se encarga de buscar el quinto nivel conforme a la estructura de egresos
//        Y se guarda los resultados en una variable
        $nuevo_nivel = $this->egresos_model->datos_nivel5($nivel1, $nivel2, $nivel3, $nivel4, $nombre);
//        Se crea un ciclo para imprimir los datos obtenidos en los elementos de una lista
        foreach($nuevo_nivel as $row) {
            echo("<option title='".$row->concepto." - ".$row->descripcion."' value='".$row->concepto."' >".$row->concepto." -  ".$row->descripcion."</option>");
        }
    }

    /**
     * Esta funcion se encarga de obtener los datos del sexto nivel de la estructura de egresos
     */
    function obtener_nivel6() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha seleccionado el nivel 6');
//        Se llama a la funcion del model de egresos encargada de tomar los nombres delos niveles que existen
        $nombres_egresos = $this->egresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_egresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

//        Se toman los valores del primer, segundo, tercer, cuarto y quinto nivel que son enviados por POST
        $nivel1 = $this->input->post("nivel1");
        $nivel2 = $this->input->post("nivel2");
        $nivel3 = $this->input->post("nivel3");
        $nivel4 = $this->input->post("nivel4");
        $nivel5 = $this->input->post("nivel5");

//        Se llamam a la funcion del modelo que se encarga de buscar el sexto nivel conforme a la estructura de egresos
//        Y se guarda los resultados en una variable
        $nuevo_nivel = $this->egresos_model->datos_nivel6($nivel1, $nivel2, $nivel3, $nivel4, $nivel5, $nombre);

//        Se crea un ciclo para imprimir los datos obtenidos en los elementos de una lista
        foreach($nuevo_nivel as $row) {
            echo("<option title='".$row->partida." - ".$row->descripcion."' value='".$row->partida."' >".$row->partida." -  ".$row->descripcion."</option>");
        }
    }

    /**
     * Esta funcion se encarga de buscar la informacion del ultimo nivel conforme a la estructura de egresos
     */
    function obtener_datosUltimoNivel() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha obtenido los datos del ultimo nivel');
//        Se llama a la funcion del model de egresos encargada de tomar los nombres delos niveles que existen
        $nombres_egresos = $this->egresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_egresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

//        Se toman los valores del primer, segundo, tercer, cuarto, quinto y sexto nivel que son enviados por POST
        $fecha = $this->input->post("fecha");
        $nivel1 = $this->input->post("nivel1");
        $nivel2 = $this->input->post("nivel2");
        $nivel3 = $this->input->post("nivel3");
        $nivel4 = $this->input->post("nivel4");
        $nivel5 = $this->input->post("nivel5");
        $nivel6 = $this->input->post("nivel6");

//        Se llama a la libreria de utilirerias, para preparar el mes actual
//        $mes_actual = $this->utilerias->preparar_mes_actual();
        $mes_actual = $this->utilerias->convertirFechaAMes($fecha);

//        Se llama a la funcion que se encarga de tomar los datos de la partida para desplegar la informacion necesaria
        $datos_nivel = $this->egresos_model->datos_de_ultimo_nivel($nivel1, $nivel2, $nivel3, $nivel4, $nivel5, $nivel6, $mes_actual, $nombre);

//        Se devuelven los valores en formato JSON para poder ingresarlos a la grafica de balance de dinero por mes
        echo(json_encode($datos_nivel));
    }

    /**
     * Esta funcion sirve para transformar el archivo .CSV que sube el usuario, para poder insertarlos a la base de datos en el formato adecuado
     * NOTA: Revisar porque cuando el archivo es muy grande, no se insertan todas las filas
     */
    function subir_niveles() {

//        Se prepara la configuracion de la libreria "Upload"
        $config['upload_path'] = './application/archivos/';
        $config['allowed_types'] = '*';

//        Se carga la libreria con sus configuraciones correspondientes
        $this->load->library('upload', $config);

//        Si el server no logra subir el archivo, despliega un mensaje de error
        if ( ! $this->upload->do_upload('archivoSubir')) {
//            Se capturan los errores en una variable y se imprimen para debug
            $error = array('error' => $this->upload->display_errors());
            $this->debugeo->imprimir_pre($error);
        }
        else {
            ini_set('memory_limit', '-1');
//            load our new PHPExcel library
            $this->load->library('excel');

//            Si el server logra subir el archivo, se toman los datos del archivo
            $data = $this->upload->data();

//            Se toma la ruta del archivo junto con su nombre y extension
            $file = $data["full_path"];

//            read file from path
            $objPHPExcel = PHPExcel_IOFactory::load($file);

//            get only the Cell Collection
            $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();

//            extract to a PHP readable array format
            foreach ($cell_collection as $cell) {
                $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
                $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
                $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();

//                header will/should be in row 1 only. of course this can be modified to suit your need.
                if ($row == 1) {
                    continue;
                } else {
                    $arr_data[$row][$column] = $data_value;
                }
            }

//            Se llama a la funcion del model de egresos encargada de tomar los nombres delos niveles que existen
            $nombres_egresos = $this->egresos_model->obtener_nombre_niveles();

//            En este arreglo se van a guardar los nombres de los niveles
            $nombre = array();

//            En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
            foreach($nombres_egresos as $fila) {
                array_push($nombre, $fila->descripcion);
            }

            $conteo = 0;

            foreach($arr_data as $datos) {
                $resultado = $this->egresos_model->insertar_niveles_egresos($datos, $nombre);
                $conteo += 1;
            }

//            Si hay un resultado positivo se redirecciona a la pagina de la estructura de egresos
            if($resultado) {
                redirect(base_url("egresos/estructura_rubros"));
            }
//            De lo contrario se imprime un error
            else {
                echo("Hubo un error al insertar los datos");
            }
        }

    }

    function buscar_nivel1() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha buscado el nivel 1');
//        Se llama a la funcion del model de egresos encargada de tomar los nombres delos niveles que existen
        $nombres_egresos = $this->egresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_egresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

//        Se toma la variable que se manda por POST
        $nivel_buscar1 = $this->input->post("nivel_buscar1", TRUE);

//        Se llamam a la funcion del modelo que se encarga de buscar los programas de financiamiento conforme a la variable recibida
//        Y se guarda los resultados en una variable
        $resultados = $this->egresos_model->buscar_nivel1($nivel_buscar1, $nombre);

//        Se crea un ciclo para imprimir los datos obtenidos en los elementos de una lista
        foreach($resultados as $fila) {
            echo("<option title='".$fila->fuente_de_financiamiento." - ".$fila->descripcion."' value='".$fila->fuente_de_financiamiento."' >".$fila->fuente_de_financiamiento." -  ".$fila->descripcion."</option>");
        }
    }

    function buscar_nivel2() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha buscado el nivel 2');
//        Se llama a la funcion del model de egresos encargada de tomar los nombres delos niveles que existen
        $nombres_egresos = $this->egresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_egresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

//        Se toman los valores del primer y segundo nivel que son enviados por POST
        $nivel_buscar1 = $this->input->post("nivel_buscar1", TRUE);
        $nivel_buscar2 = $this->input->post("nivel_buscar2", TRUE);

//        Se llamam a la funcion del modelo que se encarga de buscar el tercer nivel conforme a la estructura de egresos
//        Y se guarda los resultados en una variable
        $resultados = $this->egresos_model->buscar_nivel2($nivel_buscar1, $nivel_buscar2, $nombre);

//        Se crea un ciclo para imprimir los datos obtenidos en los elementos de una lista
        foreach($resultados as $row) {
            echo("<option title='".$row->programa_de_financiamiento." - ".$row->descripcion."' value='".$row->programa_de_financiamiento."' >".$row->programa_de_financiamiento." -  ".$row->descripcion."</option>");
        }
    }

    function buscar_nivel3() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha buscado el nivel 3');
//        Se llama a la funcion del model de egresos encargada de tomar los nombres delos niveles que existen
        $nombres_egresos = $this->egresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_egresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

//        Se toman los valores del primer, segundo y tercer nivel que son enviados por POST
        $nivel_buscar1 = $this->input->post("nivel_buscar1", TRUE);
        $nivel_buscar2 = $this->input->post("nivel_buscar2", TRUE);
        $nivel_buscar3 = $this->input->post("nivel_buscar3", TRUE);

//        Se llamam a la funcion del modelo que se encarga de buscar el cuarto nivel conforme a la estructura de egresos
//        Y se guarda los resultados en una variable
        $resultados = $this->egresos_model->buscar_nivel3($nivel_buscar1, $nivel_buscar2, $nivel_buscar3, $nombre);

//        Se crea un ciclo para imprimir los datos obtenidos en los elementos de una lista
        foreach($resultados as $row) {
            echo("<option title='".$row->centro_de_costos." - ".$row->descripcion."' value='".$row->centro_de_costos."' >".$row->centro_de_costos." -  ".$row->descripcion."</option>");
        }
    }

    function buscar_nivel4() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha buscado el nivel 4');
//        Se llama a la funcion del model de egresos encargada de tomar los nombres delos niveles que existen
        $nombres_egresos = $this->egresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_egresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

//        Se toman los valores del primer, segundo y tercer nivel que son enviados por POST
        $nivel_buscar1 = $this->input->post("nivel_buscar1", TRUE);
        $nivel_buscar2 = $this->input->post("nivel_buscar2", TRUE);
        $nivel_buscar3 = $this->input->post("nivel_buscar3", TRUE);
        $nivel_buscar4 = $this->input->post("nivel_buscar4", TRUE);

//        Se llamam a la funcion del modelo que se encarga de buscar el cuarto nivel conforme a la estructura de egresos
//        Y se guarda los resultados en una variable
        $resultados = $this->egresos_model->buscar_nivel4($nivel_buscar1, $nivel_buscar2, $nivel_buscar3, $nivel_buscar4, $nombre);

//        Se crea un ciclo para imprimir los datos obtenidos en los elementos de una lista
        foreach($resultados as $row) {
            echo("<option title='".$row->capitulo." - ".$row->descripcion."' value='".$row->capitulo."' >".$row->capitulo." -  ".$row->descripcion."</option>");
        }
    }

    function buscar_nivel5() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha buscado el nivel 5');
//        Se llama a la funcion del model de egresos encargada de tomar los nombres delos niveles que existen
        $nombres_egresos = $this->egresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_egresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

//        Se toman los valores del primer, segundo y tercer nivel que son enviados por POST
        $nivel_buscar1 = $this->input->post("nivel_buscar1", TRUE);
        $nivel_buscar2 = $this->input->post("nivel_buscar2", TRUE);
        $nivel_buscar3 = $this->input->post("nivel_buscar3", TRUE);
        $nivel_buscar4 = $this->input->post("nivel_buscar4", TRUE);
        $nivel_buscar5 = $this->input->post("nivel_buscar5", TRUE);

//        Se llamam a la funcion del modelo que se encarga de buscar el cuarto nivel conforme a la estructura de egresos
//        Y se guarda los resultados en una variable
        $resultados = $this->egresos_model->buscar_nivel5($nivel_buscar1, $nivel_buscar2, $nivel_buscar3, $nivel_buscar4, $nivel_buscar5, $nombre);

//        Se crea un ciclo para imprimir los datos obtenidos en los elementos de una lista
        foreach($resultados as $row) {
            echo("<option title='".$row->concepto." - ".$row->descripcion."' value='".$row->concepto."' >".$row->concepto." -  ".$row->descripcion."</option>");
        }
    }

    function buscar_nivel6() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha buscado el nivel 6');
//        Se llama a la funcion del model de egresos encargada de tomar los nombres delos niveles que existen
        $nombres_egresos = $this->egresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_egresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

//        Se toman los valores del primer, segundo y tercer nivel que son enviados por POST
        $nivel_buscar1 = $this->input->post("nivel_buscar1", TRUE);
        $nivel_buscar2 = $this->input->post("nivel_buscar2", TRUE);
        $nivel_buscar3 = $this->input->post("nivel_buscar3", TRUE);
        $nivel_buscar4 = $this->input->post("nivel_buscar4", TRUE);
        $nivel_buscar5 = $this->input->post("nivel_buscar5", TRUE);
        $nivel_buscar6 = $this->input->post("nivel_buscar6", TRUE);

//        Se llamam a la funcion del modelo que se encarga de buscar el cuarto nivel conforme a la estructura de egresos
//        Y se guarda los resultados en una variable
        $resultados = $this->egresos_model->buscar_nivel6($nivel_buscar1, $nivel_buscar2, $nivel_buscar3, $nivel_buscar4, $nivel_buscar5, $nivel_buscar6, $nombre);

//        Se crea un ciclo para imprimir los datos obtenidos en los elementos de una lista
        foreach($resultados as $row) {
            echo("<option title='".$row->partida." - ".$row->descripcion."' value='".$row->partida."' >".$row->partida." -  ".$row->descripcion."</option>");
        }
    }

    private function preparar_estructura_superior() {
//        Se llama la funcion del modelo de egresos encargado de contar los niveles que existen
        $total_egresos = $this->egresos_model->contar_egresos_elementos();
//        Se llama a la funcion del model de egresos encargada de tomar los nombres delos niveles que existen
        $nombres_egresos = $this->egresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_egresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

        $nombre_primerNivel = $this->egresos_model->datos_primerNivel($nombre[0]);

//        $this->debugeo->imprimir_pre($nombre_primerNivel);

        $niveles = '<div class="row" id="egresos_estrucura_superior">
                        <div class="col-lg-4">
                            <form role="form">
                                <div class="form-group" id="forma_nivel1_superior">
                                    <h5>Nivel 1</h5>
                                    <label>'.$nombre[0].'</label>
                                    <input type="text" class="form-control" id="buscar_nivel1_superior" placeholder="Buscar...">
                                    <select multiple class="form-control formas">';
        foreach($nombre_primerNivel as $fila) {
            $niveles .= '<option value="'.$fila->fuente_de_financiamiento.'" title="'.$fila->fuente_de_financiamiento.' - '.$fila->descripcion.'">'.$fila->fuente_de_financiamiento.' - '.$fila->descripcion.'</option>';
        }

        $niveles .= '                </select>
                                </div>
                            </form>
                        </div>';

        $a = 1;
        $num = 2;

        for($i = 1; $i < $total_egresos->conteo; $i++){
            $niveles .= '<div class="col-lg-4">
                            <form role="form">
                                <div class="form-group" id="forma_nivel'.$num.'_superior">
                                     <h5>Nivel '.$num.'</h5>
                                    <label>'.$nombre[$a].'</label>
                                    <input type="text" class="form-control" id="buscar_nivel'.$num.'_superior" placeholder="Buscar..." disabled>
                                    <select multiple class="form-control formas" id="select_nivel'.$num.'_superior">
                                    </select>
                                </div>
                            </form>
                        </div>';
            $a++;
            $num++;
        }

        $niveles .= '</div>';

        return $niveles;
    }

    private function preparar_estructura_inferior() {
//        Se llama la funcion del modelo de egresos encargado de contar los niveles que existen
        $total_egresos = $this->egresos_model->contar_egresos_elementos();
//        Se llama a la funcion del model de egresos encargada de tomar los nombres delos niveles que existen
        $nombres_egresos = $this->egresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_egresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

        $nombre_primerNivel = $this->egresos_model->datos_primerNivel($nombre[0]);

//        $this->debugeo->imprimir_pre($nombre_primerNivel);

        $niveles = '<div class="row" id="egresos_estrucura_inferior">
                        <div class="col-lg-4">
                            <form role="form">
                                <div class="form-group" id="forma_nivel1_inferior">
                                    <h5>Nivel 1</h5>
                                    <label>'.$nombre[0].'</label>
                                    <input type="text" class="form-control" id="buscar_nivel1_inferior" placeholder="Buscar...">
                                    <select multiple class="form-control formas">';
        foreach($nombre_primerNivel as $fila) {
            $niveles .= '<option value="'.$fila->fuente_de_financiamiento.'" title="'.$fila->fuente_de_financiamiento.' - '.$fila->descripcion.'">'.$fila->fuente_de_financiamiento.' - '.$fila->descripcion.'</option>';
        }

        $niveles .= '                </select>
                                </div>
                            </form>
                        </div>';

        $a = 1;
        $num = 2;

        for($i = 1; $i < $total_egresos->conteo; $i++){
            $niveles .= '<div class="col-lg-4">
                            <form role="form">
                                <div class="form-group" id="forma_nivel'.$num.'_inferior">
                                     <h5>Nivel '.$num.'</h5>
                                    <label>'.$nombre[$a].'</label>
                                    <input type="text" class="form-control" id="buscar_nivel'.$num.'_inferior" placeholder="Buscar..." disabled>
                                    <select multiple class="form-control formas" id="select_nivel'.$num.'_inferior">
                                    </select>
                                </div>
                            </form>
                        </div>';
            $a++;
            $num++;
        }

        $niveles .= '</div>';

        return $niveles;
    }

    private function input_nivelsuperior() {
//        Se llama la funcion del modelo de egresos encargado de contar los niveles que existen
        $total_egresos = $this->egresos_model->contar_egresos_elementos();
//        Se llama a la funcion del model de egresos encargada de tomar los nombres delos niveles que existen
        $nombres_egresos = $this->egresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_egresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

        $niveles = '';
        $num = 1;

        for($i = 0; $i < $total_egresos->conteo; $i++){
            $nom_niv = ucwords(strtolower($nombre[$i]));
            $nom_niv_1 = str_replace( "De" , "" , $nom_niv);
            $nom_niv_2 = str_replace( "Fuente" , "F." , $nom_niv_1);
            $nom_niv_f = str_replace( "Programa" , "P." , $nom_niv_2);
            $niveles .= '
                                <!-- <h5>Nivel '.$num.'</h5>
                                <label>'.$nombre[$i].'</label>-->
                                <input type="text" class="form-control" id="input_nivel'.$num.'_superior" placeholder="'.$nom_niv_f.'" >
                        ';
            $num++;
        }

        return $niveles;
    }

    private function input_nivelinferior() {
//        Se llama la funcion del modelo de egresos encargado de contar los niveles que existen
        $total_egresos = $this->egresos_model->contar_egresos_elementos();
//        Se llama a la funcion del model de egresos encargada de tomar los nombres delos niveles que existen
        $nombres_egresos = $this->egresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_egresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

        $niveles = '';
        $num = 1;

        for($i = 0; $i < $total_egresos->conteo; $i++){
            $nom_niv = ucwords(strtolower($nombre[$i]));
            $nom_niv_1 = str_replace( "De" , "" , $nom_niv);
            $nom_niv_2 = str_replace( "Fuente" , "F." , $nom_niv_1);
            $nom_niv_f = str_replace( "Programa" , "P." , $nom_niv_2);
            $niveles .= '
                                <!-- <h5>Nivel '.$num.'</h5>
                                <label>'.$nombre[$i].'</label>-->
                                <input type="text" class="form-control" id="input_nivel'.$num.'_inferior" placeholder="'.$nom_niv_f.'" >
                        ';
            $num++;
        }

        return $niveles;
    }

    function checar_datos() {
        $sql = "SELECT id_niveles, COLUMN_JSON(nivel) as estructura FROM cat_niveles WHERE id_niveles = '21941';";
        $query = $this->db->query($sql);
        $result = $query->result();
        foreach($result as $fila) {
            $this->debugeo->imprimir_pre($fila->id_niveles);
            $estructura = json_decode($fila->estructura);
            $this->debugeo->imprimir_pre($estructura);
        }

        /*$sql = "SELECT COLUMN_JSON(nivel) as estructura FROM cat_niveles WHERE id_niveles = '21941';";
        $query = $this->db->query($sql);
        $result = $query->result();
        foreach($result as $fila) {
            $estructura = json_decode($fila->estructura);
            $this->debugeo->imprimir_pre($estructura);
        }

        $sql = "SELECT *, COLUMN_JSON(estructura) as estructura FROM mov_adecuaciones_egresos_detalle WHERE numero_adecuacion = '1';";
        $query = $this->db->query($sql);
        $result = $query->result();
        foreach($result as $fila) {
            $estructura = json_decode($fila->estructura);
            $this->debugeo->imprimir_pre($estructura);
        }*/
    }

    function agregar_nivel() {
        $datos = $this->input->post();

        foreach ($datos as $key => $value) {
            $datos[$key] = trim($value);
        }

        try {

            $total_egresos = $this->egresos_model->contar_egresos_elementos();
//        Se llama a la funcion del model de egresos encargada de tomar los nombres delos niveles que existen
            $nombres_egresos = $this->egresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
            $nombres = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
            foreach($nombres_egresos as $fila) {
                array_push($nombres, $fila->descripcion);
            }

            $sql = "";

            if(isset($datos["nivel2"]) && $datos["nivel2"] == "") {

                $sql = "INSERT INTO cat_niveles (id_niveles,  nivel) VALUES (NULL, COLUMN_CREATE('" . strtolower(str_replace(' ', '_', $nombres[0])) . "','" . $datos["clave"] . "',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[1])) . "', '',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[2])) . "', '',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[3])) . "', '',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[4])) . "', '',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[5])) . "', '',
                                                                    'descripcion', '" . $datos["descripcion"] . "'));";

            }
            elseif(isset($datos["nivel3"]) && $datos["nivel3"] == "") {

                $sql = "INSERT INTO cat_niveles (id_niveles,  nivel) VALUES (NULL, COLUMN_CREATE('" . strtolower(str_replace(' ', '_', $nombres[0])) . "','" . $datos["nivel2"] . "',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[1])) . "', '" . $datos["clave"] . "',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[2])) . "', '',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[3])) . "', '',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[4])) . "', '',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[5])) . "', '',
                                                                    'descripcion', '" . $datos["descripcion"] . "'));";

            }
            elseif(isset($datos["nivel4"]) && $datos["nivel4"] == "") {

                $sql = "INSERT INTO cat_niveles (id_niveles,  nivel) VALUES (NULL, COLUMN_CREATE('" . strtolower(str_replace(' ', '_', $nombres[0])) . "','" . $datos["nivel2"] . "',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[1])) . "', '" . $datos["nivel3"] . "',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[2])) . "', '" . $datos["clave"] . "',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[3])) . "', '',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[4])) . "', '',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[5])) . "', '',
                                                                    'descripcion', '" . $datos["descripcion"] . "'));";

            }
            elseif(isset($datos["nivel5"]) && $datos["nivel5"] == "") {

                $sql = "INSERT INTO cat_niveles (id_niveles,  nivel) VALUES (NULL, COLUMN_CREATE('" . strtolower(str_replace(' ', '_', $nombres[0])) . "','" . $datos["nivel2"] . "',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[1])) . "', '" . $datos["nivel3"] . "',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[2])) . "', '" . $datos["nivel4"] . "',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[3])) . "', '" . $datos["clave"] . "',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[4])) . "', '',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[5])) . "', '',
                                                                    'descripcion', '" . $datos["descripcion"] . "'));";

            }
            elseif(isset($datos["nivel6"]) && $datos["nivel6"] == "") {

                $sql = "INSERT INTO cat_niveles (id_niveles,  nivel) VALUES (NULL, COLUMN_CREATE('" . strtolower(str_replace(' ', '_', $nombres[0])) . "','" . $datos["nivel2"] . "',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[1])) . "', '" . $datos["nivel3"] . "',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[2])) . "', '" . $datos["nivel4"] . "',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[3])) . "', '" . $datos["nivel5"] . "',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[4])) . "', '" . $datos["clave"] . "',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[5])) . "', '',
                                                                    'descripcion', '" . $datos["descripcion"] . "'));";

            }
            else {

                $sql_fuente = "SELECT descripcion FROM cat_clasificador_fuentes_financia WHERE codigo = ?;";
                $query = $this->db->query($sql_fuente, array($datos["nivel2"]));
                $resultado = $query->row_array();

                $sql = "INSERT INTO cat_niveles (id_niveles,  nivel) VALUES (NULL, COLUMN_CREATE('" . strtolower(str_replace(' ', '_', $nombres[0])) . "','" . $datos["nivel2"] . "',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[1])) . "', '" . $datos["nivel3"] . "',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[2])) . "', '" . $datos["nivel4"] . "',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[3])) . "', '" . $datos["nivel5"] . "',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[4])) . "', '" . $datos["nivel6"] . "',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[5])) . "', '" . $datos["clave"] . "',
                                                                    'descripcion', '" . $datos["descripcion"] . "',
                                                                    'enero_inicial', 0.0,
                                                                    'febrero_inicial', 0.0,
                                                                    'marzo_inicial', 0.0,
                                                                    'abril_inicial', 0.0,
                                                                    'mayo_inicial', 0.0,
                                                                    'junio_inicial', 0.0,
                                                                    'julio_inicial', 0.0,
                                                                    'agosto_inicial', 0.0,
                                                                    'septiembre_inicial', 0.0,
                                                                    'octubre_inicial', 0.0,
                                                                    'noviembre_inicial', 0.0,
                                                                    'diciembre_inicial', 0.0,
                                                                    'enero_precompromiso', 0.0,
                                                                    'febrero_precompromiso', 0.0,
                                                                    'marzo_precompromiso', 0.0,
                                                                    'abril_precompromiso', 0.0,
                                                                    'mayo_precompromiso', 0.0,
                                                                    'junio_precompromiso', 0.0,
                                                                    'julio_precompromiso', 0.0,
                                                                    'agosto_precompromiso', 0.0,
                                                                    'septiembre_precompromiso', 0.0,
                                                                    'octubre_precompromiso', 0.0,
                                                                    'noviembre_precompromiso', 0.0,
                                                                    'diciembre_precompromiso', 0.0,
                                                                    'enero_compromiso', 0.0,
                                                                    'febrero_compromiso', 0.0,
                                                                    'marzo_compromiso', 0.0,
                                                                    'abril_compromiso', 0.0,
                                                                    'mayo_compromiso', 0.0,
                                                                    'junio_compromiso', 0.0,
                                                                    'julio_compromiso', 0.0,
                                                                    'agosto_compromiso', 0.0,
                                                                    'septiembre_compromiso', 0.0,
                                                                    'octubre_compromiso', 0.0,
                                                                    'noviembre_compromiso', 0.0,
                                                                    'diciembre_compromiso', 0.0,
                                                                    'enero_devengado', 0.0,
                                                                    'febrero_devengado', 0.0,
                                                                    'marzo_devengado', 0.0,
                                                                    'abril_devengado', 0.0,
                                                                    'mayo_devengado', 0.0,
                                                                    'junio_devengado', 0.0,
                                                                    'julio_devengado', 0.0,
                                                                    'agosto_devengado', 0.0,
                                                                    'septiembre_devengado', 0.0,
                                                                    'octubre_devengado', 0.0,
                                                                    'noviembre_devengado', 0.0,
                                                                    'diciembre_devengado', 0.0,
                                                                    'enero_ejercido', 0.0,
                                                                    'febrero_ejercido', 0.0,
                                                                    'marzo_ejercido', 0.0,
                                                                    'abril_ejercido', 0.0,
                                                                    'mayo_ejercido', 0.0,
                                                                    'junio_ejercido', 0.0,
                                                                    'julio_ejercido', 0.0,
                                                                    'agosto_ejercido', 0.0,
                                                                    'septiembre_ejercido', 0.0,
                                                                    'octubre_ejercido', 0.0,
                                                                    'noviembre_ejercido', 0.0,
                                                                    'diciembre_ejercido', 0.0,
                                                                    'enero_saldo', 0.0,
                                                                    'febrero_saldo', 0.0,
                                                                    'marzo_saldo', 0.0,
                                                                    'abril_saldo', 0.0,
                                                                    'mayo_saldo', 0.0,
                                                                    'junio_saldo', 0.0,
                                                                    'julio_saldo', 0.0,
                                                                    'agosto_saldo', 0.0,
                                                                    'septiembre_saldo', 0.0,
                                                                    'octubre_saldo', 0.0,
                                                                    'noviembre_saldo', 0.0,
                                                                    'diciembre_saldo', 0.0,
                                                                    'total_anual', 0.0,
                                                                    'precomprometido_anual', 0.0,
                                                                    'comprometido_anual', 0.0,
                                                                    'devengado_anual', 0.0,
                                                                    'ejercido_anual', 0.0,
                                                                    'clave_financiamiento', '".$resultado["descripcion"]."'));";
            }

            $insertar_nivel = $this->egresos_model->insertar_nivel($sql);

            if($insertar_nivel) {
                $respuesta = array(
                    "mensaje" => '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Se ha insertado el nuevo nivel con éxito.</div>',
                );
            } else {
                throw new Exception('Ha ocurrido un error al tratar de insertar el nivel, por favor contacte a su administrador.');
            }

            echo(json_encode($respuesta));

        } catch (Exception $e) {

            $respuesta = array(
                "mensaje" => '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$e->getMessage().'</div>',
            );

            echo(json_encode($respuesta));
        }

    }

    function editar_nivel() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha editado un nivel ');
        $datos = $this->input->post();

        foreach ($datos as $key => $value) {
            $datos[$key] = trim($value);
        }

        try {

            $total_egresos = $this->egresos_model->contar_egresos_elementos();
//        Se llama a la funcion del model de egresos encargada de tomar los nombres delos niveles que existen
            $nombres_egresos = $this->egresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
            $nombres = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
            foreach($nombres_egresos as $fila) {
                array_push($nombres, $fila->descripcion);
            }

            $sql = "";

            if(isset($datos["nivel3"]) && $datos["nivel3"] == "") {

                $sql = "UPDATE cat_niveles SET nivel = COLUMN_ADD(nivel, '" . strtolower(str_replace(' ', '_', $nombres[0])) . "','" . $datos["clave"] . "',
                                                                    'descripcion', '" . $datos["descripcion"] . "')
                                                                    WHERE COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[0])) . "' as char) = '" . $datos["clave_anterior"] . "'
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[1])) . "' as char) = ''
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[2])) . "' as char) = ''
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[3])) . "' as char) = ''
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[4])) . "' as char) = ''
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[5])) . "' as char) = ''
                                                                    AND COLUMN_GET(nivel, 'descripcion' as char) = '". $datos["descripcion_anterior"] ."' ;";

                $sql .= "UPDATE cat_niveles SET nivel = COLUMN_ADD(nivel, '" . strtolower(str_replace(' ', '_', $nombres[0])) . "','" . $datos["clave"] . "')
                                                                    WHERE COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[0])) . "' as char) = '" . $datos["clave_anterior"] . "' ;";

            }
            elseif(isset($datos["nivel4"]) && $datos["nivel4"] == "") {

                $sql = "UPDATE cat_niveles SET nivel = COLUMN_ADD(nivel, '" . strtolower(str_replace(' ', '_', $nombres[1])) . "','" . $datos["clave"] . "',
                                                                    'descripcion', '" . $datos["descripcion"] . "')
                                                                    WHERE COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[0])) . "' as char) = " . $datos["nivel2"] . "'
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[1])) . "' as char) = '" . $datos["nivel3"] . "'
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[2])) . "' as char) = ''
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[3])) . "' as char) = ''
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[4])) . "' as char) = ''
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[5])) . "' as char) = ''
                                                                    AND COLUMN_GET(nivel, 'descripcion' as char) = '". $datos["descripcion_anterior"] ."' ;";

            }
            elseif(isset($datos["nivel5"]) && $datos["nivel5"] == "") {

                $sql = "UPDATE cat_niveles SET nivel = COLUMN_ADD(nivel, '" . strtolower(str_replace(' ', '_', $nombres[2])) . "','" . $datos["clave"] . "',
                                                                    'descripcion', '" . $datos["descripcion"] . "')
                                                                    WHERE COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[0])) . "' as char) = '" . $datos["nivel2"] . "'
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[1])) . "' as char) = '" . $datos["nivel3"] . "'
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[2])) . "' as char) = '" . $datos["nivel4"] . "'
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[3])) . "' as char) = ''
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[4])) . "' as char) = ''
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[5])) . "' as char) = ''
                                                                    AND COLUMN_GET(nivel, 'descripcion' as char) = '". $datos["descripcion_anterior"] ."' ;";

            }
            elseif(isset($datos["nivel6"]) && $datos["nivel6"] == "") {

                $sql = "UPDATE cat_niveles SET nivel = COLUMN_ADD(nivel, '" . strtolower(str_replace(' ', '_', $nombres[3])) . "','" . $datos["clave"] . "',
                                                                    'descripcion', '" . $datos["descripcion"] . "')
                                                                    WHERE COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[0])) . "' as char) = '" . $datos["nivel2"] . "'
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[1])) . "' as char) = '" . $datos["nivel3"] . "'
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[2])) . "' as char) = '" . $datos["nivel4"] . "'
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[3])) . "' as char) = '" . $datos["nivel5"] . "'
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[4])) . "' as char) = ''
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[5])) . "' as char) = ''
                                                                    AND COLUMN_GET(nivel, 'descripcion' as char) = '". $datos["descripcion_anterior"] ."' ;";

            }
            elseif(isset($datos["nivel7"]) && $datos["nivel7"] == "") {

                $sql = "UPDATE cat_niveles SET nivel = COLUMN_ADD(nivel, '" . strtolower(str_replace(' ', '_', $nombres[4])) . "','" . $datos["clave"] . "',
                                                                    'descripcion', '" . $datos["descripcion"] . "')
                                                                    WHERE COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[0])) . "' as char) = '" . $datos["nivel2"] . "'
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[1])) . "' as char) = '" . $datos["nivel3"] . "'
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[2])) . "' as char) = '" . $datos["nivel4"] . "'
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[3])) . "' as char) = '" . $datos["nivel5"] . "'
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[4])) . "' as char) = '" . $datos["nivel6"] . "'
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[5])) . "' as char) = '',
                                                                    AND COLUMN_GET(nivel, 'descripcion' as char) = '". $datos["descripcion_anterior"] ."' ;";

            }
            else {

                $sql = "UPDATE cat_niveles SET nivel = COLUMN_ADD(nivel, '" . strtolower(str_replace(' ', '_', $nombres[5])) . "','" . $datos["clave"] . "',
                                                                    'descripcion', '" . $datos["descripcion"] . "')
                                                                    WHERE COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[0])) . "' as char) = '" . $datos["nivel2"] . "'
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[1])) . "' as char) = '" . $datos["nivel3"] . "'
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[2])) . "' as char) = '" . $datos["nivel4"] . "'
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[3])) . "' as char) = '" . $datos["nivel5"] . "'
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[4])) . "' as char) = '" . $datos["nivel6"] . "'
                                                                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombres[5])) . "' as char) = '" . $datos["nivel7"] . "'
                                                                    AND COLUMN_GET(nivel, 'descripcion' as char) = '". $datos["descripcion_anterior"] ."' ;";
            }

//            $this->debugeo->imprimir_pre($sql);

            $insertar_nivel = $this->egresos_model->editar_nivel($sql);

            if($insertar_nivel) {
                $respuesta = array(
                    "mensaje" => '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Se ha editado el nivel con éxito.</div>',
                );
            } else {
                throw new Exception('Ha ocurrido un error al tratar de editar el nivel, por favor contacte a su administrador.');
            }

            echo(json_encode($respuesta));

        } catch (Exception $e) {

            $respuesta = array(
                "mensaje" => '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$e->getMessage().'</div>',
            );

            echo(json_encode($respuesta));
        }

    }

}