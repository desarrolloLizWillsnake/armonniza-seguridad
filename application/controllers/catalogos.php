<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*use
    DataTables\Editor,
    DataTables\Editor\Field,
    DataTables\Editor\Format,
    DataTables\Editor\Join,
    DataTables\Editor\Validate;*/

class Catalogos extends CI_Controller {

    /**
     * Se revisa si el usuario esta logueado, si no esta logueado, se reenvia a la pantalla de login
     */
    function __construct() {
        parent::__construct();
        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        }
    }

    /**
     * Esta funcion es la principal, donde se muestra la pantalla de reportes
     */
    function index() {
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Catalogos",
            "usuario" => $this->tank_auth->get_username(),
        );
        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/main_view');
        $this->load->view('front/footer_main_view', array("graficas" => TRUE));
    }

    /** Aqui empieza la sección de Catalogos */
    function indicecatalogos() {
        if($this->utilerias->get_permisos("catalogos")|| $this->utilerias->get_grupo() == 1) {
            $datos_header = array(
                "titulo_pagina" => "Armonniza | Catalogos",
                "usuario" => $this->tank_auth->get_username(),
                "tablas" => TRUE,
            );

            $this->parser->parse('front/header_main_view', $datos_header);
            $this->load->view('front/indices_catalogos_view');
            $this->load->view('front/footer_main_view', array("tablas" => TRUE, "indicecatalogos" => TRUE));
        } else {
            redirect('/auth/login');
        }

    }

    function proveedores() {
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Catálogo Proveedores",
            "usuario" => $this->tank_auth->get_username(),
            "editar_tabla" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/catalogos/catalogo_proveedores_view');
        $this->load->view('front/footer_main_view', array( "exportar_tablas" => TRUE, "catalogo_proveedores" => TRUE));
    }

    function tabla_proveedores() {
        include( "./assets/datatables/extensions/Editor-1.3.3/php/DataTables.php" );

        DataTables\Editor::inst( $db, 'cat_proveedores' )
            ->pkey( 'id_proveedores' )
            ->fields(
                DataTables\Editor\Field::inst( 'clave_prov' )->validator( 'DataTables\Editor\Validate::notEmpty' ),
                DataTables\Editor\Field::inst( 'nombre' )->validator( 'DataTables\Editor\Validate::notEmpty' ),
                DataTables\Editor\Field::inst( 'calle' ),
                DataTables\Editor\Field::inst( 'telefono' ),
                DataTables\Editor\Field::inst( 'no_exterior' ),
                DataTables\Editor\Field::inst( 'no_interior' ),
                DataTables\Editor\Field::inst( 'colonia' ),
                DataTables\Editor\Field::inst( 'cp' ),
                DataTables\Editor\Field::inst( 'ciudad' ),
                DataTables\Editor\Field::inst( 'delegacion_municipio' ),
                DataTables\Editor\Field::inst( 'pais' ),
                DataTables\Editor\Field::inst( 'email' ),
                DataTables\Editor\Field::inst( 'RFC' )->validator( 'DataTables\Editor\Validate::notEmpty' ),
                DataTables\Editor\Field::inst( 'cuenta' ),
                DataTables\Editor\Field::inst( 'clabe' ),
                DataTables\Editor\Field::inst( 'banco' ),
                DataTables\Editor\Field::inst( 'pago' ),
                DataTables\Editor\Field::inst( 'mypyme' ),
                DataTables\Editor\Field::inst( 'observaciones' )
            )
            ->process( $_POST )
            ->json();

//        $this->debugeo->imprimir_pre($resultado);

    }

    function beneficiarios() {
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Catálogo Beneficiarios",
            "usuario" => $this->tank_auth->get_username(),
            "editar_tabla" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/catalogos/catalogo_beneficiarios_view');
        $this->load->view('front/footer_main_view', array( "exportar_tablas" => TRUE, "catalogo_beneficiarios" => TRUE));
    }

    function tabla_beneficiarios() {
        include( "./assets/datatables/extensions/Editor-1.3.3/php/DataTables.php" );

        DataTables\Editor::inst( $db, 'cat_beneficiarios' )
            ->pkey( 'id_beneficiarios' )
            ->fields(
                DataTables\Editor\Field::inst( 'numero_empleado' )->validator( 'DataTables\Editor\Validate::notEmpty' ),
                DataTables\Editor\Field::inst( 'nombre' )->validator( 'DataTables\Editor\Validate::notEmpty' ),
                DataTables\Editor\Field::inst( 'adscripcion' ),
                DataTables\Editor\Field::inst( 'cargo' )
            )
            ->process( $_POST )
            ->json();
//        $this->debugeo->imprimir_pre($resultado);

    }

    function bancos() {
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Catálogo Bancos",
            "usuario" => $this->tank_auth->get_username(),
            "editar_tabla" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/catalogos/catalogo_bancos_view');
        $this->load->view('front/footer_main_view', array( "exportar_tablas" => TRUE, "catalogo_bancos" => TRUE));
    }

    function tabla_bancos() {
        include( "./assets/datatables/extensions/Editor-1.3.3/php/DataTables.php" );

        $resultado = DataTables\Editor::inst( $db, 'cat_cuentas_bancarias' )
            ->pkey( 'id_cuentas_bancarias' )
            ->fields(
                DataTables\Editor\Field::inst( 'id_cuentas_bancarias' )->validator( 'DataTables\Editor\Validate::notEmpty' ),
                DataTables\Editor\Field::inst( 'cuenta' )->validator( 'DataTables\Editor\Validate::numeric' ),
                DataTables\Editor\Field::inst( 'banco' )->validator( 'DataTables\Editor\Validate::notEmpty' ),
                DataTables\Editor\Field::inst( 'cta_contable' )->validator( 'DataTables\Editor\Validate::notEmpty' ),
                DataTables\Editor\Field::inst( 'saldo' )->validator( 'DataTables\Editor\Validate::numeric' )
            )
            ->process( $_POST )
            ->json();

//        $this->debugeo->imprimir_pre($resultado);

    }

    function cucop() {
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Clasificador CUCOP",
            "usuario" => $this->tank_auth->get_username(),
            "editar_tabla" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/catalogos/clasificador_cucop_view');
        $this->load->view('front/footer_main_view', array( "exportar_tablas" => TRUE, "clasificador_cucop" => TRUE));
    }

    function tabla_cucop() {
        include( "./assets/datatables/extensions/Editor-1.3.3/php/DataTables.php" );

        $resultado = DataTables\Editor::inst( $db, 'cat_conceptos_gasto' )
            ->pkey( 'id_conceptos_gasto' )
            ->fields(
                DataTables\Editor\Field::inst( 'id_conceptos_gasto' )->validator( 'DataTables\Editor\Validate::notEmpty' ),
                DataTables\Editor\Field::inst( 'articulo' )->validator( 'DataTables\Editor\Validate::notEmpty' ),
                DataTables\Editor\Field::inst( 'descripcion' )->validator( 'DataTables\Editor\Validate::notEmpty' ),
                DataTables\Editor\Field::inst( 'unidad' )->validator( 'DataTables\Editor\Validate::notEmpty' )
            )
            ->process( $_POST )
            ->json();

//        $this->debugeo->imprimir_pre($resultado);

    }

    function conceptos_bancarios() {
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Catálogo Conceptos Bancarios",
            "usuario" => $this->tank_auth->get_username(),
            "editar_tabla" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/catalogos/catalogo_concepto_bancario_view');
        $this->load->view('front/footer_main_view', array( "exportar_tablas" => TRUE, "catalogo_conceptos" => TRUE));
    }

    function tabla_conceptos() {
        include( "./assets/datatables/extensions/Editor-1.3.3/php/DataTables.php" );

        $resultado = DataTables\Editor::inst( $db, 'cat_bancos_conceptos' )
            ->pkey( 'id_cat_bancos_conceptos' )
            ->fields(
                DataTables\Editor\Field::inst( 'id_cat_bancos_conceptos' )->validator( 'DataTables\Editor\Validate::notEmpty' ),
                DataTables\Editor\Field::inst( 'concepto' )->validator( 'DataTables\Editor\Validate::notEmpty' )
            )
            ->process( $_POST )
            ->json();

        //       $this->debugeo->imprimir_pre($resultado);

    }

    function cadena_random_proveedor() {
        $random = '';

        do {
            $random = "A".substr(str_shuffle("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 5);
            $this->db->select('clave_prov')->from('cat_proveedores')->where('clave_prov', $random);
            $query = $this->db->get();
        } while ($query->row() != NULL);

        echo(json_encode(array("respuesta" => $random)));
    }

    function get_unidades_medida() {
        $this->db->select('clave, descripcion')->from('cat_unidad_medida');
        $query = $this->db->get();
        echo(json_encode($query->result_array()));
    }

    function medicamentos() {
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Medicamentos",
            "usuario" => $this->tank_auth->get_username(),
            "editar_tabla" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/catalogos/catalogo_medicamentos_view');
        $this->load->view('front/footer_main_view', array( "exportar_tablas" => TRUE, "catalogo_medicamentos" => TRUE));
    }

    function tabla_medicamentos() {
        include( "./assets/datatables/extensions/Editor-1.3.3/php/DataTables.php" );

        $resultado = DataTables\Editor::inst( $db, 'cat_medicamentos' )
            ->pkey( 'id_medicamentos' )
            ->fields(
                DataTables\Editor\Field::inst( 'numero' )->validator( 'DataTables\Editor\Validate::notEmpty' ),
                DataTables\Editor\Field::inst( 'clave' )->validator( 'DataTables\Editor\Validate::notEmpty' ),
                DataTables\Editor\Field::inst( 'grupo_terapeutico' )->validator( 'DataTables\Editor\Validate::notEmpty' ),
                DataTables\Editor\Field::inst( 'nombre_generico' )->validator( 'DataTables\Editor\Validate::notEmpty' ),
                DataTables\Editor\Field::inst( 'gi' )
            )
            ->process( $_POST )
            ->json();

        //       $this->debugeo->imprimir_pre($resultado);

    }

}