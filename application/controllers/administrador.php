<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require( './assets/datatables/scripts/ssp.class.php' );

class Administrador extends CI_Controller {

    /**
     * Se revisa si el usuario esta logueado, si no esta logueado, se reenvia a la pantalla de login
     */
    function __construct() {
        parent::__construct();
        if (!$this->tank_auth->is_logged_in() || $this->utilerias->get_grupo() != 1) {
            redirect('/auth/login/');
        }
        $this->sql_details = array(
            'user' => 'root',
            'pass' => '',
            'db'   => 'software',
            'host' => 'localhost',
        );
    }
    

    function index() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha Entrado al Administrador');
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Administrador",
            "usuario" => $this->tank_auth->get_username(),
        );
        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/admin/main_view');
        $this->load->view('front/footer_main_view');
    }

    function perfil() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha verificado el perfil');
        $datos_header = array(
            "titulo_pagina" => "Armonniza  | Perfil Administrador",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/admin/perfil_view');
        $this->load->view('front/footer_main_view', array("tablas" => TRUE));

    }

    /**Estas funciones tienen relación con los datos de la Empresa*/

    function empresa() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha ingresado a los datos de la empresa');
        $query = "SELECT * FROM datos_empresa WHERE id_datos_empresa = 1";
        $resultado = $this->administrador_model->get_datos_empresa($query);
//        $this->debugeo->imprimir_pre($resultado);

        $datos_header = array(
            "titulo_pagina" => "Armonniza  | Empresa",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/admin/empresa_view', $resultado);
        $this->load->view('front/footer_main_view', array("tablas" => TRUE, "empresa" => TRUE));

    }

    function insertar_datos_empresa() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha insertado datos de la empresa.');
//        Se inicializa la variable que contiene la respuesta de la inserción de datos
        $respuesta = array();

//        Se toman los datos enviados
        $datos = $this->input->post();

        foreach ($datos as $key => $value) {
            $datos[$key]=trim($value);
        }

        try {
            if (!$datos) {
                throw new Exception('Faltan datos.');
            }

//            Se insertan los datos de la caratula del compromiso
            $resultado_insertar = $this->administrador_model->insertar_datos_empresa($datos);

//            Si el resultado es exitoso, se le indica al usuario
            if($resultado_insertar) {
                $respuesta = array(
                    "mensaje" => '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-check-circle fa-2x ic-msj"></i> Datos insertados correctamente.</div>',
                );

            }
//        De lo contrario, se manda un error al usuario
            else {
                if (!$datos) {
                    throw new Exception('Hubo un error al insertar los datos.');
                }
            }

            echo(json_encode($respuesta));

        } catch (Exception $e) {
            $respuesta = array(
                "mensaje" => '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-times-circle fa-2x ic-msj"></i> '.$e->getMessage().'</div>',
            );

            echo(json_encode($respuesta));
        }
    }

    function do_upload() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha cambiado la imagen de la empresa');
            $fecha = date("Y-m-d");
            $hora = date('h:i:s A');
            $config['file_name'] = 'logotipo/'.$fecha.'.png';
            $config['upload_path'] = './img/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = '30';
            $config['max_width'] = '320';
            $config['max_height'] = '480';

            $directorio = './img/logo2.png';

            $this->load->library('upload', $config);

        try {
            if (!$this->upload->do_upload()) {
                throw new Exception('Imagen no válida.');
                //$error = array('error' => $this->upload->display_errors());
                //$this->debugeo->imprimir_pre($error);
            } else {
                $file_info = $this->upload->data();
                // $this->debugeo->imprimir_pre($file_info);
                if (move_uploaded_file($_FILES['userfile']['tmp_name'], $directorio)) {

                    //USAMOS LA FUNCIÓN create_thumbnail Y LE PASAMOS EL NOMBRE DE LA IMAGEN,
                    //ASÍ YA TENEMOS LA IMAGEN REDIMENSIONADA
                    $this->_create_thumbnail($file_info['file_name']);
                    $data = array('upload_data' => $this->upload->data());
                    $titulo = $this->input->post('titulo');
                    $imagen = 'logotipo/' . $fecha . '/' . $hora;
                    $subir = $this->administrador_model->subir($titulo, $imagen);
                    $data['titulo'] = $titulo;
                    $data['imagen'] = $imagen;
                    //    Si el resultado es exitoso, se le indica al usuario
                    if ($subir) {
                        $respuesta = array(
                            "mensaje" => '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Imagen insertada correctamente.</div>',
                        );
                    } //        De lo contrario, se manda un error al usuario
                    else {
                        if (!$datos) {
                            throw new Exception('Hubo un error al insertar los datos.');
                        }
                    }

                    echo(json_encode($respuesta));
                }
            }

        } catch (Exception $e) {
            $respuesta = array(
                "mensaje" => '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$e->getMessage().'</div>',
            );

            echo(json_encode($respuesta));
        }

    }
    //FUNCIÓN PARA CREAR LA MINIATURA A LA MEDIDA QUE LE DIGAMOS
    function _create_thumbnail($filename){
        $config['image_library'] = 'gd2';
        //CARPETA EN LA QUE ESTÁ LA IMAGEN A REDIMENSIONAR
        $config['source_image'] = 'img/'.$filename;
        $config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = TRUE;
        //CARPETA EN LA QUE GUARDAMOS LA MINIATURA
        $config['new_image']='img/';
        $config['width'] = 100;
        $config['height'] = 100;
        $this->load->library('image_lib', $config);
        $this->image_lib->resize();
    }

    /**Estas funciones tienen relación con Usuarios*/

    function usuarios() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha ingresado un usuario');
        $datos_header = array(
            "titulo_pagina" => "Armonniza  | Usuarios",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/admin/usuarios_view');
        $this->load->view('front/footer_main_view', array("tablas" => TRUE, "usuarios" => TRUE));

    }

    function tabla_usuarios() {
        $resultado = $this->administrador_model->datos_usuariosCaratula();
//        Se crea el arreglo de datos donde se va a guardar la información
        $output = array("data" => "");

//        Se recorre el arreglo fila por fila
        foreach($resultado as $fila) {

            $estatus = '';
            $opciones = '';

            if($fila->activated == 1 && $fila->banned == 0) {
                $estatus = '<button type="button" class="btn btn-estatus btn-success" disabled>Activo</button>';

                $opciones = '<a href="'.base_url("administrador/ver_usuario/".$fila->id_datos_usuario).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                             <a href="'.base_url("administrador/editar_usuario/".$fila->id_datos_usuario).'" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                             <a href="'.base_url("administrador/privilegios_usuario/".$fila->id_datos_usuario).'" data-tooltip="Privilegios"><i class="fa fa-bars"></i></a>
                             <a data-toggle="modal" data-target=".modal_restaurar_contraseña" data-tooltip="Restaurar Contraseña"><i class="fa fa-refresh"></i></a>
                             <a data-toggle="modal" data-target=".modal_baja" data-tooltip="Dar de Baja"><i class="fa fa-user-times"></i></a>
                             <a data-toggle="modal" data-target=".modal_bloquear" data-tooltip="Bloquear"><i class="fa fa-times-circle-o"></i></a>';
            } elseif($fila->activated == 1 && $fila->banned == 1){
                $estatus = '<button type="button" class="btn btn-estatus btn-warning" disabled>Bloqueado</button>';

                $opciones = '<a href="'.base_url("administrador/ver_usuario/".$fila->id_datos_usuario).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                             <a href="'.base_url("administrador/editar_usuario/".$fila->id_datos_usuario).'" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                             <a href="'.base_url("administrador/privilegios_usuario/".$fila->id_datos_usuario).'" data-tooltip="Privilegios"><i class="fa fa-bars"></i></a>
                             <a data-toggle="modal" data-target=".modal_restaurar_contraseña" data-tooltip="Restaurar Contraseña"><i class="fa fa-refresh"></i></a>
                             <a data-toggle="modal" data-target=".modal_baja" data-tooltip="Dar de Baja"><i class="fa fa-user-times"></i></a>
                             <a data-toggle="modal" data-target=".modal_desbloquear" data-tooltip="Desbloquear"><i class="fa fa-check-circle-o"></i></a>';

            } elseif($fila->activated == 0) {
                $estatus = '<button type="button" class="btn btn-estatus btn-danger" disabled>Inactivo</button>';

                $opciones = '<a href="' . base_url("administrador/ver_usuario/" . $fila->id_datos_usuario) . '" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                             <a href="' . base_url("administrador/editar_usuario/" . $fila->id_datos_usuario) . '" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                             <a href="' . base_url("administrador/privilegios_usuario/" . $fila->id_datos_usuario) . '" data-tooltip="Privilegios"><i class="fa fa-bars"></i></a>
                             <a data-toggle="modal" data-target=".modal_activar" data-tooltip="Activar"><i class="fa fa-user"></i></a>';
            }

            $output["data"][] = array(
                $fila->id,
                $fila->nombre." ".$fila->apellido_paterno." ".$fila->apellido_materno,
                $fila->puesto,
                $fila->email,
                $fila->created,
                $estatus,
                $opciones,
            );

        }

        echo json_encode($output);

    }

    function ver_usuario($usuario = NULL) {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha consultado a los usuarios' .$usuario );

        $query = "SELECT u1.*, u2.id, u2.username, u2.email, u2.activated, u2.banned, u2.created
                  FROM datos_usuario u1
                  INNER JOIN users u2 ON u1.id_usuario = u2.id
				  WHERE u1.id_datos_usuario = ?;";

        $resultado = $this->administrador_model->get_datos_usuarios($usuario, $query);
//        $this->debugeo->imprimir_pre($resultado);

        $datos_header = array(
            "titulo_pagina" => "Armonniza | Ver Usuario",
            "usuario" => $this->tank_auth->get_username(),
            "usuariocss" => TRUE,
            "tablas" => TRUE,
        );

        $resultado->ultimo = $usuario;

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/admin/ver_usuarios_view', $resultado);
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "ver_usuario" => TRUE,
        ));
    }

    function privilegios_usuario($usuario = NULL) {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha consultado a los privilegios de los usuarios' .$usuario
        );

        $query = "SELECT gu.*, g.nombre_grupo, f.firma FROM grupos_usuarios gu
                  INNER JOIN grupos g ON gu.id_grupo = g.id_grupos
                  LEFT JOIN firmas f ON gu.id_usuario = f.id_usuario
                  WHERE gu.id_usuario = ?;";
        $resultado = $this->administrador_model->get_datos_usuarios($usuario, $query);
//        $this->debugeo->imprimir_pre($resultado);
        $datos_header = array(
            "titulo_pagina" => "Armonniza  | Privilegios Usuarios",
            "usuario" => $this->tank_auth->get_username(),
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/admin/editar_privilegios_view', $resultado);
        $this->load->view('front/footer_main_view', array(
            "privilegios_usuarios" => TRUE,
        ));

    }

    function editar_usuario($usuario = NULL) {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha editado a los usuarios'.$usuario);

        $query = "SELECT u1.*, u2.id, u2.username, u2.email, u2.activated, u2.banned, u2.created
                  FROM datos_usuario u1
                  INNER JOIN users u2 ON u1.id_usuario = u2.id
				  WHERE u1.id_datos_usuario = ?;";

        $resultado = $this->administrador_model->get_datos_usuarios($usuario, $query);
//      $this->debugeo->imprimir_pre($resultado);

        $datos_header = array(
            "titulo_pagina" => "Armonniza | Editar Usuarios",
            "usuario" => $this->tank_auth->get_username(),
            "usuariocss" => TRUE,
            "tablas" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/admin/editar_usuarios_view', $resultado);
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "editar_usuarios" => TRUE,
        ));

    }

    function bloquear_usuario() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha bloqueado a usuarios'.$usuario);
        $mensaje = array(
            "mensaje" => "",
        );

//      Se toma el devengado a cancelar
        $usuario = $this->input->post("usuario");

//      Se hace el query para tomar los datos de caratula del devengado
        $query_caratula = "SELECT * FROM users WHERE id = ?";

//      Se llama a la función para tomar los datos de la caratula del devengado
        $datos_caratula = $this->administrador_model->get_datos_usuarios($usuario, $query_caratula);

//      Se actualiza el estatus del devengado a cancelado

        $query = "UPDATE users SET banned = 1 WHERE id = ?; ";
        $fin = $this->administrador_model->actualizacion_estado_usuario($usuario, $query);

        if($fin) {
            $mensaje["mensaje"] = "ok";
            echo(json_encode($mensaje));
        }
        else {
            $mensaje["mensaje"] = "error";
            echo(json_encode($mensaje));
        }

    }

    function desbloquear_usuario() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha desbloqueado a al usuario' .$usuario);
        $mensaje = array(
            "mensaje" => "",
        );

//      Se toma el devengado a cancelar
        $usuario = $this->input->post("usuario");

//      Se hace el query para tomar los datos de caratula del devengado
        $query_caratula = "SELECT * FROM users WHERE id = ?";

//      Se llama a la función para tomar los datos de la caratula del devengado
        $datos_caratula = $this->administrador_model->get_datos_usuarios($usuario, $query_caratula);

//      Se actualiza el estatus del devengado a cancelado

        $query = "UPDATE users SET banned = 0 WHERE id = ?; ";
        $fin = $this->administrador_model->actualizacion_estado_usuario($usuario, $query);

        if($fin) {
            $mensaje["mensaje"] = "ok";
            echo(json_encode($mensaje));
        }
        else {
            $mensaje["mensaje"] = "error";
            echo(json_encode($mensaje));
        }

    }

    function baja_usuario() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', el usuario ha sido dado de baja por al usuario' .$usuario);
        $mensaje = array(
            "mensaje" => "",
        );

//      Se toma el devengado a cancelar
        $usuario = $this->input->post("usuario");

//      Se hace el query para tomar los datos de caratula del devengado
        $query_caratula = "SELECT * FROM users WHERE id = ?";

//      Se llama a la función para tomar los datos de la caratula del devengado
        $datos_caratula = $this->administrador_model->get_datos_usuarios($usuario, $query_caratula);

//      Se actualiza el estatus del devengado a cancelado

        $query = "UPDATE users SET activated = 0 WHERE id = ?; ";
        $fin = $this->administrador_model->actualizacion_estado_usuario($usuario, $query);

        if($fin) {
            $mensaje["mensaje"] = "ok";
            echo(json_encode($mensaje));
        }
        else {
            $mensaje["mensaje"] = "error";
            echo(json_encode($mensaje));
        }

    }

    function activar_usuario() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', el usuario ha sido activado' .$usuario);
        $mensaje = array(
            "mensaje" => "",
        );

//      Se toma el devengado a cancelar
        $usuario = $this->input->post("usuario");

//      Se hace el query para tomar los datos de caratula del devengado
        $query_caratula = "SELECT * FROM users WHERE id = ?";

//      Se llama a la función para tomar los datos de la caratula del devengado
        $datos_caratula = $this->administrador_model->get_datos_usuarios($usuario, $query_caratula);

//      Se actualiza el estatus del devengado a cancelado

        $query = "UPDATE users SET activated = 1 WHERE id = ?; ";
        $fin = $this->administrador_model->actualizacion_estado_usuario($usuario, $query);

        if($fin) {
            $mensaje["mensaje"] = "ok";
            echo(json_encode($mensaje));
        }
        else {
            $mensaje["mensaje"] = "error";
            echo(json_encode($mensaje));
        }

    }

    function resetear_contrasena() {
        $mensaje = array(
            "mensaje" => "",
        );
//      Se toma el devengado a cancelar
        $usuario = $this->input->post("usuario");
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha resseteado la contraseña de un usuario' .$usuario);
        $new_password = "$2a$16$"."eccUydh3hZhupag/4RzsWuKBZkf44g/QY42akoAC.2ndGmFhb97SS";

//      Se hace el query para tomar los datos de caratula del devengado
        $query_caratula = "SELECT * FROM users WHERE id = ?";

//      Se llama a la función para tomar los datos de la caratula del devengado
        $datos_caratula = $this->administrador_model->get_datos_usuarios($usuario, $query_caratula);

//      Se actualiza el estatus del devengado a cancelado

//        $query = "UPDATE users SET password = '.$new_password.' WHERE id = ?; ";
        $fin = $this->administrador_model->restaurarContrasenaUsuario($new_password, $usuario);
//      $fin = $this->tank_auth->users->change_password($usuario, $new_pass);
        if($fin) {
            $mensaje["mensaje"] = "ok";
            echo(json_encode($mensaje));
        }
        else {
            $mensaje["mensaje"] = "error";
            echo(json_encode($mensaje));
        }

    }

    function insertar_usuario($usuario = NULL) {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha insertado a un usuario' .$usuario);
//        Se inicializa la variable que contiene la respuesta de la inserción de datos
        $respuesta = array();

//        Se toman los datos enviados
        $datos = $this->input->post();

        foreach ($datos as $key => $value) {
            $datos[$key]=trim($value);
        }

        try {
            if (!$datos) {
                throw new Exception('Faltan datos.');
            }
//
//            Se insertan los datos de la caratula del compromiso
            $resultado_insertar = $this->administrador_model->insertar_caratula_usuario($datos);

//            Si el resultado es exitoso, se le indica al usuario
            if($resultado_insertar) {
                $respuesta = array(
                    "mensaje" => '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Datos insertados correctamente.</div>',
                );

            }
//        De lo contrario, se manda un error al usuario
            else {
                if (!$datos) {
                    throw new Exception('Hubo un error al insertar los datos.');
                }
            }

            echo(json_encode($respuesta));

        } catch (Exception $e) {
            $respuesta = array(
                "mensaje" => '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$e->getMessage().'</div>',
            );

            echo(json_encode($respuesta));
        }
    }

    function editar_privilegios($usuario = NULL) {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha editado los privilegios de un usuario' .$usuario);
//        Se inicializa la variable que contiene la respuesta de la inserción de datos
        $respuesta = array();

//        Se toman los datos enviados
        $datos = $this->input->post();

        try {

            $this->administrador_model->actualizar_grupo_usuario($datos["usuario"], $datos["grupo"] );

//            Se insertan los datos de la caratula del compromiso
            $resultado_insertar = $this->administrador_model->insertar_privilegios_usuario($datos["datos"], $datos["usuario"]);

//            Si el resultado es exitoso, se le indica al usuario
            if($resultado_insertar) {
                $respuesta = array(
                    "mensaje" => '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Datos insertados correctamente.</div>',
                );

            }
//        De lo contrario, se manda un error al usuario
            else {
                if (!$datos) {
                    throw new Exception('Hubo un error al insertar los datos.');
                }
            }

            echo(json_encode($respuesta));

        } catch (Exception $e) {
            $respuesta = array(
                "mensaje" => '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$e->getMessage().'</div>',
            );

            echo(json_encode($respuesta));
        }
    }

    /**Estas funciones tienen relación Catálogos*/

    function catalogos() {
        $datos_header = array(
            "titulo_pagina" => "Armonniza  | Catálogos",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/admin/catalogos_view');
        $this->load->view('front/footer_main_view', array("tablas" => TRUE));

    }

    function articulos() {
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Leyes & Artículos",
            "usuario" => $this->tank_auth->get_username(),
            "editar_tabla" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/catalogos/catalogo_articulos_view');
        $this->load->view('front/footer_main_view', array( "exportar_tablas" => TRUE, "catalogo_artiulos" => TRUE));
    }

    function tabla_aticulos() {
        include( "./assets/datatables/extensions/Editor-1.3.3/php/DataTables.php" );

        $resultado = DataTables\Editor::inst( $db, 'articulos' )
            ->pkey( 'id_articulos' )
            ->fields(
                DataTables\Editor\Field::inst( 'ley' )->validator( 'DataTables\Editor\Validate::notEmpty' ),
                DataTables\Editor\Field::inst( 'articulo' )->validator( 'DataTables\Editor\Validate::notEmpty' )
            )
            ->process( $_POST )
            ->json();

//        $this->debugeo->imprimir_pre($resultado);

    }

    function librerias() {
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Librerías",
            "usuario" => $this->tank_auth->get_username(),
            "editar_tabla" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/catalogos/catalogo_librerias_view');
        $this->load->view('front/footer_main_view', array( "exportar_tablas" => TRUE, "catalogo_librerias" => TRUE));
    }

    function tabla_librerias() {
        include( "./assets/datatables/extensions/Editor-1.3.3/php/DataTables.php" );

        $resultado = DataTables\Editor::inst( $db, 'lugar_de_entrega' )
            ->pkey( 'id_lugar_de_entrega' )
            ->fields(
                DataTables\Editor\Field::inst( 'id_lugar_de_entrega' )->validator( 'DataTables\Editor\Validate::numeric' ),
                DataTables\Editor\Field::inst( 'lugar_de_entrega' )->validator( 'DataTables\Editor\Validate::notEmpty' )
            )
            ->process( $_POST )
            ->json();

//        $this->debugeo->imprimir_pre($resultado);

    }

    function autorizaciones() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha otorgado autorizaciones');
        $datos_header = array(
            "titulo_pagina" => "Armonniza  | Autorizaciones",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/admin/autorizaciones_view');
        $this->load->view('front/footer_main_view', array("tablas" => TRUE));

    }

    function autorizaciones_ciclo() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha otorgado autorizaciones');
        $datos_header = array(
            "titulo_pagina" => "Armonniza  | Autorizaciones",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/admin/autorizaciones_ciclo_view');
        $this->load->view('front/footer_main_view');

    }

    function firmas_ciclo() {
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Autorizaciones",
            "usuario" => $this->tank_auth->get_username(),
            "editar_tabla" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/admin/firmas_ciclo_view');
        $this->load->view('front/footer_main_view', array( "exportar_tablas" => TRUE, "firmas_ciclo" => TRUE));
    }

    function firmas_autorizador_ciclo() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha otorgado autorizaciones');

        $query = "SELECT * FROM cat_autorizadores_firmas WHERE id_persona = 3;";
        $resultado = $this->administrador_model->get_datos_empresa($query);

        $datos_header = array(
            "titulo_pagina" => "Armonniza  | Autorizaciones",
            "usuario" => $this->tank_auth->get_username(),
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/admin/firmas_autorizador_ciclo_view', $resultado);
        $this->load->view('front/footer_main_view', array(
            "editar_autorizador" => TRUE,
        ));

    }

    function actualizar_autorizador_ciclo() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha actualizado un autorizador en Ciclo Presupuestario' .$datos);
//        Se inicializa la variable que contiene la respuesta de la inserción de datos
        $respuesta = array();

//        Se toman los datos enviados
        $datos = $this->input->post();

        foreach ($datos as $key => $value) {
            $datos[$key]=trim($value);
        }

        try {
            if (!$datos) {
                throw new Exception('Faltan datos.');
            }
//
//            Se insertan los datos de la caratula del compromiso
            $resultado_insertar = $this->administrador_model->insertar_datos_autorizador($datos);

//            Si el resultado es exitoso, se le indica al usuario
            if($resultado_insertar) {
                $respuesta = array(
                    "mensaje" => '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Datos insertados correctamente.</div>',
                );

            }
//        De lo contrario, se manda un error al usuario
            else {
                if (!$datos) {
                    throw new Exception('Hubo un error al insertar los datos.');
                }
            }

            echo(json_encode($respuesta));

        } catch (Exception $e) {
            $respuesta = array(
                "mensaje" => '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$e->getMessage().'</div>',
            );

            echo(json_encode($respuesta));
        }
    }

    function autorizaciones_contabilidad() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha otorgado autorizaciones' );
        $datos_header = array(
            "titulo_pagina" => "Armonniza  | Autorizaciones",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/admin/autorizaciones_contabilidad_view');
        $this->load->view('front/footer_main_view');

    }

    function firmas_autorizador_contabilidad() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha otorgado autorizaciones');

        $query = "SELECT * FROM cat_autorizadores_firmas WHERE id_persona = 1;";
        $resultado = $this->administrador_model->get_datos_empresa($query);

        $datos_header = array(
            "titulo_pagina" => "Armonniza  | Autorizaciones",
            "usuario" => $this->tank_auth->get_username(),
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/admin/firmas_autorizador_contabilidad_view', $resultado);
        $this->load->view('front/footer_main_view', array(
            "editar_autorizador" => TRUE,
        ));

    }

    function actualizar_autorizador_contabilidad() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha actualizado un autorizador en Contabilidad' .$datos);
//        Se inicializa la variable que contiene la respuesta de la inserción de datos
        $respuesta = array();

//        Se toman los datos enviados
        $datos = $this->input->post();

        foreach ($datos as $key => $value) {
            $datos[$key]=trim($value);
        }

        try {
            if (!$datos) {
                throw new Exception('Faltan datos.');
            }
//
//            Se insertan los datos de la caratula del compromiso
            $resultado_insertar = $this->administrador_model->insertar_datos_autorizador_contabilidad($datos);

//            Si el resultado es exitoso, se le indica al usuario
            if($resultado_insertar) {
                $respuesta = array(
                    "mensaje" => '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Datos insertados correctamente.</div>',
                );

            }
//        De lo contrario, se manda un error al usuario
            else {
                if (!$datos) {
                    throw new Exception('Hubo un error al insertar los datos.');
                }
            }

            echo(json_encode($respuesta));

        } catch (Exception $e) {
            $respuesta = array(
                "mensaje" => '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$e->getMessage().'</div>',
            );

            echo(json_encode($respuesta));
        }
    }

    function firmas_vobo_contabilidad() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha otorgado firmas');

        $query = "SELECT * FROM cat_autorizadores_firmas WHERE id_persona = 2;";
        $resultado = $this->administrador_model->get_datos_empresa($query);

        $datos_header = array(
            "titulo_pagina" => "Armonniza  | Autorizaciones",
            "usuario" => $this->tank_auth->get_username(),
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/admin/firmas_vobo_contabilidad_view', $resultado);
        $this->load->view('front/footer_main_view', array(
            "editar_autorizador" => TRUE,
        ));

    }

    function actualizar_vobo_contabilidad() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha actualizado un autorizador en Contabilidad' .$datos);
//        Se inicializa la variable que contiene la respuesta de la inserción de datos
        $respuesta = array();

//        Se toman los datos enviados
        $datos = $this->input->post();

        foreach ($datos as $key => $value) {
            $datos[$key]=trim($value);
        }

        try {
            if (!$datos) {
                throw new Exception('Faltan datos.');
            }
//
//            Se insertan los datos de la caratula del compromiso
            $resultado_insertar = $this->administrador_model->insertar_datos_vobo_contabilidad($datos);

//            Si el resultado es exitoso, se le indica al usuario
            if($resultado_insertar) {
                $respuesta = array(
                    "mensaje" => '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Datos insertados correctamente.</div>',
                );

            }
//        De lo contrario, se manda un error al usuario
            else {
                if (!$datos) {
                    throw new Exception('Hubo un error al insertar los datos.');
                }
            }

            echo(json_encode($respuesta));

        } catch (Exception $e) {
            $respuesta = array(
                "mensaje" => '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$e->getMessage().'</div>',
            );

            echo(json_encode($respuesta));
        }
    }

    function tabla_firmas_ciclo() {
        include( "./assets/datatables/extensions/Editor-1.3.3/php/DataTables.php" );

        $resultado = DataTables\Editor::inst( $db, 'cat_personas_firma' )
            ->pkey( 'id_personas_firma' )
            ->fields(
                DataTables\Editor\Field::inst( 'id_personas_firma' )->validator( 'DataTables\Editor\Validate::numeric' ),
                DataTables\Editor\Field::inst( 'nombre' )->validator( 'DataTables\Editor\Validate::notEmpty' ),
                DataTables\Editor\Field::inst( 'puesto' )->validator( 'DataTables\Editor\Validate::notEmpty' )
            )
            ->process( $_POST )
            ->json();

//        $this->debugeo->imprimir_pre($resultado);

    }

    function firmas_contabilidad() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha verifcado las firmas de contabilidad');
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Autorizaciones",
            "usuario" => $this->tank_auth->get_username(),
            "editar_tabla" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/admin/firmas_contabilidad_view');
        $this->load->view('front/footer_main_view', array( "exportar_tablas" => TRUE, "firmas_contabilidad" => TRUE));
    }

    function tabla_firmas_contabilidad() {
        include( "./assets/datatables/extensions/Editor-1.3.3/php/DataTables.php" );

        $resultado = DataTables\Editor::inst( $db, 'cat_polizas_firmas' )
            ->pkey( 'id_persona' )
            ->fields(
                DataTables\Editor\Field::inst( 'id_persona' )->validator( 'DataTables\Editor\Validate::numeric' ),
                DataTables\Editor\Field::inst( 'nombre' )->validator( 'DataTables\Editor\Validate::notEmpty' ),
                DataTables\Editor\Field::inst( 'puesto' )->validator( 'DataTables\Editor\Validate::notEmpty' )
            )
            ->process( $_POST )
            ->json();

//        $this->debugeo->imprimir_pre($resultado);

    }

    function config_estructuras() {
        $query = "SELECT * FROM datos_estructura WHERE id_clave = 1";
        $resultado = $this->administrador_model->get_datos_empresa($query);

        $datos_header = array(
            "titulo_pagina" => "Armonniza  | Estructuras Administrativas",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/admin/configuracion_estructuras_view.php', $resultado);
        $this->load->view('front/footer_main_view', array("adminEstructuras" => TRUE));

    }

    function insertar_datos_estructura() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha actualizado datos de la estructura.');
//        Se inicializa la variable que contiene la respuesta de la inserción de datos
        $respuesta = array();

//        Se toman los datos enviados
        $datos = $this->input->post();

        foreach ($datos as $key => $value) {
            $datos[$key]=trim($value);
        }

        try {
            if (!$datos) {
                throw new Exception('Faltan datos.');
            }

//            Se insertan los datos de la caratula del compromiso
            $resultado_insertar = $this->administrador_model->insertar_datos_estructura($datos);

//            Si el resultado es exitoso, se le indica al usuario
            if($resultado_insertar) {
                $respuesta = array(
                    "mensaje" => '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-check-circle fa-2x ic-msj"></i> Datos insertados correctamente.</div>',
                );

            }
//        De lo contrario, se manda un error al usuario
            else {
                if (!$datos) {
                    throw new Exception('Hubo un error al insertar los datos.');
                }
            }

            echo(json_encode($respuesta));

        } catch (Exception $e) {
            $respuesta = array(
                "mensaje" => '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-times-circle fa-2x ic-msj"></i> '.$e->getMessage().'</div>',
            );

            echo(json_encode($respuesta));
        }
    }

    function config_contabilidad() {
        $datos_header = array(
            "titulo_pagina" => "Armonniza  | Contabilidad",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/admin/configuracion_contabilidad_view.php');
        $this->load->view('front/footer_main_view', array("adminEstructuras" => TRUE));

    }

    function subir_saldos_iniciales() {

        $respuesta = array('mensaje' => "" );

        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

//        Se prepara la configuracion de la libreria "Upload"
        $config['upload_path'] = './application/archivos/';
        $config['allowed_types'] = '*';

//        Se carga la libreria con sus configuraciones correspondientes
        $this->load->library('upload', $config);

//        Si el server no logra subir el archivo, despliega un mensaje de error
        if (!$this->upload->do_upload('archivoSubir')) {
//            Se capturan los errores en una variable y se imprimen para debug
            $error = array('error' => $this->upload->display_errors());
            $this->debugeo->imprimir_pre($error);
        } else {
            ini_set('memory_limit', '-1');
//            load our new PHPExcel library
            $this->load->library('excel');
//            Si el server logra subir el archivo, se toman los datos del archivo
            $data = $this->upload->data();

//            Se toma la ruta del archivo junto con su nombre y extension
            $file = $data["full_path"];

//            read file from path
            $objPHPExcel = PHPExcel_IOFactory::load($file);

//            get only the Cell Collection
            $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();

            try {

//                extract to a PHP readable array format
                foreach ($cell_collection as $cell) {
                    $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
                    $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
                    $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();

//                    header will/should be in row 1 only. of course this can be modified to suit your need.
                    if ($row == 1) {
                        continue;
                    } else {
                        $datos_detalle[$row][$column] = trim($data_value);
                    }
                }


                $linea = 2;

                $this->db->trans_begin();

                $sql = "SET FOREIGN_KEY_CHECKS = 0;";
                $this->db->query($sql);

                $sql = "TRUNCATE TABLE cat_cuentas_contables;";
                $this->db->query($sql);

                $sql = "SET FOREIGN_KEY_CHECKS = 1;";
                $truncar = $this->db->query($sql);

                if($truncar) {

                    foreach ($datos_detalle as $key => $value) {

                        // $this->debugeo->imprimir_pre($value);

                        $datos_insertar = array(
                            'cuenta' => $value["A"],
                            'nombre' => $value["B"],
                            'tipo' => $value["C"],
                            'saldo_inicial' => $value["D"],
                            'cargo' => 0,
                            'abono' => 0,
                            'saldo' => 0,
                            'descripcion' => $value["B"],
                        );

                        $this->db->insert('cat_cuentas_contables', $datos_insertar);

                        $linea += 1;
                    }

                    if ($this->db->trans_status() === FALSE) {
                        $this->db->trans_rollback();
                        throw new Exception('Ha ocurrido un error al insertar los datos, por favor intentelo de nuevo.');
                    } else {
                        $this->db->trans_commit();
                        for($i = 1; $i <=9; $i++) {
                            $this->acomodar_cuentas($i);
                        }
                        
                    }

                }

                $respuesta["mensaje"] = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>El archivo ha subido correctamente.</div>';


            } catch (Exception $e) {
                $this->db->trans_rollback();

                $respuesta["mensaje"] = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$e->getMessage().'</div>';

            }

            $datos_header = array(
                "titulo_pagina" => "Armonniza | Resultado Archivo",
                "usuario" => $this->tank_auth->get_username(),
                "precompromisocss" => TRUE,
            );

            $this->parser->parse('front/header_main_view', $datos_header);
            $this->load->view('front/resultado_archivo_view', $respuesta);
            $this->load->view('front/footer_main_view');

        }
    }

    private function acomodar_cuentas($cuenta) {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

        // $this->benchmark->mark('code_start');

        $this->db->trans_begin();

        $sql_cuentas = "SELECT * FROM cat_cuentas_contables WHERE cuenta LIKE ? ;";
        $query_cuentas = $this->db->query($sql_cuentas, array($cuenta . "%"));
        $cuentas_contables = $query_cuentas->result_array();

        foreach ($cuentas_contables as $key => $value) {
            $padre = $this->BuscarPadre($value["cuenta"]);
            $nivel = $this->SetNivel($value["cuenta"]);

            $datos_actualizar = array(
                'cuenta_padre' => $padre,
                'nivel' => $nivel,
            );

            $this->db->where('cuenta', $value["cuenta"]);
            $this->db->update('cat_cuentas_contables', $datos_actualizar);
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }

        // $this->benchmark->mark('code_end');
        // echo $this->benchmark->elapsed_time('code_start', 'code_end');
    }

    private function BuscarPadre($ctaSeek = NULL) {
        $posicion_punto = strrpos($ctaSeek, '.', -1);
        $cuenta_cortada = substr($ctaSeek, 0, $posicion_punto);
        if (!$cuenta_cortada) {
            $cuenta_cortada = 0;
        }
        return $cuenta_cortada;
    }

    private function SetNivel($cuenta = NULL) {
        $arreglo_cuenta = explode(".", $cuenta);
        $nivel = count($arreglo_cuenta);
        return $nivel;
    }

    function unidades_medida() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha verificado las unidades de medida');
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Configuración",
            "usuario" => $this->tank_auth->get_username(),
            "editar_tabla" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/catalogos/catalogo_unidades_medida_view');
        $this->load->view('front/footer_main_view', array( "exportar_tablas" => TRUE, "catalogo_medidas" => TRUE));
    }

    function tabla_unidades_medida() {
        include( "./assets/datatables/extensions/Editor-1.3.3/php/DataTables.php" );

        $resultado = DataTables\Editor::inst( $db, 'cat_unidad_medida' )
            ->pkey( 'id_unidad_medida' )
            ->fields(
                DataTables\Editor\Field::inst( 'clave' )->validator( 'DataTables\Editor\Validate::notEmpty'),
                DataTables\Editor\Field::inst( 'descripcion' )->validator( 'DataTables\Editor\Validate::notEmpty' )
            )
            ->process( $_POST )
            ->json();

//        $this->debugeo->imprimir_pre($resultado);

    }

    function subir_archivo() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha subido un archivo');

//        Se prepara la configuracion de la libreria "Upload"
        $config['upload_path'] = './application/archivos/';
        $config['allowed_types'] = '*';

//        Se carga la libreria con sus configuraciones correspondientes
        $this->load->library('upload', $config);

//        Si el server no logra subir el archivo, despliega un mensaje de error
        if ( ! $this->upload->do_upload('archivoSubir')) {
//            Se capturan los errores en una variable y se imprimen para debug
            $error = array('error' => $this->upload->display_errors());
            $this->debugeo->imprimir_pre($error);
        }
        else {
            try {
                ini_set('memory_limit', '-1');
//            load our new PHPExcel library
                $this->load->library('excel');
//            Si el server logra subir el archivo, se toman los datos del archivo
                $data = $this->upload->data();

//            Se toma la ruta del archivo junto con su nombre y extension
                $file = $data["full_path"];

//            read file from path
                $objPHPExcel = PHPExcel_IOFactory::load($file);

//            get only the Cell Collection
                $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();

                $this->benchmark->mark('code_start');

//            extract to a PHP readable array format
                foreach ($cell_collection as $cell) {
                    $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
                    $fila = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
                    $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();

//                header will/should be in row 1 only. of course this can be modified to suit your need.
                    if ($fila == 1) {
                        continue;
                    } else {
//                        $this->debugeo->imprimir_pre($data_value);
                        $datos_iniciales[$fila][$column] = trim($data_value);
                    }
                }

                foreach($datos_iniciales as $key => $value) {
                    if(isset($value["B"]) == TRUE && strpos($value["B"], "/")) {
//                        $this->debugeo->imprimir_pre($value);
                        $datos_encabezado[] = $value;
                    } else {
                        $datos_detalle[] = $value;
                    }

                }

//                $this->debugeo->imprimir_pre($datos_encabezado);
//                $this->debugeo->imprimir_pre($datos_detalle);

                unset($key);
                unset($value);

//                        Se llama la funcion del modelo de egresos encargado de contar los niveles que existen
                $total_egresos = $this->egresos_model->contar_egresos_elementos();
//                        Se llama a la funcion del model de egresos encargada de tomar los nombres delos niveles que existen
                $nombres_egresos = $this->egresos_model->obtener_nombre_niveles();

//                        En este arreglo se van a guardar los nombres de los niveles
                $nombre = array();

//                        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
                foreach($nombres_egresos as $row) {
                    array_push($nombre, $row->descripcion);
                }

                $last_compromiso = 0;

                foreach($datos_encabezado as $key => $value) {
                    $this->debugeo->imprimir_pre("Datos del encabezado: ");
                    $this->debugeo->imprimir_pre($value);

//                    Se toma el numero del ultimo precompromiso
                    $ultimo_compromiso = $this->ciclo_model->ultimo_compromiso();

                    if($ultimo_compromiso) {
                        $last_compromiso = $ultimo_compromiso->ultimo + 1;
                    }
                    else {
                        $last_compromiso = 1;
                    }


                    $ultimo_impresion = $this->ciclo_model->ultimo_tipoimpresion(array(
                        "tipo" => $value["V"],
                    ));

                    if($ultimo_impresion) {
                        $numero_impresion = $ultimo_impresion->ultimo + 1;
                    }
                    else {
                        $numero_impresion = 1;
                    }

                    $fecha = "";

                    $fecha_cadena = explode("/", $value["B"]);

                    switch($fecha_cadena[1]) {
                        case "Abr":
                            $fecha_inicial = "2015-04-".$fecha_cadena[0];
                            $time = strtotime($fecha_inicial);
                            $fecha = date('Y-m-d', $time);
                            break;
                        case "May":
                            $fecha_inicial = "2015-05-".$fecha_cadena[0];
                            $time = strtotime($fecha_inicial);
                            $fecha = date('Y-m-d', $time);
                            break;
                        case 5:
                            $fecha_inicial = "2015-05-".$fecha_cadena[0];
                            $time = strtotime($fecha_inicial);
                            $fecha = date('Y-m-d', $time);
                            break;
                    }

                    $total_compromiso = 0;

                    if(isset($value["U"])) {
                        $query_beneficiarios = "SELECT nombre, adscripcion, cargo FROM cat_beneficiarios WHERE numero_empleado = ?;";
                        $resultado_query_beneficiario = $this->db->query($query_beneficiarios, array($value["U"]));
                        $nombre_beneficiario = $resultado_query_beneficiario->row_array();
                        $value["T"] = "";

                        if(!$nombre_beneficiario) {
                            throw new Exception('El codigo del beneficiario '.$value["U"].' no existe dentro de la base de datos.');
                        }

                    } else {
                        $value["U"] = "";
                        $nombre_beneficiario["nombre"] = "";
                        $nombre_beneficiario["adscripcion"] = "";
                        $nombre_beneficiario["cargo"] = "";
                    }

                    $id_caratula_compromiso = $this->ciclo_model->insertar_caratula_compromisoArchivoMasivo(array(
                        "A" => $value["M"],
                        "B" => $value["T"],
                        "C" => "Oficinas Centrales",
                        "D" => "N/A",
                        "E" => "Gasto Corriente",
                        "F" => $fecha,
                        "G" => $value["N"],
                        "H" => $value["V"],
                        "numero_impresion" => $numero_impresion,
                        "I" => $nombre_beneficiario["nombre"],
                        "J" => $value["U"],
                        "K" => $nombre_beneficiario["cargo"],
                        "L" => $nombre_beneficiario["adscripcion"],
                        "M" => 101,
                        "N" => "Cheque",
                        "O" => "N/A",
                        "P" => "",
                        "Q" => 0,
                        "R" => 0,
                        "S" => 0,
                    ), $last_compromiso);

                    foreach($datos_detalle as $key_detalle => $value_detalle) {
//                        $this->debugeo->imprimir_pre("Datos del detalle: ");
//                        $this->debugeo->imprimir_pre($value_detalle);

                        if($value_detalle["A"] == $value["A"]) {

                            $query = "SELECT id_niveles FROM cat_niveles WHERE COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[0]))."' as char) = ? ";

                            for($i = 1; $i < $total_egresos->conteo; $i++){
                                $query .= "AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[$i]))."' as char) = ? ";
                            }

                            $existe = $this->ciclo_model->existeEstructura(array(
                                "nivel1" => $value_detalle["F"],
                                "nivel2" => $value_detalle["G"],
                                "nivel3" => $value_detalle["H"],
                                "nivel4" => $value_detalle["I"],
                                "nivel5" => $value_detalle["J"],
                                "nivel6" => $value_detalle["K"] ), $query);

                            if(!$existe) {
                                throw new Exception('La estructura '.$value_detalle["F"].' '.$value_detalle["G"].' '.$value_detalle["H"].' '.$value_detalle["I"].' '.$value_detalle["J"].' '.$value_detalle["K"].' no existe.');
                            } else {

                                $value_detalle["id_nivel"] = $existe->id_niveles;
                                $value_detalle["subtotal"] =  round($value_detalle["P"] - $value_detalle["Q"], 2);
                                $value_detalle["iva"] =  round($value_detalle["R"], 2);
                                $value_detalle["importe"] =  round($value_detalle["subtotal"] + $value_detalle["iva"], 2);

                                $total_compromiso += $value_detalle["importe"];

                                $query_insertar_detalle_compromiso = "INSERT INTO mov_compromiso_detalle (numero_compromiso, id_nivel, gasto, unidad_medida, cantidad, p_unitario, subtotal, iva, importe, titulo, mov_compromiso_detalle.year, especificaciones, nivel ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, COLUMN_CREATE(";

                                for($i = 0; $i < $total_egresos->conteo; $i++){
                                    $query_insertar_detalle_compromiso .= "'".strtolower(str_replace(' ', '_', $nombre[$i]))."', ?, ";
                                }

                                $query_insertar_detalle_compromiso .= "'gasto', ?));";

                                $query_gasto = "SELECT descripcion, unidad FROM cat_conceptos_gasto WHERE articulo = ?;";
                                $resultado_query_gasto = $this->db->query($query_gasto, array($value_detalle["L"]));
                                $nombre_gasto = $resultado_query_gasto->row();

                                if(!$nombre_gasto) {
                                    throw new Exception('El gasto '.$value_detalle["L"].' no existe dentro de la base de datos.');
                                }

                                $resultado_detalle_compromiso = $this->ciclo_model->insertar_detalle_compromisoArchivo(array(
                                    "id_nivel" => $value_detalle["id_nivel"],
                                    "G" => $value_detalle["L"],
                                    "H" => $nombre_gasto->unidad,
                                    "I" => 1,
                                    "K" => $value_detalle["subtotal"],
                                    "subtotal" => $value_detalle["subtotal"],
                                    "iva" => $value_detalle["iva"],
                                    "importe" => $value_detalle["importe"],
                                    "L" => $value_detalle["O"],
                                    "A" => $value_detalle["F"],
                                    "B" => $value_detalle["G"],
                                    "C" => $value_detalle["H"],
                                    "D" => $value_detalle["I"],
                                    "E" => $value_detalle["J"],
                                    "F" => $value_detalle["K"],
                                ), $query_insertar_detalle_compromiso, $last_compromiso);

                                if(!$resultado_detalle_compromiso) {
                                    throw new Exception('Hubo un error al insertar los datos de la partida.');
                                }

                                $this->ciclo_model->actualizarTotalCaratulaCompromiso($id_caratula_compromiso, $total_compromiso);

                            }

                        } else {
                            continue;
                        }

                    }

                    $last_contrarecibo = 0;

//                    Se toma el numero del ultimo precompromiso
                    $ultimo_contrarecibo = $this->ciclo_model->ultimo_contrarecibo();

                    if($ultimo_contrarecibo) {
                        $last_contrarecibo = $ultimo_contrarecibo->ultimo + 1;
                    }
                    else {
                        $last_contrarecibo = 1;
                    }

                    $this->ciclo_model->apartarContrarecibo($last_contrarecibo);

                    $this->ciclo_model->borrarDetalleContrarecibo($last_contrarecibo);

                    $resultado_contrarecibo = $this->ciclo_model->copiarDatosCompromiso($last_compromiso, $last_contrarecibo);

                    if($resultado_contrarecibo) {
                        $datos_compromiso = $this->ciclo_model->datos_Caratula_Compromiso_Contrarecibo($last_compromiso);
                    }

//                    $this->debugeo->imprimir_pre($datos_compromiso);

                    if(isset($datos_compromiso->concepto_especifico)){
                        $concepto_especifico_contrarecibo = $datos_compromiso->concepto_especifico;
                    } else {
                        $concepto_especifico_contrarecibo = "";
                    }

                    if(isset($datos_compromiso->documentacion_anexa)){
                        $documentacion_anexa_contrarecibo = $datos_compromiso->documentacion_anexa;
                    } else {
                        $documentacion_anexa_contrarecibo = "";
                    }

                    if(isset($datos_compromiso->id_persona)){
                        $id_persona_contrarecibo = $datos_compromiso->id_persona;
                    } else {
                        $id_persona_contrarecibo = 0;
                    }

                    if(isset($datos_compromiso->nombre_completo)){
                        $nombre_completo_contrarecibo = $datos_compromiso->nombre_completo;
                    } else {
                        $nombre_completo_contrarecibo = "";
                    }

                    $resultado_insertar = $this->ciclo_model->insertar_caratula_contrarecibo(array(
                        "num_compromiso" => $last_compromiso,
                        "contrarecibo" => $last_contrarecibo,
                        "clave_proveedor" => $datos_compromiso->id_proveedor,
                        "proveedor" => $datos_compromiso->nombre_proveedor,
                        "concepto" => $datos_compromiso->descripcion_general,
                        "concepto_especifico" => $concepto_especifico_contrarecibo,
                        "descripcion" => $datos_compromiso->descripcion_general,
                        "documento" => $documentacion_anexa_contrarecibo,
                        "fecha_pago" => $datos_compromiso->fecha_programada,
                        "fecha_solicita" => $datos_compromiso->fecha_emision,
                        "firme" => 1,
                        "numero_compromiso" => $last_compromiso,
                        "tipo" => $datos_compromiso->lugar_entrega,
                        "importe" => $datos_compromiso->total,
                        "tipo_documento" => "Factura",
                        "estatus" => "activo",
                        "documentacion_anexa" => $documentacion_anexa_contrarecibo,
                        "id_persona" => $id_persona_contrarecibo,
                        "nombre_completo" => $nombre_completo_contrarecibo,
                        "tipo_impresion" => $datos_compromiso->tipo_impresion,
                    ));

//                    Si el resultado es exitoso, se le indica al usuario
                    if(!$resultado_insertar) {
                        throw new Exception('Hubo un error al jalar los datos del compromiso al contrarecibo.');
                    }

                    $last_movimiento = 0;

                    $ultimo_movimiento = $this->ciclo_model->ultimo_movimientoBancario();

                    if($ultimo_movimiento) {
                        $last_movimiento = $ultimo_movimiento->ultimo + 1;
                    }
                    else {
                        $last_movimiento = 1;
                    }

                    $this->db->select('*')->from('cat_cuentas_bancarias')->like('cuenta', $value["Y"]);
                    $query_cuentas_bancarias = $this->db->get();
                    $datos_cuenta_bancaria = $query_cuentas_bancarias->row_array();

//                    $this->debugeo->imprimir_pre($datos_cuenta_bancaria);

                    $this->ciclo_model->apartarMovimientoBancario($last_movimiento, $datos_cuenta_bancaria["cuenta"], $datos_cuenta_bancaria["id_cuentas_bancarias"], "presupuesto");

                    $this->ciclo_model->copiarDatosContrarecibo($last_contrarecibo, $datos_cuenta_bancaria["cuenta"], $last_movimiento);

                    $datos_contrarecibo_movimiento = $this->ciclo_model->datos_Caratula_Contrarecibo_Movimiento($last_contrarecibo);

//                    $this->debugeo->imprimir_pre($datos_contrarecibo_movimiento);

                    $this->ciclo_model->actualizar_movimiento_bancario_presupuesto(array(
                        "hidden_id_cuenta" => $datos_cuenta_bancaria["id_cuentas_bancarias"],
                        "hidden_cuenta" => $datos_cuenta_bancaria["cuenta"],
                        "hidden_clave_concepto" => "",
                        "concepto" => "",
                        "fecha_emision" => $fecha,
                        "tiempo_movimiento" => date("H:i:s"),
                        "cheque" => "N/A",
                        "movimiento_bancario" => "N/A",
                        "importe" => $datos_contrarecibo_movimiento->importe,
                        "neto" => $datos_contrarecibo_movimiento->importe,
                        "importe" => $datos_contrarecibo_movimiento->importe,
                        "id_proveedor" => $datos_contrarecibo_movimiento->id_proveedor,
                        "proveedor" => $datos_contrarecibo_movimiento->proveedor,
                        "fecha_pago" => $fecha,
                        "descripcion_general" => $datos_contrarecibo_movimiento->concepto,
                        "tipo_movimiento" => $value["W"],
                        "check_firme" => 1,
                        "numero_contrarecibo" => $last_contrarecibo,
                        "movimiento" => $last_movimiento,
                    ));

                }


                $this->benchmark->mark('code_end');
                echo $this->benchmark->elapsed_time('code_start', 'code_end');


            } catch (Exception $e) {
                $this->db->delete('mov_compromiso_caratula', array('numero_compromiso' => $last_compromiso));
                $this->db->delete('mov_compromiso_detalle', array('numero_compromiso' => $last_compromiso));
                $this->db->delete('mov_contrarecibo_caratula', array('id_contrarecibo_caratula' => $last_contrarecibo));
                $this->db->delete('mov_contrarecibo_detalle', array('numero_contrarecibo' => $last_contrarecibo));
                $this->db->delete('mov_bancos_movimientos', array('movimiento' => $last_movimiento));
                $this->db->delete('mov_bancos_pagos_contrarecibos', array('movimiento' => $last_movimiento));
                echo($e->getMessage());
            }

        }

    }

    function revisar_partida(){
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha revisdao la partida' .$partida);
        $partida = 39911;
        $this->load->library('excel');
        $fecha = date("Y-m-d");
        $hora = date("H:i:s");

        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Adecuaciones');
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('Reporte Partida');
        $objDrawing->setPath('img/logo2.png');
        $objDrawing->setOffsetX(50);
        $objDrawing->setHeight(80);
        // $objDrawing->setWidth(10);
        $objDrawing->setCoordinates('A1');
        $objDrawing->setWorksheet($this->excel->getActiveSheet());

        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
            'font'  => array(
                'bold'  => true,
            )
        );
        $style_center = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );
        $style_left = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            )
        );

        $this->excel->getActiveSheet()->mergeCells('A1:F1')->getStyle("A1")->applyFromArray($style);
        $this->excel->getActiveSheet()->setCellValue('A1','EDUCAL, S.A. DE C.V.');
        $this->excel->getActiveSheet()->mergeCells('A2:F2')->getStyle("A2")->applyFromArray($style);
        $this->excel->getActiveSheet()->setCellValue('A2', 'Reporte de adecuaciones de la partida: '.$partida);
        $this->excel->getActiveSheet()->getStyle("C3:E3")->applyFromArray($style);
        $this->excel->getActiveSheet()->getStyle("C4:E4")->applyFromArray($style_center);
        $this->excel->getActiveSheet()->setCellValue('C3', 'Fecha Emisión');
        $this->excel->getActiveSheet()->setCellValue('E3', 'Hora Emisión');
        $this->excel->getActiveSheet()->setCellValue('C4', $fecha);
        $this->excel->getActiveSheet()->setCellValue('E4', $hora);
        $this->excel->getActiveSheet()->getStyle("A6:F6")->applyFromArray($style);
        $this->excel->getActiveSheet()->mergeCells('A5:F5');

        //set cell A1 content with some text

        $this->excel->getActiveSheet()->getStyle("A")->applyFromArray($style_left);
        $this->excel->getActiveSheet()->setCellValue('A6', 'Estructura')->getColumnDimension("A")->setWidth(27);
        $this->excel->getActiveSheet()->getStyle("B:F")->applyFromArray($style_center);
        $this->excel->getActiveSheet()->setCellValue('B6', 'No.')->getColumnDimension("B")->setWidth(10);
        $this->excel->getActiveSheet()->setCellValue('C6', 'Tipo')->getColumnDimension("C")->setWidth(20);
        $this->excel->getActiveSheet()->getStyle("D")->applyFromArray($style_left);
        $this->excel->getActiveSheet()->setCellValue('D6', 'Descripción')->getColumnDimension("D")->setWidth(35);
        $this->excel->getActiveSheet()->setCellValue('E6', 'Fecha Aplicación')->getColumnDimension("E")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('F6', 'Importe')->getColumnDimension("F")->setWidth(20);

        $resultado = $this->administrador_model->get_adecuaciones_partida($partida);
        $estructura = array();
        //$this->debugeo->imprimir_pre($resultado);
        $row = 7;

//        $this->benchmark->mark('code_start');

        foreach($resultado as $fila) {
            $estructura = json_decode($fila->estructura_final);

            $this->excel->getActiveSheet()->setCellValue('A'.$row, $estructura->centro_de_recaudacion.'  '.$estructura->rubro.'  '.$estructura->tipo.'  '.$estructura->clase);
            $this->excel->getActiveSheet()->setCellValue('B'.$row, $fila->numero_adecuacion);
            $this->excel->getActiveSheet()->setCellValue('C'.$row, $fila->texto);
            $this->excel->getActiveSheet()->setCellValue('D'.$row, $fila->titulo);
            $this->excel->getActiveSheet()->setCellValue('E'.$row, $fila->fecha_aplicacion);
            $this->excel->getActiveSheet()->setCellValue('F'.$row, '$ '.$this->cart->format_number($fila->total));
            $row += 1;
        }
    }

    function replicar_Matriz_Ingresos() {

        $sql = "SELECT COLUMN_GET(nivel, 'centro_de_recaudacion' as char) AS centro_de_recaudacion,
                COLUMN_GET(nivel, 'descripcion' as char) AS descripcion
                FROM cat_niveles
                WHERE COLUMN_GET(nivel, 'centro_de_recaudacion' as char) != ''
                GROUP BY COLUMN_GET(nivel, 'centro_de_recaudacion' as char)";

        $query = $this->db->query($sql);
        $result = $query->result_array();
        foreach($result as $fila){
            if($fila["centro_de_recaudacion"] == 101 || $fila["centro_de_recaudacion"] == 104 || $fila["centro_de_recaudacion"] == 1036 || $fila["centro_de_recaudacion"] == 1037 || $fila["centro_de_recaudacion"] == 1038 || $fila["centro_de_recaudacion"] == 1039 || $fila["centro_de_recaudacion"] == 10391 )
            {
                continue;
            }
//                $this->debugeo->imprimir_pre($fila);
            $cadena = substr($fila["centro_de_recaudacion"], 3, -1);

            $this->debugeo->imprimir_pre($cadena[0]);

            if($cadena[0] == 1)
            {
                $sql_insertar = 'INSERT INTO cat_correlacion_partidas_contables (clave, descripcion, cuenta_cargo, nombre_cargo, cuenta_abono, nombre_abono, tipo_gasto, destino) VALUES
                            ("'.$fila["centro_de_recaudacion"].'", "'.$fila["descripcion"].'", "4.1.7.4.1.4.01.'.$fila["centro_de_recaudacion"].'.03", "Ventas Generales", "1.1.2.2.1.4.01.'.$fila["centro_de_recaudacion"].'", "Clientes", 1, "Ingreso devengado"),
                            ("'.$fila["centro_de_recaudacion"].'", "'.$fila["descripcion"].'", "5.5.9.3.1.4.01.'.$fila["centro_de_recaudacion"].'.03", "Descuentos Generales", "4.1.7.4.1.4.01.'.$fila["centro_de_recaudacion"].'.04", "Servicios", 1, "Ingreso devengado"),
                            ("'.$fila["centro_de_recaudacion"].'", "'.$fila["descripcion"].'", "5.5.9.3.1.4.01.'.$fila["centro_de_recaudacion"].'.04", "Servicios", "2.1.1.7.1.2.02.0001", "I.V.A. Pendiente de Cobro (TASA 16%)", 1, "Ingreso devengado"),
                            ("'.$fila["centro_de_recaudacion"].'", "'.$fila["descripcion"].'", "8.3.2.1.1", "Presupuesto de Ingreso por Ejecutar", "8.3.4.1.1", "Presupuesto de Ingreso Devengado", 1, "Ingreso devengado"),
                            ("'.$fila["centro_de_recaudacion"].'", "'.$fila["descripcion"].'", "1.1.1.2.1.6.0001", "Bancos Cta Concentradora", "1.1.2.2.1.4.01.'.$fila["centro_de_recaudacion"].'", "Clientes", 1, "Ingreso cobrado"),
                            ("'.$fila["centro_de_recaudacion"].'", "'.$fila["descripcion"].'", "2.1.1.7.1.2.02.0001", "I.V.A. Pendiente de Cobro (TASA 16%)", "2.1.1.7.1.2.01.0001", "IVA Cobrado TASA 16%", 1, "Ingreso cobrado"),
                            ("'.$fila["centro_de_recaudacion"].'", "'.$fila["descripcion"].'", "8.3.4.1.1", "Presupuesto de Ingreso Devengado", "8.3.5.1.1", "Presupuesto de Ingreso Cobrado", 1, "Ingreso cobrado")';

            }
            elseif($cadena[0] == 2)
            {
                $sql_insertar = 'INSERT INTO cat_correlacion_partidas_contables (clave, descripcion, cuenta_cargo, nombre_cargo, cuenta_abono, nombre_abono, tipo_gasto, destino) VALUES
                            ("'.$fila["centro_de_recaudacion"].'", "'.$fila["descripcion"].'", "1.1.2.2.1.4.02.'.$fila["centro_de_recaudacion"].'", "Clientes", "4.1.7.4.1.4.02.'.$fila["centro_de_recaudacion"].'.03", "Ventas Generales", 1, "Ingreso devengado"),
                            ("'.$fila["centro_de_recaudacion"].'", "'.$fila["descripcion"].'", "5.5.9.3.1.4.02.'.$fila["centro_de_recaudacion"].'.03", "Descuentos Generales", "4.1.7.4.1.4.02.'.$fila["centro_de_recaudacion"].'.04", "Servicios", 1, "Ingreso devengado"),
                            ("'.$fila["centro_de_recaudacion"].'", "'.$fila["descripcion"].'", "5.5.9.3.1.4.02.'.$fila["centro_de_recaudacion"].'.04", "Servicios", "2.1.1.7.1.2.02.0001", "I.V.A. Pendiente de Cobro (TASA 16%)", 1, "Ingreso devengado"),
                            ("'.$fila["centro_de_recaudacion"].'", "'.$fila["descripcion"].'", "8.3.2.1.1", "Presupuesto de Ingreso por Ejecutar", "8.3.4.1.1", "Presupuesto de Ingreso Devengado", 1, "Ingreso devengado"),
                            ("'.$fila["centro_de_recaudacion"].'", "'.$fila["descripcion"].'", "1.1.1.2.1.6.0001", "Bancos Cta Concentradora", "1.1.2.2.1.4.02.'.$fila["centro_de_recaudacion"].'", "Clientes", 1, "Ingreso cobrado"),
                            ("'.$fila["centro_de_recaudacion"].'", "'.$fila["descripcion"].'", "2.1.1.7.1.2.02.0001", "I.V.A. Pendiente de Cobro (TASA 16%)", "2.1.1.7.1.2.01.0001", "IVA Cobrado TASA 16%", 1, "Ingreso cobrado"),
                            ("'.$fila["centro_de_recaudacion"].'", "'.$fila["descripcion"].'", "8.3.4.1.1", "Presupuesto de Ingreso Devengado", "8.3.5.1.1", "Presupuesto de Ingreso Cobrado", 1, "Ingreso cobrado")';
            }
            elseif($cadena[0] == 3)
            {
                $sql_insertar = 'INSERT INTO cat_correlacion_partidas_contables (clave, descripcion, cuenta_cargo, nombre_cargo, cuenta_abono, nombre_abono, tipo_gasto, destino) VALUES
                            ("'.$fila["centro_de_recaudacion"].'", "'.$fila["descripcion"].'", "1.1.2.2.1.4.03.'.$fila["centro_de_recaudacion"].'", "Clientes", "4.1.7.4.1.4.03.'.$fila["centro_de_recaudacion"].'.03", "Ventas Generales", 1, "Ingreso devengado"),
                            ("'.$fila["centro_de_recaudacion"].'", "'.$fila["descripcion"].'", "5.5.9.3.1.4.03.'.$fila["centro_de_recaudacion"].'.03", "Descuentos Generales", "4.1.7.4.1.4.03.'.$fila["centro_de_recaudacion"].'.04", "Servicios", 1, "Ingreso devengado"),
                            ("'.$fila["centro_de_recaudacion"].'", "'.$fila["descripcion"].'", "5.5.9.3.1.4.03.'.$fila["centro_de_recaudacion"].'.04", "Servicios", "2.1.1.7.1.2.02.0001", "I.V.A. Pendiente de Cobro (TASA 16%)", 1, "Ingreso devengado"),
                            ("'.$fila["centro_de_recaudacion"].'", "'.$fila["descripcion"].'", "8.3.2.1.1", "Presupuesto de Ingreso por Ejecutar", "8.3.4.1.1", "Presupuesto de Ingreso Devengado", 1, "Ingreso devengado"),
                            ("'.$fila["centro_de_recaudacion"].'", "'.$fila["descripcion"].'", "1.1.1.2.1.6.0001", "Bancos Cta Concentradora", "1.1.2.2.1.4.03.'.$fila["centro_de_recaudacion"].'", "Clientes", 1, "Ingreso cobrado"),
                            ("'.$fila["centro_de_recaudacion"].'", "'.$fila["descripcion"].'", "2.1.1.7.1.2.02.0001", "I.V.A. Pendiente de Cobro (TASA 16%)", "2.1.1.7.1.2.01.0001", "IVA Cobrado TASA 16%", 1, "Ingreso cobrado"),
                            ("'.$fila["centro_de_recaudacion"].'", "'.$fila["descripcion"].'", "8.3.4.1.1", "Presupuesto de Ingreso Devengado", "8.3.5.1.1", "Presupuesto de Ingreso Cobrado", 1, "Ingreso cobrado")';
            }

            $this->db->query($sql_insertar);
        }

    }

    function permisos_faltantes() {
        $this->db->select('id_usuario')->from('permisos')->group_by("id_usuario");
        $query = $this->db->get();

        $this->db->select('nombre_modulo')->from('modulos');
        $query_modulos = $this->db->get();

        foreach($query->result_array() as $key => $value) {
//            $this->debugeo->imprimir_pre($value);
            foreach($query_modulos->result_array() as $llave => $valor){
                $this->db->select('modulo')->from('permisos')->where('modulo', $valor["nombre_modulo"])->where('id_usuario', $value["id_usuario"]);
                $query_resultado_modulo = $this->db->get();
                if(!$query_resultado_modulo->row_array()){
                    $data = array(
                        'id_usuario' => $value["id_usuario"],
                        'modulo' => $valor["nombre_modulo"],
                        'activo' => 0,
                    );

                    $this->db->insert('permisos', $data);
                    echo("El usuario ".$value["id_usuario"]." No tiene el modulo: ".$valor["nombre_modulo"]."<br />");
                }
            }
        }
    }

    function checar_saldo_partidas() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha verificado los saldos de las partidas' .$result);

        $sql = "SELECT id_niveles, COLUMN_JSON(nivel) AS estructrua
               FROM cat_niveles
               WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) != ''
                AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) != ''
                AND COLUMN_GET(nivel, 'centro_de_costos' as char) != ''
                AND COLUMN_GET(nivel, 'capitulo' as char) != ''
                AND COLUMN_GET(nivel, 'concepto' as char) != ''
                AND COLUMN_GET(nivel, 'partida' as char) != '';";

        $query = $this->db->query($sql);
        $result = $query->result_array();
        foreach($result as $fila){
            $estructura = json_decode($fila["estructrua"], TRUE);
            foreach($estructura as $llave => $valor){
                if($valor < 0) {
                    $this->debugeo->imprimir_pre("Estructura: ".$estructura["fuente_de_financiamiento"]." ".$estructura["programa_de_financiamiento"]." ".$estructura["centro_de_costos"]." ".$estructura["capitulo"]." ".$estructura["concepto"]." ".$estructura["partida"]);
//                    $this->debugeo->imprimir_pre("ID: ".$fila["id_niveles"]);
                }
            }
//            $this->debugeo->imprimir_pre($estructura);
//            $this->debugeo->imprimir_pre("ID: ".$fila["id_niveles"]);
        }
    }

    function exportar_saldos_iniciales_ingresos() {
        $this->load->library('excel');
        $fecha = date("Y-m-d");
        $hora = date("H:i:s");

        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Saldos Iniciales');
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('Educal');
        $objDrawing->setPath('img/logo2.png');
        $objDrawing->setOffsetX(50);
        $objDrawing->setHeight(80);
        // $objDrawing->setWidth(10);
        $objDrawing->setCoordinates('A1');
        $objDrawing->setWorksheet($this->excel->getActiveSheet());

        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
            'font'  => array(
                'bold'  => true,
            )
        );
        $style_center = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );
        $style_left = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            )
        );

        $this->excel->getActiveSheet()->mergeCells('A1:F1')->getStyle("A1")->applyFromArray($style);
        $this->excel->getActiveSheet()->setCellValue('A1','EDUCAL, S.A. DE C.V.');
        $this->excel->getActiveSheet()->mergeCells('A2:F2')->getStyle("A2")->applyFromArray($style);
        $this->excel->getActiveSheet()->setCellValue('A2', 'Consulta de Saldos Iniciales de Ingresos');
        $this->excel->getActiveSheet()->getStyle("C3:E3")->applyFromArray($style);
        $this->excel->getActiveSheet()->getStyle("C4:E4")->applyFromArray($style_center);
        $this->excel->getActiveSheet()->setCellValue('C3', 'Fecha Emisión');
        $this->excel->getActiveSheet()->setCellValue('E3', 'Hora Emisión');
        $this->excel->getActiveSheet()->setCellValue('C4', $fecha);
        $this->excel->getActiveSheet()->setCellValue('E4', $hora);
        $this->excel->getActiveSheet()->getStyle("A6:F6")->applyFromArray($style);
        $this->excel->getActiveSheet()->mergeCells('A5:F5');

        //set cell A1 content with some text

        $this->excel->getActiveSheet()->getStyle("A")->applyFromArray($style_left);
        $this->excel->getActiveSheet()->setCellValue('A6', 'Estructura')->getColumnDimension("A")->setWidth(27);
        $this->excel->getActiveSheet()->getStyle("B:AA")->applyFromArray($style_center);
        $this->excel->getActiveSheet()->setCellValue('B6', 'Enero Inicial')->getColumnDimension("B");
        $this->excel->getActiveSheet()->setCellValue('C6', 'Enero Devengado')->getColumnDimension("C");
        $this->excel->getActiveSheet()->setCellValue('D6', 'Febrero Inicial')->getColumnDimension("D");
        $this->excel->getActiveSheet()->setCellValue('E6', 'Febrero Devengado')->getColumnDimension("E");
        $this->excel->getActiveSheet()->setCellValue('F6', 'Marzo Inicial')->getColumnDimension("F");
        $this->excel->getActiveSheet()->setCellValue('G6', 'Marzo Devengado')->getColumnDimension("G");
        $this->excel->getActiveSheet()->setCellValue('H6', 'Abril Inicial')->getColumnDimension("H");
        $this->excel->getActiveSheet()->setCellValue('I6', 'Abril Devengado')->getColumnDimension("I");
        $this->excel->getActiveSheet()->setCellValue('J6', 'Mayo Inicial')->getColumnDimension("J");
        $this->excel->getActiveSheet()->setCellValue('K6', 'Mayo Devengado')->getColumnDimension("K");
        $this->excel->getActiveSheet()->setCellValue('L6', 'Junio Inicial')->getColumnDimension("L");
        $this->excel->getActiveSheet()->setCellValue('M6', 'Junio Devengado')->getColumnDimension("M");
        $this->excel->getActiveSheet()->setCellValue('N6', 'Julio Inicial')->getColumnDimension("N");
        $this->excel->getActiveSheet()->setCellValue('O6', 'Julio Devengado')->getColumnDimension("O");
        $this->excel->getActiveSheet()->setCellValue('P6', 'Agosto Inicial')->getColumnDimension("P");
        $this->excel->getActiveSheet()->setCellValue('Q6', 'Agosto Devengado')->getColumnDimension("Q");
        $this->excel->getActiveSheet()->setCellValue('R6', 'Septiembre Inicial')->getColumnDimension("R");
        $this->excel->getActiveSheet()->setCellValue('S6', 'Septiembre Devengado')->getColumnDimension("S");
        $this->excel->getActiveSheet()->setCellValue('T6', 'Octubre Inicial')->getColumnDimension("T");
        $this->excel->getActiveSheet()->setCellValue('U6', 'Octubre Devengado')->getColumnDimension("U");
        $this->excel->getActiveSheet()->setCellValue('V6', 'Noviembre Inicial')->getColumnDimension("V");
        $this->excel->getActiveSheet()->setCellValue('W6', 'Noviembre Devengado')->getColumnDimension("W");
        $this->excel->getActiveSheet()->setCellValue('X6', 'Diciembre Inicial')->getColumnDimension("X");
        $this->excel->getActiveSheet()->setCellValue('Y6', 'Diciembre Devengado')->getColumnDimension("Y");
        $this->excel->getActiveSheet()->setCellValue('Z6', 'Total Anual')->getColumnDimension("Z");
        $this->excel->getActiveSheet()->setCellValue('AA6', 'Total Devengado')->getColumnDimension("AA");

        $sql = "SELECT id_niveles, COLUMN_JSON(nivel) AS estructura
               FROM cat_niveles
               WHERE COLUMN_GET(nivel, 'gerencia' as char) != ''
                AND COLUMN_GET(nivel, 'centro_de_recaudacion' as char) != ''
                AND COLUMN_GET(nivel, 'rubro' as char) != ''
                AND COLUMN_GET(nivel, 'tipo' as char) != ''
                AND COLUMN_GET(nivel, 'clase' as char) != ''";

        $query = $this->db->query($sql);
        $resultado = $query->result();

//        $this->debugeo->imprimir_pre($resultado);
        $row = 7;

        foreach($resultado as $fila) {
            $estructura = json_decode($fila->estructura);
            $total = $estructura->enero_inicial + $estructura->febrero_inicial + $estructura->marzo_inicial + $estructura->abril_inicial + $estructura->mayo_inicial + $estructura->junio_inicial + $estructura->julio_inicial + $estructura->agosto_inicial + $estructura->septiembre_inicial + $estructura->octubre_inicial + $estructura->noviembre_inicial + $estructura->diciembre_inicial;
            
            $total_devengado = $estructura->enero_devengado + $estructura->febrero_devengado + $estructura->marzo_devengado + $estructura->abril_devengado + $estructura->mayo_devengado + $estructura->junio_devengado + $estructura->julio_devengado + $estructura->agosto_devengado + $estructura->septiembre_devengado + $estructura->octubre_devengado + $estructura->noviembre_devengado + $estructura->diciembre_devengado;

            $this->excel->getActiveSheet()->setCellValue('A'.$row, $estructura->gerencia.'  '.$estructura->centro_de_recaudacion.'  '.$estructura->rubro.'  '.$estructura->tipo.'  '.$estructura->clase);
            $this->excel->getActiveSheet()->setCellValue('B'.$row, '$ '.$this->cart->format_number($estructura->enero_inicial));
            $this->excel->getActiveSheet()->setCellValue('C'.$row, '$ '.$this->cart->format_number($estructura->enero_devengado));
            $this->excel->getActiveSheet()->setCellValue('D'.$row, '$ '.$this->cart->format_number($estructura->febrero_inicial));
            $this->excel->getActiveSheet()->setCellValue('E'.$row, '$ '.$this->cart->format_number($estructura->febrero_devengado));
            $this->excel->getActiveSheet()->setCellValue('F'.$row, '$ '.$this->cart->format_number($estructura->marzo_inicial));
            $this->excel->getActiveSheet()->setCellValue('G'.$row, '$ '.$this->cart->format_number($estructura->marzo_devengado));
            $this->excel->getActiveSheet()->setCellValue('H'.$row, '$ '.$this->cart->format_number($estructura->abril_inicial));
            $this->excel->getActiveSheet()->setCellValue('I'.$row, '$ '.$this->cart->format_number($estructura->abril_devengado));
            $this->excel->getActiveSheet()->setCellValue('J'.$row, '$ '.$this->cart->format_number($estructura->mayo_inicial));
            $this->excel->getActiveSheet()->setCellValue('K'.$row, '$ '.$this->cart->format_number($estructura->mayo_devengado));
            $this->excel->getActiveSheet()->setCellValue('L'.$row, '$ '.$this->cart->format_number($estructura->junio_inicial));
            $this->excel->getActiveSheet()->setCellValue('M'.$row, '$ '.$this->cart->format_number($estructura->junio_devengado));
            $this->excel->getActiveSheet()->setCellValue('N'.$row, '$ '.$this->cart->format_number($estructura->julio_inicial));
            $this->excel->getActiveSheet()->setCellValue('O'.$row, '$ '.$this->cart->format_number($estructura->julio_devengado));
            $this->excel->getActiveSheet()->setCellValue('P'.$row, '$ '.$this->cart->format_number($estructura->agosto_inicial));
            $this->excel->getActiveSheet()->setCellValue('Q'.$row, '$ '.$this->cart->format_number($estructura->agosto_devengado));
            $this->excel->getActiveSheet()->setCellValue('R'.$row, '$ '.$this->cart->format_number($estructura->septiembre_inicial));
            $this->excel->getActiveSheet()->setCellValue('S'.$row, '$ '.$this->cart->format_number($estructura->septiembre_devengado));
            $this->excel->getActiveSheet()->setCellValue('T'.$row, '$ '.$this->cart->format_number($estructura->octubre_inicial));
            $this->excel->getActiveSheet()->setCellValue('U'.$row, '$ '.$this->cart->format_number($estructura->octubre_devengado));
            $this->excel->getActiveSheet()->setCellValue('V'.$row, '$ '.$this->cart->format_number($estructura->noviembre_inicial));
            $this->excel->getActiveSheet()->setCellValue('W'.$row, '$ '.$this->cart->format_number($estructura->noviembre_devengado));
            $this->excel->getActiveSheet()->setCellValue('X'.$row, '$ '.$this->cart->format_number($estructura->diciembre_inicial));
            $this->excel->getActiveSheet()->setCellValue('Y'.$row, '$ '.$this->cart->format_number($estructura->diciembre_devengado));
            $this->excel->getActiveSheet()->setCellValue('Z'.$row, '$ '.$this->cart->format_number($total));
            $this->excel->getActiveSheet()->setCellValue('AA'.$row, '$ '.$this->cart->format_number($total_devengado));
            $row += 1;
        }

        $filename="Saldos Iniciales Ingresos.xls"; // Agregar fecha en que se generó
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');

    }

    function resetear_modificado_ingresos() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha reseteo la modificacion de ingresos');
        $sql = "SELECT id_niveles, COLUMN_JSON(nivel) AS estructura
               FROM cat_niveles
               WHERE COLUMN_GET(nivel, 'gerencia' as char) != ''
                AND COLUMN_GET(nivel, 'centro_de_recaudacion' as char) != ''
                AND COLUMN_GET(nivel, 'rubro' as char) != ''
                AND COLUMN_GET(nivel, 'tipo' as char) != ''
                AND COLUMN_GET(nivel, 'clase' as char) != ''";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        foreach($result as $fila){
            $estructura = json_decode($fila["estructura"], TRUE);
            $this->debugeo->imprimir_pre($estructura);
            $this->debugeo->imprimir_pre("ID: ".$fila["id_niveles"]);

            $sql_actualizar = "UPDATE cat_niveles SET nivel = COLUMN_ADD(nivel, 'enero_saldo', 0),
                            nivel = COLUMN_ADD(nivel, 'febrero_saldo', 0),
                            nivel = COLUMN_ADD(nivel, 'marzo_saldo', 0),
                            nivel = COLUMN_ADD(nivel, 'abril_saldo', 0),
                            nivel = COLUMN_ADD(nivel, 'mayo_saldo', 0),
                            nivel = COLUMN_ADD(nivel, 'junio_saldo', 0),
                            nivel = COLUMN_ADD(nivel, 'julio_saldo', 0),
                            nivel = COLUMN_ADD(nivel, 'agosto_saldo', 0),
                            nivel = COLUMN_ADD(nivel, 'septiembre_saldo', 0),
                            nivel = COLUMN_ADD(nivel, 'octubre_saldo', 0),
                            nivel = COLUMN_ADD(nivel, 'noviembre_saldo', 0),
                            nivel = COLUMN_ADD(nivel, 'diciembre_saldo', 0),
                            nivel = COLUMN_ADD(nivel, 'total_anual', 0)
                            WHERE id_niveles = ?;";
            $query_actualizar = $this->db->query($sql_actualizar, array(
                $fila["id_niveles"],
            ));
            $this->debugeo->imprimir_pre($query_actualizar);
        }
    }

    function resetear_modificado_devengado() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha reseteo la modificacion de devengado' .$estructura);
        $sql = "SELECT id_niveles, COLUMN_JSON(nivel) AS estructura
               FROM cat_niveles
               WHERE COLUMN_GET(nivel, 'gerencia' as char) != ''
                AND COLUMN_GET(nivel, 'centro_de_recaudacion' as char) != ''
                AND COLUMN_GET(nivel, 'rubro' as char) != ''
                AND COLUMN_GET(nivel, 'tipo' as char) != ''
                AND COLUMN_GET(nivel, 'clase' as char) != ''";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        foreach($result as $fila){
            $estructura = json_decode($fila["estructura"], TRUE);
            $this->debugeo->imprimir_pre($estructura);
            $this->debugeo->imprimir_pre("ID: ".$fila["id_niveles"]);

            $sql_actualizar = "UPDATE cat_niveles SET nivel = COLUMN_ADD(nivel, 'enero_devengado', 0),
                            nivel = COLUMN_ADD(nivel, 'febrero_devengado', 0),
                            nivel = COLUMN_ADD(nivel, 'marzo_devengado', 0),
                            nivel = COLUMN_ADD(nivel, 'abril_devengado', 0),
                            nivel = COLUMN_ADD(nivel, 'mayo_devengado', 0),
                            nivel = COLUMN_ADD(nivel, 'junio_devengado', 0),
                            nivel = COLUMN_ADD(nivel, 'julio_devengado', 0),
                            nivel = COLUMN_ADD(nivel, 'agosto_devengado', 0),
                            nivel = COLUMN_ADD(nivel, 'septiembre_devengado', 0),
                            nivel = COLUMN_ADD(nivel, 'octubre_devengado', 0),
                            nivel = COLUMN_ADD(nivel, 'noviembre_devengado', 0),
                            nivel = COLUMN_ADD(nivel, 'diciembre_devengado', 0),
                            nivel = COLUMN_ADD(nivel, 'devengado_anual', 0)
                            WHERE id_niveles = ?;";

            $query_actualizar = $this->db->query($sql_actualizar, array(
                $fila["id_niveles"],
            ));
            $this->debugeo->imprimir_pre($query_actualizar);
        }
    }

    function resetear_modificado_recaudado() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha reseteo la modificacion de recaudado');
        $sql = "SELECT id_niveles, COLUMN_JSON(nivel) AS estructura
               FROM cat_niveles
               WHERE COLUMN_GET(nivel, 'gerencia' as char) != ''
                AND COLUMN_GET(nivel, 'centro_de_recaudacion' as char) != ''
                AND COLUMN_GET(nivel, 'rubro' as char) != ''
                AND COLUMN_GET(nivel, 'tipo' as char) != ''
                AND COLUMN_GET(nivel, 'clase' as char) != ''";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        foreach($result as $fila){
            $estructura = json_decode($fila["estructura"], TRUE);
            $this->debugeo->imprimir_pre($estructura);
            $this->debugeo->imprimir_pre("ID: ".$fila["id_niveles"]);

            $sql_actualizar = "UPDATE cat_niveles SET nivel = COLUMN_ADD(nivel, 'enero_recaudado', 0),
                            nivel = COLUMN_ADD(nivel, 'febrero_recaudado', 0),
                            nivel = COLUMN_ADD(nivel, 'marzo_recaudado', 0),
                            nivel = COLUMN_ADD(nivel, 'abril_recaudado', 0),
                            nivel = COLUMN_ADD(nivel, 'mayo_recaudado', 0),
                            nivel = COLUMN_ADD(nivel, 'junio_recaudado', 0),
                            nivel = COLUMN_ADD(nivel, 'julio_recaudado', 0),
                            nivel = COLUMN_ADD(nivel, 'agosto_recaudado', 0),
                            nivel = COLUMN_ADD(nivel, 'septiembre_recaudado', 0),
                            nivel = COLUMN_ADD(nivel, 'octubre_recaudado', 0),
                            nivel = COLUMN_ADD(nivel, 'noviembre_recaudado', 0),
                            nivel = COLUMN_ADD(nivel, 'diciembre_recaudado', 0),
                            nivel = COLUMN_ADD(nivel, 'recaudado_anual', 0)
                            WHERE id_niveles = ?;";

            $query_actualizar = $this->db->query($sql_actualizar, array(
                $fila["id_niveles"],
            ));
            $this->debugeo->imprimir_pre($query_actualizar);
        }
    }

    function ejecutar_devengado() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha ejecutado la función del devengado' .$estructura);
        $sql = "SELECT * FROM mov_devengado_detalle;";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        foreach($result as $fila) {
            $this->debugeo->imprimir_pre($fila);
            $this->db->select('COLUMN_JSON(nivel) AS estructura')->from('cat_niveles')->where('id_niveles', $fila["id_nivel"]);
            $query_detalle = $this->db->get();
            $fila_detalle = $query_detalle->row_array();
            $estructura = json_decode($fila_detalle["estructura"], TRUE);

            $mes_actual = $this->utilerias->convertirFechaAMes($fila["fecha_aplicacion"]);

            $mes_devengado = $estructura[$mes_actual."_devengado"] + $fila["total_importe"];

            $anual_devengado = $estructura["devengado_anual"] + $fila["total_importe"];

//            $this->debugeo->imprimir_pre($estructura);

            $sql_actualizar_devengado = "UPDATE cat_niveles SET nivel = COLUMN_ADD(nivel, '".$mes_actual."_devengado', ?),
                                                nivel = COLUMN_ADD(nivel, 'devengado_anual', ?) WHERE id_niveles = ?;";
            $resultado_actualizar = $this->db->query($sql_actualizar_devengado, array($mes_devengado, $anual_devengado, $fila["id_nivel"]));
        }
    }

    function acomodar_compromisos_usados() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha acomodado los compromisos usados');
        $this->db->select('numero_compromiso')->from('mov_contrarecibo_caratula')->where('enfirme', 1);
        $query = $this->db->get();

        foreach($query->result_array() as $key => $value) {
            $datos_actualizar = array(
                'usado' => 1,
            );
            $this->db->where('numero_compromiso', $value["numero_compromiso"]);
            $this->db->update('mov_compromiso_caratula', $datos_actualizar);
//            $this->debugeo->imprimir_pre($value);
        }

    }


    function subir_archivo_polizas() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha subido un archivo');

//        Se prepara la configuracion de la libreria "Upload"
        $config['upload_path'] = './application/archivos/';
        $config['allowed_types'] = '*';

//        Se carga la libreria con sus configuraciones correspondientes
        $this->load->library('upload', $config);

//        Si el server no logra subir el archivo, despliega un mensaje de error
        if ( ! $this->upload->do_upload('archivoSubir')) {
//            Se capturan los errores en una variable y se imprimen para debug
            $error = array('error' => $this->upload->display_errors());
            $this->debugeo->imprimir_pre($error);
        }
        else {
            try {
                ini_set('memory_limit', '-1');
//            load our new PHPExcel library
                $this->load->library('excel');
//            Si el server logra subir el archivo, se toman los datos del archivo
                $data = $this->upload->data();

//            Se toma la ruta del archivo junto con su nombre y extension
                $file = $data["full_path"];

//            read file from path
                $objPHPExcel = PHPExcel_IOFactory::load($file);

//            get only the Cell Collection
                $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();

                $this->benchmark->mark('code_start');

//            extract to a PHP readable array format
                foreach ($cell_collection as $cell) {
                    $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
                    $fila = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
                    $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();

//                header will/should be in row 1 only. of course this can be modified to suit your need.
                    if ($fila == 1) {
                        continue;
                    } else {
                        $datos_detalle[$fila][$column] = trim($data_value);
                    }
                }

                $this->db->trans_begin();

                foreach($datos_detalle as $key => $value) {

                    if(!isset($value["A"]) || $value["A"] == NULL || $value["A"] == "") {
                        $value["A"] = "";
                    }

                    if(!isset($value["B"]) || $value["B"] == NULL || $value["B"] == "") {
                        $value["B"] = "";
                    }

                    if(!isset($value["C"]) || $value["C"] == NULL || $value["C"] == "") {
                        $value["C"] = "";
                    }

                    if(!isset($value["D"]) || $value["D"] == NULL || $value["D"] == "") {
                        $value["D"] = "";
                    }

                    if(!isset($value["E"]) || $value["E"] == NULL || $value["E"] == "") {
                        $value["E"] = "";
                    }

                    if(!isset($value["F"]) || $value["F"] == NULL || $value["F"] == "") {
                        $value["F"] = "";
                    }

                    if(!isset($value["G"]) || $value["G"] == NULL || $value["G"] == "") {
                        $value["G"] = "";
                    }

                    $cadena_por_encontrar_siete = "7.6.1.1.1";

                    if (strpos($value["A"], $cadena_por_encontrar_siete) !== FALSE) {
                        $cuenta = explode(".", $value["A"]);
                        $centro_recaudacion = str_split($cuenta[5]);

                        if($centro_recaudacion[3] == 1) {
                            $cuenta[4] = 1;
                        } elseif($centro_recaudacion[3] == 2) {
                            $cuenta[4] = 2;
                        } elseif($centro_recaudacion[3] == 3) {
                            $cuenta[4] = 3;
                        }

                        $value["A"] = $cuenta[0].".".$cuenta[1].".".$cuenta[2].".".$cuenta[3].".".$cuenta[4].".".$cuenta[5]."";

//                        $this->debugeo->imprimir_pre($value["A"]);
//                        $this->debugeo->imprimir_pre($centro_recaudacion);
                    }

                    $cadena_por_encontrar_activo = "1.1.4.1.6.1";

                    if (strpos($value["A"], $cadena_por_encontrar_activo) !== FALSE) {
                        $cuenta_activo = explode(".", $value["A"]);
                        $centro_recaudacion_activo = str_split($cuenta_activo[5]);

//                        $this->debugeo->imprimir_pre($centro_recaudacion_activo);

                        if($centro_recaudacion_activo[3] == 1) {
                            $cuenta_activo[5] = 1;
                        } elseif($centro_recaudacion_activo[3] == 2) {
                            $cuenta_activo[5] = 2;
                        } elseif($centro_recaudacion_activo[3] == 3) {
                            $cuenta_activo[5] = 3;
                        }

                        if($cuenta_activo[7] == 0) {
                            $cuenta_activo[7] = 1;
                        }

                        $value["A"] = $cuenta_activo[0].".".$cuenta_activo[1].".".$cuenta_activo[2].".".$cuenta_activo[3].".".$cuenta_activo[4].".".$cuenta_activo[5].".".$cuenta_activo[6].".".$cuenta_activo[7].".".$cuenta_activo[8]."";

                        $this->debugeo->imprimir_pre($value["A"]);
//                        $this->debugeo->imprimir_pre($centro_recaudacion);
                    }

                    $cadena_por_encontrar_cinco = "5.6.2.1.1.4.01";

                    if (strpos($value["A"], $cadena_por_encontrar_cinco) !== FALSE) {
                        $cuenta_cinco = explode(".", $value["A"]);
                        $centro_recaudacion_cinco = str_split($cuenta_cinco[5]);

                        if($centro_recaudacion_cinco[3] == 1) {
                            $cuenta_cinco[6] = "01";
                        } elseif($centro_recaudacion_cinco[3] == 2) {
                            $cuenta_cinco[6] = "03";
                        } elseif($centro_recaudacion_cinco[3] == 3) {
                            $cuenta_cinco[6] = "03";
                        }

                        $value["A"] = $cuenta_cinco[0].".".$cuenta_cinco[1].".".$cuenta_cinco[2].".".$cuenta_cinco[3].".".$cuenta_cinco[4].".".$cuenta_cinco[5].".".$cuenta_cinco[6].".".$cuenta_cinco[7].".".$cuenta_cinco[8]."";

//                        $this->debugeo->imprimir_pre($value["A"]);
//                        $this->debugeo->imprimir_pre($centro_recaudacion);
                    }

                    $datos_insertar = array(
                        'cuenta' => $value["A"],
                        'centro_costos' => $value["B"],
                        'partida' => $value["C"],
                        'subsidio' => $value["D"],
                        'descripcion_cuenta' => $value["E"],
                        'debe' => $value["F"],
                        'haber' => $value["G"],
                    );

                    $this->db->insert('tabla_temporal_cuentas_ana', $datos_insertar);

//                    $this->debugeo->imprimir_pre($value);
                }

                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    throw new Exception('Ha ocurrido un error al insertar el archivo');
                }
                else {
                    $this->db->trans_commit();
                }


                $this->benchmark->mark('code_end');
                echo $this->benchmark->elapsed_time('code_start', 'code_end');


            } catch (Exception $e) {

                echo($e->getMessage());
            }

        }

    }
}