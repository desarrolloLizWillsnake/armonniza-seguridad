<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Patrimonio extends CI_Controller {

    /**
     * Se revisa si el usuario esta logueado, si no esta logueado, se reenvia a la pantalla de login
     */
    function __construct()
    {
        parent::__construct();
        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        }
    }

    /**
     * Esta funcion es la principal, donde se muestra la pantalla de patrimonio
     */
    function index()
    {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha ingresado al modulo patrimonio');
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Patrimonio",
            "usuario" => $this->tank_auth->get_username(),
        );
        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/main_view');
        $this->load->view('front/footer_main_view', array("graficas" => TRUE));
    }

    /** Aqui empieza la sección de Patrimonio */

    /** Sección Nota de Entrada */
    function nota_entrada() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha generado una nota de entrada');
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Nota de Entrada",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/patrimonio_nota_entrada_view');
        $this->load->view('front/footer_main_view', array("tablas" => TRUE, 'nota_entrada' => TRUE));
    }

    function agregar_nota_entrada() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha agregado una nota de entrada');
        $last = 0;
//        Se toma el numero del ultimo precompromiso
        $ultimo = $this->patrimonio_model->ultima_nota_entrada();

        if($ultimo) {
            $last = $ultimo->ultimo + 1;
        }
        else {
            $last = 1;
        }

        $this->patrimonio_model->apartar_nota_entrada($last);

        $datos_header = array(
            "titulo_pagina" => "Armonniza | Nota de Entrada",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
            "precompromisocss" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/agregar_nota_entrada_view', array(
            "ultima_nota" => $last,
            "niveles" => $this->input_niveles_egresos(),
            "niveles_modal" => $this->preparar_estructura_egresos(),
        ));
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "agregar_nota_entrada" => TRUE,
        ));
    }

    function editar_nota_entrada($notaEntrada = NULL) {
//        Se guarda en el log el movimiento del usuario
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha editado la nota de entrada: '.$notaEntrada);

//        Se toma el detalle del movimiento de  la nota de entrada
        $this->db->select('*')->from('mov_nota_entrada_caratula')->where('numero_movimiento', $notaEntrada);
        $query = $this->db->get();
        $resultado = $query->row_array();

//        Se toma el numero de compromiso, y se guarda en otra variable para no causar conflicto con la variable que se encarga de cargar el encabezado y el pie de pagina
        $resultado["compromiso_nota_entrada"] = $resultado["compromiso"];
        unset($resultado["compromiso"]);

//        Se preparan las estructuras de egresos
        $resultado["niveles"] = $this->input_niveles_egresos();
        $resultado["niveles_modal"] = $this->preparar_estructura_egresos();

//        $this->debugeo->imprimir_pre($resultado);

        $datos_header = array(
            "titulo_pagina" => "Armonniza | Nota de Entrada",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/editar_nota_entrada_view', $resultado);
        $this->load->view('front/footer_main_view', array("tablas" => TRUE, "editar_nota_entrada" => TRUE));
    }

    /**
     * Esta función sirve para poder editar el detalle por línea de las notas de entrada
     * @return string Envía un mensaje ya sea de éxito o de error al usuario final
     */
    function editar_detalle_nota_entrada() {

        // Se inicializa la variable que contiene la respuesta del resultado de la consulta
        $mensaje = array(
            "mensaje" => "",
        );

//        Se toman los datos del compromiso a insertar
        $datos = $this->input->post();

        // Se guarda el registo del movimiento en el log
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha editado un id detalle Nota de Entrada '.$datos["borrar_nota_hidden"]);

        // Se recorren todos los campos y se limpian de espacios en blanco
        foreach ($datos as $key => $value) {
            $datos[$key]=trim($value);
        }

        try {

            // Se verifica que el centro de costos proporcionado por el usuario, en realidad exista
            $existe_cc = $this->patrimonio_model->verificar_centro_costos($datos["editar_cc_nota"]);

            // En caso de que el centro de costos no exista, se manda un mensaje de error al usuario
            if (!isset($existe_cc["centro_costos"])) {
                throw new Exception('El centro de costos proporcionado, no existe dentro de la base de datos.');
            }

            $datos["centro_costos"] = $existe_cc["centro_costos"];

            // Se verifica que el articulo proporcionado por el usuario, en realidad exista
            $existe_articulo = $this->patrimonio_model->verificar_articulo($datos["editar_articulo_nota"]);

            // En caso de que el articulo no exista, se manda un mensaje de error al usuario
            if (!isset($existe_articulo["articulo"])) {
                throw new Exception('El articulo proporcionado, no existe dentro de la base de datos.');
            }

            $datos["articulo"] = $existe_articulo["articulo"];
            $datos["titulo"] = $existe_articulo["descripcion"];

            // Se actualizan los nuevos datos a la línea
            $resultado_actualizar = $this->patrimonio_model->editarDetalleNotaEntrada($datos);

            // En caso de que la actualizacion de los datos sea un exito, se le notifica al usuario de que todo salio bien
            if($resultado_actualizar) {
                $mensaje = array(
                    "mensaje" => '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-check-circle fa-2x ic-msj"></i> Los datos han sido actualizados con &eacute;xito.</div>',
                );
            }
            // De lo contrario, se manda un error al usuario
            else {
                throw new Exception('Hubo un error al tratar de actualizar los datos, por favor contacte a su administrador.');
            }

        } catch (Exception $e) {

            $mensaje = array(
                "mensaje" => '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$e->getMessage().'</div>',
            );
        }

        // Se imprime el mensaje hacia el usuario
        echo(json_encode($mensaje));

    }

    function copiar_datos_contrarecibo() {
        $datos = $this->input->post();
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha copiado datos del Contra Recibo: '.$datos['contrarecibo'].' a la nota de entrada: '.$datos['ultima_nota']);

        foreach ($datos as $key => $value) {
            $datos[$key]=trim($value);
        }

        //$this->debugeo->imprimir_pre($datos);

        $this->db->where('numero_movimiento', $datos['ultima_nota']);
        $this->db->delete('mov_nota_entrada_detalle');

        $resultado = $this->patrimonio_model->copiarDatosContrarecibo($datos);

        echo(json_encode($resultado));
    }

    function insertar_detalle_nota_entrada() {

        $datos = $this->input->post();

        foreach ($datos as $key => $value) {
            $datos[$key]=trim($value);
        }

//        $this->debugeo->imprimir_pre($datos);

        try {
//        Se llama la funcion del modelo de egresos encargado de contar los niveles que existen
            $total_egresos = $this->egresos_model->contar_egresos_elementos();
//        Se llama a la funcion del model de egresos encargada de tomar los nombres delos niveles que existen
            $nombres_egresos = $this->egresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
            $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
            foreach($nombres_egresos as $fila) {
                array_push($nombre, $fila->descripcion);
            }

//        Se genera el query para revisar que la estructura exista
            $query = "SELECT id_niveles FROM cat_niveles WHERE COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[0]))."' as char) = ? ";

            for($i = 1; $i < $total_egresos->conteo; $i++){
                $query .= "AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[$i]))."' as char) = ? ";
            }

//            Primero hay que revisar si existe la estructura
            $existe = $this->ciclo_model->existeEstructura($datos, $query);

            if($existe->id_niveles) {
//            Se calcula el subtotal del gasto multiplicando la cantidad por el precio insertado
                $datos["subtotal"] = $datos["cantidad"] * $datos["precio"];
                $datos["subtotal"] = round($datos["subtotal"], 2);

                $datos['id_nivel'] = $existe->id_niveles;

                $datos["iva"] = 0;
                $datos["importe"] = round($datos["subtotal"], 2);

                $query_insertar = "INSERT INTO mov_nota_entrada_detalle (
                                                                        numero_movimiento, id_nivel, articulo, unidad_medida, cantidad,
                                                                        p_unitario, subtotal, iva, importe, titulo,
                                                                        descripcion, tipo_poliza, observaciones, estructura
                                                                        )
                                                                        VALUES (
                                                                        ?, ?, ?, ?, ?,
                                                                        ?, ?, ?, ?, ?,
                                                                        ?, ?, ?, COLUMN_CREATE(";

                for($i = 0; $i < $total_egresos->conteo; $i++){
                    $query_insertar .= "'".strtolower(str_replace(' ', '_', $nombre[$i]))."', ?, ";
                }

                $query_insertar .= "'articulo', ?));";

                log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha insertado un detalle en la nota de entrada: '.$this->input->post("ultima_nota", TRUE).' con la siguiente informacion:
                FF: '.$datos["nivel1"].'
                PF: '.$datos["nivel2"].'
                CC: '.$datos["nivel3"].'
                CAP: '.$datos["nivel4"].'
                CON: '.$datos["nivel5"].'
                PAR: '.$datos["nivel6"].'
                Gasto: '.$datos["gasto"].'
                Titulo del Gasto: '.$datos["titulo_gasto"].'
                U Medida: '.$datos["u_medida"].'
                Cantidad: '.$datos["cantidad"].'
                Precio: '.$datos["precio"].'
                Subtotal: '.$datos["subtotal"].'
                IVA: '.$datos["iva"].'
                Importe: '.$datos["importe"].'
                Descripcion: '.$datos["descripcion_detalle"].'
                Tipo Nota Entrada: '.$datos["tipo_poliza"]);

                $resultado_insertar_detalle = $this->patrimonio_model->insertar_nota_entrada_detalle($datos, $query_insertar);

                if(!$resultado_insertar_detalle) {
                    throw new Exception('Hubo un error al insertar los datos.');
                }
                else {
                    $respuesta = array(
                        "mensaje" => '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-check-circle fa-2x ic-msj"></i> Se han insertado los datos correctamente.</div>',
                    );

                }

                echo(json_encode($respuesta));
            } else {
                throw new Exception('No existe la estructura.');
            }

        } catch (Exception $e) {
            $respuesta = array(
                "mensaje" => '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-times-circle fa-2x ic-msj"></i> '.$e->getMessage().'</div>',
            );
            echo(json_encode($respuesta));
        }

    }

    function insertar_nota_entrada() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha insertado la nota de entrada: '.$this->input->post("ultima_nota", TRUE));

        $datos = $this->input->post();

        foreach ($datos as $key => $value) {
            $datos[$key]=trim($value);
        }

        try {

            if($datos["check_firme"] == 1) {
                $datos["estatus"] = "activo";
            } else {
                $datos["estatus"] = "espera";
            }

            $resultado_insertar = $this->patrimonio_model->insertar_caratula_nota_entrada($datos);

            if($resultado_insertar) {

                $respuesta = array(
                    "mensaje" => '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-check-circle fa-2x ic-msj"></i> Datos insertados correctamente</div>',
                );

                if($datos["check_firme"] == 1) {

                    $datos_insertados_kardex = $this->patrimonio_model->insertar_detalle_nota_entrada_kardex($datos);

                    if(!$datos_insertados_kardex) {
                        throw new Exception('Error al insertar los datos, por favor contacte a su administrador');
                    }
                }

            } else {
                throw new Exception('Error al insertar los datos, por favor contacte a su administrador');
            }

            echo(json_encode($respuesta));

        } catch (Exception $e) {
            $respuesta = array(
                "mensaje" => '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-times-circle fa-2x ic-msj"></i> '.$e->getMessage().'</div>',
            );

            echo(json_encode($respuesta));
        }

    }

    function tabla_nota_entrada_principal() {
        $resultado = $this->patrimonio_model->tabla_nota_entrada_principal();
//        $this->debugeo->imprimir_pre($resultado);
        $output = array("data" => "");
        foreach($resultado as $fila) {

            if($fila->enfirme == 1 && $fila->cancelada == 0) {
                $firme = '<button type="button" class="btn btn-circle btn-firmea" disabled><i class="fa fa-check"></i></button>';
                $estatus = "success";
            } elseif($fila->enfirme == 0 && $fila->cancelada == 0) {
                $firme = '<button type="button" class="btn btn-circle btn-firmec" disabled><i class="fa fa-times"></i></button>';
                $estatus = "warning";
            } elseif($fila->enfirme == 0 && $fila->cancelada == 1) {
                $firme = '<button type="button" class="btn btn-circle btn-firmec" disabled><i class="fa fa-times"></i></button>';
                $estatus = "danger";
            }

            if($this->utilerias->get_grupo() == 1) {
                $opciones = '<a href="' . base_url("patrimonio/ver_nota_entrada/" . $fila->numero_movimiento) . '" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                             <a href="' . base_url("patrimonio/editar_nota_entrada/" . $fila->numero_movimiento) . '" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                             <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>
                             <a href="' . base_url("patrimonio/imprimir_nota_entrada/" . $fila->numero_movimiento) . '" data-tooltip="Imprimir"><i class="fa fa-print"></i></a>';
            } else{
                if($this->utilerias->get_permisos("editar_nota_entrada") && $this->utilerias->get_permisos("ver_nota_entrada") && $this->utilerias->get_permisos("ver_nota_entrada")){
                    $opciones = '<a href="' . base_url("patrimonio/editar_nota_entrada/" . $fila->numero_movimiento) . '" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                             <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>
                             <a href="' . base_url("patrimonio/imprimir_nota_entrada/" . $fila->numero_movimiento) . '" data-tooltip="Imprimir"><i class="fa fa-print"></i></a>';
                } elseif($this->utilerias->get_permisos("ver_nota_entrada")){
                    $opciones = '<a href="' . base_url("patrimonio/ver_nota_entrada/" . $fila->numero_movimiento) . '" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                             <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>
                             <a href="' . base_url("patrimonio/imprimir_nota_entrada/" . $fila->numero_movimiento) . '" data-tooltip="Imprimir"><i class="fa fa-print"></i></a>';
                } elseif($this->utilerias->get_permisos("editar_nota_entrada")){
                    $opciones = '<a href="' . base_url("patrimonio/editar_nota_entrada/" . $fila->numero_movimiento) . '" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                             <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>
                             <a href="' . base_url("patrimonio/imprimir_nota_entrada/" . $fila->numero_movimiento) . '" data-tooltip="Imprimir"><i class="fa fa-print"></i></a>';
                } else{
                    $opciones = '<a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>
                             <a href="' . base_url("patrimonio/imprimir_nota_entrada/" . $fila->numero_movimiento) . '" data-tooltip="Imprimir"><i class="fa fa-print"></i></a>';
                }
            }

            $output["data"][] = array(
                '<div class="center">'.$fila->numero_movimiento.'</div>',
                '<div class="center">'.$fila->contrarecibo.'</div>',
                '<div class="center">'.$fila->tipo_poliza.'</div>',
                '<div class="center">'.$fila->documento.'</div>',
                '<div class="center">'.$fila->fecha_emision.'</div>',
                '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe == 0 || $fila->importe == NULL ? '0.00' : (number_format($fila->importe, 2)).'</div>'),
                $fila->creado_por,
                $firme,
                '<button type="button" class="btn btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                $opciones,
            );
        }
//        $this->debugeo->imprimir_pre($output);
        echo json_encode($output);
    }

    function tabla_detalle_nota_entrada() {
//        Se llama la funcion del modelo de egresos encargado de contar los niveles que existen
        $total_egresos = $this->egresos_model->contar_egresos_elementos();
//        Se llama a la funcion del model de egresos encargada de tomar los nombres delos niveles que existen
        $nombres_egresos = $this->egresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_egresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

        $query = "SELECT id_nota_entrada_detalle, ";

        for($i = 0; $i < $total_egresos->conteo; $i++){
            $query .= "COLUMN_GET(estructura, '".strtolower(str_replace(' ', '_', $nombre[$i]))."' as char) AS ".strtolower(str_replace(' ', '_', $nombre[$i])).", ";
        }

        $query .= "articulo, unidad_medida, cantidad, p_unitario, subtotal, iva, importe, titulo";
        $query .= " FROM mov_nota_entrada_detalle ";
        $query .= " WHERE numero_movimiento = ?;";

        $nota = $this->input->post("nota", TRUE);

        $resultado = $this->patrimonio_model->datosNotaEntradaDetalle($nota, $query);

        $output = array();

        foreach($resultado as $fila) {
            $output[] = array(
                $fila->id_nota_entrada_detalle,
                $fila->fuente_de_financiamiento,
                $fila->programa_de_financiamiento,
                $fila->centro_de_costos,
                $fila->capitulo,
                $fila->concepto,
                $fila->partida,
                $fila->articulo,
                $fila->unidad_medida,
                $fila->cantidad,
                $fila->p_unitario,
                $fila->subtotal,
                $fila->importe,
                $fila->titulo,
                '<a data-toggle="modal" data-target=".modal_editar" data-tooltip="Editar"><i class="fa fa-pencil-square-o"></i></a>',
            );
        }
//        $this->debugeo->imprimir_pre($output);
        echo json_encode($output);
    }

    function tabla_contrarecibo_nota_entrada() {
        $this->db->select('*')->from('mov_contrarecibo_caratula')->where('enfirme', 1)->where('destino', 'Bienes')->where('usado_patrimonio', 0)->order_by("id_contrarecibo_caratula", "desc") ;
        $query = $this->db->get();
        $resultado = $query->result();
//        $this->debugeo->imprimir_pre($resultado);
        $output = array("data" => "");
        foreach($resultado as $fila) {
            $firme = '<button type="button" class="btn btn-circle btn-firmea" disabled><i class="fa fa-check"></i></button>';
            $estatus = "success";
            $output["data"][] = array(
                $fila->id_contrarecibo_caratula,
                $fila->numero_compromiso,
                $fila->fecha_pago,
                $fila->destino,
                $fila->proveedor,
                "$".$this->cart->format_number($fila->importe),
                $fila->creado_por,
                $firme,
                '<button type="button" class="btn btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                '<a data-dismiss="modal"><i class="fa fa-check"></i></a>'
            );
        }
//        $this->debugeo->imprimir_pre($output);
        echo json_encode($output);
    }

    function ver_nota_entrada($nota_etrada = NULL) {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha visualizado una nota de entrada'.$nota_etrada);

        $query = "SELECT * FROM mov_nota_entrada_caratula WHERE numero_movimiento = ?";
        $resultado = $this->patrimonio_model->get_datos_nota_entrada_caratula($nota_etrada, $query);

        $datos_header = array(
            "titulo_pagina" => "Armonniza | Nota Entrada",
            "usuario" => $this->tank_auth->get_username(),
            "compromisocss" => TRUE,
            "tablas" => TRUE,
        );

//        $this->debugeo->imprimir_pre($resultado);

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/ver_nota_entrada_view', $resultado);
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "ver_nota_entrada" => TRUE,
        ));
    }

    function cancelar_nota_entrada_caratula() {
        $mensaje = array(
            "mensaje" => "",
        );

//      Se toma el devengado a cancelar
        $nota_entrada = $this->input->post("nota_entrada");
        //$this->debugeo->imprimir_pre($nota_entrada);
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha cancelado la Nota de Entrada '.$nota_entrada);

//      Se hace el query para tomar los datos de caratula del devengado
        $query_caratula = "SELECT * FROM mov_nota_entrada_caratula WHERE numero_movimiento = ?";

//      Se llama a la función para tomar los datos de la caratula del devengado
        $datos_caratula = $this->patrimonio_model->get_datos_nota_entrada_caratula($nota_entrada, $query_caratula);

//      Se actualiza el estatus del devengado a cancelado

        $query = "UPDATE mov_nota_entrada_caratula SET estatus = 'cancelado', cancelada = 1, fecha_cancelada = ? WHERE numero_movimiento = ?; ";
        $fin = $this->patrimonio_model->cancelarNotaEntradaCaratula($nota_entrada, $query);

        if($fin) {
            $mensaje["mensaje"] = "ok";
            echo(json_encode($mensaje));
        }
        else {
            $mensaje["mensaje"] = "error";
            echo(json_encode($mensaje));
        }

    }

    function imprimir_nota_entrada($nota_entrada = NULL) {
        $query = 'SELECT * FROM mov_nota_entrada_caratula WHERE numero_movimiento = ?;';
        $resultado_caratula = $this->patrimonio_model->get_datos_nota_entrada_caratula($nota_entrada, $query);
        //$this->debugeo->imprimir_pre($resultado_caratula);
        $query = 'SELECT  *, COLUMN_JSON(estructura) AS estructura FROM mov_nota_entrada_detalle WHERE numero_movimiento = ?;';
        $resultado_detalle = $this->patrimonio_model->datos_nota_entrada($nota_entrada, $query);
        //$this->debugeo->imprimir_pre($resultado_detalle);
        $this->load->library('Pdf');

        $pdf = new Pdf('L', 'cm', 'A4', true, 'UTF-8', false);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Armonniza');
        $pdf->SetTitle('Nota de Entrada');
        $pdf->SetSubject('Nota de Entrada');
        $pdf->SetKeywords('Armonniza');

// set default header data
//        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
//        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        // remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
//        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, 10 , PDF_MARGIN_RIGHT);
//        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// ---------------------------------------------------------

// set font
        $pdf->SetFont('helvetica', '', 12);

// add a page
        $pdf->AddPage('L', 'Letter');

        $encabezado = '<table cellspacing="3" style="font-size: small;">
                        <tr>
                            <td align="left"><b>Tipo de Entrada</b></td>
                            <td align="right"><b>Fecha Emisión</b></td>
                        </tr>
                        <tr>
                            <td align="left">'.$resultado_caratula->tipo_poliza.'</td>
                            <td align="right">'.$resultado_caratula->fecha_emision.'</td>
                        </tr>
                       </table>';

        $informacion_general = '<table cellspacing="3" style="font-size: small;">
                        <tr>
                            <td align="left" ><b>No. Nota de Entrada</b></td>
                            <td align="left" width="10%">'.$resultado_caratula->numero_movimiento.'</td>
                            <td align="left"><b>Clave Proveedor</b></td>
                            <td align="left" width="30%">'.$resultado_caratula->id_proveedor.'</td>
                             <td align="left"><b>Comprobante Fiscal</b></td>
                            <td align="left">'.$resultado_caratula->documento.'</td>
                        </tr>
                         <tr>
                            <td align="left"><b>No. Contra Recibo</b></td>
                            <td align="left" width="10%">'.$resultado_caratula->contrarecibo.'</td>
                            <td align="left"><b>Proveedor</b></td>
                            <td align="left" width="30%">'.$resultado_caratula->proveedor.'</td>
                            <td align="left"><b>No. Comprobante Fiscal</b></td>
                        </tr>
                        <tr>
                            <td align="left"><b>No. Compromiso</b></td>
                            <td align="left" width="10%">'.$resultado_caratula->compromiso.'</td>
                            <td align="left"></td>
                            <td align="left" width="30%"></td>
                            <td align="left"  width="25%">'.$resultado_caratula->no_documento.'</td>
                        </tr>
                       </table>';

        $total_unitario = 0;
        $total_importe = 0;

        $tabla_general = '<table style="font-size: small;  border: 1px solid #BDBDBD;" cellpadding="4">
                            <tr>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="22%">Estructura</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD; line-height: 20px;" width="12%">Artículo/Código</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="20%"><br>Descripción</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD" width="10%">Cantidad</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD; line-height: 20px;" width="10%">U/M</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD; line-height: 20px;" width="13%">Precio Unitario</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD; line-height: 20px;" width="13%">Importe</td>
                            </tr>';

        foreach ($resultado_detalle as $fila) {
            //$this->debugeo->imprimir_pre($fila);
            $estructura = json_decode($fila->estructura);
            //$this->debugeo->imprimir_pre($estructura);
            $tabla_general .= '<tr>';
            $tabla_general .= '<td align="left" style=" border-right: 1px solid #BDBDBD;" width="22%">' . $estructura->fuente_de_financiamiento . '&nbsp;&nbsp;' . $estructura->programa_de_financiamiento . '&nbsp;&nbsp;' . $estructura->centro_de_costos . '&nbsp;&nbsp;' . $estructura->capitulo . '&nbsp;&nbsp;' . $estructura->concepto . '&nbsp;&nbsp;' . $estructura->partida . '</td>';
            $tabla_general .= '<td align="lef" style=" border-right: 1px solid #BDBDBD;" width="12%">' . $fila->articulo . '</td>';
            $tabla_general .= '<td align="justify" style=" border-right: 1px solid #BDBDBD;" width="20%">' . $fila->titulo . '</td>';
            $tabla_general .= '<td align="center" style=" border-right: 1px solid #BDBDBD;" width="10%">' . ($fila->cantidad == NULL ? '-' : $fila->cantidad) . '</td>';
            $tabla_general .= '<td align="center" style=" border-right: 1px solid #BDBDBD;"  width="10%">' . ($fila->unidad_medida == NULL ? '-' : $fila->unidad_medida) . '</td>';
            $tabla_general .= '<td align="left" width="2%">$</td>';
            $tabla_general .= '<td align="right" style=" border-right: 1px solid #BDBDBD;" width="11%">' . ($fila->p_unitario == 0 || $fila->p_unitario == NULL ? '0.00' : (number_format($fila->p_unitario, 2))) . '</td>';
            $tabla_general .= '<td align="left" width="2%">$</td>';
            $tabla_general .= '<td align="right" style=" border-right: 1px solid #BDBDBD;" width="11%">' . ($fila->importe == 0 || $fila->importe == NULL ? '0.00' : (number_format($fila->importe, 2))) . '</td>';
            $tabla_general .= '</tr>';

            $total_unitario += $fila->p_unitario;
            $total_importe += $fila->importe;
        }
        $tabla_general .= '</table>';

        $totales = '<table style="font-size: small;  border: 1px solid #BDBDBD;" cellpadding="4">
                        <tr>
                            <td align="left" style=" border-right: 1px solid #BDBDBD;" width="43.5%"><b>Total</b></td>
                            <td align="left" width="5%">$</td>
                            <td align="right" style=" border-right: 1px solid #BDBDBD;"  width="23%"><b>'.($total_unitario == 0 || $total_unitario == NULL ? '0.00' : (number_format($total_unitario, 2))).'</b></td>
                            <td align="left" width="5%">$</td>
                            <td align="right" width="23.5%"><b>'.($total_importe == 0 || $total_importe == NULL ? '0.00' : (number_format($total_importe, 2))).'</b></td>
                        </tr>
                       </table>';


        $html = '
        <style>
            .cont-general{
                border: 1px solid #eee;
                border-radius: 1%;
                margin: 2% 14%;
                width: 102%;
            }
            .cont-general2{
                border: 1px solid #BDBDBD;
            }
            .cont-general .borde-inf{
                border-bottom: 1px solid #eee;
            }
            .cont-general .borde-sup{
                border-top: 1px solid #eee;
            }
        </style>

        <table class="cont-general" border="0" cellspacing="3" style="font-size: small;" cellpadding="2">
            <tr cellspacing="10">
                <th width="2%"></th>
                <th align="left" width="10%"><img src="'.base_url("img/logo2.png").'" /></th>
                <th align="center" width="84%" style="line-height: 7px;">
                    <h3>EDUCAL, S.A. DE C.V.</h3>
                    <h5>Nota de Entrada</h5>
                    '.$encabezado.'
                </th>
                <th width="2%"></th>
            </tr>
            <tr>
                <td width="1%" class="borde-sup"></td>
                <td width="98%" class="borde-sup">'.$informacion_general.'</td>
            </tr>
            <tr><td width="99%" class="borde-sup"></td></tr>
            <tr>
                <td width="1%"></td>
                <td width="96.5%">'.$tabla_general.'</td>
                <td width="1%"></td>
            </tr>
            <tr>
                <td width="53%"></td>
                <td width="44.5%">'.$totales.'</td>
                <td width="1%"></td>
            </tr>

</table>';

// output the HTML content

        $pdf->writeHTML($html, false, false, true, false, 'top');

//Close and output PDF document
        $pdf->Output('Nota de Entrada.pdf', 'I');
    }

    function subir_nota_entrada() {

        $mensaje = array("mensaje" => "" );

//        Se prepara la configuracion de la libreria "Upload"
        $config['upload_path'] = './application/archivos/';
        $config['allowed_types'] = '*';

//        Se carga la libreria con sus configuraciones correspondientes
        $this->load->library('upload', $config);

//        Si el server no logra subir el archivo, despliega un mensaje de error
        if ( ! $this->upload->do_upload('archivoSubir')) {
//            Se capturan los errores en una variable y se imprimen para debug
            $error = array('error' => $this->upload->display_errors());
            $this->debugeo->imprimir_pre($error);
        }
        else {
            ini_set('memory_limit', '-1');
//            load our new PHPExcel library
            $this->load->library('excel');
//            Si el server logra subir el archivo, se toman los datos del archivo
            $data = $this->upload->data();

//            Se toma la ruta del archivo junto con su nombre y extension
            $file = $data["full_path"];

//            read file from path
            $objPHPExcel = PHPExcel_IOFactory::load($file);

//            get only the Cell Collection
            $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();

            try {

                foreach ($cell_collection as $cell) {
                    $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
                    $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
                    $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();

                    if ($row == 1 || $row == 3) {
                        continue;
                    } elseif ($row == 2) {
                        if(empty($data_value)) {
                            throw new Exception('Ha ocurrido un error en el encabezado del documento, por favor revíselo.');
                        } else {
                            $datos_caratula[$row][$column] = trim($data_value);
                        }
                    } else {
                        if(empty($data_value)) {
                            throw new Exception('Ha ocurrido un error en el detalle del documento, por favor revíselo.');
                        } else {
                            $datos_detalle[$row][$column] = trim($data_value);
                        }

                    }
                }

                $last = 0;
                $total = 0;


                $this->db->trans_begin();

                // Se toma el numero de la última adecuacion
                $ultimo = $this->patrimonio_model->ultima_nota_entrada();

                if($ultimo) {
                    $last = $ultimo->ultimo + 1;
                }
                else {
                    $last = 1;
                }

                $this->patrimonio_model->apartar_nota_entrada($last);

                $numero_fila = 4;

                foreach($datos_detalle as $key => $value) {

                    // Se llama la funcion del modelo de egresos encargado de contar los niveles que existen
                    $total_egresos = $this->egresos_model->contar_egresos_elementos();

                    // Se llama a la funcion del model de egresos encargada de tomar los nombres delos niveles que existen
                    $nombres_egresos = $this->egresos_model->obtener_nombre_niveles();

                    // En este arreglo se van a guardar los nombres de los niveles
                    $nombre = array();

                    // En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
                    foreach($nombres_egresos as $fila) {
                        array_push($nombre, $fila->descripcion);
                    }

                    $value["ultima_nota"] = $last;

                    $existe = $this->revisar_estructura_existe($value);

                    if($existe) {

                        $existe_articulo = $this->patrimonio_model->verificar_articulo($value["G"]);

                        if(!$existe_articulo["articulo"]) {
                            throw new Exception('La art&iacute;culo en la fila '.$numero_fila.' no existe, por favor rev&iacute;sela.');
                        }

                        $value["titulo_gasto"] = $existe_articulo["descripcion"];

                        $value["u_medida"] = $this->patrimonio_model->tomar_unidad_medida($value["H"]);

                        // Se calcula el subtotal del gasto multiplicando la cantidad por el precio insertado
                        $value["subtotal"] = $value["I"] * $value["J"];

                        if($value["K"] == "Si") {
                            $value["iva"] = $value["subtotal"] * 0.16;

                        } else {
                            $value["iva"] = 0;
                        }

                        $value["id_nivel"] = $existe["ID"];

                        $value["importe"] = $value["subtotal"] + $value["iva"];

                        $total += $value["importe"];

                        $query_insertar = "INSERT INTO mov_nota_entrada_detalle (
                                                                                numero_movimiento, id_nivel, articulo, unidad_medida, cantidad,
                                                                                p_unitario, subtotal, iva, importe, titulo,
                                                                                descripcion, observaciones, estructura
                                                                                )
                                                                                VALUES (
                                                                                ?, ?, ?, ?, ?,
                                                                                ?, ?, ?, ?,
                                                                                ?, ?, ?, COLUMN_CREATE(";

                        for($i = 0; $i < $total_egresos->conteo; $i++) {
                            $query_insertar .= "'".strtolower(str_replace(' ', '_', $nombre[$i]))."', ?, ";
                        }

                        $query_insertar .= "'articulo', ?));";

                        $resultado_insertar_detalle = $this->patrimonio_model->insertar_nota_entrada_detalle_archivo($value, $query_insertar);

                        if(!$resultado_insertar_detalle) {
                            throw new Exception('Hubo un error al insertar los datos, por favor, contacte a su administrador.');
                        }

                    } else {
                        throw new Exception('La estructura en la fila '.$numero_fila.' no existe, por favor rev&iacute;sela.');
                    }

                    $numero_fila += 1;
                }

                unset($key);
                unset($value);

                $respuesta_caratula = FALSE;

                foreach($datos_caratula as $key => $value) {

                    $fecha = ($value['B'] - 25569) * 86400;
                    $value["B"] = date('Y-m-d', $fecha);
                    $value["importe_total"] = $total;
                    $value["ultima_nota"] = $last;

                    $resultado_insertar = $this->patrimonio_model->insertar_caratula_nota_entrada_archivo($value);

                }

                if ($this->db->trans_status() === FALSE) {
                    throw new Exception('Hubo un error al insertar los datos, por favor, contacte a su administrador.');
                }
                else {
                    $this->db->trans_commit();
                    $mensaje["mensaje"] = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>La nota de entrada se insert&oacute; correctamente.</div>';
                }


            } catch (Exception $e) {
                $this->db->trans_rollback();

                $mensaje["mensaje"] = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$e->getMessage().'</div>';

            }

            $datos_header = array(
                "titulo_pagina" => "Armonniza | Resultado Archivo",
                "usuario" => $this->tank_auth->get_username(),
                "precompromisocss" => TRUE,
            );

            $this->parser->parse('front/header_main_view', $datos_header);
            $this->load->view('front/resultado_archivo_view', array(
                "mensaje" => $mensaje["mensaje"],
            ));
            $this->load->view('front/footer_main_view');

        }
    }

    private function revisar_estructura_existe($datos) {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha revisado una estructura ya existente');
//        Se llama a la funcion del model de egresos encargada de tomar los nombres delos niveles que existen
        $nombres_egresos = $this->egresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_egresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

//       Se toman los datos de la estructura inferior
        $datos_partida_inferior = $this->patrimonio_model->datos_de_partida(array(
            "nivel1" => $datos["A"],
            "nivel2" => $datos["B"],
            "nivel3" => $datos["C"],
            "nivel4" => $datos["D"],
            "nivel5" => $datos["E"],
            "nivel6" => $datos["F"]),
            $nombre);

        if($datos_partida_inferior) {
            return $datos_partida_inferior;
        } else {
            return FALSE;
        }
    }

    /** Sección Nota de Salida */
    function nota_salida() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha insertado un detalle en la nota de salida');
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Nota de Salida",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/patrimonio_nota_salida_view');
        $this->load->view('front/footer_main_view', array("tablas" => TRUE, 'nota_salida' => TRUE));
    }

    function tabla_nota_salida_principal() {
        $resultado = $this->patrimonio_model->tabla_nota_salida_principal();
//        $this->debugeo->imprimir_pre($resultado);
        $output = array("data" => "");
        foreach($resultado as $fila) {

            if($fila->enfirme == 1 && $fila->cancelada == 0) {
                $firme = '<button type="button" class="btn btn-circle btn-firmea" disabled><i class="fa fa-check"></i></button>';
                $estatus = "success";
            } elseif($fila->enfirme == 0 && $fila->cancelada == 0) {
                $firme = '<button type="button" class="btn btn-circle btn-firmec" disabled><i class="fa fa-times"></i></button>';
                $estatus = "warning";
            } elseif($fila->enfirme == 0 && $fila->cancelada == 1) {
                $firme = '<button type="button" class="btn btn-circle btn-firmec" disabled><i class="fa fa-times"></i></button>';
                $estatus = "danger";
            }

            if($this->utilerias->get_grupo() == 1) {
                $opciones = '<a href="' . base_url("patrimonio/ver_nota_salida/" . $fila->numero_movimiento) . '" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                             <a href="' . base_url("patrimonio/editar_nota_salida/" . $fila->numero_movimiento) . '" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                             <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>
                             <a href="' . base_url("patrimonio/imprimir_nota_salida/" . $fila->numero_movimiento) . '" data-tooltip="Imprimir"><i class="fa fa-print"></i></a>';
            } else{
                if($this->utilerias->get_permisos("editar_nota_salida") && $this->utilerias->get_permisos("ver_nota_salida")){
                    $opciones = '<a href="' . base_url("patrimonio/ver_nota_salida/" . $fila->numero_movimiento) . '" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                             <a href="' . base_url("patrimonio/editar_nota_salida/" . $fila->numero_movimiento) . '" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                             <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>
                             <a href="' . base_url("patrimonio/imprimir_nota_salida/" . $fila->numero_movimiento) . '" data-tooltip="Imprimir"><i class="fa fa-print"></i></a>';
                } elseif($this->utilerias->get_permisos("ver_nota_salida")){
                    $opciones = '<a href="' . base_url("patrimonio/ver_nota_salida/" . $fila->numero_movimiento) . '" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                             <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>
                             <a href="' . base_url("patrimonio/imprimir_nota_salida/" . $fila->numero_movimiento) . '" data-tooltip="Imprimir"><i class="fa fa-print"></i></a>';
                } elseif($this->utilerias->get_permisos("editar_nota_salida")){
                    $opciones = '<a href="' . base_url("patrimonio/editar_nota_salida/" . $fila->numero_movimiento) . '" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                             <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>
                             <a href="' . base_url("patrimonio/imprimir_nota_salida/" . $fila->numero_movimiento) . '" data-tooltip="Imprimir"><i class="fa fa-print"></i></a>';
                } else{
                    $opciones = '<a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>
                             <a href="' . base_url("patrimonio/imprimir_nota_salida/" . $fila->numero_movimiento) . '" data-tooltip="Imprimir"><i class="fa fa-print"></i></a>';
                }
            }

            $output["data"][] = array(
                $fila->id_nota_salida_caratula,
                '<div class="center">'.$fila->numero_movimiento.'</div>',
                '<div class="center">'.$fila->concepto_salida.'</div>',
                '<div class="center">'.$fila->fecha_emision.'</div>',
                '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe == 0 || $fila->importe == NULL ? '0.00' : (number_format($fila->importe, 2)).'</div>'),
                $fila->creado_por,
                $firme,
                '<button type="button" class="btn btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                $opciones,
            );
        }
//        $this->debugeo->imprimir_pre($output);
        echo json_encode($output);
    }

    function tabla_detalle_nota_salida() {
//        Se llama la funcion del modelo de egresos encargado de contar los niveles que existen
        $total_egresos = $this->egresos_model->contar_egresos_elementos();
//        Se llama a la funcion del model de egresos encargada de tomar los nombres delos niveles que existen
        $nombres_egresos = $this->egresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_egresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

        $query = "SELECT id_nota_salida_detalle, ";

        for($i = 0; $i < $total_egresos->conteo; $i++){
            $query .= "COLUMN_GET(estructura, '".strtolower(str_replace(' ', '_', $nombre[$i]))."' as char) AS ".strtolower(str_replace(' ', '_', $nombre[$i])).", ";
        }

        $query .= "articulo, unidad_medida, cantidad, p_unitario, subtotal, iva, importe, titulo";
        $query .= " FROM mov_nota_salida_detalle ";
        $query .= " WHERE numero_movimiento = ?;";

        $nota = $this->input->post("nota", TRUE);

        $resultado = $this->patrimonio_model->datosNotaEntradaDetalle($nota, $query);

        $output = array();

        foreach($resultado as $fila) {
            $output[] = array(
                $fila->id_nota_salida_detalle,
                $fila->fuente_de_financiamiento,
                $fila->programa_de_financiamiento,
                $fila->centro_de_costos,
                $fila->capitulo,
                $fila->concepto,
                $fila->partida,
                $fila->articulo,
                $fila->unidad_medida,
                $fila->cantidad,
                $fila->p_unitario,
                $fila->subtotal,
                $fila->importe,
                $fila->titulo,
                '<a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Eliminar"><i class="fa fa-remove"></i></a>',
            );
        }
//        $this->debugeo->imprimir_pre($output);
        echo json_encode($output);
    }

    function agregar_nota_salida() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha agregado una nota de salida');
        $last = 0;
//        Se toma el numero del ultima Nota de Salida
        $ultimo = $this->patrimonio_model->ultima_nota_salida();

        if($ultimo) {
            $last = $ultimo->ultimo + 1;
        }
        else {
            $last = 1;
        }

        $this->patrimonio_model->apartar_nota_salida($last);

        $datos_header = array(
            "titulo_pagina" => "Armonniza | Nota de Salida",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
            "precompromisocss" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/agregar_nota_salida_view', array(
            "ultima_nota" => $last,
            "niveles" => $this->input_niveles_egresos(),
            "niveles_modal" => $this->preparar_estructura_egresos(),
        ));
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "agregar_nota_salida" => TRUE,
        ));
    }

    function insertar_nota_salida() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha insertado la nota de salida: '.$this->input->post("ultima_nota", TRUE));

        $datos = $this->input->post();

        foreach ($datos as $key => $value) {
            $datos[$key]=trim($value);
        }
        try {

            if($datos["check_firme"] == 1) {
                $datos["estatus"] = "activo";
            } else {
                $datos["estatus"] = "espera";
            }

            $resultado_insertar = $this->patrimonio_model->insertar_caratula_nota_salida($datos);

            if($resultado_insertar) {

                $respuesta = array(
                    "mensaje" => '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-check-circle fa-2x ic-msj"></i> Datos insertados correctamente.</div>',
                );

                if($datos["check_firme"] == 1) {

                    $datos_insertados_kardex = $this->patrimonio_model->insertar_detalle_nota_salida_kardex($datos);

                    if(is_string($datos_insertados_kardex)) {
                        throw new Exception($datos_insertados_kardex);
                    }
                    elseif($datos_insertados_kardex == FALSE) {
                        throw new Exception('Error al insertar los datos, por favor contacte a su administrador');
                    }
                }

            } else {
                throw new Exception('Error al insertar los datos, por favor contacte a su administrador');
            }

            echo(json_encode($respuesta));

        } catch (Exception $e) {
            $respuesta = array(
                "mensaje" => '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-times-circle fa-2x ic-msj"></i> '.$e->getMessage().'</div>',
            );

            echo(json_encode($respuesta));
        }

    }

    function insertar_detalle_nota_salida() {

        $datos = $this->input->post();

        foreach ($datos as $key => $value) {
            $datos[$key]=trim($value);
        }

//        $this->debugeo->imprimir_pre($datos);

        try {
//        Se llama la funcion del modelo de egresos encargado de contar los niveles que existen
            $total_egresos = $this->egresos_model->contar_egresos_elementos();
//        Se llama a la funcion del model de egresos encargada de tomar los nombres delos niveles que existen
            $nombres_egresos = $this->egresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
            $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
            foreach($nombres_egresos as $fila) {
                array_push($nombre, $fila->descripcion);
            }

//        Se genera el query para revisar que la estructura exista
            $query = "SELECT id_niveles FROM cat_niveles WHERE COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[0]))."' as char) = ? ";

            for($i = 1; $i < $total_egresos->conteo; $i++){
                $query .= "AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[$i]))."' as char) = ? ";
            }

//            Primero hay que revisar si existe la estructura
            $existe = $this->ciclo_model->existeEstructura($datos, $query);

            if($existe->id_niveles) {
//            Se calcula el subtotal del gasto multiplicando la cantidad por el precio insertado
                $datos["subtotal"] = $datos["cantidad"] * $datos["precio"];
                $datos["subtotal"] = round($datos["subtotal"], 2);

                $datos['id_nivel'] = $existe->id_niveles;

                $datos["iva"] = 0;
                $datos["importe"] = round($datos["subtotal"], 2);

                $query_insertar = "INSERT INTO mov_nota_salida_detalle (
                                                                        numero_movimiento, id_nivel, articulo, unidad_medida, cantidad,
                                                                        p_unitario, subtotal, iva, importe, titulo,
                                                                        descripcion, numero_movimiento_entrada, estructura
                                                                        )
                                                                        VALUES (
                                                                        ?, ?, ?, ?, ?,
                                                                        ?, ?, ?, ?, ?,
                                                                        ?, ?, COLUMN_CREATE(";

                for($i = 0; $i < $total_egresos->conteo; $i++){
                    $query_insertar .= "'".strtolower(str_replace(' ', '_', $nombre[$i]))."', ?, ";
                }

                $query_insertar .= "'articulo', ?));";

                log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha insertado un detalle en la nota de salida: '.$this->input->post("ultima_nota", TRUE).' con la siguiente información:
                FF: '.$datos["nivel1"].'
                PF: '.$datos["nivel2"].'
                CC: '.$datos["nivel3"].'
                CAP: '.$datos["nivel4"].'
                CON: '.$datos["nivel5"].'
                PAR: '.$datos["nivel6"].'
                Gasto: '.$datos["gasto"].'
                Titulo del Gasto: '.$datos["titulo_gasto"].'
                U Medida: '.$datos["u_medida"].'
                Cantidad: '.$datos["cantidad"].'
                Precio: '.$datos["precio"].'
                Subtotal: '.$datos["subtotal"].'
                IVA: '.$datos["iva"].'
                Importe: '.$datos["importe"].'
                Descripcion: '.$datos["descripcion_detalle"]);

                $resultado_insertar_detalle = $this->patrimonio_model->insertar_nota_salida_detalle($datos, $query_insertar);

                if(!$resultado_insertar_detalle) {
                    throw new Exception('Hubo un error al insertar los datos.');
                }
                else {
                    $respuesta = array(
                        "mensaje" => '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-check-circle fa-2x ic-msj"></i> Se han insertado los datos correctamente.</div>',
                    );

                }

                echo(json_encode($respuesta));
            } else {
                throw new Exception('No existe la estructura.');
            }

        } catch (Exception $e) {
            $respuesta = array(
                "mensaje" => '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-times-circle fa-2x ic-msj"></i> '.$e->getMessage().'</div>',
            );
            echo(json_encode($respuesta));
        }

    }

    function editar_nota_salida($notaSalida = NULL) {
//        Se guarda en el log el movimiento del usuario
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha editado la Nota de Salida: '.$notaSalida);

//        Se toma el detalle del movimiento de  la nota de entrada
        $this->db->select('*')->from('mov_nota_salida_caratula')->where('numero_movimiento', $notaSalida);
        $query = $this->db->get();
        $resultado = $query->row_array();

//        Se preparan las estructuras de egresos
        $resultado["niveles"] = $this->input_niveles_egresos();
        $resultado["niveles_modal"] = $this->preparar_estructura_egresos();

//        $this->debugeo->imprimir_pre($resultado);

        $datos_header = array(
            "titulo_pagina" => "Armonniza | Nota de Salida",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
            "editar_nota_salida" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/editar_nota_salida_view', $resultado);
        $this->load->view('front/footer_main_view', array("tablas" => TRUE, "editar_nota_salida" => TRUE));
    }

    function borrar_detalle_nota_salida() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha borrado un detalle en la nota de Salida');
        $mensaje = array(
            "mensaje" => "",
        );

        $nota = $this->input->post("id_nota");

        $resultado = $this->patrimonio_model->borrarDetalleNotaSalida($nota);

        echo(json_encode($mensaje));
    }

    function tabla_productos_nota_entrada() {
        $resultados = $this->patrimonio_model->datos_productos_nota_entrada();
        $output = array('data' => '');
        foreach($resultados as $fila) {
            $estructura = json_decode($fila->estructura);
            //$this->debugeo->imprimir_pre($estructura->programa_de_financiamiento);
            $output['data'][] = array(
                $fila->numero_movimiento,
                $fila->articulo,
                $fila->titulo,
                $fila->unidad_medida,
                $fila->cantidad,
                "$".($fila->p_unitario == 0 || $fila->p_unitario == NULL ? '0.00' : number_format($fila->p_unitario, 2)),
                $fila->descripcion,
                $estructura->fuente_de_financiamiento,
                $estructura->programa_de_financiamiento,
                $estructura->centro_de_costos,
                $estructura->capitulo,
                $estructura->concepto,
                $estructura->partida,
                '<a class="seleccion_gasto" data-toggle="modal" data-dismiss="modal" title="Seleccionar Gasto"><i class="fa fa-check"></i></a>',
            );
        }
//        $this->debugeo->imprimir_pre($output);
        echo(json_encode($output));
    }

    function cancelar_nota_salida_caratula() {
        $mensaje = array(
            "mensaje" => "",
        );

//      Se toma el devengado a cancelar
        $nota_salida = $this->input->post("nota_salida");
        //$this->debugeo->imprimir_pre($nota_entrada);
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha cancelado la Nota de Salida '.$nota_salida);

//      Se hace el query para tomar los datos de caratula de la Nota de Salida
        $query_caratula = "SELECT * FROM mov_nota_salida_caratula WHERE numero_movimiento = ?";

//      Se llama a la función para tomar los datos de la caratula de la Nota de Salida
        $datos_caratula = $this->patrimonio_model->get_datos_nota_salida_caratula($nota_salida, $query_caratula);

//      Se actualiza el estatus de la Nota de Salida a cancelado

        $query = "UPDATE mov_nota_salida_caratula SET estatus = 'cancelado', cancelada = 1, fecha_cancelada = ? WHERE numero_movimiento = ?; ";
        $fin = $this->patrimonio_model->cancelarNotaSalidaCaratula($nota_salida, $query);

        if($fin) {
            $mensaje["mensaje"] = "ok";
            echo(json_encode($mensaje));
        }
        else {
            $mensaje["mensaje"] = "error";
            echo(json_encode($mensaje));
        }

    }

    function ver_nota_salida($nota_salida = NULL) {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha visualizado una Nota de Salida'.$nota_salida);

        $query = "SELECT * FROM mov_nota_salida_caratula WHERE numero_movimiento = ?";
        $resultado = $this->patrimonio_model->get_datos_nota_salida_caratula($nota_salida, $query);

        $datos_header = array(
            "titulo_pagina" => "Armonniza | Nota Salida",
            "usuario" => $this->tank_auth->get_username(),
            "compromisocss" => TRUE,
            "tablas" => TRUE,
        );

//        $this->debugeo->imprimir_pre($resultado);

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/ver_nota_salida_view', $resultado);
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "ver_nota_salida" => TRUE,
        ));
    }

    function imprimir_nota_salida($nota_salida = NULL) {
        $query = 'SELECT * FROM mov_nota_salida_caratula WHERE numero_movimiento = ?;';
        $resultado_caratula = $this->patrimonio_model->get_datos_nota_salida_caratula($nota_salida, $query);
        //$this->debugeo->imprimir_pre($resultado_caratula);
        $query = 'SELECT  *, COLUMN_JSON(estructura) AS estructura FROM mov_nota_salida_detalle WHERE numero_movimiento = ?;';
        $resultado_detalle = $this->patrimonio_model->datos_nota_salida($nota_salida, $query);
        //$this->debugeo->imprimir_pre($resultado_detalle);
        $this->load->library('Pdf');

        $pdf = new Pdf('L', 'cm', 'A4', true, 'UTF-8', false);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Armonniza');
        $pdf->SetTitle('Nota de Salida');
        $pdf->SetSubject('Nota de Salida');
        $pdf->SetKeywords('Armonniza');

// set default header data
//        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
//        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        // remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
//        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, 10 , PDF_MARGIN_RIGHT);
//        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// ---------------------------------------------------------

// set font
        $pdf->SetFont('helvetica', '', 12);

// add a page
        $pdf->AddPage('L', 'Letter');

        $encabezado = '<table cellspacing="3" style="font-size: small;">
                        <tr>
                            <td align="left"><b>Concepto de Salida</b></td>
                            <td align="right"><b>Fecha Emisión</b></td>
                        </tr>
                        <tr>
                            <td align="left">'.$resultado_caratula->concepto_salida.'</td>
                            <td align="right">'.$resultado_caratula->fecha_emision.'</td>
                        </tr>
                       </table>';

        $informacion_general = '<table cellspacing="3" style="font-size: small;">
                        <tr>
                            <td align="left" width="15%"><b>No. Nota de Salida</b></td>
                            <td align="left" width="5%">'.$resultado_caratula->numero_movimiento.'</td>
                            <td align="left" width="15%"><b>Centro de Costos</b></td>
                            <td align="left" width="50%">'.$resultado_caratula->centro_costos.' / '.$resultado_caratula->descripcion_centro_costos.'</td>
                        </tr>
                        <tr>
                            <td align="left" width="15%"><b></b></td>
                            <td align="left" width="5%"></td>
                            <td align="left" width="15%"><b>Observaciones</b></td>
                            <td align="left" width="50%">'.$resultado_caratula->observaciones.'</td>
                        </tr>
                       </table>';

        $total_unitario = 0;
        $total_importe = 0;

        $tabla_general = '<table style="font-size: small;  border: 1px solid #BDBDBD;" cellpadding="4">
                            <tr>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="22%">Estructura</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD; line-height: 20px;" width="12%">Artículo/Código</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="20%"><br>Descripción</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD" width="10%">Cantidad</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD; line-height: 20px;" width="10%">U/M</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD; line-height: 20px;" width="13%">Precio Unitario</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD; line-height: 20px;" width="13%">Importe</td>
                            </tr>';

        foreach ($resultado_detalle as $fila) {
            //$this->debugeo->imprimir_pre($fila);
            $estructura = json_decode($fila->estructura);
            //$this->debugeo->imprimir_pre($estructura);
            $tabla_general .= '<tr>';
            $tabla_general .= '<td align="left" style=" border-right: 1px solid #BDBDBD;" width="22%">' . $estructura->fuente_de_financiamiento . '&nbsp;&nbsp;' . $estructura->programa_de_financiamiento . '&nbsp;&nbsp;' . $estructura->centro_de_costos . '&nbsp;&nbsp;' . $estructura->capitulo . '&nbsp;&nbsp;' . $estructura->concepto . '&nbsp;&nbsp;' . $estructura->partida . '</td>';
            $tabla_general .= '<td align="lef" style=" border-right: 1px solid #BDBDBD;" width="12%">' . $fila->articulo . '</td>';
            $tabla_general .= '<td align="justify" style=" border-right: 1px solid #BDBDBD;" width="20%">' . $fila->titulo . '</td>';
            $tabla_general .= '<td align="center" style=" border-right: 1px solid #BDBDBD;" width="10%">' . ($fila->cantidad == NULL ? '-' : $fila->cantidad) . '</td>';
            $tabla_general .= '<td align="center" style=" border-right: 1px solid #BDBDBD;"  width="10%">' . ($fila->unidad_medida == NULL ? '-' : $fila->unidad_medida) . '</td>';
            $tabla_general .= '<td align="left" width="2%">$</td>';
            $tabla_general .= '<td align="right" style=" border-right: 1px solid #BDBDBD;" width="11%">' . ($fila->p_unitario == 0 || $fila->p_unitario == NULL ? '0.00' : (number_format($fila->p_unitario, 2))) . '</td>';
            $tabla_general .= '<td align="left" width="2%">$</td>';
            $tabla_general .= '<td align="right" style=" border-right: 1px solid #BDBDBD;" width="11%">' . ($fila->importe == 0 || $fila->importe == NULL ? '0.00' : (number_format($fila->importe, 2))) . '</td>';
            $tabla_general .= '</tr>';

            $total_unitario += $fila->p_unitario;
            $total_importe += $fila->importe;
        }
        $tabla_general .= '</table>';

        $totales = '<table style="font-size: small;  border: 1px solid #BDBDBD;" cellpadding="4">
                        <tr>
                            <td align="left" style=" border-right: 1px solid #BDBDBD;" width="43.5%"><b>Total</b></td>
                            <td align="left" width="5%">$</td>
                            <td align="right" style=" border-right: 1px solid #BDBDBD;"  width="23%"><b>'.($total_unitario == 0 || $total_unitario == NULL ? '0.00' : (number_format($total_unitario, 2))).'</b></td>
                            <td align="left" width="5%">$</td>
                            <td align="right" width="23.5%"><b>'.($total_importe == 0 || $total_importe == NULL ? '0.00' : (number_format($total_importe, 2))).'</b></td>
                        </tr>
                       </table>';


        $html = '
        <style>
            .cont-general{
                border: 1px solid #eee;
                border-radius: 1%;
                margin: 2% 14%;
                width: 102%;
            }
            .cont-general2{
                border: 1px solid #BDBDBD;
            }
            .cont-general .borde-inf{
                border-bottom: 1px solid #eee;
            }
            .cont-general .borde-sup{
                border-top: 1px solid #eee;
            }
        </style>

        <table class="cont-general" border="0" cellspacing="3" style="font-size: small;" cellpadding="2">
            <tr cellspacing="10">
                <th width="2%"></th>
                <th align="left" width="10%"><img src="'.base_url("img/logo2.png").'" /></th>
                <th align="center" width="84%" style="line-height: 7px;">
                    <h3>EDUCAL, S.A. DE C.V.</h3>
                    <h5>Nota de Salida</h5>
                    '.$encabezado.'
                </th>
                <th width="2%"></th>
            </tr>
            <tr>
                <td width="1%" class="borde-sup"></td>
                <td width="98%" class="borde-sup">'.$informacion_general.'</td>
            </tr>
            <tr><td width="99%" class="borde-sup"></td></tr>
            <tr>
                <td width="1%"></td>
                <td width="96.5%">'.$tabla_general.'</td>
                <td width="1%"></td>
            </tr>
            <tr>
                <td width="53%"></td>
                <td width="44.5%">'.$totales.'</td>
                <td width="1%"></td>
            </tr>

</table>';

// output the HTML content

        $pdf->writeHTML($html, false, false, true, false, 'top');

//Close and output PDF document
        $pdf->Output('Nota de Entrada.pdf', 'I');
    }

    function subir_nota_salida() {

        $mensaje = array("mensaje" => "" );

//        Se prepara la configuracion de la libreria "Upload"
        $config['upload_path'] = './application/archivos/';
        $config['allowed_types'] = '*';

//        Se carga la libreria con sus configuraciones correspondientes
        $this->load->library('upload', $config);

//        Si el server no logra subir el archivo, despliega un mensaje de error
        if ( ! $this->upload->do_upload('archivoSubir')) {
//            Se capturan los errores en una variable y se imprimen para debug
            $error = array('error' => $this->upload->display_errors());
            $this->debugeo->imprimir_pre($error);
        }
        else {
            ini_set('memory_limit', '-1');
//            load our new PHPExcel library
            $this->load->library('excel');
//            Si el server logra subir el archivo, se toman los datos del archivo
            $data = $this->upload->data();

//            Se toma la ruta del archivo junto con su nombre y extension
            $file = $data["full_path"];

//            read file from path
            $objPHPExcel = PHPExcel_IOFactory::load($file);

//            get only the Cell Collection
            $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();

            try {

                foreach ($cell_collection as $cell) {
                    $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
                    $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
                    $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();

                    if ($row == 1 || $row == 3) {
                        continue;
                    } elseif ($row == 2) {
                        // $this->debugeo->imprimir_pre($data_value);
                        if(empty($data_value)) {
                            throw new Exception('Ha ocurrido un error en el encabezado del documento, por favor revíselo.');
                        } else {
                            $datos_caratula[$row][$column] = trim($data_value);
                        }
                    } else {
                        if(empty($data_value)) {
                            throw new Exception('Ha ocurrido un error en el detalle del documento, por favor revíselo.');
                        } else {
                            $datos_detalle[$row][$column] = trim($data_value);
                        }

                    }
                }

                $last = 0;
                $total = 0;

                $this->db->trans_begin();

                // Se toma el numero de la última adecuacion
                $ultimo = $this->patrimonio_model->ultima_nota_salida();

                if($ultimo) {
                    $last = $ultimo->ultimo + 1;
                }
                else {
                    $last = 1;
                }

                $this->patrimonio_model->apartar_nota_salida($last);

                $numero_fila = 4;

                foreach($datos_detalle as $key => $value) {

                    // Se llama la funcion del modelo de egresos encargado de contar los niveles que existen
                    $total_egresos = $this->egresos_model->contar_egresos_elementos();

                    // Se llama a la funcion del model de egresos encargada de tomar los nombres delos niveles que existen
                    $nombres_egresos = $this->egresos_model->obtener_nombre_niveles();

                    // En este arreglo se van a guardar los nombres de los niveles
                    $nombre = array();

                    // En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
                    foreach($nombres_egresos as $fila) {
                        array_push($nombre, $fila->descripcion);
                    }

                    $value["ultima_nota"] = $last;

                    $existe = $this->revisar_estructura_existe($value);

                    if($existe) {

                        $existe_articulo = $this->patrimonio_model->verificar_articulo($value["G"]);

                        if(!$existe_articulo["articulo"]) {
                            throw new Exception('La art&iacute;culo en la fila '.$numero_fila.' no existe, por favor rev&iacute;sela.');
                        }

                        $value["titulo_gasto"] = $existe_articulo["descripcion"];

                        $value["u_medida"] = $this->patrimonio_model->tomar_unidad_medida($value["H"]);

                        // Se calcula el subtotal del gasto multiplicando la cantidad por el precio insertado
                        $value["subtotal"] = $value["I"] * $value["J"];

                        if($value["K"] == "Si") {
                            $value["iva"] = $value["subtotal"] * 0.16;

                        } else {
                            $value["iva"] = 0;
                        }

                        $value["id_nivel"] = $existe["ID"];

                        $value["importe"] = $value["subtotal"] + $value["iva"];

                        $total += $value["importe"];

                        $query_insertar = "INSERT INTO mov_nota_salida_detalle (
                                                                                numero_movimiento, numero_movimiento_entrada, id_nivel, articulo, unidad_medida, cantidad,
                                                                                p_unitario, subtotal, iva, importe, titulo,
                                                                                descripcion, observaciones, estructura
                                                                                )
                                                                                VALUES (
                                                                                ?, ?, ?, ?, ?, ?,
                                                                                ?, ?, ?, ?, ?,
                                                                                ?, ?, COLUMN_CREATE(";

                        for($i = 0; $i < $total_egresos->conteo; $i++) {
                            $query_insertar .= "'".strtolower(str_replace(' ', '_', $nombre[$i]))."', ?, ";
                        }

                        $query_insertar .= "'articulo', ?));";

                        $resultado_insertar_detalle = $this->patrimonio_model->insertar_nota_salida_detalle_archivo($value, $query_insertar);

                        if(!$resultado_insertar_detalle) {
                            throw new Exception('Hubo un error al insertar los datos, por favor, contacte a su administrador.');
                        }

                    } else {
                        throw new Exception('La estructura en la fila '.$numero_fila.' no existe, por favor rev&iacute;sela.');
                    }

                    $numero_fila += 1;
                }

                unset($key);
                unset($value);

                $respuesta_caratula = FALSE;

                foreach($datos_caratula as $key => $value) {

                    $fecha = ($value['B'] - 25569) * 86400;
                    $value["B"] = date('Y-m-d', $fecha);
                    $value["importe_total"] = $total;
                    $value["ultima_nota"] = $last;

                    $resultado_insertar = $this->patrimonio_model->insertar_caratula_nota_salida_archivo($value);

                }

                if ($this->db->trans_status() === FALSE) {
                    throw new Exception('Hubo un error al insertar los datos, por favor, contacte a su administrador.');
                }
                else {
                    $this->db->trans_commit();
                    $mensaje["mensaje"] = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>La nota de salida se insert&oacute; correctamente.</div>';
                }


            } catch (Exception $e) {
                $this->db->trans_rollback();

                $mensaje["mensaje"] = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$e->getMessage().'</div>';

            }

            $datos_header = array(
                "titulo_pagina" => "Armonniza | Resultado Archivo",
                "usuario" => $this->tank_auth->get_username(),
                "precompromisocss" => TRUE,
            );

            $this->parser->parse('front/header_main_view', $datos_header);
            $this->load->view('front/resultado_archivo_view', array(
                "mensaje" => $mensaje["mensaje"],
            ));
            $this->load->view('front/footer_main_view');

        }
    }

    /** Sección Kardex de Existencias */
    function kardex() {
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Kardex de Existencias",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/patrimonio_kardex_view');
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "kardex_index" => TRUE,
        ));
    }

    function tabla_kardex_principal() {

        $resultado = $this->patrimonio_model->get_datos_indice_kardex();
        //$this->debugeo->imprimir_pre($resultado);

        $output = array("data" => "");

        foreach($resultado as $key => $value) {

            $output["data"][] = array(
                '<div class="center">'.$value["movimiento"].'</div>',
                '<div class="center">'.$value["movimiento_salida"].'</div>',
                '<div class="center">'.$value["operacion"].'</div>',
                $value["articulo"],
                $value["descripcion"],
                '<div class="center">'.$value["fecha"].'</div>',
                '<div class="center">'.$value["unidad_medida"].'</div>',
                '<div class="center">'.$value["entra"].'</div>',
                '<div class="center">'.$value["sale"].'</div>',
                '<div class="center">'.$value["existe"].'</div>',
                '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($value["precio_unitario"] == 0 || $value["precio_unitario"] == NULL ? '0.00' : (number_format($value["precio_unitario"], 2)).'</div>'),
            );

        }
//        $this->debugeo->imprimir_pre($output);
        echo json_encode($output);
    }

    /** Sección Consulta Inventarios de Bienes*/
    function inventarios() {
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Inventario de Bienes",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
            "precompromisocss" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/patrimonio_inventario_bienes_view');
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "consulta_inventarios" => TRUE,
        ));
    }

    function imprimir_consulta_inventarios() {

        // Primero se toman todos los datos enviador por el usuario
        $datos = $this->input->post();

        // Se recorren todos los datos del arreglo y se les quita los espacios en blanco
        foreach ($datos as $key => $value) {
            $datos[$key]=trim($value);
        }

        $year = date("Y");

        // En caso de que la fecha inicial venga vacia, se toma el primer dia del año corriente
        if($datos["fecha_inicial"] == NULL || $datos["fecha_inicial"] == "") {
            $datos["fecha_inicial"] = $year."-01-01";
        }

        // En caso de que la fecha final venga vacia, se toma el último dia del año corriente
        if($datos["fecha_final"] == NULL || $datos["fecha_final"] == "") {
            $datos["fecha_final"] = $year."-12-31";
        }

        // Se toman los datos que se van a imprimir
        $datos_impresion = $this->patrimonio_model->get_datos_inventario_impresion($datos);

        // $this->debugeo->imprimir_pre($datos_impresion);

        // Se carga la librería para gener los PDF
        $this->load->library('Pdf');

        // Se inicializa la configuracion básica del documento
        $pdf = new Pdf('L', 'cm', 'A4', true, 'UTF-8', false);

        // Información principal del documento
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Armonniza');
        $pdf->SetTitle('Nota de Salida');
        $pdf->SetSubject('Nota de Salida');
        $pdf->SetKeywords('Armonniza');

// set default header data
        // $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
//        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        // remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
//        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, 10 , PDF_MARGIN_RIGHT);
//        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// ---------------------------------------------------------

// set font
        $pdf->SetFont('helvetica', '', 12);

// add a page
        $pdf->AddPage('L', 'Letter');

        $encabezado = '<table cellspacing="3" style="font-size: small;">
                        <tr>
                            <td align="left"><b>Periodo</b></td>
                            <td align="center"><b>Fecha Emisión</b></td>
                            <td align="right"><b>Hora</b></td>
                        </tr>
                        <tr>
                            <td align="left">'.$datos["fecha_inicial"].' al '.$datos["fecha_final"].'</td>
                            <td align="center">'.date("d-m-Y").'</td>
                            <td align="right">'.date("H:i:s").'</td>
                        </tr>
                       </table>';

        $tabla_general = '<table style="font-size: small;  border: 1px solid #BDBDBD;" cellpadding="4">
                            <tr>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="20%">Clave</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD; line-height: 20px;" width="35%">Descripción</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="10%"><br>U/M</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD" width="15%">Existencia</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD; line-height: 20px;" width="20%">Costo Promedio</td>
                            </tr>';

        foreach ($datos_impresion as $key => $value) {
            $tabla_general .= '<tr>';
            $tabla_general .= '<td align="lef" style=" border-right: 1px solid #BDBDBD;" width="20%">' . $value["articulo"] . '</td>';
            $tabla_general .= '<td align="justify" style=" border-right: 1px solid #BDBDBD;" width="35%">' . $value["descripcion"] . '</td>';
            $tabla_general .= '<td align="center" style=" border-right: 1px solid #BDBDBD;" width="10%">' . $value["unidad_medida"] . '</td>';
            $tabla_general .= '<td align="center" style=" border-right: 1px solid #BDBDBD;"  width="15%">' . $value["existe"] . '</td>';
            $tabla_general .= '<td align="left" width="2%">$</td>';
            $tabla_general .= '<td align="right" style=" border-right: 1px solid #BDBDBD;" width="18%">' . ($value["precio_promedio"] == 0 || $value["precio_promedio"] == NULL ? '0.00' : (number_format($value["precio_promedio"], 2))) . '</td>';
            $tabla_general .= '</tr>';
        }

        $tabla_general .= '</table>';

        $html = '
                <style>
                    .cont-general{
                        border: 1px solid #eee;
                        border-radius: 1%;
                        margin: 2% 14%;
                        width: 102%;
                    }
                    .cont-general2{
                        border: 1px solid #BDBDBD;
                    }
                    .cont-general .borde-inf{
                        border-bottom: 1px solid #eee;
                    }
                    .cont-general .borde-sup{
                        border-top: 1px solid #eee;
                    }
                </style>

                <table class="cont-general" border="0" cellspacing="3" style="font-size: small;" cellpadding="2">
                    <tr cellspacing="10">
                        <th width="2%"></th>
                        <th align="left" width="10%"><img src="'.base_url("img/logo2.png").'" /></th>
                        <th align="center" width="84%" style="line-height: 7px;">
                            <h3>EDUCAL, S.A. DE C.V.</h3>
                            <h5>Consulta de Inventarios</h5>
                            '.$encabezado.'
                        </th>
                        <th width="2%"></th>
                    </tr>
                    <tr><td width="99%" class="borde-sup"></td></tr>
                    <tr>

                        <td width="99%">'.$tabla_general.'</td>
                    </tr>

        </table>';

// output the HTML content

        $pdf->writeHTML($html, false, false, true, false, 'top');

// //Close and output PDF document
        $pdf->Output('Consulta de Inventario.pdf', 'I');
    }

    function exportar_clasificador_productos() {

        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

        $datos = $this->input->post();
//      $this->debugeo->imprimir_pre($datos);
        $fecha = date("Y-m-d");
        $hora = date("H:i:s");

        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Clasificador de Productos');
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('Educal');
        $objDrawing->setPath('img/logo2.png');
        $objDrawing->setOffsetX(50);
        $objDrawing->setHeight(80);
        // $objDrawing->setWidth(10);
        $objDrawing->setCoordinates('A1');
        $objDrawing->setWorksheet($this->excel->getActiveSheet());

        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
            'font'  => array(
                'bold'  => true,
            )
        );
        $style_center = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );
        $style_left = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            )
        );

        $this->excel->getActiveSheet()->mergeCells('A1:G1')->getStyle("A1")->applyFromArray($style);
        $this->excel->getActiveSheet()->setCellValue('A1','EDUCAL, S.A. DE C.V.');
        $this->excel->getActiveSheet()->mergeCells('A2:G2')->getStyle("A2")->applyFromArray($style);
        $this->excel->getActiveSheet()->setCellValue('A2', 'Clasificador de Productos');
        $this->excel->getActiveSheet()->getStyle("B4:G4")->applyFromArray($style);
        $this->excel->getActiveSheet()->getStyle("B5:G5")->applyFromArray($style_center);
        $this->excel->getActiveSheet()->setCellValue('B4', 'Fecha Emisión');
        $this->excel->getActiveSheet()->setCellValue('F4', 'Hora Emisión');
        $this->excel->getActiveSheet()->setCellValue('B5', $fecha);
        $this->excel->getActiveSheet()->setCellValue('F5', $hora);
        $this->excel->getActiveSheet()->getStyle("A7:N7")->applyFromArray($style);
        $this->excel->getActiveSheet()->getStyle("A8:N8")->applyFromArray($style);
        $this->excel->getActiveSheet()->mergeCells('A6:G6');

        //set cell A1 content with some text

        $this->excel->getActiveSheet()->setCellValue('A7', 'Artículo')->getColumnDimension("A")->setWidth(25);
        $this->excel->getActiveSheet()->setCellValue('B7', 'Descripción')->getColumnDimension("B")->setWidth(35);
        $this->excel->getActiveSheet()->setCellValue('C7', 'U/M')->getColumnDimension("C")->setWidth(15);
        $this->excel->getActiveSheet()->setCellValue('D8', 'Existencias')->getColumnDimension("D")->setWidth(15);
        $this->excel->getActiveSheet()->setCellValue('E8', 'Código')->getColumnDimension("E")->setWidth(15);
        $this->excel->getActiveSheet()->setCellValue('F8', 'Clasificación')->getColumnDimension("F")->setWidth(15);
        $this->excel->getActiveSheet()->setCellValue('G8', 'Localización')->getColumnDimension("G")->setWidth(15);
        $this->excel->getActiveSheet()->setCellValue('H8', 'Clave')->getColumnDimension("H")->setWidth(15);
        $this->excel->getActiveSheet()->setCellValue('I8', 'Estante')->getColumnDimension("I")->setWidth(15);
        $this->excel->getActiveSheet()->setCellValue('J8', 'Anaquel')->getColumnDimension("J")->setWidth(15);
        $this->excel->getActiveSheet()->setCellValue('K8', 'Ultimo Costo')->getColumnDimension("K")->setWidth(15);
        $this->excel->getActiveSheet()->setCellValue('L8', 'Fecha de Compra')->getColumnDimension("L")->setWidth(15);
        $this->excel->getActiveSheet()->setCellValue('M8', 'Partida')->getColumnDimension("M")->setWidth(15);
        $this->excel->getActiveSheet()->setCellValue('N8', 'Clave Activo')->getColumnDimension("N")->setWidth(15);
        $this->excel->getActiveSheet()->setCellValue('O8', 'Marca')->getColumnDimension("O")->setWidth(15);
        $this->excel->getActiveSheet()->setCellValue('P8', 'Tipo')->getColumnDimension("P")->setWidth(15);
        $this->excel->getActiveSheet()->setCellValue('Q8', 'Modelo')->getColumnDimension("Q")->setWidth(15);
        $this->excel->getActiveSheet()->setCellValue('R8', 'Serie')->getColumnDimension("R")->setWidth(15);
        $this->excel->getActiveSheet()->setCellValue('S8', 'Tipo de Unidad')->getColumnDimension("S")->setWidth(15);
        $this->excel->getActiveSheet()->setCellValue('T8', 'Máximo Stock')->getColumnDimension("T")->setWidth(15);
        $this->excel->getActiveSheet()->setCellValue('U8', 'Punto de Reorden')->getColumnDimension("U")->setWidth(15);
        $this->excel->getActiveSheet()->setCellValue('V8', 'Mínimo Stock')->getColumnDimension("V")->setWidth(15);
        $this->excel->getActiveSheet()->setCellValue('W8', 'Area Asignado')->getColumnDimension("W")->setWidth(15);
        $this->excel->getActiveSheet()->setCellValue('X8', 'Nombre del Resguardante')->getColumnDimension("X")->setWidth(15);
        $this->excel->getActiveSheet()->setCellValue('Y8', 'Tipo / Clase')->getColumnDimension("Y")->setWidth(15);

        $resultado = $this->patrimonio_model->get_datos_indice_inventario($datos);
        $row = 9;

        foreach($resultado as $fila) {
            $this->excel->getActiveSheet()->getStyle("A:B")->applyFromArray($style_center);
            $this->excel->getActiveSheet()->getStyle("C:D")->applyFromArray($style_center);
            $this->excel->getActiveSheet()->setCellValue('A' . $row, $fila["articulo"]);
            $this->excel->getActiveSheet()->setCellValue('B' . $row, $fila["descripcion"]);
            $this->excel->getActiveSheet()->setCellValue('C' . $row, $fila["unidad"]);
            $this->excel->getActiveSheet()->setCellValue('D' . $row, $fila["existencia"]);
            $this->excel->getActiveSheet()->setCellValue('E' . $row, $fila["codigo"]);
            $this->excel->getActiveSheet()->setCellValue('F' . $row, $fila["clasificacion"]);
            $this->excel->getActiveSheet()->setCellValue('G' . $row, $fila["localizacion"]);
            $this->excel->getActiveSheet()->setCellValue('H' . $row, $fila["clave"]);
            $this->excel->getActiveSheet()->setCellValue('I' . $row, $fila["estante"]);
            $this->excel->getActiveSheet()->setCellValue('J' . $row, $fila["anaquel"]);
            $this->excel->getActiveSheet()->setCellValue('K' . $row, $fila["ultimo_costo"]);
            $this->excel->getActiveSheet()->setCellValue('L' . $row, $fila["fecha_compra"]);
            $this->excel->getActiveSheet()->setCellValue('M' . $row, $fila["partida"]);
            $this->excel->getActiveSheet()->setCellValue('N' . $row, $fila["clave_activo"]);
            $this->excel->getActiveSheet()->setCellValue('O' . $row, $fila["marca"]);
            $this->excel->getActiveSheet()->setCellValue('P' . $row, $fila["tipo"]);
            $this->excel->getActiveSheet()->setCellValue('Q' . $row, $fila["modelo"]);
            $this->excel->getActiveSheet()->setCellValue('R' . $row, $fila["serie"]);
            $this->excel->getActiveSheet()->setCellValue('S' . $row, $fila["tipo_unidad"]);
            $this->excel->getActiveSheet()->setCellValue('T' . $row, $fila["maximo_stock"]);
            $this->excel->getActiveSheet()->setCellValue('U' . $row, $fila["punto_reorden"]);
            $this->excel->getActiveSheet()->setCellValue('V' . $row, $fila["minimo_stock"]);
            $this->excel->getActiveSheet()->setCellValue('W' . $row, $fila["area_asignado"]);
            $this->excel->getActiveSheet()->setCellValue('X' . $row, $fila["nombre_resguardante"]);
            $this->excel->getActiveSheet()->setCellValue('Y' . $row, $fila["tipo_clase"]);

            $row += 1;
        }

//        $this->benchmark->mark('code_end');

//        echo $this->benchmark->elapsed_time('code_start', 'code_end');

        $filename="Clasificador de Productos.xls"; // Agregar fecha en que se generó
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
//        save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
//        if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
//        force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');

    }

    function tabla_concepto_gasto_modal() {
        $resultado = $this->patrimonio_model->get_datos_indice_inventario();

        $output = array("data" => "");

        foreach($resultado as $key => $value) {

            $output["data"][] = array(
                $value["articulo"],
                $value["descripcion"],
                '<a class="seleccion_gasto" data-toggle="modal" data-dismiss="modal" title="Seleccionar Gasto"><i class="fa fa-check"></i></a>',
            );
        }

        echo json_encode($output);
    }

    function exportar_consulta_inventarios() {
        // Primero se toman todos los datos enviador por el usuario
        $datos = $this->input->post();

        // $this->debugeo->imprimir_pre($datos);

        // Se recorren todos los datos del arreglo y se les quita los espacios en blanco
        foreach ($datos as $key => $value) {
            $datos[$key]=trim($value);
        }

        $year = date("Y");

        // En caso de que la fecha inicial venga vacia, se toma el primer dia del año corriente
        if($datos["fecha_inicial"] == NULL || $datos["fecha_inicial"] == "") {
            $datos["fecha_inicial"] = $year."-01-01";
        }

        // En caso de que la fecha final venga vacia, se toma el último dia del año corriente
        if($datos["fecha_final"] == NULL || $datos["fecha_final"] == "") {
            $datos["fecha_final"] = $year."-12-31";
        }

        $ID_gastos = $this->patrimonio_model->get_ID_gastos($datos["gasto_inicial"], $datos["gasto_final"]);

        // $this->debugeo->imprimir_pre($ID_gastos);

        $datos["gasto_inicial"] = $ID_gastos["ID_inicial"];
        $datos["gasto_final"] = $ID_gastos["ID_final"];

        // Se toman los datos que se van a imprimir
        $datos_impresion = $this->patrimonio_model->get_datos_inventario_impresion($datos);

        $this->load->library('excel');
        $fecha = date("Y-m-d");
        $hora = date("H:i:s");

        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Adecuaciones Ingresos');
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('Educal');
        $objDrawing->setPath('img/logo2.png');
        $objDrawing->setOffsetX(50);
        $objDrawing->setHeight(80);
        // $objDrawing->setWidth(10);
        $objDrawing->setCoordinates('A1');
        $objDrawing->setWorksheet($this->excel->getActiveSheet());

        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
            'font'  => array(
                'bold'  => true,
            )
        );
        $style_center = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );
        $style_left = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            )
        );
        $style_color = array(
            'font'  => array(
                'color' => array('rgb' => 'DF0101')
            )
        );

        $this->excel->getActiveSheet()->mergeCells('B1:E1')->getStyle("B1")->applyFromArray($style);
        $this->excel->getActiveSheet()->setCellValue('B1','EDUCAL, S.A. DE C.V.');
        $this->excel->getActiveSheet()->mergeCells('B2:E2')->getStyle("B2")->applyFromArray($style);
        $this->excel->getActiveSheet()->setCellValue('B2', 'Consulta Inventario');
        $this->excel->getActiveSheet()->getStyle("B3:E3")->applyFromArray($style);
        $this->excel->getActiveSheet()->getStyle("B4:E4")->applyFromArray($style_center);
        $this->excel->getActiveSheet()->setCellValue('B3', 'Período');
        $this->excel->getActiveSheet()->setCellValue('C3', 'Fecha Emisión');
        $this->excel->getActiveSheet()->setCellValue('E3', 'Hora Emisión');
        $this->excel->getActiveSheet()->setCellValue('B4', $datos["fecha_inicial"] .' al '. $datos["fecha_final"]);
        $this->excel->getActiveSheet()->setCellValue('C4', $fecha);
        $this->excel->getActiveSheet()->setCellValue('E4', $hora);
        $this->excel->getActiveSheet()->getStyle("A6:E6")->applyFromArray($style);
        $this->excel->getActiveSheet()->mergeCells('A5:E5');

        //set cell A1 content with some text

        $this->excel->getActiveSheet()->setCellValue('A6', 'Clave')->getColumnDimension("A")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('B6', 'Descripción')->getColumnDimension("B")->setWidth(40);
        $this->excel->getActiveSheet()->setCellValue('C6', 'U/M')->getColumnDimension("C")->setWidth(15);
        $this->excel->getActiveSheet()->setCellValue('D6', 'Existencia')->getColumnDimension("D")->setWidth(15);
        $this->excel->getActiveSheet()->setCellValue('E6', 'Costo Promedio')->getColumnDimension("E")->setWidth(15);

        $row = 7;

//        $this->benchmark->mark('code_start');

        foreach($datos_impresion as $key => $value) {

            $this->excel->getActiveSheet()->setCellValue('A'.$row, $value["articulo"]);
            $this->excel->getActiveSheet()->setCellValue('B'.$row, $value["descripcion"]);
            $this->excel->getActiveSheet()->setCellValue('C'.$row, $value["unidad_medida"]);
            $this->excel->getActiveSheet()->setCellValue('D'.$row, $value["existencia"]);
            $this->excel->getActiveSheet()->setCellValue('E'.$row, '$ '.($value["precio_promedio"] == 0 || $value["precio_promedio"] == NULL ? "0.00" : (number_format($value["precio_promedio"], 2))));

            $row += 1;
        }

//        $this->benchmark->mark('code_end');

//        echo $this->benchmark->elapsed_time('code_start', 'code_end');

        $filename="Consulta de Inventarios.xls"; // Agregar fecha en que se generó
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
//        save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
//        if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
//        force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }

    /** === Estructuras === */

    /**
     * Estructuras Egresos
     */
    private function input_niveles_egresos() {
//        Se llama la funcion del modelo de egresos encargado de contar los niveles que existen
        $total_egresos = $this->egresos_model->contar_egresos_elementos();
//        Se llama a la funcion del model de egresos encargada de tomar los nombres delos niveles que existen
        $nombres_egresos = $this->egresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_egresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

        $niveles = '';
        $num = 1;

        for($i = 0; $i < $total_egresos->conteo; $i++){
            $nom_niv = ucwords(strtolower($nombre[$i]));
            $nom_niv_1 = str_replace( "De" , "" , $nom_niv);
            $nom_niv_2 = str_replace( "Fuente" , "F." , $nom_niv_1);
            $nom_niv_f = str_replace( "Programa" , "P." , $nom_niv_2);
            $niveles .= '
                                <!-- <h5>Nivel '.$num.'</h5>
                                <label>'.$nombre[$i].'</label>-->
                                <input type="text" class="form-control" id="input_nivel'.$num.'" placeholder="'.$nom_niv_f.'" >
                        ';
            $num++;
        }

        return $niveles;
    }
    private function preparar_estructura_egresos() {
//        Se llama la funcion del modelo de egresos encargado de contar los niveles que existen
        $total_egresos = $this->egresos_model->contar_egresos_elementos();
//        Se llama a la funcion del model de egresos encargada de tomar los nombres delos niveles que existen
        $nombres_egresos = $this->egresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_egresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

        $nombre_primerNivel = $this->egresos_model->datos_primerNivel($nombre[0]);

//        $this->debugeo->imprimir_pre($nombre_primerNivel);

        $niveles = '<div class="row" id="egresos_estrucura">
                        <div class="col-lg-4">
                            <form role="form">
                                <div class="form-group" id="forma_nivel1">
                                    <h5>Nivel 1</h5>
                                    <label>'.$nombre[0].'</label>
                                    <input type="text" class="form-control" id="buscar_nivel1" placeholder="Buscar...">
                                    <select multiple class="form-control formas">';
        foreach($nombre_primerNivel as $fila) {
            $niveles .= '<option value="'.$fila->fuente_de_financiamiento.'" title="'.$fila->fuente_de_financiamiento.' - '.$fila->descripcion.'">'.$fila->fuente_de_financiamiento.' - '.$fila->descripcion.'</option>';
        }

        $niveles .= '                </select>
                                </div>
                            </form>
                        </div>';

        $a = 1;
        $num = 2;

        for($i = 1; $i < $total_egresos->conteo; $i++){
            $niveles .= '<div class="col-lg-4">
                            <form role="form">
                                <div class="form-group" id="forma_nivel'.$num.'">
                                     <h5>Nivel '.$num.'</h5>
                                    <label>'.$nombre[$a].'</label>
                                    <input type="text" class="form-control" id="buscar_nivel'.$num.'" placeholder="Buscar..." disabled>
                                    <select multiple class="form-control formas" id="select_nivel'.$num.'">
                                    </select>
                                </div>
                            </form>
                        </div>';
            $a++;
            $num++;
        }

        $niveles .= '</div>';

        return $niveles;
    }

    // ------------------------------------------------------------------------ //

    /**
     * Aqui inicia la sección del catalogo de inventarios
     */

    /**
     * Esta función se encarga de mostrar la vista que contiene la tabla donde está la tabla que contiene
     * el detalle del inventario
     **/
    function catalogo_inventarios() {

        // $this->debugeo->imprimir_pre("Catalogo de Inventarios");

        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha ingresado al modulo patrimonio');
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Inventario",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
        );
        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/catalogo_inventario_view');
        $this->load->view('front/footer_main_view', array("tablas" => TRUE,
            "catalogo_inventario" => TRUE,));
    }

    function tabla_partidas() {
        $resultado = $this->patrimonio_model->contenido_tabla_partidas();

        $output = array("data" => "");

        foreach($resultado as $key => $value) {
            $estructura = json_decode($value["estructura"], TRUE);

            $output["data"][] = array(
                $estructura["partida"],
                $estructura["descripcion"],
                '<a class="seleccion_gasto" data-toggle="modal" data-dismiss="modal" title="Seleccionar Gasto"><i class="fa fa-check"></i></a>',
            );
        }

        echo json_encode($output);
    }

    function tabla_unidad_medida() {
        $resultado = $this->patrimonio_model->contenido_tabla_unidad_medida();

        $output = array("data" => "");

        foreach($resultado as $key => $value) {

            $output["data"][] = array(
                $value["clave"],
                $value["descripcion"],
                '<a class="seleccion_gasto" data-toggle="modal" data-dismiss="modal" title="Seleccionar Gasto"><i class="fa fa-check"></i></a>',
            );
        }

        echo json_encode($output);
    }

    function tabla_inventario_principal() {

        $resultado = $this->patrimonio_model->get_datos_indice_inventario();

        $output = array("data" => "");

        foreach($resultado as $key => $value) {
            if ($this->utilerias->get_grupo() == 1){
                $opciones = '<a href="' . base_url("patrimonio/ver_producto_inventario/" . $value["id_conceptos_gasto"]) . '" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                             <a href="' . base_url("patrimonio/editar_producto_inventario/" . $value["id_conceptos_gasto"]) . '" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                             <a data-toggle="modal" data-target=".modal_borrar" title="Borrar Producto" data-tooltip="Eliminar"><i class="fa fa-trash"></i></a>';
            } else {
                if ($this->utilerias->get_permisos("editar_producto") && $this->utilerias->get_permisos("ver_producto") && $this->utilerias->get_permisos("eliminar_producto")){
                    $opciones = '<a href="' . base_url("patrimonio/ver_producto_inventario/" . $value["id_conceptos_gasto"]) . '" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                             <a href="' . base_url("patrimonio/editar_producto_inventario/" . $value["id_conceptos_gasto"]) . '" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                             <a data-toggle="modal" data-target=".modal_borrar" title="Borrar Producto" data-tooltip="Eliminar"><i class="fa fa-trash"></i></a>';
                } elseif ($this->utilerias->get_permisos("editar_producto")){
                    $opciones = '<a href="' . base_url("patrimonio/editar_producto_inventario/" . $value["id_conceptos_gasto"]) . '" data-tooltip="Editar"><i class="fa fa-edit"></i></a>';
                } elseif ($this->utilerias->get_permisos("ver_producto")){
                    $opciones = '<a href="' . base_url("patrimonio/ver_producto_inventario/" . $value["id_conceptos_gasto"]) . '" data-tooltip="Ver"><i class="fa fa-eye"></i></a>';
                } elseif ($this->utilerias->get_permisos("eliminar_producto")){
                    $opciones = '<a data-toggle="modal" data-target=".modal_borrar" title="Borrar Producto" data-tooltip="Eliminar"><i class="fa fa-trash"></i></a>';
                }
            }
            $output["data"][] = array(
                $value["id_conceptos_gasto"],
                $value["articulo"],
                // $value["codigo"],
                $value["descripcion"],
                $value["unidad"],
                '<div class="center">'.$value["existencia"].'</div>',
                $opciones,
            );
        }
//        $this->debugeo->imprimir_pre($output);
        echo json_encode($output);
    }

    /**
     * Esta función se encarga
     * @return [type] [description]
     */
    function agregar_producto_inventario() {

        // Se crea una variable que tiene el mensaje a desplegar de la insercion de un registro nuevo
        $mensaje = "";

        // Se carga el encabezado de la página
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Inventario",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
            "precompromisocss" => TRUE,
        );
        $this->parser->parse('front/header_main_view', $datos_header);

        // Se crean las reglas para validar los campos enviados por el usuario por separado
        $this->form_validation->set_rules('clave', 'Clave', 'required|xss_clean|is_unique[cat_conceptos_gasto.articulo]');
        $this->form_validation->set_rules('descripcion', 'Descripci&oacute;n', 'required|xss_clean');
        $this->form_validation->set_rules('unidad_medida', 'Unidad de Medida', 'required|xss_clean');
        $this->form_validation->set_rules('tipo_clase', 'Tipo / Clase', 'required|xss_clean');
        $this->form_validation->set_rules('codigo', 'C&oacute;digo', 'xss_clean');
        $this->form_validation->set_rules('ultimo_costo', 'Ultimo Costo', 'xss_clean');
        $this->form_validation->set_rules('fecha_compra', 'Fecha de Compra', 'xss_clean');
        $this->form_validation->set_rules('partida', 'Partida', 'required|xss_clean');
        $this->form_validation->set_rules('clave_activo', 'Clave de Activo', 'xss_clean');
        $this->form_validation->set_rules('clasificacion_patriomnial', 'Clasificaci&oacute;n Patrimonial', 'xss_clean');
        $this->form_validation->set_rules('marca', 'Marca', 'required|xss_clean');
        $this->form_validation->set_rules('tipo', 'Tipo', 'xss_clean');
        $this->form_validation->set_rules('modelo', 'Modelo', 'xss_clean');
        $this->form_validation->set_rules('serie', 'No. de Serie', 'required|xss_clean');
        $this->form_validation->set_rules('tipo_unidad', 'Tipo de Unidad', 'xss_clean');
        $this->form_validation->set_rules('existencia', 'Existencia', 'required|xss_clean');
        $this->form_validation->set_rules('maximo_stock', 'M&aacute;ximo en Stock', 'xss_clean');
        $this->form_validation->set_rules('punto_reorden', 'Punto de Reorden', 'xss_clean');
        $this->form_validation->set_rules('minimo_stock', 'M&iacute;nimo en Stock', 'xss_clean');
        $this->form_validation->set_rules('estante', 'Estante', 'xss_clean');
        $this->form_validation->set_rules('anaquel', 'Anaquel', 'xss_clean');
        $this->form_validation->set_rules('localizacion', 'Localizaci&oacute;n', 'xss_clean');
        $this->form_validation->set_rules('area_asignado', 'Area Asignado', 'xss_clean');
        $this->form_validation->set_rules('nombre_resguardante', 'Nombre Resguardante', 'xss_clean');

        // Con esta parte se cambian los delimitadores de los errores que se muestran al usuario
        // se pueden editar para poder mostrar los errores como se deseen
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');

        if ($this->form_validation->run() == FALSE) {
            // En caso de que el formulario no sea valido
            // o que el usuario entre por primera vez a la página,
            // se carga la vista sin variables
            $this->load->view('front/catalogos/catalogo_agregar_producto_view');
        }
        else {
            // Se inicializa la varibale que contiene la ruta de la imagen
            $ruta = "";

            // En caso de que el formulario sea valido, se hace la carga del archivo de la imagen
            // Configuracion para subir la imagen, en caso de querer modificar las validaciones de la imagen, descomentar las ultimas tres lineas
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['remove_spaces'] = TRUE;
            // $config['max_size'] = '100';
            // $config['max_width']  = '1024';
            // $config['max_height']  = '768';

            // Se carga la libreria para subir archivos, con la configuracion previa
            $this->load->library('upload', $config);

            // Se incializa la librería con la configuracion previa
            $this->upload->initialize($config);

            // Se verifica que haya un archivo para subir, si es verdadero, se hace la carga del mismo
            if (is_uploaded_file($_FILES['imagen']['tmp_name'])) {

                // Si existe un problema al cargar la imagen, se le notifica al usuario
                if ( ! $this->upload->do_upload('imagen', FALSE)) {
                    $error = array('error' => $this->upload->display_errors());

                    $conjunto_errores = "";

                    foreach($error as $key => $value) {
                        $conjunto_errores .= '<div class="alert alert-danger">';
                        $conjunto_errores .= $value;
                        $conjunto_errores .= '</div>';
                    }
                    $this->load->view('front/catalogos/catalogo_agregar_producto_view', array(
                        'error_imagen' => $conjunto_errores,
                    ));

                }

                // De lo contrario, se hace la carga del archivo
                else {

                    // Se guarda la informacion del archivo en una variable
                    $data = array('upload_data' => $this->upload->data());

                    // Se hace la carga del archivo
                    $upload_data = $this->upload->data();

                    // Se crea la configuracion del servidor para poder manipular las imagenes
                    // y se le indica que vamos a crear un thumbnail (una muestra de imagen)
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = $upload_data['full_path'];
                    $config['create_thumb'] = TRUE;
                    $config['maintain_ratio'] = TRUE;
                    $config['width']     = 500;
                    $config['height']   = 500;

                    // Se carga la libreria de las imagenes con la configuracion adecuada para poder hacer el thumbnail
                    $this->load->library('image_lib', $config);

                    // Se le indica al servidor que modifique la imagen previamente subida
                    $this->image_lib->resize();

                    // En caso de que exista un error al realizar la operacion, se le indica al usuario
                    if ( ! $this->image_lib->resize()) {
                        $error = array('error' => $this->image_lib->display_errors());

                        $conjunto_errores = "";

                        foreach($error as $key => $value) {
                            $conjunto_errores .= '<div class="alert alert-danger">';
                            $conjunto_errores .= $value;
                            $conjunto_errores .= '</div>';
                        }

                        $this->load->view('front/catalogos/catalogo_agregar_producto_view', array(
                            'error_imagen' => $conjunto_errores,
                        ));
                    }

                    // Se toman los datos de la imagen que son necesarios para poder guardar la ruta de la imagen en la base de datos
                    $nombre_archivo = $upload_data["raw_name"]."_thumb".$upload_data["file_ext"];
                    $ruta = base_url("uploads/".$nombre_archivo);

                }
            }

            // Si los valores pasan todas las validaciones, se guardan los datos en la base de datos
            // Primero se toman todos los datos enviador por el usuario
            $datos = $this->input->post();

            // Se recorren todos los datos del arreglo y se les quita los espacios en blanco
            foreach ($datos as $key => $value) {
                $datos[$key]=trim($value);
            }

            // Se guarda en el log la accion que está realizando el usuario
            log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha insertado el producto con clave'.$datos["clave"]);

            // Se agrega la ruta de la imagen, ya sea si existe o no
            $datos["ruta_imagen"] = $ruta;

            // Se mandan a guardar los datos
            $resultado_insertar = $this->patrimonio_model->insertar_catalogo_nuevo_producto($datos);

            // En caso de que el resultado al insertar sea un exito, se manda un mensaje al usuario de que todo salio bien
            if($resultado_insertar) {
                $mensaje = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-check-circle fa-2x ic-msj"></i> Datos insertados correctamente.</div>';
            }
            // De lo contrario, se manda un mensaje de error
            else {
                $mensaje = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-times-circle fa-2x ic-msj"></i> Hubo un error al tratar de insertar los datos, por favor comuniquese con su administrador.</div>';
            }

            // Se manda el mensaje ya sea de exito o de error al usuario
            $this->load->view('front/catalogos/catalogo_agregar_producto_view', array(
                'mensaje' => $mensaje,
                'ruta' => $ruta,
            ));
        }

        // Se carga el pie de pagina
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "catalogo_inventario_agregar_producto" => TRUE,
        ));

    }

    function editar_producto_inventario($producto = NULL) {

        // Se crea una variable que tiene el mensaje a desplegar de la insercion de un registro nuevo
        $mensaje = "";

        // Se carga el encabezado de la página
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Inventario",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
            "precompromisocss" => TRUE,
        );
        $this->parser->parse('front/header_main_view', $datos_header);

        // Se crean las reglas para validar los campos enviados por el usuario por separado
        $this->form_validation->set_rules('clave', 'Clave', 'required|xss_clean|is_unique[cat_conceptos_gasto.articulo]');
        $this->form_validation->set_rules('descripcion', 'Descripci&oacute;n', 'required|xss_clean');
        $this->form_validation->set_rules('unidad_medida', 'Unidad de Medida', 'required|xss_clean');
        $this->form_validation->set_rules('tipo_clase', 'Tipo / Clase', 'required|xss_clean');
        $this->form_validation->set_rules('codigo', 'C&oacute;digo', 'xss_clean');
        $this->form_validation->set_rules('ultimo_costo', 'Ultimo Costo', 'xss_clean');
        $this->form_validation->set_rules('fecha_compra', 'Fecha de Compra', 'xss_clean');
        $this->form_validation->set_rules('partida', 'Partida', 'required|xss_clean');
        $this->form_validation->set_rules('clave_activo', 'Clave de Activo', 'xss_clean');
        $this->form_validation->set_rules('clasificacion_patriomnial', 'Clasificaci&oacute;n Patrimonial', 'xss_clean');
        $this->form_validation->set_rules('marca', 'Marca', 'required|xss_clean');
        $this->form_validation->set_rules('tipo', 'Tipo', 'xss_clean');
        $this->form_validation->set_rules('modelo', 'Modelo', 'xss_clean');
        $this->form_validation->set_rules('serie', 'No. de Serie', 'required|xss_clean');
        $this->form_validation->set_rules('tipo_unidad', 'Tipo de Unidad', 'xss_clean');
        $this->form_validation->set_rules('existencia', 'Existencia', 'required|xss_clean');
        $this->form_validation->set_rules('maximo_stock', 'M&aacute;ximo en Stock', 'xss_clean');
        $this->form_validation->set_rules('punto_reorden', 'Punto de Reorden', 'xss_clean');
        $this->form_validation->set_rules('minimo_stock', 'M&iacute;nimo en Stock', 'xss_clean');
        $this->form_validation->set_rules('estante', 'Estante', 'xss_clean');
        $this->form_validation->set_rules('anaquel', 'Anaquel', 'xss_clean');
        $this->form_validation->set_rules('localizacion', 'Localizaci&oacute;n', 'xss_clean');
        $this->form_validation->set_rules('area_asignado', 'Area Asignado', 'xss_clean');
        $this->form_validation->set_rules('nombre_resguardante', 'Nombre Resguardante', 'xss_clean');

        // Con esta parte se cambian los delimitadores de los errores que se muestran al usuario
        // se pueden editar para poder mostrar los errores como se deseen
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');

        if ($this->form_validation->run() == FALSE) {
            // En caso de que el formulario no sea valido
            // o que el usuario entre por primera vez a la página,
            // se carga la vista con las variables que le corresponden al producto
            if(isset($producto)) {
                $resultado = $this->patrimonio_model->get_datos_producto($producto);
            } else {
                // Si hay un error dentro de la validación, ya no se toma el dato que ingreso al principio,
                // ahora se toma el id que está enviando el usuarios por modificar
                $datos = $this->input->post();

                // Se recorren todos los datos del arreglo y se les quita los espacios en blanco
                foreach ($datos as $key => $value) {
                    $datos[$key]=trim($value);
                }

                // $this->debugeo->imprimir_pre($datos);

                $resultado = $this->patrimonio_model->get_datos_producto($datos["id_conceptos_gasto"]);
            }

            // $this->debugeo->imprimir_pre($resultado);

            // Se le agrega la ruta de la imagen al arreglo
            $resultado["ruta"] = $resultado["ruta_imagen"];

            $this->load->view('front/catalogos/catalogo_editar_producto_view', $resultado);
        }
        else {
            // Se inicializa la varibale que contiene la ruta de la imagen
            $ruta = "";

            // En caso de que el formulario sea valido, se hace la carga del archivo de la imagen
            // Configuracion para subir la imagen, en caso de querer modificar las validaciones de la imagen, descomentar las ultimas tres lineas
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['remove_spaces'] = TRUE;
            // $config['max_size'] = '100';
            // $config['max_width']  = '1024';
            // $config['max_height']  = '768';

            // Se carga la libreria para subir archivos, con la configuracion previa
            $this->load->library('upload', $config);

            // Se incializa la librería con la configuracion previa
            $this->upload->initialize($config);
            if (is_uploaded_file($_FILES['imagen']['tmp_name'])) {

                // Si existe un problema al cargar la imagen, se le notifica al usuario
                if ( ! $this->upload->do_upload('imagen', FALSE)) {
                    $error = array('error' => $this->upload->display_errors());

                    $conjunto_errores = "";

                    foreach($error as $key => $value) {
                        $conjunto_errores .= '<div class="alert alert-danger">';
                        $conjunto_errores .= $value;
                        $conjunto_errores .= '</div>';
                    }
                    $this->load->view('front/catalogos/catalogo_editar_producto_view', array(
                        'error_imagen' => $conjunto_errores,
                    ));

                }
                // De lo contrario, se hace la carga del archivo
                else {

                    // Se guarda la informacion del archivo en una variable
                    $data = array('upload_data' => $this->upload->data());

                    // Se hace la carga del archivo
                    $upload_data = $this->upload->data();

                    // Se crea la configuracion del servidor para poder manipular las imagenes
                    // y se le indica que vamos a crear un thumbnail (una muestra de imagen)
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = $upload_data['full_path'];
                    $config['create_thumb'] = TRUE;
                    $config['maintain_ratio'] = TRUE;
                    $config['width']     = 500;
                    $config['height']   = 500;

                    // Se carga la libreria de las imagenes con la configuracion adecuada para poder hacer el thumbnail
                    $this->load->library('image_lib', $config);

                    // Se le indica al servidor que modifique la imagen previamente subida
                    $this->image_lib->resize();

                    // En caso de que exista un error al realizar la operacion, se le indica al usuario
                    if ( ! $this->image_lib->resize()) {
                        $error = array('error' => $this->image_lib->display_errors());

                        $conjunto_errores = "";

                        foreach($error as $key => $value) {
                            $conjunto_errores .= '<div class="alert alert-danger">';
                            $conjunto_errores .= $value;
                            $conjunto_errores .= '</div>';
                        }

                        $this->load->view('front/catalogos/catalogo_editar_producto_view', array(
                            'error_imagen' => $conjunto_errores,
                        ));
                    }

                    // Se toman los datos de la imagen que son necesarios para poder guardar la ruta de la imagen en la base de datos
                    $nombre_archivo = $upload_data["raw_name"]."_thumb".$upload_data["file_ext"];
                    $ruta = base_url("uploads/".$nombre_archivo);

                }

            }

            // Si no hay ningún error dentro de las validaciones, se toman los datos ingresados por el usuario
            $datos = $this->input->post();

            // Se recorren todos los datos del arreglo y se les quita los espacios en blanco
            foreach ($datos as $key => $value) {
                $datos[$key]=trim($value);
            }

            // Se guarda en el log la accion que está realizando el usuario
            log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha insertado el producto con clave'.$datos["clave"]);

            $datos["ruta_imagen"] = $ruta;

            // Se mandan a guardar los datos
            $resultado_insertar = $this->patrimonio_model->actualizar_catalogo_producto($datos);

            // En caso de que el resultado al insertar sea un exito, se manda un mensaje al usuario de que todo salio bien
            if($resultado_insertar) {
                $mensaje = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-check-circle fa-2x ic-msj"></i> Datos actualizados correctamente.</div>';
            }
            // De lo contrario, se manda un mensaje de error
            else {
                $mensaje = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-times-circle fa-2x ic-msj"></i> Hubo un error al tratar de actualizar los datos, por favor comuniquese con su administrador.</div>';
            }

            // Se manda el mensaje ya sea de exito o de error al usuario
            $this->load->view('front/catalogos/catalogo_editar_producto_view', array(
                'mensaje' => $mensaje,
                'ruta' => $ruta,
            ));
        }

        // Se carga el pie de pagina
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "catalogo_inventario_editar_producto" => TRUE,
        ));

    }

    /**
     * Esta función sirve para poder ver los datos del producto, sin la opción de edición
     * @param  int $producto Es el ID correspondiente al producto que se desea ver
     */
    function ver_producto_inventario($producto) {

        if(!isset($producto) || $producto == NULL || $producto == "") {

        }

        // Se carga el encabezado de la página
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Inventario",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
            "precompromisocss" => TRUE,
        );
        $this->parser->parse('front/header_main_view', $datos_header);

        // Se carga la vista con las variables que le corresponden al producto
        $resultado = $this->patrimonio_model->get_datos_producto($producto);

        // $this->debugeo->imprimir_pre($resultado);

        // Se le agrega la ruta de la imagen al arreglo
        $resultado["ruta"] = $resultado["ruta_imagen"];

        // Se carga la vista con todos las variables que le corresponden
        $this->load->view('front/catalogos/catalogo_ver_producto_view', $resultado);

        // Se carga el pie de pagina
        $this->load->view('front/footer_main_view');

    }

    function borrar_producto() {

        // Se inicializa la variable que va a contener el mensaje que se devuelve al usuario
        $mensaje = array(
            "mensaje" => "",
        );

        try {

            // Se toman los datos enviados por el usuario
            $producto = $this->input->post("id_producto", TRUE);

            // Se llama a la función que se encarga de borrar el registro de la base de datos
            $resultado_eliminar = $this->patrimonio_model->borrar_producto($producto);

            if($resultado_eliminar) {
                // Si la operación fue exitosa, se manda un mensaje al usuario avisando que todo fue un éxito
                $mensaje["mensaje"] = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>El producto fue eliminado.</div>';
            } else {
                // Si hubo un problema con la operación, se le avisa al usuario que contacte a su administrador
                throw new Exception($mensaje);
            }

        } catch (Exception $e) {
            // Si hay algún error, se manda un mensaje al usuario avisando del error
            $mensaje["mensaje"] = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$e->getMessage().'</div>';
        }

        echo(json_encode($mensaje));
    }

    /** Tablas Generales*/
    function tabla_centro_costos()
    {
        $sql = "SELECT COLUMN_GET(nivel, 'centro_de_recaudacion' as char) AS centro_de_recaudacion, COLUMN_GET(nivel, 'descripcion' as char) AS descripcion FROM cat_niveles WHERE COLUMN_GET(nivel, 'centro_de_recaudacion' as char)  != '' GROUP BY centro_de_recaudacion;";
        $query = $this->db->query($sql);
        $resultados_cuenta = $query->result();
        $output = array('data' => '');
        foreach ($resultados_cuenta as $fila) {
//            $this->debugeo->imprimir_pre($fila);
            $output['data'][] = array(
                $fila->centro_de_recaudacion,
                $fila->descripcion,
                '<a class="seleccion_gasto" data-toggle="modal" data-dismiss="modal" title="Seleccionar Centro de Costos"><i class="fa fa-check"></i></a>'
            );
        }
//        $this->debugeo->imprimir_pre($output);
        echo(json_encode($output));
    }


}