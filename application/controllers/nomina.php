<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require( './assets/datatables/scripts/ssp.class.php' );

/**
 * Este controlador se encarga de mostrar la nómina y controlar las funciones
 **/
class Nomina extends CI_Controller {

    var $sql_details;

    /**
     * Se revisa si el usuario esta logueado, si no esta logueado, se reenvia a la pantalla de login
     */
    function __construct() {
        parent::__construct();
        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        }
        $this->sql_details = array(
            'user' => 'root',
            'pass' => '',
            'db' => 'software',
            'host' => 'localhost',
        );
    }

    /**
     * Esta funcion es la principal, donde se muestra la pantalla principal de la nómina
     */
    function index() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha entrado a nómina');
        $datos_header = array(
            "titulo_pagina" => "Armonniza  | Nómina",
            "usuario" => $this->tank_auth->get_username(),
        );
        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/nomina/main_view');
        $this->load->view('front/footer_main_view', array("graficas" => TRUE));
    }

    /** Aquí empieza la sección de Empleados */
    function empleados() {
        log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ', ha entrado a Recursos Humano, sección Empleados');
        $datos_header = array(
            "titulo_pagina" => "Armonniza  | Empleados",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
            "precompromisocss" => TRUE,
        );
        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/nomina/empleados_view');
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "indice_empleados" => TRUE,
            ));
    }

    function nuevo_empleado() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha entrado a la lista de empleados');
        $datos_header = array(
            "titulo_pagina" => "Armonniza  | Nuevo Empleado",
            "usuario" => $this->tank_auth->get_username(),
        );
        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/nomina/nuevo_empleado_view');
        $this->load->view('front/footer_main_view', array("graficas" => TRUE));

    }

    function consulta_empleados() {
            log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ', ha entrado a la lista de empleados');
            $datos_header = array(
                "titulo_pagina" => "Armonniza  | Lista de empleados",
                "usuario" => $this->tank_auth->get_username(),
            );
            $this->parser->parse('front/header_main_view', $datos_header);
            $this->load->view('front/nomina/lista_empleados_view');
            $this->load->view('front/footer_main_view', array("graficas" => TRUE));
    }

    function exportar_empleados() {
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Personal",
            "usuario" => $this->tank_auth->get_username(),
            "precompromisocss" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/nomina/seleccion_exportar_empleados_view');
        $this->load->view('front/footer_main_view', array(
            "devengado" => TRUE,
        ));
    }

    function exportar_empleados_formato() {
        $datos = $this->input->post();
//        $this->debugeo->imprimir_pre($datos);
        $this->load->library('excel');
        $fecha = date("Y-m-d");
        $hora = date("H:i:s");

        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Listado Personal');
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('Educal');
        $objDrawing->setPath('img/logo2.png');
        $objDrawing->setOffsetX(50);
        $objDrawing->setHeight(80);
        // $objDrawing->setWidth(10);
        $objDrawing->setCoordinates('A1');
        $objDrawing->setWorksheet($this->excel->getActiveSheet());

        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
            'font'  => array(
                'bold'  => true,
            )
        );
        $style_center = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );
        $style_left = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            )
        );

        $this->excel->getActiveSheet()->mergeCells('A1:V1')->getStyle("A1")->applyFromArray($style);
        $this->excel->getActiveSheet()->setCellValue('A1','SERVICIOS DE SALUD DE MICHOACAN');
        $this->excel->getActiveSheet()->mergeCells('A2:V2')->getStyle("A2")->applyFromArray($style);
        $this->excel->getActiveSheet()->setCellValue('A2', 'Personal');
        $this->excel->getActiveSheet()->getStyle("C3:V3")->applyFromArray($style);
        $this->excel->getActiveSheet()->getStyle("C4:V4")->applyFromArray($style_center);

        $this->excel->getActiveSheet()->setCellValue('E3', 'Periodo');
        $this->excel->getActiveSheet()->setCellValue('L3', 'Fecha Emision');
        $this->excel->getActiveSheet()->setCellValue('T3', 'Hora Emision');
        $this->excel->getActiveSheet()->setCellValue('E4', $datos["fecha_inicial"].' al '.$datos["fecha_final"]);
        $this->excel->getActiveSheet()->setCellValue('L4', $fecha);
        $this->excel->getActiveSheet()->setCellValue('T4', $hora);
        $this->excel->getActiveSheet()->mergeCells('A5:V5');
        $this->excel->getActiveSheet()->getStyle("A6:V6")->applyFromArray($style);

        //set cell A1 content with some text
        $this->excel->getActiveSheet()->setCellValue('A6', 'No.')->getColumnDimension("A")->setWidth(9);
        $this->excel->getActiveSheet()->setCellValue('B6', 'Clave')->getColumnDimension("B")->setWidth(10);
        $this->excel->getActiveSheet()->setCellValue('C6', 'A. Paterno')->getColumnDimension("C")->setWidth(15);
        $this->excel->getActiveSheet()->setCellValue('D6', 'A. Materno')->getColumnDimension("D")->setWidth(15);
        $this->excel->getActiveSheet()->setCellValue('E6', 'Nombre(s)')->getColumnDimension("E")->setWidth(15);
        $this->excel->getActiveSheet()->setCellValue('F6', 'RFC')->getColumnDimension("F")->setWidth(15);
        $this->excel->getActiveSheet()->setCellValue('G6', 'Curp')->getColumnDimension("G")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('H6', 'Sexo')->getColumnDimension("H")->setWidth(10);
        $this->excel->getActiveSheet()->setCellValue('I6', 'Fecha Ingreso Gob.')->getColumnDimension("I")->setWidth(15);
        $this->excel->getActiveSheet()->setCellValue('J6', 'Crespo')->getColumnDimension("J")->setWidth(10);
        $this->excel->getActiveSheet()->setCellValue('K6', 'Descripcion')->getColumnDimension("K")->setWidth(25);
        $this->excel->getActiveSheet()->setCellValue('L6', 'Clave Clues')->getColumnDimension("L")->setWidth(15);
        $this->excel->getActiveSheet()->setCellValue('M6', 'Prog_subp')->getColumnDimension("M")->setWidth(10);

        $this->excel->getActiveSheet()->setCellValue('N6', 'Unidad')->getColumnDimension("N")->setWidth(10);
        $this->excel->getActiveSheet()->setCellValue('O6', 'Ptda')->getColumnDimension("O")->setWidth(10);
        $this->excel->getActiveSheet()->setCellValue('P6', 'Puesto')->getColumnDimension("P")->setWidth(10);
        $this->excel->getActiveSheet()->setCellValue('Q6', 'Proy')->getColumnDimension("Q")->setWidth(10);
        $this->excel->getActiveSheet()->setCellValue('R6', 'No. Plaza')->getColumnDimension("R")->setWidth(10);
        $this->excel->getActiveSheet()->setCellValue('S6', 'Descripcion Puesto')->getColumnDimension("S")->setWidth(25);
        $this->excel->getActiveSheet()->setCellValue('T6', 'Rama')->getColumnDimension("T")->setWidth(25);
        $this->excel->getActiveSheet()->setCellValue('U6', 'Fuente Financiamiento')->getColumnDimension("U")->setWidth(25);
        $this->excel->getActiveSheet()->setCellValue('V6', 'Tipo Contrato')->getColumnDimension("V")->setWidth(25);

        $resultado = $this->nomina_model->get_datos_filtros_personal($datos);
        //$this->debugeo->imprimir_pre($resultado);
        $row = 7;

        foreach($resultado as $fila) {
            $this->excel->getActiveSheet()->getStyle("A:V")->applyFromArray($style_left);
            $this->excel->getActiveSheet()->setCellValue('A'.$row, $fila->id_usuarios_nomina);
            $this->excel->getActiveSheet()->setCellValue('B'.$row, $fila->clave);
            $this->excel->getActiveSheet()->setCellValue('C'.$row, $fila->a_paterno);
            $this->excel->getActiveSheet()->setCellValue('D'.$row, $fila->a_materno);
            $this->excel->getActiveSheet()->setCellValue('E'.$row, $fila->nombre);
            $this->excel->getActiveSheet()->setCellValue('F'.$row, $fila->rfc);
            $this->excel->getActiveSheet()->setCellValue('G'.$row, $fila->curp);
            $this->excel->getActiveSheet()->setCellValue('H'.$row, $fila->sexo);
            $this->excel->getActiveSheet()->setCellValue('I'.$row, $fila->fech_ing_gob);
            $this->excel->getActiveSheet()->setCellValue('J'.$row, $fila->crespo);
            $this->excel->getActiveSheet()->setCellValue('K'.$row, $fila->descripcion);
            $this->excel->getActiveSheet()->setCellValue('L'.$row,  $fila->clave_clues);
            $this->excel->getActiveSheet()->setCellValue('M'.$row,  $fila->prog_subp);

            $this->excel->getActiveSheet()->setCellValue('N'.$row, $fila->unidad);
            $this->excel->getActiveSheet()->setCellValue('O'.$row, $fila->ptda);
            $this->excel->getActiveSheet()->setCellValue('P'.$row, $fila->puesto);
            $this->excel->getActiveSheet()->setCellValue('Q'.$row, $fila->proy);
            $this->excel->getActiveSheet()->setCellValue('R'.$row, $fila->num_plaza);
            $this->excel->getActiveSheet()->setCellValue('S'.$row, $fila->descripcion_puesto);
            $this->excel->getActiveSheet()->setCellValue('T'.$row, $fila->rama);
            $this->excel->getActiveSheet()->setCellValue('U'.$row,  $fila->fuente_financiamiento);
            $this->excel->getActiveSheet()->setCellValue('V'.$row,  $fila->tipo_contrato);

            $row += 1;
        }
//        $this->benchmark->mark('code_start');
//        $this->benchmark->mark('code_end');

//        echo $this->benchmark->elapsed_time('code_start', 'code_end');

        $filename="Personal.xls"; // Agregar fecha en que se generó
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
//        save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
//        if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
//        force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');


    }

    /** Aquí empieza la sección de Nómina */

    function nominas() {
        log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ',  ha entrado a Recursos Humanos, sección Nómina');
        $datos_header = array(
            "titulo_pagina" => "Armonniza  | N&oacute;mina",
            "usuario" => $this->tank_auth->get_username(),
        );
        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/nomina/nomina_view');
        $this->load->view('front/footer_main_view', array("graficas" => TRUE));
    }


    function mostrarDatos ($resultados)
    {
        if ($resultados != NULL) {

            echo "- Nombre: " . $resultados['nombre'] . "<br/> ";
            echo "- Apellidos: " . $resultados['apellidos'] . "<br/>";
            echo "- Dirección: " . $resultados['direccion'] . "<br/>";
            echo "- Teléfono: " . $resultados['telefono'] . "<br/>";
            echo "- Edad: " . $resultados['edad'] . "<br/>";




        } else {
            echo "<br/>No hay más datos!!! <br/>";
        }
    }


    function consulta_nomina() {
            log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha entrado a la lista de empleados');
            $datos_header = array(
                "titulo_pagina" => "Armonniza  | Lista de N&oacute;minas",
                "usuario" => $this->tank_auth->get_username(),
            );
            $this->parser->parse('front/header_main_view', $datos_header);
            $this->load->view('front/nomina/lista_nomina_view');
            $this->load->view('front/footer_main_view', array("graficas" => TRUE));

    }

    function nueva_nomina() {
            log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha entrado a la lista de empleados');
            $datos_header = array(
                "titulo_pagina" => "Armonniza  | Nueva N&oacute;mina",
                "usuario" => $this->tank_auth->get_username(),
            );
            $this->parser->parse('front/header_main_view', $datos_header);
            $this->load->view('front/nomina/nueva_nomina_view');
            $this->load->view('front/footer_main_view', array("graficas" => TRUE));

    }

    function tabla_empleados_indice() {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

        $resultado = $this->nomina_model->get_datos_indice_empleados();

        $output = array();

        foreach($resultado as $key => $value) {
            // $this->debugeo->imprimir_pre($value);
            $output["data"][] = array(

                $value["id_usuarios_nomina"],
                $value["rfc"],
                $value["a_paterno"],
                $value["a_materno"],
                $value["nombre"],
                $value["curp"],
                $value["sexo"],
                $value["fech_ing_gob"],
                $value["crespo"],
//                $value["descripcion"],
//                $value["clave_clues"],
                $value["prog_subp"],
                $value["unidad"],
                $value["ptda"],
                $value["puesto"],
                $value["proy"],
                $value["num_plaza"],
                $value["descripcion_puesto"],
                $value["rama"],
                $value["fuente_financiamiento"],
                $value["tipo_contrato"],
                $value ["juris"],
                $value["municipio"],
                $value["tplaza"],
                $value["nacionalidad"],
                $value["dias_lab"],
                $value["email"],
                $value["escolaridad"],
                $value["jornada"],
                $value["tu"],
                $value["clues_desc_real"],
                $value["clues_desc_nomina"],
                $value["entidad_federativa_plaza"],
                $value["descripcion_clues2"],
                $value["descripcion_crespo2"],
                $value["actividad_principal"],
                $value["area_servicio_trab2"],
                $value ["Inst_pert_plaza"],
                $value["cedula_pro"],


            );
        }
//        $this->debugeo->imprimir_pre($output);
        echo json_encode($output);
    }
}


