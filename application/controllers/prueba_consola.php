<?php

class Prueba_consola extends CI_Controller {

    public function mensaje($to = 'Mundo') {
        echo "Hola {$to}!".PHP_EOL;
    }

    function prueba_log() {
        $some_var = '';

        if ($some_var == "") {
            log_message('error', 'Some variable did not contain a value.');
        }
        else {
            log_message('debug', 'Some variable was correctly set');
        }

        log_message('info', 'The purpose of some variable is to provide some value.');
    }

}