<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Recaudacion extends CI_Controller
{
    /**
     * Se revisa si el usuario esta logueado, si no esta logueado, se reenvia a la pantalla de login
     */
    function __construct()
    {
        parent::__construct();
        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        }
    }

    /**
     * Esta funcion es la principal, donde se muestra la pantalla de Canales de Venta
     */
    function index()
    {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha ingresado al modulo recaudación');
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Recaudación",
            "usuario" => $this->tank_auth->get_username(),
        );
        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/main_view');
        $this->load->view('front/footer_main_view', array("graficas" => TRUE));
    }

    /** Sección de Devengado */
    function devengado() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha ingrsado e la sección del devengado');
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Devengado",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
            "precompromisocss" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/indices_recaudacion_devengado_view');
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "devengado" => TRUE
        ));
    }

    function tabla_indice_devengado() {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

        $resultado = $this->recaudacion_model->get_datos_devengado_centro();

//        $this->debugeo->imprimir_pre($resultado);

        $output = array("data" => "");
        foreach($resultado as $fila) {
            $estructura = json_decode($fila->estructura);
            $firme = '';
            $estatus = '';
            $opciones = '';

            if($this->utilerias->get_permisos("ver_devengado") || $this->utilerias->get_grupo() == 1) {
                $opciones .= '<a href="'.base_url("recaudacion/ver_devengado/".$fila->numero).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>';
            }


            if($fila->enfirme == 0) {
                $firme = '<button type="button" class="btn btn-circle btn-firmec" disabled><i class="fa fa-times"></i></button>';
            }
            elseif($fila->enfirme == 1){
                $firme = '<button type="button" class="btn btn-circle btn-firmea" disabled><i class="fa fa-check"></i></button>';
            }

            if($fila->estatus == "Activo") {
                $estatus = "success";
                if($this->tank_auth->get_username() == $fila->creado_por || $this->utilerias->get_grupo() == 1) {
                    $opciones .= ' <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>
                                 <a href="'.base_url("recaudacion/imprimir_devengado/".$fila->numero).'" data-tooltip="Imprimir" style="margin-right: 0%;"><i class="fa fa-print"></i></a>';
                } else {
                    $opciones .= '<a href="'.base_url("recaudacion/imprimir_devengado/".$fila->numero).'" data-tooltip="Imprimir"><i class="fa fa-print"></i></a>';
                }
            } elseif($fila->estatus == "Pendiente") {
                $estatus = "warning";
                if($this->tank_auth->get_username() == $fila->creado_por || $this->utilerias->get_grupo() == 1) {
                    if($this->utilerias->get_permisos("editar_devengado")) {
                        $opciones .= '<a href="'.base_url("recaudacion/editar_devengado/".$fila->numero).'" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                                     <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>';
                    } else {
                        $opciones .= '<a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>';
                    }
                } else {
                    $opciones .= '<a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>';
                }
            } else {
                $estatus = "danger";
                $opciones .= '<a href="'.base_url("recaudacion/imprimir_devengado/".$fila->numero).'" data-tooltip="Imprimir"><i class="fa fa-print"></i></a>';
            }

            if($this->utilerias->get_grupo() == 1) {
                $opciones = '<a href="'.base_url("recaudacion/ver_devengado/".$fila->numero).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                             <a href="'.base_url("recaudacion/editar_devengado/".$fila->numero).'" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                             <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>
                             <a href="'.base_url("recaudacion/imprimir_devengado/".$fila->numero).'" data-tooltip="Imprimir"><i class="fa fa-print"></i></a>';
            }

            if($fila->poliza == 0 && $fila->estatus == "Activo") {
                $opciones .= '<a data-toggle="modal" data-target=".modal_poliza" data-tooltip="Póliza" style="margin-left: 6%;"><i class="fa fa-leanpub"></i></a>';
            }


            $output["data"][] = array(
                $fila->id_devengado,
                $fila->numero,
                $fila->clasificacion,
                $fila->no_movimiento,
                $estructura->centro_de_recaudacion,
                $fila->fecha_solicitud,
                //$fila->fecha_aplicacion,
                '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                $fila->creado_por,
                $firme,
                '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                $opciones,
            );
        }
//        $this->debugeo->imprimir_pre($output);
        echo json_encode($output);
    }
    function tabla_detalle_devengado() {
//        Se llama la funcion del modelo de Ingresos encargado de contar los niveles que existen
        $total_ingresos = $this->recaudacion_model->contar_ingresos_elementos();
//        Se llama a la funcion del model de Ingresos encargada de tomar los nombres delos niveles que existen
        $nombres_ingresos= $this->recaudacion_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_ingresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }
        $query = "SELECT id_devengado_detalle, ";

        for($i = 0; $i < $total_ingresos->conteo; $i++){
            $query .= "COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[$i]))."' as char) AS ".strtolower(str_replace(' ', '_', $nombre[$i])).", ";
        }
        //Se quito del jquery subtotal_grabado, subtotal_no_grabado, descuento_gravado,  descuento_no_gravado
        $query .= "fecha_aplicacion, tipo_pago, cantidad, importe, descuento, iva, total_importe";
        //Se quito cuenta
        $query .= " FROM mov_devengado_detalle ";
        $query .= " WHERE numero_devengado = ?;";

        $devengado = $this->input->post("devengado", TRUE);
//        $devengado = 36;
//        $this->debugeo->imprimir_pre($devengado);

        $resultado = $this->recaudacion_model->get_datos_devengadoDetalle($devengado, $query);
    //    $this->debugeo->imprimir_pre($resultado);

        $output = array();

        foreach($resultado as $fila) {
            $output[] = array(
                $fila->id_devengado_detalle,
                $fila->gerencia,
                $fila->centro_de_recaudacion,
                $fila->rubro,
                $fila->tipo,
                $fila->clase,
                //$fila->subsidio,
                $fila->fecha_aplicacion,
                $fila->tipo_pago,
                $fila->cantidad,
                $fila->importe,
                $fila->descuento,
                //$fila->subtotal_no_grabado,
                //$fila->descuento_no_gravado,
                //$fila->subtotal_grabado,
                //$fila->descuento_gravado,
                $fila->iva,
                $fila->total_importe,
                //$fila->cuenta,
                //$fila->descripcion_detalle,
                '<a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Eliminar"><i class="fa fa-remove"></i></a>',
            );
        }
//        $this->debugeo->imprimir_pre($output);
        echo json_encode($output);
    }
    function tabla_subsidio() {
        $resultados_subsidio = $this->recaudacion_model->datos_subsidio();
//        $this->debugeo->imprimir_pre($resultado);
        $output = array('data' => '');
        foreach($resultados_subsidio as $fila) {
            $output['data'][] = array(
                $fila->codigo,
                $fila->descripcion,
                '<a class="seleccion_gasto" data-toggle="modal" data-dismiss="modal" title="Seleccionar Subsidio"><i class="fa fa-check"></i></a>'
            );
        }
//        $this->debugeo->imprimir_pre($output);
        echo(json_encode($output));
    }
    function tabla_librerias() {
        $resultados_librerias = $this->recaudacion_model->datos_librerias();
//        $this->debugeo->imprimir_pre($resultado);
        $output = array('data' => '');
        foreach($resultados_librerias as $fila) {
            $output['data'][] = array(
                $fila->clave_centro_costo,
                $fila->clave_libreria,
                '<a class="seleccion_gasto" data-toggle="modal" data-dismiss="modal" title="Seleccionar Subsidio"><i class="fa fa-check"></i></a>'
            );
        }
//        $this->debugeo->imprimir_pre($output);
        echo(json_encode($output));
    }
    function tabla_centro_recaudacion() {
        $sql = "SELECT COLUMN_GET(nivel, 'centro_de_recaudacion' as char) AS centro_de_recaudacion,
	                   COLUMN_GET(nivel, 'descripcion' as char) AS descripcion
                FROM cat_niveles WHERE COLUMN_GET(nivel, 'centro_de_recaudacion' as char)  != ''
                GROUP BY  centro_de_recaudacion;";
        $query = $this->db->query($sql);
        $resultados_cuenta = $query->result();
        $output = array('data' => '');
        foreach($resultados_cuenta as $fila) {
//            $this->debugeo->imprimir_pre($fila);
            $output['data'][] = array(
                $fila->centro_de_recaudacion,
                $fila->descripcion,
                '<a class="seleccion_gasto" data-toggle="modal" data-dismiss="modal" title="Seleccionar Centro de Costo"><i class="fa fa-check"></i></a>'
            );
        }
//        $this->debugeo->imprimir_pre($output);
        echo(json_encode($output));
    }
    function tabla_cliente() {
        $sql = "SELECT clave_cliente, cliente FROM mov_devengado_caratula GROUP BY cliente;";
        $query = $this->db->query($sql);
        $resultado = $query->result();
        $output = array('data' => '');
        foreach($resultado as $fila) {;
            $output['data'][] = array(
                $fila->clave_cliente,
                $fila->cliente,
                '<a class="seleccion_gasto" data-toggle="modal" data-dismiss="modal" title="Seleccionar Cliente"><i class="fa fa-check"></i></a>'
            );
        }
//        $this->debugeo->imprimir_pre($output);
        echo(json_encode($output));
    }

    function exportar_devengado_recaudacion_filtro() {
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Exportar Devengado",
            "usuario" => $this->tank_auth->get_username(),
            "precompromisocss" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/exportar_devengado_filtro');
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "devengado" => TRUE
        ));
    }

    function exportar_devengado_recaudacion() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha exportado un devengado en recaudación');

        $datos = $this->input->post();

        foreach ($datos as $key => $value) {
            $datos[$key]=trim($value);
        }

        $this->load->library('excel');

        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Devengado para Recaudar');

        $this->excel->getActiveSheet()->setCellValue('A1', 'Numero de Devengado');
        $this->excel->getActiveSheet()->setCellValue('B1', 'Fecha');
        $this->excel->getActiveSheet()->setCellValue('C1', 'Centro de Recaudación');
        $this->excel->getActiveSheet()->setCellValue('D1', 'Factura');
        $this->excel->getActiveSheet()->setCellValue('E1', 'Importe');
        $this->excel->getActiveSheet()->setCellValue('F1', 'IVA');
        $this->excel->getActiveSheet()->setCellValue('G1', 'Total Importe');
        $this->excel->getActiveSheet()->setCellValue('H1', 'Recaudado');
        $this->excel->getActiveSheet()->setCellValue('I1', 'Factura Egresos');
        $this->excel->getActiveSheet()->setCellValue('J1', 'Total Factura Egresos');

        $datos_detalle = $this->recaudacion_model->get_datos_devengado_recaudado_exportar($datos["fecha_inicial"], $datos["fecha_final"]);
        $estructura = array();

        $row = 2;

        foreach($datos_detalle as $fila) {
//            $this->debugeo->imprimir_pre($fila);

            $this->excel->getActiveSheet()->setCellValue('A'.$row, $fila["numero"]);
            $this->excel->getActiveSheet()->setCellValue('B'.$row, $fila["fecha_solicitud"]);
            $this->excel->getActiveSheet()->setCellValue('C'.$row, $fila["centro_de_recaudacion"]);
            $this->excel->getActiveSheet()->setCellValue('D'.$row, $fila["no_movimiento"]);
            $this->excel->getActiveSheet()->setCellValue('E'.$row, $fila["importe"]);
            $this->excel->getActiveSheet()->setCellValue('F'.$row, $fila["iva"]);
            $this->excel->getActiveSheet()->setCellValue('G'.$row, $fila["total_importe"]);
            $this->excel->getActiveSheet()->setCellValue('I'.$row, $fila["factura_egreso"]);
            $this->excel->getActiveSheet()->setCellValue('J'.$row, $fila["total_egreso"]);

            $row += 1;
        }

        $filename="Movimientos Devengado.xls"; // Agregar fecha en que se generó
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
//        save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
//        if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
//        force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }

    function exportar_devengado_por_recaudar() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha exportado un devengado por recaudar');

        $this->load->library('excel');

        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Devengado por Recaudar');

        $this->excel->getActiveSheet()->setCellValue('A1', 'Numero de Devengado');
        $this->excel->getActiveSheet()->setCellValue('B1', 'Fecha');
        $this->excel->getActiveSheet()->setCellValue('C1', 'Factura');
        $this->excel->getActiveSheet()->setCellValue('D1', 'Importe');

        $datos_detalle = $this->recaudacion_model->get_devengado_por_recaudar();
        $estructura = array();

        $row = 2;

        foreach($datos_detalle as $fila) {
//            $this->debugeo->imprimir_pre($fila);

            $this->excel->getActiveSheet()->setCellValue('A'.$row, $fila["numero"]);
            $this->excel->getActiveSheet()->setCellValue('B'.$row, $fila["fecha_solicitud"]);
            $this->excel->getActiveSheet()->setCellValue('C'.$row, $fila["no_movimiento"]);
            $this->excel->getActiveSheet()->setCellValue('D'.$row, $fila["importe_total"]);

            $row += 1;
        }

        $filename="Movimientos Devengado por Recaudar.xls"; // Agregar fecha en que se generó
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
//        save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
//        if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
//        force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }

    function agregar_devengado() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha agregado un devengado');
        $last = 0;
//      Se toma el numero del ultimo devengado
        $ultimo = $this->recaudacion_model->ultimo_devengado();
        if($ultimo) {
            $last = $ultimo->ultimo + 1;
        }
        else {
            $last = 1;
        }
        $this->recaudacion_model->apartarDevengado($last);

        $datos_header = array(
            "titulo_pagina" => "Armonniza | Agregar Devengado",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
            "recaudacioncss" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/agregar_devengado_view', array(
            "ultimo" => $last,
            "niveles" => $this->input_niveles_ingresos(),
            "niveles_modal" => $this->preparar_estructura_ingresos(),
            "graficas" => TRUE,
        ));

        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "efectos" => TRUE,
            "graficas" => TRUE,
            "agregar_devengado" => TRUE
        ));
    }
    /**
     * Esta función se encarga de insertar los datos en la carátula de devengado
     * Primero revisa que el devengado esté en firme
     * Solo guarda el devengado
     */
    function insertar_devengado() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha insertado un devengado');
//        Se inicializa la variable que contiene la respuesta de la inserción de datos
        $respuesta = array();

//        Se toman los datos enviados
        $datos = $this->input->post();

        foreach ($datos as $key => $value) {
            $datos[$key]=trim($value);
        }

        try {
            if (!$datos) {
                throw new Exception('Faltan datos.');
            }
//            Si el devengado está firmado
            if($datos["check_firme"] == 1) {
//                Se pasa el estatus del devengado a "Activo"
                $datos["estatus"] = "Activo";

                $this->recaudacion_model->marcarDevengado($datos);
            }
//            De lo contrario, se guarda el compromiso en espera de ser autorizado
            else {
                $datos["estatus"] = "Pendiente";
            }

//            Se insertan los datos de la caratula del compromiso
            $resultado_insertar = $this->recaudacion_model->insertar_caratula_devengado($datos);

//            Si el resultado es exitoso, se le indica al usuario
            if($resultado_insertar) {
                $respuesta = array(
                    "mensaje" => '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-check-circle fa-2x ic-msj"></i> Datos insertados correctamente.</div>',
                );

            }
//        De lo contrario, se manda un error al usuario
            else {
                if (!$datos) {
                    throw new Exception('Hubo un error al insertar los datos.');
                }
            }

            echo(json_encode($respuesta));

        } catch (Exception $e) {
            $respuesta = array(
                "mensaje" => '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-times-circle fa-2x ic-msj"></i>'.$e->getMessage().'</div>',
            );

            echo(json_encode($respuesta));
        }
    }
    /**
     * Esta función se encarga de insertar los datos en la tabla detalle de devengado
     */
    function insertar_detalle_devengado() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha insertado detalle en el devengado');
//        Se llama la funcion del modelo de Ingresos encargado de contar los niveles que existen
        $total_ingresos = $this->recaudacion_model->contar_ingresos_elementos();
//        Se llama a la funcion del model de Ingresos encargada de tomar los nombres delos niveles que existen
        $nombres_ingresos = $this->recaudacion_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_ingresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }
//        Se genera el query para revisar que la estructura exista
        $query = "SELECT id_niveles FROM cat_niveles WHERE COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[0]))."' as char) = ? ";

        for($i = 1; $i < $total_ingresos->conteo; $i++){
            $query .= "AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[$i]))."' as char) = ? ";
        }
//        Se guardan los datos enviados en un arreglo
        $datos = $this->input->post();
//        $this->debugeo->imprimir_pre($datos);

        foreach ($datos as $key => $value) {
            $datos[$key]=trim($value);
        }

//        Se inicializa la variable con la respuesta del servidor
        $respuesta = array(
            "estatus" => '',
            "mensaje" => '',
        );

//        Primero hay que revisar si existe la estructura
        $existe = $this->recaudacion_model->existeEstructura($datos, $query);

        if($existe->id_niveles) {

//            Se toma el id del nivel de la partida
            $datos["id_nivel"] = $existe->id_niveles;

//            Si el usuario ha ingresado que el importe tiene iva, se multiplica el subtotal por el iva de 16% y se guarda en uan variable llamada "importe"
            //$datos["iva"] = round(($datos["importe_grabado"] - $datos["descuento_gravado"]) * 0.16, 2);
            //$datos["total_importe"] = round( ($datos["importe_grabado"] - $datos["descuento_gravado"]) + $datos["iva"] + ($datos["importe_no_grabado"] - $datos["descuento_no_gravado"]), 2);
            $datos["total_importe"] = round( ($datos["importe"] - $datos["descuento"]) + $datos["iva"], 2);

//             $this->debugeo->imprimir_pre($datos);
            //Agregar campos en INSERT INTO descuento_gravado, descuento_no_gravado, subtotal_grabado, subtotal_no_grabado,


            $query_insertar = "INSERT INTO mov_devengado_detalle (numero_devengado, id_nivel, fecha_aplicacion, subsidio, cantidad, importe, descuento, iva, total_importe, descripcion_detalle, fecha_sql, hora_aplicacion, tipo_pago, nivel ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, COLUMN_CREATE(";
//Se quito campo Cuenta
            for ($i = 0; $i < $total_ingresos->conteo; $i++) {
                $query_insertar .= "'" . strtolower(str_replace(' ', '_', $nombre[$i])) . "', ?, ";
            }

            $query_insertar .= "'dato', ?));";

            $resultado = $this->recaudacion_model->insertar_detalle_devengado($datos, $query_insertar);

            if ($resultado) {
                $respuesta["estatus"] = "ok";
                $respuesta["mensaje"] = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-check-circle fa-2x ic-msj"></i> El detalle del devengado se insertó correctamente.</div>';
            } else {
                $respuesta["estatus"] = "error_datos";
                $respuesta["mensaje"] = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-times-circle fa-2x ic-msj"></i> Hubo un error al insertar los datos.</div>';
            }
        }else{
            $respuesta["mensaje"] = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-times-circle fa-2x ic-msj"></i> La estructura no es correcta.</div>';
        }

        echo(json_encode($respuesta));
    }
    /**Estas funciones se encargan del borrar los detalles del devengado*/
    function borrar_detalle_devengado() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha borrado un detalle del devengado');
        $mensaje = array(
            "mensaje" => "",
        );
        $devengado = $this->input->post("id_deven");

        $resultado = $this->recaudacion_model->borrarDetalleDevengado($devengado);

        if($resultado) {
            $mensaje["mensaje"] = "ok";
            echo(json_encode($mensaje));
        }
        else{
            $mensaje["mensaje"] = "error";
            echo(json_encode($mensaje));
        }
    }
    private function privado_borrar_detalle_devengado($devengado) {

        $resultado = $this->recaudacion_model->cancelarDevengado($devengado);

        if($resultado) {
            return TRUE;
        }
        else{
            return FALSE;
        }
    }
    /**Esta función permite visulizar el devengado*/
    function ver_devengado($devengado = NULL) {

        $query = "SELECT * FROM mov_devengado_caratula WHERE numero = ?";
        $resultado = $this->recaudacion_model->get_datos_devengado_caratula($devengado, $query);
        $resultado_detalle = $this->recaudacion_model->get_datos_devengado_detalle($devengado);
//        $this->debugeo->imprimir_pre($resultado_detalle);

        $this->db->select('*')->from('facturas_adjuntas')->where('no_mov', $resultado->ticket);
        $query = $this->db->get();
        $resultado_egresos = $query->result_array();
//        $this->debugeo->imprimir_pre($resultado_egresos);

        $datos_header = array(
            "titulo_pagina" => " Armonniza| Ver Devengado",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
        );

        $resultado->ultimo = $devengado;

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/ver_devengado_view', $resultado);
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "ver_devengado" => TRUE,
        ));
    }
    /**Esta función permite editar el devengado*/
    function editar_devengado($devengado = NULL) {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha editado una partida del devengado');

        $query = "SELECT * FROM mov_devengado_caratula WHERE numero = ?";

        $resultado = $this->recaudacion_model->get_datos_devengado_caratula($devengado, $query);
//        $this->debugeo->imprimir_pre($resultado);

//        Se llama la funcion del modelo de Ingresos encargado de contar los niveles que existen
        $total_ingresos = $this->ingresos_model->contar_ingresos_elementos();
//        Se llama a la funcion del model de Ingresos encargada de tomar los nombres delos niveles que existen
        $nombres_ingresos = $this->ingresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_ingresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

        $niveles = '';
        $num = 1;

        for($i = 0; $i < $total_ingresos->conteo; $i++) {
            $nom_niv = ucwords(strtolower($nombre[$i]));
            $niveles .= '
                                <!-- <h5>Nivel '.$num.'</h5>
                                <label>'.$nombre[$i].'</label>-->
                                <input type="text" class="form-control" id="input_nivel'.$num.'" placeholder="'.$nom_niv.'" >
                        ';
            $num++;
        }

        $datos_header = array(
            "titulo_pagina" => "Armonniza | Editar Devengado",
            "usuario" => $this->tank_auth->get_username(),
            "recaudacioncss" => TRUE,
            "tablas" => TRUE,
        );

        $resultado->ultimo = $devengado;
        $resultado->niveles = $niveles;
        $resultado->niveles_modal = $this->preparar_estructura_ingresos();

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/editar_devengado_view', $resultado);
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "editar_devengado" => TRUE,
        ));

    }
    /**Esta función permiten cancelar un devengado*/
    function cancelar_devengado_caratula() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha cancelado una partida del devengado');
        $mensaje = array(
            "mensaje" => "",
        );

//      Se toma el devengado a cancelar
        $devengado = $this->input->post("devengado");

//      Se hace el query para tomar los datos de caratula del devengado
        $query_caratula = "SELECT * FROM mov_devengado_caratula WHERE numero = ?";

//      Se llama a la función para tomar los datos de la caratula del devengado
        $datos_caratula = $this->recaudacion_model->get_datos_devengado_caratula($devengado, $query_caratula);

//      Se actualiza el estatus del devengado a cancelado

        $query = "UPDATE mov_devengado_caratula SET estatus = 'cancelado', cancelada = 1, enfirme = 0, fecha_cancelada = ? WHERE numero = ?; ";
        $fin = $this->recaudacion_model->cancelarDevengadoCaratula($devengado, $query);

        if($fin) {
            $mensaje["mensaje"] = "ok";
            echo(json_encode($mensaje));
        }
        else {
            $mensaje["mensaje"] = "error";
            echo(json_encode($mensaje));
        }

    }
    /**Esta función permite generar formato impresión Devengado Individual*/
    function imprimir_devengado($devengado = NULL) {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha impreso una partida del devengado');
        $datos = $this->input->post();
        $fecha = date("Y-m-d");
       // $this->debugeo->imprimir_pre($devengado);

        $query = "SELECT * FROM mov_devengado_caratula WHERE id_devengado = ?;";
        $datos = $this->recaudacion_model->get_datos_devengado_caratula($devengado, $query);
        $datos_detalle = $this->recaudacion_model->get_datos_devengado_detalle($devengado);
        //$this->debugeo->imprimir_pre($datos_detalle);

        $this->load->library('Pdf');

        $pdf = new Pdf('L', 'cm', 'A4', true, 'UTF-8', false);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Armonniza');
        $pdf->SetTitle('Comprobante Devengado');
        $pdf->SetSubject('Comprobante Devengado');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
//        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
//        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        // remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
//        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
        $pdf->SetMargins(10, 10 , 10);
//        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// ---------------------------------------------------------

// set font
        $pdf->SetFont('helvetica', '', 12);

// add a page
        $pdf->AddPage();

        $encabezado = '<table cellspacing="3" style="font-size: small;">
                        <tr>
                            <td align="left"><b>&nbsp; &nbsp; &nbsp;Fecha Emisión</b></td>
                            <td align="center"><b></b></td>
                            <td align="right"><b>No. Devengado</b></td>
                        </tr>
                        <tr>
                            <td align="left">&nbsp; &nbsp; &nbsp; '.$fecha.' </td>
                            <td align="center"></td>
                            <td align="right">'.$datos->numero.'</td>
                        </tr>
                       </table>';
        $tabla_titulo = '<table cellspacing="3">
                        <tr>
                            <th width="1%"></th>
                            <th align="left" width="12%" style="margin-top: 200px;"><img src="'.base_url("img/logo2.png").'" /></th>
                            <th align="center" width="86%" style="line-height: 7px;">
                               <h3>EDUCAL, S.A. DE C.V.</h3>
                               <h5>Comprobante Devengado</h5>
                               '.$encabezado.'
                            </th>
                            <th width="1%"></th>
                        </tr>
                       </table>';


        $tabla_general = '';
        $total = 0;
        foreach($datos_detalle as $fila) {
            $estructura = json_decode($fila->estructura);
            $tabla_general .= '<tr>
                           <td align="left" style=" border-right: 1px solid #BDBDBD;" width="22%">'.$estructura->gerencia.' &nbsp;'.$estructura->centro_de_recaudacion.' &nbsp; '.$estructura->rubro.' &nbsp; '.$estructura->tipo.' &nbsp; '.$estructura->clase.'</td>
                           <td align="left" style=" border-right: 1px solid #BDBDBD;" width="15%">'.$fila->subsidio.'</td>
                           <td align="center" style=" border-right: 1px solid #BDBDBD;" width="15%">'.$fila->fecha_aplicacion.'</td>
                           <td align="justify" style=" border-right: 1px solid #BDBDBD;" width="29%">'.$fila->descripcion_detalle.'</td>
                           <td align="left" width="3%">$</td>
                           <td align="right" width="15%">'.$this->cart->format_number(round($fila->total_importe, 2)).'</td>
                        </tr>';
            $total += $fila->total_importe;
        }
        $tabla_datos = '<table style="font-size: small;" cellspacing="3">
                            <tr>
                                <td align="left" width="20%"><b>Clasificación</b></td>
                                <td align="left" width="40%">'. $datos->clasificacion .'</td>
                                <td align="left" width="20%"><b>Fecha de Solicitud</b></td>
                                <td align="left" width="20%">'. $datos->fecha_solicitud .'</td>
                            </tr>
                            <tr>
                                <td align="left" width="20%"><b>Clave</b></td>
                                <td align="left" width="40%">'. $datos->clave_cliente .'</td>
                                <td align="left" width="20%"><b>No. Factura</b></td>
                                <td align="left" width="20%">'. $datos->no_movimiento .'</td>
                            </tr>
                             <tr>
                                <td align="left" width="20%"><b>Centro de Recaudación</b></td>
                                <td align="left" width="40%">'. $datos->cliente .'</td>
                                <td align="left" width="20%"><b>Importe Total</b></td>
                                <td align="left" width="20%">$ '. $this->cart->format_number($total) .'</td>
                            </tr>
                        </table>';
        $total = '<table style="font-size: small;" cellpadding="4">
                        <tr>
                           <td align="left" width="62%" style="border-right: 1px solid #BDBDBD;"><b>Total</b></td>
                           <td align="left" width="6%">$</td>
                           <td align="right" width="31%">'.$this->cart->format_number($total).'</td>
                        </tr>
                   </table>';


        $html = '
        <style>
            .cont-general{
                border: 1px solid #eee;
                border-radius: 1%;
                margin: 2% 14%;
            }
            .cont-general2{
                border: 1px solid #BDBDBD;
            }
            .cont-general .borde-inf{
                border-bottom: 1px solid #eee;
            }
            .cont-general .borde-sup{
                border-top: 1px solid #eee;
            }
        </style>

        <table class="cont-general" border="0" cellspacing="3" style="font-size: small;" cellpadding="2">

            <tr><td class="borde-inf"  width="100%">'.$tabla_titulo.'</td></tr>
            <tr>
                <td width="100%">'.$tabla_datos.'</td>
            </tr>
            <tr><td width="100%" class="borde-sup"></td></tr>
            <tr>
                <td width="100%">
                    <table style="font-size: small;  border: 1px solid #BDBDBD;" cellpadding="4">
                        <tr>
                             <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="22%">Estructura Administrativa</td>
                             <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="15%">Subsidio</td>
                             <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="15%">Fecha Aplicada</td>
                              <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="29%">Descripción</td>
                             <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="18%">Importe</td>
                        </tr>
                        '.$tabla_general.'
                       </table>
                </td>
            </tr>
            <tr>
                <td width="52%"></td>
                <td width="46%" class="cont-general2">'.$total.'</td>
                <td width="2%"></td>
            </tr>
            <tr><td width="100%"></td></tr>
</table>';

// output the HTML content

        $pdf->writeHTML($html, false, false, true, false, 'top');

//Close and output PDF document
        $pdf->Output('Comprobante Devengado.pdf', 'I');
    }
    /**Esta función permite generar formato impresión Devengados en General */
    function imprimir_devengados() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha impreso los devengados');
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Devengado",
            "usuario" => $this->tank_auth->get_username(),
            "precompromisocss" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/seleccion_imprimir_devengado_view');
        $this->load->view('front/footer_main_view', array(
            "devengado" => TRUE,
            "tablas" => TRUE,
        ));
    }
    function imprimir_devengado_formato() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha impreso el formato del devengado');
        $datos = $this->input->post();
        $fecha = date("Y-m-d");
        $hora = date("H:i:s");
//      $this->debugeo->imprimir_pre($datos);

        $datos_detalle = $this->recaudacion_model->get_datos_filtros_devengado($datos);
//        $this->debugeo->imprimir_pre($datos_detalle);

        $this->load->library('Pdf');

            $pdf = new Pdf('P', 'cm', 'Letter', true, 'UTF-8', false);
            // set document information
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAuthor('Armonniza');
            $pdf->SetTitle('Movimientos Devengado');
            $pdf->SetSubject('Movimientos Devengado');
            $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
//        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
//        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            // remove default header/footer
            $pdf->setPrintHeader(false);
            $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
//        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
            $pdf->SetMargins(10, 10, 10);
//        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
            $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// ---------------------------------------------------------

// set font
            $pdf->SetFont('helvetica', '', 12);

// add a page
            $pdf->AddPage('L', 'Letter');

//        Aqui se inicianiza la tabla y se crea la fila superior con los títulos de la misma
            $tabla_datos = '<table style="font-size: small;  border: 1px solid #BDBDBD;" cellpadding="4">
                            <tr>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="6%">No.</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="12%">Clasificación</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="17%">Centro de Recaudación</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="11%">No. Factura</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="15%">Estructura Administrativa</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="9%">Fecha Solicitud</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="16%">Descripción</td>
                                <td align="center" style="font-weight: bold; border-bottom: 1px solid #BDBDBD;" width="14%">Importe</td>
                            </tr>';

            $importe = 0;

//        Este bucle lo que hace es recorrer cada una de las filas de la tabla de movimientos bancarios donde exista un registro de la cuenta seleccionada
            foreach($datos_detalle as $fila) {
                $estructura = json_decode($fila->estructura, TRUE);

//            Ya que estamos dentro del ciclo, recorremos cada elemento del arreglo y ponemos las variables en las celdas que les corresponde
                $tabla_datos .= '<tr>
                                    <td align="center" style="border-right: 1px solid #BDBDBD;" width="6%">'.$fila->numero.'</td>
                                    <td align="justify" style="border-right: 1px solid #BDBDBD;" width="12%">'.$fila->clasificacion.'</td>
                                    <td align="justify" style="border-right: 1px solid #BDBDBD;" width="17%">'.$fila->cliente.'</td>
                                    <td align="center" style="border-right: 1px solid #BDBDBD;" width="11%">'.$fila->no_movimiento.'</td>
                                    <td align="center" style="border-right: 1px solid #BDBDBD;" width="15%">'.$estructura["gerencia"].'&nbsp;&nbsp;'.$estructura["centro_de_recaudacion"].'&nbsp;&nbsp;'.$estructura["rubro"].'&nbsp;&nbsp;'.$estructura["tipo"].'&nbsp;&nbsp;'.$estructura["clase"].'</td>
                                    <td align="center" style="border-right: 1px solid #BDBDBD;" width="9%">'.$fila->fecha_aplicacion_detalle.'</td>
                                    <td align="justify" style="border-right: 1px solid #BDBDBD;" width="16%">'.$fila->descripcion_detalle.'</td>
                                    <td align="left" width="2%">$</td>
                                    <td align="right" width="12%">'.$this->cart->format_number($fila->total_importe).'</td>
                                </tr>';

//            Se suma el importe total a la variable que guarda el importe total
                $importe += $fila->total_importe;
            }

            $tabla_datos .= '</table>';

            $encabezado = '<table cellspacing="3" style="font-size: small;">
                        <tr>
                            <td align="left"><b>&nbsp; &nbsp; &nbsp;Período</b></td>
                            <td align="center"><b>Importe Total</b></td>
                            <td align="right"><b>Fecha Emisión</b></td>
                        </tr>
                        <tr>
                            <td align="left">&nbsp; &nbsp; &nbsp;'.$datos["fecha_inicial"].' al '.$datos["fecha_final"].'</td>
                            <td align="center">$ '.($importe == 0 || $importe == NULL ? '0.00' : (number_format($importe, 2))) .'</td>
                            <td align="right">'.$fecha.'</td>
                        </tr>
                       </table>';
        $tabla_titulo = '<table cellspacing="3" >
                            <tr>
                                <th align="left" width="8%"><img src="'.base_url("img/logo2.png").'" /></th>
                                <th align="center" width="91%" style="line-height: 7px;">
                                    <h3>EDUCAL, S.A. DE C.V.</h3>
                                    <h5>Movimientos Devengado</h5>
                                    '.$encabezado.'
                                </th>
                            </tr>
                        </table>';



            $html = '
        <style>
            .cont-general{
		        border: 1px solid #eee;
		        border-radius: 1%;
		        margin: 2% 14%;
	        }
	        .cont-general2{
	            border: 1px solid #BDBDBD;
	        }
	        .cont-general .borde-inf{
                border-bottom: 1px solid #eee;
	        }
	        .cont-general .borde-sup{
                border-top: 1px solid #eee;
	        }
	        .cont-general .borde-der{
                border-right: 1px solid #eee;
	        }
	        .cont-general2 .borde-inf2{
                border-bottom: 1px solid #BDBDBD;
	        }
	        .cont-general2 .borde-der2{
                border-right: 1px solid #BDBDBD;
	        }

        </style>

        <table class="cont-general" border="0" cellspacing="3" style="font-size: small;">
           <tr><td class="borde-inf" width="100%">'.$tabla_titulo.'</td></tr>
           <tr><td width="100%"></td></tr>
           <tr><td width="100%">'.$tabla_datos.'</td></tr>
           <tr><td width="100%"></td></tr>
        </table>';

// output the HTML content

            $pdf->writeHTML($html, false, false, true, false, 'top');

//Close and output PDF document
            $pdf->Output('Movimientos Devengado.pdf', 'I');


    }
    /**Esta función permite exportar Devengados en General*/
    function exportar_devengado() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha exportado un devengado');
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Devengado",
            "usuario" => $this->tank_auth->get_username(),
            "precompromisocss" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/seleccion_exportar_devengado_view');
        $this->load->view('front/footer_main_view', array(
            "devengado" => TRUE,
            "tablas" => TRUE,
        ));
    }
    function exportar_devengado_formato() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha exportado un formato del devengado');
        $datos = $this->input->post();
        $this->load->library('excel');
        $fecha = date("Y-m-d");
        $hora = date("H:i:s");

        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Movimientos Devengado');
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('Educal');
        $objDrawing->setPath('img/logo2.png');
        $objDrawing->setOffsetX(50);
        $objDrawing->setHeight(80);
        // $objDrawing->setWidth(10);
        $objDrawing->setCoordinates('A1');
        $objDrawing->setWorksheet($this->excel->getActiveSheet());

        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
            'font'  => array(
                'bold'  => true,
            )
        );
        $style_center = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );
        $style_left = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            )
        );

        //set cell A1 content with some text
        $this->excel->getActiveSheet()->getStyle("A")->applyFromArray($style_center);
        $this->excel->getActiveSheet()->setCellValue('A6', 'No.')->getColumnDimension("A")->setWidth(10);
        $this->excel->getActiveSheet()->getStyle("B:I")->applyFromArray($style_left);
        $this->excel->getActiveSheet()->setCellValue('B6', 'Clasificación')->getColumnDimension("B")->setWidth(25);
        $this->excel->getActiveSheet()->setCellValue('C6', 'Centro de Recaudación')->getColumnDimension("C")->setWidth(35);
        $this->excel->getActiveSheet()->setCellValue('D6', 'No. Factura')->getColumnDimension("D")->setWidth(10);
        $this->excel->getActiveSheet()->setCellValue('E6', 'Gerencia')->getColumnDimension("E")->setWidth(8);
        $this->excel->getActiveSheet()->setCellValue('F6', 'Centro de Recaudación')->getColumnDimension("F")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('G6', 'Rubro')->getColumnDimension("G")->setWidth(8);
        $this->excel->getActiveSheet()->setCellValue('H6', 'Tipo')->getColumnDimension("H")->setWidth(8);
        $this->excel->getActiveSheet()->setCellValue('I6', 'Clase')->getColumnDimension("I")->setWidth(8);
        $this->excel->getActiveSheet()->getStyle("J")->applyFromArray($style_center);
        $this->excel->getActiveSheet()->setCellValue('J6', 'Fecha Solicitud')->getColumnDimension("J")->setWidth(15);
        $this->excel->getActiveSheet()->getStyle("K:L")->applyFromArray($style_left);
        $this->excel->getActiveSheet()->setCellValue('K6', 'Descripción')->getColumnDimension("K")->setWidth(35);
        $this->excel->getActiveSheet()->setCellValue('L6', 'Importe')->getColumnDimension("L")->setWidth(25);


        $datos_detalle = $this->recaudacion_model->get_datos_filtros_devengado($datos);
        $estructura = array();
        //$this->debugeo->imprimir_pre($resultado);
        $row = 7;
        $importe = 0;

//        $this->benchmark->mark('code_start');

        foreach($datos_detalle as $fila) {
            $estructura = json_decode($fila->estructura);

            $this->excel->getActiveSheet()->setCellValue('A'.$row, $fila->numero);
            $this->excel->getActiveSheet()->setCellValue('B'.$row, $fila->clasificacion);
            $this->excel->getActiveSheet()->setCellValue('C'.$row, $fila->cliente);
            $this->excel->getActiveSheet()->setCellValue('D'.$row, $fila->no_movimiento);
            $this->excel->getActiveSheet()->setCellValue('E'.$row, $estructura->gerencia);
            $this->excel->getActiveSheet()->setCellValue('F'.$row, $estructura->centro_de_recaudacion);
            $this->excel->getActiveSheet()->setCellValue('G'.$row, $estructura->rubro);
            $this->excel->getActiveSheet()->setCellValue('H'.$row, $estructura->tipo);
            $this->excel->getActiveSheet()->setCellValue('I'.$row, $estructura->clase);
            $this->excel->getActiveSheet()->setCellValue('J'.$row, $fila->fecha_aplicacion_detalle);
            $this->excel->getActiveSheet()->setCellValue('K'.$row, $fila->descripcion_detalle);
            $this->excel->getActiveSheet()->setCellValue('L'.$row, '$ '.$this->cart->format_number($fila->total_importe));

            $importe += $fila->total_importe;
            $row += 1;
        }

        $this->excel->getActiveSheet()->mergeCells('A1:L1')->getStyle("A1")->applyFromArray($style);
        $this->excel->getActiveSheet()->setCellValue('A1','EDUCAL, S.A. DE C.V.');
        $this->excel->getActiveSheet()->mergeCells('A2:L2')->getStyle("A2")->applyFromArray($style);
        $this->excel->getActiveSheet()->setCellValue('A2', 'Movimientos Devengado');
        $this->excel->getActiveSheet()->getStyle("C3:K3")->applyFromArray($style);
        $this->excel->getActiveSheet()->getStyle("C4:K4")->applyFromArray($style_center);
        $this->excel->getActiveSheet()->mergeCells('F3:H3');
        $this->excel->getActiveSheet()->mergeCells('F4:H4');
        $this->excel->getActiveSheet()->setCellValue('C3', 'Período');
        $this->excel->getActiveSheet()->setCellValue('F3', 'Importe Total');
        $this->excel->getActiveSheet()->setCellValue('K3', 'Fecha Emisión');
        $this->excel->getActiveSheet()->setCellValue('C4', $datos["fecha_inicial"] .' al '. $datos["fecha_final"]);
        $this->excel->getActiveSheet()->setCellValue('F4', '$ '.$this->cart->format_number($importe));
        $this->excel->getActiveSheet()->setCellValue('K4', $fecha);
        $this->excel->getActiveSheet()->getStyle("A6:L6")->applyFromArray($style);
        $this->excel->getActiveSheet()->mergeCells('A5:L5');

//        $this->benchmark->mark('code_end');

//        echo $this->benchmark->elapsed_time('code_start', 'code_end');

        $filename="Movimientos Devengado.xls"; // Agregar fecha en que se generó
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
//        save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
//        if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
//        force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }

    function generar_poliza_devengado() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha generado una poliza de devengado');
        $respuesta = array(
            "mensaje" => "",
        );

        try {
            $id = $this->tank_auth->get_user_id();

            $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
            $resultado_query_usuario = $this->db->query($query_usuario, array($id));
            $nombre_encontrado = $resultado_query_usuario->row();
            $nombre_completo = $nombre_encontrado->nombre." ".$nombre_encontrado->apellido_paterno." ".$nombre_encontrado->apellido_materno;

//        Se toma el numero del contrarecibo que se va a buscar
            $devengado = $this->input->post("devengado");
//            $devengado = 1;

//        Se inicizaliza la variable con el ultimo valor de las polizas
            $last = 0;
//        Se toma el numero de la ultima poliza
            $ultimo = $this->ciclo_model->ultima_poliza();

//        Se le suma uno al ultimo valor de las polizas
            if($ultimo) {
                $last = $ultimo->ultimo + 1;
            }
//        De lo contrario se inicia en 1
            else {
                $last = 1;
            }

//        Se prepara el query para llamar todos los datos de la caratula del contrarecibo
            $query_caratula = "SELECT * FROM mov_devengado_caratula WHERE numero = ?;";

//        Se llama a la función que se encarga de tomar todos los datos de la caratula del contrarecibo
            $datos_caratula_devengado = $this->recaudacion_model->get_datos_devengado_caratula($devengado, $query_caratula);

//            $this->debugeo->imprimir_pre($datos_caratula_devengado);

//        Se prepara el query para tomar los datos del detalle de compromiso que están ligados al contrarecibo
            $query_detalle = "SELECT *, COLUMN_JSON(nivel) AS estructura FROM mov_devengado_detalle WHERE numero_devengado = ?;";

//        Se llama ala funcion que se encarga de tomar todos los datos del detalle del compromiso que está ligado con el contrarecibo
            $datos_detalle = $this->recaudacion_model->get_datos_devengadoDetalle($datos_caratula_devengado->numero, $query_detalle);

//            $this->debugeo->imprimir_pre($datos_detalle);

//        Se crea un arreglo donde se van a guardar los resultados del detalle del compromiso
            $datos_detalle_poliza = array();

            if (!$datos_detalle) {
                throw new Exception('No hay partidas dentro del compromiso.');
            }

            $centro_costo = '';
            $partida = '';
            $nombre_fuente_financiamiento = '';
            $total_filas = 0;
            $cargos = 0;
            $abonos = 0;
            $resultado_insertar_caratula_query = FALSE;
            $resultado_insertar_abono_query = FALSE;
            $resultado_insertar_cargo_query = FALSE;

            $this->db->trans_begin();

            $query_insertar_caratula_poliza = "INSERT INTO mov_polizas_cabecera (numero_poliza, tipo_poliza, fecha, fecha_real, concepto, enfirme, sifirme, no_partidas, importe, cancelada, estatus, creado_por, autorizada, cargos, abonos, no_devengado, clave_cliente, cliente, no_movimiento) VALUES (?, ?, NOW(), NOW(), ?, 0, 0, ?, ?, 0, ?, ?, 0, ?, ?, ?, ?, ?, ?);";
            $datos_caratula_poliza = array(
                $last,
                'Diario',
                $datos_caratula_devengado->descripcion,
                0, //$total_filas
                $datos_caratula_devengado->importe_total,
                'espera',
                $nombre_completo,
                0, //$cargos
                0, //$abonos
                $datos_caratula_devengado->numero,
                $datos_caratula_devengado->clave_cliente,
                $datos_caratula_devengado->cliente,
                $datos_caratula_devengado->no_movimiento,
            );

            $resultado_insertar_caratula_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_caratula_poliza, $datos_caratula_poliza);

            $datos_actualizar_poliza = array(
                'poliza' => 1,
            );

            $this->db->where('id_devengado', $devengado);
            $this->db->update('mov_devengado_caratula', $datos_actualizar_poliza);

            foreach ($datos_detalle as $row) {
                $estructura = json_decode($row->estructura);

//                $this->debugeo->imprimir_pre($row);
//                $this->debugeo->imprimir_pre($estructura);

                $datos_cuentas = $this->recaudacion_model->tomar_cuentas_devengado($estructura->centro_de_recaudacion);

//                $this->debugeo->imprimir_pre($datos_cuentas);

                if (!$datos_cuentas) {
                    throw new Exception('No existe cuenta contable asociada a la partida.');
                }

//                $this->debugeo->imprimir_pre($datos_cuentas);

                foreach($datos_cuentas as $cuenta){
                    $check = substr($estructura->clase, -1, 1);

//                    $this->debugeo->imprimir_pre($row);
                    if($check == 2)
                    {
                        if($cuenta->cuenta_cargo) {

                            if (strpos(strtolower($cuenta->nombre_cargo),"servicio") !== FALSE ) {
//                            $this->debugeo->imprimir_pre($cuenta);
                                $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_cargo = $this->db->query($sql_cargo, array($cuenta->cuenta_cargo));
                                $resultado_cuenta_cargo = $query_cargo->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_cargo);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_cargo.' '.$cuenta->nombre_cargo);
                                }

                                $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_cargo = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_cargo,
                                    $resultado_cuenta_cargo->id_padre,
                                    $resultado_cuenta_cargo->nivel,
                                    $datos_caratula_devengado->descripcion,
                                    $row->descuento,
                                    0.0,
                                    "Recursos Fiscales",
                                    $cuenta->nombre_cargo,
                                    $estructura->gerencia,
                                    $estructura->centro_de_recaudacion,
                                );

                                $resultado_insertar_cargo_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                $cargos += $row->descuento;

                                $total_filas += 1;
                            } elseif (strpos(strtolower($cuenta->nombre_cargo), "presupuesto") !== FALSE) {
                                $_cantidad = ($row->importe - $row->descuento) + $row->iva;
                                $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_cargo = $this->db->query($sql_cargo, array($cuenta->cuenta_cargo));
                                $resultado_cuenta_cargo = $query_cargo->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_cargo);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_cargo.' '.$cuenta->nombre_cargo);
                                }

                                $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida, concepto_especifico) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_cargo = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_cargo,
                                    $resultado_cuenta_cargo->id_padre,
                                    $resultado_cuenta_cargo->nivel,
                                    $datos_caratula_devengado->descripcion,
                                    $_cantidad,
                                    0.0,
                                    "Recursos Fiscale",
                                    $cuenta->nombre_cargo,
                                    $estructura->gerencia,
                                    $estructura->centro_de_recaudacion,
                                    0,
                                );

                                $resultado_insertar_cargo_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                $cargos += $_cantidad;

                                $total_filas += 1;
                            } elseif (strpos(strtolower($cuenta->nombre_cargo), "i.v.a.") !== FALSE || strpos(strtolower($cuenta->nombre_cargo), "i.v.a") !== FALSE || strpos(strtolower($cuenta->nombre_abono), "iva") !== FALSE) {
                                if ($row->iva !== 0) {
                                    $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                    $query_cargo = $this->db->query($sql_cargo, array($cuenta->cuenta_cargo));
                                    $resultado_cuenta_cargo = $query_cargo->row();

                                    $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_cargo);

                                    if ($existe_cuenta == FALSE) {
                                        throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_cargo.' '.$cuenta->nombre_cargo);
                                    }

                                    $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida, concepto_especifico) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                    $datos_cargo = array(
                                        $last,
                                        'Diario',
                                        $cuenta->id_correlacion_partidas_contables,
                                        $cuenta->cuenta_cargo,
                                        $resultado_cuenta_cargo->id_padre,
                                        $resultado_cuenta_cargo->nivel,
                                        $datos_caratula_devengado->descripcion,
                                        $row->iva,
                                        0.0,
                                        "Recursos Fiscale",
                                        $cuenta->nombre_cargo,
                                        $estructura->gerencia,
                                        $estructura->centro_de_recaudacion,
                                        0,
                                    );

                                    $resultado_insertar_cargo_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                    $cargos += $row->iva;

                                    $total_filas += 1;
                                }
                            } elseif (strpos(strtolower($cuenta->nombre_cargo), "general") !== FALSE) {
//                                continue;
                            }
                            else {
                                $_cantidad = ($row->importe - $row->descuento) + $row->iva;

                                $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_cargo = $this->db->query($sql_cargo, array($cuenta->cuenta_cargo));
                                $resultado_cuenta_cargo = $query_cargo->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_cargo);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_cargo.' '.$cuenta->nombre_cargo);
                                }

                                $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_cargo = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_cargo,
                                    $resultado_cuenta_cargo->id_padre,
                                    $resultado_cuenta_cargo->nivel,
                                    $datos_caratula_devengado->descripcion,
                                    $_cantidad,
                                    0.0,
                                    "Recursos Fiscales",
                                    $cuenta->nombre_cargo,
                                    $estructura->gerencia,
                                    $estructura->centro_de_recaudacion,
                                );

                                $resultado_insertar_cargo_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                $cargos += $_cantidad;

                                $total_filas += 1;
                            }

                        }

                        if($cuenta->cuenta_abono) {

                            if (strpos(strtolower($cuenta->nombre_abono),"servicio") !== FALSE ) {
//                            $this->debugeo->imprimir_pre($cuenta);
                                $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_abono = $this->db->query($sql_abono, array($cuenta->cuenta_abono));
                                $resultado_cuenta_abono = $query_abono->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_abono);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_abono.' '.$cuenta->nombre_abono);
                                }

                                $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_abono = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_abono,
                                    $resultado_cuenta_abono->id_padre,
                                    $resultado_cuenta_abono->nivel,
                                    $datos_caratula_devengado->descripcion,
                                    0.0,
                                    $row->importe,
                                    "Recursos Fiscales",
                                    $cuenta->nombre_abono,
                                    $estructura->gerencia,
                                    $estructura->centro_de_recaudacion,
                                );

                                $resultado_insertar_abono_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                $abonos += $row->importe;

                                $total_filas += 1;

                            } elseif (strpos(strtolower($cuenta->nombre_abono), "i.v.a.") !== FALSE || strpos(strtolower($cuenta->nombre_abono), "i.v.a") !== FALSE || strpos(strtolower($cuenta->nombre_abono), "iva") !== FALSE) {
                                if ($row->iva !== 0) {
                                    $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                    $query_abono = $this->db->query($sql_abono, array($cuenta->cuenta_abono));
                                    $resultado_cuenta_abono = $query_abono->row();

                                    $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_abono);

                                    if ($existe_cuenta == FALSE) {
                                        throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_abono.' '.$cuenta->nombre_abono);
                                    }

                                    $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida, concepto_especifico) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                    $datos_abono = array(
                                        $last,
                                        'Diario',
                                        $cuenta->id_correlacion_partidas_contables,
                                        $cuenta->cuenta_abono,
                                        $resultado_cuenta_abono->id_padre,
                                        $resultado_cuenta_abono->nivel,
                                        $datos_caratula_devengado->descripcion,
                                        0.0,
                                        $row->iva,
                                        "Recursos Fiscales",
                                        $cuenta->nombre_abono,
                                        $estructura->gerencia,
                                        $estructura->centro_de_recaudacion,
                                        0,
                                    );

                                    $resultado_insertar_abono_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                    $abonos += $row->iva;

                                    $total_filas += 1;
                                }
                            } elseif (strpos(strtolower($cuenta->nombre_abono), "presupuesto") !== FALSE) {
                                $_cantidad = ($row->importe - $row->descuento) + $row->iva;
                                $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_abono = $this->db->query($sql_abono, array($cuenta->cuenta_abono));
                                $resultado_cuenta_abono = $query_abono->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_abono);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_abono.' '.$cuenta->nombre_abono);
                                }

                                $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida, concepto_especifico) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_abono = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_abono,
                                    $resultado_cuenta_abono->id_padre,
                                    $resultado_cuenta_abono->nivel,
                                    $datos_caratula_devengado->descripcion,
                                    0.0,
                                    $_cantidad,
                                    "Recursos Fiscale",
                                    $cuenta->nombre_abono,
                                    $estructura->gerencia,
                                    $estructura->centro_de_recaudacion,
                                    0,
                                );

                                $resultado_insertar_abono_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                $abonos += $_cantidad;

                                $total_filas += 1;
                            } elseif (strpos(strtolower($cuenta->nombre_abono), "general") !== FALSE) {
//                                continue;
                            } else {
                                $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_abono = $this->db->query($sql_abono, array($cuenta->cuenta_abono));
                                $resultado_cuenta_abono = $query_abono->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_abono);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_abono.' '.$cuenta->nombre_abono);
                                }

                                $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_abono = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_abono,
                                    $resultado_cuenta_abono->id_padre,
                                    $resultado_cuenta_abono->nivel,
                                    $datos_caratula_devengado->descripcion,
                                    0.0,
                                    $row->total_importe,
                                    "Recursos Fiscales",
                                    $cuenta->nombre_abono,
                                    $estructura->gerencia,
                                    $estructura->centro_de_recaudacion,
                                );

                                $resultado_insertar_abono_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                $abonos += $row->total_importe;

                                $total_filas += 1;
                            }

                        }
                    }
                    else {
                        if($cuenta->cuenta_cargo) {

                            if (strpos(strtolower($cuenta->nombre_cargo),"descuent") !== FALSE ) {
//                            $this->debugeo->imprimir_pre($cuenta);
                                $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_cargo = $this->db->query($sql_cargo, array($cuenta->cuenta_cargo));
                                $resultado_cuenta_cargo = $query_cargo->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_cargo);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_cargo.' '.$cuenta->nombre_cargo);
                                }

                                $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_cargo = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_cargo,
                                    $resultado_cuenta_cargo->id_padre,
                                    $resultado_cuenta_cargo->nivel,
                                    $datos_caratula_devengado->descripcion,
                                    $row->descuento,
                                    0.0,
                                    "Recursos Fiscales",
                                    $cuenta->nombre_cargo,
                                    $estructura->gerencia,
                                    $estructura->centro_de_recaudacion,
                                );

                                $resultado_insertar_cargo_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                $cargos += $row->descuento;

                                $total_filas += 1;
                            } elseif (strpos(strtolower($cuenta->nombre_cargo), "presupuesto") !== FALSE) {
                                $_cantidad = ($row->importe - $row->descuento) + $row->iva;
                                $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_cargo = $this->db->query($sql_cargo, array($cuenta->cuenta_cargo));
                                $resultado_cuenta_cargo = $query_cargo->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_cargo);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_cargo.' '.$cuenta->nombre_cargo);
                                }

                                $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida, concepto_especifico) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_cargo = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_cargo,
                                    $resultado_cuenta_cargo->id_padre,
                                    $resultado_cuenta_cargo->nivel,
                                    $datos_caratula_devengado->descripcion,
                                    $_cantidad,
                                    0.0,
                                    "Recursos Fiscale",
                                    $cuenta->nombre_cargo,
                                    $estructura->gerencia,
                                    $estructura->centro_de_recaudacion,
                                    0,
                                );

                                $resultado_insertar_cargo_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                $cargos += $_cantidad;

                                $total_filas += 1;
                            } elseif (strpos(strtolower($cuenta->nombre_cargo), "i.v.a.") !== FALSE || strpos(strtolower($cuenta->nombre_cargo), "i.v.a") !== FALSE || strpos(strtolower($cuenta->nombre_abono), "iva") !== FALSE) {
                                if ($row->iva !== 0) {
                                    $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                    $query_cargo = $this->db->query($sql_cargo, array($cuenta->cuenta_cargo));
                                    $resultado_cuenta_cargo = $query_cargo->row();

                                    $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_cargo);

                                    if ($existe_cuenta == FALSE) {
                                        throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_cargo.' '.$cuenta->nombre_cargo);
                                    }

                                    $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida, concepto_especifico) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                    $datos_cargo = array(
                                        $last,
                                        'Diario',
                                        $cuenta->id_correlacion_partidas_contables,
                                        $cuenta->cuenta_cargo,
                                        $resultado_cuenta_cargo->id_padre,
                                        $resultado_cuenta_cargo->nivel,
                                        $datos_caratula_devengado->descripcion,
                                        $row->iva,
                                        0.0,
                                        "Recursos Fiscale",
                                        $cuenta->nombre_cargo,
                                        $estructura->gerencia,
                                        $estructura->centro_de_recaudacion,
                                        0,
                                    );

                                    $resultado_insertar_cargo_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                    $cargos += $row->iva;

                                    $total_filas += 1;
                                }
                            } else {
                                $_cantidad = ($row->importe - $row->descuento) + $row->iva;

                                $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_cargo = $this->db->query($sql_cargo, array($cuenta->cuenta_cargo));
                                $resultado_cuenta_cargo = $query_cargo->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_cargo);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_cargo.' '.$cuenta->nombre_cargo);
                                }

                                $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_cargo = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_cargo,
                                    $resultado_cuenta_cargo->id_padre,
                                    $resultado_cuenta_cargo->nivel,
                                    $datos_caratula_devengado->descripcion,
                                    $_cantidad,
                                    0.0,
                                    "Recursos Fiscales",
                                    $cuenta->nombre_cargo,
                                    $estructura->gerencia,
                                    $estructura->centro_de_recaudacion,
                                );

                                $resultado_insertar_cargo_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                $cargos += $_cantidad;

                                $total_filas += 1;
                            }

                        }

                        if($cuenta->cuenta_abono) {

                            if (strpos(strtolower($cuenta->nombre_abono),"venta") !== FALSE ) {
//                            $this->debugeo->imprimir_pre($cuenta);
                                $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_abono = $this->db->query($sql_abono, array($cuenta->cuenta_abono));
                                $resultado_cuenta_abono = $query_abono->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_abono);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_abono.' '.$cuenta->nombre_abono);
                                }

                                $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_abono = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_abono,
                                    $resultado_cuenta_abono->id_padre,
                                    $resultado_cuenta_abono->nivel,
                                    $datos_caratula_devengado->descripcion,
                                    0.0,
                                    $row->importe,
                                    "Recursos Fiscales",
                                    $cuenta->nombre_abono,
                                    $estructura->gerencia,
                                    $estructura->centro_de_recaudacion,
                                );

                                $resultado_insertar_abono_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                $abonos += $row->importe;

                                $total_filas += 1;

                            } elseif (strpos(strtolower($cuenta->nombre_abono), "i.v.a.") !== FALSE || strpos(strtolower($cuenta->nombre_abono), "i.v.a") !== FALSE || strpos(strtolower($cuenta->nombre_abono), "iva") !== FALSE) {
                                if ($row->iva !== 0) {
                                    $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                    $query_abono = $this->db->query($sql_abono, array($cuenta->cuenta_abono));
                                    $resultado_cuenta_abono = $query_abono->row();

                                    $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_abono);

                                    if ($existe_cuenta == FALSE) {
                                        throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_abono.' '.$cuenta->nombre_abono);
                                    }

                                    $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida, concepto_especifico) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                    $datos_abono = array(
                                        $last,
                                        'Diario',
                                        $cuenta->id_correlacion_partidas_contables,
                                        $cuenta->cuenta_abono,
                                        $resultado_cuenta_abono->id_padre,
                                        $resultado_cuenta_abono->nivel,
                                        $datos_caratula_devengado->descripcion,
                                        0.0,
                                        $row->iva,
                                        "Recursos Fiscales",
                                        $cuenta->nombre_abono,
                                        $estructura->gerencia,
                                        $estructura->centro_de_recaudacion,
                                        0,
                                    );

                                    $resultado_insertar_abono_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                    $abonos += $row->iva;

                                    $total_filas += 1;
                                }
                            } elseif (strpos(strtolower($cuenta->nombre_abono), "presupuesto") !== FALSE) {
                                $_cantidad = ($row->importe - $row->descuento) + $row->iva;
                                $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_abono = $this->db->query($sql_abono, array($cuenta->cuenta_abono));
                                $resultado_cuenta_abono = $query_abono->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_abono);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_abono.' '.$cuenta->nombre_abono);
                                }

                                $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida, concepto_especifico) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_abono = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_abono,
                                    $resultado_cuenta_abono->id_padre,
                                    $resultado_cuenta_abono->nivel,
                                    $datos_caratula_devengado->descripcion,
                                    0.0,
                                    $_cantidad,
                                    "Recursos Fiscale",
                                    $cuenta->nombre_abono,
                                    $estructura->gerencia,
                                    $estructura->centro_de_recaudacion,
                                    0,
                                );

                                $resultado_insertar_abono_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                $abonos += $_cantidad;

                                $total_filas += 1;
                            } else {
                                $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_abono = $this->db->query($sql_abono, array($cuenta->cuenta_abono));
                                $resultado_cuenta_abono = $query_abono->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_abono);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_abono.' '.$cuenta->nombre_abono);
                                }

                                $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_abono = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_abono,
                                    $resultado_cuenta_abono->id_padre,
                                    $resultado_cuenta_abono->nivel,
                                    $datos_caratula_devengado->descripcion,
                                    0.0,
                                    $row->total_importe,
                                    "Recursos Fiscales",
                                    $cuenta->nombre_abono,
                                    $estructura->gerencia,
                                    $estructura->centro_de_recaudacion,
                                );

                                $resultado_insertar_abono_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                $abonos += $row->total_importe;

                                $total_filas += 1;
                            }

                        }
                    }

                }

            }

            $datos_actualizar_caratula = array(
                'no_partidas' => $total_filas,
                'cargos' => $cargos,
                'abonos' => $abonos,
            );

            $this->db->where('numero_poliza', $last);
            $this->db->update('mov_polizas_cabecera', $datos_actualizar_caratula);

            $datos_actualizar_devengado = array(
                'poliza' => 1,
            );

            $this->db->where('numero', $devengado);
            $this->db->update('mov_devengado_caratula', $datos_actualizar_devengado);

            $this->db->trans_commit();

            $respuesta = array(
                "mensaje" => "Se ha generado la póliza de Diario No. ".$last,
            );

            echo(json_encode($respuesta));

        } catch (Exception $e) {
            $this->db->trans_rollback();

            $this->db->where('numero_poliza', $last);
            $this->db->delete('mov_poliza_detalle');

            $this->db->where('numero_poliza', $last);
            $this->db->delete('mov_polizas_cabecera');

            $respuesta = array(
                "mensaje" => "".$e->getMessage(),
            );

            echo(json_encode($respuesta));
        }
    }

    function subir_devengado() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha subido un nuevo devengado');

//        Se prepara la configuracion de la libreria "Upload"
        $config['upload_path'] = './application/archivos/';
        $config['allowed_types'] = '*';

//        Se carga la libreria con sus configuraciones correspondientes
        $this->load->library('upload', $config);

//        Si el server no logra subir el archivo, despliega un mensaje de error
        if ( ! $this->upload->do_multi_upload('archivoSubir')) {
//            Se capturan los errores en una variable y se imprimen para debug
            $error = array('error' => $this->upload->display_errors());
            $this->debugeo->imprimir_pre($error);
        }
        else {
            try {
                ini_set('memory_limit', '-1');
                $mensaje = "";

                $datos = $this->upload->get_multi_upload_data();
                foreach($datos as $dato) {
//                   Nombre del archivo $dato["client_name"]
//                    $this->debugeo->imprimir_pre($dato);

//                    load our new PHPExcel library
                    $this->load->library('excel');
                    //            Si el server logra subir el archivo, se toman los datos del archivo
                    $data = $this->upload->data();

                    //            Se toma la ruta del archivo junto con su nombre y extension
                    $file = $dato["full_path"];

                    //            read file from path
                    $objPHPExcel = PHPExcel_IOFactory::load($file);

                    //            get only the Cell Collection
                    $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();

                    //            extract to a PHP readable array format
                    foreach ($cell_collection as $cell) {
                        $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
                        $fila = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
                        $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();

                        //                header will/should be in row 1 only. of course this can be modified to suit your need.
                        if ($fila == 1 || $fila == 3) {
                            continue;
                        } elseif ($fila == 2) {
                            $datos_caratula[$fila][$column] = $data_value;
                        } else {
                            $datos_detalle[$fila][$column] = $data_value;
                        }
                    }

//                    $this->debugeo->imprimir_pre($datos_caratula);

//                    $this->debugeo->imprimir_pre($datos_detalle);

                    $respuesta = array(
                        "mensaje" => '',
                    );

//                    Esta función es para tomar la fecha del compromiso
                    foreach ($datos_caratula as $fila) {
                        $fecha = ($fila['C'] - 25569) * 86400;
                        $fecha = date('Y-m-d', $fecha);
                    }

                    $linea = 1;

//                    Se revisa que existan las estructuras que se van a ingresar
                    foreach($datos_detalle as $fila) {

//                        $this->debugeo->imprimir_pre($fila);


                        $last = 0;
//      Se toma el numero del ultimo devengado
                        $ultimo = $this->recaudacion_model->ultimo_devengado();
                        if($ultimo) {
                            $last = $ultimo->ultimo + 1;
                        }
                        else {
                            $last = 1;
                        }
                        $this->recaudacion_model->apartarDevengado($last);

//        Se llama la funcion del modelo de Ingresos encargado de contar los niveles que existen
                        $total_ingresos = $this->recaudacion_model->contar_ingresos_elementos();
//        Se llama a la funcion del model de Ingresos encargada de tomar los nombres delos niveles que existen
                        $nombres_ingresos= $this->recaudacion_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
                        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
                        foreach($nombres_ingresos as $row) {
                            array_push($nombre, $row->descripcion);
                        }

                        $query = "SELECT id_niveles FROM cat_niveles WHERE COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[0]))."' as char) = ? ";

                        for($i = 1; $i < $total_ingresos->conteo; $i++){
                            $query .= "AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[$i]))."' as char) = ? ";
                        }

                        $existe = $this->recaudacion_model->existeEstructura(array("nivel1" => $fila["A"], "nivel2" => $fila["B"], "nivel3" => $fila["C"], "nivel4" => $fila["D"], "nivel5" => $fila["E"] ), $query);

                        if(!$existe) {
                            throw new Exception('La partida estructura en la línea'.$linea.' no existe.');
                        } else {

                            $query_insertar = "INSERT INTO mov_devengado_detalle (numero_devengado, id_nivel, fecha_aplicacion, subsidio, cantidad, importe, descuento, iva, total_importe, descripcion_detalle, fecha_sql, hora_aplicacion, tipo_pago, nivel ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, COLUMN_CREATE(";
//Se quito campo Cuenta
                            for ($i = 0; $i < $total_ingresos->conteo; $i++) {
                                $query_insertar .= "'" . strtolower(str_replace(' ', '_', $nombre[$i])) . "', ?, ";
                            }

                            $query_insertar .= "'dato', ?));";

                            $resultado = $this->recaudacion_model->insertar_detalle_devengado(array(
                                "ultimo" => $last,
                                "id_nivel" => $existe->id_niveles,
                                "fecha_detalle" => $fecha,
                                "subsidio" => "Ingresos Propios",
                                "cantidad" => $fila["G"],
                                "importe" => $fila["I"],
                                "descuento" => $fila["J"],
                                "iva" => $fila["K"],
                                "total_importe" => $fila["K"] + ($fila["I"] - $fila["J"]),
                                "descripcion_detalle" => $datos_caratula[2]["D"],
                                "tipo_pago" => "No Identificado",
                                "nivel1" => $fila["A"],
                                "nivel2" => $fila["B"],
                                "nivel3" => $fila["C"],
                                "nivel4" => $fila["D"],
                                "nivel5" => $fila["E"],
                            ), $query_insertar);

                            if(!$resultado) {
                                throw new Exception('Hubo un error al insertar los datos de la línea: '.$linea);
                            }

                            $cliente = $this->recaudacion_model->tomarCliente($fila["B"]);

                            $this->recaudacion_model->insertar_caratula_devengado(array(
                                "ultimo" => $last,
                                "clasificacion" => $datos_caratula[2]["A"],
                                "num_movimiento" => $datos_caratula[2]["B"],
                                "clave_cliente" => $cliente["centro_de_recaudacion"],
                                "cliente" => $cliente["descripcion"],
                                "check_firme" => 1,
                                "sucursal" => $datos_caratula[2]["A"],
                                "importe_total" => $fila["K"] + ($fila["I"] - $fila["J"]),
                                "fecha_solicitud" => $fecha,
                                "descripcion" => $datos_caratula[2]["D"],
                                "enfirme" => 0,
                                "estatus" => "Activo",
                            ));

                            $this->recaudacion_model->marcarDevengado(array(
                                "ultimo" => $last,
                                "fecha_solicitud" => $fecha,
                            ));

                        }

                        $linea += 1;
                    }

                    $mensaje = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Se insertó el detalle correctamente</div>';

                }


            } catch (Exception $e) {
                $this->db->delete('mov_devengado_caratula', array('numero' => $last));
                $this->db->delete('mov_devengado_detalle', array('numero_devengado' => $last));
//
                $mensaje = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$e->getMessage().'</div>';

            }

            $datos_header = array(
                "titulo_pagina" => "Armonniza | Resultado Archivo",
                "usuario" => $this->tank_auth->get_username(),
                "precompromisocss" => TRUE,
            );

            $this->parser->parse('front/header_main_view', $datos_header);
            $this->load->view('front/resultado_archivo_view', array(
                "mensaje" => $mensaje,
            ));
            $this->load->view('front/footer_main_view');

        }

    }


    /** === Sección de Recaudación === */
    function recaudado()
    {
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Recaudado",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/indices_recaudacion_recaudado_view');
        $this->load->view('front/footer_main_view', array("tablas" => TRUE, "recaudado" => TRUE));
    }

    function agregar_recaudado() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha agregado una partida de recaudado');
        $last = 0;
//      Se toma el numero del ultimo devengado
        $ultimo = $this->recaudacion_model->ultimo_recaudado();
        if($ultimo) {
            $last = $ultimo->ultimo + 1;
        }
        else {
            $last = 1;
        }
        $this->recaudacion_model->apartarRecaudado($last);

        $datos_header = array(
            "titulo_pagina" => "Armonniza | Agregar Recaudado",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
            "recaudacioncss" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/agregar_recaudado_view', array(
            "ultimo" => $last,
            "niveles" => $this->input_niveles_ingresos(),
            "niveles_modal" => $this->preparar_estructura_ingresos(),
            "graficas" => TRUE,
        ));

        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "efectos" => TRUE,
            "graficas" => TRUE,
            "agregar_recaudado" => TRUE
        ));
    }

    function insertar_detalle_recaudado() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha insertado detalle en el recaudado.');
//        Se llama la funcion del modelo de Ingresos encargado de contar los niveles que existen
        $total_ingresos = $this->recaudacion_model->contar_ingresos_elementos();
//        Se llama a la funcion del model de Ingresos encargada de tomar los nombres delos niveles que existen
        $nombres_ingresos = $this->recaudacion_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_ingresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }
//        Se genera el query para revisar que la estructura exista
        $query = "SELECT id_niveles FROM cat_niveles WHERE COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[0]))."' as char) = ? ";

        for($i = 1; $i < $total_ingresos->conteo; $i++){
            $query .= "AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[$i]))."' as char) = ? ";
        }
//        Se guardan los datos enviados en un arreglo
        $datos = $this->input->post();
//        $this->debugeo->imprimir_pre($datos);

        foreach ($datos as $key => $value) {
            $datos[$key]=trim($value);
        }

//        Se inicializa la variable con la respuesta del servidor
        $respuesta = array(
            "estatus" => '',
            "mensaje" => '',
        );

//        Primero hay que revisar si existe la estructura
        $existe = $this->recaudacion_model->existeEstructura($datos, $query);

        if($existe->id_niveles) {

//            Se toma el id del nivel de la partida
            $datos["id_nivel"] = $existe->id_niveles;

//            Si el usuario ha ingresado que el importe tiene iva, se multiplica el subtotal por el iva de 16% y se guarda en uan variable llamada "importe"
            //$datos["iva"] = round(($datos["importe_grabado"] - $datos["descuento_gravado"]) * 0.16, 2);
            //$datos["total_importe"] = round( ($datos["importe_grabado"] - $datos["descuento_gravado"]) + $datos["iva"] + ($datos["importe_no_grabado"] - $datos["descuento_no_gravado"]), 2);
            $datos["total_importe"] = round( ($datos["importe"] - $datos["descuento"]) + $datos["iva"], 2);

//             $this->debugeo->imprimir_pre($datos);
            //Agregar campos en INSERT INTO descuento_gravado, descuento_no_gravado, subtotal_grabado, subtotal_no_grabado,


            $query_insertar = "INSERT INTO mov_recaudado_detalle (numero_recaudado, id_nivel, fecha_aplicacion, subsidio, cantidad, importe, descuento, iva, total_importe, descripcion_detalle, fecha_sql, hora_aplicacion, tipo_pago, nivel ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, COLUMN_CREATE(";
//Se quito campo Cuenta
            for ($i = 0; $i < $total_ingresos->conteo; $i++) {
                $query_insertar .= "'" . strtolower(str_replace(' ', '_', $nombre[$i])) . "', ?, ";
            }

            $query_insertar .= "'dato', ?));";

            $resultado = $this->recaudacion_model->insertar_detalle_recaudado($datos, $query_insertar);

            if ($resultado) {
                $respuesta["estatus"] = "ok";
                $respuesta["mensaje"] = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-check-circle fa-2x ic-msj"></i> El detalle del recaudado se insertó correctamente.</div>';
            } else {
                $respuesta["estatus"] = "error_datos";
                $respuesta["mensaje"] = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-times-circle fa-2x ic-msj"></i> Hubo un error al insertar los datos.</div>';
            }
        }else{
            $respuesta["mensaje"] = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-times-circle fa-2x ic-msj"></i> La estructura no es correcta.</div>';
        }

        echo(json_encode($respuesta));
    }

    function tabla_devengado_recaudado() {
        $resultado = $this->recaudacion_model->get_datos_devengado_recaudado();
//        $this->debugeo->imprimir_pre($resultado);

        $output = array("data" => "");
        foreach($resultado as $fila) {
            $estructura = json_decode($fila->estructura);

            if($fila->enfirme == 0) {
                $firme = '<button type="button" class="btn btn-circle btn-firmec" disabled><i class="fa fa-times"></i></button>';
            }
            elseif($fila->enfirme == 1){
                $firme = '<button type="button" class="btn btn-circle btn-firmea" disabled><i class="fa fa-check"></i></button>';
            }

            $output["data"][] = array(
                $fila->id_devengado,
                $fila->numero,
                $fila->clasificacion,
                $fila->no_movimiento,
                $estructura->centro_de_recaudacion,
                $fila->fecha_solicitud,
                //$fila->fecha_aplicacion,
                '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                $fila->creado_por,
                $firme,
                '<button type="button" class="btn btn-estatus btn-success disabled">'.$fila->estatus.'</button>',
                '<a data-dismiss="modal"><i class="fa fa-check"></i></a>',
            );
        }
//        $this->debugeo->imprimir_pre($output);
        echo json_encode($output);
    }

    function tabla_indice_recaudado() {
        $resultado = $this->recaudacion_model->get_datos_recaudado_centro();
//        $this->debugeo->imprimir_pre($resultado);

        $output = array("data" => "");
        foreach($resultado as $fila) {
          $estructura = json_decode($fila->estructura);
            $firme = '';
            $estatus = '';
            $opciones = '';

            if($this->utilerias->get_permisos("ver_recaudado")){
                $opciones .= '<a href="'.base_url("recaudacion/ver_recaudado/".$fila->numero).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>';
            }

            if($fila->enfirme == 0) {
                $firme = '<button type="button" class="btn btn-circle btn-firmec" disabled><i class="fa fa-times"></i></button>';
            }
            elseif($fila->enfirme == 1){
                $firme = '<button type="button" class="btn btn-circle btn-firmea" disabled><i class="fa fa-check"></i></button>';
            }

            if($fila->estatus == "Activo") {
                $estatus = "success";
                if($this->tank_auth->get_username() == $fila->creado_por) {
                    $opciones .= '<a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>';
                } else {
                    $opciones .= '';
                }
            } elseif($fila->estatus == "Pendiente") {
                $estatus = "warning";
                if($this->tank_auth->get_username() == $fila->creado_por && $this->utilerias->get_permisos("editar_recaudado")) {
                    $opciones .= '<a href="' . base_url("recaudacion/editar_recaudado/" . $fila->numero) . '" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                                  <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>';
                } else{
                    $opciones .= '<a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>';
                }
            } else {
                $estatus = "danger";
                $opciones .= '';
            }

            if($this->utilerias->get_grupo() == 1) {
                $opciones = '<a href="'.base_url("recaudacion/ver_recaudado/".$fila->numero).'" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                             <a href="'.base_url("recaudacion/editar_recaudado/".$fila->numero).'" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                             <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>';
            }

            if($fila->poliza == 0 && $fila->estatus == "Activo") {
                $opciones .= '<a data-toggle="modal" data-target=".modal_poliza" data-tooltip="Póliza" style="margin-left: 6%;"><i class="fa fa-leanpub"></i></a>';
            }

            $output["data"][] = array(
                $fila->id_recaudado,
                $fila->numero,
                $fila->numero_devengado,
                $fila->tipo,
                $fila->no_movimiento,
                $estructura->centro_de_recaudacion,
                $fila->clasificacion,
                $fila->fecha_recaudado,
                '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->importe_total == 0 || $fila->importe_total == NULL ? '0.00' : $this->cart->format_number(round($fila->importe_total, 2)).'</div>'),
                $fila->creado_por,
                '<button type="button" class="btn btn-estatus btn-'.$estatus.' disabled">'.$fila->estatus.'</button>',
                $opciones,
            );

        }
//        $this->debugeo->imprimir_pre($output);
        echo json_encode($output);}

    function tabla_detalle_recaudado() {
//        Se llama la funcion del modelo de Ingresos encargado de contar los niveles que existen
        $total_ingresos = $this->recaudacion_model->contar_ingresos_elementos();
//        Se llama a la funcion del model de Ingresos encargada de tomar los nombres delos niveles que existen
        $nombres_ingresos= $this->recaudacion_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_ingresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }
        $query = "SELECT id_recaudado_detalle, ";

        for($i = 0; $i < $total_ingresos->conteo; $i++){
            $query .= "COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[$i]))."' as char) AS ".strtolower(str_replace(' ', '_', $nombre[$i])).", ";
        }
        //Agregar en Jquery subtotal_grabado, subtotal_no_grabado,  descuento_gravado,  descuento_no_gravado
        $query .= "subsidio, fecha_aplicacion, tipo_pago, cantidad, importe, descuento, iva, total_importe, descripcion_detalle";
        $query .= " FROM mov_recaudado_detalle ";
        $query .= " WHERE numero_recaudado = ?;";

        $devengado = $this->input->post("recaudado", TRUE);
//        $devengado = 36;
//        $this->debugeo->imprimir_pre($devengado);

        $resultado = $this->recaudacion_model->get_datos_devengadoRecaudado($devengado, $query);
//        $this->debugeo->imprimir_pre($resultado);

        $output = array();

        foreach($resultado as $fila) {
            $output[] = array(
                $fila->id_recaudado_detalle,
                $fila->gerencia,
                $fila->centro_de_recaudacion,
                $fila->rubro,
                $fila->tipo,
                $fila->clase,
                //$fila->subsidio,
                $fila->fecha_aplicacion,
                $fila->tipo_pago,
                $fila->cantidad,
                $fila->importe,
                //$fila->subtotal_no_grabado,
                //$fila->descuento_no_gravado,
                //$fila->subtotal_grabado,
                //$fila->descuento_gravado,
                $fila->iva,
                $fila->total_importe,
                '<a data-toggle="modal" data-target=".modal_editar" data-tooltip="Editar"><i class="fa fa-pencil-square-o"></i></a>',
                //$fila->cuenta,
                //$fila->descripcion_detalle,
            );
        }
//        $this->debugeo->imprimir_pre($output);
        echo json_encode($output);
    }

    function tomar_datos_Devengado() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha tomado datos del devengado');
//        Se inicializa el arreglo que va a contener los mensajes al usuario y el contenido del precompromiso
        $datos_devengado = array();

//        Se toman los numeros del compromiso y precompromiso
        $recaudado = $this->input->post("recaudado");
        $devengado = $this->input->post("devengado");

        $this->db->where('numero_recaudado', $recaudado);
        $this->db->delete('mov_recaudado_detalle');

//        Se llama a la función para copiar el detalle del precompromiso al detalle del compromiso
//        La cual devuelve los mensajes de exito o error al calcular el presupuesto disponible
        $resultado = $this->recaudacion_model->copiarDatosDevengado($devengado, $recaudado);
//        $this->debugeo->imprimir_pre($resultado);

//        Se toman los datos de la caratula del precompromiso
        $datos_devengado = $this->recaudacion_model->datos_Caratula_Devengado_Recaudado($devengado);

        echo(json_encode($datos_devengado));
    }

    function insertar_recaudado() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha insertado datos al recaudado');
//        Se inicializa la variable que contiene la respuesta de la inserción de datos
        $respuesta = array();

//        Se toman los datos enviados
        $datos = $this->input->post();

        foreach ($datos as $key => $value) {
            $datos[$key] = trim($value);
        }

//        En caso de que el usuario no haya ingresado algú precompromiso, se pasa la variable como 0
//        if(!$datos["numero"]) {
        $datos["no_devengado"] = 0;
//        }

//        Si el compromiso está firmado y tiene un numero de precompromiso asociado, se hace la actualización de la información conforme a los datos proporcionados
        if($datos["check_firme"] == 1 && $datos["no_devengado"] != 0) {
//            Se pasa el estatus del compromiso a "activo"
            $datos["estatus"] = "Activo";

//            Se toma el total del compromiso, y el total del precompromiso asociado para actualizar los saldos
            $this->db->select('importe_total, importe_total_recaudado')->from('mov_devengado_caratula')->where('numero', $datos["no_devengado"]);
            $query = $this->db->get();
            $total_devengado = $query->row();

//            Se suma el total del importe del compromiso, al total que actualmente está en el precompromiso
            $total = round($datos["importe_total"], 2) + round($total_devengado->importe_total_recaudado, 2);

//            Se llama a la función que se encarga de actualizar el saldo de las partidas del compromiso
            $respuesta = $this->recaudacion_model->actualizar_saldo_recaudado($datos);

            $query_actualizar = "UPDATE mov_devengado_caratula SET importe_total_recaudado = ? WHERE numero = ?;";
            $query_final = $this->db->query($query_actualizar, array($total, $datos["no_devengado"]));

            $this->generar_poliza_recaudado($datos["no_devengado"], $datos["ultimo"]);

        } elseif($datos["check_firme"] == 1 && $datos["no_devengado"] == 0) {
//            Se pasa el estatus del compromiso a "activo"
            $datos["estatus"] = "Activo";

            $respuesta = $this->recaudacion_model->actualizar_saldo_recaudado($datos);

            $this->generar_poliza_recaudado($datos["no_devengado"], $datos["ultimo"]);
        }
        else {
            $datos["estatus"] = "Pendiente";
        }

//        Se insertan los datos de la caratula del compromiso
        $resultado_insertar = $this->recaudacion_model->insertar_caratula_recaudacion($datos);

//        Si el resultado es exitoso, se le indica al usuario
        if($resultado_insertar) {
            $respuesta["mensaje_insertar"] = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-check-circle fa-2x ic-msj"></i> Datos insertados correctamente.</div>';
        }
//        De lo contrario, se manda un error al usuario
        else {
            $respuesta["mensaje_insertar"] = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-times-circle fa-2x ic-msj"></i> Error al insertar los datos.</div>';
        }

        echo(json_encode($respuesta));

    }

    function editar_detalle_recaudado() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha editado un detalle en el recaudado');
        $mensaje = array(
            "mensaje" => "",
        );

//        Se toman los datos del compromiso a insertar
        $datos = $this->input->post();
//         $datos = array(
//             "id_compromiso" => 1366,
//             "cantidad" => 1,
//             "fecha" => "2015-02-12",
//             "precio" => 20000,
//             "iva" => 1,
//             );

        foreach ($datos as $key => $value) {
            $datos[$key]=trim($value);
        }

        // echo("Datos iniciales: ");
//         $this->debugeo->imprimir_pre($datos);

        $datos_actualizar = array(
            'total_importe' => $datos["editar_importe"] + $datos["editar_iva"],
            'iva' => $datos["editar_iva"],
            'importe' => $datos["editar_importe"],
        );

        $this->db->where('id_recaudado_detalle', $datos["editar_id_detalle"]);
        $resultado = $this->db->update('mov_recaudado_detalle', $datos_actualizar);

        if($resultado) {
            $mensaje["mensaje"] = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-check-circle fa-2x ic-msj"></i> Éxito al cambiar las cantidades.</div>';
            echo(json_encode($mensaje));
        }
        else{

            $mensaje["mensaje"] = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-times-circle fa-2x ic-msj"></i> Hubo un error al tratar de actualizar los datos, por favor, intentelo de nuevo.</div>';
            echo(json_encode($mensaje));
        }
    }

    function editar_recaudado($recaudado = NULL) {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha editado un recaudado');

        $query = "SELECT * FROM mov_recaudado_caratula WHERE numero = ?";

        $resultado = $this->ciclo_model->get_datos_compromiso_caratula($recaudado, $query);

//        $this->debugeo->imprimir_pre($resultado);
        //        Se llama la funcion del modelo de Ingresos encargado de contar los niveles que existen
        $total_ingresos = $this->ingresos_model->contar_ingresos_elementos();
//        Se llama a la funcion del model de Ingresos encargada de tomar los nombres delos niveles que existen
        $nombres_ingresos = $this->ingresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_ingresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

        $niveles = '';
        $num = 1;

        for($i = 0; $i < $total_ingresos->conteo; $i++) {
            $nom_niv = ucwords(strtolower($nombre[$i]));
            $niveles .= '
                                <!-- <h5>Nivel '.$num.'</h5>
                                <label>'.$nombre[$i].'</label>-->
                                <input type="text" class="form-control" id="input_nivel'.$num.'" placeholder="'.$nom_niv.'" >
                        ';
            $num++;
        }


        $datos_header = array(
            "titulo_pagina" => "Armonniza | Editar recaudado",
            "usuario" => $this->tank_auth->get_username(),
            "compromisocss" => TRUE,
            "tablas" => TRUE,
        );

//        $resultado->ultimo = $recaudado;
        $resultado->niveles = $niveles;
        $resultado->niveles_modal = $this->preparar_estructura_ingresos();

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/editar_recaudado_view', $resultado);
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "efectos" => TRUE,
            "graficas" => TRUE,
            "editar_recaudado" => TRUE,
        ));

    }

    function subir_recaudado_devengado() {

        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

//        Se prepara la configuracion de la libreria "Upload"
        $config['upload_path'] = './application/archivos/';
        $config['allowed_types'] = '*';

//        Se carga la libreria con sus configuraciones correspondientes
        $this->load->library('upload', $config);

//        Si el server no logra subir el archivo, despliega un mensaje de error
        if ( ! $this->upload->do_upload('archivoSubir')) {
//            Se capturan los errores en una variable y se imprimen para debug
            $error = array('error' => $this->upload->display_errors());
            $this->debugeo->imprimir_pre($error);
        }
        else {

            try {
                ini_set('memory_limit', '-1');
//            load our new PHPExcel library
                $this->load->library('excel');
//            Si el server logra subir el archivo, se toman los datos del archivo
                $data = $this->upload->data();

//            Se toma la ruta del archivo junto con su nombre y extension
                $file = $data["full_path"];

//            read file from path
                $objPHPExcel = PHPExcel_IOFactory::load($file);

//            get only the Cell Collection
                $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();

//            extract to a PHP readable array format
                foreach ($cell_collection as $cell) {
                    $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
                    $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
                    $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();

//                header will/should be in row 1 only. of course this can be modified to suit your need.
                    if ($row == 1) {
                        continue;
                    } else {
                        $datos_detalle[$row][$column] = $data_value;
                    }
                }

//                $this->debugeo->imprimir_pre($datos_detalle);

                $this->db->trans_begin();

                foreach($datos_detalle as $key => $value) {

                    $value["devengado"] = $value["D"];

//                    $this->debugeo->imprimir_pre($value);

                    $recaudado = $this->tomar_datos_Devengado_archivo($value);

                    $recaudado["no_devengado"] = $recaudado["numero"];

//                    $this->debugeo->imprimir_pre($recaudado);

                    $resultado = $this->insertar_recaudado_acomodado($recaudado);

                    if($resultado) {

                        $this->db->trans_commit();

                        $respuesta = array(
                            "mensaje" => '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>El archivo se ha insertado con &eacute;xito.</div>',
                        );

                    } else {
                        throw new Exception('Hubo un error al insertar el detalle del archivo, por favor, contacte a su administrador.');
                    }
                }

            }

            catch (Exception $e) {
                $this->db->trans_rollback();

                $this->db->delete('mov_devengado_caratula', array('numero' => $last));
                $this->db->delete('mov_devengado_detalle', array('numero_devengado' => $last));

                $respuesta = array(
                    "mensaje" => '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$e->getMessage().'</div>',
                );
            }

            $datos_header = array(
                "titulo_pagina" => "Armonniza | Resultado Archivo",
                "usuario" => $this->tank_auth->get_username(),
                "precompromisocss" => TRUE,
            );

            $this->parser->parse('front/header_main_view', $datos_header);
            $this->load->view('front/resultado_archivo_view', array(
                "mensaje" => $respuesta["mensaje"],
            ));
            $this->load->view('front/footer_main_view');
        }
    }

    function cancelar_recaudado_caratula() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha cancelado una partida del devengado');
        $mensaje = array(
            "mensaje" => "",
        );

//      Se toma el devengado a cancelar
        $recaudado = $this->input->post("recaudado");

//      Se hace el query para tomar los datos de caratula del devengado
        $query_caratula = "SELECT * FROM mov_recaudado_caratula WHERE id_recaudado = ?";

//      Se llama a la función para tomar los datos de la caratula del devengado
        $datos_caratula = $this->recaudacion_model->get_datos_devengado_caratula($recaudado, $query_caratula);

//      Se actualiza el estatus del devengado a cancelado

        $query = "UPDATE mov_recaudado_caratula SET estatus = 'cancelado', cancelada = 1, enfirme = 0, fecha_cancelada = ? WHERE numero = ?; ";
        $fin = $this->recaudacion_model->cancelarDevengadoCaratula($recaudado, $query);

        if($fin) {
            $mensaje["mensaje"] = "ok";
            echo(json_encode($mensaje));
        }
        else {
            $mensaje["mensaje"] = "error";
            echo(json_encode($mensaje));
        }

    }

    /**
     * Esta función se encarga de recaudar los datos que se indican
     */
    function recaudar_devengado() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha recaudado un registro del devengado');

        $devengado = trim($this->input->post('devengado', TRUE));
        $fecha = trim($this->input->post('fecha', TRUE));

//        $devengado = 1;
//        $fecha = "2015-03-20";

//        $this->debugeo->imprimir_pre($devengado);
        $this->db->select('*')->from('mov_devengado_detalle')->where('numero_devengado', $devengado);
        $query = $this->db->get();
        $resultado_detalle = $query->result();

        try{

            if(!$resultado_detalle) {
                throw new Exception('No hay detalle que recaudar');
            }

//            $this->debugeo->imprimir_pre($resultado_detalle);
            foreach($resultado_detalle as $fila) {
                $data = array(
                    'numero_recaudado' => $last,
                    'tipo' => 'Recaudado',
                    'subsidio' => $fila->subsidio,
                    'tipo_pago' => $fila->tipo_pago,
                    'fecha_aplicacion' => $fila->fecha_aplicacion,
                    'hora_aplicacion' => $fila->hora_aplicacion,
                    'fecha_sql' => $fila->fecha_sql,
                    'cantidad' => $fila->cantidad,
                    'importe' => $fila->importe,
                    'descuento' => $fila->descuento,
                    //'subtotal_grabado' => $fila->subtotal_grabado,
                    //'subtotal_no_grabado' => $fila->subtotal_no_grabado,
                    //'descuento_gravado' => $fila->descuento_gravado,
                    //'descuento_no_gravado' => $fila->descuento_no_gravado,
                    'iva' => $fila->iva,
                    'total_importe' => $fila->total_importe,
                    'descripcion_detalle' => $fila->descripcion_detalle,
                    'partida' => $fila->partida,
                    'id_nivel' => $fila->id_nivel,
                    'nivel' => $fila->nivel,
                    'capitulo' => $fila->capitulo,
                    'mes_destino' => $fila->mes_destino,
                );

                $this->db->insert('mov_recaudado_detalle', $data);
            }

            $this->db->select('*')->from('mov_devengado_caratula')->where('numero', $devengado);
            $query = $this->db->get();
            $resultado_caratula = $query->row();

            $datos = array(
                'tipo' => 'Recaudado',
                'numero' => $last,
                'numero_devengado' => $resultado_caratula->numero,
                'no_solicitud' => $resultado_caratula->no_solicitud,
                'no_autorizacion' => $resultado_caratula->no_autorizacion,
                'no_movimiento' => $resultado_caratula->no_movimiento,
                'fecha_solicitud' => $resultado_caratula->fecha_solicitud,
                'fecha_aplicacion' => $resultado_caratula->fecha_aplicacion,
                'fecha_sql' => $resultado_caratula->fecha_sql,
                'year' => $resultado_caratula->year,
                'clasificacion' => $resultado_caratula->clasificacion,
                'no_movimiento' => $resultado_caratula->no_movimiento,
                'clave_cliente' => $resultado_caratula->clave_cliente,
                'cliente' => $resultado_caratula->cliente,
                'importe_total' => $resultado_caratula->importe_total,
                'cancelada' => $resultado_caratula->cancelada,
                'fecha_cancelada' => $resultado_caratula->fecha_cancelada,
                'fecha_recaudado' => date("Y-m-d"),
                'hora_recaudado' => date("H:i:s"),
                'enfirme' => $resultado_caratula->enfirme,
                'sifirme' => $resultado_caratula->sifirme,
                'estatus' => $resultado_caratula->estatus,
                'descripcion' => $resultado_caratula->descripcion,
                'id_usuario' => $resultado_caratula->id_usuario,
                'creado_por' => $resultado_caratula->creado_por,
            );

            $resultado_insertar_caratula = $this->db->insert('mov_recaudado_caratula', $datos);

//            $this->actualizar_saldo_ingreso($last, $fecha);

            echo(json_encode($respuesta));

        } catch(Exception $e) {

            $respuesta = array(
                "mensaje" => "".$e->getMessage(),
            );

            echo(json_encode($respuesta));
        }

    }

    function actualizar_saldo_ingreso($id, $fecha) {
        $sql_caratula = "SELECT * FROM mov_recaudado_caratula WHERE numero = ?;";
        $datos_caratula = $this->recaudacion_model->get_datos_recaudado_caratula($id, $sql_caratula);
        $datos_detalle = $this->recaudacion_model->get_datos_recaudado_detalle($datos_caratula->numero);
        //$this->debugeo->imprimir_pre($datos_caratula);
        //$this->debugeo->imprimir_pre($datos_detalle);

        $mes = $this->utilerias->convertirFechaAMes($fecha);

        foreach($datos_detalle as $fila) {
//            Se toma el saldo del mes y del año
            $saldo_mes = $this->recaudacion_model->get_saldo_partida($mes, $fila->id_nivel);

            //$this->debugeo->imprimir_pre($saldo_mes);

            $nuevo_saldo_recaudado_mes = round($saldo_mes->mes_devengado, 2) + round($fila->total_importe, 2);

            $nuevo_saldo_recaudado = round($saldo_mes->recaudado_anual, 2) + round($fila->total_importe, 2);

            $nuevo_saldo_year = round($saldo_mes->total_anual, 2) + round($fila->total_importe, 2);

//            Se genera el movimiento
            $this->recaudacion_model->actualizar_saldo_partida($mes, $nuevo_saldo_recaudado_mes, $nuevo_saldo_recaudado, $nuevo_saldo_year, $fila->id_nivel);
        }

    }

    private function generar_poliza_recaudado($devengado = NULL, $recaudado = NULL) {
        $respuesta = array(
            "mensaje" => "",
        );

        try {
            $id = $this->tank_auth->get_user_id();

            $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
            $resultado_query_usuario = $this->db->query($query_usuario, array($id));
            $nombre_encontrado = $resultado_query_usuario->row();
            $nombre_completo = $nombre_encontrado->nombre." ".$nombre_encontrado->apellido_paterno." ".$nombre_encontrado->apellido_materno;

//        Se inicizaliza la variable con el ultimo valor de las polizas
            $last = 0;
//        Se toma el numero de la ultima poliza
            $ultimo = $this->ciclo_model->ultima_poliza();

//        Se le suma uno al ultimo valor de las polizas
            if($ultimo) {
                $last = $ultimo->ultimo + 1;
            }
//        De lo contrario se inicia en 1
            else {
                $last = 1;
            }

            $query = "SELECT * FROM mov_devengado_caratula WHERE numero = ?";
            $resultado = $this->recaudacion_model->get_datos_devengado_caratula($devengado, $query);

//            $contrarecibo = $datos_caratula_movimiento->numero;

//            $this->debugeo->imprimir_pre($resultado);

            $resultado_detalle = $this->recaudacion_model->get_datos_devengado_detalle($devengado);

//            $this->debugeo->imprimir_pre($resultado_detalle);

//        Se crea un arreglo donde se van a guardar los resultados del detalle del compromiso
            $datos_detalle_poliza = array();

            if (!$resultado_detalle) {
                throw new Exception('No hay movimientos dentro del devengado.');
            }

            $centro_recaudacion = '';
            $clase = '';
            $nombre_fuente_financiamiento = '';
            $total_filas = 0;
            $cargos = 0;
            $abonos = 0;

            $this->db->trans_begin();

            $this->db->select('numero_poliza')->from('mov_polizas_cabecera')->where('no_devengado', $devengado);
            $query_numero_devengado_diario = $this->db->get();
            $numero_devengado_diario = $query_numero_devengado_diario->row_array();

            if(!isset($numero_devengado_diario["numero_poliza"])) {
                throw new Exception('No se ha generado la póliza de Diario del devengado '.$devengado);
            }

            $query_insertar_caratula_poliza = "INSERT INTO mov_polizas_cabecera (numero_poliza, tipo_poliza, fecha, fecha_real, concepto, enfirme, sifirme, no_partidas, importe, cancelada, estatus, creado_por, autorizada, cargos, abonos, no_devengado, no_recaudado, concepto_especifico, clave_cliente, cliente, no_movimiento, poliza_devengado) VALUES (?, ?, NOW(), NOW(), ?, 0, 0, ?, ?, 0, ?, ?, 0, ?, ?, ?, ?, ?, ?, ?, ? ,?);";
            $datos_caratula_poliza = array(
                $last,
                'Ingresos',
                $resultado->descripcion,
                $total_filas,
                $resultado->importe_total,
                'espera',
                $nombre_completo,
                $cargos,
                $abonos,
                $devengado,
                $recaudado,
                $resultado->descripcion,
                $resultado->clave_cliente,
                $resultado->cliente,
                $resultado->no_movimiento,
                $numero_devengado_diario["numero_poliza"],
            );

            $resultado_insertar_caratula = $this->ciclo_model->insertar_detalle_poliza($query_insertar_caratula_poliza, $datos_caratula_poliza);

            foreach ($resultado_detalle as $row) {
//                $this->debugeo->imprimir_pre($row);

                $estructura = json_decode($row->estructura);

//                $this->debugeo->imprimir_pre($estructura);

                $centro_recaudacion = $estructura->centro_de_recaudacion;
                $clase = $estructura->clase;

                $datos_cuentas = $this->recaudacion_model->tomar_cuentas_recaudacion($centro_recaudacion);

                foreach ($datos_cuentas as $cuenta) {

                    if (!$cuenta) {
                        throw new Exception('No existe cuenta contable asociada a la partida.');
                    }

//                    $this->debugeo->imprimir_pre($cuenta);

                    if ($cuenta->cuenta_cargo) {
                        if (strpos(strtolower($cuenta->nombre_cargo), "i.v.a.") !== FALSE || strpos(strtolower($cuenta->nombre_cargo), "i.v.a") !== FALSE || strpos(strtolower($cuenta->nombre_abono), "iva") !== FALSE) {
                            if ($row->iva !== 0) {
                                $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_cargo = $this->db->query($sql_cargo, array($cuenta->cuenta_cargo));
                                $resultado_cuenta_cargo = $query_cargo->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_cargo);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_cargo.' '.$cuenta->nombre_cargo);
                                }

                                $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida, concepto_especifico) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_cargo = array(
                                    $last,
                                    'Ingresos',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_cargo,
                                    $resultado_cuenta_cargo->id_padre,
                                    $resultado_cuenta_cargo->nivel,
                                    $resultado->descripcion,
                                    $row->iva,
                                    0.0,
                                    4,
                                    $cuenta->nombre_cargo,
                                    $centro_recaudacion,
                                    $clase,
                                    0,
                                );

                                $resultado_insertar_cargo_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                $cargos += $row->iva;

                                $total_filas += 1;
                            }
                        }
                        else {
                            $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                            $query_cargo = $this->db->query($sql_cargo, array($cuenta->cuenta_cargo));
                            $resultado_cuenta_cargo = $query_cargo->row();

                            $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_cargo);

                            if ($existe_cuenta == FALSE) {
                                throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_cargo.' '.$cuenta->nombre_cargo);
                            }

                            $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida, concepto_especifico) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                            $datos_cargo = array(
                                $last,
                                'Ingresos',
                                $cuenta->id_correlacion_partidas_contables,
                                $cuenta->cuenta_cargo,
                                $resultado_cuenta_cargo->id_padre,
                                $resultado_cuenta_cargo->nivel,
                                $resultado->descripcion,
                                $row->total_importe,
                                0.0,
                                4,
                                $cuenta->nombre_cargo,
                                $centro_recaudacion,
                                $clase,
                                0,
                            );

                            $resultado_insertar_cargo_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                            $cargos += $row->total_importe;

                            $total_filas += 1;
                        }
                    }

                    if ($cuenta->cuenta_abono) {
                        if (strpos(strtolower($cuenta->nombre_abono), "i.v.a.") !== FALSE || strpos(strtolower($cuenta->nombre_abono), "i.v.a") !== FALSE || strpos(strtolower($cuenta->nombre_abono), "iva") !== FALSE) {
                            if ($row->iva !== 0) {
                                $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_abono = $this->db->query($sql_abono, array($cuenta->cuenta_abono));
                                $resultado_cuenta_abono = $query_abono->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_abono);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_abono.' '.$cuenta->nombre_abono);
                                }

                                $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida, concepto_especifico) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_abono = array(
                                    $last,
                                    'Ingresos',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_abono,
                                    $resultado_cuenta_abono->id_padre,
                                    $resultado_cuenta_abono->nivel,
                                    $resultado->descripcion,
                                    0.0,
                                    $row->iva,
                                    4,
                                    $cuenta->nombre_abono,
                                    $centro_recaudacion,
                                    $clase,
                                    0,
                                );

                                $resultado_insertar_abono_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                $abonos += $row->iva;

                                $total_filas += 1;
                            }
                        }
                        else {
                            $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                            $query_abono = $this->db->query($sql_abono, array($cuenta->cuenta_abono));
                            $resultado_cuenta_abono = $query_abono->row();

                            $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_abono);

                            if ($existe_cuenta == FALSE) {
                                throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_abono.' '.$cuenta->nombre_abono);
                            }

                            $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida, concepto_especifico) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                            $datos_abono = array(
                                $last,
                                'Ingresos',
                                $cuenta->id_correlacion_partidas_contables,
                                $cuenta->cuenta_abono,
                                $resultado_cuenta_abono->id_padre,
                                $resultado_cuenta_abono->nivel,
                                $resultado->descripcion,
                                0.0,
                                $row->total_importe,
                                4,
                                $cuenta->nombre_abono,
                                $centro_recaudacion,
                                $clase,
                                0,
                            );

                            $resultado_insertar_abono_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                            $abonos += $row->total_importe;

                            $total_filas += 1;
                        }
                    }
                }
            }

            $datos_actualizar_caratula = array(
                'no_partidas' => $total_filas,
                'cargos' => $cargos,
                'abonos' => $abonos,
            );

            $this->db->where('numero_poliza', $last);
            $this->db->update('mov_polizas_cabecera', $datos_actualizar_caratula);

            $datos_actualizar_recaudado = array(
                'poliza' => 1,
            );

            $this->db->where('numero', $resultado->numero);
            $this->db->update('mov_recaudado_caratula', $datos_actualizar_recaudado);

            $this->db->trans_commit();

            return "exito";

        } catch (Exception $e) {
            $this->db->trans_rollback();

            $this->db->where('numero_poliza', $last);
            $this->db->delete('mov_poliza_detalle');

            $this->db->where('numero_poliza', $last);
            $this->db->delete('mov_polizas_cabecera');

            return $e->getMessage();
        }
    }

    function checar_datos() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha checado datos del recaudado');
        $sql = "SELECT COLUMN_JSON(nivel) AS estructura FROM cat_niveles WHERE id_niveles = 578";
        $query = $this->db->query($sql);
        $resultado = $query->result();
        foreach($resultado as $fila) {
            $estructura = json_decode($fila->estructura);
            $this->debugeo->imprimir_pre($estructura
            );
        }
    }

    /**Esta función permite visulizar el Recaudado*/
    function ver_recaudado($devengado = NULL) {
        $query = "SELECT * FROM mov_recaudado_caratula WHERE numero = ?;";
        $resultado = $this->recaudacion_model->get_datos_recaudado_caratula($devengado, $query);
        $resultado_detalle = $this->recaudacion_model->get_datos_recaudado_detalle($devengado);
        //$this->debugeo->imprimir_pre($resultado_detalle);

        $datos_header = array(
            "titulo_pagina" => " Armonniza| Ver Recaudado",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
        );

        $resultado->ultimo = $devengado;

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/ver_recaudado_view', $resultado);
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "ver_recaudado" => TRUE,
        ));
    }

    /*Reporte de visualización Devengado con información del Recaudado*/
    function exportar_devengado_info_recaudado() {
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Exportar Devengado con Recaudado",
            "usuario" => $this->tank_auth->get_username(),
            "precompromisocss" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/exportar_devengado_info_recaudado_filtro');
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "devengado" => TRUE
        ));
    }

    function devengado_info_recaudado_formato() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha exportado el reporte Devengado vs. Recaudado');
        $datos = $this->input->post();
        $this->load->library('excel');
        $fecha = date("Y-m-d");
        $hora = date("H:i:s");

        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Devengado vs. Recaudado');
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('Educal');
        $objDrawing->setPath('img/logo2.png');
        $objDrawing->setOffsetX(50);
        $objDrawing->setHeight(80);
        // $objDrawing->setWidth(10);
        $objDrawing->setCoordinates('A1');
        $objDrawing->setWorksheet($this->excel->getActiveSheet());

        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
            'font'  => array(
                'bold'  => true,
            )
        );
        $style_center = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );
        $style_left = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            )
        );

        $this->excel->getActiveSheet()->mergeCells('A1:L1')->getStyle("A1")->applyFromArray($style);
        $this->excel->getActiveSheet()->setCellValue('A1','EDUCAL, S.A. DE C.V.');
        $this->excel->getActiveSheet()->mergeCells('A2:L2')->getStyle("A2")->applyFromArray($style);
        $this->excel->getActiveSheet()->setCellValue('A2', 'Devengado vs. Recaudado');
        $this->excel->getActiveSheet()->getStyle("C3:L3")->applyFromArray($style);
        $this->excel->getActiveSheet()->getStyle("C4:L4")->applyFromArray($style_center);
        $this->excel->getActiveSheet()->setCellValue('C3', 'Período');
        $this->excel->getActiveSheet()->setCellValue('G3', 'Fecha Emisión');
        $this->excel->getActiveSheet()->setCellValue('L3', 'Hora Emisión');
        $this->excel->getActiveSheet()->setCellValue('C4', $datos["fecha_inicial"] .' al '. $datos["fecha_final"]);
        $this->excel->getActiveSheet()->setCellValue('G4', $fecha);
        $this->excel->getActiveSheet()->setCellValue('L4', $hora);
        $this->excel->getActiveSheet()->getStyle("A6:L6")->applyFromArray($style);
        $this->excel->getActiveSheet()->mergeCells('A5:L5');


        //set cell A1 content with some text
        $this->excel->getActiveSheet()->getStyle("A:D")->applyFromArray($style_center);
        $this->excel->getActiveSheet()->setCellValue('A6', 'No. Devengado')->getColumnDimension("A")->setWidth(15);
        $this->excel->getActiveSheet()->setCellValue('B6', 'Centro de Recaudación')->getColumnDimension("B")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('C6', 'No. Factura')->getColumnDimension("C")->setWidth(15);
        $this->excel->getActiveSheet()->setCellValue('D6', 'Fecha Devengado')->getColumnDimension("D")->setWidth(15);
        $this->excel->getActiveSheet()->setCellValue('E6', 'Importe')->getColumnDimension("E")->setWidth(15);
        $this->excel->getActiveSheet()->setCellValue('F6', 'I.V.A.')->getColumnDimension("F")->setWidth(15);
        $this->excel->getActiveSheet()->setCellValue('G6', 'Importe Total')->getColumnDimension("G")->setWidth(15);
        $this->excel->getActiveSheet()->getStyle("H")->applyFromArray($style_center);
        $this->excel->getActiveSheet()->setCellValue('H6', 'No. Recaudado')->getColumnDimension("H")->setWidth(15);
        $this->excel->getActiveSheet()->setCellValue('I6', 'Fecha Recaudado')->getColumnDimension("I")->setWidth(15);
        $this->excel->getActiveSheet()->setCellValue('J6', 'Importe Total')->getColumnDimension("J")->setWidth(15);
        $this->excel->getActiveSheet()->getStyle("K")->applyFromArray($style_center);
        $this->excel->getActiveSheet()->setCellValue('K6', 'No. Factura Egresos')->getColumnDimension("K")->setWidth(18);
        $this->excel->getActiveSheet()->setCellValue('L6', 'Total Factura Egresos')->getColumnDimension("L")->setWidth(18);


        $resultado = $this->recaudacion_model->get_datos_devengado_info_recaudado_exportar($datos);
        //$this->debugeo->imprimir_pre($resultado);
        $row = 7;
        $importe = 0;

        foreach($resultado as $fila) {
            $this->excel->getActiveSheet()->setCellValue('A'.$row, $fila["numero"]);
            $this->excel->getActiveSheet()->setCellValue('B'.$row, $fila["centro_de_recaudacion"]);
            $this->excel->getActiveSheet()->setCellValue('C'.$row, $fila["no_movimiento"]);
            $this->excel->getActiveSheet()->setCellValue('D'.$row, $fila["fecha_solicitud"]);
            $this->excel->getActiveSheet()->setCellValue('E'.$row, '$ '.$this->cart->format_number($fila["importe"]));
            $this->excel->getActiveSheet()->setCellValue('F'.$row, '$ '.$this->cart->format_number($fila["iva"]));
            $this->excel->getActiveSheet()->setCellValue('G'.$row, '$ '.$this->cart->format_number($fila["total_importe"]));
            $this->excel->getActiveSheet()->setCellValue('H'.$row, $fila["numero_recaudado"]);
            $this->excel->getActiveSheet()->setCellValue('I'.$row, $fila["fecha_recaudado"]);
            if($fila["importe_recaudado"] != NULL) {
                $this->excel->getActiveSheet()->setCellValue('J' . $row, '$ ' . $this->cart->format_number($fila["importe_recaudado"]));
            } else{
                $this->excel->getActiveSheet()->setCellValue('J' . $row, ' ');
            }
            $this->excel->getActiveSheet()->setCellValue('K'.$row, $fila["factura_egreso"]);
            if($fila["total_egreso"] != NULL){
                $this->excel->getActiveSheet()->setCellValue('L'.$row, '$ '.$this->cart->format_number($fila["total_egreso"]));
            } else {
                $this->excel->getActiveSheet()->setCellValue('L'.$row, ' ');
            }


            $row += 1;
        }

//        $this->benchmark->mark('code_start');

//        $this->benchmark->mark('code_end');

//        echo $this->benchmark->elapsed_time('code_start', 'code_end');

        $filename="Devengado vs. Recaudado.xls"; // Agregar fecha en que se generó
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
//        save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
//        if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
//        force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');

    }

    /**Esta función permite generar formato impresión Recaudado Individual*/
    function imprimir_recaudado($devengado = NULL) {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha impreso datos del recaudado ');
        $datos = $this->input->post();
        $fecha = date("Y-m-d");
        //$this->debugeo->imprimir_pre($devengado);

        $query = "SELECT * FROM mov_recaudado_caratula WHERE id_recaudado = ?;";
        $datos = $this->recaudacion_model->get_datos_recaudado_caratula($devengado, $query);
        //$this->debugeo->imprimir_pre($datos);

        $datos_detalle = $this->recaudacion_model->get_datos_recaudado_detalle($devengado);
        //$this->debugeo->imprimir_pre($datos_detalle);

        $this->load->library('Pdf');

        $pdf = new Pdf('L', 'cm', 'A4', true, 'UTF-8', false);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Armonniza');
        $pdf->SetTitle('Comprobante Recaudado');
        $pdf->SetSubject('Comprobante Recaudado');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
//        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
//        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        // remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
//        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, 15 , PDF_MARGIN_RIGHT);
//        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// ---------------------------------------------------------

// set font
        $pdf->SetFont('helvetica', '', 12);

// add a page
        $pdf->AddPage();

        $encabezado = '<table cellspacing="3" style="font-size: small;">
                        <tr>
                            <td align="left"><b>&nbsp; &nbsp; &nbsp;Fecha Emisión</b></td>
                            <td align="center"><b></b></td>
                            <td align="right"><b>No. Recaudado</b></td>
                        </tr>
                        <tr>
                            <td align="left">&nbsp; &nbsp; &nbsp; '.$fecha.' </td>
                            <td align="center"></td>
                            <td align="right">'.$datos->numero.'</td>
                        </tr>
                       </table>';
        $tabla_general = '';
        $total = 0;
        foreach($datos_detalle as $fila) {
            $estructura = json_decode($fila->estructura);
            $tabla_general .= '<tr>
                           <td align="left" style=" border-right: 1px solid #BDBDBD;" width="25%">'.$estructura->centro_de_recaudacion.' &nbsp; '.$estructura->rubro.' &nbsp; '.$estructura->tipo.' &nbsp; '.$estructura->clase.'</td>
                           <td align="left" style=" border-right: 1px solid #BDBDBD;" width="15%">'.$fila->subsidio.'</td>
                           <td align="center" style=" border-right: 1px solid #BDBDBD;" width="15%">'.$fila->fecha_aplicacion.'</td>
                           <td align="left" style=" border-right: 1px solid #BDBDBD;" width="30%">'.$fila->descripcion_detalle.'</td>
                           <td align="center" width="16%">$ '.$this->cart->format_number(round($fila->total_importe, 2)).'</td>
                        </tr>';
            $total += $fila->total_importe;
        }

        $tabla_datos = '<table style="font-size: small;" cellspacing="3">
                            <tr>
                                <td align="left" width="20%"><b>Clasificación</b></td>
                                <td align="left" width="40%">'. $datos->clasificacion .'</td>
                                <td align="left" width="20%"><b>Fecha de Solicitud</b></td>
                                <td align="left" width="20%">'. $datos->fecha_solicitud .'</td>
                            </tr>
                            <tr>
                                <td align="left" width="20%"><b>Clave</b></td>
                                <td align="left" width="40%">'. $datos->clave_cliente .'</td>
                                <td align="left" width="20%"><b>No. Factura</b></td>
                                <td align="left" width="20%">'. $datos->no_movimiento .'</td>
                            </tr>
                             <tr>
                                <td align="left" width="20%"><b>Centro de Recaudación</b></td>
                                <td align="left" width="40%">'. $datos->cliente .'</td>
                                <td align="left" width="20%"><b>Importe Total</b></td>
                                <td align="left" width="20%">$ '. $this->cart->format_number($total) .'</td>
                            </tr>
                        </table>';
        $total = '<table style="font-size: small;" cellpadding="4">
                        <tr>
                           <td align="left" width="60%" style="border-right: 1px solid #BDBDBD;"><b>Total</b></td>
                           <td align="center" width="50%">$ '.$this->cart->format_number($total).'</td>
                        </tr>
                   </table>';


        $html = '
        <style>
            .cont-general{
                border: 1px solid #eee;
                border-radius: 1%;
                margin: 2% 14%;
            }
            .cont-general2{
                border: 1px solid #BDBDBD;
            }
            .cont-general .borde-inf{
                border-bottom: 1px solid #eee;
            }
            .cont-general .borde-sup{
                border-top: 1px solid #eee;
            }
        </style>

        <table class="cont-general" border="0" cellspacing="3" style="font-size: small;" cellpadding="2">
            <tr cellspacing="10">
                <th width="2%"></th>
                <th align="left" width="12%" style="margin-top: 200px;"><img src="'.base_url("img/logo2.png").'" /></th>
                <th align="center" width="84%" style="line-height: 7px;">
                   <h3>EDUCAL, S.A. DE C.V.</h3>
                   <h5>Comprobante Recaudado</h5>
                   '.$encabezado.'
                </th>
                <th width="2%"></th>
            </tr>
            <tr><td class="borde-sup"  width="506"></td></tr>
            <tr>
                <td width="2%"></td>
                <td width="96%">'.$tabla_datos.'</td>
                <td width="2%"></td>
            </tr>
            <tr><td width="100%" class="borde-sup"></td></tr>
            <tr>
                <td width="2%"></td>
                <td width="96%">
                    <table style="font-size: small;  border: 1px solid #BDBDBD;" cellpadding="4">
                        <tr>
                             <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="25%">Estructura Administrativa</td>
                             <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="15%">Subsidio</td>
                             <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="15%">Fecha Aplicada</td>
                              <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="30%">Descripción</td>
                             <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="16%">Importe</td>
                        </tr>
                        '.$tabla_general.'
                       </table>
                </td>
                <td width="2%"></td>
            </tr>
            <tr><td width="100%" class="borde-inf"></td></tr>
            <tr><td width="506"></td></tr>
            <tr>
                <td width="60%"></td>
                <td width="38%" class="cont-general2">'.$total.'</td>
                <td width="2%"></td>
            </tr>
            <tr><td width="506"></td></tr>
</table>';

// output the HTML content

        $pdf->writeHTML($html, false, false, true, false, 'top');

//Close and output PDF document
        $pdf->Output('Comprobante Recaudado.pdf', 'I');
    }
    /**Esta función permite generar formato impresión Recaudados en General*/
    function imprimir_recaudados() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha impreso datos recaudado');
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Recaudado",
            "usuario" => $this->tank_auth->get_username(),
            "precompromisocss" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/seleccion_imprimir_recaudado_view');
        $this->load->view('front/footer_main_view', array(
            "devengado" => TRUE,
            "tablas" => TRUE,
        ));
    }
    function imprimir_recaudado_formato() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha impreso el formato del recaudado');
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

        $datos = $this->input->post();
        $fecha = date("Y-m-d");
        $hora = date("H:i:s");
//        $this->debugeo->imprimir_pre($datos);

        $datos_detalle = $this->recaudacion_model->get_datos_filtros_recaudado($datos);
//        $this->debugeo->imprimir_pre($datos_detalle);

        $this->load->library('Pdf');

        $pdf = new Pdf('P', 'cm', 'Letter', true, 'UTF-8', false);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Armonniza');
        $pdf->SetTitle('Movimientos Recaudado');
        $pdf->SetSubject('Movimientos Recaudado');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
//        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
//        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        // remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
//        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
        $pdf->SetMargins(10, 10, 10);
//        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// ---------------------------------------------------------

// set font
        $pdf->SetFont('helvetica', '', 12);

// add a page
        $pdf->AddPage('L', 'Letter');

//        Aqui se inicianiza la tabla y se crea la fila superior con los títulos de la misma
        $tabla_datos = '<table style="font-size: small;  border: 1px solid #BDBDBD;" cellpadding="4">
                            <tr>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="6%">No.</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="12%">Clasificación</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="17%">Centro de Recaudación</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="11%">No. Factura</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="15%">Estructura Administrativa</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="9%">Fecha Solicitud</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="16%">Descripción</td>
                                <td align="center" style="font-weight: bold; border-bottom: 1px solid #BDBDBD;" width="14%">Importe</td>
                            </tr>';

        $importe = 0;

//        Este bucle lo que hace es recorrer cada una de las filas de la tabla de movimientos bancarios donde exista un registro de la cuenta seleccionada
        foreach($datos_detalle as $fila) {
            $estructura = json_decode($fila->estructura, TRUE);

//            Ya que estamos dentro del ciclo, recorremos cada elemento del arreglo y ponemos las variables en las celdas que les corresponde
            $tabla_datos .= '<tr>
                                    <td align="center" style="border-right: 1px solid #BDBDBD;" width="6%">'.$fila->numero.'</td>
                                    <td align="justify" style="border-right: 1px solid #BDBDBD;" width="12%">'.$fila->clasificacion.'</td>
                                    <td align="justify" style="border-right: 1px solid #BDBDBD;" width="17%">'.$fila->cliente.'</td>
                                    <td align="center" style="border-right: 1px solid #BDBDBD;" width="11%">'.$fila->no_movimiento.'</td>
                                    <td align="center" style="border-right: 1px solid #BDBDBD;" width="15%">'.$estructura["gerencia"].'&nbsp;&nbsp;'.$estructura["centro_de_recaudacion"].'&nbsp;&nbsp;'.$estructura["rubro"].'&nbsp;&nbsp;'.$estructura["tipo"].'&nbsp;&nbsp;'.$estructura["clase"].'</td>
                                    <td align="center" style="border-right: 1px solid #BDBDBD;" width="9%">'.$fila->fecha_aplicacion_detalle.'</td>
                                    <td align="justify" style="border-right: 1px solid #BDBDBD;" width="16%">'.$fila->descripcion_detalle.'</td>
                                    <td align="left" width="2%">$</td>
                                    <td align="right" width="12%">'.$this->cart->format_number($fila->total_importe).'</td>
                                </tr>';

//            Se suma el importe total a la variable que guarda el importe total
            $importe += $fila->total_importe;
        }

        $tabla_datos .= '</table>';

        $encabezado = '<table cellspacing="3" style="font-size: small;">
                        <tr>
                            <td align="left"><b>&nbsp; &nbsp; &nbsp;Período</b></td>
                            <td align="center"><b>Importe Total</b></td>
                            <td align="right"><b>Fecha Emisión</b></td>
                        </tr>
                        <tr>
                            <td align="left">&nbsp; &nbsp; &nbsp;'.$datos["fecha_inicial"].' al '.$datos["fecha_final"].'</td>
                            <td align="center"> $ '.$this->cart->format_number($importe).'</td>
                            <td align="right">'.$fecha.'</td>
                        </tr>
                       </table>';
        $tabla_titulo = '<table cellspacing="3" >
                            <tr>
                                <th align="left" width="8%"><img src="'.base_url("img/logo2.png").'" /></th>
                                <th align="center" width="91%" style="line-height: 7px;">
                                    <h3>EDUCAL, S.A. DE C.V.</h3>
                                    <h5>Movimientos Recaudado</h5>
                                    '.$encabezado.'
                                </th>
                            </tr>
                        </table>';



        $html = '
        <style>
            .cont-general{
		        border: 1px solid #eee;
		        border-radius: 1%;
		        margin: 2% 14%;
	        }
	        .cont-general2{
	            border: 1px solid #BDBDBD;
	        }
	        .cont-general .borde-inf{
                border-bottom: 1px solid #eee;
	        }
	        .cont-general .borde-sup{
                border-top: 1px solid #eee;
	        }
	        .cont-general .borde-der{
                border-right: 1px solid #eee;
	        }
	        .cont-general2 .borde-inf2{
                border-bottom: 1px solid #BDBDBD;
	        }
	        .cont-general2 .borde-der2{
                border-right: 1px solid #BDBDBD;
	        }

        </style>

        <table class="cont-general" border="0" cellspacing="3" style="font-size: small;">
           <tr><td class="borde-inf" width="100%">'.$tabla_titulo.'</td></tr>
           <tr><td width="100%"></td></tr>
           <tr><td width="100%">'.$tabla_datos.'</td></tr>
           <tr><td width="100%"></td></tr>
        </table>';

// output the HTML content

        $pdf->writeHTML($html, false, false, true, false, 'top');

//Close and output PDF document
        $pdf->Output('Movimientos Recaudado.pdf', 'I');
    }
    /**Esta función permite exportar Recaudados en General*/
    function exportar_recaudados() {
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Recaudado",
            "usuario" => $this->tank_auth->get_username(),
            "precompromisocss" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/seleccion_exportar_recaudado_view');
        $this->load->view('front/footer_main_view', array(
            "devengado" => TRUE,
            "tablas" => TRUE,
        ));
    }
    function exportar_recaudado_formato() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha exportado el formato de recaudado');
        $datos = $this->input->post();
        $this->load->library('excel');
        $fecha = date("Y-m-d");

        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Movimientos Recaudado');
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('Educal');
        $objDrawing->setPath('img/logo2.png');
        $objDrawing->setOffsetX(50);
        $objDrawing->setHeight(80);
        // $objDrawing->setWidth(10);
        $objDrawing->setCoordinates('A1');
        $objDrawing->setWorksheet($this->excel->getActiveSheet());

        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
            'font'  => array(
                'bold'  => true,
            )
        );
        $style_center = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );
        $style_left = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            )
        );

        //set cell A1 content with some text
        $this->excel->getActiveSheet()->getStyle("A")->applyFromArray($style_center);
        $this->excel->getActiveSheet()->setCellValue('A6', 'No.')->getColumnDimension("A")->setWidth(10);
        $this->excel->getActiveSheet()->getStyle("B:I")->applyFromArray($style_left);
        $this->excel->getActiveSheet()->setCellValue('B6', 'Clasificación')->getColumnDimension("B")->setWidth(25);
        $this->excel->getActiveSheet()->setCellValue('C6', 'Centro de Recaudación')->getColumnDimension("C")->setWidth(35);
        $this->excel->getActiveSheet()->setCellValue('D6', 'No. Factura')->getColumnDimension("D")->setWidth(10);
        $this->excel->getActiveSheet()->setCellValue('E6', 'Gerencia')->getColumnDimension("E")->setWidth(8);
        $this->excel->getActiveSheet()->setCellValue('F6', 'Centro de Recaudación')->getColumnDimension("F")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('G6', 'Rubro')->getColumnDimension("G")->setWidth(8);
        $this->excel->getActiveSheet()->setCellValue('H6', 'Tipo')->getColumnDimension("H")->setWidth(8);
        $this->excel->getActiveSheet()->setCellValue('I6', 'Clase')->getColumnDimension("I")->setWidth(8);
        $this->excel->getActiveSheet()->getStyle("J")->applyFromArray($style_center);
        $this->excel->getActiveSheet()->setCellValue('J6', 'Fecha Solicitud')->getColumnDimension("J")->setWidth(15);
        $this->excel->getActiveSheet()->getStyle("K:L")->applyFromArray($style_left);
        $this->excel->getActiveSheet()->setCellValue('K6', 'Descripción')->getColumnDimension("K")->setWidth(35);
        $this->excel->getActiveSheet()->setCellValue('L6', 'Importe')->getColumnDimension("L")->setWidth(25);


        $datos_detalle = $this->recaudacion_model->get_datos_filtros_recaudado($datos);
        $estructura = array();
        //$this->debugeo->imprimir_pre($resultado);
        $row = 7;
        $importe = 0;

//        $this->benchmark->mark('code_start');

        foreach($datos_detalle as $fila) {
            $estructura = json_decode($fila->estructura);

            $this->excel->getActiveSheet()->setCellValue('A'.$row, $fila->numero);
            $this->excel->getActiveSheet()->setCellValue('B'.$row, $fila->clasificacion);
            $this->excel->getActiveSheet()->setCellValue('C'.$row, $fila->cliente);
            $this->excel->getActiveSheet()->setCellValue('D'.$row, $fila->no_movimiento);
            $this->excel->getActiveSheet()->setCellValue('E'.$row, $estructura->gerencia);
            $this->excel->getActiveSheet()->setCellValue('F'.$row, $estructura->centro_de_recaudacion);
            $this->excel->getActiveSheet()->setCellValue('G'.$row, $estructura->rubro);
            $this->excel->getActiveSheet()->setCellValue('H'.$row, $estructura->tipo);
            $this->excel->getActiveSheet()->setCellValue('I'.$row, $estructura->clase);
            $this->excel->getActiveSheet()->setCellValue('J'.$row, $fila->fecha_aplicacion_detalle);
            $this->excel->getActiveSheet()->setCellValue('K'.$row, $fila->descripcion_detalle);
            $this->excel->getActiveSheet()->setCellValue('L'.$row, '$ '.$this->cart->format_number($fila->total_importe));

            $importe += $fila->total_importe;
            $row += 1;
        }

        $this->excel->getActiveSheet()->mergeCells('A1:L1')->getStyle("A1")->applyFromArray($style);
        $this->excel->getActiveSheet()->setCellValue('A1','EDUCAL, S.A. DE C.V.');
        $this->excel->getActiveSheet()->mergeCells('A2:L2')->getStyle("A2")->applyFromArray($style);
        $this->excel->getActiveSheet()->setCellValue('A2', 'Movimientos Recaudado');
        $this->excel->getActiveSheet()->getStyle("C3:K3")->applyFromArray($style);
        $this->excel->getActiveSheet()->getStyle("C4:K4")->applyFromArray($style_center);
        $this->excel->getActiveSheet()->mergeCells('F3:H3');
        $this->excel->getActiveSheet()->mergeCells('F4:H4');
        $this->excel->getActiveSheet()->setCellValue('C3', 'Período');
        $this->excel->getActiveSheet()->setCellValue('F3', 'Importe Total');
        $this->excel->getActiveSheet()->setCellValue('K3', 'Fecha Emisión');
        $this->excel->getActiveSheet()->setCellValue('C4', $datos["fecha_inicial"] .' al '. $datos["fecha_final"]);
        $this->excel->getActiveSheet()->setCellValue('F4', '$ '.$this->cart->format_number($importe));
        $this->excel->getActiveSheet()->setCellValue('K4', $fecha);
        $this->excel->getActiveSheet()->getStyle("A6:L6")->applyFromArray($style);
        $this->excel->getActiveSheet()->mergeCells('A5:L5');

//        $this->benchmark->mark('code_end');

//        echo $this->benchmark->elapsed_time('code_start', 'code_end');

        $filename="Movimientos Recaudado.xls"; // Agregar fecha en que se generó
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
//        save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
//        if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
//        force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }


    /** === Estructuras === */
    /** Estructura Ingresos */
    private function input_niveles_ingresos() {
//        Se llama la funcion del modelo de recaudación encargado de contar los niveles que existen
        $total_ingresos = $this->recaudacion_model->contar_ingresos_elementos();
//        Se llama a la funcion del model de recaudación encargada de tomar los nombres delos niveles que existen
        $nombres_ingresos = $this->recaudacion_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_ingresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

        $niveles = '';
        $num = 1;

        for($i = 0; $i < $total_ingresos->conteo; $i++){
            $nom_niv = ucwords(strtolower($nombre[$i]));
            $nom_niv1 = str_replace( "De" , "de" , $nom_niv);
            $nom_niv2 = str_replace( "Recaudacion" , "Recaudación" , $nom_niv1);
            $niveles .= '
                                <!-- <h5>Nivel '.$num.'</h5>
                                <label>'.$nombre[$i].'</label>-->
                                <input type="text" class="form-control" id="input_nivel'.$num.'" placeholder="'.$nom_niv2.'" >
                        ';
            $num++;
        }
        return $niveles;
    }
    private function preparar_estructura_ingresos() {
//        Se llama la funcion del modelo de ingresos encargado de contar los niveles que existen
        $total_ingresos = $this->recaudacion_model->contar_ingresos_elementos();
//        Se llama a la funcion del model de ingresos encargada de tomar los nombres delos niveles que existen
        $nombres_ingresos = $this->recaudacion_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_ingresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

        $nombre_primerNivel = $this->recaudacion_model->datos_primerNivel($nombre[0]);

//        $this->debugeo->imprimir_pre($nombre_primerNivel);

        $niveles = '<div class="row" id="ingresos_estrucura_superior">
                        <div class="col-lg-4">
                            <form role="form">
                                <div class="form-group" id="forma_nivel1">
                                    <h5>Nivel 1</h5>
                                    <label>'.$nombre[0].'</label>
                                    <input type="text" class="form-control" id="buscar_nivel1" placeholder="Buscar...">
                                    <select multiple class="form-control formas" id="select_nivel1">';
        foreach($nombre_primerNivel as $fila) {
            $niveles .= '<option value="'.$fila->gerencia.'" title="'.$fila->gerencia.' - '.$fila->descripcion.'">'.$fila->gerencia.' - '.$fila->descripcion.'</option>';
        }

        $niveles .= '                </select>
                                </div>
                            </form>
                        </div>';

        $a = 1;
        $num = 2;

        for($i = 1; $i < $total_ingresos->conteo; $i++){
            $niveles .= '<div class="col-lg-4">
                            <form role="form">
                                <div class="form-group" id="forma_nivel'.$num.'">
                                     <h5>Nivel '.$num.'</h5>
                                    <label>'.$nombre[$a].'</label>
                                    <input type="text" class="form-control" id="buscar_nivel'.$num.'" placeholder="Buscar..." disabled>
                                    <select multiple class="form-control formas" id="select_nivel'.$num.'">
                                    </select>
                                </div>
                            </form>
                        </div>';
            $a++;
            $num++;
        }

        $niveles .= '</div>';

        return $niveles;
    }

    private function _revisar_cuenta($arreglo_cuenta) {
        if(isset($arreglo_cuenta->id_padre))
            return TRUE;
        return false;
    }



    function acomodar_recaudados() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha acomodado recaudados');
        $this->db->select('*')->from('tabla_temporal_recaudado');
        $query = $this->db->get();
        foreach($query->result_array() as $key => $value) {

            $value["devengado"] = $value["numero_devengado"];

            $recaudado = $this->tomar_datos_Devengado_archivo($value);

            $this->debugeo->imprimir_pre($recaudado);

            $recaudado["no_devengado"] = $recaudado["numero"];

            $resultado = $this->insertar_recaudado_acomodado($recaudado);


        }

    }

    private function tomar_datos_Devengado_archivo($datos) {
        $last = 0;
//      Se toma el numero del ultimo devengado
        $ultimo = $this->recaudacion_model->ultimo_recaudado();
        if($ultimo) {
            $last = $ultimo->ultimo + 1;
        }
        else {
            $last = 1;
        }
        $this->recaudacion_model->apartarRecaudado($last);

//        Se inicializa el arreglo que va a contener los mensajes al usuario y el contenido del precompromiso
        $datos_devengado = array();

//        Se toman los numeros del compromiso y precompromiso
        $recaudado = $last;
        $devengado = $datos["devengado"];

//        Se llama a la función para copiar el detalle del precompromiso al detalle del compromiso
//        La cual devuelve los mensajes de exito o error al calcular el presupuesto disponible
        $resultado = $this->recaudacion_model->copiarDatosDevengadoArchivo($devengado, $recaudado, $datos);
//        $this->debugeo->imprimir_pre($resultado);

//        Se toman los datos de la caratula del precompromiso
        $datos_devengado = $this->recaudacion_model->datos_Caratula_Devengado_Recaudado_Acomodado($devengado);

        $datos_devengado["ultimo"] = $recaudado;

        return $datos_devengado;
//        echo(json_encode($datos_devengado));
    }

    private function insertar_recaudado_acomodado($datos = NULL) {

        $datos["check_firme"] = 1;

//            Se pasa el estatus del compromiso a "activo"
        $datos["estatus"] = "Activo";

//            Se toma el total del compromiso, y el total del precompromiso asociado para actualizar los saldos
        $this->db->select('importe_total, importe_total_recaudado')->from('mov_devengado_caratula')->where('numero', $datos["no_devengado"]);
        $query = $this->db->get();
        $total_devengado = $query->row();

//            Se suma el total del importe del compromiso, al total que actualmente está en el precompromiso
        $total = round($datos["importe_total"], 2) + round($total_devengado->importe_total_recaudado, 2);

//            Se llama a la función que se encarga de actualizar el saldo de las partidas del compromiso
        $respuesta = $this->recaudacion_model->actualizar_saldo_recaudado($datos);

        $query_actualizar = "UPDATE mov_devengado_caratula SET importe_total_recaudado = ? WHERE numero = ?;";
        $query_final = $this->db->query($query_actualizar, array($total, $datos["no_devengado"]));

        $datos["fecha_recaudado"] = $datos["fecha_solicitud"];
        $datos["devengado_hidden"] = $datos["numero"];
        $datos["no_factura"] = $datos["no_movimiento"];

//        Se insertan los datos de la caratula del compromiso
        $resultado_insertar = $this->recaudacion_model->insertar_caratula_recaudacion($datos);

//        Si el resultado es exitoso, se le indica al usuario
        if($resultado_insertar) {
            return TRUE;
        }
//        De lo contrario, se manda un error al usuario
        else {
            return FALSE;
        }

    }

    function generar_polizas_devengado() {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha generado polizas de devengado');
        $this->db->select('numero')->from('mov_devengado_caratula')->where('poliza', 0)->where('cancelada !=', 1)->where('enfirme', 1);
        $query = $this->db->get();
        foreach($query->result_array() as $key => $value) {
//            $this->debugeo->imprimir_pre($value);
            $poliza = $this->generar_poliza_devengado_atomatico($value["numero"]);
            if($poliza != "exito") {
                $this->debugeo->imprimir_pre($value);
                $this->debugeo->imprimir_pre($poliza);
            }
        }
    }

    function generar_polizas_recaudado() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha generado pólizas de recaudado');
        $this->db->select('numero')->from('mov_recaudado_caratula')->where('poliza', 0)->where('cancelada !=', 1)->where('enfirme', 1);
        $query = $this->db->get();
        foreach($query->result_array() as $key => $value) {
//            $this->debugeo->imprimir_pre($value);
            $poliza = $this->generar_poliza_recaudado_automatico($value["numero"]);
            if($poliza != "exito") {
                $this->debugeo->imprimir_pre($value);
                $this->debugeo->imprimir_pre($poliza);
            }
        }
    }

    private function generar_poliza_devengado_atomatico($devengado) {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

        $respuesta = array(
            "mensaje" => "",
        );

        try {
            $id = $this->tank_auth->get_user_id();

            $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
            $resultado_query_usuario = $this->db->query($query_usuario, array($id));
            $nombre_encontrado = $resultado_query_usuario->row();
            $nombre_completo = $nombre_encontrado->nombre." ".$nombre_encontrado->apellido_paterno." ".$nombre_encontrado->apellido_materno;

//        Se inicizaliza la variable con el ultimo valor de las polizas
            $last = 0;
//        Se toma el numero de la ultima poliza
            $ultimo = $this->ciclo_model->ultima_poliza();

//        Se le suma uno al ultimo valor de las polizas
            if($ultimo) {
                $last = $ultimo->ultimo + 1;
            }
//        De lo contrario se inicia en 1
            else {
                $last = 1;
            }

//        Se prepara el query para llamar todos los datos de la caratula del contrarecibo
            $query_caratula = "SELECT * FROM mov_devengado_caratula WHERE numero = ?;";

//        Se llama a la función que se encarga de tomar todos los datos de la caratula del contrarecibo
            $datos_caratula_devengado = $this->recaudacion_model->get_datos_devengado_caratula($devengado, $query_caratula);

//            $this->debugeo->imprimir_pre($datos_caratula_devengado);

//        Se prepara el query para tomar los datos del detalle de compromiso que están ligados al contrarecibo
            $query_detalle = "SELECT *, COLUMN_JSON(nivel) AS estructura FROM mov_devengado_detalle WHERE numero_devengado = ?;";

//        Se llama ala funcion que se encarga de tomar todos los datos del detalle del compromiso que está ligado con el contrarecibo
            $datos_detalle = $this->recaudacion_model->get_datos_devengadoDetalle($datos_caratula_devengado->numero, $query_detalle);

//            $this->debugeo->imprimir_pre($datos_detalle);

//        Se crea un arreglo donde se van a guardar los resultados del detalle del compromiso
            $datos_detalle_poliza = array();

            if (!$datos_detalle) {
                throw new Exception('No hay partidas dentro del compromiso.');
            }

            $centro_costo = '';
            $partida = '';
            $nombre_fuente_financiamiento = '';
            $total_filas = 0;
            $cargos = 0;
            $abonos = 0;
            $resultado_insertar_caratula_query = FALSE;
            $resultado_insertar_abono_query = FALSE;
            $resultado_insertar_cargo_query = FALSE;

            $this->db->trans_begin();

            $query_insertar_caratula_poliza = "INSERT INTO mov_polizas_cabecera (numero_poliza, tipo_poliza, fecha, fecha_real, concepto, enfirme, sifirme, no_partidas, importe, cancelada, estatus, creado_por, autorizada, cargos, abonos, no_devengado, clave_cliente, cliente, no_movimiento) VALUES (?, ?, ?, NOW(), ?, 0, 0, ?, ?, 0, ?, ?, 0, ?, ?, ?, ?, ?, ?);";
            $datos_caratula_poliza = array(
                $last,
                'Diario',
                $datos_caratula_devengado->fecha_solicitud,
                $datos_caratula_devengado->descripcion,
                0, //$total_filas
                $datos_caratula_devengado->importe_total,
                'espera',
                $nombre_completo,
                0, //$cargos
                0, //$abonos
                $datos_caratula_devengado->numero,
                $datos_caratula_devengado->clave_cliente,
                $datos_caratula_devengado->cliente,
                $datos_caratula_devengado->no_movimiento,
            );

            $resultado_insertar_caratula_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_caratula_poliza, $datos_caratula_poliza);

            $datos_actualizar_poliza = array(
                'poliza' => 1,
            );

            $this->db->where('id_devengado', $devengado);
            $this->db->update('mov_devengado_caratula', $datos_actualizar_poliza);

            foreach ($datos_detalle as $row) {
                $estructura = json_decode($row->estructura);

//                $this->debugeo->imprimir_pre($row);
//                $this->debugeo->imprimir_pre($estructura);

                $datos_cuentas = $this->recaudacion_model->tomar_cuentas_devengado($estructura->centro_de_recaudacion);

//                $this->debugeo->imprimir_pre($datos_cuentas);

                if (!$datos_cuentas) {
                    throw new Exception('No existe cuenta contable asociada a la partida.');
                }

//                $this->debugeo->imprimir_pre($datos_cuentas);

                foreach($datos_cuentas as $cuenta){
                    $check = substr($estructura->clase, -1, 1);

//                    $this->debugeo->imprimir_pre($row);
                    if($check == 2)
                    {
                        if($cuenta->cuenta_cargo) {

                            if (strpos(strtolower($cuenta->nombre_cargo),"servicio") !== FALSE ) {
//                            $this->debugeo->imprimir_pre($cuenta);
                                $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_cargo = $this->db->query($sql_cargo, array($cuenta->cuenta_cargo));
                                $resultado_cuenta_cargo = $query_cargo->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_cargo);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_cargo.' '.$cuenta->nombre_cargo);
                                }

                                $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_cargo = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_cargo,
                                    $resultado_cuenta_cargo->id_padre,
                                    $resultado_cuenta_cargo->nivel,
                                    $datos_caratula_devengado->descripcion,
                                    $row->descuento,
                                    0.0,
                                    "Recursos Fiscales",
                                    $cuenta->nombre_cargo,
                                    $estructura->gerencia,
                                    $estructura->centro_de_recaudacion,
                                );

                                $resultado_insertar_cargo_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                $cargos += $row->descuento;

                                $total_filas += 1;
                            } elseif (strpos(strtolower($cuenta->nombre_cargo), "presupuesto") !== FALSE) {
                                $_cantidad = ($row->importe - $row->descuento) + $row->iva;
                                $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_cargo = $this->db->query($sql_cargo, array($cuenta->cuenta_cargo));
                                $resultado_cuenta_cargo = $query_cargo->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_cargo);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_cargo.' '.$cuenta->nombre_cargo);
                                }

                                $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida, concepto_especifico) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_cargo = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_cargo,
                                    $resultado_cuenta_cargo->id_padre,
                                    $resultado_cuenta_cargo->nivel,
                                    $datos_caratula_devengado->descripcion,
                                    $_cantidad,
                                    0.0,
                                    "Recursos Fiscale",
                                    $cuenta->nombre_cargo,
                                    $estructura->gerencia,
                                    $estructura->centro_de_recaudacion,
                                    0,
                                );

                                $resultado_insertar_cargo_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                $cargos += $_cantidad;

                                $total_filas += 1;
                            } elseif (strpos(strtolower($cuenta->nombre_cargo), "i.v.a.") !== FALSE || strpos(strtolower($cuenta->nombre_cargo), "i.v.a") !== FALSE || strpos(strtolower($cuenta->nombre_abono), "iva") !== FALSE) {
                                if ($row->iva !== 0) {
                                    $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                    $query_cargo = $this->db->query($sql_cargo, array($cuenta->cuenta_cargo));
                                    $resultado_cuenta_cargo = $query_cargo->row();

                                    $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_cargo);

                                    if ($existe_cuenta == FALSE) {
                                        throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_cargo.' '.$cuenta->nombre_cargo);
                                    }

                                    $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida, concepto_especifico) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                    $datos_cargo = array(
                                        $last,
                                        'Diario',
                                        $cuenta->id_correlacion_partidas_contables,
                                        $cuenta->cuenta_cargo,
                                        $resultado_cuenta_cargo->id_padre,
                                        $resultado_cuenta_cargo->nivel,
                                        $datos_caratula_devengado->descripcion,
                                        $row->iva,
                                        0.0,
                                        "Recursos Fiscale",
                                        $cuenta->nombre_cargo,
                                        $estructura->gerencia,
                                        $estructura->centro_de_recaudacion,
                                        0,
                                    );

                                    $resultado_insertar_cargo_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                    $cargos += $row->iva;

                                    $total_filas += 1;
                                }
                            } elseif (strpos(strtolower($cuenta->nombre_cargo), "descuentos") !== FALSE) {

                            }
                            else {
                                $_cantidad = ($row->importe - $row->descuento) + $row->iva;

                                $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_cargo = $this->db->query($sql_cargo, array($cuenta->cuenta_cargo));
                                $resultado_cuenta_cargo = $query_cargo->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_cargo);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_cargo.' '.$cuenta->nombre_cargo);
                                }

                                $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_cargo = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_cargo,
                                    $resultado_cuenta_cargo->id_padre,
                                    $resultado_cuenta_cargo->nivel,
                                    $datos_caratula_devengado->descripcion,
                                    $_cantidad,
                                    0.0,
                                    "Recursos Fiscales",
                                    $cuenta->nombre_cargo,
                                    $estructura->gerencia,
                                    $estructura->centro_de_recaudacion,
                                );

                                $resultado_insertar_cargo_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                $cargos += $_cantidad;

                                $total_filas += 1;
                            }

                        }

                        if($cuenta->cuenta_abono) {

                            if (strpos(strtolower($cuenta->nombre_abono),"servicio") !== FALSE ) {
//                            $this->debugeo->imprimir_pre($cuenta);
                                $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_abono = $this->db->query($sql_abono, array($cuenta->cuenta_abono));
                                $resultado_cuenta_abono = $query_abono->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_abono);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_abono.' '.$cuenta->nombre_abono);
                                }

                                $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_abono = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_abono,
                                    $resultado_cuenta_abono->id_padre,
                                    $resultado_cuenta_abono->nivel,
                                    $datos_caratula_devengado->descripcion,
                                    0.0,
                                    $row->importe,
                                    "Recursos Fiscales",
                                    $cuenta->nombre_abono,
                                    $estructura->gerencia,
                                    $estructura->centro_de_recaudacion,
                                );

                                $resultado_insertar_abono_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                $abonos += $row->importe;

                                $total_filas += 1;

                            } elseif (strpos(strtolower($cuenta->nombre_abono), "i.v.a.") !== FALSE || strpos(strtolower($cuenta->nombre_abono), "i.v.a") !== FALSE || strpos(strtolower($cuenta->nombre_abono), "iva") !== FALSE) {
                                if ($row->iva !== 0) {
                                    $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                    $query_abono = $this->db->query($sql_abono, array($cuenta->cuenta_abono));
                                    $resultado_cuenta_abono = $query_abono->row();

                                    $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_abono);

                                    if ($existe_cuenta == FALSE) {
                                        throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_abono.' '.$cuenta->nombre_abono);
                                    }

                                    $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida, concepto_especifico) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                    $datos_abono = array(
                                        $last,
                                        'Diario',
                                        $cuenta->id_correlacion_partidas_contables,
                                        $cuenta->cuenta_abono,
                                        $resultado_cuenta_abono->id_padre,
                                        $resultado_cuenta_abono->nivel,
                                        $datos_caratula_devengado->descripcion,
                                        0.0,
                                        $row->iva,
                                        "Recursos Fiscales",
                                        $cuenta->nombre_abono,
                                        $estructura->gerencia,
                                        $estructura->centro_de_recaudacion,
                                        0,
                                    );

                                    $resultado_insertar_abono_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                    $abonos += $row->iva;

                                    $total_filas += 1;
                                }
                            } elseif (strpos(strtolower($cuenta->nombre_abono), "presupuesto") !== FALSE) {
                                $_cantidad = ($row->importe - $row->descuento) + $row->iva;
                                $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_abono = $this->db->query($sql_abono, array($cuenta->cuenta_abono));
                                $resultado_cuenta_abono = $query_abono->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_abono);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_abono.' '.$cuenta->nombre_abono);
                                }

                                $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida, concepto_especifico) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_abono = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_abono,
                                    $resultado_cuenta_abono->id_padre,
                                    $resultado_cuenta_abono->nivel,
                                    $datos_caratula_devengado->descripcion,
                                    0.0,
                                    $_cantidad,
                                    "Recursos Fiscale",
                                    $cuenta->nombre_abono,
                                    $estructura->gerencia,
                                    $estructura->centro_de_recaudacion,
                                    0,
                                );

                                $resultado_insertar_abono_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                $abonos += $_cantidad;

                                $total_filas += 1;
                            } elseif (strpos(strtolower($cuenta->nombre_abono), "ventas") !== FALSE) {

                            } else {
                                $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_abono = $this->db->query($sql_abono, array($cuenta->cuenta_abono));
                                $resultado_cuenta_abono = $query_abono->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_abono);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_abono.' '.$cuenta->nombre_abono);
                                }

                                $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_abono = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_abono,
                                    $resultado_cuenta_abono->id_padre,
                                    $resultado_cuenta_abono->nivel,
                                    $datos_caratula_devengado->descripcion,
                                    0.0,
                                    $row->total_importe,
                                    "Recursos Fiscales",
                                    $cuenta->nombre_abono,
                                    $estructura->gerencia,
                                    $estructura->centro_de_recaudacion,
                                );

                                $resultado_insertar_abono_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                $abonos += $row->total_importe;

                                $total_filas += 1;
                            }

                        }
                    }
                    else {
                        if($cuenta->cuenta_cargo) {
                            if (strpos(strtolower($cuenta->nombre_cargo),"servicios") !== FALSE ) {

                            } elseif (strpos(strtolower($cuenta->nombre_cargo),"descuentos") !== FALSE ) {
//                            $this->debugeo->imprimir_pre($cuenta);
                                $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_cargo = $this->db->query($sql_cargo, array($cuenta->cuenta_cargo));
                                $resultado_cuenta_cargo = $query_cargo->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_cargo);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_cargo.' '.$cuenta->nombre_cargo);
                                }

                                $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_cargo = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_cargo,
                                    $resultado_cuenta_cargo->id_padre,
                                    $resultado_cuenta_cargo->nivel,
                                    $datos_caratula_devengado->descripcion,
                                    $row->descuento,
                                    0.0,
                                    "Recursos Fiscales",
                                    $cuenta->nombre_cargo,
                                    $estructura->gerencia,
                                    $estructura->centro_de_recaudacion,
                                );

                                $resultado_insertar_cargo_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                $cargos += $row->descuento;

                                $total_filas += 1;
                            } elseif (strpos(strtolower($cuenta->nombre_cargo), "presupuesto") !== FALSE) {
                                $_cantidad = ($row->importe - $row->descuento) + $row->iva;
                                $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_cargo = $this->db->query($sql_cargo, array($cuenta->cuenta_cargo));
                                $resultado_cuenta_cargo = $query_cargo->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_cargo);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_cargo.' '.$cuenta->nombre_cargo);
                                }

                                $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida, concepto_especifico) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_cargo = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_cargo,
                                    $resultado_cuenta_cargo->id_padre,
                                    $resultado_cuenta_cargo->nivel,
                                    $datos_caratula_devengado->descripcion,
                                    $_cantidad,
                                    0.0,
                                    "Recursos Fiscale",
                                    $cuenta->nombre_cargo,
                                    $estructura->gerencia,
                                    $estructura->centro_de_recaudacion,
                                    0,
                                );

                                $resultado_insertar_cargo_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                $cargos += $_cantidad;

                                $total_filas += 1;
                            } elseif (strpos(strtolower($cuenta->nombre_cargo), "i.v.a.") !== FALSE || strpos(strtolower($cuenta->nombre_cargo), "i.v.a") !== FALSE || strpos(strtolower($cuenta->nombre_abono), "iva") !== FALSE) {
                                if ($row->iva !== 0) {
                                    $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                    $query_cargo = $this->db->query($sql_cargo, array($cuenta->cuenta_cargo));
                                    $resultado_cuenta_cargo = $query_cargo->row();

                                    $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_cargo);

                                    if ($existe_cuenta == FALSE) {
                                        throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_cargo.' '.$cuenta->nombre_cargo);
                                    }

                                    $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida, concepto_especifico) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                    $datos_cargo = array(
                                        $last,
                                        'Diario',
                                        $cuenta->id_correlacion_partidas_contables,
                                        $cuenta->cuenta_cargo,
                                        $resultado_cuenta_cargo->id_padre,
                                        $resultado_cuenta_cargo->nivel,
                                        $datos_caratula_devengado->descripcion,
                                        $row->iva,
                                        0.0,
                                        "Recursos Fiscale",
                                        $cuenta->nombre_cargo,
                                        $estructura->gerencia,
                                        $estructura->centro_de_recaudacion,
                                        0,
                                    );

                                    $resultado_insertar_cargo_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                    $cargos += $row->iva;

                                    $total_filas += 1;
                                }
                            } else {
                                $_cantidad = ($row->importe - $row->descuento) + $row->iva;

                                $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_cargo = $this->db->query($sql_cargo, array($cuenta->cuenta_cargo));
                                $resultado_cuenta_cargo = $query_cargo->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_cargo);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_cargo.' '.$cuenta->nombre_cargo);
                                }

                                $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_cargo = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_cargo,
                                    $resultado_cuenta_cargo->id_padre,
                                    $resultado_cuenta_cargo->nivel,
                                    $datos_caratula_devengado->descripcion,
                                    $_cantidad,
                                    0.0,
                                    "Recursos Fiscales",
                                    $cuenta->nombre_cargo,
                                    $estructura->gerencia,
                                    $estructura->centro_de_recaudacion,
                                );

                                $resultado_insertar_cargo_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                $cargos += $_cantidad;

                                $total_filas += 1;
                            }

                        }

                        if($cuenta->cuenta_abono) {

                            if (strpos(strtolower($cuenta->nombre_abono),"servicios") !== FALSE ) {

                            } elseif(strpos(strtolower($cuenta->nombre_abono),"ventas") !== FALSE ) {
//                            $this->debugeo->imprimir_pre($cuenta);
                                $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_abono = $this->db->query($sql_abono, array($cuenta->cuenta_abono));
                                $resultado_cuenta_abono = $query_abono->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_abono);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_abono.' '.$cuenta->nombre_abono);
                                }

                                $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_abono = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_abono,
                                    $resultado_cuenta_abono->id_padre,
                                    $resultado_cuenta_abono->nivel,
                                    $datos_caratula_devengado->descripcion,
                                    0.0,
                                    $row->importe,
                                    "Recursos Fiscales",
                                    $cuenta->nombre_abono,
                                    $estructura->gerencia,
                                    $estructura->centro_de_recaudacion,
                                );

                                $resultado_insertar_abono_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                $abonos += $row->importe;

                                $total_filas += 1;

                            } elseif (strpos(strtolower($cuenta->nombre_abono), "i.v.a.") !== FALSE || strpos(strtolower($cuenta->nombre_abono), "i.v.a") !== FALSE || strpos(strtolower($cuenta->nombre_abono), "iva") !== FALSE) {
                                if ($row->iva !== 0) {
                                    $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                    $query_abono = $this->db->query($sql_abono, array($cuenta->cuenta_abono));
                                    $resultado_cuenta_abono = $query_abono->row();

                                    $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_abono);

                                    if ($existe_cuenta == FALSE) {
                                        throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_abono.' '.$cuenta->nombre_abono);
                                    }

                                    $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida, concepto_especifico) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                    $datos_abono = array(
                                        $last,
                                        'Diario',
                                        $cuenta->id_correlacion_partidas_contables,
                                        $cuenta->cuenta_abono,
                                        $resultado_cuenta_abono->id_padre,
                                        $resultado_cuenta_abono->nivel,
                                        $datos_caratula_devengado->descripcion,
                                        0.0,
                                        $row->iva,
                                        "Recursos Fiscales",
                                        $cuenta->nombre_abono,
                                        $estructura->gerencia,
                                        $estructura->centro_de_recaudacion,
                                        0,
                                    );

                                    $resultado_insertar_abono_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                    $abonos += $row->iva;

                                    $total_filas += 1;
                                }
                            } elseif (strpos(strtolower($cuenta->nombre_abono), "presupuesto") !== FALSE) {
                                $_cantidad = ($row->importe - $row->descuento) + $row->iva;
                                $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_abono = $this->db->query($sql_abono, array($cuenta->cuenta_abono));
                                $resultado_cuenta_abono = $query_abono->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_abono);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_abono.' '.$cuenta->nombre_abono);
                                }

                                $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida, concepto_especifico) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_abono = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_abono,
                                    $resultado_cuenta_abono->id_padre,
                                    $resultado_cuenta_abono->nivel,
                                    $datos_caratula_devengado->descripcion,
                                    0.0,
                                    $_cantidad,
                                    "Recursos Fiscale",
                                    $cuenta->nombre_abono,
                                    $estructura->gerencia,
                                    $estructura->centro_de_recaudacion,
                                    0,
                                );

                                $resultado_insertar_abono_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                $abonos += $_cantidad;

                                $total_filas += 1;
                            } else {
                                $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_abono = $this->db->query($sql_abono, array($cuenta->cuenta_abono));
                                $resultado_cuenta_abono = $query_abono->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_abono);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_abono.' '.$cuenta->nombre_abono);
                                }

                                $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_abono = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_abono,
                                    $resultado_cuenta_abono->id_padre,
                                    $resultado_cuenta_abono->nivel,
                                    $datos_caratula_devengado->descripcion,
                                    0.0,
                                    $row->total_importe,
                                    "Recursos Fiscales",
                                    $cuenta->nombre_abono,
                                    $estructura->gerencia,
                                    $estructura->centro_de_recaudacion,
                                );

                                $resultado_insertar_abono_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                $abonos += $row->total_importe;

                                $total_filas += 1;
                            }

                        }
                    }

                }

            }

            $datos_actualizar_caratula = array(
                'no_partidas' => $total_filas,
                'cargos' => $cargos,
                'abonos' => $abonos,
            );

            $this->db->where('numero_poliza', $last);
            $this->db->update('mov_polizas_cabecera', $datos_actualizar_caratula);

            $datos_actualizar_devengado = array(
                'poliza' => 1,
            );

            $this->db->where('numero', $datos_caratula_devengado->numero);
            $this->db->update('mov_devengado_caratula', $datos_actualizar_devengado);

            $this->db->trans_commit();

            return "exito";

        } catch (Exception $e) {
            $this->db->trans_rollback();

            $this->db->where('numero_poliza', $last);
            $this->db->delete('mov_poliza_detalle');

            $this->db->where('numero_poliza', $last);
            $this->db->delete('mov_polizas_cabecera');

            return $e->getMessage();
        }
    }

    private function generar_poliza_recaudado_automatico($recaudado = NULL) {
        $respuesta = array(
            "mensaje" => "",
        );

        try {
            $id = $this->tank_auth->get_user_id();

            $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
            $resultado_query_usuario = $this->db->query($query_usuario, array($id));
            $nombre_encontrado = $resultado_query_usuario->row();
            $nombre_completo = $nombre_encontrado->nombre." ".$nombre_encontrado->apellido_paterno." ".$nombre_encontrado->apellido_materno;

//        Se inicizaliza la variable con el ultimo valor de las polizas
            $last = 0;
//        Se toma el numero de la ultima poliza
            $ultimo = $this->ciclo_model->ultima_poliza();

//        Se le suma uno al ultimo valor de las polizas
            if($ultimo) {
                $last = $ultimo->ultimo + 1;
            }
//        De lo contrario se inicia en 1
            else {
                $last = 1;
            }

            $query = "SELECT * FROM mov_recaudado_caratula WHERE numero = ?";
            $resultado = $this->recaudacion_model->get_datos_devengado_caratula($recaudado, $query);

//            $this->debugeo->imprimir_pre($resultado);

            $resultado_detalle = $this->recaudacion_model->get_datos_recaudado_detalle_automatico($recaudado);
            $devengado = $resultado->numero_devengado;

//            $this->debugeo->imprimir_pre($resultado_detalle);

//        Se crea un arreglo donde se van a guardar los resultados del detalle del compromiso
            $datos_detalle_poliza = array();

            if (!$resultado_detalle) {
                throw new Exception('No hay movimientos dentro del devengado.');
            }

            $centro_recaudacion = '';
            $clase = '';
            $nombre_fuente_financiamiento = '';
            $total_filas = 0;
            $cargos = 0;
            $abonos = 0;

            $this->db->trans_begin();

            $this->db->select('numero_poliza')->from('mov_polizas_cabecera')->where('no_devengado', $devengado);
            $query_numero_devengado_diario = $this->db->get();
            $numero_devengado_diario = $query_numero_devengado_diario->row_array();

            $query_insertar_caratula_poliza = "INSERT INTO mov_polizas_cabecera (numero_poliza, tipo_poliza, fecha, fecha_real, concepto, enfirme, sifirme, no_partidas, importe, cancelada, estatus, creado_por, autorizada, cargos, abonos, no_devengado, no_recaudado, concepto_especifico, clave_cliente, cliente, no_movimiento, poliza_devengado) VALUES (?, ?, ?, NOW(), ?, 0, 0, ?, ?, 0, ?, ?, 0, ?, ?, ?, ?, ?, ?, ?, ? ,?);";
            $datos_caratula_poliza = array(
                $last,
                'Ingresos',
                $resultado->fecha_solicitud,
                $resultado->descripcion,
                0, //$total_filas
                $resultado->importe_total,
                'espera',
                $nombre_completo,
                0, //$cargos
                0, //$abonos
                $devengado,
                $recaudado,
                $resultado->descripcion,
                $resultado->clave_cliente,
                $resultado->cliente,
                $resultado->no_movimiento,
                $numero_devengado_diario["numero_poliza"],
            );

            $resultado_insertar_caratula = $this->ciclo_model->insertar_detalle_poliza($query_insertar_caratula_poliza, $datos_caratula_poliza);

            foreach ($resultado_detalle as $row) {
//                $this->debugeo->imprimir_pre($row);

                $estructura = json_decode($row->estructura);

//                $this->debugeo->imprimir_pre($estructura);

                $centro_recaudacion = $estructura->centro_de_recaudacion;
                $clase = $estructura->clase;

                $datos_cuentas = $this->recaudacion_model->tomar_cuentas_recaudacion($centro_recaudacion);

                foreach ($datos_cuentas as $cuenta) {

                    if (!$cuenta) {
                        throw new Exception('No existe cuenta contable asociada a la partida.');
                    }

//                    $this->debugeo->imprimir_pre($cuenta);

                    if ($cuenta->cuenta_cargo) {
                        if (strpos(strtolower($cuenta->nombre_cargo), "i.v.a.") !== FALSE || strpos(strtolower($cuenta->nombre_cargo), "i.v.a") !== FALSE || strpos(strtolower($cuenta->nombre_abono), "iva") !== FALSE) {
                            if ($row->iva !== 0) {
                                $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_cargo = $this->db->query($sql_cargo, array($cuenta->cuenta_cargo));
                                $resultado_cuenta_cargo = $query_cargo->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_cargo);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_cargo.' '.$cuenta->nombre_cargo);
                                }

                                $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida, concepto_especifico) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_cargo = array(
                                    $last,
                                    'Ingresos',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_cargo,
                                    $resultado_cuenta_cargo->id_padre,
                                    $resultado_cuenta_cargo->nivel,
                                    $resultado->descripcion,
                                    $row->iva,
                                    0.0,
                                    4,
                                    $cuenta->nombre_cargo,
                                    $centro_recaudacion,
                                    $clase,
                                    0,
                                );

                                $resultado_insertar_cargo_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                $cargos += $row->iva;

                                $total_filas += 1;
                            }
                        }
                        else {
                            $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                            $query_cargo = $this->db->query($sql_cargo, array($cuenta->cuenta_cargo));
                            $resultado_cuenta_cargo = $query_cargo->row();

                            $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_cargo);

                            if ($existe_cuenta == FALSE) {
                                throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_cargo.' '.$cuenta->nombre_cargo);
                            }

                            $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida, concepto_especifico) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                            $datos_cargo = array(
                                $last,
                                'Ingresos',
                                $cuenta->id_correlacion_partidas_contables,
                                $cuenta->cuenta_cargo,
                                $resultado_cuenta_cargo->id_padre,
                                $resultado_cuenta_cargo->nivel,
                                $resultado->descripcion,
                                $row->total_importe,
                                0.0,
                                4,
                                $cuenta->nombre_cargo,
                                $centro_recaudacion,
                                $clase,
                                0,
                            );

                            $resultado_insertar_cargo_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                            $cargos += $row->total_importe;

                            $total_filas += 1;
                        }
                    }

                    if ($cuenta->cuenta_abono) {
                        if (strpos(strtolower($cuenta->nombre_abono), "i.v.a.") !== FALSE || strpos(strtolower($cuenta->nombre_abono), "i.v.a") !== FALSE || strpos(strtolower($cuenta->nombre_abono), "iva") !== FALSE) {
                            if ($row->iva !== 0) {
                                $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_abono = $this->db->query($sql_abono, array($cuenta->cuenta_abono));
                                $resultado_cuenta_abono = $query_abono->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_abono);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_abono.' '.$cuenta->nombre_abono);
                                }

                                $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida, concepto_especifico) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_abono = array(
                                    $last,
                                    'Ingresos',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_abono,
                                    $resultado_cuenta_abono->id_padre,
                                    $resultado_cuenta_abono->nivel,
                                    $resultado->descripcion,
                                    0.0,
                                    $row->iva,
                                    4,
                                    $cuenta->nombre_abono,
                                    $centro_recaudacion,
                                    $clase,
                                    0,
                                );

                                $resultado_insertar_abono_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                $abonos += $row->iva;

                                $total_filas += 1;
                            }
                        }
                        else {
                            $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                            $query_abono = $this->db->query($sql_abono, array($cuenta->cuenta_abono));
                            $resultado_cuenta_abono = $query_abono->row();

                            $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_abono);

                            if ($existe_cuenta == FALSE) {
                                throw new Exception('No existe la cuenta contable '.$cuenta->cuenta_abono.' '.$cuenta->nombre_abono);
                            }

                            $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida, concepto_especifico) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                            $datos_abono = array(
                                $last,
                                'Ingresos',
                                $cuenta->id_correlacion_partidas_contables,
                                $cuenta->cuenta_abono,
                                $resultado_cuenta_abono->id_padre,
                                $resultado_cuenta_abono->nivel,
                                $resultado->descripcion,
                                0.0,
                                $row->total_importe,
                                4,
                                $cuenta->nombre_abono,
                                $centro_recaudacion,
                                $clase,
                                0,
                            );

                            $resultado_insertar_abono_query = $this->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                            $abonos += $row->total_importe;

                            $total_filas += 1;
                        }
                    }
                }
            }

            $datos_actualizar_caratula = array(
                'no_partidas' => $total_filas,
                'cargos' => $cargos,
                'abonos' => $abonos,
            );

            $this->db->where('numero_poliza', $last);
            $this->db->update('mov_polizas_cabecera', $datos_actualizar_caratula);

            $datos_actualizar_recaudado = array(
                'poliza' => 1,
            );

            $this->db->where('numero', $recaudado);
            $this->db->update('mov_recaudado_caratula', $datos_actualizar_recaudado);

            $this->db->trans_commit();

            return "exito";

        } catch (Exception $e) {
            $this->db->trans_rollback();

            return $e->getMessage();
        }
    }

}