<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

ini_set('memory_limit', '-1');
ini_set('max_execution_time', '-1');

require( './assets/datatables/scripts/ssp.class.php' );

class Contabilidad extends CI_Controller
{

    var $sql_details;

    /**
     * Se revisa si el usuario esta logueado, si no esta logueado, se reenvia a la pantalla de login
     */
    function __construct()
    {
        parent::__construct();
        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        }

        $this->sql_details = array(
            'user' => 'root',
            'pass' => '',
            'db' => 'software',
            'host' => 'localhost',
        );
    }

    /**
     * Esta funcion es la principal, donde se muestra la pantalla de contabilidad
     */
    function index()
    {
        log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ', ha Ingresado a Contabilidad');
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Contabilidad",
            "usuario" => $this->tank_auth->get_username(),
        );
        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/main_view');
        $this->load->view('front/footer_main_view', array("graficas" => TRUE));
    }

    /** Aqui empieza la sección del Asientos / Polizas */
    function polizas()
    {
        log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ', ha Ingresado a polizas');
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Asientos/Polizas",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/conta_polizas_view');
        $this->load->view('front/footer_main_view', array("tablas" => TRUE, "polizas" => TRUE));
    }

    function tabla_indice_polizas()
    {
        $resultado = $this->contabilidad_model->datosCaratulaPolizas();
//        $this->debugeo->imprimir_pre($resultado);

        $output = array();
        foreach ($resultado as $fila) {

            $firme = '';

            if ($fila->enfirme == 0) {
                $firme = '<button type="button" class="btn btn-circle btn-firmec" disabled><i class="fa fa-times"></i></button>';
            } elseif ($fila->enfirme == 1) {
                $firme = '<button type="button" class="btn btn-circle btn-firmea" disabled><i class="fa fa-check"></i></button>';
            }

            if ($fila->estatus == "activo") {
                $estatus = '<button type="button" class="btn btn-success disabled">Activo</button>';
            } elseif ($fila->estatus == "espera") {
                $estatus = '<button type="button" class="btn btn-warning disabled">Espera</button>';
            } else {
                $estatus = '<button type="button" class="btn btn-danger disabled">Cancelado</button>';
            }

            if ($fila->cancelada == 1) {
                $opciones = '<a href="' . base_url("contabilidad/ver_poliza/" . $fila->numero_poliza) . '" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                         <a href="' . base_url("contabilidad/editar_poliza/" . $fila->numero_poliza) . '" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                         <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>
                         <a href="' . base_url("contabilidad/imprimir_poliza/" . $fila->numero_poliza) . '" data-tooltip="Imprimir"><i class="fa fa-print"></i></a>';
            } else {
                $opciones = '<a href="' . base_url("contabilidad/ver_poliza/" . $fila->numero_poliza) . '" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                         <a href="' . base_url("contabilidad/editar_poliza/" . $fila->numero_poliza) . '" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                         <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>
                         <a href="' . base_url("contabilidad/imprimir_poliza/" . $fila->numero_poliza) . '" data-tooltip="Imprimir"><i class="fa fa-print"></i></a>
                         <a data-toggle="modal" data-target=".modal_regenerar" data-tooltip="Volver a Generar Póliza"><i class="fa fa-refresh"></i></a>';
            }

            $output["data"][] = array(
                $fila->numero_poliza,
                $fila->tipo_poliza,
                $fila->fecha,
                $fila->no_movimiento,
//                '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->cargos == 0 || $fila->cargos == NULL ? '0.00' : $this->cart->format_number(round($fila->cargos, 2)).'</div>'),
//                '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->abonos == 0 || $fila->abonos == NULL ? '0.00' : $this->cart->format_number(round($fila->abonos, 2)).'</div>'),
                '<div class="table-formant-sign">' . "$" . '</div><div class="table-formant-coin">' . ($fila->importe == 0 || $fila->importe == NULL ? '0.00' : $this->cart->format_number(round($fila->importe, 2)) . '</div>'),
                $fila->creado_por,
                $firme,
                $estatus,
                $opciones,
            );
        }

//        $this->debugeo->imprimir_pre($output);
        echo json_encode($output);
    }

    function tabla_indice_polizasDiario()
    {
        $resultado = $this->contabilidad_model->datosCaratulaPolizasDiario();
//        $this->debugeo->imprimir_pre($resultado);

        $output = array();
        foreach ($resultado as $fila) {
            $no_diario = '0';
            $firme = '';

            if ($fila->enfirme == 0) {
                $firme = '<button type="button" class="btn btn-circle btn-firmec" disabled><i class="fa fa-times"></i></button>';
            } elseif ($fila->enfirme == 1) {
                $firme = '<button type="button" class="btn btn-circle btn-firmea" disabled><i class="fa fa-check"></i></button>';
            }

            if ($fila->estatus == "activo") {
                $estatus = '<button type="button" class="btn btn-success disabled">Activo</button>';
            } elseif ($fila->estatus == "espera") {
                $estatus = '<button type="button" class="btn btn-warning disabled">Espera</button>';
            } else {
                $estatus = '<button type="button" class="btn btn-danger disabled">Cancelado</button>';
            }

            $opciones = '<a href="' . base_url("contabilidad/ver_poliza/" . $fila->numero_poliza) . '" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                         <a href="' . base_url("contabilidad/editar_poliza/" . $fila->numero_poliza) . '" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                         <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>
                         <a href="' . base_url("contabilidad/imprimir_poliza/" . $fila->numero_poliza) . '" data-tooltip="Imprimir"><i class="fa fa-print"></i></a>';

            if($fila->no_devengado != 0){
                $no_diario =  $fila->no_devengado;
            } elseif($fila->contrarecibo != 0) {
                $no_diario = $fila->contrarecibo;
            }

            $output["data"][] = array(
                $fila->numero_poliza,
                $no_diario,
                $fila->tipo_poliza,
                $fila->fecha,
                $fila->no_movimiento,
//                '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->cargos == 0 || $fila->cargos == NULL ? '0.00' : $this->cart->format_number(round($fila->cargos, 2)).'</div>'),
//                '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->abonos == 0 || $fila->abonos == NULL ? '0.00' : $this->cart->format_number(round($fila->abonos, 2)).'</div>'),
                '<div class="table-formant-sign">' . "$" . '</div><div class="table-formant-coin">' . ($fila->importe == 0 || $fila->importe == NULL ? '0.00' : $this->cart->format_number(round($fila->importe, 2)) . '</div>'),
                $fila->creado_por,
                $firme,
                $estatus,
                $opciones,
            );
        }

//        $this->debugeo->imprimir_pre($output);
        echo json_encode($output);
    }

    function tabla_indice_polizasIngresos()
    {
        $resultado = $this->contabilidad_model->datosCaratulaPolizasIngresos();
//        $this->debugeo->imprimir_pre($resultado);

        $output = array();
        foreach ($resultado as $fila) {

            $firme = '';

            if ($fila->enfirme == 0) {
                $firme = '<button type="button" class="btn btn-circle btn-firmec" disabled><i class="fa fa-times"></i></button>';
            } elseif ($fila->enfirme == 1) {
                $firme = '<button type="button" class="btn btn-circle btn-firmea" disabled><i class="fa fa-check"></i></button>';
            }

            if ($fila->estatus == "activo") {
                $estatus = '<button type="button" class="btn btn-success disabled">Activo</button>';
            } elseif ($fila->estatus == "espera") {
                $estatus = '<button type="button" class="btn btn-warning disabled">Espera</button>';
            } else {
                $estatus = '<button type="button" class="btn btn-danger disabled">Cancelado</button>';
            }
            $opciones = '<a href="' . base_url("contabilidad/ver_poliza/" . $fila->numero_poliza) . '" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                         <a href="' . base_url("contabilidad/editar_poliza/" . $fila->numero_poliza) . '" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                         <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>
                         <a href="' . base_url("contabilidad/imprimir_poliza/" . $fila->numero_poliza) . '" data-tooltip="Imprimir"><i class="fa fa-print"></i></a>';

            $output["data"][] = array(
                $fila->numero_poliza,
                $fila->no_recaudado,
                $fila->tipo_poliza,
                $fila->fecha,
                $fila->no_movimiento,
//                '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->cargos == 0 || $fila->cargos == NULL ? '0.00' : $this->cart->format_number(round($fila->cargos, 2)).'</div>'),
//                '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->abonos == 0 || $fila->abonos == NULL ? '0.00' : $this->cart->format_number(round($fila->abonos, 2)).'</div>'),
                '<div class="table-formant-sign">' . "$" . '</div><div class="table-formant-coin">' . ($fila->importe == 0 || $fila->importe == NULL ? '0.00' : $this->cart->format_number(round($fila->importe, 2)) . '</div>'),
                $fila->creado_por,
                $firme,
                $estatus,
                $opciones,
            );
        }

//        $this->debugeo->imprimir_pre($output);
        echo json_encode($output);
    }

    function tabla_indice_polizasEgresos()
    {
        $resultado = $this->contabilidad_model->datosCaratulaPolizasEgresos();
//        $this->debugeo->imprimir_pre($resultado);

        $output = array();
        foreach ($resultado as $fila) {

            $firme = '';
            $opciones = '';
            if ($this->utilerias->get_permisos("ver_polizas")) {
                $opciones .= '<a href="' . base_url("contabilidad/ver_poliza/" . $fila->numero_poliza) . '" data-tooltip="Ver"><i class="fa fa-eye"></i></a>';
            }

            if ($fila->enfirme == 0) {
                $firme = '<button type="button" class="btn btn-circle btn-firmec" disabled><i class="fa fa-times"></i></button>';
            } elseif ($fila->enfirme == 1) {
                $firme = '<button type="button" class="btn btn-circle btn-firmea" disabled><i class="fa fa-check"></i></button>';
            }

            if ($fila->estatus == "activo") {
                $estatus = '<button type="button" class="btn btn-success disabled">Activo</button>';
            } elseif ($fila->estatus == "espera") {
                $estatus = '<button type="button" class="btn btn-warning disabled">Espera</button>';
            } else {
                $estatus = '<button type="button" class="btn btn-danger disabled">Cancelado</button>';
            }

            if ($this->utilerias->get_grupo() == 1) {
                $opciones = '<a href="' . base_url("contabilidad/ver_poliza/" . $fila->numero_poliza) . '" data-tooltip="Ver"><i class="fa fa-eye"></i></a>
                              <a href="' . base_url("contabilidad/editar_poliza/" . $fila->numero_poliza) . '" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                              <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>
                              <a href="' . base_url("contabilidad/imprimir_poliza/" . $fila->numero_poliza) . '" data-tooltip="Imprimir"><i class="fa fa-print"></i></a>
                              <a data-toggle="modal" data-target=".modal_regenerar" data-tooltip="Volver a Generar Póliza"><i class="fa fa-refresh"></i></a>';
            } elseif ($fila->cancelada == 1) {
                if ($this->utilerias->get_permisos("editar_polizas")) {
                    $opciones .= '<a href="' . base_url("contabilidad/editar_poliza/" . $fila->numero_poliza) . '" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                                  <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>
                                  <a href="' . base_url("contabilidad/imprimir_poliza/" . $fila->numero_poliza) . '" data-tooltip="Imprimir"><i class="fa fa-print"></i></a>
                                  <a data-toggle="modal" data-target=".modal_regenerar" data-tooltip="Volver a Generar Póliza"><i class="fa fa-refresh"></i></a>';
                } else {
                    $opciones .= '<a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>
                                  <a href="' . base_url("contabilidad/imprimir_poliza/" . $fila->numero_poliza) . '" data-tooltip="Imprimir"><i class="fa fa-print"></i></a>
                                  <a data-toggle="modal" data-target=".modal_regenerar" data-tooltip="Volver a Generar Póliza"><i class="fa fa-refresh"></i></a>';
                }
            } else {
                if ($this->utilerias->get_permisos("editar_polizas")) {
                    $opciones .= '<a href="' . base_url("contabilidad/editar_poliza/" . $fila->numero_poliza) . '" data-tooltip="Editar"><i class="fa fa-edit"></i></a>
                                  <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>
                                  <a href="' . base_url("contabilidad/imprimir_poliza/" . $fila->numero_poliza) . '" data-tooltip="Imprimir"><i class="fa fa-print"></i></a>';
                } else {
                    $opciones .= '<a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>
                                  <a href="' . base_url("contabilidad/imprimir_poliza/" . $fila->numero_poliza) . '" data-tooltip="Imprimir"><i class="fa fa-print"></i></a>';
                }
            }

            $output["data"][] = array(
                $fila->numero_poliza,
                $fila->movimiento,
                $fila->tipo_poliza,
                $fila->fecha,
                $fila->no_movimiento,
//                '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->cargos == 0 || $fila->cargos == NULL ? '0.00' : $this->cart->format_number(round($fila->cargos, 2)).'</div>'),
//                '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila->abonos == 0 || $fila->abonos == NULL ? '0.00' : $this->cart->format_number(round($fila->abonos, 2)).'</div>'),
                '<div class="table-formant-sign">' . "$" . '</div><div class="table-formant-coin">' . ($fila->importe == 0 || $fila->importe == NULL ? '0.00' : $this->cart->format_number(round($fila->importe, 2)) . '</div>'),
                $fila->creado_por,
                $firme,
                $estatus,
                $opciones,
            );
        }

//        $this->debugeo->imprimir_pre($output);
        echo json_encode($output);
    }

    function tabla_detalle_poliza()
    {
        $poliza = $this->input->post("poliza", TRUE);
//        $poliza = 1;

        $resultado = $this->contabilidad_model->datos_polizaDetalle($poliza);
//        $this->debugeo->imprimir_pre($resultado);

        $output = array();

        foreach ($resultado as $fila) {
            $output[] = array(
                $fila->id_poliza_detalle,
                $fila->cuenta,
                $fila->centro_costo,
                $fila->partida,
                $fila->subsidio,
                $fila->nombre,
                $fila->debe,
                $fila->haber,
                $fila->numero_poliza,
                '<a data-toggle="modal" data-target=".modal_editar" data-tooltip="Editar"><i class="fa fa-pencil-square-o"></i></a>
                <a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Eliminar"><i class="fa fa-remove"></i></a>',
            );
        }
//        $this->debugeo->imprimir_pre($output);
        echo json_encode($output);
    }

    function editar_detalle_partida()
    {
//        Se toman los datos del compromiso a insertar
        $datos = $this->input->post();
        log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ', ha editado el detalle de la póliza' . $datos["editar_id_detalle_poliza"]);

        foreach ($datos as $key => $value) {
            $datos[$key] = trim($value);
        }

        try {
//            $this->debugeo->imprimir_pre($datos);

            $resultado_editar_detalle = $this->contabilidad_model->editar_detalle_partida($datos);

            if (!$resultado_editar_detalle) {
                $respuesta = array(
                    "mensaje" => '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-times-circle fa-2x ic-msj"></i> Hubo un error al tratar de editar el detalle de la póliza</div>',
                );

            } else {

                $respuesta = array(
                    "mensaje" => '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-check-circle fa-2x ic-msj"></i> El detalle se ha cambiado con éxito.</div>',
                );

            }

            echo(json_encode($respuesta));

        } catch (Exception $e) {
            $respuesta = array(
                "mensaje" => "" . $e->getMessage(),
            );

            echo(json_encode($respuesta));
        }

    }

    function borrar_detalle_movimiento()
    {
//        Se toman los datos del compromiso a insertar
        $datos = $this->input->post();
        //log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ', ha borrado el detalle de movimiento' . $datos["movimiento"]);

        foreach ($datos as $key => $value) {
            $datos[$key] = trim($value);
        }

        try {
//            $this->debugeo->imprimir_pre($datos);

            $this->db->where('id_poliza_detalle', $datos["movimiento"]);
            $resultado = $this->db->delete('mov_poliza_detalle');

            if (!$resultado) {
                $respuesta = array(
                    "mensaje" => '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-times-circle fa-2x ic-msj"></i> Hubo un error al tratar de borrar el detalle de la póliza</div>',
                );

            } else {

                $respuesta = array(
                    "mensaje" => '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-check-circle fa-2x ic-msj"></i> El detalle se ha eliminado con éxito.</div>',
                );

            }

            echo(json_encode($respuesta));

        } catch (Exception $e) {
            $respuesta = array(
                "mensaje" => "" . $e->getMessage(),
            );

            echo(json_encode($respuesta));
        }
    }

    function agregarPoliza()
    {
//        Se inicizaliza la variable con el ultimo valor de las polizas
        $last = 0;
//        Se toma el numero de la ultima poliza
        $ultimo = $this->ciclo_model->ultima_poliza();
//        Se le suma uno al ultimo valor de las polizas
        if ($ultimo) {
            $last = $ultimo->ultimo + 1;
        } //        De lo contrario se inicia en 1
        else {
            $last = 1;
        }
        log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ', ha agregado una póliza' .$last);

        $this->contabilidad_model->apartarPoliza($last);

        $query_fecha = "SELECT max(fecha_termina) AS fecha FROM mov_polizas_periodos WHERE cerrado = 1;";
        $resultado_fecha = $this->db->query($query_fecha);
        $fecha = $resultado_fecha->row_array();
        $fecha_minima = $fecha["fecha"];

        $datos_header = array(
            "titulo_pagina" => "Armonniza | Agregar Póliza",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
            "precompromisocss" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/agregar_poliza_completa_view', array('ultimo' => $last));
        $this->load->view('front/footer_main_view', array(
            "agregarPolizas" => TRUE,
            "fecha_minima" => $fecha_minima,
        ));
    }

    function insertar_poliza()
    {
        $datos = $this->input->post();
        //log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ', ha insertado una póliza' . $datos);

        foreach ($datos as $key => $value) {
            $datos[$key] = trim($value);
        }

        try {

            if ($datos['check_firme'] == 1) {
                $datos['estatus'] = 'activo';

                $datos_detalle_poliza = $this->contabilidad_model->getdatosDetallePolizas($datos["ultima_poliza"]);

                foreach ($datos_detalle_poliza as $key => $value) {
//                    $this->debugeo->imprimir_pre($value);
                    $cuenta = $this->contabilidad_model->datos_cuenta($value->cuenta);
//                    $this->debugeo->imprimir_pre($cuenta);
                }

            } else {
                $datos['estatus'] = 'espera';
            }

            $resultado_insertar_caratula = $this->contabilidad_model->actualizar_poliza_caratula($datos);

            if (!$resultado_insertar_caratula) {
                $respuesta = array(
                    "mensaje" => '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-times-circle fa-2x ic-msj"></i> Hubo un error al tratar de insertar los datos</div>',
                );

            } else {

                $respuesta = array(
                    "mensaje" => '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-check-circle fa-2x ic-msj"></i> Datos insertados correctamente</div>',
                );

            }

            echo(json_encode($respuesta));

        } catch (Exception $e) {
            $respuesta = array(
                "mensaje" => "" . $e->getMessage(),
            );

            echo(json_encode($respuesta));
        }

    }

    function insertar_detalle_poliza(){
        $datos = $this->input->post();

        log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ', ha Insertado un detalle de póliza' . $datos['ultima_poliza']);

        foreach ($datos as $key => $value) {
            $datos[$key] = trim($value);
        }

        try {

            $resultado_insertar_detalle = $this->contabilidad_model->insertar_detalle_poliza($datos);

            if (!$resultado_insertar_detalle) {
                $respuesta = array(
                    "mensaje" => '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-times-circle fa-2x ic-msj"></i> Hubo un error al tratar de insertar el detalle de la póliza.</div>',
                );

            } else {

                $this->db->select('cargos, abonos')->from('mov_polizas_cabecera')->where('numero_poliza', $datos['ultima_poliza']);

                $query = $this->db->get();

                $cargos_abonos = $query->row();

                $cargos = $cargos_abonos->cargos + $datos['debe'];
                $abonos = $cargos_abonos->abonos + $datos['haber'];

                $respuesta = array(
                    "cargos" => round($cargos, 2),
                    "abonos" => round($abonos, 2),
                    "mensaje" => '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-check-circle fa-2x ic-msj"></i> Los datos se han insertado correctamente.</div>',
                );

                $datos_actualizar = array(
                    'cargos' => round($cargos, 2),
                    'abonos' => round($abonos, 2),
                );

                $this->db->where('numero_poliza', $datos['ultima_poliza']);
                $this->db->update('mov_polizas_cabecera', $datos_actualizar);

            }

            echo(json_encode($respuesta));

        } catch (Exception $e) {
            $respuesta = array(
                "mensaje" => "" . $e->getMessage(),
                "cargos" => "0",
                "abonos" => "0",
            );

            echo(json_encode($respuesta));
        }

    }

    function editar_poliza($poliza = NULL)
    {
        log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ', ha editado la póliza' . $poliza);
        $this->db->select('*')->from('mov_polizas_cabecera')->where('numero_poliza', $poliza);
        $query = $this->db->get();
        $resultado_caratula = $query->row_array();

//        $this->debugeo->imprimir_pre($resultado_caratula);

        $query_fecha = "SELECT max(fecha_termina) AS fecha FROM mov_polizas_periodos WHERE cerrado = 1;";
        $resultado_fecha = $this->db->query($query_fecha);
        $fecha = $resultado_fecha->row_array();
        $fecha_minima = $fecha["fecha"];

        $datos_header = array(
            "titulo_pagina" => "Armonniza | Editar Póliza",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
            "precompromisocss" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/editar_poliza_completa_view', $resultado_caratula);
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "editarPolizas" => TRUE,
            "fecha_minima" => $fecha_minima,
        ));
    }

    function ver_poliza($poliza = NULL){
        log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ', ha visto la póliza' . $poliza);
        $this->db->select('*')->from('mov_polizas_cabecera')->where('numero_poliza', $poliza);
        $query = $this->db->get();
        $resultado_caratula = $query->row_array();

//        $this->debugeo->imprimir_pre($resultado_caratula);

        $datos_header = array(
            "titulo_pagina" => "Armonniza | Ver Póliza",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
            "precompromisocss" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/ver_poliza_completa_view', $resultado_caratula);
        $this->load->view('front/footer_main_view', array("tablas" => TRUE, "verPolizas" => TRUE));
    }

    function cancelar_poliza_caratula()
    {
//        Se toman los datos del compromiso a insertar
        $datos = $this->input->post();
        log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ', ha cancelado la póliza' . $datos["poliza"]);

        foreach ($datos as $key => $value) {
            $datos[$key] = trim($value);
        }

        try {
//            $this->debugeo->imprimir_pre($datos);

            $data = array(
                'cancelada' => 1,
                'estatus' => "cancelada",
            );

            $this->db->where('numero_poliza', $datos["poliza"]);
            $respuesta = $this->db->update('mov_polizas_cabecera', $data);

            echo(json_encode($respuesta));

        } catch (Exception $e) {
            $respuesta = array(
                "mensaje" => "" . $e->getMessage(),
            );

            echo(json_encode($respuesta));
        }


    }

    function imprimir_poliza($poliza = NULL)
    {
        log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ', ha impreso la póliza' . $poliza);
        $query = "SELECT * FROM mov_polizas_cabecera WHERE numero_poliza = ?;";
        $datos_caratula = $this->contabilidad_model->get_datos_polizas_caratula($query, $poliza);

        //    $this->debugeo->imprimir_pre($datos_caratula);

        if ($datos_caratula->enfirme == 0 && $datos_caratula->estatus == 'espera') {
            $mensaje = "La póliza aún no esta en firme.";
        } else {
            $mensaje = NULL;
        }

        $datos_header = array(
            "titulo_pagina" => "Armonniza | Imprimir Póliza",
            "usuario" => $this->tank_auth->get_username(),
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/seleccion_imprimir_poliza_view', array(
            "poliza" => $poliza,
            "mensaje" => $mensaje,
            "tipo_requisicion" => $datos_caratula->tipo_poliza,
        ));
        $this->load->view('front/footer_main_view');
    }

    function imprimir_poliza_formato()
    {
        $poliza = $this->input->post("poliza");
        log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ', ha Impreso el formato de póliza' . $poliza);

        $persona = $this->input->post("persona");
        $this->db->select('*')->from('cat_polizas_firmas')->where('id_persona', $persona);
        $query = $this->db->get();
        $resultado_persona = $query->row();

        $persona_vobo = '2';
        $this->db->select('*')->from('cat_autorizadores_firmas')->where('id_persona', $persona_vobo);
        $query = $this->db->get();
        $resultado_persona_vobo = $query->row();

        $persona_autoriza = '1';
        $this->db->select('*')->from('cat_autorizadores_firmas')->where('id_persona', $persona_autoriza);
        $query = $this->db->get();
        $resultado_persona_autoriza = $query->row();


        $query = "SELECT p1.*, p2.id_contrarecibo_caratula, p2.tipo_documento, p3.movimiento, p3.contrarecibo AS contrarecibo_numero_tabla
                  FROM mov_polizas_cabecera p1
                  LEFT JOIN mov_bancos_movimientos p3 ON p3.movimiento = p1.movimiento
                  LEFT JOIN mov_contrarecibo_caratula p2 ON p2.id_contrarecibo_caratula = p1.contrarecibo OR p2.id_contrarecibo_caratula = p3.contrarecibo
                  WHERE p1.numero_poliza = ?;";
        $datos = $this->contabilidad_model->get_datos_polizas_caratula($query, $poliza);
        $datos_partida = $this->contabilidad_model->getdatosDetallePolizasPartida($poliza);
//        $this->debugeo->imprimir_pre($datos_partida);

        if ($datos->contrarecibo == 0 || $datos->contrarecibo == NULL) {
            if ($datos_partida->partida == NULL) {
                $datos_detalle = $this->contabilidad_model->getdatosDetallePolizasDirectas($poliza);
            } else {
                $datos_detalle = $this->contabilidad_model->getdatosDetallePolizasIngresos($poliza);
            }
        } else {
            $datos_detalle = $this->contabilidad_model->getdatosDetallePolizas($poliza);
        }
//       $this->debugeo->imprimir_pre($datos_detalle);

        $this->load->library('Pdf');

        $pdf = new Pdf('L', 'cm', 'A4', true, 'UTF-8', false);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Armonniza');
        $pdf->SetTitle('Pólizas');
        $pdf->SetSubject('Pólizas');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
//        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
//        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        // remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
//        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, 10, PDF_MARGIN_RIGHT);
//        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// ---------------------------------------------------------

// set font
        $pdf->SetFont('helvetica', '', 12);

// add a page
        $pdf->AddPage('L', 'Letter');

        $encabezado = '<table cellspacing="3" style="font-size: small;">
                            <tr><td></td><td></td><td></td></tr>
                            <tr>
                                <td align="left"><b>&nbsp; &nbsp; &nbsp;Fecha Solicitud</b></td>
                                <td align="center"><b>Tipo Póliza</b></td>
                                <td align="right"><b>No. Póliza</b></td>
                            </tr>
                            <tr>
                                <td align="left">&nbsp; &nbsp; &nbsp; &nbsp;' . $datos->fecha . '</td>
                                <td align="center">' . $datos->tipo_poliza . '</td>
                                <td align="right">' . $datos->numero_poliza . '</td>
                            </tr>
                       </table>';

        $tabla_detalle = '';
        $total_debe = 0;
        $total_haber = 0;
        $numero = 1;

        foreach ($datos_detalle as $fila) {
            if (isset($fila->partida) == NULL) {
                $fila->partida = " ";
                $fila->descripcion = " ";
            } else {
                $fila->partida == $fila->partida;
                $fila->descripcion = $fila->descripcion;
            }
            $tabla_detalle .= '<tr>
                                   <td align="center" style=" border-right: 1px solid #BDBDBD;" width="8%">' . $fila->fecha . '</td>
                                   <td align="center" style=" border-right: 1px solid #BDBDBD;" width="5.3%">' . $fila->numero_poliza . '</td>
                                   <td align="center" style=" border-right: 1px solid #BDBDBD;" width="6%">' . $numero . '</td>
                                   <td align="center" style=" border-right: 1px solid #BDBDBD;" width="8%">' . $datos->tipo_documento . '</td>
                                   <td align="left" style=" border-right: 1px solid #BDBDBD;" width="12%">' . $fila->cuenta . '</td>
                                   <td align="center" style=" border-right: 1px solid #BDBDBD;" width="16%">' . $fila->nombre . '</td>
                                   <td align="center" style=" border-right: 1px solid #BDBDBD;" width="7%">' . $fila->partida . '</td>
                                   <td align="center" style=" border-right: 1px solid #BDBDBD;" width="14%">' . $fila->descripcion . '</td>
                                   <td align="left" width="1.9%">$</td>
                                   <td align="right" style=" border-right: 1px solid #BDBDBD;" width="11%">' . ($fila->debe == 0 || $fila->debe == NULL ? '0.00' : $this->cart->format_number(round($fila->debe, 2))) . '</td>
                                   <td align="left" width="1.9%">$</td>
                                   <td  align="right" width="11%">' . ($fila->haber == 0 || $fila->haber == NULL ? '0.00' : $this->cart->format_number(round($fila->haber, 2))) . '</td>

                               </tr>';

            $total_debe += $fila->debe;
            $total_haber += $fila->haber;
            $numero++;
        }

        $total = '<table style="font-size: small;" cellpadding="4">
                    <tr>
                        <td align="left" width="35%" style="font-size: 9px; border-right: 1px solid #BDBDBD;">Subtotal</td>
                        <td align="left" width="5.4%">$</td>
                        <td align="right" width="27%" style="border-right: 1px solid #BDBDBD;">
                        ' . ($total_debe == 0 || $total_debe == NULL ? '0.00' : $this->cart->format_number(round($total_debe, 2))) . '
                        </td>
                        <td align="left" width="5.4%">$</td>
                        <td align="right" width="27%" >
                        ' . ($total_haber == 0 || $total_haber == NULL ? '0.00' : $this->cart->format_number(round($total_haber, 2))) . '
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="35%" style="font-size: 9px; border-right: 1px solid #BDBDBD;"><b>Total</b></td>
                        <td align="left" width="5.4%">$</td>
                        <td align="right" width="27%" style="border-right: 1px solid #BDBDBD;">
                        ' . ($total_debe == 0 || $total_debe == NULL ? '0.00' : $this->cart->format_number(round($total_debe, 2))) . '
                        </td>
                        <td align="left" width="5.4%">$</td>
                        <td align="right" width="27%">
                        ' . ($total_haber == 0 || $total_haber == NULL ? '0.00' : $this->cart->format_number(round($total_haber, 2))) . '

                        </td>
                    </tr>
                   </table>';

        $descripcion = '<table style="font-size: small;" cellpadding="4">
                    <tr>
                        <td align="left" width="100%" style="font-size: 9px;"><b>Descripción</b></td>
                    </tr>
                    <tr>
                        <td align="left" width="100%" style="font-size: 8px;">' . $datos->concepto_especifico . '</td>
                    </tr>
                   </table>';

        $html = '
        <style>
            .cont-general{
                border: 1px solid #eee;
                border-radius: 1%;
                margin: 2% 14%;
            }
            .cont-general2{
                border: 1px solid #BDBDBD;
            }
            .cont-general .borde-inf{
                border-bottom: 1px solid #eee;
            }
            .cont-general .borde-sup{
                border-top: 1px solid #eee;
            }
        </style>

        <table class="cont-general" border="0" cellspacing="2" style="font-size: small;" cellpadding="2">
            <tr cellspacing="10">
                <th width="2%"></th>
                <th align="left" width="10%"><img src="' . base_url("img/logo2.png") . '" /></th>
                <th align="center" width="84%" style="line-height: 7px;">
                    <h3>EDUCAL, S.A. DE C.V.</h3>
                    <h5>Pólizas</h5>
                    ' . $encabezado . '
                </th>
                <th width="2%"></th>
            </tr>
            <tr><td width="99%" class="borde-sup"></td></tr>
            <tr>
                <td width="96%">
                    <table class="cont-general2" style="font-size:8px;" cellspacing="0" cellpadding="3" >
                     <tr>
                             <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD; line-height: 50px;" rowspan="3" width="8%">Fecha</td>
                             <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD; " rowspan="3" width="5.3%"><br><br>No. Evento</td>
                             <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD; " rowspan="3" width="6%"><br><br>No. Asiento</td>
                             <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD; " rowspan="3" width="8%"><br><br>Documento Fuente</td>
                             <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="49%">Código y nombre de la cuenta</td>
                             <td align="center" style="font-weight: bold; border-bottom: 1px solid #BDBDBD;" width="25.8%">Monto</td>
                        </tr>
                         <tr>
                             <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="49%">Contable / Presupuestal</td>
                             <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" rowspan="2" width="12.9%"><br><br>Debe</td>
                             <td align="center" style="font-weight: bold; border-bottom: 1px solid #BDBDBD;" rowspan="2" width="12.9%"><br><br>Haber</td>
                        </tr>
                         <tr>
                             <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="12%"><br>Código</td>
                             <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="16%"><br>Nombre</td>
                             <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="7%"><br>Partida</td>
                             <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="14%"><br>Nombre</td>
                        </tr>
                    ' . $tabla_detalle . '
                </table>
                </td>
            </tr>
            <tr><td width="99%"></td></tr>
            <tr>
                <td width=".7%"></td>
                <td width="57.3%" class="cont-general2">' . $descripcion . '</td>
                <td width="1%" ></td>
                <td width="38%" class="cont-general2">' . $total . '</td>
            </tr>
            <tr><td width="99%" class="borde-inf"></td></tr>
             <tr>
                <td align="center" width="1%"></td>
                <td align="center" width="32%" style="font-size: 8px;"><b>Elaboró</b> <br><br>' . $resultado_persona->grado_estudio . ' ' . $resultado_persona->nombre . '  <br> ' . $resultado_persona->puesto . '</td>
                <td align="center" width="32%" style="font-size: 8px;"><b>Autorizó</b> <br><br>' . $resultado_persona_vobo->grado_estudio . ' ' . $resultado_persona_vobo->nombre . '  <br> ' . $resultado_persona_vobo->puesto . '</td>
                <td align="center" width="32%" style="font-size: 8px;"><b>Vo.Bo.</b> <br><br>' . $resultado_persona_autoriza->grado_estudio . ' ' . $resultado_persona_autoriza->nombre . '  <br> ' . $resultado_persona_autoriza->puesto . '</td>
                <td align="center" width="2%"></td>
            </tr>
</table>';

// output the HTML content

        $pdf->writeHTML($html, false, false, true, false, 'top');

//Close and output PDF document
        $pdf->Output('Póliza.pdf', 'I');
    }

    function subir_detalle_poliza() {

        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

//        Se prepara la configuracion de la libreria "Upload"
        $config['upload_path'] = './application/archivos/';
        $config['allowed_types'] = '*';

//        Se carga la libreria con sus configuraciones correspondientes
        $this->load->library('upload', $config);

//        Si el server no logra subir el archivo, despliega un mensaje de error
        if (!$this->upload->do_upload('archivoSubir')) {
//            Se capturan los errores en una variable y se imprimen para debug
            $error = array('error' => $this->upload->display_errors());
            $this->debugeo->imprimir_pre($error);
        } else {
            ini_set('memory_limit', '-1');
//            load our new PHPExcel library
            $this->load->library('excel');
//            Si el server logra subir el archivo, se toman los datos del archivo
            $data = $this->upload->data();

            $datos_post = $this->input->post();

            foreach ($datos_post as $key => $value) {
                $datos_post[$key] = trim($value);
            }

//            Se toma la ruta del archivo junto con su nombre y extension
            $file = $data["full_path"];

//            read file from path
            $objPHPExcel = PHPExcel_IOFactory::load($file);

//            get only the Cell Collection
            $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();

            try {

//                extract to a PHP readable array format
                foreach ($cell_collection as $cell) {
                    $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
                    $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
                    $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();

//                    header will/should be in row 1 only. of course this can be modified to suit your need.
                    if ($row == 1) {
                        continue;
                    } else {
                        $datos_detalle[$row][$column] = trim($data_value);
                    }
                }


                $linea = 2;

                $this->db->trans_begin();
                foreach ($datos_detalle as $key => $value) {

                    if (!isset($value["A"]) || $value["A"] == NULL || $value["A"] == "") {
                        throw new Exception('La línea ' . $linea . ' no tiene cuenta contable.');
                    }

                    if (!isset($value["E"]) || $value["E"] == NULL || $value["E"] == "") {
                        throw new Exception('La línea ' . $linea . ' no tiene descripción de la cuenta contable.');
                    }

                    $this->db->select('nombre, tipo')->from('cat_cuentas_contables')->where('cuenta', $value["A"]);
                    $query_cuenta_existe = $this->db->get();
                    $existe_cuenta = $query_cuenta_existe->row_array();

                    if (!isset($existe_cuenta["nombre"]) || $existe_cuenta["nombre"] == NULL || $existe_cuenta["nombre"] == "") {
                        throw new Exception('La cuenta contable en la línea ' . $linea . ' no existe dentro del plan de cuentas.');
                    }

                    if ($existe_cuenta["tipo"] != "D") {
                        throw new Exception('La cuenta contable en la línea ' . $linea . ' no es de detalle, no se pueden utilizar cuentas acumulativas.');
                    }

                    if (!isset($value["B"])) {
                        $value["B"] = "";
                    }

                    if (!isset($value["C"])) {
                        $value["C"] = "";
                    }

                    if (!isset($value["D"])) {
                        $value["D"] = "";
                    }

                    if (!isset($value["F"]) || $value["F"] == NULL || $value["F"] == "") {
                        $value["F"] = 0;
                    }

                    if (!isset($value["G"]) || $value["G"] == NULL || $value["G"] == "") {
                        $value["G"] = 0;
                    }

                    $data = array(
                        'numero_poliza' => $datos_post["ultima_poliza"],
                        'tipo_poliza' => $datos_post["tipo_poliza"],
                        'fecha' => $datos_post["fecha"],
                        'hora' => date("H:i"),
                        'fecha_real' => date("Y-m-d"),
                        'cuenta' => $value["A"],
                        'concepto' => $datos_post["concepto"],
                        'debe' => $value["F"],
                        'haber' => $value["G"],
                        'subsidio' => $value["D"],
                        'nombre' => $value["E"],
                        'centro_costo' => $value["B"],
                        'partida' => $value["C"],
                        'concepto_especifico' => $datos_post["concepto_especifico"],
                    );

                    $this->db->insert('mov_poliza_detalle', $data);

                    $linea += 1;
                }

                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    throw new Exception('Ha ocurrido un error al insertar los datos, por favor intentelo de nuevo.');
                } else {
                    $this->db->trans_commit();
                    echo(json_encode(array("status" => 200)));
                }


            } catch (Exception $e) {
                $this->db->trans_rollback();

                echo(json_encode(array(
                    "mensaje" => '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' . $e->getMessage() . '.</div>',
                )));

            }

        }
    }

    function regenerar_poliza()
    {
        log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ', ha regenerado la póliza' .$this->input->post("poliza", TRUE));
        $resultado_de_insertar = "";

        $poliza = $this->input->post("poliza", TRUE);
//        $poliza = 4001;

        $this->db->select('contrarecibo, movimiento, no_recaudado')->from('mov_polizas_cabecera')->where('numero_poliza', $poliza);
        $query = $this->db->get();
        $resultado_caratula = $query->row_array();

//        $this->debugeo->imprimir_pre($resultado_caratula);

        if ($resultado_caratula["no_recaudado"] != 0) {
            $resultado_de_insertar = $this->utilerias->generar_poliza_ingresos($resultado_caratula["no_recaudado"]);
        } elseif ($resultado_caratula["movimiento"] != 0) {
            $resultado_de_insertar = $this->utilerias->generar_poliza_egresos($resultado_caratula["movimiento"]);
        } elseif ($resultado_caratula["contrarecibo"] != 0) {
            $resultado_de_insertar = $this->utilerias->generar_poliza_diario_egresos($resultado_caratula["contrarecibo"]);
        }

//        $this->debugeo->imprimir_pre($resultado_de_insertar);

        echo(json_encode(array(
            "mensaje" => $resultado_de_insertar,
        )));

    }

    function exportaExcel($name, $objExcel)
    {
        $filename = $name; // Agregar fecha en que se generó
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
//        save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
//        if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($objExcel, 'Excel5');
//        force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }

    function exportarPolizas()
    {
        
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Exportar Pólizas",
            "usuario" => $this->tank_auth->get_username(),
            "usuariocss" => TRUE,
            "tablas" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/conta_exportar_polizas_view');
        $this->load->view('front/footer_main_view', array("tablas" => TRUE, "exportarPolizas" => TRUE));
    }

    function exportar_polizas_excel()
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

        $this->load->library('excel');
//        $this->benchmark->mark('code_start');

        $datos = $this->input->post();

//        $datos = array(
//            "fecha_inicial" => "2015-01-01",
//            "fecha_final" => "2015-01-31",
//            "tipo_poliza" => "todo",
//            "estatus_poliza" => "todo",
//        );

        foreach ($datos as $key => $value) {
            $datos[$key] = trim($value);
        }

        try {
            $datos_polizas = $this->contabilidad_model->get_polizas_excel($datos);

            $row = 2;
            $objWorkSheet = $this->excel->createSheet();
            $this->generaEncabezadoExcelPolizas($objWorkSheet);

            foreach ($datos_polizas as $fila) {
//                $this->debugeo->imprimir_pre($fila);
                $this->guardaRegistroExcelPolizas($fila, $objWorkSheet, $row);
                $row += 1;

            }

//        $this->benchmark->mark('code_end');
//        echo $this->benchmark->elapsed_time('code_start', 'code_end');
            //Se remueve la primer hoja, que no tiene ninguna utilidad
            $this->excel->removeSheetByIndex(0);
            $this->exportaExcel("Reporte de Pólizas.xls", $this->excel);

        } catch (Exception $e) {
            $respuesta = array(
                "mensaje" => "" . $e->getMessage(),
            );
//            echo(json_encode($respuesta));
        }
    }

    function generaEncabezadoExcelPolizas($objWorkSheet)
    {
        $objWorkSheet->setTitle('Reporte Pólizas');

        $objWorkSheet->setCellValue('A1', 'Número de Póliza');
        $objWorkSheet->setCellValue('B1', 'Fuente de Financiamiento');
        $objWorkSheet->setCellValue('C1', 'Fecha');
        $objWorkSheet->setCellValue('D1', 'Factura');
        $objWorkSheet->setCellValue('E1', 'Tipo de Póliza');
        $objWorkSheet->setCellValue('F1', 'Centro de Costos / Centro de Recaudación');
        $objWorkSheet->setCellValue('G1', 'Partida');
        $objWorkSheet->setCellValue('H1', 'Proveedor / Cliente');
        $objWorkSheet->setCellValue('I1', 'Contrarecibo');
        $objWorkSheet->setCellValue('J1', 'Movimiento Bancario');
        $objWorkSheet->setCellValue('K1', 'Devengado');
        $objWorkSheet->setCellValue('L1', 'Recaudado');
        $objWorkSheet->setCellValue('M1', 'Concepto');
        $objWorkSheet->setCellValue('N1', 'Cuenta');
        $objWorkSheet->setCellValue('O1', 'Descripción de Cuenta');
        $objWorkSheet->setCellValue('P1', 'Cargos');
        $objWorkSheet->setCellValue('Q1', 'Abonos');
        $objWorkSheet->setCellValue('R1', 'Total Cargos');
        $objWorkSheet->setCellValue('S1', 'Total Abonos');

    }

    function guardaRegistroExcelPolizas($fila, $objWorkSheet, $row)
    {
        $arreglo_datos = array(
            "numero_poliza" => $fila["numero_poliza"],
            "subsidio" => $fila["subsidio"],
            "fecha" => $fila["fecha"],
            "factura" => $fila["no_movimiento"],
            "tipo_poliza" => $fila["tipo_poliza"],
            "centro_costo" => $fila["centro_costo"],
            "partida" => $fila["partida"],
            "proveedor" => ($fila["proveedor"] != NULL ? $fila["proveedor"] : $fila["cliente"]),
            "contrarecibo" => $fila["contrarecibo"],
            "movimiento" => $fila["movimiento"],
            "no_devengado" => $fila["no_devengado"],
            "no_recaudado" => $fila["no_recaudado"],
            "concepto" => $fila["concepto"],
            "cuenta" => $fila["cuenta"],
            "nombre" => $fila["nombre"],
            "debe" => $fila["debe"],
            "haber" => $fila["haber"],
            "cargos" => $fila["cargos"],
            "abonos" => $fila["abonos"],
        );

//        $this->debugeo->imprimir_pre($arreglo_datos);

        $objWorkSheet->fromArray($arreglo_datos, NULL, 'A' . $row);

    }

    private function insertar_movimiento_ingresos_caratula($datos)
    {
        $respuesta = array();

        foreach ($datos as $key => $value) {
            $datos[$key] = trim($value);
        }

        if ($datos["check_firme"] == 1) {
            $this->db->where('id_cuentas_bancarias', $datos["hidden_id_cuenta"]);
            $this->db->select('saldo');

            $query = $this->db->get('cat_cuentas_bancarias');

            $cuenta = $query->row();

            $saldo = $cuenta->saldo + $datos["importe"];

            $data = array(
                'saldo' => $saldo,
            );

            $this->db->where('id_cuentas_bancarias', $datos["hidden_id_cuenta"]);
            $this->db->update('cat_cuentas_bancarias', $data);

        }

//        $this->debugeo->imprimir_pre($datos);
        log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ', ha exportado la pólizas' .$resultado_insertar);

        $resultado_insertar = $this->ciclo_model->insertar_movimiento_bancario($datos);

//        Si el resultado es exitoso, se le indica al usuario
        if ($resultado_insertar) {
            $respuesta["mensaje_insertar"] = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Datos insertados correctamente.</div>';
        } //        De lo contrario, se manda un error al usuario
        else {
            $respuesta["mensaje_insertar"] = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Error al insertar los datos.</div>';
        }

        echo(json_encode($respuesta));
    }

    function ponerPolizasEnFirme()
    {
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Poner Pólizas En Firme",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
            "precompromisocss" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/filtro_polizas_enfirme_view');
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "polizas" => TRUE,
            "polizas_firme" => TRUE,
        ));
    }

    function poner_polizas_firme()
    {
        $datos = $this->input->post();

        foreach ($datos as $key => $value) {
            $datos[$key] = trim($value);
        }

        try {

            $sql = "UPDATE mov_polizas_cabecera SET enfirme = 1, estatus = 'activo' ";

            if (isset($datos["fecha_inicial"]) && $datos["fecha_inicial"] != NULL && $datos["fecha_inicial"] != "") {
                if (strpos($sql, 'WHERE') !== FALSE) {
                    $sql .= "AND fecha >= '" . $datos["fecha_inicial"] . "' ";
                } else {
                    $sql .= "WHERE fecha >= '" . $datos["fecha_inicial"] . "' ";
                }
            }

            if (isset($datos["fecha_final"]) && $datos["fecha_final"] != NULL && $datos["fecha_final"] != "") {
                if (strpos($sql, 'WHERE') !== FALSE) {
                    $sql .= "AND fecha <= '" . $datos["fecha_final"] . "' ";
                } else {
                    $sql .= "WHERE fecha <= '" . $datos["fecha_final"] . "' ";
                }
            }

            if ($datos["tipo_poliza"] == "Diario") {

                if (strpos($sql, 'WHERE') !== FALSE) {
                    $sql .= "AND tipo_poliza = 'Diario' ";
                } else {
                    $sql .= "WHERE tipo_poliza = 'Diario' ";
                }

            } elseif ($datos["tipo_poliza"] == "Egresos") {

                if (strpos($sql, 'WHERE') !== FALSE) {
                    $sql .= "AND tipo_poliza = 'Egresos' ";
                } else {
                    $sql .= "WHERE tipo_poliza = 'Egresos' ";
                }

            } elseif ($datos["tipo_poliza"] == "Ingresos") {

                if (strpos($sql, 'WHERE') !== FALSE) {
                    $sql .= "AND tipo_poliza = 'Ingresos' ";
                } else {
                    $sql .= "WHERE tipo_poliza = 'Ingresos' ";
                }

            } elseif ($datos["tipo_poliza"] == "Todas") {

                if (strpos($sql, 'WHERE') !== FALSE) {
                    $sql .= "AND tipo_poliza LIKE '%%' ";
                } else {
                    $sql .= "WHERE tipo_poliza LIKE '%%' ";
                }

            }

            if (isset($datos["numero_inicial"]) && $datos["numero_inicial"] != NULL && $datos["numero_inicial"] != "") {

                if (strpos($sql, 'WHERE') !== FALSE) {
                    $sql .= "AND numero_poliza >= '" . $datos["numero_inicial"] . "' ";
                } else {
                    $sql .= "WHERE numero_poliza >= '" . $datos["numero_inicial"] . "' ";
                }

            }

            if (isset($datos["numero_final"]) && $datos["numero_final"] != NULL && $datos["numero_final"] != "") {

                if (strpos($sql, 'WHERE') !== FALSE) {
                    $sql .= "AND numero_poliza <= '" . $datos["numero_final"] . "' ";
                } else {
                    $sql .= "WHERE numero_poliza <= '" . $datos["numero_final"] . "' ";
                }

            }

            $sql .= ";";

//            $this->debugeo->imprimir_pre($sql);

            $this->db->trans_begin();

            $query = $this->db->query($sql);

            if ($query) {

                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    throw new Exception('Hubo un error al realizar la operación, por favor contacte a su administrador.');
                } else {
                    $this->db->trans_commit();
                    $respuesta = array(
                        "mensaje" => '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>La operaci&oacute;n se ha realizado con &eacute;xito.</div>',
                    );
                }

                echo(json_encode($respuesta));

            } else {
                $this->db->trans_rollback();
                throw new Exception('Hubo un error al realizar la operación, por favor contacte a su administrador.');
            }

        } catch (Exception $e) {
            $this->db->trans_rollback();
            $respuesta = array(
                "mensaje" => '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' . $e->getMessage() . '</div>',
            );
            echo(json_encode($respuesta));
        }
    }

    /** Aqui empieza la sección de Consulta Balanza */
    function balanza()
    {
        log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ', ha consulta de balanza');
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Consulta Balanza",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
            "reportescss" => TRUE,
            "editar_tabla" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/conta_balanza_view');
        $this->load->view('front/footer_main_view', array("tablas" => TRUE, "exportar_tablas" => TRUE, "balanza" => TRUE));
    }

    function prueba_balanza($nivel = NULL, $cuenta_inicial = NULL, $cuenta_final = NULL)
    {
        log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ', ha consultado una prueba de balanza');
        echo('Nivel: ');
        $this->debugeo->imprimir_pre($nivel);
        echo('Cuenta Inicial: ');
        $this->debugeo->imprimir_pre($cuenta_inicial);
        echo('Cuenta Final: ');
        $this->debugeo->imprimir_pre($cuenta_final);

        $cuenta_inicial_separada = explode(".", $cuenta_inicial);

        $sql = "SELECT cuenta FROM cat_cuentas_contables WHERE cuenta LIKE ?";
        $query = $this->db->query($sql, array($cuenta_inicial_separada[0] . '%'));

        $primer_nivel_cuenta_inicial = $query->result_array();

        $arreglos_de_cuentas_iniciales = array();

        foreach ($primer_nivel_cuenta_inicial as $row) {
            $cuenta_de_base = explode(".", $row['cuenta']);
            $arreglos_de_cuentas_iniciales[] = $cuenta_de_base;
        }

        $longitud_de_cuenta = max($arreglos_de_cuentas_iniciales);
        $longitud_de_cuenta = count($longitud_de_cuenta);

        $this->debugeo->imprimir_pre($longitud_de_cuenta);

    }

    function consulta_balanza()
    {
        log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ', ha consultado una balanza');
        $datos = $this->input->post();

        foreach ($datos as $key => $value) {
            $datos[$key] = trim($value);
        }

//        $this->debugeo->imprimir_pre($datos);

        $datos_header = array(
            "titulo_pagina" => "Armonniza | Consulta Balanza",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/ver_conta_balanza_view', $datos);
        $this->load->view('front/footer_main_view', array("tablas" => TRUE, "consulta_balanza" => TRUE));

    }

    function generar_tabla_principal_balanza()
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

//        $this->benchmark->mark('code_start');

        $datos = $this->input->post();

//        $datos = array(
//            "subsidio" => "",
//            "centro_costos" => "",
//            "fecha_inicial" => "2015-01-01",
//            "fecha_final" => "2015-05-31",
//            "cuenta_inicial" => "",
//            "cuenta_final" => "",
//            "nivel_cuenta" => 99,
//            "tipo_radio" => 2,
//        );

//        $this->debugeo->imprimir_pre($datos);

        $truncar = $this->db->truncate('balanza_temporal');

        if ($truncar) {
//            Se toman todas las cuentas del catalogo de cuentas contables
            $sql = "SELECT id_cuentas_contables, saldo_inicial, cuenta, nombre, cargo, abono, cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables ORDER BY cuenta;";
            $query = $this->db->query($sql);
            $cuentas_contables = $query->result_array();

//        echo("Cuentas contables: ");
//        $this->debugeo->imprimir_pre($cuentas_contables);

//        Después se toman todos los movimientos anteriores a los datos especificados
            $sql_anterior = "SELECT ccc.id_cuentas_contables AS id_cuentas, pd.cuenta, SUM(pd.debe) AS cargo, SUM(pd.haber) AS abono, ccc.cuenta_padre AS id_padre, pd.nivel
                            FROM mov_poliza_detalle pd
                                JOIN mov_polizas_cabecera pc
                                ON pd.numero_poliza = pc.numero_poliza
                                JOIN cat_cuentas_contables ccc
                                ON pd.cuenta = ccc.cuenta
                            WHERE pc.fecha < ?
                                AND pd.subsidio LIKE ?
                                AND pd.centro_costo LIKE ?
                                AND pc.enfirme = 1
                                AND pc.cancelada = 0
                        GROUP BY cuenta ORDER BY cuenta;";
            $query_anterior = $this->db->query($sql_anterior, array($datos["fecha_inicial"], "%" . $datos["subsidio"] . "%", "%" . $datos["centro_costos"] . "%"));
            $tabla_temporal_anterior = $query_anterior->result_array();

//        echo("Movimientos anteriores a la consulta: ");
//        $this->debugeo->imprimir_pre($tabla_temporal_anterior);
//
//        Después se toman las pólizas que se generaron en la fecha determinada
            $sql_consulta = "SELECT ccc.id_cuentas_contables AS id_cuentas, ccc.saldo_inicial, pd.cuenta, SUM(pd.debe) AS cargo, SUM(pd.haber) AS abono, ccc.cuenta_padre AS id_padre, pd.nivel, pd.numero_poliza
                    FROM mov_poliza_detalle pd
                        JOIN mov_polizas_cabecera pc
                        ON pd.numero_poliza = pc.numero_poliza
                        JOIN cat_cuentas_contables   ccc
                        ON pd.cuenta = ccc.cuenta
                    WHERE pc.fecha >= ?
                            AND pc.fecha <= ?
                            AND pd.subsidio LIKE ?
                            AND pd.centro_costo LIKE ?
                            AND pc.enfirme = 1
                            AND pc.cancelada = 0
                            GROUP BY pd.cuenta;";
            $query_consulta = $this->db->query($sql_consulta, array($datos["fecha_inicial"], $datos["fecha_final"], "%" . $datos["subsidio"] . "%", "%" . $datos["centro_costos"] . "%"));
            $resultado_consulta = $query_consulta->result_array();

//        echo("Movimientos dentro de la consulta: ");
//        $this->debugeo->imprimir_pre($resultado_consulta);

//        Esta seccion es para calcular los movimientos
            foreach ($tabla_temporal_anterior as $key => $value) {

//            Se toma el indice padre de la cuenta, si es que lo tiene
                $key_padre = array_search($value["id_padre"], array_column($cuentas_contables, 'cuenta'));

//            Se recorre un ciclo donde, mientras la llave padre siga siendo diferente de 0, entra en él
                do {
//                Se suman los cargos y abonos de la tabla, al catalodgo de cuentas contables
                    $cuentas_contables[$key_padre]["cargo"] += $value["cargo"];
                    $cuentas_contables[$key_padre]["abono"] += $value["abono"];

//                Se toma el ID padre de la cuenta para poder ejecutar la recursividad, donde se van a volver a agregar los catgos y abonos, siempre y cuando el ID padre sea diferente de 0
                    $key_padre = array_search($cuentas_contables[$key_padre]["id_padre"], array_column($cuentas_contables, 'cuenta'));

                } while ($key_padre > -1);

//            Al finalizar el cálculo de los padres, se suman los cargos y abonos a la cuenta inicial, dentro del catálogo de cuentas
                $key_cuenta = array_search($value["cuenta"], array_column($cuentas_contables, 'cuenta'));

                $cuentas_contables[$key_cuenta]["cargo"] += $value["cargo"];
                $cuentas_contables[$key_cuenta]["abono"] += $value["abono"];

                unset($key);
                unset($value);
                unset($key_padre);
                unset($key_cuenta);

            }

            unset($key);
            unset($value);

//        echo("Cuentas contables después de calcular los movimientos anteriores al periodo: ");
//        $this->debugeo->imprimir_pre($cuentas_contables);

//        Se recorreo el arreglo de las cuentas contables para calcular el saldo final
            foreach ($cuentas_contables as $key => $value) {
                $cuentas_contables[$key]["saldo_final"] = $cuentas_contables[$key]["saldo_inicial"] + $cuentas_contables[$key]["cargo"] - $cuentas_contables[$key]["abono"];
                $cuentas_contables[$key]["saldo_inicial"] = $cuentas_contables[$key]["saldo_inicial"] + $cuentas_contables[$key]["cargo"] - $cuentas_contables[$key]["abono"];
//                $cuentas_contables[$key]["cargo"] = 0;
//                $cuentas_contables[$key]["abono"] = 0;
                $cuentas_contables[$key]["saldo_final"] = 0;
            }

            unset($key);
            unset($value);

//        echo("Cuentas contables después de calcular el saldo inicial ");
//        $this->debugeo->imprimir_pre($cuentas_contables);

//        Se recorre el arreglo de la consulta generada por el usuario
            foreach ($resultado_consulta as $key => $value) {
//                $this->debugeo->imprimir_pre($value["numero_poliza"]);
//
//                if($value["numero_poliza"] >= 12867) {
//                    $this->debugeo->imprimir_pre($value);
//                }

//            Se toma el indice padre de la cuenta, si es que lo tiene
                $key_padre = array_search($value["id_padre"], array_column($cuentas_contables, 'cuenta'));
//            echo("Llave padre inicial: ".$key_padre."<br />");

//            Se recorre un ciclo donde, mientras la llave padre siga siendo diferente de 0, entra en él
                do {
//                    $this->debugeo->imprimir_pre($value);

//                echo("Cuentas contables padre antes de agregar los cargos y abonos: ");
//                $this->debugeo->imprimir_pre($cuentas_contables[$key_padre]);

//                Se suman los cargos y abonos de la tabla, al catalodgo de cuentas contables
                    $cuentas_contables[$key_padre]["cargo"] += $value["cargo"];
                    $cuentas_contables[$key_padre]["abono"] += $value["abono"];

//                echo("Cuentas contables padre después de agregar los cargos y abonos: ");
//                $this->debugeo->imprimir_pre($cuentas_contables[$key_padre]);

//                Se toma el ID padre de la cuenta para poder ejecutar la recursividad, donde se van a volver a agregar los catgos y abonos, siempre y cuando el ID padre sea diferente de 0
                    $key_padre = array_search($cuentas_contables[$key_padre]["id_padre"], array_column($cuentas_contables, 'cuenta'));

//                echo("Nuevo ID Padre: ".$key_padre."<br />");

                } while ($key_padre > -1);

//            Al finalizar el cálculo de los padres, se suman los cargos y abonos a la cuenta inicial, dentro del catálogo de cuentas
                $key_cuenta = array_search($value["cuenta"], array_column($cuentas_contables, 'cuenta'));

//            echo("Ya salió del ciclo:<br />");
//            echo("ID de la cuenta: ".$key_cuenta."<br />");

//            echo("Cuentas contables antes de agregar los cargos y abonos al ID de la cuenta: ");
//            $this->debugeo->imprimir_pre($cuentas_contables[$key_cuenta]);

                $cuentas_contables[$key_cuenta]["cargo"] += $value["cargo"];
                $cuentas_contables[$key_cuenta]["abono"] += $value["abono"];

//            echo("Cuentas contables después de agregar los cargos y abonos al ID de la cuenta: ");
//            $this->debugeo->imprimir_pre($cuentas_contables[$key_cuenta]);

            }

            unset($key);
            unset($value);

            if (isset($datos["cuenta_inicial"]) && $datos["cuenta_inicial"] != "") {
                $cuenta_indice_inicial = array_search($datos["cuenta_inicial"], array_column($cuentas_contables, 'cuenta'));
            } else {
                $cuenta_indice_inicial = 0;
            }

            if (isset($datos["cuenta_final"]) && $datos["cuenta_final"] != "") {
                $cuenta_indice_final = array_search($datos["cuenta_final"], array_column($cuentas_contables, 'cuenta'));
            } else {
                end($cuentas_contables);
                $cuenta_indice_final = key($cuentas_contables);
            }


//        echo("Cuentas contables después de calcular el saldo actual: ");
//        $this->debugeo->imprimir_pre($cuentas_contables);

//        Se recorreo el arreglo de las cuentas contables para calcular el saldo final
            foreach ($cuentas_contables as $key => $value) {
//                $this->debugeo->imprimir_pre($key);
//                $this->debugeo->imprimir_pre($value);

                if ($key >= $cuenta_indice_inicial && $key <= $cuenta_indice_final) {
//                    $this->debugeo->imprimir_pre($value);

                    $nivel = explode('.', $value['cuenta']);
                    $nivel_cuenta = count($nivel);

                    if ($nivel_cuenta <= $datos["nivel_cuenta"]) {
//                        $this->debugeo->imprimir_pre($value);

                        $datos_insertar = array(
                            'cuenta' => $cuentas_contables[$key]["cuenta"],
                            'nombre' => $cuentas_contables[$key]["nombre"],
                            'saldo_inicial' => $cuentas_contables[$key]["saldo_inicial"],
                            'cargo' => $cuentas_contables[$key]["cargo"],
                            'abono' => $cuentas_contables[$key]["abono"],
                            'saldo_final' => ($cuentas_contables[$key]["saldo_inicial"] + $cuentas_contables[$key]["cargo"]) - $cuentas_contables[$key]["abono"],
                            'id_padre' => $cuentas_contables[$key]["id_padre"],
                            'nivel' => $cuentas_contables[$key]["nivel"],
                        );

                        $this->db->insert('balanza_temporal', $datos_insertar);
                    }
                } else {
                    continue;
                }

            }

//        echo("Cuentas contables después de calcular el saldo final: ");
//        $this->debugeo->imprimir_pre($cuentas_contables);

//        $this->benchmark->mark('code_end');
//        echo $this->benchmark->elapsed_time('code_start', 'code_end');

            echo(json_encode(TRUE));
        }

    }

    function tabla_balanza()
    {
        $truncar = $this->db->truncate('balanza_temporal');

        $datos = $this->input->post();

        foreach ($datos as $key => $value) {
            $datos[$key] = trim($value);
        }

//        $this->debugeo->imprimir_pre($datos);

        $datos_header = array(
            "titulo_pagina" => "Armonniza | Balanza",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
            "precompromisocss" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/balanza_view', $datos);
        $this->load->view('front/footer_main_view', array("exportar_tablas" => TRUE, "consulta_balanza" => TRUE));
    }

    function tabla_principal_balanza()
    {
//        $datos = $this->input->post();
//        $datos = array(
//            "centro_costos" => "",
//            "cuenta_final" => "",
//            "cuenta_inicial" => "",
//            "fecha_final" => "2015-12-31",
//            "fecha_inicial" => "2015-01-01",
//            "nivel_cuenta" => "",
//            "subsidio" => "Ingresos Propios",
//        );

//        foreach ($datos as $key => $value) {
//            $datos[$key]=trim($value);
//        }

//        if(!$datos["cuenta_inicial"]) {
//            $sql_inicial = "SELECT cuenta FROM cat_cuentas_contables ORDER BY cuenta ASC LIMIT 1;";
//            $query_inicial = $this->db->query($sql_inicial);
//            $cuenta_inicial = $query_inicial->row_array();
//            $datos["cuenta_inicial"] = $cuenta_inicial["cuenta"];
//        }
//
//        if(!$datos["cuenta_final"]) {
//            $sql_final = "SELECT cuenta FROM cat_cuentas_contables ORDER BY cuenta DESC LIMIT 1;";
//            $query_final = $this->db->query($sql_final);
//            $cuenta_final = $query_final->row_array();
//            $datos["cuenta_final"] = $cuenta_final["cuenta"];
//        }

//        $resultado = $this->contabilidad_model->obtener_datos_balanza($datos);
        $resultado = $this->contabilidad_model->obtener_datos_balanza();

        foreach ($resultado as $fila) {
//            $this->debugeo->imprimir_pre($fila);
            $output["data"][] = array(
                $fila["cuenta"],
                $fila["nombre"],
                '<div class="table-formant-sign">' . "$" . '</div><div class="table-formant-coin">' . ($fila["saldo_inicial"] == 0 || $fila["saldo_inicial"] == NULL ? '0.00' : (number_format($fila["saldo_inicial"], 2)) . '</div>'),
                '<div class="table-formant-sign">' . "$" . '</div><div class="table-formant-coin">' . ($fila["cargo"] == 0 || $fila["cargo"] == NULL ? '0.00' : (number_format($fila["cargo"], 2)) . '</div>'),
                '<div class="table-formant-sign">' . "$" . '</div><div class="table-formant-coin">' . ($fila["abono"] == 0 || $fila["abono"] == NULL ? '0.00' : (number_format($fila["abono"], 2)) . '</div>'),
                '<div class="table-formant-sign">' . "$" . '</div><div class="table-formant-coin">' . ($fila["saldo_final"] == 0 || $fila["saldo_final"] == NULL ? '0.00' : (number_format($fila["saldo_final"], 2)) . '</div>'),
            );
        }
//        $this->debugeo->imprimir_pre($output);
        echo json_encode($output);

    }

    function imprimir_reporte_consulta_balanza()
    {
        log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ', ha Impreso un reporte de consulta de balanza');
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

        $tabla_general = '<table style="font-size: small;  border: 1px solid #BDBDBD;" cellpadding="4">
                            <tr>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="19%">No. Cuenta</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;">Nombre Cuenta</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="20%">Saldo Inicial</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="20%">Debe</td>
                                <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="20%">Haber</td>
                                <td align="center" style="font-weight: bold; border-bottom: 1px solid #BDBDBD;" width="20%">Saldo Final</td>
                            </tr>';

//        $datos = $this->input->post();

        $datos = array(
            "fecha_inicial" => "2015-01-01",
            "fecha_final" => "2015-12-31",
        );

    }
    /* function imprimir_reporte_consulta_balanza() {
         $datos = $this->input->post();

         $fecha = date("Y-m-d");
         $hora = date("H:i:s");

         $this->load->library('Pdf');
         $datos_procesos = $this->contabilidad_model->obtener_datos_balanza();
 //      $this->debugeo->imprimir_pre($datos_procesos);


         $pdf = new Pdf('L', 'cm', 'A4', true, 'UTF-8', false);
         // set document information
         $pdf->SetCreator(PDF_CREATOR);
         $pdf->SetAuthor('Armonniza');
         $pdf->SetTitle('Balanza de Comprobación');
         $pdf->SetSubject('Balanza de Comprobación');
         $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

 // set default header data
 //        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

 // set header and footer fonts
 //        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
         // remove default header/footer
         $pdf->setPrintHeader(false);
         $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

 // set default monospaced font
 //        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

 // set margins
         $pdf->SetMargins(PDF_MARGIN_LEFT, 15 , PDF_MARGIN_RIGHT);
 //        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
         $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

 // set auto page breaks
         $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

 // ---------------------------------------------------------

 // set font
         $pdf->SetFont('helvetica', '', 12);

 // add a page
         $pdf->AddPage('L', 'Letter');

         $encabezado = '<table cellspacing="3" style="font-size: small;">
                         <tr><td></td><td></td><td></td></tr>
                         <tr>
                             <td align="left"><b>&nbsp; &nbsp; &nbsp;Período</b></td>
                             <td align="center"><b>Fecha Emisión</b></td>
                             <td align="right"><b>Hora Emisión</b></td>
                         </tr>
                         <tr>
                             <td align="left">&nbsp; &nbsp; &nbsp;'.$datos["fecha_inicial"].' &nbsp; al &nbsp; '.$datos["fecha_final"].'</td>
                             <td align="center">'.$fecha.'</td>
                             <td align="right">'.$hora.'</td>
                         </tr>
                        </table>';

         foreach($datos_procesos as $fila) {

             $tabla_general .= '<tr>
                                    <td align="justify" style=" border-right: 1px solid #BDBDBD;" width="19%">'.$fila["cuenta"].'</td>
                                    <td align="left" width="20%">$ '.($fila["saldo_inicial"] == 0 || $fila["saldo_inicial"] == NULL ? '0.00' : $this->cart->format_number(round($fila["saldo_inicial"], 2))) .'</td>
                                    <td align="right" style=" border-right: 1px solid #BDBDBD;" width="20%">$ '.($fila["cargo"] == 0 || $fila["cargo"] == NULL ? '0.00' : $this->cart->format_number(round($fila["cargo"], 2))) .'</td>
                                    <td align="left" width="20%">$ '.($fila["abono"] == 0 || $fila["abono"] == NULL ? '0.00' : $this->cart->format_number(round($fila["abono"], 2))) .'</td>
                                    <td  align="right" width=20%">$ '.($fila["saldo_final"] == 0 || $fila["saldo_final"] == NULL ? '0.00' : $this->cart->format_number(round($fila["saldo_final"], 2))) .'</td>
                                </tr>';
         }
         $tabla_general .= '</table>';
 //      $this->debugeo->imprimir_pre($tabla_general);
         //foreach($datos_procesos as $fila) {
             $tabla_general = '<tr>
                                    <td align="justify" style=" border-right: 1px solid #BDBDBD;" width="19%"></td>
                                    <td align="left" width="20%">$ </td>
                                    <td align="right" style=" border-right: 1px solid #BDBDBD;" width="20%">$ </td>
                                    <td align="left" width="20%">$ </td>
                                    <td  align="right" width=20%">$ </td>
                                </tr>';
         //}
 >>>>>>> Stashed changes

         $html = '
         <style>
             .cont-general{
                 border: 1px solid #eee;
                 border-radius: 1%;
                 margin: 2% 14%;
             }
             .cont-general2{
                 border: 1px solid #BDBDBD;
             }
             .cont-general .borde-inf{
                 border-bottom: 1px solid #eee;
             }
              .cont-general .borde-sup{
                 border-top: 1px solid #eee;
             }
         </style>

         <table class="cont-general" border="0" cellspacing="3" style="font-size: small;" cellpadding="2">
             <tr cellspacing="10">
                 <th width="2%"></th>
                 <th align="left" width="10%"><img src="'.base_url("img/logo2.png").'" /></th>
                 <th align="center" width="84%" style="line-height: 7px;">
                     <h3>EDUCAL, S.A. DE C.V.</h3>
                     <h5>Balanza de Comprobación</h5>
                     '.$encabezado.'
                 </th>
                 <th width="2%"></th>
             </tr>
             <tr><td width="100%" class="borde-sup"></td></tr>
             <tr>
                 <!-- <td width="2%"></td> -->
                 <td width="96%">
                     '.$tabla_general.'
                 </td>
                 <!-- <td width="2%"></td> -->
             </tr>
             <tr><td width="100%" class="borde-inf"></td></tr>
             <tr><td width="100%"></td></tr>
             <tr><td width="100%"></td></tr>
         </table>';

         $this->debugeo->imprimir_pre($html);

 // output the HTML content

         $pdf->writeHTML($html, false, false, true, false, 'top');

 //Close and output PDF document
         $pdf->Output('Balanza de Comprobación.pdf', 'I');
     } */

    /* function imprimir_reporte_consulta_balanza() {
 //        $datos = $this->input->post();
 //        $this->debugeo->imprimir_pre($datos);

         $fecha = date("Y-m-d");
         $hora = date("H:i:s");

         $datos_procesos = $this->contabilidad_model->obtener_datos_balanza();
 //        $this->debugeo->imprimir_pre($datos_procesos);

         $this->load->library('Pdf');

         $pdf = new Pdf('L', 'cm', 'A4', true, 'UTF-8', false);
         // set document information
         $pdf->SetCreator(PDF_CREATOR);
         $pdf->SetAuthor('Armonniza');
         $pdf->SetTitle('Consulta Balanza');
         $pdf->SetSubject('Consulta Balanza');
         $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

 // set default header data
 //        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

 // set header and footer fonts
 //        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
         // remove default header/footer
         $pdf->setPrintHeader(false);
         $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

 // set default monospaced font
 //        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

 // set margins
         $pdf->SetMargins(PDF_MARGIN_LEFT, 10 , PDF_MARGIN_RIGHT);
 //        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
         $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

 // set auto page breaks
         $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

 // ---------------------------------------------------------

 // set font
         $pdf->SetFont('helvetica', '', 12);

 // add a page
         $pdf->AddPage('L', 'Letter');

         $tabla_estructura = '<table style="font-size: small;  border: 1px solid #BDBDBD;" cellpadding="4">
                             <tr>
                                  <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="18.5%">Cuenta</td>
                                  <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="20%">Saldo Inicial</td>
                                  <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="20%">Cargos</td>
                                  <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="20%">Abonos</td>
                                  <td align="center" style="font-weight: bold; border-bottom: 1px solid #BDBDBD;" width="20%">Saldo Final</td>
                             </tr>';

          foreach($datos_procesos as $fila) {
              $tabla_estructura .= '<tr>';
              $tabla_estructura .= '<td align="left" width="18.5%">'.$fila["cuenta"].'</td>';
              $tabla_estructura .= '<td align="left" width="20%"></td>';
              $tabla_estructura .= '<td align="left" width="20%"></td>';
              $tabla_estructura .= '<td align="left" width="20%"></td>';
              $tabla_estructura .= '<td align="left" width="20%">$ </td>';
              $tabla_estructura .= '</tr>';
          }

         $tabla_estructura .= '</table>';

         $encabezado = '<table cellspacing="3" style="font-size: small;">
                         <tr>
                             <td align="left"><b>&nbsp; &nbsp; &nbsp;Período</b></td>
                             <td align="center"><b>Fecha Emisión</b></td>
                             <td align="right"><b>Hora Emisión</b></td>
                         </tr>
                         <tr>
                             <td align="left">&nbsp; &nbsp; &nbsp;</td>
                             <td align="center">'.$fecha.'</td>
                             <td align="right">'.$hora.'</td>
                         </tr>
                        </table>';

         $html = '
         <style>
             .cont-general{
                 border: 1px solid #eee;
                 border-radius: 1%;
                 margin: 2% 14%;
             }
             .cont-general2{
                 border: 1px solid #BDBDBD;
             }
             .cont-general .borde-inf{
                 border-bottom: 1px solid #eee;
             }
             .cont-general .borde-sup{
                 border-top: 1px solid #eee;
             }
         </style>

         <table class="cont-general" border="0" cellspacing="3" style="font-size: small;" cellpadding="2">
             <tr cellspacing="10">
                 <th width="2%"></th>
                 <th align="left" width="10%"><img src="'.base_url("img/logo2.png").'" /></th>
                 <th align="center" width="84%" style="line-height: 7px;">
                     <h3>EDUCAL, S.A. DE C.V.</h3>
                     <h5> Consulta Balanza </h5>
                     '.$encabezado.'
                 </th>
                 <th width="2%"></th>
             </tr>
             <tr><td width="100%" class="borde-sup"></td></tr>
            '.$tabla_estructura.'
             <tr><td width="100%"></td></tr>

 </table>';

 // output the HTML content

         $pdf->writeHTML($html, false, false, true, false, 'top');

 //Close and output PDF document
         $pdf->Output('Precompromisos Autorizados.pdf', 'I');

     }*/

    function exportar_consulta_balanza()
    {
        log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ', ha exportado una consulta de balanza');

        $datos = $this->input->post();
        $this->load->library('excel');
        $fecha = date("Y-m-d");
        $hora = date("H:i:s");

        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Consulta de Balanza');
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('Educal');
        $objDrawing->setPath('img/logo2.png');
        $objDrawing->setOffsetX(50);
        $objDrawing->setHeight(80);
        // $objDrawing->setWidth(10);
        $objDrawing->setCoordinates('A1');
        $objDrawing->setWorksheet($this->excel->getActiveSheet());

        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
            'font' => array(
                'bold' => true,
            )
        );
        $style_center = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );
        $style_left = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            )
        );

        $this->excel->getActiveSheet()->mergeCells('A1:F1')->getStyle("A1")->applyFromArray($style);
        $this->excel->getActiveSheet()->setCellValue('A1', 'EDUCAL, S.A. DE C.V.');
        $this->excel->getActiveSheet()->mergeCells('A2:F2')->getStyle("A2")->applyFromArray($style);
        $this->excel->getActiveSheet()->setCellValue('A2', 'Consulta de Balanza');
        $this->excel->getActiveSheet()->getStyle("B3:F3")->applyFromArray($style);
        $this->excel->getActiveSheet()->getStyle("B4:F4")->applyFromArray($style_center);
        $this->excel->getActiveSheet()->setCellValue('B3', 'Período');
        $this->excel->getActiveSheet()->setCellValue('D3', 'Fecha Emisión');
        $this->excel->getActiveSheet()->setCellValue('F3', 'Hora Emisión');
        $this->excel->getActiveSheet()->setCellValue('B4', $datos["fecha_inicial"] . ' al ' . $datos["fecha_final"]);
        $this->excel->getActiveSheet()->setCellValue('D4', $fecha);
        $this->excel->getActiveSheet()->setCellValue('F4', $hora);
        $this->excel->getActiveSheet()->getStyle("A6:F6")->applyFromArray($style);
        $this->excel->getActiveSheet()->mergeCells('A5:F5');

        //set cell A1 content with some text

        $this->excel->getActiveSheet()->getStyle("A")->applyFromArray($style_left);
        $this->excel->getActiveSheet()->setCellValue('A6', 'Cuenta')->getColumnDimension("A")->setWidth(25);
        $this->excel->getActiveSheet()->setCellValue('B6', 'Nombre')->getColumnDimension("B")->setWidth(35);
        $this->excel->getActiveSheet()->setCellValue('C6', 'Saldo Inicial')->getColumnDimension("C")->setWidth(25);
        $this->excel->getActiveSheet()->setCellValue('D6', 'Cargos')->getColumnDimension("D")->setWidth(25);
        $this->excel->getActiveSheet()->setCellValue('E6', 'Abonos')->getColumnDimension("E")->setWidth(25);
        $this->excel->getActiveSheet()->setCellValue('F6', 'Saldo Final')->getColumnDimension("F")->setWidth(25);

//        $this->db->select('cuenta, saldo_inicial, cargo, abono, saldo_final')->from('balanza_temporal');
//        $query = $this->db->get();
//        $resultado = $query->result_array();
        $resultado = $this->contabilidad_model->obtener_datos_balanza();

        $row = 7;

//        $this->benchmark->mark('code_start');

        foreach ($resultado as $fila) {
//            $this->debugeo->imprimir_pre($fila);
            $this->excel->getActiveSheet()->setCellValue('A' . $row, $fila["cuenta"]);
            $this->excel->getActiveSheet()->setCellValue('B' . $row, $fila["nombre"]);
            $this->excel->getActiveSheet()->setCellValue('C' . $row, '$ ' . ($fila["saldo_inicial"] == 0 || $fila["saldo_inicial"] == NULL ? '0.00' : number_format($fila["saldo_inicial"], 2)));
            $this->excel->getActiveSheet()->setCellValue('D' . $row, '$ ' . ($fila["cargo"] == 0 || $fila["cargo"] == NULL ? '0.00' : number_format($fila["cargo"], 2)));
            $this->excel->getActiveSheet()->setCellValue('E' . $row, '$ ' . ($fila["abono"] == 0 || $fila["abono"] == NULL ? '0.00' : number_format($fila["abono"], 2)));
            $this->excel->getActiveSheet()->setCellValue('F' . $row, '$ ' . ($fila["saldo_final"] == 0 || $fila["saldo_final"] == NULL ? '0.00' : number_format($fila["saldo_final"], 2)));
            $row += 1;
        }

//        $this->benchmark->mark('code_end');

//        echo $this->benchmark->elapsed_time('code_start', 'code_end');

        $filename = "Consulta Balanza.xls"; // Agregar fecha en que se generó
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
//        save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
//        if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
//        force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }

    /** Plan de Cuenta */

    function plancuentas()
    {
        log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ', ha consultado el plan de cuentas');
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Plan de Cuentas Contables",
            "usuario" => $this->tank_auth->get_username(),
            "editar_tabla" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/catalogos/catalogo_plan_cuentas_view');
        $this->load->view('front/footer_main_view', array("exportar_tablas" => TRUE, "catalogo_cuentas" => TRUE));
    }

    function tabla_cuenta()
    {
        include("./assets/datatables/extensions/Editor-1.3.3/php/DataTables.php");

        $resultado = DataTables\Editor::inst($db, 'cat_cuentas_contables')
            ->pkey('id_cuentas_contables')
            ->fields(
                DataTables\Editor\Field::inst('id_cuentas_contables')->validator('DataTables\Editor\Validate::notEmpty'),
                DataTables\Editor\Field::inst('cuenta')->validator('DataTables\Editor\Validate::notEmpty'),
                DataTables\Editor\Field::inst('nombre')->validator('DataTables\Editor\Validate::notEmpty'),
                DataTables\Editor\Field::inst('tipo')->validator('DataTables\Editor\Validate::notEmpty'),
                DataTables\Editor\Field::inst('descripcion')->validator('DataTables\Editor\Validate::notEmpty'),
                DataTables\Editor\Field::inst('cuenta_padre')->validator('DataTables\Editor\Validate::notEmpty')
            )
            ->process($_POST)
            ->json();

        //       $this->debugeo->imprimir_pre($resultado);

    }

    function exportar_plan_cuentas()
    {
        log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ', ha exportado el plan de cuentas');
        $this->load->library('excel');

        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Plan de Cuentas');
        //set cell A1 content with some text
        $this->excel->getActiveSheet()->setCellValue('A1', 'Cuenta');
        $this->excel->getActiveSheet()->setCellValue('B1', 'Nombre');
        $this->excel->getActiveSheet()->setCellValue('C1', 'Tipo');
        $this->excel->getActiveSheet()->setCellValue('D1', 'Saldo Inicial');
        $this->excel->getActiveSheet()->setCellValue('E1', 'Cargo');
        $this->excel->getActiveSheet()->setCellValue('F1', 'Abono');
        $this->excel->getActiveSheet()->setCellValue('G1', 'Saldo');

        $this->db->select('cuenta, nombre, tipo, saldo_inicial, cargo, abono, saldo')->from('cat_cuentas_contables')->order_by("cuenta");
        $query = $this->db->get();
        $resultado = $query->result_array();

        $row = 2;

//        $this->benchmark->mark('code_start');

        foreach ($resultado as $fila) {
//            $this->debugeo->imprimir_pre($fila);
            $this->excel->getActiveSheet()->setCellValue('A' . $row, $fila["cuenta"]);
            $this->excel->getActiveSheet()->setCellValue('B' . $row, $fila["nombre"]);
            if ($fila["tipo"] == "A") {
                $this->excel->getActiveSheet()->setCellValue('C' . $row, "Acumulativa");
            } else {
                $this->excel->getActiveSheet()->setCellValue('C' . $row, "Detalle");
            }
            $this->excel->getActiveSheet()->setCellValue('D' . $row, $fila["saldo_inicial"]);
            $this->excel->getActiveSheet()->setCellValue('E' . $row, $fila["cargo"]);
            $this->excel->getActiveSheet()->setCellValue('F' . $row, $fila["abono"]);
            $this->excel->getActiveSheet()->setCellValue('G' . $row, $fila["saldo"]);

            $row += 1;
        }

//        $this->benchmark->mark('code_end');

//        echo $this->benchmark->elapsed_time('code_start', 'code_end');

        $filename = "Plan de cuentas.xls"; // Agregar fecha en que se generó
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
//        save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
//        if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
//        force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }

    function select_cuentas()
    {
        $this->db->select('cuenta')->from('cat_cuentas_contables');
        $query = $this->db->get();
        echo(json_encode($query->result_array()));
    }

    /** Aqui empieza la sección de Matriz de Conversión */
    function matriz()
    {
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Matriz de Conversión",
            "usuario" => $this->tank_auth->get_username(),
            "editar_tabla" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/conta_matriz_view');
        $this->load->view('front/footer_main_view', array("exportar_tablas" => TRUE, "matrizjs" => TRUE));
    }

    function tabla_indice_matriz()
    {
        include("./assets/datatables/extensions/Editor-1.3.3/php/DataTables.php");

        DataTables\Editor::inst($db, 'cat_correlacion_partidas_contables')
            ->pkey('id_correlacion_partidas_contables')
            ->fields(
                DataTables\Editor\Field::inst('id_correlacion_partidas_contables'),
                DataTables\Editor\Field::inst('clave'),
                DataTables\Editor\Field::inst('descripcion'),
                DataTables\Editor\Field::inst('cuenta_cargo'),
                DataTables\Editor\Field::inst('nombre_cargo'),
                DataTables\Editor\Field::inst('cuenta_abono'),
                DataTables\Editor\Field::inst('nombre_abono')
            )
            ->process($_POST)
            ->json();

    }

    function agregar_matriz()
    {
        log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ', ha agregado una matriz');
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Agregar Matriz",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/agregar_matriz_view');
        $this->load->view('front/footer_main_view', array("tablas" => TRUE, "agregar_matriz" => TRUE));
    }

    function subir_matriz() {
        $respuesta = array(
            "mensaje" => "",
        );

        log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ', ha subido una matriz');
//        Se prepara la configuracion de la libreria "Upload"
        $config['upload_path'] = './application/archivos/';
        $config['allowed_types'] = '*';

//        Se carga la libreria con sus configuraciones correspondientes
        $this->load->library('upload', $config);

//        Si el server no logra subir el archivo, despliega un mensaje de error
        if (!$this->upload->do_upload('archivoSubir')) {
//            Se capturan los errores en una variable y se imprimen para debug
            $error = array('error' => $this->upload->display_errors());
            $this->debugeo->imprimir_pre($error);
        } else {
            ini_set('memory_limit', '-1');
//            load our new PHPExcel library
            $this->load->library('excel');
//            Si el server logra subir el archivo, se toman los datos del archivo
            $data = $this->upload->data();

//            Se toma la ruta del archivo junto con su nombre y extension
            $file = $data["full_path"];

//            read file from path
            $objPHPExcel = PHPExcel_IOFactory::load($file);

//            get only the Cell Collection
            $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();

            try {

                //                extract to a PHP readable array format
                foreach ($cell_collection as $cell) {
                    $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
                    $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
                    $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();

//                    header will/should be in row 1 only. of course this can be modified to suit your need.
                    if ($row <= 1) {
                        continue;
                    } elseif ($row >= 2) {
                            // throw new Exception('Ha ocurrido un error en el encabezado del documento, por favor revíselo.');
                        $datos_detalle[$row][$column] = trim($data_value);
                    }
                }

                $clave = array(
                    'clave' => "",
                    'descripcion' => "",
                );

                $linea = 2;

                // Se verifica que la partida exista
                foreach ($datos_detalle as $key => $value) {
                    $partida = $this->contabilidad_model->verificar_partida($value["A"]);

                    if(!isset($partida["partida"])) {
                        $centro_recaudacion = $this->contabilidad_model->verificar_centro_recaudacion($value["A"]);

                        if(!isset($centro_recaudacion["centro_de_recaudacion"])) {
                            throw new Exception('La partida en la l&iacute;nea '.$linea.' no existe.');
                        } else {
                            $clave["clave"] = $centro_recaudacion["centro_de_recaudacion"];
                            $clave["descripcion"] = $centro_recaudacion["descripcion"];
                        }
                    } else {
                        $clave["clave"] = $partida["partida"];
                        $clave["descripcion"] = $partida["descripcion"];
                    }

                    $cuenta_cargo = $this->contabilidad_model->verificar_cuenta_cargo($value["B"]);

                    if(!isset($cuenta_cargo["cuenta"])) {
                        throw new Exception('La cuenta de cargo en la l&iacute;nea '.$linea.' no existe.');
                    }

                    $cuenta_abono = $this->contabilidad_model->verificar_cuenta_cargo($value["C"]);

                    if(!isset($cuenta_abono["cuenta"])) {
                        throw new Exception('La cuenta de abono en la l&iacute;nea '.$linea.' no existe.');
                    }

                    $value["partida"] = $clave["clave"];
                    $value["descripcion_partida"] = $clave["descripcion"];

                    $value["cuenta_cargo"] = $cuenta_cargo["cuenta"];
                    $value["descripcion_cuenta_cargo"] = $cuenta_cargo["descripcion"];

                    $value["cuenta_abono"] = $cuenta_abono["cuenta"];
                    $value["descripcion_cuenta_abono"] = $cuenta_abono["descripcion"];

                    $resultado_insertar = $this->contabilidad_model->insertar_matriz($value);

                    $linea += 1;

                }

                $respuesta["mensaje"] = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>La matriz fue introducida de manera correcta.</div>';

            } catch (Exception $e) {

                $respuesta["mensaje"] = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$e->getMessage().'</div>';

            }

            $datos_header = array(
                "titulo_pagina" => "Armonniza | Resultado Archivo",
                "usuario" => $this->tank_auth->get_username(),
                "precompromisocss" => TRUE,
            );

            $this->parser->parse('front/header_main_view', $datos_header);
            $this->load->view('front/resultado_archivo_view', $respuesta);
            $this->load->view('front/footer_main_view');

        }

    }

    function tomar_datos_cog()
    {
        include("./assets/datatables/extensions/Editor-1.3.3/php/DataTables.php");

        $resultado = DataTables\Editor::inst($db, 'cat_proveedores')
            ->pkey('id_proveedores')
            ->fields(
                DataTables\Editor\Field::inst('id_proveedores'),
                DataTables\Editor\Field::inst('nombre')->validator('DataTables\Editor\Validate::notEmpty'),
                DataTables\Editor\Field::inst('calle'),
                DataTables\Editor\Field::inst('telefono'),
                DataTables\Editor\Field::inst('no_exterior'),
                DataTables\Editor\Field::inst('no_interior'),
                DataTables\Editor\Field::inst('colonia'),
                DataTables\Editor\Field::inst('cp'),
                DataTables\Editor\Field::inst('ciudad'),
                DataTables\Editor\Field::inst('delegacion_municipio'),
                DataTables\Editor\Field::inst('pais'),
                DataTables\Editor\Field::inst('email'),
                DataTables\Editor\Field::inst('RFC'),
                DataTables\Editor\Field::inst('cuenta'),
                DataTables\Editor\Field::inst('clabe'),
                DataTables\Editor\Field::inst('banco'),
                DataTables\Editor\Field::inst('pago'),
                DataTables\Editor\Field::inst('mypyme'),
                DataTables\Editor\Field::inst('observaciones')
            )
            ->process($_POST)
            ->json();
    }

    /** Aqui empieza la sección de Auxiliar */
    function auxiliar()
    {
        log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ', ha entrado en la seccción auxiliar');
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Auxiliar",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
            "reportescss" => TRUE,
            "editar_tabla" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/conta_auxiliar_view');
        $this->load->view('front/footer_main_view', array("tablas" => TRUE, "exportar_tablas" => TRUE, "balanza" => TRUE));
    }

    function imprimir_reporte_auxiliar()
    {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha impreso un reporte auxiliar');

        $datos = $this->input->post();
        $cuenta_inicial = $datos["cuenta_inicial"];
        $cuenta_final = $datos["cuenta_final"];
//     $this->debugeo->imprimir_pre($datos);

        $fecha = date("Y-m-d");
        $hora = date("H:i:s");

        $query = "SELECT pc.fecha AS fecha_caratula, pc.estatus, pc.enfirme, pd.*
                FROM mov_poliza_detalle pd
                INNER JOIN mov_polizas_cabecera pc ON pd.numero_poliza = pc.numero_poliza
                WHERE pd.cuenta >= ?
                AND pd.cuenta <= ?
                AND pc.enfirme = 1
                AND pc.estatus = 'activo'
                AND pc.fecha >= ?
                AND pc.fecha <= ?;";

        $datos_detalle = $this->contabilidad_model->get_datos_polizas_detalle($query, $cuenta_inicial, $cuenta_final, $datos);

        $datos_cuenta = $this->contabilidad_model->get_datos_cuenta($cuenta_inicial);

        $this->load->library('Pdf');

        $pdf = new Pdf('L', 'cm', 'A4', true, 'UTF-8', false);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Armonniza');
        $pdf->SetTitle('Auxiliar');
        $pdf->SetSubject('Auxiliar');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
//        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
//        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        // remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
//        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, 10, PDF_MARGIN_RIGHT);
//        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// ---------------------------------------------------------

// set font
        $pdf->SetFont('helvetica', '', 12);

// add a page
        $pdf->AddPage('L', 'Letter');

        $encabezado = '<table cellspacing="3" style="font-size: small;">
                        <tr>
                            <td align="left"><b>&nbsp; &nbsp; &nbsp;Período</b></td>
                            <td align="center"><b>Fecha Emisión</b></td>
                            <td align="right"><b>Hora Emisión</b></td>
                        </tr>
                        <tr>
                            <td align="left">&nbsp; &nbsp; &nbsp;' . $datos["fecha_inicial"] . ' &nbsp; al &nbsp; ' . $datos["fecha_final"] . '</td>
                            <td align="center">' . $fecha . '</td>
                            <td align="right">' . $hora . '</td>
                        </tr>
                       </table>';
        $tabla_datos = '';
        if (!isset($datos_cuenta->saldo_inicial) || $datos_cuenta->saldo_inicial == NULL || $datos_cuenta->saldo_inicial == "") {
            $saldo = 0;
        } else {
            $saldo = $datos_cuenta->saldo_inicial;
        }
        $total_debe = 0;
        $total_haber = 0;

        foreach ($datos_detalle as $fila) {
            if (isset($fila->debe) && $fila->debe != 0) {
                $saldo += $fila->debe;
            } elseif (isset($fila->haber) && $fila->haber != 0) {
                $saldo -= $fila->haber;
            }
            $tabla_datos .= '<tr>
                                 <td align="center" width="6%" style="border-right: 1px solid #BDBDBD;">' . $fila->numero_poliza . '</td>
                                 <td align="center" width="8%" style="border-right: 1px solid #BDBDBD;">' . $fila->tipo_poliza . '</td>
                                 <td align="center" width="10%" style="border-right: 1px solid #BDBDBD;">' . $fila->fecha_caratula . '</td>
                                 <td align="justify" width="29.5%" style="border-right: 1px solid #BDBDBD;">' . $fila->concepto . '</td>
                                 <td align="left" width="2%">$</td>
                                 <td align="right" width="13%" style="border-right: 1px solid #BDBDBD;">' . ($fila->debe == 0 || $fila->debe == NULL ? '0.00' : $this->cart->format_number(round($fila->debe, 2))) . '</td>
                                 <td align="left" width="2%">$</td>
                                 <td align="right" width="13%" style="border-right: 1px solid #BDBDBD;">' . ($fila->haber == 0 || $fila->haber == NULL ? '0.00' : $this->cart->format_number(round($fila->haber, 2))) . '</td>
                                 <td align="left" width="2%">$</td>
                                 <td align="right" width="13%">' . ($saldo == 0 || $saldo == NULL ? '0.00' : $this->cart->format_number(round($saldo, 2))) . '</td>
                               </tr>';

            $total_debe += $fila->debe;
            $total_haber += $fila->haber;
        }

        $total = '<table style="font-size: small; border: 1px solid #BDBDBD;" cellpadding="4">
                       <tr>
                           <td align="left" style="border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="34%"><b>Total</b></td>
                           <td align="left" width="3%">$</td>
                           <td align="right" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="17.5%">' . ($total_debe == 0 || $total_debe == NULL ? '0.00' : $this->cart->format_number(round($total_debe, 2))) . '</td>
                           <td align="left" width="3%">$</td>
                           <td align="right" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="17.5%">' . ($total_haber == 0 || $total_haber == NULL ? '0.00' : $this->cart->format_number(round($total_haber, 2))) . '</td>
                           <td align="left" width="3%">$</td>
                           <td align="right" style="font-weight: bold; border-bottom: 1px solid #BDBDBD;" width="17.5%">' . ($saldo == 0 || $saldo == NULL ? '0.00' : $this->cart->format_number(round($saldo, 2))) . '</td>
                       </tr>
                       </table>';
        $html = '
        <style>
            .cont-general{
                border: 1px solid #eee;
                border-radius: 1%;
                margin: 2% 14%;
            }
            .cont-general2{
                border: 1px solid #BDBDBD;
            }
            .cont-general .borde-inf{
                border-bottom: 1px solid #eee;
            }
            .cont-general .borde-sup{
                border-top: 1px solid #eee;
            }
        </style>

        <table class="cont-general" border="0" cellspacing="3" style="font-size: small;" cellpadding="2">
            <tr cellspacing="10">
                <th width="2%"></th>
                <th align="left" width="10%"><img src="' . base_url("img/logo2.png") . '" /></th>
                <th align="center" width="84%" style="line-height: 7px;">
                    <h3>EDUCAL, S.A. DE C.V.</h3>
                    <h5>Auxiliar</h5>
                    <h5>' . $datos["cuenta_inicial"] . '  al  ' . $datos["cuenta_final"] . '</h5>
                    <h5>' . $datos["nombre_cuenta"] . '  al  ' . $datos["nombre_cuenta_final"] . ' </h5>
                    ' . $encabezado . '
                </th>
                <th width="2%"></th>
            </tr>
            <tr><td width="100%" class="borde-sup"></td></tr>
            <tr>
                <td width="2%"></td>
                <td width="96%">
                    <table style="font-size: small; border: 1px solid #BDBDBD;" cellpadding="4">
                       <tr>
                           <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="6%">No.</td>
                           <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="8%">Tipo Póliza</td>
                           <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="10%">Fecha</td>
                           <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="29.5%">Concepto</td>
                           <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="15%">Cargos</td>
                           <td align="center" style="font-weight: bold; border-right: 1px solid #BDBDBD; border-bottom: 1px solid #BDBDBD;" width="15%">Abonos</td>
                           <td align="center" style="font-weight: bold; border-bottom: 1px solid #BDBDBD;" width="15%">Saldos</td>
                       </tr>
                       ' . $tabla_datos . '
                    </table>
                </td>
                <td width="2%"></td>
            </tr>
            <tr><td width="100%" class="borde-inf"></td></tr>
            <tr>
                <td width="26%"></td>
                <td width="74%">' . $total . '</td>
            </tr>
</table>';

// output the HTML content

        $pdf->writeHTML($html, false, false, true, false, 'top');

//Close and output PDF document
        $pdf->Output('Auxiliar.pdf', 'I');
    }

    function exportar_reporte_auxiliar()
    {
        log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ', ha exportado un reporte auxiliar');
        $datos = $this->input->post();
        $cuenta_inicial = $datos["cuenta_inicial_exportar"];
        $cuenta_final = $datos["cuenta_final_exportar"];
        $this->load->library('excel');

        $fecha = date("Y-m-d");
        $hora = date("H:i:s");

        $query = "SELECT pc.fecha AS fecha_caratula, pc.estatus, pc.enfirme, pd.*
                FROM mov_poliza_detalle pd
                INNER JOIN mov_polizas_cabecera pc ON pd.numero_poliza = pc.numero_poliza
                WHERE pd.cuenta >= ?
                AND pd.cuenta <= ?
                AND pc.enfirme = 1
                AND pc.estatus = 'activo'
                AND pc.fecha >= ?
                AND pc.fecha <= ?;";

        $datos_detalle = $this->contabilidad_model->get_datos_polizas_detalle($query, $cuenta_inicial, $cuenta_final, $datos);
        $datos_cuenta = $this->contabilidad_model->get_datos_cuenta($cuenta_inicial);

        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Auxiliar');
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('Educal');
        $objDrawing->setPath('img/logo2.png');
        $objDrawing->setOffsetX(50);
        $objDrawing->setHeight(80);
        // $objDrawing->setWidth(10);
        $objDrawing->setCoordinates('A1');
        $objDrawing->setWorksheet($this->excel->getActiveSheet());

        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
            'font' => array(
                'bold' => true,
            )
        );
        $style_bold = array(
            'font' => array(
                'bold' => true,
            )
        );
        $style_center = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );
        $style_left = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            )
        );

        $this->excel->getActiveSheet()->mergeCells('A1:I1')->getStyle("A1")->applyFromArray($style);
        $this->excel->getActiveSheet()->setCellValue('A1', 'EDUCAL, S.A. DE C.V.');
        $this->excel->getActiveSheet()->mergeCells('A2:I2')->getStyle("A2")->applyFromArray($style);
        $this->excel->getActiveSheet()->setCellValue('A2', 'Auxiliar');
        $this->excel->getActiveSheet()->getStyle("C3:I3")->applyFromArray($style);
        $this->excel->getActiveSheet()->getStyle("C4:I4")->applyFromArray($style_center);
        $this->excel->getActiveSheet()->setCellValue('C3', 'Período');
        $this->excel->getActiveSheet()->setCellValue('E3', 'Fecha Emisión');
        $this->excel->getActiveSheet()->setCellValue('I3', 'Hora Emisión');
        $this->excel->getActiveSheet()->setCellValue('C4', $datos["fecha_inicial"] . ' al ' . $datos["fecha_final"]);
        $this->excel->getActiveSheet()->setCellValue('E4', $fecha);
        $this->excel->getActiveSheet()->setCellValue('I4', $hora);
        $this->excel->getActiveSheet()->getStyle("A6:I6")->applyFromArray($style);
        $this->excel->getActiveSheet()->mergeCells('A5:I5');

        //set cell A1 content with some text
        $this->excel->getActiveSheet()->getStyle("A:C")->applyFromArray($style_center);
        $this->excel->getActiveSheet()->setCellValue('A6', 'No.')->getColumnDimension("A")->setWidth(10);
        $this->excel->getActiveSheet()->setCellValue('B6', 'Tipo Póliza')->getColumnDimension("B")->setWidth(15);
        $this->excel->getActiveSheet()->setCellValue('C6', 'Fecha ')->getColumnDimension("C")->setWidth(15);
        $this->excel->getActiveSheet()->getStyle("D")->applyFromArray($style_left);
        $this->excel->getActiveSheet()->setCellValue('D6', 'Concepto')->getColumnDimension("D")->setWidth(40);
        $this->excel->getActiveSheet()->getStyle("E")->applyFromArray($style_center);
        $this->excel->getActiveSheet()->setCellValue('E6', 'Cuenta')->getColumnDimension("E")->setWidth(20);
        $this->excel->getActiveSheet()->getStyle("F:G")->applyFromArray($style_left);
        $this->excel->getActiveSheet()->setCellValue('F6', 'Descripción Interna')->getColumnDimension("F")->setWidth(40);
        $this->excel->getActiveSheet()->setCellValue('G6', 'Cargos')->getColumnDimension("G")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('H6', 'Abonos')->getColumnDimension("H")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('I6', 'Saldos')->getColumnDimension("I")->setWidth(20);

        $row = 7;
        $saldo = $datos_cuenta->saldo_inicial;
        $total_debe = 0;
        $total_haber = 0;

//        $this->benchmark->mark('code_start');

        foreach ($datos_detalle as $fila) {
            if (isset($fila->debe) && $fila->debe != 0) {
                $saldo += $fila->debe;
            } elseif (isset($fila->haber) && $fila->haber != 0) {
                $saldo -= $fila->haber;
            }
            $this->excel->getActiveSheet()->setCellValue('A' . $row, $fila->numero_poliza);
            $this->excel->getActiveSheet()->setCellValue('B' . $row, $fila->tipo_poliza);
            $this->excel->getActiveSheet()->setCellValue('C' . $row, $fila->fecha_caratula);
            $this->excel->getActiveSheet()->setCellValue('D' . $row, $fila->concepto);
            $this->excel->getActiveSheet()->setCellValue('E' . $row, $fila->cuenta);
            $this->excel->getActiveSheet()->setCellValue('F' . $row, $fila->nombre);
            $this->excel->getActiveSheet()->setCellValue('G' . $row, '$ ' . (number_format($fila->debe, 2)));
            $this->excel->getActiveSheet()->setCellValue('H' . $row, '$ ' . (number_format($fila->haber, 2)));
            $this->excel->getActiveSheet()->setCellValue('I' . $row, '$ ' . (number_format($saldo, 2)));

            $total_debe += $fila->debe;
            $total_haber += $fila->haber;
            $row += 1;
        }

        $row = $row + 1;
        $this->excel->getActiveSheet()->getStyle("A")->applyFromArray($style_bold);
        $this->excel->getActiveSheet()->setCellValue('A' . $row, '');
        $this->excel->getActiveSheet()->setCellValue('B' . $row, '');
        $this->excel->getActiveSheet()->setCellValue('C' . $row, '');
        $this->excel->getActiveSheet()->setCellValue('D' . $row, '');
        $this->excel->getActiveSheet()->setCellValue('E' . $row, '');
        $this->excel->getActiveSheet()->setCellValue('F' . $row, 'Total');
        $this->excel->getActiveSheet()->setCellValue('G' . $row, '$ ' . ($total_debe == 0 || $total_debe == NULL ? '0.00' : (number_format($total_debe, 2))));
        $this->excel->getActiveSheet()->setCellValue('H' . $row, '$ ' . ($total_haber == 0 || $total_haber == NULL ? '0.00' : (number_format($total_haber, 2))));
        $this->excel->getActiveSheet()->setCellValue('I' . $row, '$ ' . ($saldo == 0 || $saldo == NULL ? '0.00' : (number_format($saldo, 2))));

//        $this->benchmark->mark('code_end');

//        echo $this->benchmark->elapsed_time('code_start', 'code_end');

        $filename = "Auxiliar.xls"; // Agregar fecha en que se generó
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
//        save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
//        if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
//        force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }

    function checar_datos()
    {
        $query = "SELECT * COUNT(id_correlacion_partidas_contables) AS conteo FROM cat_correlacion_partidas_contables GROUP BY clave;";
        $query = $this->db->query($query);
        $result = $query->result();
        $this->debugeo->imprimir_pre($result);
    }

    /** Aqui empieza la sección de Contabilidad Electrónica */
    function contaelectronica()
    {
        log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ', ha consultado contabilidad electronica');
        $datos_header = array(
            "titulo_pagina" => "Armonniza  | Contabilidad Electrónica",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/conta_electronica_view');
        $this->load->view('front/footer_main_view', array("tablas" => TRUE, "auxiliar" => TRUE));
    }

    /** Aqui empieza la sección de Periodos Contables*/
    function periodoscontables() {
        log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ', ha consultado los periodos contables');
        $datos_header = array(
            "titulo_pagina" => "Armonniza  | Periodos Contables",
            "usuario" => $this->tank_auth->get_username(),
            "usuariocss" => TRUE,
            "tablas" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/conta_periodos_view');
        $this->load->view('front/footer_main_view', array("tablas" => TRUE, "periodos_contables" => TRUE));
    }

    function tabla_periodos_contables()
    {
//        Se toman todos los datos de la caratula de precompromisos
        $resultado = $this->contabilidad_model->datos_periodos_caratula();

//        Se crea el arreglo de datos donde se va a guardar la información
        $output = array("data" => "");

//        Se recorre el arreglo fila por fila
        foreach ($resultado as $fila) {

            if ($fila["cerrado"] == 1) {
                $cerrado = '<button type="button" class="btn btn-estatus btn-end disabled">Cerrado</button>';
            } else {
                $cerrado = '<button type="button" class="btn btn-estatus btn-success disabled">Abierto</button>';
            }

            $output["data"][] = array(
                $fila["id_polizas_periodos"],
                $fila["year"],
                $fila["fecha_inicia"],
                $fila["fecha_termina"],
                '<div class="table-formant-sign">' . "$" . '</div><div class="table-formant-coin">' . ($fila["salario_anterior"] == 0 || $fila["salario_anterior"] == NULL ? '0.00' : (number_format($fila["salario_anterior"], 2)) . '</div>'),
//                '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila["debe"] == 0 || $fila["debe"] == NULL ? '0.00' : (number_format($fila["debe"], 2)).'</div>'),
//                '<div class="table-formant-sign">'."$".'</div><div class="table-formant-coin">'.($fila["haber"] == 0 || $fila["haber"] == NULL ? '0.00' : (number_format($fila["haber"], 2)).'</div>'),
                '<div class="table-formant-sign">' . "$" . '</div><div class="table-formant-coin">' . ($fila["salario_actual"] == 0 || $fila["salario_actual"] == NULL ? '0.00' : (number_format($fila["salario_actual"], 2)) . '</div>'),
                $cerrado,
                '<a data-toggle="modal" data-target=".modal_editar" data-tooltip="Editar"><i class="fa fa-pencil-square-o"></i></a>
                 <a data-toggle="modal" data-target="#modal_cuentas_afectadas" data-tooltip="Ver Cuentas"><i class="fa fa-eye"></i></a>',
            );
        }

        echo json_encode($output);
    }

    function tabla_cuentas_periodos_contables()
    {
        $datos = $this->input->post();
//        $datos = array(
//            "fecha_inicial" => "2015-01-01",
//            "fecha_final" => "2015-01-31",
//        );

//        $this->debugeo->imprimir_pre($datos);

//        Se toman todos los datos de la caratula de precompromisos
        $resultado = $this->contabilidad_model->datos_cuentas_afectadas($datos);
//        $this->debugeo->imprimir_pre($resultado);

//        Se crea el arreglo de datos donde se va a guardar la información
        $tabla = "";

//        Se recorre el arreglo fila por fila
        foreach ($resultado as $fila) {

            $tabla .= "<tr>";
            $tabla .= "<td>" . $fila["fecha_inicial"] . "</td>";
            $tabla .= "<td>" . $fila["fecha_final"] . "</td>";
            $tabla .= "<td>" . $fila["cuenta"] . "</td>";
            $tabla .= "<td>" . $fila["descripcion_cuenta"] . "</td>";
            $tabla .= "<td><div class='table-formant-sign'>$</div><div class='table-formant-coin'>" . ($fila["saldo_inicial"] == 0 || $fila["saldo_inicial"] == NULL ? "0.00" : (number_format($fila["saldo_inicial"], 2))) . "</div></td>";
            $tabla .= "<td><div class='table-formant-sign'>$</div><div class='table-formant-coin'>" . ($fila["saldo_final"] == 0 || $fila["saldo_final"] == NULL ? "0.00" : (number_format($fila["saldo_final"], 2))) . "</div></td>";
            $tabla .= "</tr>";
        }

        echo $tabla;

    }

    function agregar_periodo()
    {
        log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ', ha agregado un periodo');
        $datos = $this->input->post();

        foreach ($datos as $key => $value) {
            $datos[$key] = trim($value);
        }

        try {

            $this->db->select('poliza')->from('mov_contrarecibo_caratula')->where('fecha_emision >=', $datos["fecha_inicio"])->where('fecha_emision <=', $datos["fecha_termina"])->where('cancelada !=', 1)->where('enfirme', 1);
            $query = $this->db->get();
            foreach ($query->result_array() as $key => $value) {
//                $this->debugeo->imprimir_pre($value);
                if ($value["poliza"] == 0) {
                    throw new Exception('Existen contra recibos que aún no tienen póliza dentro del rango de fechas.');
                }
            }

            unset($key);
            unset($value);

            $this->db->select('poliza')->from('mov_bancos_movimientos')->where('fecha_emision >=', $datos["fecha_inicio"])->where('fecha_emision <=', $datos["fecha_termina"])->where('cancelada !=', 1)->where('enfirme', 1);
            $query = $this->db->get();
            foreach ($query->result_array() as $key => $value) {
//                $this->debugeo->imprimir_pre($value);
                if ($value["poliza"] == 0) {
                    throw new Exception('Existen movimientos bancarios que aún no tienen póliza dentro del rango de fechas.');
                }
            }

            unset($key);
            unset($value);

            $this->db->select('poliza')->from('mov_devengado_caratula')->where('fecha_aplicacion >=', $datos["fecha_inicio"])->where('fecha_aplicacion <=', $datos["fecha_termina"])->where('cancelada !=', 1)->where('enfirme', 1);
            $query = $this->db->get();
            foreach ($query->result_array() as $key => $value) {

//                $this->debugeo->imprimir_pre($value);
                if ($value["poliza"] == 0) {
                    throw new Exception('Existen movimientos en devengado que aún no tienen póliza dentro del rango de fechas.');
                }
            }

            unset($key);
            unset($value);

            $this->db->select('poliza')->from('mov_recaudado_caratula')->where('fecha_aplicacion >=', $datos["fecha_inicio"])->where('fecha_aplicacion <=', $datos["fecha_termina"])->where('cancelada !=', 1)->where('enfirme', 1);
            $query = $this->db->get();
            foreach ($query->result_array() as $key => $value) {
//                $this->debugeo->imprimir_pre($value);
                if ($value["poliza"] == 0) {
                    throw new Exception('Existen movimientos en recaudado que aún no tienen póliza dentro del rango de fechas.');
                }
            }

            unset($key);
            unset($value);

//            $this->debugeo->imprimir_pre($datos);

            // Se genera el cálculo de la balanza
            $balanza = $this->generar_balanza_por_fechas(array(
                "fecha_inicial" => $datos["fecha_inicio"],
                "fecha_final" => $datos["fecha_termina"],
            ));

//            $this->debugeo->imprimir_pre($balanza);

            $insertar_registro = $this->contabilidad_model->insertar_periodo(array(
                "year" => $datos["year"],
                "fecha_inicia" => $datos["fecha_inicio"],
                "fecha_termina" => $datos["fecha_termina"],
                "salario_anterior" => $balanza["saldo_inicial"],
                "debe" => $balanza["cargos"],
                "haber" => $balanza["abonos"],
                "salario_actual" => $balanza["saldo_final"],
            ));

            if (!$insertar_registro) {
                throw new Exception('Hubo un error al insertar el periodo contable, por favor contacte a su administrador.');
            } else {
                echo(json_encode(array(
                    "mensaje" => "El período contable se ha generado con éxito.",
                )));
            }

        } catch (Exception $e) {
            $respuesta = array(
                "mensaje" => "" . $e->getMessage(),
            );
            echo(json_encode($respuesta));
        }
    }

    function editar_periodo_contable()
    {
        log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ', ha editado un periodo contable');
        $datos = $this->input->post();

        foreach ($datos as $key => $value) {
            $datos[$key] = trim($value);
        }

        try {

            $editar_registro = $this->contabilidad_model->editar_periodo($datos);

            if (!$editar_registro) {
                throw new Exception('Hubo un error al editar el periodo contable, por favor contacte a su administrador.');
            } else {
                echo(json_encode(array(
                    "mensaje" => "Exito",
                )));
            }

        } catch (Exception $e) {
            $respuesta = array(
                "mensaje" => "" . $e->getMessage(),
            );
            echo(json_encode($respuesta));
        }
    }

    private function generar_balanza_por_fechas($datos = NULL) {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

//        $this->benchmark->mark('code_start');

//        $datos = array(
//            "subsidio" => "",
//            "centro_costos" => "",
//            "fecha_inicial" => "2015-01-01",
//            "fecha_final" => "2015-01-31",
//            "cuenta_inicial" => 1,
//            "cuenta_final" => 9,
//            "nivel_cuenta" => 0,
//            "tipo_radio" => 2,
//        );

//        $this->debugeo->imprimir_pre($datos);

        $truncar = $this->db->truncate('balanza_temporal');

        if ($truncar) {
//            Se toman todas las cuentas del catalogo de cuentas contables
            $sql = "SELECT id_cuentas_contables, saldo_inicial, cuenta, nombre, cargo, abono, cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables ORDER BY cuenta;";
            $query = $this->db->query($sql);
            $cuentas_contables = $query->result_array();

//        echo("Cuentas contables: ");
//        $this->debugeo->imprimir_pre($cuentas_contables);

//        Después se toman todos los movimientos anteriores a los datos especificados
            $sql_anterior = "SELECT ccc.id_cuentas_contables AS id_cuentas, pd.cuenta, SUM(pd.debe) AS cargo, SUM(pd.haber) AS abono, ccc.cuenta_padre AS id_padre, pd.nivel
                            FROM mov_poliza_detalle pd
                                JOIN mov_polizas_cabecera pc
                                ON pd.numero_poliza = pc.numero_poliza
                                JOIN cat_cuentas_contables ccc
                                ON pd.cuenta = ccc.cuenta
                            WHERE pc.fecha < ?
                                AND pc.enfirme = 1
                                AND pc.cancelada = 0
                        GROUP BY cuenta ORDER BY cuenta;";
            $query_anterior = $this->db->query($sql_anterior, array($datos["fecha_inicial"]));
            $tabla_temporal_anterior = $query_anterior->result_array();

//        echo("Movimientos anteriores a la consulta: ");
//        $this->debugeo->imprimir_pre($tabla_temporal_anterior);
//
//        Después se toman las pólizas que se generaron en la fecha determinada
            $sql_consulta = "SELECT ccc.id_cuentas_contables AS id_cuentas, ccc.saldo_inicial, pd.cuenta, SUM(pd.debe) AS cargo, SUM(pd.haber) AS abono, ccc.cuenta_padre AS id_padre, pd.nivel
                    FROM mov_poliza_detalle pd
                        JOIN mov_polizas_cabecera pc
                        ON pd.numero_poliza = pc.numero_poliza
                        JOIN cat_cuentas_contables   ccc
                        ON pd.cuenta = ccc.cuenta
                    WHERE pc.fecha >= ?
                            AND pc.fecha <= ?
                            AND pc.enfirme = 1
                            AND pc.cancelada = 0
                            GROUP BY pd.cuenta;";
            $query_consulta = $this->db->query($sql_consulta, array($datos["fecha_inicial"], $datos["fecha_final"]));
            $resultado_consulta = $query_consulta->result_array();

//        echo("Movimientos dentro de la consulta: ");
//        $this->debugeo->imprimir_pre($resultado_consulta);

//        Esta seccion es para calcular los movimientos
            foreach ($tabla_temporal_anterior as $key => $value) {

//            Se toma el indice padre de la cuenta, si es que lo tiene
                $key_padre = array_search($value["id_padre"], array_column($cuentas_contables, 'cuenta'));

//            Se recorre un ciclo donde, mientras la llave padre siga siendo diferente de 0, entra en él
                do {
//                Se suman los cargos y abonos de la tabla, al catalodgo de cuentas contables
                    $cuentas_contables[$key_padre]["cargo"] += $value["cargo"];
                    $cuentas_contables[$key_padre]["abono"] += $value["abono"];

//                Se toma el ID padre de la cuenta para poder ejecutar la recursividad, donde se van a volver a agregar los catgos y abonos, siempre y cuando el ID padre sea diferente de 0
                    $key_padre = array_search($cuentas_contables[$key_padre]["id_padre"], array_column($cuentas_contables, 'cuenta'));

                } while ($key_padre > -1);

//            Al finalizar el cálculo de los padres, se suman los cargos y abonos a la cuenta inicial, dentro del catálogo de cuentas
                $key_cuenta = array_search($value["cuenta"], array_column($cuentas_contables, 'cuenta'));

                $cuentas_contables[$key_cuenta]["cargo"] = $value["cargo"];
                $cuentas_contables[$key_cuenta]["abono"] = $value["abono"];

                unset($key);
                unset($value);
                unset($key_padre);
                unset($key_cuenta);

            }

            unset($key);
            unset($value);

//        echo("Cuentas contables después de calcular los movimientos anteriores al periodo: ");
//        $this->debugeo->imprimir_pre($cuentas_contables);

            $arreglo_cuentas = array(
                "cuenta1_inicial" => 0,
                "cuenta1_descripcion" => "",
                "cuenta2_inicial" => 0,
                "cuenta2_descripcion" => "",
                "cuenta3_inicial" => 0,
                "cuenta3_descripcion" => "",
                "cuenta4_inicial" => 0,
                "cuenta4_descripcion" => "",
                "cuenta5_inicial" => 0,
                "cuenta5_descripcion" => "",
                "saldo_inicial_total" => 0,
                "cuenta1_final" => 0,
                "cuenta2_final" => 0,
                "cuenta3_final" => 0,
                "cuenta4_final" => 0,
                "cuenta5_final" => 0,
                "saldo_final_total" => 0,
            );

//        Se recorreo el arreglo de las cuentas contables para calcular el saldo final
            foreach ($cuentas_contables as $key => $value) {

                $saldo = $cuentas_contables[$key]["saldo_inicial"] + $cuentas_contables[$key]["cargo"] - $cuentas_contables[$key]["abono"];

                $cuentas_contables[$key]["saldo_inicial"] = $saldo;
                $cuentas_contables[$key]["cargo"] = 0;
                $cuentas_contables[$key]["abono"] = 0;
                $cuentas_contables[$key]["saldo_final"] = 0;

                if ($value["cuenta"] == 1) {
                    $arreglo_cuentas["cuenta1_inicial"] += round($saldo, 2);
                    $arreglo_cuentas["cuenta1_descripcion"] = $value["nombre"];
                    $arreglo_cuentas["saldo_inicial_total"] += round($saldo, 2);
                } elseif ($value["cuenta"] == 2) {
                    $arreglo_cuentas["cuenta2_inicial"] += round($saldo, 2);
                    $arreglo_cuentas["cuenta2_descripcion"] = $value["nombre"];
                    $arreglo_cuentas["saldo_inicial_total"] += round($saldo, 2);
                } elseif ($value["cuenta"] == 3) {
                    $arreglo_cuentas["cuenta3_inicial"] += round($saldo, 2);
                    $arreglo_cuentas["cuenta3_descripcion"] = $value["nombre"];
                    $arreglo_cuentas["saldo_inicial_total"] += round($saldo, 2);
                } elseif ($value["cuenta"] == 4) {
                    $arreglo_cuentas["cuenta4_inicial"] += round($saldo, 2);
                    $arreglo_cuentas["cuenta4_descripcion"] = $value["nombre"];
                    $arreglo_cuentas["saldo_inicial_total"] += round($saldo, 2);
                } elseif ($value["cuenta"] == 5) {
                    $arreglo_cuentas["cuenta5_inicial"] += round($saldo, 2);
                    $arreglo_cuentas["cuenta5_descripcion"] = $value["nombre"];
                    $arreglo_cuentas["saldo_inicial_total"] += round($saldo, 2);
                }

            }

            unset($key);
            unset($value);

//        echo("Cuentas contables después de calcular el saldo inicial ");
//        $this->debugeo->imprimir_pre($cuentas_contables);

//        Se recorre el arreglo de la consulta generada por el usuario
            foreach ($resultado_consulta as $key => $value) {
//            $this->debugeo->imprimir_pre($value)

//            Se toma el indice padre de la cuenta, si es que lo tiene
                $key_padre = array_search($value["id_padre"], array_column($cuentas_contables, 'cuenta'));
//            echo("Llave padre inicial: ".$key_padre."<br />");

//            Se recorre un ciclo donde, mientras la llave padre siga siendo diferente de 0, entra en él
                do {
//                    $this->debugeo->imprimir_pre($value);

//                echo("Cuentas contables padre antes de agregar los cargos y abonos: ");
//                $this->debugeo->imprimir_pre($cuentas_contables[$key_padre]);

//                Se suman los cargos y abonos de la tabla, al catalodgo de cuentas contables
                    $cuentas_contables[$key_padre]["cargo"] += $value["cargo"];
                    $cuentas_contables[$key_padre]["abono"] += $value["abono"];

//                echo("Cuentas contables padre después de agregar los cargos y abonos: ");
//                $this->debugeo->imprimir_pre($cuentas_contables[$key_padre]);

//                Se toma el ID padre de la cuenta para poder ejecutar la recursividad, donde se van a volver a agregar los catgos y abonos, siempre y cuando el ID padre sea diferente de 0
                    $key_padre = array_search($cuentas_contables[$key_padre]["id_padre"], array_column($cuentas_contables, 'cuenta'));

//                echo("Nuevo ID Padre: ".$key_padre."<br />");

                } while ($key_padre > -1);

//            Al finalizar el cálculo de los padres, se suman los cargos y abonos a la cuenta inicial, dentro del catálogo de cuentas
                $key_cuenta = array_search($value["cuenta"], array_column($cuentas_contables, 'cuenta'));

//            echo("Ya salió del ciclo:<br />");
//            echo("ID de la cuenta: ".$key_cuenta."<br />");

//            echo("Cuentas contables antes de agregar los cargos y abonos al ID de la cuenta: ");
//            $this->debugeo->imprimir_pre($cuentas_contables[$key_cuenta]);

                $cuentas_contables[$key_cuenta]["cargo"] = $value["cargo"];
                $cuentas_contables[$key_cuenta]["abono"] = $value["abono"];

//            echo("Cuentas contables después de agregar los cargos y abonos al ID de la cuenta: ");
//            $this->debugeo->imprimir_pre($cuentas_contables[$key_cuenta]);

            }

            unset($key);
            unset($value);

//        echo("Cuentas contables después de calcular el saldo actual: ");
//        $this->debugeo->imprimir_pre($cuentas_contables);

            $balanza_final = array(
                "saldo_inicial" => 0,
                "cargos" => 0,
                "abonos" => 0,
                "saldo_final" => 0,
            );

//        Se recorreo el arreglo de las cuentas contables para calcular el saldo final
            foreach ($cuentas_contables as $key => $value) {

                $saldo = ($cuentas_contables[$key]["saldo_inicial"] + $cuentas_contables[$key]["cargo"]) - $cuentas_contables[$key]["abono"];

                $balanza_final["saldo_inicial"] += $cuentas_contables[$key]["saldo_inicial"];
                $balanza_final["cargos"] += $cuentas_contables[$key]["cargo"];
                $balanza_final["abonos"] += $cuentas_contables[$key]["abono"];
                $balanza_final["saldo_final"] += $saldo;

//                $this->debugeo->imprimir_pre($value);

                if ($value["cuenta"] == 1) {
                    $arreglo_cuentas["cuenta1_final"] += round($saldo, 2);
                    $arreglo_cuentas["saldo_final_total"] += round($saldo, 2);
                } elseif ($value["cuenta"] == 2) {
                    $arreglo_cuentas["cuenta2_final"] += round($saldo, 2);
                    $arreglo_cuentas["saldo_final_total"] += round($saldo, 2);
                } elseif ($value["cuenta"] == 3) {
                    $arreglo_cuentas["cuenta3_final"] += round($saldo, 2);
                    $arreglo_cuentas["saldo_final_total"] += round($saldo, 2);
                } elseif ($value["cuenta"] == 4) {
                    $arreglo_cuentas["cuenta4_final"] += round($saldo, 2);
                    $arreglo_cuentas["saldo_final_total"] += round($saldo, 2);
                } elseif ($value["cuenta"] == 5) {
                    $arreglo_cuentas["cuenta5_final"] += round($saldo, 2);
                    $arreglo_cuentas["saldo_final_total"] += round($saldo, 2);
                }


            }

            $datos_cuenta_1 = array(
                'fecha_inicial' => $datos["fecha_inicial"],
                'fecha_final' => $datos["fecha_final"],
                'cuenta' => "1",
                'descripcion_cuenta' => $arreglo_cuentas["cuenta1_descripcion"],
                'saldo_inicial' => $arreglo_cuentas["cuenta1_inicial"],
                'saldo_final' => $arreglo_cuentas["cuenta1_final"],
            );

            $this->db->insert('periodos_cuentas_afectadas', $datos_cuenta_1);

            $datos_cuenta_2 = array(
                'fecha_inicial' => $datos["fecha_inicial"],
                'fecha_final' => $datos["fecha_final"],
                'cuenta' => "2",
                'descripcion_cuenta' => $arreglo_cuentas["cuenta2_descripcion"],
                'saldo_inicial' => $arreglo_cuentas["cuenta2_inicial"],
                'saldo_final' => $arreglo_cuentas["cuenta2_final"],
            );

            $this->db->insert('periodos_cuentas_afectadas', $datos_cuenta_2);

            $datos_cuenta_3 = array(
                'fecha_inicial' => $datos["fecha_inicial"],
                'fecha_final' => $datos["fecha_final"],
                'cuenta' => "3",
                'descripcion_cuenta' => $arreglo_cuentas["cuenta3_descripcion"],
                'saldo_inicial' => $arreglo_cuentas["cuenta3_inicial"],
                'saldo_final' => $arreglo_cuentas["cuenta3_final"],
            );

            $this->db->insert('periodos_cuentas_afectadas', $datos_cuenta_3);

            $datos_cuenta_4 = array(
                'fecha_inicial' => $datos["fecha_inicial"],
                'fecha_final' => $datos["fecha_final"],
                'cuenta' => "4",
                'descripcion_cuenta' => $arreglo_cuentas["cuenta4_descripcion"],
                'saldo_inicial' => $arreglo_cuentas["cuenta4_inicial"],
                'saldo_final' => $arreglo_cuentas["cuenta4_final"],
            );

            $this->db->insert('periodos_cuentas_afectadas', $datos_cuenta_4);

            $datos_cuenta_5 = array(
                'fecha_inicial' => $datos["fecha_inicial"],
                'fecha_final' => $datos["fecha_final"],
                'cuenta' => "5",
                'descripcion_cuenta' => $arreglo_cuentas["cuenta5_descripcion"],
                'saldo_inicial' => $arreglo_cuentas["cuenta5_inicial"],
                'saldo_final' => $arreglo_cuentas["cuenta5_final"],
            );

            $this->db->insert('periodos_cuentas_afectadas', $datos_cuenta_5);

            $balanza_final["saldo_inicial"] = $arreglo_cuentas["saldo_inicial_total"];
            $balanza_final["saldo_final"] = $arreglo_cuentas["saldo_final_total"];

//            echo("Cuentas contables después de calcular el saldo final: ");
//            $this->debugeo->imprimir_pre($cuentas_contables);
//
//            $this->debugeo->imprimir_pre($balanza_final);

//        $this->benchmark->mark('code_end');
//        echo $this->benchmark->elapsed_time('code_start', 'code_end');

            return $balanza_final;

        }

    }

    /**Tablas*/
    function tabla_subsidio()
    {
        $resultados_subsidio = $this->recaudacion_model->datos_subsidio();
        $output = array('data' => '');
        foreach ($resultados_subsidio as $fila) {
            $output['data'][] = array(
                $fila->codigo,
                $fila->descripcion,
                '<a class="seleccion_gasto" data-toggle="modal" data-dismiss="modal" title="Seleccionar Subsidio"><i class="fa fa-check"></i></a>'
            );
        }
//        $this->debugeo->imprimir_pre($output);
        echo(json_encode($output));
    }

    function tabla_cuentas_inicial()
    {
        $this->db->select('cuenta, nombre, tipo, descripcion')->from('cat_cuentas_contables');
        $query = $this->db->get();
        $resultados_cuenta = $query->result();
        $output = array('data' => '');
        foreach ($resultados_cuenta as $fila) {
            $output['data'][] = array(
                $fila->cuenta,
                $fila->nombre,
                $fila->tipo,
                '<a class="seleccion_gasto" data-toggle="modal" data-dismiss="modal" title="Seleccionar Cuenta"><i class="fa fa-check"></i></a>'
            );
        }
//        $this->debugeo->imprimir_pre($output);
        echo(json_encode($output));
    }

    function tabla_cuentas_final()
    {
        $this->db->select('cuenta, nombre, tipo, descripcion')->from('cat_cuentas_contables');
        $query = $this->db->get();
        $resultados_cuenta = $query->result();
        $output = array('data' => '');
        foreach ($resultados_cuenta as $fila) {
            $output['data'][] = array(
                $fila->cuenta,
                $fila->nombre,
                $fila->tipo,
                '<a class="seleccion_gasto" data-toggle="modal" data-dismiss="modal" title="Seleccionar Cuenta"><i class="fa fa-check"></i></a>'
            );
        }
//        $this->debugeo->imprimir_pre($output);
        echo(json_encode($output));
    }

    function tabla_centro_costos()
    {
        $sql = "SELECT COLUMN_GET(nivel, 'centro_de_costos' as char) AS centro_de_costo, COLUMN_GET(nivel, 'descripcion' as char) AS descripcion FROM cat_niveles WHERE COLUMN_GET(nivel, 'centro_de_costos' as char)  != '' GROUP BY  centro_de_costo;";
        $query = $this->db->query($sql);
        $resultados_cuenta = $query->result();
        $output = array('data' => '');
        foreach ($resultados_cuenta as $fila) {
//            $this->debugeo->imprimir_pre($fila);
            $output['data'][] = array(
                $fila->centro_de_costo,
                $fila->descripcion,
                '<a class="seleccion_gasto" data-toggle="modal" data-dismiss="modal" title="Seleccionar Centro de Costo"><i class="fa fa-check"></i></a>'
            );
        }
//        $this->debugeo->imprimir_pre($output);
        echo(json_encode($output));
    }

    function tabla_cuentas() {

        $this->db->select('cuenta, nombre, tipo')->from('cat_cuentas_contables')->where('tipo', "D");
        $query = $this->db->get();
        $resultado = $query->result();

        foreach($resultado as $fila) {

            $output["data"][] = array(
                $fila->cuenta,
                $fila->nombre,
                $fila->tipo,
            );
        }

        echo json_encode($output);
    }


//    function reescribir_XML() {
//        echo(base_url("/plantillas/EDU8202178I3201508BN.xml"));
//        $documento = base_url("/plantillas/EDU8202178I3201508BN.xml");
//
//        $doc = new DOMDocument();
//        $doc->load( $documento );
//
//        $root = $doc->getElementsByTagName( "Ctas" );;
//
//        foreach( $root as $item ) {
//            $saldo_inicial = $item->getAttribute('SaldoIni');
//            $debe = $item->getAttribute('Debe');
//            $haber = $item->getAttribute('Haber');
//            $saldo_final = $item->getAttribute('SaldoFin');
//
//            $item->setAttribute('SaldoIni', number_format((float)$saldo_inicial, 2, '.', ''));
//            $item->setAttribute('Debe', number_format((float)$debe, 2, '.', ''));
//            $item->setAttribute('Haber', number_format((float)$haber, 2, '.', ''));
//            $item->setAttribute('SaldoFin', number_format((float)$saldo_final, 2, '.', ''));
//
//        }
//
//        $doc->save("EDU8202178I3201508BN.xml");
//
//    }

    function acomodar_saldos_iniciales()
    {

        $sql = "SELECT cuenta, saldo
                FROM tabla_temporal;";
        $query = $this->db->query($sql);

        foreach ($query->result_array() as $key => $value) {
//            $this->debugeo->imprimir_pre($value);

            $datos_actualizar = array(
                'saldo_inicial' => $value["saldo"],
            );

            $this->db->where('cuenta', $value["cuenta"]);
            $query_actualizar = $this->db->update('cat_cuentas_contables', $datos_actualizar);

            if ($query_actualizar) {
                $this->debugeo->imprimir_pre("Se actualizó el saldo");
            }

        }

//        $this->debugeo->imprimir_pre($cuentas_contables);

    }

    function acomodar_cuentas()
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

        $cuenta_post = $this->input->post("poliza", TRUE);
        $cuenta_inicial = substr($cuenta_post, 0);

        $this->benchmark->mark('code_start');

        $this->db->trans_begin();

        $sql_cuentas = "SELECT * FROM cat_cuentas_contables WHERE cuenta LIKE ? ;";
        $query_cuentas = $this->db->query($sql_cuentas, array($cuenta_inicial . "%"));
        $cuentas_contables = $query_cuentas->result_array();

        foreach ($cuentas_contables as $key => $value) {
            $padre = $this->BuscarPadre($value["cuenta"]);
            $nivel = $this->SetNivel($value["cuenta"]);

            $datos_actualizar = array(
                'cuenta_padre' => $padre,
                'nivel' => $nivel,
            );

            $this->db->where('cuenta', $value["cuenta"]);
            $this->db->update('cat_cuentas_contables', $datos_actualizar);
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
        }

        $this->benchmark->mark('code_end');
        echo $this->benchmark->elapsed_time('code_start', 'code_end');
    }

    private function BuscarPadre($ctaSeek = NULL)
    {
        $posicion_punto = strrpos($ctaSeek, '.', -1);
        $cuenta_cortada = substr($ctaSeek, 0, $posicion_punto);
        if (!$cuenta_cortada) {
            $cuenta_cortada = 0;
        }
        return $cuenta_cortada;
    }

    private function SetNivel($cuenta = NULL)
    {
        $arreglo_cuenta = explode(".", $cuenta);
        $nivel = count($arreglo_cuenta);
        return $nivel;
    }


    function revisar_polizas_diario()
    {
        $this->db->select('numero_poliza')->from('mov_poliza_detalle')->group_by("numero_poliza");
        $query = $this->db->get();
        $resultado = $query->result_array();

        foreach ($resultado as $key => $value) {
            $this->db->select('SUM(debe) AS debe, SUM(haber) AS haber')->from('mov_poliza_detalle')->where('numero_poliza', $value["numero_poliza"]);
            $query_interno = $this->db->get();
            $resultado_interno = $query_interno->row_array();
            if ($resultado_interno["debe"] !== $resultado_interno["haber"]) {
                $this->debugeo->imprimir_pre($value["numero_poliza"]);
//                $this->debugeo->imprimir_pre($resultado_interno);
            }
//            $this->debugeo->imprimir_pre($resultado_interno);
        }
    }

// Cierre Anual
    function cerrar_periodo_anual()
    {
//        Se inicia la variable que contiene la respuesta a todo lo que pasa dentro de la función
        $respuesta = array(
            "mensaje" => ''
        );

        try {

//            Se incializan dos variables que se encargan de revisar que existan las fechas iniciales y finales del año
//            Se tienen que inicializar en FALSO para que se haga correcta la validación
            $check_inicial = FALSE;
            $check_final = FALSE;

//            Se toman todos los periodos contables
            $resultado = $this->contabilidad_model->tomar_periodos_contables();

//            Se recorren todos los periodos contables
            foreach ($resultado as $key => $value) {

//                Se verifican que los periodos contables esten cerrados, de lo contrario se manda un mensaje de error al usuario
                if ($value["cerrado"] == 0 || $value["cerrado"] == NULL || $value["cerrado"] == "") {
                    throw new Exception('Faltan periodos contables por cerrar');
                }

//               Se toma el año en curso
                $year = date("Y");

//               Se verifica que exista la fecha inicial en el arreglo de periodos contables
                $fecha_inicial = array_search($year . '-01-01', $value);

//                En caso de que exista, se cambia la variable del check inicial a VERDADERO, indicando que existe la fecha
                if ($fecha_inicial) {
                    $check_inicial = TRUE;
                }

//                Se verifica que exista la fecha final en el arreglo de periodos contables
                $fecha_final = array_search($year . '-05-31', $value);


//                En caso de que exista, se cambia la variable del check final a VERDADERO, indicando que existe la fecha
                if ($fecha_final) {
                    $check_final = TRUE;
                }
            }

//            Se verifica que los checks sean VERDADEROS,
//            si son verdaderos, se continua con el proceso
            if ($check_inicial && $check_final) {
//                Esto sirve para saber si estan las fechas iniciales y finales,
//                solo es para debug, se debe de comentar una vez que se termine de trabajar en la función
//                $this->debugeo->imprimir_pre("Si estan");

                /**
                 * Aqui se tiene que llamar a la función "generar_balanza_por_fechas" con los siguientes parametros:
                 *
                 * $datos = array(
                 *  "fecha_inicial" => "2015-01-01",
                 *  "fecha_final" => "2015-12-31",
                 * );
                 *
                 */

                $datos = array(
                    "fecha_inicial" => '2015-01-01',
                    "fecha_final" => '2015-05-31',
                );

                $cierre = $this->generar_tabla_principal_balanza_anual();

//                $this->debugeo->imprimir_pre($cierre);

//              Una vez que termina el proceso de la balanza, se tienen que insertar los datos devueltos en la tabla: "periodos_contables_anual"

//                $datos_insertar = array(
//                    'year' => $datos["year"],
//                    'fecha_inicia' => $datos["fecha_inicia"],
//                    'fecha_termina' => $datos["fecha_termina"],
//                    'cerrado' => 1,
//
//                );

//                $query = $this->db->insert('periodos_contables_anual', $datos_insertar);

            } //            De lo contrario, se manda un mensaje de error al usuario
            else {
                throw new Exception('Faltan Periodos Contables por Generar');
            }

//            Si todo el proceso ha sido exitoso, se manda un mensaje al usuario, indicando que la operación ha sido fructuosa
            $respuesta["mensaje"] .= $cierre;

            echo(json_encode($respuesta));

        } catch (Exception $e) {
//            Se obtiene el mensaje de error para el usuario
            $respuesta["mensaje"] = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' . $e->getMessage() . '</div>';

//            Se imprime el mensaje para el usuario
            echo(json_encode($respuesta));
        }

    }

    function generar_tabla_principal_balanza_anual()
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

//        $this->benchmark->mark('code_start');


//            Se toman todas las cuentas del catalogo de cuentas contables
        $sql = "SELECT id_cuentas_contables, saldo_inicial, cuenta, nombre, tipo, cargo, abono, cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables ORDER BY cuenta;";
        $query = $this->db->query($sql);
        $cuentas_contables = $query->result_array();

//        echo("Cuentas contables: ");
//        $this->debugeo->imprimir_pre($cuentas_contables);

//        Después se toman todos los movimientos anteriores a los datos especificados
        $sql_anterior = "SELECT ccc.id_cuentas_contables AS id_cuentas, pd.cuenta, SUM(pd.debe) AS cargo, SUM(pd.haber) AS abono, ccc.cuenta_padre AS id_padre, pd.nivel
                        FROM mov_poliza_detalle pd
                            JOIN mov_polizas_cabecera pc
                            ON pd.numero_poliza = pc.numero_poliza
                            JOIN cat_cuentas_contables ccc
                            ON pd.cuenta = ccc.cuenta
                        WHERE pc.fecha < ?
                            AND pd.subsidio LIKE ?
                            AND pd.centro_costo LIKE ?
                            AND pc.enfirme = 1
                            AND pc.cancelada = 0
                    GROUP BY cuenta ORDER BY cuenta;";
        $query_anterior = $this->db->query($sql_anterior, array("2015-01-01", "%%", "%%"));
        $tabla_temporal_anterior = $query_anterior->result_array();

//        echo("Movimientos anteriores a la consulta: ");

//        $this->debugeo->imprimir_pre($tabla_temporal_anterior);
//
//        Después se toman las pólizas que se generaron en la fecha determinada
        $sql_consulta = "SELECT ccc.id_cuentas_contables AS id_cuentas, ccc.saldo_inicial, pd.cuenta, SUM(pd.debe) AS cargo, SUM(pd.haber) AS abono, ccc.cuenta_padre AS id_padre, pd.nivel
                FROM mov_poliza_detalle pd
                    JOIN mov_polizas_cabecera pc
                    ON pd.numero_poliza = pc.numero_poliza
                    JOIN cat_cuentas_contables   ccc
                    ON pd.cuenta = ccc.cuenta
                WHERE pc.fecha >= ?
                        AND pc.fecha <= ?
                        AND pd.subsidio LIKE ?
                        AND pd.centro_costo LIKE ?
                        AND pc.enfirme = 1
                        AND pc.cancelada = 0
                        GROUP BY pd.cuenta;";
        $query_consulta = $this->db->query($sql_consulta, array("2015-01-01", "2015-05-31", "%%", "%%"));
        $resultado_consulta = $query_consulta->result_array();

//        echo("Movimientos dentro de la consulta: ");
//        $this->debugeo->imprimir_pre($resultado_consulta);

//        Esta seccion es para calcular los movimientos
        foreach ($tabla_temporal_anterior as $key => $value) {

//            Se toma el indice padre de la cuenta, si es que lo tiene
            $key_padre = array_search($value["id_padre"], array_column($cuentas_contables, 'cuenta'));

//            Se recorre un ciclo donde, mientras la llave padre siga siendo diferente de 0, entra en él
            do {
//                Se suman los cargos y abonos de la tabla, al catalodgo de cuentas contables
                $cuentas_contables[$key_padre]["cargo"] += $value["cargo"];
                $cuentas_contables[$key_padre]["abono"] += $value["abono"];

//                Se toma el ID padre de la cuenta para poder ejecutar la recursividad, donde se van a volver a agregar los catgos y abonos, siempre y cuando el ID padre sea diferente de 0
                $key_padre = array_search($cuentas_contables[$key_padre]["id_padre"], array_column($cuentas_contables, 'cuenta'));

            } while ($key_padre > -1);

//            Al finalizar el cálculo de los padres, se suman los cargos y abonos a la cuenta inicial, dentro del catálogo de cuentas
            $key_cuenta = array_search($value["cuenta"], array_column($cuentas_contables, 'cuenta'));

            $cuentas_contables[$key_cuenta]["cargo"] = $value["cargo"];
            $cuentas_contables[$key_cuenta]["abono"] = $value["abono"];

            unset($key);
            unset($value);
            unset($key_padre);
            unset($key_cuenta);

        }

        unset($key);
        unset($value);

//        echo("Cuentas contables después de calcular los movimientos anteriores al periodo: ");
//        $this->debugeo->imprimir_pre($cuentas_contables);

//        Se recorreo el arreglo de las cuentas contables para calcular el saldo final
        foreach ($cuentas_contables as $key => $value) {
            $cuentas_contables[$key]["saldo_final"] = $cuentas_contables[$key]["saldo_inicial"] + $cuentas_contables[$key]["cargo"] - $cuentas_contables[$key]["abono"];
            $cuentas_contables[$key]["saldo_inicial"] = $cuentas_contables[$key]["saldo_inicial"] + $cuentas_contables[$key]["cargo"] - $cuentas_contables[$key]["abono"];
            $cuentas_contables[$key]["cargo"] = 0;
            $cuentas_contables[$key]["abono"] = 0;
            $cuentas_contables[$key]["saldo_final"] = 0;
        }

        unset($key);
        unset($value);

//        echo("Cuentas contables después de calcular el saldo inicial ");
//        $this->debugeo->imprimir_pre($cuentas_contables);

//        Se recorre el arreglo de la consulta generada por el usuario
        foreach ($resultado_consulta as $key => $value) {
//            $this->debugeo->imprimir_pre($value)

//            Se toma el indice padre de la cuenta, si es que lo tiene
            $key_padre = array_search($value["id_padre"], array_column($cuentas_contables, 'cuenta'));
//            echo("Llave padre inicial: ".$key_padre."<br />");

//            Se recorre un ciclo donde, mientras la llave padre siga siendo diferente de 0, entra en él
            do {
//                    $this->debugeo->imprimir_pre($value);

//                echo("Cuentas contables padre antes
//                de agregar los cargos y abonos: ");
//                $this->debugeo->imprimir_pre($cuentas_contables[$key_padre]);

//                Se suman los cargos y abonos de la tabla, al catalodgo de cuentas contables
                $cuentas_contables[$key_padre]["cargo"] += $value["cargo"];
                $cuentas_contables[$key_padre]["abono"] += $value["abono"];

//                echo("Cuentas contables padre después de agregar los cargos y abonos: ");
//                $this->debugeo->imprimir_pre($cuentas_contables[$key_padre]);

//                Se toma el ID padre de la cuenta para poder ejecutar la recursividad, donde se van a volver a agregar los catgos y abonos, siempre y cuando el ID padre sea diferente de 0
                $key_padre = array_search($cuentas_contables[$key_padre]["id_padre"], array_column($cuentas_contables, 'cuenta'));

//                echo("Nuevo ID Padre: ".$key_padre."<br />");

            } while ($key_padre > -1);

//            Al finalizar el cálculo de los padres, se suman los cargos y abonos a la cuenta inicial, dentro del catálogo de cuentas
            $key_cuenta = array_search($value["cuenta"], array_column($cuentas_contables, 'cuenta'));

//            echo("Ya salió del ciclo:<br />");
//            echo("ID de la cuenta: ".$key_cuenta."<br />");

//            echo("Cuentas contables antes de agregar los cargos y abonos al ID de la cuenta: ");
//            $this->debugeo->imprimir_pre($cuentas_contables[$key_cuenta]);

            $cuentas_contables[$key_cuenta]["cargo"] = $value["cargo"];
            $cuentas_contables[$key_cuenta]["abono"] = $value["abono"];

//            echo("Cuentas contables después de agregar los cargos y abonos al ID de la cuenta: ");
//            $this->debugeo->imprimir_pre($cuentas_contables[$key_cuenta]);

        }

        unset($key);
        unset($value);


//        echo("Cuentas contables después de calcular el saldo actual: ");
//        $this->debugeo->imprimir_pre($cuentas_contables);

        $cuenta4 = array();
        $cuenta5 = array();

        foreach ($cuentas_contables as $key => $value) {
//            $this->debugeo->imprimir_pre($value);

            $cuenta = explode(".", $value["cuenta"]);

            if ($cuenta[0] == 4 || $cuenta[0] == 5) {

                if ($value["cargo"] == 0) {

                    $value["cargo"] = $value["abono"];
                    $value["abono"] = 0;

                } elseif ($value["abono"] == 0) {

                    $value["abono"] = $value["cargo"];
                    $value["cargo"] = 0;

                }

                if ($value["cargo"] != $value["abono"] && $value["cargo"] != 0 && $value["abono"] != 0) {
                    $diferencia = 0;

                    if ($value["cargo"] > $value["abono"]) {

                        $diferencia = $value["cargo"] - $value["abono"];
                        $value["abono"] = $diferencia;
                        $value["cargo"] = 0;

                    } elseif ($value["abono"] > $value["cargo"]) {

                        $diferencia = $value["abono"] - $value["cargo"];
                        $value["cargo"] = $diferencia;
                        $value["abono"] = 0;

                    }

                }

            }

            if ($cuenta[0] == 4 && $value["tipo"] == "D") {

                if($value["cargo"] == 0 && $value["abono"] == 0) {
                    continue;
                } else {
                    $cuenta4[] = $value;
                }

            } elseif ($cuenta[0] == 5 && $value["tipo"] == "D") {

                if($value["cargo"] == 0 && $value["abono"] == 0) {
                    continue;
                } else {
                    $cuenta5[] = $value;
                }
            }

        }

//        $this->debugeo->imprimir_pre($cuenta4);
//        $this->debugeo->imprimir_pre($cuenta5);


        $suma_cargos_4 = 0;
        $suma_cargos_5 = 0;
        $suma_abonos_4 = 0;
        $suma_abonos_5 = 0;
        $suma_cargos = 0;
        $suma_abonos = 0;


        unset($key);
        unset($value);

//        Se inicizaliza la variable con el ultimo valor de las polizas
        $last = 0;
//        Se toma el numero de la ultima poliza
        $ultimo = $this->ciclo_model->ultima_poliza();

//        Se le suma uno al ultimo valor de las polizas
        if ($ultimo) {
            $last = $ultimo->ultimo + 1;
        } //        De lo contrario se inicia en 1
        else {
            $last = 1;
        }

        $nuevo_last = $last + 1;

        $actual_year = date("Y");

        $this->db->trans_start();

        $query_insertar_caratula_poliza = "INSERT INTO mov_polizas_cabecera (numero_poliza, tipo_poliza, fecha, fecha_real, importe, concepto,
                                                                                enfirme, estatus, creado_por, cargos, abonos,
                                                                                proveedor, contrarecibo, movimiento, concepto_especifico,
                                                                                poliza_contrarecibo, no_devengado, no_recaudado, no_movimiento, poliza_devengado)
                                                                    VALUES  (?, 'Diario', ?, NOW(), 0, 'Poliza de cierre anual',
                                                                             1, 'activo', 'Sistema Armonniza', 0, 0,
                                                                             'Educal S.A. de C.V.', 0, 0, 'Poliza de cierre anual',
                                                                             0, 0, 0, 0, 0);";

        $datos_caratula_poliza = array(
            $last,
            $actual_year."-12-31",
        );

        $this->ciclo_model->insertar_detalle_poliza($query_insertar_caratula_poliza, $datos_caratula_poliza);

        $query_insertar_caratula_poliza_2 = "INSERT INTO mov_polizas_cabecera (numero_poliza, tipo_poliza, fecha, fecha_real, importe, concepto,
                                                                                enfirme, estatus, creado_por, cargos, abonos,
                                                                                proveedor, contrarecibo, movimiento, concepto_especifico,
                                                                                poliza_contrarecibo, no_devengado, no_recaudado, no_movimiento, poliza_devengado)
                                                                    VALUES  (?, 'Diario', ?, NOW(), 0, 'Poliza de cierre anual',
                                                                             1, 'activo', 'Sistema Armonniza', 0, 0,
                                                                             'Educal S.A. de C.V.', 0, 0, 'Poliza de cierre anual',
                                                                             0, 0, 0, 0, 0);";

        $datos_caratula_poliza_2 = array(
            $nuevo_last,
            $actual_year."-12-31",
        );
//        $this->debugeo->imprimir_pre($nuevo_last);

        $this->ciclo_model->insertar_detalle_poliza($query_insertar_caratula_poliza_2, $datos_caratula_poliza_2);

        $this->db->trans_complete();

        $this->db->trans_start();

        foreach($cuenta4 as $key => $value) {
            $suma_cargos_4 += $value["cargo"];
            $suma_cargos += $value["cargo"];
            $suma_abonos_4 += $value["abono"];
            $suma_abonos += $value["abono"];

            $this->utilerias->insertar_poliza_directa_detalle(array(
                "numero_poliza" => 1,
                "cuenta" => $value["cuenta"],
                "cargos" => $value["cargo"],
                "abonos" => $value["abono"],
            ));

            $this->db->select('nombre')->from('cat_cuentas_contables')->where('cuenta', $value["cuenta"]);
            $query_cuenta_existe = $this->db->get();
            $existe_cuenta = $query_cuenta_existe->row_array();

            $query_insertar_detalle_poliza_4 = "INSERT INTO mov_poliza_detalle (
                                                                    numero_poliza, tipo_poliza, fecha, hora, fecha_real,
                                                                    id_cuenta, id_padre, nivel, concepto, subsidio,
                                                                    centro_costo, partida, concepto_especifico, cuenta,
                                                                    nombre, debe, haber)
                                                                    VALUES (
                                                                    ?, ?, NOW(), NOW(), NOW(),
                                                                    0, 0, 0, 'Cierre anual', 0,
                                                                    0, 0, 'Cierre anual', ?,
                                                                    ?, ?, ?);";

            $datos_detalle_poliza_4 = array(
                $last,
                "Diario",
                $value["cuenta"],
                $existe_cuenta["nombre"],
                $value["cargo"],
                $value["abono"],
            );

//            $this->debugeo->imprimir_pre($datos_detalle_poliza_4);

            $this->ciclo_model->insertar_detalle_poliza($query_insertar_detalle_poliza_4, $datos_detalle_poliza_4);

        }

        $this->db->trans_complete();

        unset($key);
        unset($value);

        $this->db->trans_start();

        foreach($cuenta5 as $key => $value) {
            $suma_cargos_5 += $value["cargo"];
            $suma_cargos += $value["cargo"];
            $suma_abonos_5 += $value["abono"];
            $suma_abonos += $value["abono"];

            $this->utilerias->insertar_poliza_directa_detalle(array(
                "numero_poliza" => 1,
                "cuenta" => $value["cuenta"],
                "cargos" => $value["cargo"],
                "abonos" => $value["abono"],
            ));

            $this->db->select('nombre')->from('cat_cuentas_contables')->where('cuenta', $value["cuenta"]);
            $query_cuenta_existe = $this->db->get();
            $existe_cuenta = $query_cuenta_existe->row_array();

            $query_insertar_detalle_poliza_5 = "INSERT INTO mov_poliza_detalle (
                                                                    numero_poliza, tipo_poliza, fecha, hora, fecha_real,
                                                                    id_cuenta, id_padre, nivel, concepto, subsidio,
                                                                    centro_costo, partida, concepto_especifico, cuenta,
                                                                    nombre, debe, haber)
                                                                    VALUES (
                                                                    ?, ?, NOW(), NOW(), NOW(),
                                                                    0, 0, 0, 'Cierre anual', 0,
                                                                    0, 0, 'Cierre anual', ?,
                                                                    ?, ?, ?);";

            $datos_detalle_poliza_5 = array(
                $last,
                "Diario",
                $value["cuenta"],
                $existe_cuenta["nombre"],
                $value["cargo"],
                $value["abono"],
            );

            $this->ciclo_model->insertar_detalle_poliza($query_insertar_detalle_poliza_5, $datos_detalle_poliza_5);

        }

        $this->db->trans_complete();

        $this->db->trans_start();

        if($suma_abonos_4 != 0) {
            $this->utilerias->insertar_poliza_directa_detalle(array(
                "numero_poliza" => 1,
                "cuenta" => "6.1.1.2.1",
                "cargos" => $suma_abonos_4,
                "abonos" => 0,
            ));

            $this->db->select('nombre')->from('cat_cuentas_contables')->where('cuenta', "6.1.1.2.1");
            $query_cuenta_existe = $this->db->get();
            $existe_cuenta = $query_cuenta_existe->row_array();

            $query_insertar_detalle_poliza = "INSERT INTO mov_poliza_detalle (
                                                                    numero_poliza, tipo_poliza, fecha, hora, fecha_real,
                                                                    id_cuenta, id_padre, nivel, concepto, subsidio,
                                                                    centro_costo, partida, concepto_especifico, cuenta,
                                                                    nombre, debe, haber)
                                                                    VALUES (
                                                                    ?, ?, NOW(), NOW(), NOW(),
                                                                    0, 0, 0, 'Cierre anual', 0,
                                                                    0, 0, 'Cierre anual', ?,
                                                                    ?, ?, ?);";

            $datos_detalle_poliza = array(
                $last,
                "Diario",
                "6.1.1.2.1",
                $existe_cuenta["nombre"],
                $suma_abonos_4,
                0,
            );

            $this->ciclo_model->insertar_detalle_poliza($query_insertar_detalle_poliza, $datos_detalle_poliza);
        }

        if($suma_cargos_5 != 0) {
            $this->utilerias->insertar_poliza_directa_detalle(array(
                "numero_poliza" => 1,
                "cuenta" => "6.1.1.2.1",
                "cargos" => 0,
                "abonos" => $suma_cargos_5,
            ));

            $this->db->select('nombre')->from('cat_cuentas_contables')->where('cuenta', "6.1.1.2.1");
            $query_cuenta_existe = $this->db->get();
            $existe_cuenta = $query_cuenta_existe->row_array();

            $query_insertar_detalle_poliza = "INSERT INTO mov_poliza_detalle (
                                                                    numero_poliza, tipo_poliza, fecha, hora, fecha_real,
                                                                    id_cuenta, id_padre, nivel, concepto, subsidio,
                                                                    centro_costo, partida, concepto_especifico, cuenta,
                                                                    nombre, debe, haber)
                                                                    VALUES (
                                                                    ?, ?, NOW(), NOW(), NOW(),
                                                                    0, 0, 0, 'Cierre anual', 0,
                                                                    0, 0, 'Cierre anual', ?,
                                                                    ?, ?, ?);";

            $datos_detalle_poliza = array(
                $last,
                "Diario",
                "6.1.1.2.1",
                $existe_cuenta["nombre"],
                0,
                $suma_cargos_5,
            );

            $this->ciclo_model->insertar_detalle_poliza($query_insertar_detalle_poliza, $datos_detalle_poliza);

        }

        $this->utilerias->insertar_poliza_directa_detalle(array(
            "numero_poliza" => 1,
            "cuenta" => "6.1.1.2.1",
            "cargos" => 0,
            "abonos" => $suma_cargos_4,
        ));

        $this->db->select('nombre')->from('cat_cuentas_contables')->where('cuenta', "6.1.1.2.1");
        $query_cuenta_existe = $this->db->get();
        $existe_cuenta = $query_cuenta_existe->row_array();

        $query_insertar_detalle_poliza = "INSERT INTO mov_poliza_detalle (
                                                                    numero_poliza, tipo_poliza, fecha, hora, fecha_real,
                                                                    id_cuenta, id_padre, nivel, concepto, subsidio,
                                                                    centro_costo, partida, concepto_especifico, cuenta,
                                                                    nombre, debe, haber)
                                                                    VALUES (
                                                                    ?, ?, NOW(), NOW(), NOW(),
                                                                    0, 0, 0, 'Cierre anual', 0,
                                                                    0, 0, 'Cierre anual', ?,
                                                                    ?, ?, ?);";

        $datos_detalle_poliza = array(
            $last,
            "Diario",
            "6.1.1.2.1",
            $existe_cuenta["nombre"],
            0,
            $suma_cargos_4,
        );

        $this->ciclo_model->insertar_detalle_poliza($query_insertar_detalle_poliza, $datos_detalle_poliza);


        $this->utilerias->insertar_poliza_directa_detalle(array(
            "numero_poliza" => 1,
            "cuenta" => "6.1.1.2.1",
            "cargos" => $suma_abonos_5,
            "abonos" => 0,
        ));

        $this->db->select('nombre')->from('cat_cuentas_contables')->where('cuenta', "6.1.1.2.1");
        $query_cuenta_existe = $this->db->get();
        $existe_cuenta = $query_cuenta_existe->row_array();

        $query_insertar_detalle_poliza = "INSERT INTO mov_poliza_detalle (
                                                                    numero_poliza, tipo_poliza, fecha, hora, fecha_real,
                                                                    id_cuenta, id_padre, nivel, concepto, subsidio,
                                                                    centro_costo, partida, concepto_especifico, cuenta,
                                                                    nombre, debe, haber)
                                                                    VALUES (
                                                                    ?, ?, NOW(), NOW(), NOW(),
                                                                    0, 0, 0, 'Cierre anual', 0,
                                                                    0, 0, 'Cierre anual', ?,
                                                                    ?, ?, ?);";

        $datos_detalle_poliza = array(
            $last,
            "Diario",
            "6.1.1.2.1",
            $existe_cuenta["nombre"],
            $suma_abonos_5,
            0,
        );

        $this->ciclo_model->insertar_detalle_poliza($query_insertar_detalle_poliza, $datos_detalle_poliza);

        $this->db->trans_complete();


        $diferencia_final = 0;
        $cargos_finales = 0;
        $abonos_finales = 0;

        if ($suma_cargos > $suma_abonos) {

            $diferencia_final = $suma_cargos - $suma_abonos;
            $cargos_finales = $diferencia_final;

        } elseif ($suma_abonos > $suma_cargos) {

            $diferencia_final = $suma_abonos - $suma_cargos;
            $abonos_finales = $diferencia_final;

        }

        $this->db->trans_start();

        $this->utilerias->insertar_poliza_directa_detalle(array(
            "numero_poliza" => 1,
            "cuenta" => "6.1.1.2.1",
            "cargos" => $cargos_finales,
            "abonos" => $abonos_finales,
        ));

        $this->db->select('nombre')->from('cat_cuentas_contables')->where('cuenta', "6.1.1.2.1");
        $query_cuenta_existe = $this->db->get();
        $existe_cuenta = $query_cuenta_existe->row_array();

        $query_insertar_detalle_poliza = "INSERT INTO mov_poliza_detalle (
                                                                    numero_poliza, tipo_poliza, fecha, hora, fecha_real,
                                                                    id_cuenta, id_padre, nivel, concepto, subsidio,
                                                                    centro_costo, partida, concepto_especifico, cuenta,
                                                                    nombre, debe, haber)
                                                                    VALUES (
                                                                    ?, ?, NOW(), NOW(), NOW(),
                                                                    0, 0, 0, 'Cierre anual', 0,
                                                                    0, 0, 'Cierre anual', ?,
                                                                    ?, ?, ?);";

        $datos_detalle_poliza = array(
            $last,
            "Diario",
            "6.1.1.2.1",
            $existe_cuenta["nombre"],
            $cargos_finales,
            $abonos_finales,
        );

        $this->ciclo_model->insertar_detalle_poliza($query_insertar_detalle_poliza, $datos_detalle_poliza);

        if ($cargos_finales > $abonos_finales) {

            $this->utilerias->insertar_poliza_directa_detalle(array(
                "numero_poliza" => 1,
                "cuenta" => "6.2.1.2.1",
                "cargos" => $abonos_finales,
                "abonos" => $cargos_finales,
            ));

            $this->db->select('nombre')->from('cat_cuentas_contables')->where('cuenta', "6.2.1.2.1");
            $query_cuenta_existe = $this->db->get();
            $existe_cuenta = $query_cuenta_existe->row_array();

            $query_insertar_detalle_poliza = "INSERT INTO mov_poliza_detalle (
                                                                    numero_poliza, tipo_poliza, fecha, hora, fecha_real,
                                                                    id_cuenta, id_padre, nivel, concepto, subsidio,
                                                                    centro_costo, partida, concepto_especifico, cuenta,
                                                                    nombre, debe, haber)
                                                                    VALUES (
                                                                    ?, ?, NOW(), NOW(), NOW(),
                                                                    0, 0, 0, 'Cierre anual', 0,
                                                                    0, 0, 'Cierre anual', ?,
                                                                    ?, ?, ?);";

            $datos_detalle_poliza = array(
                $last,
                "Diario",
                "6.2.1.2.1",
                $existe_cuenta["nombre"],
                $abonos_finales,
                $cargos_finales,
            );

            $this->ciclo_model->insertar_detalle_poliza($query_insertar_detalle_poliza, $datos_detalle_poliza);

            $this->utilerias->insertar_poliza_directa_detalle(array(
                "numero_poliza" => 2,
                "cuenta" => "6.2.1.2.1",
                "cargos" => $cargos_finales,
                "abonos" => $abonos_finales,
            ));

            $this->db->select('nombre')->from('cat_cuentas_contables')->where('cuenta', "6.2.1.2.1");
            $query_cuenta_existe = $this->db->get();
            $existe_cuenta = $query_cuenta_existe->row_array();

            $query_insertar_detalle_poliza = "INSERT INTO mov_poliza_detalle (
                                                                    numero_poliza, tipo_poliza, fecha, hora, fecha_real,
                                                                    id_cuenta, id_padre, nivel, concepto, subsidio,
                                                                    centro_costo, partida, concepto_especifico, cuenta,
                                                                    nombre, debe, haber)
                                                                    VALUES (
                                                                    ?, ?, NOW(), NOW(), NOW(),
                                                                    0, 0, 0, 'Cierre anual', 0,
                                                                    0, 0, 'Cierre anual', ?,
                                                                    ?, ?, ?);";

            $datos_detalle_poliza = array(
                $nuevo_last,
                "Diario",
                "6.2.1.2.1",
                $existe_cuenta["nombre"],
                $cargos_finales,
                $abonos_finales,
            );

            $this->ciclo_model->insertar_detalle_poliza($query_insertar_detalle_poliza, $datos_detalle_poliza);

            $this->utilerias->insertar_poliza_directa_detalle(array(
                "numero_poliza" => 2,
                "cuenta" => "3.2.1.1.1.1",
                "cargos" => $abonos_finales,
                "abonos" => $cargos_finales,
            ));

            $this->db->select('nombre')->from('cat_cuentas_contables')->where('cuenta', "3.2.1.1.1.1");
            $query_cuenta_existe = $this->db->get();
            $existe_cuenta = $query_cuenta_existe->row_array();

            $query_insertar_detalle_poliza = "INSERT INTO mov_poliza_detalle (
                                                                    numero_poliza, tipo_poliza, fecha, hora, fecha_real,
                                                                    id_cuenta, id_padre, nivel, concepto, subsidio,
                                                                    centro_costo, partida, concepto_especifico, cuenta,
                                                                    nombre, debe, haber)
                                                                    VALUES (
                                                                    ?, ?, NOW(), NOW(), NOW(),
                                                                    0, 0, 0, 'Cierre anual', 0,
                                                                    0, 0, 'Cierre anual', ?,
                                                                    ?, ?, ?);";

            $datos_detalle_poliza = array(
                $nuevo_last,
                "Diario",
                "3.2.1.1.1.1",
                $existe_cuenta["nombre"],
                $abonos_finales,
                $cargos_finales,
            );

            $this->ciclo_model->insertar_detalle_poliza($query_insertar_detalle_poliza, $datos_detalle_poliza);

        } elseif ($abonos_finales > $cargos_finales) {

            $this->utilerias->insertar_poliza_directa_detalle(array(
                "numero_poliza" => 1,
                "cuenta" => "6.3.1.2.1",
                "cargos" => $abonos_finales,
                "abonos" => $cargos_finales,
            ));

            $this->db->select('nombre')->from('cat_cuentas_contables')->where('cuenta', "6.3.1.2.1");
            $query_cuenta_existe = $this->db->get();
            $existe_cuenta = $query_cuenta_existe->row_array();

            $query_insertar_detalle_poliza = "INSERT INTO mov_poliza_detalle (
                                                                    numero_poliza, tipo_poliza, fecha, hora, fecha_real,
                                                                    id_cuenta, id_padre, nivel, concepto, subsidio,
                                                                    centro_costo, partida, concepto_especifico, cuenta,
                                                                    nombre, debe, haber)
                                                                    VALUES (
                                                                    ?, ?, NOW(), NOW(), NOW(),
                                                                    0, 0, 0, 'Cierre anual', 0,
                                                                    0, 0, 'Cierre anual', ?,
                                                                    ?, ?, ?);";

            $datos_detalle_poliza = array(
                $last,
                "Diario",
                "6.3.1.2.1",
                $existe_cuenta["nombre"],
                $abonos_finales,
                $cargos_finales,
            );

            $this->ciclo_model->insertar_detalle_poliza($query_insertar_detalle_poliza, $datos_detalle_poliza);

            $this->utilerias->insertar_poliza_directa_detalle(array(
                "numero_poliza" => 2,
                "cuenta" => "6.3.1.2.1",
                "cargos" => $cargos_finales,
                "abonos" => $abonos_finales,
            ));

            $this->db->select('nombre')->from('cat_cuentas_contables')->where('cuenta', "6.3.1.2.1");
            $query_cuenta_existe = $this->db->get();
            $existe_cuenta = $query_cuenta_existe->row_array();

            $query_insertar_detalle_poliza = "INSERT INTO mov_poliza_detalle (
                                                                    numero_poliza, tipo_poliza, fecha, hora, fecha_real,
                                                                    id_cuenta, id_padre, nivel, concepto, subsidio,
                                                                    centro_costo, partida, concepto_especifico, cuenta,
                                                                    nombre, debe, haber)
                                                                    VALUES (
                                                                    ?, ?, NOW(), NOW(), NOW(),
                                                                    0, 0, 0, 'Cierre anual', 0,
                                                                    0, 0, 'Cierre anual', ?,
                                                                    ?, ?, ?);";

            $datos_detalle_poliza = array(
                $nuevo_last,
                "Diario",
                "6.3.1.2.1",
                $existe_cuenta["nombre"],
                $cargos_finales,
                $abonos_finales,
            );

            $this->ciclo_model->insertar_detalle_poliza($query_insertar_detalle_poliza, $datos_detalle_poliza);

            $this->utilerias->insertar_poliza_directa_detalle(array(
                "numero_poliza" => 2,
                "cuenta" => "3.2.1.1.1.2",
                "cargos" => $abonos_finales,
                "abonos" => $cargos_finales,
            ));

            $this->db->select('nombre')->from('cat_cuentas_contables')->where('cuenta', "3.2.1.1.1.2");
            $query_cuenta_existe = $this->db->get();
            $existe_cuenta = $query_cuenta_existe->row_array();

            $query_insertar_detalle_poliza = "INSERT INTO mov_poliza_detalle (
                                                                    numero_poliza, tipo_poliza, fecha, hora, fecha_real,
                                                                    id_cuenta, id_padre, nivel, concepto, subsidio,
                                                                    centro_costo, partida, concepto_especifico, cuenta,
                                                                    nombre, debe, haber)
                                                                    VALUES (
                                                                    ?, ?, NOW(), NOW(), NOW(),
                                                                    0, 0, 0, 'Cierre anual', 0,
                                                                    0, 0, 'Cierre anual', ?,
                                                                    ?, ?, ?);";

            $datos_detalle_poliza = array(
                $nuevo_last,
                "Diario",
                "3.2.1.1.1.2",
                $existe_cuenta["nombre"],
                $abonos_finales,
                $cargos_finales,
            );

            $this->ciclo_model->insertar_detalle_poliza($query_insertar_detalle_poliza, $datos_detalle_poliza);

        }

        $datos_actualizar_caratula = array(
            'cargos' => $cargos_finales,
            'abonos' => $abonos_finales,
        );

        $this->db->where('numero_poliza', $last);
        $this->db->update('mov_polizas_cabecera', $datos_actualizar_caratula);

        $datos_actualizar_caratula_2 = array(
            'cargos' => $cargos_finales,
            'abonos' => $abonos_finales,
        );

        $this->db->where('numero_poliza', $nuevo_last);
        $this->db->update('mov_polizas_cabecera', $datos_actualizar_caratula_2);

        $this->db->trans_complete();

//        $this->benchmark->mark('code_end');
//        echo $this->benchmark->elapsed_time('code_start', 'code_end');

        return '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Se ha generado la poliza de Diario No. '.$last.' </div><div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Se ha generado la poliza de Diario No. '.$nuevo_last.' </div>';

    }

    function revisar_tabla_cierre_anual() {
        $this->db->select('*')->from('poliza_detalle_cierre_anual')->limit(1);
        $query = $this->db->get();
        $resultado = $query->row_array();
        if(isset($resultado["cuenta"])) {
            echo(json_encode(array(
                "mensaje" => "mostrar"
            )));
        } else {
            echo(json_encode(array(
                "mensaje" => "ocultar"
            )));
        }
    }

    function exportar_cierre_anual_detalle() {
        $this->load->library('excel');
        $fecha = date("Y-m-d");
        $hora = date("H:i:s");

        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Póliza Cierre del Ejercicio');
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('Educal');
        $objDrawing->setPath('img/logo2.png');
        $objDrawing->setOffsetX(50);
        $objDrawing->setHeight(80);
        // s$objDrawing->setWidth(10);
        $objDrawing->setCoordinates('A1');
        $objDrawing->setWorksheet($this->excel->getActiveSheet());

        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
            'font'  => array(
                'bold'  => true,
            )
        );
        $style_center = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );
        $style_left = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            )
        );
        $style_right = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
            )
        );

        $this->excel->getActiveSheet()->mergeCells('B1:D1')->getStyle("B1")->applyFromArray($style);
        $this->excel->getActiveSheet()->setCellValue('B1','EDUCAL, S.A. DE C.V.');
        $this->excel->getActiveSheet()->mergeCells('B2:D2')->getStyle("B2")->applyFromArray($style);
        $this->excel->getActiveSheet()->setCellValue('B2', 'Póliza Cierre del Ejercicio');
        $this->excel->getActiveSheet()->getStyle("B3:D3")->applyFromArray($style);
        $this->excel->getActiveSheet()->getStyle("B4:D4")->applyFromArray($style_center);
        $this->excel->getActiveSheet()->setCellValue('B3', 'Fecha Emisión');
        $this->excel->getActiveSheet()->setCellValue('D3', 'Hora Emisión');
        $this->excel->getActiveSheet()->setCellValue('B4', $fecha);
        $this->excel->getActiveSheet()->setCellValue('D4', $hora);
        $this->excel->getActiveSheet()->getStyle("C:D")->applyFromArray($style_right);
        $this->excel->getActiveSheet()->getStyle("A6:D6")->applyFromArray($style);
        $this->excel->getActiveSheet()->mergeCells('A5:D5');

        $this->excel->getActiveSheet()->setCellValue('A6', 'Cuenta')->getColumnDimension("A")->setWidth(30);
        $this->excel->getActiveSheet()->setCellValue('B6', 'Descripción de Cuenta')->getColumnDimension("B")->setWidth(40);
        $this->excel->getActiveSheet()->setCellValue('C6', 'Cargos')->getColumnDimension("C")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('D6', 'Abonos')->getColumnDimension("D")->setWidth(20);

        $resultado = $this->contabilidad_model->get_polizas_cierre_anual_detalle();
        $row = 7;

        foreach($resultado as $key => $value) {

            $this->excel->getActiveSheet()->setCellValue('A'.$row, $value["cuenta"]);
            $this->excel->getActiveSheet()->setCellValue('B'.$row, $value["descripcion_cuenta"]);
            $this->excel->getActiveSheet()->setCellValue('C'.$row, '$'.($value["cargos"] == 0 || $value["cargos"] == NULL ? '0.00' : (number_format($value["cargos"], 2))));
            $this->excel->getActiveSheet()->setCellValue('D'.$row, '$'.($value["abonos"] == 0 || $value["abonos"] == NULL ? '0.00' : (number_format($value["abonos"], 2))));
            $row += 1;
        }

//        $this->benchmark->mark('code_start');
//        $this->benchmark->mark('code_end');
//        echo $this->benchmark->elapsed_time('code_start', 'code_end');

        $filename="Poliza Cierre del Ejercicio.xls"; // Agregar fecha en que se generó
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
//        save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
//        if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
//        force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }

    function exportar_cierre_anual_totales() {
        $this->load->library('excel');
        $fecha = date("Y-m-d");
        $hora = date("H:i:s");

        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Póliza Registro P o G');
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('Educal');
        $objDrawing->setPath('img/logo2.png');
        $objDrawing->setOffsetX(50);
        $objDrawing->setHeight(80);
        // $objDrawing->setWidth(10);
        $objDrawing->setCoordinates('A1');
        $objDrawing->setWorksheet($this->excel->getActiveSheet());

        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
            'font'  => array(
                'bold'  => true,
            )
        );
        $style_center = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );
        $style_left = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            )
        );
        $style_right = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
            )
        );

        $this->excel->getActiveSheet()->mergeCells('B1:D1')->getStyle("B1")->applyFromArray($style);
        $this->excel->getActiveSheet()->setCellValue('B1','EDUCAL, S.A. DE C.V.');
        $this->excel->getActiveSheet()->mergeCells('B2:D2')->getStyle("B2")->applyFromArray($style);
        $this->excel->getActiveSheet()->setCellValue('B2', 'Póliza Registro de Pérdida o Ganancia');
        $this->excel->getActiveSheet()->getStyle("B3:D3")->applyFromArray($style);
        $this->excel->getActiveSheet()->getStyle("B4:D4")->applyFromArray($style_center);
        $this->excel->getActiveSheet()->setCellValue('B3', 'Fecha Emisión');
        $this->excel->getActiveSheet()->setCellValue('D3', 'Hora Emisión');
        $this->excel->getActiveSheet()->setCellValue('B4', $fecha);
        $this->excel->getActiveSheet()->setCellValue('D4', $hora);
        $this->excel->getActiveSheet()->getStyle("C:D")->applyFromArray($style_right);
        $this->excel->getActiveSheet()->getStyle("A6:D6")->applyFromArray($style);
        $this->excel->getActiveSheet()->mergeCells('A5:D5');


        $this->excel->getActiveSheet()->setCellValue('A6', 'Cuenta')->getColumnDimension("A")->setWidth(30);
        $this->excel->getActiveSheet()->setCellValue('B6', 'Descripción de Cuenta')->getColumnDimension("B")->setWidth(40);
        $this->excel->getActiveSheet()->setCellValue('C6', 'Cargos')->getColumnDimension("C")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('D6', 'Abonos')->getColumnDimension("D")->setWidth(20);

        $resultado = $this->contabilidad_model->get_polizas_cierre_anual_total();
//      $this->debugeo->imprimir_pre($resultado);

        $row = 7;

        foreach($resultado as $key => $value) {

            $this->excel->getActiveSheet()->setCellValue('A'.$row, $value["cuenta"]);
            $this->excel->getActiveSheet()->setCellValue('B'.$row, $value["descripcion_cuenta"]);
            $this->excel->getActiveSheet()->setCellValue('C'.$row, '$'.($value["cargos"] == 0 || $value["cargos"] == NULL ? '0.00' : (number_format($value["cargos"], 2))));
            $this->excel->getActiveSheet()->setCellValue('D'.$row, '$'.($value["abonos"] == 0 || $value["abonos"] == NULL ? '0.00' : (number_format($value["abonos"], 2))));
            $row += 1;
        }

//        $this->benchmark->mark('code_start');
//        $this->benchmark->mark('code_end');
//        echo $this->benchmark->elapsed_time('code_start', 'code_end');

        $filename="Poliza Registro de Perdida o Ganancia.xls"; // Agregar fecha en que se generó
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
//        save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
//        if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
//        force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }

    function exportar_balanza_cierre() {
        
        $this->benchmark->mark('code_start');

        $year = date("Y");

        $balanza = $this->generar_tabla_principal_balanza_final();

        // $this->debugeo->imprimir_pre($balanza);

        $this->exportar_polizas_excel_balanza_final($balanza);

        echo("Terminado<br />");

        $this->benchmark->mark('code_end');
        echo $this->benchmark->elapsed_time('code_start', 'code_end');

    }

    function generar_tabla_principal_balanza_final() {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

        $year = date("Y");

//        $this->benchmark->mark('code_start');

       $datos = array(
           "fecha_inicial" => $year."-01-01",
           "fecha_final" => $year."-12-31",
       );

//        $this->debugeo->imprimir_pre($datos);

        $truncar = $this->db->truncate('balanza_temporal');

        if ($truncar) {
//            Se toman todas las cuentas del catalogo de cuentas contables
            $sql = "SELECT id_cuentas_contables, saldo_inicial, cuenta, nombre, tipo, cargo, abono, cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables ORDER BY cuenta;";
            $query = $this->db->query($sql);
            $cuentas_contables = $query->result_array();

//        echo("Cuentas contables: ");
//        $this->debugeo->imprimir_pre($cuentas_contables);

//        Después se toman todos los movimientos anteriores a los datos especificados
            $sql_anterior = "SELECT ccc.id_cuentas_contables AS id_cuentas, pd.cuenta, SUM(pd.debe) AS cargo, SUM(pd.haber) AS abono, ccc.cuenta_padre AS id_padre, pd.nivel
                            FROM mov_poliza_detalle pd
                                JOIN mov_polizas_cabecera pc
                                ON pd.numero_poliza = pc.numero_poliza
                                JOIN cat_cuentas_contables ccc
                                ON pd.cuenta = ccc.cuenta
                            WHERE pc.fecha < ?
                                AND pc.enfirme = 1
                                AND pc.cancelada = 0
                        GROUP BY cuenta ORDER BY cuenta;";
            $query_anterior = $this->db->query($sql_anterior, array($datos["fecha_inicial"]));
            $tabla_temporal_anterior = $query_anterior->result_array();

//        echo("Movimientos anteriores a la consulta: ");
//        $this->debugeo->imprimir_pre($tabla_temporal_anterior);
//
//        Después se toman las pólizas que se generaron en la fecha determinada
            $sql_consulta = "SELECT ccc.id_cuentas_contables AS id_cuentas, ccc.saldo_inicial, pd.cuenta, SUM(pd.debe) AS cargo, SUM(pd.haber) AS abono, ccc.cuenta_padre AS id_padre, pd.nivel, pd.numero_poliza
                    FROM mov_poliza_detalle pd
                        JOIN mov_polizas_cabecera pc
                        ON pd.numero_poliza = pc.numero_poliza
                        JOIN cat_cuentas_contables   ccc
                        ON pd.cuenta = ccc.cuenta
                    WHERE pc.fecha >= ?
                            AND pc.fecha <= ?
                            AND pc.enfirme = 1
                            AND pc.cancelada = 0
                            GROUP BY pd.cuenta;";
            $query_consulta = $this->db->query($sql_consulta, array($datos["fecha_inicial"], $datos["fecha_final"]));
            $resultado_consulta = $query_consulta->result_array();

//        echo("Movimientos dentro de la consulta: ");
//        $this->debugeo->imprimir_pre($resultado_consulta);

//        Esta seccion es para calcular los movimientos
            foreach ($tabla_temporal_anterior as $key => $value) {

//            Se toma el indice padre de la cuenta, si es que lo tiene
                $key_padre = array_search($value["id_padre"], array_column($cuentas_contables, 'cuenta'));

//            Se recorre un ciclo donde, mientras la llave padre siga siendo diferente de 0, entra en él
                do {
//                Se suman los cargos y abonos de la tabla, al catalodgo de cuentas contables
                    $cuentas_contables[$key_padre]["cargo"] += $value["cargo"];
                    $cuentas_contables[$key_padre]["abono"] += $value["abono"];

//                Se toma el ID padre de la cuenta para poder ejecutar la recursividad, donde se van a volver a agregar los catgos y abonos, siempre y cuando el ID padre sea diferente de 0
                    $key_padre = array_search($cuentas_contables[$key_padre]["id_padre"], array_column($cuentas_contables, 'cuenta'));

                } while ($key_padre > -1);

//            Al finalizar el cálculo de los padres, se suman los cargos y abonos a la cuenta inicial, dentro del catálogo de cuentas
                $key_cuenta = array_search($value["cuenta"], array_column($cuentas_contables, 'cuenta'));

                $cuentas_contables[$key_cuenta]["cargo"] += $value["cargo"];
                $cuentas_contables[$key_cuenta]["abono"] += $value["abono"];

                unset($key);
                unset($value);
                unset($key_padre);
                unset($key_cuenta);

            }

            unset($key);
            unset($value);

//        echo("Cuentas contables después de calcular los movimientos anteriores al periodo: ");
//        $this->debugeo->imprimir_pre($cuentas_contables);

//        Se recorreo el arreglo de las cuentas contables para calcular el saldo final
            foreach ($cuentas_contables as $key => $value) {
                $cuentas_contables[$key]["saldo_final"] = $cuentas_contables[$key]["saldo_inicial"] + $cuentas_contables[$key]["cargo"] - $cuentas_contables[$key]["abono"];
                $cuentas_contables[$key]["saldo_inicial"] = $cuentas_contables[$key]["saldo_inicial"] + $cuentas_contables[$key]["cargo"] - $cuentas_contables[$key]["abono"];
//                $cuentas_contables[$key]["cargo"] = 0;
//                $cuentas_contables[$key]["abono"] = 0;
                $cuentas_contables[$key]["saldo_final"] = 0;
            }

            unset($key);
            unset($value);

//        echo("Cuentas contables después de calcular el saldo inicial ");
//        $this->debugeo->imprimir_pre($cuentas_contables);

//        Se recorre el arreglo de la consulta generada por el usuario
            foreach ($resultado_consulta as $key => $value) {
//                $this->debugeo->imprimir_pre($value["numero_poliza"]);
//
//                if($value["numero_poliza"] >= 12867) {
//                    $this->debugeo->imprimir_pre($value);
//                }

//            Se toma el indice padre de la cuenta, si es que lo tiene
                $key_padre = array_search($value["id_padre"], array_column($cuentas_contables, 'cuenta'));
//            echo("Llave padre inicial: ".$key_padre."<br />");

//            Se recorre un ciclo donde, mientras la llave padre siga siendo diferente de 0, entra en él
                do {
//                    $this->debugeo->imprimir_pre($value);

//                echo("Cuentas contables padre antes de agregar los cargos y abonos: ");
//                $this->debugeo->imprimir_pre($cuentas_contables[$key_padre]);

//                Se suman los cargos y abonos de la tabla, al catalodgo de cuentas contables
                    $cuentas_contables[$key_padre]["cargo"] += $value["cargo"];
                    $cuentas_contables[$key_padre]["abono"] += $value["abono"];

//                echo("Cuentas contables padre después de agregar los cargos y abonos: ");
//                $this->debugeo->imprimir_pre($cuentas_contables[$key_padre]);

//                Se toma el ID padre de la cuenta para poder ejecutar la recursividad, donde se van a volver a agregar los catgos y abonos, siempre y cuando el ID padre sea diferente de 0
                    $key_padre = array_search($cuentas_contables[$key_padre]["id_padre"], array_column($cuentas_contables, 'cuenta'));

//                echo("Nuevo ID Padre: ".$key_padre."<br />");

                } while ($key_padre > -1);

//            Al finalizar el cálculo de los padres, se suman los cargos y abonos a la cuenta inicial, dentro del catálogo de cuentas
                $key_cuenta = array_search($value["cuenta"], array_column($cuentas_contables, 'cuenta'));

//            echo("Ya salió del ciclo:<br />");
//            echo("ID de la cuenta: ".$key_cuenta."<br />");

//            echo("Cuentas contables antes de agregar los cargos y abonos al ID de la cuenta: ");
//            $this->debugeo->imprimir_pre($cuentas_contables[$key_cuenta]);

                $cuentas_contables[$key_cuenta]["cargo"] += $value["cargo"];
                $cuentas_contables[$key_cuenta]["abono"] += $value["abono"];

//            echo("Cuentas contables después de agregar los cargos y abonos al ID de la cuenta: ");
//            $this->debugeo->imprimir_pre($cuentas_contables[$key_cuenta]);

            }

            unset($key);
            unset($value);

//        echo("Cuentas contables después de calcular el saldo actual: ");
//        $this->debugeo->imprimir_pre($cuentas_contables);
       $balanza_regresar = array();

//        Se recorreo el arreglo de las cuentas contables para calcular el saldo final
            foreach ($cuentas_contables as $key => $value) {
//                $this->debugeo->imprimir_pre($key);
//                $this->debugeo->imprimir_pre($value);
               $balanza_regresar[] = array(
                    'cuenta' => $cuentas_contables[$key]["cuenta"],
                    'nombre' => $cuentas_contables[$key]["nombre"],
                    'tipo' => $cuentas_contables[$key]["tipo"],
                    'saldo_inicial' => ($cuentas_contables[$key]["saldo_inicial"] + $cuentas_contables[$key]["cargo"]) - $cuentas_contables[$key]["abono"],
                    'cargo' => 0,
                    'abono' => 0,
                    'saldo_final' => 0,
                    'nivel' => $cuentas_contables[$key]["nivel"],
                );

            }

//        echo("Cuentas contables después de calcular el saldo final: ");
//        $this->debugeo->imprimir_pre($cuentas_contables);

//        $this->benchmark->mark('code_end');
//        echo $this->benchmark->elapsed_time('code_start', 'code_end');

            return $balanza_regresar;
        }
    }

    function exportar_polizas_excel_balanza_final($datos) {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

        $this->load->library('excel');
//        $this->benchmark->mark('code_start');

        try {

            $objWorkSheet = $this->excel->createSheet();
            
            $objWorkSheet->setTitle('Saldos Iniciales');

            $objWorkSheet->setCellValue('A1', 'Cuenta');
            $objWorkSheet->setCellValue('B1', 'Nombre');
            $objWorkSheet->setCellValue('C1', 'Tipo');
            $objWorkSheet->setCellValue('D1', 'Saldo Inicial');
    
            // $this->generaEncabezadoExcelPolizas($objWorkSheet);
            
            $row = 2;

            foreach ($datos as $key => $value) {
                // $this->debugeo->imprimir_pre($value);
                $arreglo_datos = array(
                    "cuenta" => $value["cuenta"],
                    "nombre" => $value["nombre"],
                    "tipo" => $value["tipo"],
                    "saldo_inicial" => ($value["saldo_inicial"] == 0 || $value["saldo_inicial"] == NULL ? '0.00' : ($value["saldo_inicial"])),
                );

                $objWorkSheet->fromArray($arreglo_datos, NULL, 'A' . $row);

                $row += 1;

            }

//        $this->benchmark->mark('code_end');
//        echo $this->benchmark->elapsed_time('code_start', 'code_end');
            //Se remueve la primer hoja, que no tiene ninguna utilidad
            $this->excel->removeSheetByIndex(0);
            $this->exportaExcel("Saldos Iniciales.xls", $this->excel);

        } catch (Exception $e) {
            $respuesta = array(
                "mensaje" => "" . $e->getMessage(),
            );
        }
    }

}