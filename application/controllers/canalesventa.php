<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Canalesventa extends CI_Controller
{

    /**
     * Se revisa si el usuario esta logueado, si no esta logueado, se reenvia a la pantalla de login
     */
    function __construct()
    {
        parent::__construct();
        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        }
    }

    /**
     * Esta funcion es la principal, donde se muestra la pantalla de Canales de Venta
     */
    function index()
    {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha Ingresado  a canal de venta');
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Canales de Venta",
            "usuario" => $this->tank_auth->get_username(),
        );
        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/main_view');
        $this->load->view('front/footer_main_view', array("graficas" => TRUE));
    }

    /** Aqui empieza la sección de Canales de Venta */
    function indicecanales()
    {
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Canales de Venta",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/indices_canales_venta_view');
        $this->load->view('front/footer_main_view', array("tablas" => TRUE, "indicecanales" => TRUE));
    }
}