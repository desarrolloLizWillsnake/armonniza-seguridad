<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
ini_set("memory_limit","512M");

class Manuales extends CI_Controller
{

    /**
     * Se revisa si el usuario esta logueado, si no esta logueado, se reenvia a la pantalla de login
     */
    function __construct()
    {
        parent::__construct();
        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        }
    }

    /**
     * Esta funcion es la principal, donde se muestra la pantalla de Políticas y Manuales
     */
    function index()
    {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha entrado al modulo de manuales');
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Guías de Usuario",
            "usuario" => $this->tank_auth->get_username(),
        );
        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/main_view');
        $this->load->view('front/footer_main_view', array("graficas" => TRUE));
    }

    /** Aqui empieza la sección de Políticas y Manuales*/
    function manualespoliticas()
    {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha ingresado al manual de politicas');
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Guías de Usuario",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/indices_manuales_view');
        $this->load->view('front/footer_main_view', array("manuales" => TRUE, "tablas" => TRUE));
    }

}