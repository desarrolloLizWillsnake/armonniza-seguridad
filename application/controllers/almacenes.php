<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Almacenes extends CI_Controller
{

    /**
     * Se revisa si el usuario esta logueado, si no esta logueado, se reenvia a la pantalla de login
     */
    function __construct()
    {
        parent::__construct();
        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        }
    }

    /**
     * Esta funcion es la principal, donde se muestra la pantalla de Almacenes
     */
    function index()
    {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha ingresado al modulo de almacenes');
        if($this->utilerias->get_permisos("almacenes")|| $this->utilerias->get_grupo() == 1) {
            $datos_header = array(
                "titulo_pagina" => "Armonniza | Almacenes",
                "usuario" => $this->tank_auth->get_username(),
            );
            $this->parser->parse('front/header_main_view', $datos_header);
            $this->load->view('front/main_view');
            $this->load->view('front/footer_main_view', array("graficas" => TRUE));
        } else {
            redirect('/auth/login');
        }

    }

    /** Aqui empieza la sección de Almacenes */
    function indicealmacenes()
    {
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Almacenes",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/indices_almacenes_view');
        $this->load->view('front/footer_main_view', array("tablas" => TRUE, "indicecanales" => TRUE));
    }
}