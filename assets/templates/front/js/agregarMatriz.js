var datos_tabla_cog;
var datos_tabla_cri;
var datos_tabla_cuenta_cargo;
var datos_tabla_cuenta_abono;

$('#tabla_cog tbody').on( 'click', 'tr', function () {
    datos_tabla_cog = $('#tabla_cog').DataTable().row( this ).data();
});

$('#modal_cog').on('hidden.bs.modal', function () {
    $.ajax({
        url: '/contabilidad/tomar_datos_cog',
        dataType: 'json',
        method: 'POST',
        data: {
            cog: datos_tabla_cog[0]
        },
        success: function(s){
            if(s.mensaje == "ok") {

                $("#clave").val(s.clave);
                $("#titulo_clave").val(s.descripcion);

            }
            else {
                alert(s.mensaje);
            }
        },
        error: function(e){
            console.log(e.responseText);
        }
    });

});

$('#tabla_cri tbody').on( 'click', 'tr', function () {
    datos_tabla_cri = $('#tabla_cri').DataTable().row( this ).data();
});

$('#modal_cri').on('hidden.bs.modal', function () {
    $.ajax({
        url: '/contabilidad/tomar_datos_cri',
        dataType: 'json',
        method: 'POST',
        data: {
            cri: datos_tabla_cri[0]
        },
        success: function(s){
            if(s.mensaje == "ok") {

                $("#clave").val(s.clave);
                $("#titulo_clave").val(s.titulo);

            }
            else {
                alert(s.mensaje);
            }
        },
        error: function(e){
            console.log(e.responseText);
        }
    });

});

$('#tabla_cuenta_cargo tbody').on( 'click', 'tr', function () {
    datos_tabla_cuenta_cargo = $('#tabla_cuenta_cargo').DataTable().row( this ).data();
});

$('#modal_cuenta_cargo').on('hidden.bs.modal', function () {
    $.ajax({
        url: '/contabilidad/tomar_datos_cuenta',
        dataType: 'json',
        method: 'POST',
        data: {
            cuenta: datos_tabla_cuenta_cargo[0]
        },
        success: function(s){
            if(s.mensaje == "ok") {

                $("#cuenta_cargo").val(s.cuenta);
                $("#descripcion_cuenta_cargo").val(s.nombre);

            }
            else {
                alert(s.mensaje);
            }
        },
        error: function(e){
            console.log(e.responseText);
        }
    });

});

$('#tabla_cuenta_abono tbody').on( 'click', 'tr', function () {
    datos_tabla_cuenta_abono = $('#tabla_cuenta_abono').DataTable().row( this ).data();
});

$('#modal_cuenta_abono').on('hidden.bs.modal', function () {
    $.ajax({
        url: '/contabilidad/tomar_datos_cuenta',
        dataType: 'json',
        method: 'POST',
        data: {
            cuenta: datos_tabla_cuenta_abono[0]
        },
        success: function(s){
            if(s.mensaje == "ok") {

                $("#cuenta_abono").val(s.cuenta);
                $("#descripcion_cuenta_abono").val(s.nombre);

            }
            else {
                alert(s.mensaje);
            }
        },
        error: function(e){
            console.log(e.responseText);
        }
    });

});

$("#guardar_matriz").on( "click", function() {
    var map = {};
    $(":input").each(function() {
        map[$(this).attr("name")] = $(this).val();
    });

    map.tipo_gasto = $('input[name=tipo_gasto]:checked').val();

    $.ajax({
        url: '/contabilidad/insertar_matriz',
        method: 'POST',
        dataType: 'json',
        data: map,
        success: function(s){
            if(s) {
                location.href="/contabilidad/matriz";
            }
        },
        error: function(e){
            console.log(e.responseText);
        }
    });
});

$(document).ready(function() {

    $('#tabla_cog').dataTable({
        "ajax": "tabla_cog",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs": [ {
            "targets": [ 0 ],
            "visible": false,
            "searchable": false
        } ]
    });

    $('#tabla_cri').dataTable({
        "ajax": "tabla_cri",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs": [ {
            "targets": [ 0 ],
            "visible": false,
            "searchable": false
        } ]
    });

    $('#tabla_cuenta_cargo').dataTable({
        "ajax": "tabla_cuentas",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs": [ {
            "targets": [ 0 ],
            "visible": false,
            "searchable": false
        } ]
    });

    $('#tabla_cuenta_abono').dataTable({
        "ajax": "tabla_cuentas",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs": [ {
            "targets": [ 0 ],
            "visible": false,
            "searchable": false
        } ]
    });

});