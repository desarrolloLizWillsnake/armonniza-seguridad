/**
 * Created by Lizbeth on 26/10/2015.
 */
var editor;

$.fn.dataTable.TableTools.buttons.download = $.extend(
    true,
    {},
    $.fn.dataTable.TableTools.buttonBase,
    {
        "sButtonText": "Download",
        "sUrl":      "",
        "sType":     "POST",
        "fnData":    false,
        "fnClick": function( button, config ) {
            var dt = new $.fn.dataTable.Api( this.s.dt );
            var data = dt.ajax.params() || {};

            // Optional static additional parameters
            // data.customParameter = ...;

            if ( config.fnData ) {
                config.fnData( data );
            }

            var iframe = $('<iframe/>', {
                id: "RemotingIFrame"
            }).css( {
                border: 'none',
                width: 0,
                height: 0
            } )
                .appendTo( 'body' );

            var contentWindow = iframe[0].contentWindow;
            contentWindow.document.open();
            contentWindow.document.close();

            var form = contentWindow.document.createElement( 'form' );
            form.setAttribute( 'method', config.sType );
            form.setAttribute( 'action', config.sUrl );

            var input = contentWindow.document.createElement( 'input' );
            input.name = 'json';
            input.value = JSON.stringify( data );

            form.appendChild( input );
            contentWindow.document.body.appendChild( form );
            form.submit();
        }
    }
);

$(document).ready(function() {
    editor = new $.fn.dataTable.Editor( {
        ajax: "/contabilidad/tabla_cuenta",
        table: ".datos_tabla",
        fields: [ {
            label: "Cuenta",
            name: "cuenta"
        }, {
            label: "Nombre",
            name: "nombre"
        }, {
            label: "Tipo",
            name:  "tipo",
            dataProp: "1",
            type:  "select",
            ipOpts: getTipoCuenta()

        }, {
            label: "Cuenta Padre:",
            name: "cuenta_padre",
            dataProp: "1",
            type: "select",
            ipOpts: getListaCuentas()
        }
        ],
        i18n: {
            create: {
                button: "<i class='fa fa-plus-circle circle ic-catalogo'></i> Nuevo",
                title:  "<i class='fa fa-plus-circle ic-catalogo-emergente'></i> Nuevo Plan de Cuenta Contable",
                submit: "Ingresar"
            },
            edit: {
                button: "<i class='fa fa-edit circle ic-catalogo'></i> Editar",
                title:  "<i class='fa fa-edit ic-catalogo-emergente'></i> Editar Plan de Cuenta Contable",
                submit: "Actualizar"
            },
            remove: {
                button: "<i class='fa fa-trash-o circle ic-catalogo'></i> Borrar",
                title:  "<i class='fa fa-trash-o ic-catalogo-emergente'></i> Borrar",
                submit: "Borrar",
                confirm: {
                    _: "�Esta seguro que desea eliminar los %d Planes de Cuentas Contables seleccionados?",
                    1: "�Esta seguro que desea eliminar el Plan de Cuenta Contable seleccionado?"
                }
            },
            error: {
                system: "Ha ocurrido un error, por favor, contacte al administrador del sistema."
            }
        }
    } );

    editor.on( 'postCreate', function ( e, json, data ) {

        $.ajax({
            url: '/contabilidad/acomodar_cuentas',
            data: { cuenta: data.cuenta },
            method: 'POST',
            success: function(s){
                console.log(s);
            },
            error: function(e){
                console.log(e.responseText);
            }
        });

    } );

    $('.datos_tabla').DataTable( {
        dom: '<"clear">lfrtip',
        ajax: {
            url: "/contabilidad/tabla_cuenta",
            type: "POST"
        },
        serverSide: true,
        columns: [
            { data: "cuenta" },
            { data: "nombre" },
            {
                data: "tipo",
                render: function (val, type, row) {
                    return val == "A" ? "Acumulativa" : "Detalle";
                }
            }
        ],
        oTableTools: {
            "aButtons": [ {
                "sExtends":    "download",
                "sButtonText": "Download XLS",
                "sUrl":        "/generate_xls.php"
            } ]
        },
        tableTools: {
            sRowSelect: "os",
            sSwfPath: js_base_url("assets/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"),
            aButtons: [
                { sExtends: "editor_create", editor: editor },
                { sExtends: "editor_edit",   editor: editor },
                { sExtends: "editor_remove", editor: editor },
                {
                    "sExtends":    "download",
                    "sButtonText": "<i class='fa fa-download ic-catalogo'></i> Descargar",
                    "sUrl":        "/contabilidad/exportar_plan_cuentas"
                }
            ]
        },
        language: {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ning�n dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "�ltimo",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    } );

} );

function getListaCuentas() {

    var listaCuentas = new Array();

    $.ajax({
        url: '/contabilidad/select_cuentas',
        async: false,
        dataType: 'json',
        method: 'POST',
        success: function(s){
            for (i = 0; i < s.length; i++) {
                obj = { "label" : s[i].cuenta, "value" : s[i].cuenta};
                listaCuentas.push(obj);
            }

        },
        error: function(e){
            console.log(e.responseText);
        }
    });

    return listaCuentas;
}

function getTipoCuenta() {

    var listaTipoCuenta = new Array();
    obj = { "label" : "Acumulativa", "value" : "A" };
    listaTipoCuenta.push(obj);
    obj2 = { "label" : "Detalle", "value" : "D" };
    listaTipoCuenta.push(obj2);

    return listaTipoCuenta;
}