var datos_proveedor;
var datos_cuentas;
var datos_centro_costo;
var datos_subsidio;
var datos_tabla_detalle;
var d = new Date();
var curr_date = d.getDate();
var curr_month = d.getMonth() + 1; //Months are zero based
var curr_year = d.getFullYear();

//Esta funcion es para traducir el datepicker
$(function($){
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);
});

//Se crean las cajas para la seleccion de fecha
//$( "#fecha_solicitud" ).datepicker({ dateFormat: 'yy-mm-dd', minDate: new Date() })

$('#tabla_proveedores tbody').on( 'click', 'tr', function () {
    datos_proveedor = $('#tabla_proveedores').DataTable().row( this ).data();
});

$('#modal_proveedores').on('hidden.bs.modal', function () {
    $('#proveedor').val(datos_proveedor[2]);
    $('#id_proveedor').val(datos_proveedor[1]);
});

$('#tabla_cuentas tbody').on( 'click', 'tr', function () {
    datos_cuentas = $('#tabla_cuentas').DataTable().row( this ).data();
});

$('#modal_cuenta').on('hidden.bs.modal', function () {
    $('#cuenta').val(datos_cuentas[0]);
    $('#descripcion_cuenta').val(datos_cuentas[1]);
});

$('#tabla_centro_costos tbody').on( 'click', 'tr', function () {
    datos_centro_costo = $('#tabla_centro_costos').DataTable().row( this ).data();
});

$('#modal_centro_costo').on('hidden.bs.modal', function () {
    $('#centro_costos').val(datos_centro_costo[0]);
});

$('#tabla_subsidio tbody').on( 'click', 'tr', function () {
    datos_subsidio = $('#tabla_subsidio').DataTable().row( this ).data();
});

$('#modal_subsidio').on('hidden.bs.modal', function () {
    $('#subsidio').val(datos_subsidio[1]);
});

$('#datos_tabla tbody').on( 'click', 'tr', function () {
    datos_tabla_detalle = $('#datos_tabla').DataTable().row( this ).data();

    $("#borrar_detalle_poliza").val(datos_tabla_detalle[0]);
    $("#editar_id_detalle_poliza").val(datos_tabla_detalle[0]);
    $("#editar_cuenta_poliza").val(datos_tabla_detalle[1]);
    $("#editar_centro_costo_poliza").val(datos_tabla_detalle[2]);
    $("#editar_subsidio_poliza").val(datos_tabla_detalle[3]);
    $("#editar_concepto_poliza").val(datos_tabla_detalle[4]);
    $("#editar_debe_poliza").val(datos_tabla_detalle[6]);
    $("#editar_haber_poliza").val(datos_tabla_detalle[7]);

});

$(document).ready(function() {

    $('#tabla_proveedores').dataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "/ciclo/tabla_proveedores_AJAX",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs": [ {
            "targets":  0 ,
            "visible": false,
            "searchable": false
        }, {
            "targets": -1,
            "data": null,
            "defaultContent":
                '<a data-dismiss="modal"><i class="fa fa-check"></i></a>'
        } ]
    });

    $('#tabla_cuentas').dataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "tabla_cuentas",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs": [ {
            "targets": -1,
            "data": null,
            "defaultContent":
                '<a data-dismiss="modal"><i class="fa fa-check"></i></a>'
        } ]
    });

    $('#tabla_centro_costos').dataTable({
        "ajax": "tabla_centro_costos",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });

    $('#tabla_subsidio').dataTable({
        "ajax": "tabla_subsidio",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });

    $('#datos_tabla').dataTable({
        "ajax": "/contabilidad/tabla_detalle_poliza",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "NingÃºn dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Ãšltimo",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs": [ {
            "targets":  0 ,
            "visible": false,
            "searchable": false
        }, {
            "targets": [ 6 , 7 ],
            "render": function ( data, type, row ) {
                return '<span style="text-align: left; float: left;">$</span>'+'<span style="text-align: right; float: right;">'+ data +'</span>';
            }
        } ]
    });

});

$("#guardar_poliza_detalle").on( "click", function() {

    var map = {};
    $(":input").each(function() {
        map[$(this).attr("name")] = $(this).val();
    });

    if($('#check_firme').is(':checked')){
        map.check_firme = 1;
    } else {
        map.check_firme = 0;
    }

    $(".forma_poliza_general").validate({
        focusInvalid: true,
        rules: {
            centro_costos: {
                required: true
            },
            concepto: {
                required: true
            },
            concepto_detalle: {
                required: true
            },
            cuenta: {
                required: true
            },
            debe: {
                required: true
            },
            descripcion_cuenta: {
                required: true
            },
            fecha: {
                required: true,
                date: true
            },
            haber: {
                required: true
            },
            subsidio: {
                required: true
            },
            tipo_poliza: {
                required: true
            },
            importe: {
                required: true
            }
        },
        messages: {
            centro_costos: {
                required: "* Este campo es obligatorio"
            },
            concepto: {
                required: "* Este campo es obligatorio"
            },
            concepto_detalle: {
                required: "* Este campo es obligatorio"
            },
            cuenta: {
                required: "* Este campo es obligatorio"
            },
            debe: {
                required: "* Este campo es obligatorio"
            },
            descripcion_cuenta: {
                required: "* Este campo es obligatorio"
            },
            fecha: {
                required: "* Este campo es obligatorio",
                date: "El campo tiene que tener el siguiente formato AAAA-MM-DD"
            },
            haber: {
                required: "* Este campo es obligatorio"
            },
            subsidio: {
                required: "* Este campo es obligatorio"
            },
            tipo_poliza: {
                required: "* Este campo es obligatorio"
            },
            importe: {
                required: "* Este campo es obligatorio"
            }
        }
    }).form();

    if($(".forma_poliza_general").valid()){
        $.ajax({
            url: '/contabilidad/insertar_detalle_poliza',
            method: 'POST',
            dataType: 'json',
            data: map,
            success: function(s){
                if(s.cargos != s.abonos) {
                    $('#check_firme').prop('disabled', true);
                    $("#sumas_totales").html("<div class='row'><div class='col-lg-3'></div><div class='col-lg-2 table-poliza-result-f'>Cargos"+ "<br><span style='color:#848484; font-weight: normal; line-height: 200%;'>" + " $ "+$.number(s.cargos, 2 )+ "</span></div><div class='col-lg-2'></div>"+"<div class='col-lg-2 table-poliza-result-f'>Abonos"+ "<br><span style='color:#848484;font-weight: normal; line-height: 200%;'>" + " $ "+$.number( s.abonos, 2 )+ "</span></div><div class='col-lg-3'></div></div>");

                } else {
                    $('#check_firme').prop('disabled', false);
                    $("#sumas_totales").html("<div class='row'><div class='col-lg-3'></div><div class='col-lg-2 table-poliza-result'>Cargos"+ "<br><span style='color:#848484; font-weight: normal; line-height: 200%;'>" + " $ "+$.number(s.cargos, 2 )+ "</span></div><div class='col-lg-2'></div>"+"<div class='col-lg-2 table-poliza-result'>Abonos"+ "<br><span style='color:#848484;font-weight: normal; line-height: 200%;'>" + " $ "+$.number( s.abonos, 2 )+ "</span></div><div class='col-lg-3'></div></div>");
                }

                $("#resultado_insertar_detalle").html(s.mensaje);
                refrescarDetalle();
            },
            error: function(e){
                console.log(e.responseText);
            }
        });
    }

});

$("#guardar_poliza").on( "click", function() {
    $(".forma_poliza_general").validate({
        focusInvalid: true,
        rules: {
            tipo_poliza:{
                required: true
            },
            concepto_especifico:{
                required: true
            },
            concepto: {
                required: true
            },
            fecha: {
                required: true,
                date: true
            },
            cuenta: {
                required: false
            },
            centro_costos: {
                required: false
            },
            subsidio: {
                required: false
            },
            concepto_detalle: {
                required: false
            },
            importe: {
                required: true
            }
        },
        messages: {
            tipo_poliza: {
                required: "* Este campo es obligatorio"
            },
            proveedor: {
                required: "* Este campo es obligatorio"
            },
            concepto_especifico: {
                required: "* Este campo es obligatorio"
            },
            concepto: {
                required: "* Este campo es obligatorio"
            },
            fecha: {
                required: "* Este campo es obligatorio",
                date: "El campo tiene que tener el siguiente formato AAAA-MM-DD"
            },
            importe: {
                required: "* Este campo es obligatorio"
            }
        }
    }).form();

    if($(".forma_poliza_general").valid()) {
        var map = {};
        $(":input").each(function () {
            map[$(this).attr("name")] = $(this).val();
        });

        if(map.no_movimiento.indexOf(', ') != -1){
            var newchar = ','
            map.no_movimiento = map.no_movimiento.split(',').join(newchar);
        }else if(map.no_movimiento.indexOf(',') != -1){
            var newchar = ', '
            map.no_movimiento = map.no_movimiento.split(',').join(newchar);
        }

        if ($('#check_firme').is(':checked')) {
            map.check_firme = 1;
        } else {
            map.check_firme = 0;
        }

        $.ajax({
            url: '/contabilidad/insertar_poliza',
            method: 'POST',
            dataType: 'json',
            data: map,
            success: function (s) {
                $("#resultado_insertar_caratula").html(s.mensaje);
                $("#guardar_poliza").prop('disabled', true);
            },
            error: function (e) {
                console.log(e.responseText);
            }
        });
    }
});

$("#elegir_editar_detalle").click(function() {
    var map = {};
    $(":input").each(function() {
        map[$(this).attr("name")] = $(this).val();
    });

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/contabilidad/editar_detalle_partida",
        data: map
    })
        .done(function( data ) {
            $("#resultado_insertar_detalle").html(data.mensaje);
            refrescarDetalle();
        })
        .fail(function(e) {
            console.log(e.responseText);
        });
});

function refrescarDetalle() {
    $.ajax({
        url: '/contabilidad/tabla_detalle_poliza',
        dataType: 'json',
        method: 'POST',
        data: {
            poliza: $("#ultima_poliza").val()
        },
        success: function(s){
            $('#datos_tabla').dataTable().fnClearTable();
            var debe = 0;
            var haber = 0;
            for(var i = 0; i < s.length; i++) {
                debe += parseFloat(s[i][6]);
                haber += parseFloat(s[i][7]);
                $('#datos_tabla').dataTable().fnAddData([
                    s[i][0],
                    s[i][1],
                    s[i][2],
                    s[i][3],
                    s[i][4],
                    s[i][5],
                    $.number( s[i][6], 2 ),
                    $.number( s[i][7], 2 ),
                    s[i][8],
                    s[i][9]
                ]);
            } // End For

            if(debe != haber) {
                $('#check_firme').prop('disabled', true);
            } else {
                $('#check_firme').prop('disabled', false);
            }

            $("#debe_hidden").val(debe);
            $("#haber_hidden").val(haber);
        },
        error: function(e){
            console.log(e.responseText);
        }
    });
}

$("#elegir_borrar_movimiento").click(function() {
    var movimiento = $("#borrar_detalle_poliza").val();

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/contabilidad/borrar_detalle_movimiento",
        data: {
            movimiento: movimiento
        }
    })
        .done(function( data ) {
            $("#resultado_insertar_detalle").html(data.mensaje);
            refrescarDetalle();
        })
        .fail(function(e) {
            console.log(e.responseText);
        });
});

function submitFile() {

    var formData = new FormData($('#forma_poliza_detalle_archivo')[0]);
    $(":input").each(function() {
        formData.append($(this).attr("name"), $(this).val());
    });

    $(".forma_poliza_general").validate({
        focusInvalid: true,
        rules: {
            fecha: {
                required: true,
                date: true
            },
            concepto: {
                required: true
            },
            concepto_especifico: {
                required: true
            },
            tipo_poliza: {
                required: true
            },
            centro_costos: {
                required: false
            },
            concepto_detalle: {
                required: false
            },
            cuenta: {
                required: false
            },
            debe: {
                required: false
            },
            descripcion_cuenta: {
                required: false
            },
            haber: {
                required: false
            },
            proveedor: {
                required: false
            },
            subsidio: {
                required: false
            },
            importe: {
                required: true
            }
        },
        messages: {
            fecha: {
                required: "* Este campo es obligatorio",
                date: "El campo tiene que tener el siguiente formato AAAA-MM-DD"
            },
            concepto: {
                required: "* Este campo es obligatorio"
            },
            concepto_especifico: {
                required: "* Este campo es obligatorio"
            },
            tipo_poliza: {
                required: "* Este campo es obligatorio"
            },
            importe: {
                required: "* Este campo es obligatorio"
            }
        }
    }).form();

    if($(".forma_poliza_general").valid()){
        $.ajax({
            url: '/contabilidad/subir_detalle_poliza',
            type: 'POST',
            dataType: 'json',
            data: formData,
            mimeType: "multipart/form-data",
            contentType: false,
            cache: false,
            processData: false,
            success: function(data, textSatus, jqXHR){
                //now get here response returned by PHP in JSON fomat you can parse it using JSON.parse(data)
                if(data.status == 200) {
                    $('#subirArchivo').modal('hide');
                    refrescarDetalle();
                } else {
                    $('#mensaje_resultado_detalle_poliza').html(data.mensaje);
                }
            },
            error: function(jqXHR, textStatus, errorThrown){
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            }
        });
    } else {
        $('#subirArchivo').modal('hide');
    }
}

