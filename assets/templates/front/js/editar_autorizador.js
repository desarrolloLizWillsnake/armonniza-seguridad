/**
 * Created by Lizbeth on 19/10/2015.
 */

/*Autorizaciones Ciclo*/
$("#guardar_autorizacion").on( "click", function(e) {
    e.preventDefault();

    var map = {};
    $(":input").each(function() {
        map[$(this).attr("name")] = $(this).val();
    });

    map.tipo_radio = $('input[name=tipo_radio]:checked').val();

    $.ajax({
        url: '/administrador/actualizar_autorizador_ciclo',
        method: 'POST',
        dataType: 'json',
        data: map,
        success: function(s){
            $("#resultado_insertar_caratula").html(s.mensaje);
        },
        error: function(e){
            console.log(e.responseText);
        }
    });
});

/*Autorizaciones Contabilidad*/
$("#guardar_autorizacion_conta").on( "click", function(e) {
    e.preventDefault();

    var map = {};
    $(":input").each(function() {
        map[$(this).attr("name")] = $(this).val();
    });

    map.tipo_radio = $('input[name=tipo_radio]:checked').val();

    $.ajax({
        url: '/administrador/actualizar_autorizador_contabilidad',
        method: 'POST',
        dataType: 'json',
        data: map,
        success: function(s){
            $("#resultado_insertar_caratula").html(s.mensaje);
        },
        error: function(e){
            console.log(e.responseText);
        }
    });
});

$("#guardar_vobo_conta").on( "click", function(e) {
    e.preventDefault();

    var map = {};
    $(":input").each(function() {
        map[$(this).attr("name")] = $(this).val();
    });

    map.tipo_radio = $('input[name=tipo_radio]:checked').val();

    $.ajax({
        url: '/administrador/actualizar_vobo_contabilidad',
        method: 'POST',
        dataType: 'json',
        data: map,
        success: function(s){
            $("#resultado_insertar_caratula").html(s.mensaje);
        },
        error: function(e){
            console.log(e.responseText);
        }
    });
});
