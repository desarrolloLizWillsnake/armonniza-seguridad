var nivel1;
var nivel2;
var nivel3;
var nivel4;
var nivel5;
var nivel6;

var nivel_buscar1;
var nivel_buscar2;
var nivel_buscar3;
var nivel_buscar4;
var nivel_buscar5;
var nivel_buscar6;

var selected=[];
var nivel4;
var nivel5;
var partida;
var datos_tabla_transferencia;
var d = new Date();
var curr_date = d.getDate();
var curr_month = d.getMonth() + 1; //Months are zero based
var curr_year = d.getFullYear();

//Esta funcion es para traducir el datepicker
$(function($){
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);
});

//Se crean las cajas para la seleccion de fecha
$( "#fecha_solicitud" ).datepicker({ dateFormat: 'yy-mm-dd', minDate: '2015-01-01' });
$( "#fecha_aplicar_caratula" ).datepicker({ dateFormat: 'yy-mm-dd', minDate: '2015-01-01' });
$( "#fecha_aplicar_detalle" ).datepicker({ dateFormat: 'yy-mm-dd', minDate: '2015-01-01' });

$("#elegir_ultimoNivel_superior").click(function() {
    $("#input_nivel1_superior").val(nivel1);
    $("#input_nivel2_superior").val(nivel2);
    $("#input_nivel3_superior").val(nivel3);
    $("#input_nivel4_superior").val(nivel4);
    $("#input_nivel5_superior").val(nivel5);
    $("#input_nivel6_superior").val(nivel6);
});

/**
 * Inputs nivel superior
 */

$("#input_nivel1_superior").keyup(function() {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/egresos/obtenerDatosPartida",
        data: {
            nivel1: $("#input_nivel1_superior").val(),
            nivel2: $("#input_nivel2_superior").val(),
            nivel3: $("#input_nivel3_superior").val(),
            nivel4: $("#input_nivel4_superior").val(),
            nivel5: $("#input_nivel5_superior").val(),
            nivel6: $("#input_nivel6_superior").val()
        }
    })
        .done(function( data ) {
            ponerDatos(data);
        })
        .fail(function(e) {
            // If there is no communication between the server, show an error
            console.log("Error");
            $("#enero_superior").val("$"+$.number( 0, 2 ));
            $("#febrero_superior").val("$"+$.number( 0, 2 ));
            $("#marzo_superior").val("$"+$.number( 0, 2 ));
            $("#abril_superior").val("$"+$.number( 0, 2 ));
            $("#mayo_superior").val("$"+$.number( 0, 2 ));
            $("#junio_superior").val("$"+$.number( 0, 2 ));
            $("#julio_superior").val("$"+$.number( 0, 2 ));
            $("#agosto_superior").val("$"+$.number( 0, 2 ));
            $("#septiembre_superior").val("$"+$.number( 0, 2 ));
            $("#octubre_superior").val("$"+$.number( 0, 2 ));
            $("#noviembre_superior").val("$"+$.number( 0, 2 ));
            $("#diciembre_superior").val("$"+$.number( 0, 2 ));

            $("#total_superior").html("Total $"+$.number( 0, 2 ));
            //alert( "error occured" );
        });
});

$("#input_nivel2_superior").keyup(function() {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/egresos/obtenerDatosPartida",
        data: {
            nivel1: $("#input_nivel1_superior").val(),
            nivel2: $("#input_nivel2_superior").val(),
            nivel3: $("#input_nivel3_superior").val(),
            nivel4: $("#input_nivel4_superior").val(),
            nivel5: $("#input_nivel5_superior").val(),
            nivel6: $("#input_nivel6_superior").val()
        }
    })
        .done(function( data ) {
            ponerDatos(data);
        })
        .fail(function(e) {
            // If there is no communication between the server, show an error
            console.log("Error");
            $("#enero_superior").val("$"+$.number( 0, 2 ));
            $("#febrero_superior").val("$"+$.number( 0, 2 ));
            $("#marzo_superior").val("$"+$.number( 0, 2 ));
            $("#abril_superior").val("$"+$.number( 0, 2 ));
            $("#mayo_superior").val("$"+$.number( 0, 2 ));
            $("#junio_superior").val("$"+$.number( 0, 2 ));
            $("#julio_superior").val("$"+$.number( 0, 2 ));
            $("#agosto_superior").val("$"+$.number( 0, 2 ));
            $("#septiembre_superior").val("$"+$.number( 0, 2 ));
            $("#octubre_superior").val("$"+$.number( 0, 2 ));
            $("#noviembre_superior").val("$"+$.number( 0, 2 ));
            $("#diciembre_superior").val("$"+$.number( 0, 2 ));

            $("#total_superior").html("Total $"+$.number( 0, 2 ));
            //alert( "error occured" );
        });
});

$("#input_nivel3_superior").keyup(function() {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/egresos/obtenerDatosPartida",
        data: {
            nivel1: $("#input_nivel1_superior").val(),
            nivel2: $("#input_nivel2_superior").val(),
            nivel3: $("#input_nivel3_superior").val(),
            nivel4: $("#input_nivel4_superior").val(),
            nivel5: $("#input_nivel5_superior").val(),
            nivel6: $("#input_nivel6_superior").val()
        }
    })
        .done(function( data ) {
            ponerDatos(data);
        })
        .fail(function(e) {
            // If there is no communication between the server, show an error
            console.log("Error");
            $("#enero_superior").val("$"+$.number( 0, 2 ));
            $("#febrero_superior").val("$"+$.number( 0, 2 ));
            $("#marzo_superior").val("$"+$.number( 0, 2 ));
            $("#abril_superior").val("$"+$.number( 0, 2 ));
            $("#mayo_superior").val("$"+$.number( 0, 2 ));
            $("#junio_superior").val("$"+$.number( 0, 2 ));
            $("#julio_superior").val("$"+$.number( 0, 2 ));
            $("#agosto_superior").val("$"+$.number( 0, 2 ));
            $("#septiembre_superior").val("$"+$.number( 0, 2 ));
            $("#octubre_superior").val("$"+$.number( 0, 2 ));
            $("#noviembre_superior").val("$"+$.number( 0, 2 ));
            $("#diciembre_superior").val("$"+$.number( 0, 2 ));

            $("#total_superior").html("Total $"+$.number( 0, 2 ));
            //alert( "error occured" );
        });
});

$("#input_nivel4_superior").keyup(function() {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/egresos/obtenerDatosPartida",
        data: {
            nivel1: $("#input_nivel1_superior").val(),
            nivel2: $("#input_nivel2_superior").val(),
            nivel3: $("#input_nivel3_superior").val(),
            nivel4: $("#input_nivel4_superior").val(),
            nivel5: $("#input_nivel5_superior").val(),
            nivel6: $("#input_nivel6_superior").val()
        }
    })
        .done(function( data ) {
            ponerDatos(data);
        })
        .fail(function(e) {
            // If there is no communication between the server, show an error
            console.log("Error");
            $("#enero_superior").val("$"+$.number( 0, 2 ));
            $("#febrero_superior").val("$"+$.number( 0, 2 ));
            $("#marzo_superior").val("$"+$.number( 0, 2 ));
            $("#abril_superior").val("$"+$.number( 0, 2 ));
            $("#mayo_superior").val("$"+$.number( 0, 2 ));
            $("#junio_superior").val("$"+$.number( 0, 2 ));
            $("#julio_superior").val("$"+$.number( 0, 2 ));
            $("#agosto_superior").val("$"+$.number( 0, 2 ));
            $("#septiembre_superior").val("$"+$.number( 0, 2 ));
            $("#octubre_superior").val("$"+$.number( 0, 2 ));
            $("#noviembre_superior").val("$"+$.number( 0, 2 ));
            $("#diciembre_superior").val("$"+$.number( 0, 2 ));

            $("#total_superior").html("Total $"+$.number( 0, 2 ));
            //alert( "error occured" );
        });
});

$("#input_nivel5_superior").keyup(function() {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/egresos/obtenerDatosPartida",
        data: {
            nivel1: $("#input_nivel1_superior").val(),
            nivel2: $("#input_nivel2_superior").val(),
            nivel3: $("#input_nivel3_superior").val(),
            nivel4: $("#input_nivel4_superior").val(),
            nivel5: $("#input_nivel5_superior").val(),
            nivel6: $("#input_nivel6_superior").val()
        }
    })
        .done(function( data ) {
            ponerDatos(data);
        })
        .fail(function(e) {
            // If there is no communication between the server, show an error
            console.log("Error");
            $("#enero_superior").val("$"+$.number( 0, 2 ));
            $("#febrero_superior").val("$"+$.number( 0, 2 ));
            $("#marzo_superior").val("$"+$.number( 0, 2 ));
            $("#abril_superior").val("$"+$.number( 0, 2 ));
            $("#mayo_superior").val("$"+$.number( 0, 2 ));
            $("#junio_superior").val("$"+$.number( 0, 2 ));
            $("#julio_superior").val("$"+$.number( 0, 2 ));
            $("#agosto_superior").val("$"+$.number( 0, 2 ));
            $("#septiembre_superior").val("$"+$.number( 0, 2 ));
            $("#octubre_superior").val("$"+$.number( 0, 2 ));
            $("#noviembre_superior").val("$"+$.number( 0, 2 ));
            $("#diciembre_superior").val("$"+$.number( 0, 2 ));

            $("#total_superior").html("Total $"+$.number( 0, 2 ));
            //alert( "error occured" );
        });
});

$("#input_nivel6_superior").keyup(function() {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/egresos/obtenerDatosPartida",
        data: {
            nivel1: $("#input_nivel1_superior").val(),
            nivel2: $("#input_nivel2_superior").val(),
            nivel3: $("#input_nivel3_superior").val(),
            nivel4: $("#input_nivel4_superior").val(),
            nivel5: $("#input_nivel5_superior").val(),
            nivel6: $("#input_nivel6_superior").val()
        }
    })
        .done(function( data ) {
            ponerDatos(data);
        })
        .fail(function(e) {
            // If there is no communication between the server, show an error
            console.log("Error");
            $("#enero_superior").val("$"+$.number( 0, 2 ));
            $("#febrero_superior").val("$"+$.number( 0, 2 ));
            $("#marzo_superior").val("$"+$.number( 0, 2 ));
            $("#abril_superior").val("$"+$.number( 0, 2 ));
            $("#mayo_superior").val("$"+$.number( 0, 2 ));
            $("#junio_superior").val("$"+$.number( 0, 2 ));
            $("#julio_superior").val("$"+$.number( 0, 2 ));
            $("#agosto_superior").val("$"+$.number( 0, 2 ));
            $("#septiembre_superior").val("$"+$.number( 0, 2 ));
            $("#octubre_superior").val("$"+$.number( 0, 2 ));
            $("#noviembre_superior").val("$"+$.number( 0, 2 ));
            $("#diciembre_superior").val("$"+$.number( 0, 2 ));

            $("#total_superior").html("Total $"+$.number( 0, 2 ));
            //alert( "error occured" );
        });
});

/**
 * Modal de gasto nivel superior
 */

$("#forma_nivel1_superior").click(function(){
    $("#buscar_nivel3_superior").prop('disabled', true);
    $( "#select_nivel3_superior" ).html( '' );
    $("#buscar_nivel4_superior").prop('disabled', true);
    $( "#select_nivel4_superior" ).html( '' );
    $("#buscar_nivel5_superior").prop('disabled', true);
    $( "#select_nivel5_superior" ).html( '' );
    $("#buscar_nivel6_superior").prop('disabled', true);
    $( "#select_nivel6_superior" ).html( '' );

    $('#forma_nivel1_superior option:selected').each(function(){
        nivel1 = $(this).val();
        $.ajax( {
                url: "/egresos/obtener_nivel2",
                type: "POST",
                data: { nivel1: nivel1 },
                success:function(result) {
                    $("#input_nivel1_superior").val(nivel1);
                    $("#buscar_nivel2_superior").prop('disabled', false);
                    $( "#select_nivel2_superior" ).html( '' );
                    $( "#select_nivel2_superior" ).append( result );
                }
            }
        );
    });
});

$("#buscar_nivel1_superior").keyup(function(){
    nivel_buscar1 = $('#buscar_nivel1_superior').val();
    $.ajax( {
            url: "/egresos/buscar_nivel1",
            type: "POST",
            data: { nivel_buscar1: nivel_buscar1  },
            success:function(result) {
                $( "#select_nivel1_superior" ).html( '' );
                $( "#sfelect_nivel1_superior" ).append( result );
            }
        }
    );
});

$("#forma_nivel2_superior").click(function(){

    $("#buscar_nivel4_superior").prop('disabled', true);
    $( "#select_nivel4_superior" ).html( '' );
    $("#buscar_nivel5_superior").prop('disabled', true);
    $( "#select_nivel5_superior" ).html( '' );
    $("#buscar_nivel6_superior").prop('disabled', true);
    $( "#select_nivel6_superior" ).html( '' );

    $('#forma_nivel2_superior option:selected').each(function(){
        nivel2 = $(this).val();
        $.ajax( {
                url: "/egresos/obtener_nivel3",
                type: "POST",
                data: { nivel1: nivel1, nivel2: nivel2 },
                success: function(result) {
                    $("#input_nivel2_superior").val(nivel2);
                    $("#buscar_nivel3_superior").prop('disabled', false);
                    $( "#select_nivel3_superior" ).html( '' );
                    $( "#select_nivel3_superior" ).append( result );
                }
            }
        );
    });
});

$("#buscar_nivel2_superior").keyup(function(){
    nivel_buscar2 = $('#buscar_nivel2_superior').val();
    $.ajax( {
            url: "/egresos/buscar_nivel2",
            type: "POST",
            data: {
                nivel_buscar1: nivel1,
                nivel_buscar2: nivel_buscar2
            },
            success:function(result) {
                $( "#select_nivel2_superior" ).html( '' );
                $( "#select_nivel2_superior" ).append( result );
            }
        }
    );
});

$("#forma_nivel3_superior").click(function(){

    $("#buscar_nivel5_superior").prop('disabled', true);
    $( "#select_nivel5_superior" ).html( '' );
    $("#buscar_nivel6_superior").prop('disabled', true);
    $( "#select_nivel6_superior" ).html( '' );

    $('#forma_nivel3_superior option:selected').each(function(){
        nivel3 = $(this).val();
        $.ajax( {
                url: "/egresos/obtener_nivel4",
                type: "POST",
                data: {
                    nivel1: nivel1,
                    nivel2: nivel2,
                    nivel3: nivel3
                },
                success: function(result) {
                    $("#input_nivel3_superior").val(nivel3);
                    $("#buscar_nivel4_superior").prop('disabled', false);
                    $( "#select_nivel4_superior" ).html( '' );
                    $( "#select_nivel4_superior" ).append( result );
                }
            }
        );
    });
});

$("#buscar_nivel3_superior").keyup(function(){
    nivel_buscar3 = $('#buscar_nivel3_superior').val();
    $.ajax( {
            url: "/egresos/buscar_nivel3",
            type: "POST",
            data: {
                nivel_buscar1: nivel1,
                nivel_buscar2: nivel2,
                nivel_buscar3: nivel_buscar3
            },
            success:function(result) {
                $( "#select_nivel3_superior" ).html( '' );
                $( "#select_nivel3_superior" ).append( result );
            }
        }
    );
});

$("#forma_nivel4_superior").click(function(){
    $("#buscar_nivel6").prop('disabled', true);
    $( "#select_nivel6" ).html( '' );

    $('#forma_nivel4_superior option:selected').each(function(){
        nivel4 = $(this).val();
        $.ajax( {
                url: "/egresos/obtener_nivel5",
                type: "POST",
                data: {
                    nivel1: nivel1,
                    nivel2: nivel2,
                    nivel3: nivel3,
                    nivel4: nivel4
                },
                success: function(result) {
                    $("#input_nivel4_superior").val(nivel4);
                    $("#buscar_nivel5_superior").prop('disabled', false);
                    $( "#select_nivel5_superior" ).html( '' );
                    $( "#select_nivel5_superior" ).append( result );
                }
            }
        );
    });
});

$("#buscar_nivel4_superior").keyup(function(){
    nivel_buscar4 = $('#buscar_nivel4_superior').val();
    $.ajax( {
            url: "/egresos/buscar_nivel4",
            type: "POST",
            data: {
                nivel_buscar1: nivel1,
                nivel_buscar2: nivel2,
                nivel_buscar3: nivel3,
                nivel_buscar4: nivel_buscar4
            },
            success:function(result) {
                $( "#select_nivel4_superior" ).html( '' );
                $( "#select_nivel4_superior" ).append( result );
            }
        }
    );
});

$("#forma_nivel5_superior").click(function(){
    $('#forma_nivel5_superior option:selected').each(function(){
        nivel5 = $(this).val();
        $.ajax( {
                url: "/egresos/obtener_nivel6",
                type: "POST",
                data: {
                    nivel1: nivel1,
                    nivel2: nivel2,
                    nivel3: nivel3,
                    nivel4: nivel4,
                    nivel5: nivel5
                },
                success: function(result) {
                    $("#input_nivel5_superior").val(nivel5);
                    $("#buscar_nivel6_superior").prop('disabled', false);
                    $( "#select_nivel6_superior" ).html( '' );
                    $( "#select_nivel6_superior" ).append( result );
                }
            }
        );
    });
});

$("#buscar_nivel5_superior").keyup(function(){
    nivel_buscar5 = $('#buscar_nivel5_superior').val();
    $.ajax( {
            url: "/egresos/buscar_nivel5",
            type: "POST",
            data: {
                nivel_buscar1: nivel1,
                nivel_buscar2: nivel2,
                nivel_buscar3: nivel3,
                nivel_buscar4: nivel4,
                nivel_buscar5: nivel_buscar5
            },
            success:function(result) {
                $( "#select_nivel5_superior" ).html( '' );
                $( "#select_nivel5_superior" ).append( result );
            }
        }
    );
});

//Esta funcion es para obtener los datos del ultimo nivel
$("#forma_nivel6_superior").click(function(){
    $('#forma_nivel6_superior option:selected').each(function(){
        nivel6 = $(this).val();
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "/egresos/obtenerDatosPartida",
            data: {
                nivel1: nivel1,
                nivel2: nivel2,
                nivel3: nivel3,
                nivel4: nivel4,
                nivel5: nivel5,
                nivel6: nivel6
            }
        })
            .done(function( data ) {
                $("#elegir_ultimoNivel_superior").prop('disabled', false);
                ponerDatos(data);
            })
            .fail(function(e) {
                console.log(e.responseText);
                // If there is no communication between the server, show an error
                // alert( "error occured" );
            });
    });

});

$("#buscar_nivel6_superior").keyup(function(){
    nivel_buscar6 = $('#buscar_nivel6').val();
    $.ajax( {
            url: "/egresos/buscar_nivel6",
            type: "POST",
            data: {
                nivel_buscar1: nivel1,
                nivel_buscar2: nivel2,
                nivel_buscar3: nivel3,
                nivel_buscar4: nivel4,
                nivel_buscar5: nivel5,
                nivel_buscar6: nivel_buscar6
            },
            success:function(result) {
                $( "#select_nivel6_superior" ).html( '' );
                $( "#select_nivel6_superior" ).append( result );
            }
        }
    );
});

$(document).ready(function() {

    $('#tabla_datos_transferencia').dataTable({
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs": [ {
            "targets": [ 0 ],
            "visible": false,
            "searchable": false
        } ]
    });

    $("#fecha_solicitud").val(curr_year + "-" + curr_month + "-" + curr_date);
    $("#fecha_aplicar_detalle").val(curr_year + "-" + curr_month + "-" + curr_date);

});

$(".forma_transferencia input[name='presupuesto']").on( "change", function() {
    if($(this).val() == "abierto") {
        $("#fecha_entrega").val(curr_year + "-12-31");
        $("#fecha_entrega").prop('disabled', true);
    }
    else {
        $("#fecha_entrega").val("");
        $("#fecha_entrega").prop('disabled', false);
    }
});

$('#tabla_datos_transferencia tbody').on( 'click', 'tr', function () {
    datos_tabla_transferencia = $('#tabla_datos_transferencia').DataTable().row( this ).data();
    $("#borrar_transferencia_hidden").val(datos_tabla_transferencia[0]);
});

$(".borrar_transferencia_detalle").on( "click", function () {
    console.log($("#datos_tabla_transferencia").val());
});

$("#guardar_transferencia_detalle").on( "click", function() {

    var map = {};
    $(":input").each(function() {
        map[$(this).attr("name")] = $(this).val();
    });

    if($('#check_firme').is(':checked')){
        map.check_firme = 1;
    }
    else {
        map.check_firme = 0;
    }

    map.tipo_gasto = $('input[name=tipo_gasto]:checked').val();

    map.nivel1_superior = $('#input_nivel1_superior').val();
    map.nivel2_superior = $('#input_nivel2_superior').val();
    map.nivel3_superior = $('#input_nivel3_superior').val();
    map.nivel4_superior = $('#input_nivel4_superior').val();
    map.nivel5_superior = $('#input_nivel5_superior').val();
    map.nivel6_superior = $('#input_nivel6_superior').val();

    map.nivel1_inferior = $('#input_nivel1_inferior').val();
    map.nivel2_inferior = $('#input_nivel2_inferior').val();
    map.nivel3_inferior = $('#input_nivel3_inferior').val();
    map.nivel4_inferior = $('#input_nivel4_inferior').val();
    map.nivel5_inferior = $('#input_nivel5_inferior').val();
    map.nivel6_inferior = $('#input_nivel6_inferior').val();

    $(".forma_transferencia").validate({
        focusInvalid: true,
        rules: {
            clasificacion: {
                required: true
            },
            fecha_solicitud: {
                required: true,
                date: true
            },
            fecha_aplicar_caratula: {
                required: true,
                date: true
            },
            concepto: {
                required: true
            },
            aplicacion: {
                required: true
            },
            descripcion: {
                required: true
            },
            tipo_gasto: {
                required: true
            },
            mes_destino: {
                required: true
            },
            cantidad: {
                required: true
            },
            fecha_aplicar_detalle: {
                required: true,
                date: true
            }
        },
        messages: {
            clasificacion: {
                required: "* Este campo es obligatorio"
            },
            fecha_solicitud: {
                required: "* Este campo es obligatorio"
            },
            fecha_aplicar_caratula: {
                required: "* Este campo es obligatorio"
            },
            concepto: {
                required: "* Este campo es obligatorio"
            },
            aplicacion: {
                required: "* Este campo es obligatorio"
            },
            descripcion: {
                required: "* Este campo es obligatorio"
            },
            tipo_gasto: {
                required: "* Este campo es obligatorio"
            },
            mes_destino: {
                required: "* Este campo es obligatorio"
            },
            cantidad: {
                required: "* Este campo es obligatorio"
            },
            fecha_aplicar_detalle: {
                required: "* Este campo es obligatorio"
            }
        }
    }).form();

    if($(".forma_transferencia").valid()){

        $.ajax({
            url: '/egresos/insertar_transferencia_detalle',
            method: 'POST',
            dataType: 'json',
            data: map,
            success: function(s){
                $("#resutado_insertar_detalle").html(s.mensaje);
                $("#input_nivel1_superior").val('');
                $("#input_nivel2_superior").val('');
                $("#input_nivel3_superior").val('');
                $("#input_nivel4_superior").val('');
                $("#input_nivel5_superior").val('');
                $("#input_nivel6_superior").val('');
                $('#mes_destino').prop('selectedIndex',0);
                refrescarDetalle();
            },
            error: function(e){
                console.log(e.responseText);
            }
        });
    }

});

$("#guardar_transferencia").on( "click", function() {
    $(".forma_transferencia").validate({
        focusInvalid: true,
        rules: {
            clasificacion:{
                required: true
            },
            descripcion:{
                required: true
            },
            fecha_aplicar_caratula: {
                required: true,
                date: true
            },
            mes_destino: {
                required: false
            }
        },
        messages: {
            clasificacion: {
                required: "* Este campo es obligatorio"
            },
            descripcion: {
                required: "* Este campo es obligatorio"
            },
            fecha_aplicar_caratula: {
                required: "* Este campo es obligatorio",
                date: "El campo tiene que tener el siguiente formato AAAA-MM-DD"
            }
        }
    }).form();

    if($(".forma_transferencia").valid()) {
        var map = {};
        $(":input").each(function () {
            map[$(this).attr("name")] = $(this).val();
        });

        if ($('#check_firme').is(':checked')) {
            map.check_firme = 1;
        } else {
            map.check_firme = 0;
        }

        map.tipo_radio = $('input[name=tipo_gasto]:checked').val();

        $.ajax({
            url: '/egresos/insertar_transferencia',
            method: 'POST',
            dataType: 'json',
            data: map,
            success: function (s) {
                if(s.mensaje.indexOf("danger") > -1) {
                    $("#check_firme").prop( "checked", false );
                    $('#check_firme').prop('disabled', true);
                } else {
                    $('#check_firme').prop('disabled', false);
                }

                $("#resultado_verificado").html(s.mensaje);
            },
            error: function (e) {
                console.log(e.responseText);
            }
        });
    }
});

$("#borrar_cambios").on( "click", function() {
    var map = {};
    $(":input").each(function() {
        map[$(this).attr("name")] = $(this).val();
    });

    if($('#check_firme').is(':checked')){
        map.check_firme = 1;
    }

    map.tipo_radio = $('input[name=tipo_radio]:checked').val();

    $.ajax({
        url: '/egresos/borrar_cambios_transferencia',
        method: 'POST',
        dataType: 'json',
        data: map,
        success: function(s){
            if(s.mensaje == "ok") {
                location.href="/egresos/precompromiso";
            }
        },
        error: function(e){
            console.log(e.responseText);
        }
    });
});

$('.modal_borrar').on('show.bs.modal', function (event) {
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this);
    modal.find('#borrar_transferencia_hidden').val(datos_tabla_transferencia[0]);
});

$("#elegir_cancelar_transferencia").click(function() {
    var id_transferencia = $("#borrar_transferencia_hidden").val();

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/egresos/borrar_detalle_transferencia",
        data: {
            transferencia: id_transferencia
        }
    })
        .done(function( data ) {
            if(data.mensaje == "ok") {
                refrescarDetalle();
            }
            else {
                alert("Ha ocurrido un error");
            }
        })
        .fail(function(e) {
            console.log(e.responseText);
            // If there is no communication between the server, show an error
            console.log(e.responseText);
            // console.log("Error");
            //alert( "error occured" );
        });
});

function refrescarDetalle() {
    var total_ampliacion = 0;
    var total_reduccion = 0;
    var total = 0;

    $.ajax({
        url: '/egresos/tabla_detalle_transferencia',
        dataType: 'json',
        method: 'POST',
        data: {
            transferencia: $("#ultimo").val()
        },
        success: function(s){
            $.ajax({
                url: '/egresos/tomar_total_transferencia',
                dataType: 'json',
                method: 'POST',
                data: {
                    transferencia: $("#ultimo").val()
                },
                success: function(e){
                    total_ampliacion = e.total_ampliacion;
                    total_reduccion = e.total_reduccion;
                    total = e.total_final;
                    $('#tabla_datos_transferencia').dataTable().fnClearTable();
                    for(var i = 0; i < s.length; i++) {
                        $('#tabla_datos_transferencia').dataTable().fnAddData([
                            s[i][0],
                            s[i][1],
                            s[i][2],
                            s[i][3],
                            s[i][4],
                            s[i][5],
                            s[i][6],
                            "$"+$.number( s[i][7], 2 ),
                            s[i][8],
                            s[i][9],
                            s[i][10],
                            s[i][11]
                        ]);
                    } // End For
                    $("#total_hidden").val(total);

                    $("#suma_total").html('<span style="margin-right: 8%;">Ampliación <span style="color:#848484;"> $'+$.number( total_ampliacion, 2 )+'</span></span><span style="margin-right: 8%;">Reducción <span style="color:#848484;"> $'+$.number( total_reduccion, 2 )+'</span></span>');

                    if(total_ampliacion.toFixed(2) != total_reduccion.toFixed(2)) {
                        $("#check_firme").prop("disabled", true);
                    } else if(total_ampliacion.toFixed(2) == total_reduccion.toFixed(2)) {
                        $("#check_firme").prop("disabled", false);
                    }
                },
                error: function(e){
                    console.log(e.responseText);
                }
            });
        },
        error: function(e){
            console.log(e.responseText);
        }
    });
}

$("#fecha_aplicar_caratula").on( "change", function () {
    var fecha = $("#fecha_aplicar_caratula").val();
    $("#fecha_aplicar_detalle").val(fecha);
});

function ponerDatos(data) {
    console.log(data);
    $("#enero_inicial").val("$"+$.number( data.enero_inicial, 2 ));
    $("#enero_superior").val("$"+$.number( data.enero_saldo, 2 ));
    $("#enero_modificado").val("$"+$.number( data.enero_modificado, 2 ));
    $("#enero_superior_precompromiso").val("$"+$.number( data.enero_precompromiso, 2 ));
    $("#enero_superior_compromiso").val("$"+$.number( data.enero_compromiso, 2 ));
    $("#enero_superior_devengado").val("$"+$.number( data.enero_devengado, 2 ));
    $("#enero_superior_recaudado").val("$"+$.number( data.enero_ejercido, 2 ));
    $("#enero_superior_ejercido").val("$"+$.number( data.enero_pagado, 2 ));

    $("#febrero_inicial").val("$"+$.number( data.febrero_inicial, 2 ));
    $("#febrero_superior").val("$"+$.number( data.febrero_saldo, 2 ));
    $("#febrero_modificado").val("$"+$.number( data.febrero_modificado, 2 ));
    $("#febrero_superior_precompromiso").val("$"+$.number( data.febrero_precompromiso, 2 ));
    $("#febrero_superior_compromiso").val("$"+$.number( data.febrero_compromiso, 2 ));
    $("#febrero_superior_devengado").val("$"+$.number( data.febrero_devengado, 2 ));
    $("#febrero_superior_recaudado").val("$"+$.number( data.febrero_ejercido, 2 ));
    $("#febrero_superior_ejercido").val("$"+$.number( data.febrero_pagado, 2 ));

    $("#marzo_inicial").val("$"+$.number( data.marzo_inicial, 2 ));
    $("#marzo_superior").val("$"+$.number( data.marzo_saldo, 2 ));
    $("#marzo_modificado").val("$"+$.number( data.marzo_modificado, 2 ));
    $("#marzo_superior_precompromiso").val("$"+$.number( data.marzo_precompromiso, 2 ));
    $("#marzo_superior_compromiso").val("$"+$.number( data.marzo_compromiso, 2 ));
    $("#marzo_superior_devengado").val("$"+$.number( data.marzo_devengado, 2 ));
    $("#marzo_superior_recaudado").val("$"+$.number( data.marzo_ejercido, 2 ));
    $("#marzo_superior_ejercido").val("$"+$.number( data.marzo_pagado, 2 ));

    $("#abril_inicial").val("$"+$.number( data.abril_inicial, 2 ));
    $("#abril_superior").val("$"+$.number( data.abril_saldo, 2 ));
    $("#abril_modificado").val("$"+$.number( data.abril_modificado, 2 ));
    $("#abril_superior_precompromiso").val("$"+$.number( data.abril_precompromiso, 2 ));
    $("#abril_superior_compromiso").val("$"+$.number( data.abril_compromiso, 2 ));
    $("#abril_superior_devengado").val("$"+$.number( data.abril_devengado, 2 ));
    $("#abril_superior_recaudado").val("$"+$.number( data.abril_ejercido, 2 ));
    $("#abril_superior_ejercido").val("$"+$.number( data.abril_pagado, 2 ));

    $("#mayo_inicial").val("$"+$.number( data.mayo_inicial, 2 ));
    $("#mayo_superior").val("$"+$.number( data.mayo_saldo, 2 ));
    $("#mayo_modificado").val("$"+$.number( data.mayo_modificado, 2 ));
    $("#mayo_superior_precompromiso").val("$"+$.number( data.mayo_precompromiso, 2 ));
    $("#mayo_superior_compromiso").val("$"+$.number( data.mayo_compromiso, 2 ));
    $("#mayo_superior_devengado").val("$"+$.number( data.mayo_devengado, 2 ));
    $("#mayo_superior_recaudado").val("$"+$.number( data.mayo_ejercido, 2 ));
    $("#mayo_superior_ejercido").val("$"+$.number( data.mayo_pagado, 2 ));

    $("#junio_inicial").val("$"+$.number( data.junio_inicial, 2 ));
    $("#junio_superior").val("$"+$.number( data.junio_saldo, 2 ));
    $("#junio_modificado").val("$"+$.number( data.junio_modificado, 2 ));
    $("#junio_superior_precompromiso").val("$"+$.number( data.junio_precompromiso, 2 ));
    $("#junio_superior_compromiso").val("$"+$.number( data.junio_compromiso, 2 ));
    $("#junio_superior_devengado").val("$"+$.number( data.junio_devengado, 2 ));
    $("#junio_superior_recaudado").val("$"+$.number( data.junio_ejercido, 2 ));
    $("#junio_superior_ejercido").val("$"+$.number( data.junio_pagado, 2 ));

    $("#julio_inicial").val("$"+$.number( data.julio_inicial, 2 ));
    $("#julio_superior").val("$"+$.number( data.julio_saldo, 2 ));
    $("#julio_modificado").val("$"+$.number( data.julio_modificado, 2 ));
    $("#julio_superior_precompromiso").val("$"+$.number( data.julio_precompromiso, 2 ));
    $("#julio_superior_compromiso").val("$"+$.number( data.julio_compromiso, 2 ));
    $("#julio_superior_devengado").val("$"+$.number( data.julio_devengado, 2 ));
    $("#julio_superior_recaudado").val("$"+$.number( data.julio_ejercido, 2 ));
    $("#julio_superior_ejercido").val("$"+$.number( data.julio_pagado, 2 ));

    $("#agosto_inicial").val("$"+$.number( data.agosto_inicial, 2 ));
    $("#agosto_superior").val("$"+$.number( data.agosto_saldo, 2 ));
    $("#agosto_modificado").val("$"+$.number( data.agosto_modificado, 2 ));
    $("#agosto_superior_precompromiso").val("$"+$.number( data.agosto_precompromiso, 2 ));
    $("#agosto_superior_compromiso").val("$"+$.number( data.agosto_compromiso, 2 ));
    $("#agosto_superior_devengado").val("$"+$.number( data.agosto_devengado, 2 ));
    $("#agosto_superior_recaudado").val("$"+$.number( data.agosto_ejercido, 2 ));
    $("#agosto_superior_ejercido").val("$"+$.number( data.agosto_pagado, 2 ));

    $("#septiembre_inicial").val("$"+$.number( data.septiembre_inicial, 2 ));
    $("#septiembre_superior").val("$"+$.number( data.septiembre_saldo, 2 ));
    $("#septiembre_modificado").val("$"+$.number( data.septiembre_modificado, 2 ));
    $("#septiembre_superior_precompromiso").val("$"+$.number( data.septiembre_precompromiso, 2 ));
    $("#septiembre_superior_compromiso").val("$"+$.number( data.septiembre_compromiso, 2 ));
    $("#septiembre_superior_devengado").val("$"+$.number( data.septiembre_devengado, 2 ));
    $("#septiembre_superior_recaudado").val("$"+$.number( data.septiembre_ejercido, 2 ));
    $("#septiembre_superior_ejercido").val("$"+$.number( data.septiembre_pagado, 2 ));

    $("#octubre_inicial").val("$"+$.number( data.octubre_inicial, 2 ));
    $("#octubre_superior").val("$"+$.number( data.octubre_saldo, 2 ));
    $("#octubre_modificado").val("$"+$.number( data.octubre_modificado, 2 ));
    $("#octubre_superior_precompromiso").val("$"+$.number( data.octubre_precompromiso, 2 ));
    $("#octubre_superior_compromiso").val("$"+$.number( data.octubre_compromiso, 2 ));
    $("#octubre_superior_devengado").val("$"+$.number( data.octubre_devengado, 2 ));
    $("#octubre_superior_recaudado").val("$"+$.number( data.octubre_ejercido, 2 ));
    $("#octubre_superior_ejercido").val("$"+$.number( data.octubre_pagado, 2 ));

    $("#noviembre_inicial").val("$"+$.number( data.noviembre_inicial, 2 ));
    $("#noviembre_superior").val("$"+$.number( data.noviembre_saldo, 2 ));
    $("#noviembre_modificado").val("$"+$.number( data.noviembre_modificado, 2 ));
    $("#noviembre_superior_precompromiso").val("$"+$.number( data.noviembre_precompromiso, 2 ));
    $("#noviembre_superior_compromiso").val("$"+$.number( data.noviembre_compromiso, 2 ));
    $("#noviembre_superior_devengado").val("$"+$.number( data.noviembre_devengado, 2 ));
    $("#noviembre_superior_recaudado").val("$"+$.number( data.noviembre_ejercido, 2 ));
    $("#noviembre_superior_ejercido").val("$"+$.number( data.noviembre_pagado, 2 ));

    $("#diciembre_inicial").val("$"+$.number( data.diciembre_inicial, 2 ));
    $("#diciembre_superior").val("$"+$.number( data.diciembre_saldo, 2 ));
    $("#diciembre_modificado").val("$"+$.number( data.diciembre_modificado, 2 ));
    $("#diciembre_superior_precompromiso").val("$"+$.number( data.diciembre_precompromiso, 2 ));
    $("#diciembre_superior_compromiso").val("$"+$.number( data.diciembre_compromiso, 2 ));
    $("#diciembre_superior_devengado").val("$"+$.number( data.diciembre_devengado, 2 ));
    $("#diciembre_superior_recaudado").val("$"+$.number( data.diciembre_ejercido, 2 ));
    $("#diciembre_superior_ejercido").val("$"+$.number( data.diciembre_pagado, 2 ));

    suma_total_inicial = parseFloat(data.enero_inicial) + parseFloat(data.febrero_inicial) + parseFloat(data.marzo_inicial) + parseFloat(data.abril_inicial) + parseFloat(data.mayo_inicial) + parseFloat(data.junio_inicial) + parseFloat(data.julio_inicial) + parseFloat(data.agosto_inicial) + parseFloat(data.septiembre_inicial) + parseFloat(data.octubre_inicial) + parseFloat(data.noviembre_inicial) + parseFloat(data.diciembre_inicial);
    suma_total = parseFloat(data.enero_saldo) + parseFloat(data.febrero_saldo) + parseFloat(data.marzo_saldo) + parseFloat(data.abril_saldo) + parseFloat(data.mayo_saldo) + parseFloat(data.junio_saldo) + parseFloat(data.julio_saldo) + parseFloat(data.agosto_saldo) + parseFloat(data.septiembre_saldo) + parseFloat(data.octubre_saldo) + parseFloat(data.noviembre_saldo) + parseFloat(data.diciembre_saldo);
    suma_total_modificado = parseFloat(data.enero_modificado) + parseFloat(data.febrero_modificado) + parseFloat(data.marzo_modificado) + parseFloat(data.abril_modificado) + parseFloat(data.mayo_modificado) + parseFloat(data.junio_modificado) + parseFloat(data.julio_modificado) + parseFloat(data.agosto_modificado) + parseFloat(data.septiembre_modificado) + parseFloat(data.octubre_modificado) + parseFloat(data.noviembre_modificado) + parseFloat(data.diciembre_modificado);
    suma_total_precomprometido = parseFloat(data.enero_precompromiso) + parseFloat(data.febrero_precompromiso) + parseFloat(data.marzo_precompromiso) + parseFloat(data.abril_precompromiso) + parseFloat(data.mayo_precompromiso) + parseFloat(data.junio_precompromiso) + parseFloat(data.julio_precompromiso) + parseFloat(data.agosto_precompromiso) + parseFloat(data.septiembre_precompromiso) + parseFloat(data.octubre_precompromiso) + parseFloat(data.noviembre_precompromiso) + parseFloat(data.diciembre_precompromiso);
    suma_total_comprometido = parseFloat(data.enero_compromiso) + parseFloat(data.febrero_compromiso) + parseFloat(data.marzo_compromiso) + parseFloat(data.abril_compromiso) + parseFloat(data.mayo_compromiso) + parseFloat(data.junio_compromiso) + parseFloat(data.julio_compromiso) + parseFloat(data.agosto_compromiso) + parseFloat(data.septiembre_compromiso) + parseFloat(data.octubre_compromiso) + parseFloat(data.noviembre_compromiso) + parseFloat(data.diciembre_compromiso);
    suma_total_devengado = parseFloat(data.enero_devengado) + parseFloat(data.febrero_devengado) + parseFloat(data.marzo_devengado) + parseFloat(data.abril_devengado) + parseFloat(data.mayo_devengado) + parseFloat(data.junio_devengado) + parseFloat(data.julio_devengado) + parseFloat(data.agosto_devengado) + parseFloat(data.septiembre_devengado) + parseFloat(data.octubre_devengado) + parseFloat(data.noviembre_devengado) + parseFloat(data.diciembre_devengado);
    suma_total_recaudado = parseFloat(data.enero_ejercido) + parseFloat(data.febrero_ejercido) + parseFloat(data.marzo_ejercido) + parseFloat(data.abril_ejercido) + parseFloat(data.mayo_ejercido) + parseFloat(data.junio_ejercido) + parseFloat(data.julio_ejercido) + parseFloat(data.agosto_ejercido) + parseFloat(data.septiembre_ejercido) + parseFloat(data.octubre_ejercido) + parseFloat(data.noviembre_ejercido) + parseFloat(data.diciembre_ejercido);
    suma_total_pagado = parseFloat(data.enero_pagado) + parseFloat(data.febrero_pagado) + parseFloat(data.marzo_pagado) + parseFloat(data.abril_pagado) + parseFloat(data.mayo_pagado) + parseFloat(data.junio_pagado) + parseFloat(data.julio_pagado) + parseFloat(data.agosto_pagado) + parseFloat(data.septiembre_pagado) + parseFloat(data.octubre_pagado) + parseFloat(data.noviembre_pagado) + parseFloat(data.diciembre_pagado);

    $("#total_superior").html(
    "<hr/><div class='row'>"
    +"<div class='col-lg-3'><b>Inicial Anual</b>" + "<p class='color-gray'> $" + $.number( suma_total_inicial, 2 ) + "</p>" + "</div>"
    +"<div class='col-lg-3'><b>Modificado Anual</b>"+ "<p class='color-gray'> $" + $.number( suma_total_modificado, 2 )+ "</p>" + "</div>"
    +"<div class='col-lg-3'><b>Precomprometido Anual</b>"+ "<p class='color-gray'> $" + $.number( suma_total_precomprometido, 2 )+ "</p>" + "</div>"
    +"<div class='col-lg-3'><b>Comprometido Anual</b>"+ "<p class='color-gray'> $" + $.number( suma_total_comprometido, 2 )+ "</p>" + "</div></div> <hr/>"
    +"<i class='fa fa-calendar-o fa-3x' style='color:#B6CE33; margin-right: 2%;'></i>"
    +"<hr/><div class='row'>"
    +"<div class='col-lg-3'><b>Devengado Anual</b>"+ "<p class='color-gray'> $" + $.number( suma_total_devengado, 2 )+ "</p>" + "</div> "
    +"<div class='col-lg-3'><b>Ejercido Anual</b>"+ "<p class='color-gray'> $" + $.number( suma_total_recaudado, 2 ) + "</p>" + "</div>"
    +"<div class='col-lg-3'><b>Pagado Anual</b>"+ "<p class='color-gray'> $" + $.number( suma_total_pagado, 2 )+ "</p>" + "</div> "
    +"<div class='col-lg-3'><b>Disponible Anual</b>"+ "<p class='color-gray'> $" + $.number( suma_total, 2 )+ "</p>" + "</div></div>");


}