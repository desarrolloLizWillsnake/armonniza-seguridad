var editor;

$(document).ready(function() {
    editor = new $.fn.dataTable.Editor( {
        ajax: "/catalogos/tabla_bancos",
        table: ".datos_tabla",
        fields: [ {
            label: "Cuenta",
            name: "cuenta"
        }, {
            label: "Banco",
            name: "banco"
        }, {
            label: "Cuenta Contable",
            name: "cta_contable"
        }, {
            label: "Saldo",
            name: "saldo"
        }, {
            label: "Sucursal",
            name: "sucursal"
        }, {
            label: "No. Sucursal",
            name: "num_sucursal"
        }, {
            label: "Fecha Apertura",
            name: "fecha_apertura"
        }, {
            label: "Saldo Mínimo",
            name: "minimo"
        }, {
            label: "Nombre",
            name: "nombre"
        }
        ],
        i18n: {
            create: {
                button: "<i class='fa fa-plus-circle circle ic-catalogo'></i> Nuevo",
                title:  "<i class='fa fa-plus-circle ic-catalogo-emergente'></i> Nueva Cuenta Bancaria",
                submit: "Ingresar"
            },
            edit: {
                button: "<i class='fa fa-edit circle ic-catalogo'></i> Editar",
                title:  "<i class='fa fa-edit ic-catalogo-emergente'></i> Editar Cuenta Bancaria",
                submit: "Actualizar"
            },
            remove: {
                button: "<i class='fa fa-trash-o circle ic-catalogo'></i> Borrar",
                title:  "<i class='fa fa-trash-o ic-catalogo-emergente'></i> Borrar",
                submit: "Borrar",
                confirm: {
                    _: "¿Esta seguro que desea eliminar las %d Cuentas Bancarias seleccionadas?",
                    1: "¿Esta seguro que desea eliminar la Cuenta Bancaria seleccionada?"
                }
            },
            error: {
                system: "Ha ocurrido un error, por favor, contacte al administrador del sistema."
            }
        }
    } );

    $('.datos_tabla').DataTable( {
        dom: "Tfrtip",
        ajax: {
            url: "/catalogos/tabla_bancos",
            type: "POST"
        },
        serverSide: true,
        columns: [
            { data: "id_cuentas_bancarias" },
            { data: "cuenta" },
            { data: "banco" },
            { data: "cta_contable" },
            { data: "saldo" },
        ],
        tableTools: {
            sRowSelect: "os",
            aButtons: [
                { sExtends: "editor_create", editor: editor },
                { sExtends: "editor_edit",   editor: editor },
                { sExtends: "editor_remove", editor: editor }
            ]
        },
        language: {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    } );
} );