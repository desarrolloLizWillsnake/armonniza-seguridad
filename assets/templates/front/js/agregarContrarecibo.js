var datos_proveedor;
var datos_tabla_contrarecibo;
var datos_tabla_compromiso;
var d = new Date();
var curr_date = d.getDate();
var curr_month = d.getMonth() + 1; //Months are zero based
var curr_year = d.getFullYear();

//Esta funcion es para traducir el datepicker
$(function($){
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);
});

//Se crean las cajas para la seleccion de fecha
$( "#fecha_solicitud" ).datepicker({ dateFormat: 'yy-mm-dd', minDate: '2015-01-01' });
$( "#fecha_p_pago" ).datepicker({ dateFormat: 'yy-mm-dd', minDate: '2015-01-01' });

$('#tabla_datos_contrarecibo tbody').on( 'click', 'tr', function () {
    datos_tabla_contrarecibo = $('#tabla_datos_contrarecibo').DataTable().row( this ).data();
    $("#borrar_contrarecibo_hidden").val(datos_tabla_contrarecibo[0]);
});

$('#tabla_compromiso tbody').on( 'click', 'tr', function () {
    datos_tabla_compromiso = $('#tabla_compromiso').DataTable().row( this ).data();
});

$('#tabla_proveedores tbody').on( 'click', 'tr', function () {
    datos_proveedor = $('#tabla_proveedores').DataTable().row( this ).data();
});

$('#modal_proveedores').on('hidden.bs.modal', function () {
    $('#proveedor').val("");
    $('#proveedor').val(datos_proveedor[2]);
});

$('#modal_compromiso').on('hidden.bs.modal', function () {
    $.ajax({
        url: '/ciclo/tomar_datos_Compromiso',
        dataType: 'json',
        method: 'POST',
        data: {
            compromiso: datos_tabla_compromiso[0],
            contrarecibo: $("#ultimo_contrarecibo").val()
        },
        success: function(s){
            if(s.mensaje == "ok") {

                if(s.tipo_compromiso == "Fondo Revolvente" || s.tipo_compromiso == "Gastos a Comprobar" || s.tipo_compromiso == "Viáticos Nacionales" || s.tipo_compromiso == "Viáticos Internacionales") {
                    $(".forma_normal").hide();
                    $(".forma_diferente").show();
                }

                if(s.tipo_impresion) {
                    $("#hidden_tipo_impresion").val(s.tipo_impresion);
                } else {
                    $("#hidden_tipo_impresion").val(s.tipo_compromiso);
                }

                $("#no_compromiso").val(s.numero_compromiso);
                $("#hidden_clave_proveedor").val(s.id_proveedor);
                $("#concepto_especifico").val(s.concepto_especifico);
                $("#concepto").val(s.descripcion_general);
                $("#alterno").val(s.alterno);
                $("#proveedor").val(s.nombre_proveedor);
                $("#destino").val(s.tipo_compromiso);
                $("#hidden_id_persona").val(s.id_persona);
                $("#nombre_completo").val(s.nombre_completo);
                refrescarDetalle();
            }
        },
        error: function(e){
            console.log(e.responseText);
        }
    });

});

$(".borrar_contrarecibo_detalle").on( "click", function () {
    console.log($("#datos_tabla_compromiso").val());
});

$("#guardar_contrarecibo").on( "click", function(e) {
    $(".forma_contrarecibo").validate({
        focusInvalid: true,
        rules: {
            no_compromiso:{
                required: true
            },
            destino:{
                required: true
            },
            concepto_especifico: {
                required: true
            },
            concepto: {
                required: true
            },
            proveedor: {
                required: true
            },
            tipo_documento: {
                required: true
            },
            documento: {
                required: true
            },
            descripcion_general: {
                required: true
            },
            fecha_solicitud: {
                required: true,
                date: true
            },
            fecha_p_pago: {
                required: true,
                date: true
            },
            documentacion_anexa: {
                required: false
            }
        },
        messages: {
            no_compromiso: {
                required: "* Este campo es obligatorio"
            },
            destino: {
                required: "* Este campo es obligatorio"
            },
            concepto_especifico: {
                required: "* Este campo es obligatorio"
            },
            concepto: {
                required: "* Este campo es obligatorio"
            },
            proveedor: {
                required: "* Este campo es obligatorio"
            },
            tipo_documento: {
                required: "* Este campo es obligatorio"
            },
            documento: {
                required: "* Este campo es obligatorio"
            },
            descripcion_general: {
                required: "* Este campo es obligatorio"
            },
            fecha_solicitud: {
                required: "* Este campo es obligatorio",
                date: "El campo tiene que tener el siguiente formato AAAA-MM-DD"
            },
            fecha_p_pago: {
                required: "* Este campo es obligatorio",
                date: "El campo tiene que tener el siguiente formato AAAA-MM-DD"
            }
        }
    }).form();

    if($(".forma_contrarecibo").valid()) {
        e.preventDefault();

        var firme = 0;

        if ($('#check_firme').is(':checked')) {
            firme = 1;
        }

        $.ajax({
            url: '/ciclo/insertar_contrarecibo',
            method: 'POST',
            dataType: 'json',
            data: {
                contrarecibo: $("#ultimo_contrarecibo").val(),
                num_compromiso: $("#no_compromiso").val(),
                fecha_solicita: $("#fecha_solicitud").val(),
                fecha_pago: $("#fecha_p_pago").val(),
                concepto_especifico: $("#concepto_especifico").val(),
                concepto: $("#concepto").val(),
                documento: $("#documento").val(),
                tipo_documento: $("#tipo_documento").val(),
                clave_proveedor: $("#hidden_clave_proveedor").val(),
                proveedor: $("#proveedor").val(),
                tipo: $("#destino").val(),
                importe: $("#total_hidden").val(),
                descripcion: $("#descripcion_general").val(),
                documentacion_anexa: $("#documentacion_anexa").val(),
                id_persona: $("#hidden_id_persona").val(),
                nombre_completo: $("#nombre_completo").val(),
                tipo_impresion: $("#hidden_tipo_impresion").val(),
                firme: firme
            },
            success: function (s) {
                $("#resultado_insertar_caratula").html(s.mensaje_insertar);
                $("#guardar_contrarecibo").prop('disabled', true);
            },
            error: function (e) {
                console.log(e.responseText);
            }
        });
    }
});

$("#borrar_cambios").on( "click", function() {
    var firme = 0;

    $.ajax({
        url: '/ciclo/borrar_cambios_contrarecibo',
        method: 'POST',
        dataType: 'json',
        data: {
            contrarecibo: $("#ultimo_contrarecibo").val(),
            num_compromiso: $("#no_compromiso").val(),
            fecha_solicita: $("#fecha_solicitud").val(),
            fecha_pago: $("#fecha_p_pago").val(),
            clave_proveedor: $("#hidden_clave_proveedor").val(),
            concepto: $("#concepto").val(),
            documento: $("#documento").val(),
            tipo_documento: $("#tipo_documento").val(),
            alterno: $("#alterno").val(),
            proveedor: $("#proveedor").val(),
            tipo: $("#destino").val(),
            importe: $("#suma_total").val(),
            descripcion: $("#descripcion_general").val(),
            firme: firme
        },
        success: function(s){
            if(s.mensaje == "ok") {
                location.href="/ciclo/contrarecibos";
            }
        },
        error: function(e){
            console.log(e.responseText);
        }
    });
});

$('.modal_borrar').on('show.bs.modal', function (event) {
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this);
    modal.find('#borrar_contrarecibo_hidden').val(datos_tabla_contrarecibo[0]);
});

$("#elegir_cancelar_contrarecibo").click(function() {
    var id_contrarecibo = $("#borrar_contrarecibo_hidden").val();

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/ciclo/borrar_detalle_contrarecibo",
        data: {
            id_contrarecibo: id_contrarecibo
        }
    })
        .done(function( data ) {
            console.log(data);
            if(data.mensaje == "ok") {
                $("#no_compromiso").val('');
                $("#hidden_clave_proveedor").val('');
                $("#concepto").val('');
                $("#alterno").val('');
                $("#proveedor").val('');
                $("#destino").val('');
                $("#descripcion_general").val('');
                refrescarDetalle();
            }
            else {
                alert("Ha ocurrido un error");
            }
        })
        .fail(function(e) {
            console.log(e.responseText);
            // If there is no communication between the server, show an error
            console.log("Error");
            //alert( "error occured" );
        });
});

$(document).ready(function() {
    $("#fecha_solicitud").val(curr_year + "-" + curr_month + "-" + curr_date);

    $('#tabla_datos_contrarecibo').dataTable({
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs": [ {
            "targets": [ 0, -1 ],
            "visible": false,
            "searchable": false
        } ]
    });

    $('#tabla_compromiso').dataTable({
        "ajax": "tabla_compromiso_contrarecibo_AJAX",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });

    $('#tabla_proveedores').dataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "tabla_proveedores_AJAX",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs": [ {
            "targets":  0 ,
            "visible": false,
            "searchable": false
        }, {
            "targets": -1,
            "data": null,
            "defaultContent":
                '<a data-dismiss="modal"><i class="fa fa-check"></i></a>'
        } ]
    });

});

function refrescarDetalle() {
    $.ajax({
        url: '/ciclo/tabla_detalle_contrarecibo_AJAX',
        dataType: 'json',
        method: 'POST',
        data: {
            contrarecibo: $("#ultimo_contrarecibo").val()
        },
        success: function(s){
            $('#tabla_datos_contrarecibo').dataTable().fnClearTable();
            var subtotal = 0;
            var iva = 0;
            var total = 0;
            for(var i = 0; i < s.length; i++) {
                subtotal += parseFloat(s[i][3]);
                iva += parseFloat(s[i][4]);
                total += parseFloat(s[i][5]);
                $('#tabla_datos_contrarecibo').dataTable().fnAddData([
                    s[i][0],
                    s[i][1],
                    s[i][2],
                    "$"+$.number( s[i][3], 2 ),
                    "$"+$.number( s[i][4], 2 ),
                    "$"+$.number( s[i][5], 2 ),
                    s[i][6],
                    s[i][7]
                ]);
            } // End For

            var destino = $("#destino").val();

            if(destino == "Honorarios") {
                var total_pagar = total;
                var isr = subtotal * .10;
                var iva_retenido = (iva / 3) * 2;
                total_pagar -= isr;
                total_pagar -= iva_retenido;
                $("#desglose_honorarios").html(
                    '<div class="col-lg-12"> ' +
                    '<div class="panel panel-default"> ' +
                    '<div class="panel-heading">' +
                    'Desglose de Honorarios' +
                    '</div> ' +
                    '<div class="panel-body"> ' +
                    '<div class="row"> ' +
                    '<div class="col-lg-3"></div>'+
                    '<div class="col-lg-3"> ' +
                    '<h5>Monto de los honorarios</h5> ' +
                    '<h5>IVA 16%</h5> ' +
                    '<hr>'+
                    '<h5><b>Subtotal</b></h5> ' +
                    '<hr>'+
                    '<h5>ISR 10%</h5> ' +
                    '<h5>IVA Retenido</h5> ' +
                    '<hr>'+
                    '<h4 style="background:#f8f8f8; padding: 4%;">Total a pagar</h4> ' +
                    '</div> ' +
                    '<div class="col-lg-3"> ' +
                    '<h5>$'+$.number( subtotal, 2 )+'</h5> ' +
                    '<h5>$'+$.number( iva, 2 )+'</h5> ' +
                    '<hr>'+
                    '<h5><b>$'+$.number( total, 2 )+'</b></h5> ' +
                    '<hr>'+
                    '<h5>$'+$.number( isr, 2 )+'</h5> ' +
                    '<h5>$'+$.number( iva_retenido, 2 )+'</h5> ' +
                    '<hr>'+
                    '<h4  style="background:#f8f8f8; padding: 4%;">$'+$.number( total_pagar , 2 )+'</h4> ' +
                    '</div> ' +
                    '<div class="col-lg-3"></div>'+
                    '</div> ' +
                    '</div> ' +
                    '</div>');
            }

            $("#suma_total").html("Total"+ "<span style='color:#848484;'>" + " $ " +$.number( total, 2 )+ "</span>");

            $("#total_hidden").val(total);
        },
        error: function(e){
            console.log(e.responseText);
        }
    });
}