var datos_tabla_bancos_concepto;
var datos_tabla_cuenta_cargo;
var datos_tabla_cuenta_abono;
var datos_tabla_contrarecibo;
var datos_tabla_detalle;
var datos_proveedor;
var d = new Date();
var curr_date = d.getDate();
var curr_month = d.getMonth() + 1; //Months are zero based
var curr_year = d.getFullYear();

//Esta funcion es para traducir el datepicker
$(function($){
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);
});

//Se crean las cajas para la seleccion de fecha
$( "#fecha_emision" ).datepicker({ dateFormat: 'yy-mm-dd', minDate: '2015-01-01' });
$( "#fecha_pago" ).datepicker({ dateFormat: 'yy-mm-dd', minDate: '2015-01-01' });
$( "#fecha_inicial" ).datepicker({ dateFormat: 'yy-mm-dd', minDate: '2015-01-01' });
$( "#fecha_final" ).datepicker({ dateFormat: 'yy-mm-dd', minDate: '2015-01-01' });

$('#tabla_bancos_concepto tbody').on( 'click', 'tr', function () {
    datos_tabla_bancos_concepto = $('#tabla_bancos_concepto').DataTable().row( this ).data();
});

$('#modal_bancos_concepto').on('hidden.bs.modal', function () {
    $('#hidden_clave_concepto').val(datos_tabla_bancos_concepto[0]);
    $('#concepto').val(datos_tabla_bancos_concepto[1]);
});

$('#tabla_proveedores tbody').on( 'click', 'tr', function () {
    datos_proveedor = $('#tabla_proveedores').DataTable().row( this ).data();
});

$('#modal_proveedores').on('hidden.bs.modal', function () {
    $('#proveedor').val(datos_proveedor[2]);
    $('#id_proveedor').val(datos_proveedor[1]);
});

$('#tabla_cuenta_cargo tbody').on( 'click', 'tr', function () {
    datos_tabla_cuenta_cargo = $('#tabla_cuenta_cargo').DataTable().row( this ).data();
});

$('#modal_cuenta_cargo').on('hidden.bs.modal', function () {
    $('#cuenta_cargo').val(datos_tabla_cuenta_cargo[0]);
    $('#descripcion_cuenta_cargo').val(datos_tabla_cuenta_cargo[1]);
});

$('#tabla_cuenta_abono tbody').on( 'click', 'tr', function () {
    datos_tabla_cuenta_abono = $('#tabla_cuenta_abono').DataTable().row( this ).data();
});

$('#modal_cuenta_abono').on('hidden.bs.modal', function () {
    $('#cuenta_abono').val(datos_tabla_cuenta_abono[0]);
    $('#descripcion_cuenta_abono').val(datos_tabla_cuenta_abono[1]);
});

$('#tabla_contrarecibos tbody').on( 'click', 'tr', function () {
    datos_tabla_contrarecibo = $('#tabla_contrarecibos').DataTable().row( this ).data();
    $('#hidden_contrarecibo').val(datos_tabla_contrarecibo[0]);
});

$('#modal_contrarecibo').on('hidden.bs.modal', function () {
    var contrarecibo = $('#hidden_contrarecibo').val();
    var cuenta = $('#hidden_cuenta').val();
    var movimiento = $('#ultimo_movimiento').val();

    $.ajax({
        url: '/ciclo/tomar_datos_Contrarecibo',
        dataType: 'json',
        method: 'POST',
        data: {
            contrarecibo: contrarecibo,
            cuenta: cuenta,
            movimiento: movimiento
        },
        success: function(s){

            if(s.mensaje == "ok") {
                $("#id_proveedor").val(s.id_proveedor);
                $("#numero_contrarecibo").val(datos_tabla_contrarecibo[0]);
                $("#proveedor").val(s.proveedor);
                $("#concepto_especifico").val(s.concepto_especifico);
                $("#descripcion_general").val(s.concepto);
                $("#concepto").val(s.destino);

                if(s.destino == "Honorarios") {
                    $('#honorarios').val(1);
                } else {
                    $('#honorarios').val(0);
                    $("#desglose_honorarios").html('');
                }

                refrescarDetalle();
            }
        },
        error: function(e){
            console.log(e.responseText);
        }
    });
});

$('#tabla_detalle tbody').on( 'click', 'tr', function () {
    datos_tabla_detalle = $('#tabla_detalle').DataTable().row( this ).data();
    $('#borrar_movimiento_hidden').val(datos_tabla_detalle[0]);
});

$("#elegir_borrar_movimiento").click(function() {
    var detalle = $("#borrar_movimiento_hidden").val();

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/ciclo/borrar_detalle_movimiento",
        data: {
            id_detalle: detalle
        }
    })
        .done(function( data ) {
            if(data.mensaje == "ok") {
                $("#proveedor").val('');
                $("#descripcion_general").html('');
                refrescarDetalle();
            }
            else {
                console.log("Error al cancelar el compromiso");
            }
        })
        .fail(function(e) {
            console.log(e.responseText);
        });
});

$("#borrar_cambios").click(function() {
    var movimiento = $("#ultimo_movimiento").val();

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/ciclo/borrar_movimiento",
        data: {
            movimiento: movimiento
        }
    })
        .done(function( data ) {
            if(data) {
                window.history.back();
            }
            else {
                console.log("Error al cancelar el compromiso");
            }
        })
        .fail(function(e) {
            console.log(e.responseText);
        });
});

$("#guardar_movimiento").on( "click", function(e) {
    $(".forma_movimiento").validate({
        focusInvalid: true,
        rules: {
            tipo_movimiento:{
                required: true
            },
            concepto: {
                required: true
            },
            cheque: {
                required: true
            },
            movimiento_bancario: {
                required: true
            },
            importe: {
                required: true
            },
            proveedor: {
                required: true
            },
            concepto_especifico: {
                required: true
            },
            descripcion_general: {
                required: true
            },
            fecha_emision: {
                required: true,
                date: true
            },
            fecha_pago: {
                required: true,
                date: true
            },
            tiempo_movimiento: {
                required: false
            }
        },
        messages: {
            tipo_movimiento: {
                required: "* Este campo es obligatorio"
            },
            concepto: {
                required: "* Este campo es obligatorio"
            },
            cheque: {
                required: "* Este campo es obligatorio"
            },
            movimiento_bancario: {
                required: "* Este campo es obligatorio"
            },
            importe: {
                required: "* Este campo es obligatorio"
            },
            proveedor: {
                required: "* Este campo es obligatorio"
            },
            concepto_especifico: {
                required: "* Este campo es obligatorio"
            },
            descripcion_general: {
                required: "* Este campo es obligatorio"
            },
            fecha_emision: {
                required: "* Este campo es obligatorio",
                date: "El campo tiene que tener el siguiente formato AAAA-MM-DD"
            },
            fecha_pago: {
                required: "* Este campo es obligatorio",
                date: "El campo tiene que tener el siguiente formato AAAA-MM-DD"
            },
            tiempo_movimiento: {
                required: "* Este campo es obligatorio"
            }
        }
    }).form();

    if($(".forma_movimiento").valid()) {
        e.preventDefault();

        var map = {};
        $(":input").each(function () {
            map[$(this).attr("name")] = $(this).val();
        });

        if ($('#check_firme').is(':checked')) {
            map.check_firme = 1;
        } else {
            map.check_firme = 0;
        }

        $.ajax({
            url: '/ciclo/insertar_movimiento',
            method: 'POST',
            dataType: 'json',
            data: map,
            success: function (s) {
                $("#resultado_insertar_caratula").html(s.mensaje_insertar);
                $("#guardar_movimiento").prop('disabled', true);
            },
            error: function (e) {
                console.log(e.responseText);
            }
        });
    }
});

$("#guardar_movimiento_presupuesto").on( "click", function(e) {
    $(".forma_movimiento").validate({
        focusInvalid: true,
        rules: {
            numero_contrarecibo:{
                required: true
            },
            tipo_movimiento:{
                required: true
            },
            concepto: {
                required: true
            },
            cheque: {
                required: true
            },
            movimiento_bancario: {
                required: true
            },
            importe: {
                required: true
            },
            neto: {
                required: true
            },
            proveedor: {
                required: true
            },
            concepto_especifico: {
                required: true
            },
            descripcion_general: {
                required: true
            },
            fecha_emision: {
                required: true,
                date: true
            },
            fecha_pago: {
                required: true,
                date: true
            },
            tiempo_movimiento: {
                required: false
            }
        },
        messages: {
            numero_contrarecibo: {
                required: "* Este campo es obligatorio"
            },
            tipo_movimiento: {
                required: "* Este campo es obligatorio"
            },
            concepto: {
                required: "* Este campo es obligatorio"
            },
            cheque: {
                required: "* Este campo es obligatorio"
            },
            movimiento_bancario: {
                required: "* Este campo es obligatorio"
            },
            importe: {
                required: "* Este campo es obligatorio"
            },
            neto: {
                required: "* Este campo es obligatorio"
            },
            proveedor: {
                required: "* Este campo es obligatorio"
            },
            concepto_especifico: {
                required: "* Este campo es obligatorio"
            },
            descripcion_general: {
                required: "* Este campo es obligatorio"
            },
            fecha_emision: {
                required: "* Este campo es obligatorio",
                date: "El campo tiene que tener el siguiente formato AAAA-MM-DD"
            },
            fecha_pago: {
                required: "* Este campo es obligatorio",
                date: "El campo tiene que tener el siguiente formato AAAA-MM-DD"
            },
            tiempo_movimiento: {
                required: "* Este campo es obligatorio"
            }
        }
    }).form();

    if($(".forma_movimiento").valid()) {
        e.preventDefault();

        var map = {};
        $(":input").each(function () {
            map[$(this).attr("name")] = $(this).val();
        });

        if ($('#check_firme').is(':checked')) {
            map.check_firme = 1;
        } else {
            map.check_firme = 0;
        }

        $.ajax({
            url: '/ciclo/insertar_movimiento_presupuesto',
            method: 'POST',
            dataType: 'json',
            data: map,
            success: function (s) {
                $("#resultado_insertar_caratula").html(s.mensaje_insertar);
                $("#guardar_movimiento_presupuesto").prop('disabled', true);
            },
            error: function (e) {
                console.log(e.responseText);
            }
        });
    }
});

$(document).ready(function() {

    $('#tabla_proveedores').dataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "tabla_proveedores_AJAX",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs": [ {
            "targets":  0 ,
            "visible": false,
            "searchable": false
        }, {
            "targets": -1,
            "data": null,
            "defaultContent":
                '<a data-dismiss="modal"><i class="fa fa-check"></i></a>'
        } ]
    });

    $('#tabla_detalle').dataTable({
        "ajax": "tabla_detalle_movimiento",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs": [ {
            "targets": [ 0 ],
            "visible": false,
            "searchable": false
        } ]
    });

    $('#tabla_contrarecibos').dataTable({
        "ajax": "tabla_contrarecibos_movimientos",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });

    $('#tabla_cuenta_cargo').dataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": "tabla_cuentas_contables",
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs": [ {
            "targets": -1,
            "data": null,
            "defaultContent":
                '<a data-dismiss="modal"><i class="fa fa-check"></i></a>'
        } ]
    } );

    $('#tabla_cuenta_abono').dataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": "tabla_cuentas_contables",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs": [ {
            "targets": -1,
            "data": null,
            "defaultContent":
                '<a data-dismiss="modal"><i class="fa fa-check"></i></a>'
        } ]
    } );

    $('#tabla_bancos_concepto').dataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": "tabla_bancos_conceptos",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs": [ {
            "targets": -1,
            "data": null,
            "defaultContent":
                '<a data-dismiss="modal"><i class="fa fa-check"></i></a>'
        } ]
    } );

    $("#fecha_emision").val(curr_year + "-" + curr_month + "-" + curr_date);
    $("#fecha_inicial").val("2015-01-01");
    $("#fecha_final").val("2015-12-31");

});

function refrescarDetalle(data) {
    $.ajax({
        url: '/ciclo/tabla_detalle_movimiento',
        dataType: 'json',
        method: 'POST',
        data: {
            movimiento: $('#ultimo_movimiento').val()
        },
        success: function(s){
            $('#tabla_detalle').dataTable().fnClearTable();
            var total = 0;
            for(var i = 0; i < s.length; i++) {
                total += parseFloat(s[i][3]);
                $('#tabla_detalle').dataTable().fnAddData([
                    s[i][0],
                    s[i][1],
                    s[i][2],
                    "$"+$.number( s[i][3], 2 ),
                    s[i][4]
                ]);
            } // End For

            if($('#honorarios').val() == 1) {
                var subtotal = total / 1.16;
                var iva = subtotal * .16;

                var total_pagar = total;
                var isr = subtotal * .10;
                var iva_retenido = (iva / 3) * 2;
                total_pagar -= isr;
                total_pagar -= iva_retenido;
                $("#desglose_honorarios").html(
                    '<div class="col-lg-12"> ' +
                    '<div class="panel panel-default"> ' +
                    '<div class="panel-heading">' +
                    'Desglose de Honorarios' +
                    '</div> ' +
                    '<div class="panel-body"> ' +
                    '<div class="row"> ' +
                    '<div class="col-lg-3"></div>'+
                    '<div class="col-lg-3"> ' +
                    '<h5>Monto de los honorarios</h5> ' +
                    '<h5>IVA 16%</h5> ' +
                    '<hr>'+
                    '<h5><b>Subtotal</b></h5> ' +
                    '<hr>'+
                    '<h5>ISR 10%</h5> ' +
                    '<h5>IVA Retenido</h5> ' +
                    '<hr>'+
                    '<h4 style="background:#f8f8f8; padding: 4%;">Total a pagar</h4> ' +
                    '</div> ' +
                    '<div class="col-lg-3"> ' +
                    '<h5>$'+$.number( subtotal, 2 )+'</h5> ' +
                    '<h5>$'+$.number( iva, 2 )+'</h5> ' +
                    '<hr>'+
                    '<h5><b>$'+$.number( total, 2 )+'</b></h5> ' +
                    '<hr>'+
                    '<h5>$'+$.number( isr, 2 )+'</h5> ' +
                    '<h5>$'+$.number( iva_retenido, 2 )+'</h5> ' +
                    '<hr>'+
                    '<h4  style="background:#f8f8f8; padding: 4%;">$'+$.number( total_pagar , 2 )+'</h4> ' +
                    '</div> ' +
                    '<div class="col-lg-3"></div>'+
                    '</div> ' +
                    '</div> ' +
                    '</div>');
            } else {
                total_pagar = total;
            }

            $('#importe').val(total);
            $('#neto').val(total_pagar);
        },
        error: function(e){
            console.log(e.responseText);
        }
    });
}

$("#elegir_borrar_matriz").click(function() {
    var matriz = $("#ultimo_movimiento").val();

    var table = $('#tabla_detalle').DataTable();

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "tabla_detalle_movimiento",
        data: {
            matriz: matriz
        }
    })
        .done(function( data ) {
            console.log(data);
            if(data.mensaje == "ok") {
                table.ajax.reload();
            }
            else {
                alert("Ha ocurrido un error");
            }
        })
        .fail(function(e) {
            // If there is no communication between the server, show an error
            console.log(e.responseText);
            //alert( "error occured" );
        });
});
