$(document).ready(function() {

    $('#tabla_proveedores').dataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "/ciclo/tabla_proveedores_AJAX",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs": [ {
            "targets":  0 ,
            "visible": false,
            "searchable": false
        }, {
            "targets": -1,
            "data": null,
            "defaultContent":
                '<a data-dismiss="modal"><i class="fa fa-check"></i></a>'
        } ]
    });

    $('#tabla_cuentas').dataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "../tabla_cuentas",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs": [ {
            "targets": -1,
            "data": null,
            "defaultContent":
                '<a data-dismiss="modal"><i class="fa fa-check"></i></a>'
        } ]
    });

    $('#tabla_centro_costos').dataTable({
        "ajax": "../tabla_centro_costos",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });

    $('#tabla_subsidio').dataTable({
        "ajax": "../tabla_subsidio",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });

    $('.datos_tabla').dataTable({
        "ajax": "../tabla_detalle_poliza",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs": [ {
            "targets":  0 ,
            "visible": false,
            "searchable": false
        }, {
            "targets":  -1 ,
            "visible": false,
            "searchable": false
        }, {
            "targets": [ 6 , 7 ],
            "render": function ( data, type, row ) {
                return '<span style="text-align: left; float: left;">$</span>'+'<span style="text-align: right; float: right;">'+ data +'</span>';
            }
        } ]
    });

    refrescarDetalle();

});

function refrescarDetalle() {
    $.ajax({
        url: '/contabilidad/tabla_detalle_poliza',
        dataType: 'json',
        method: 'POST',
        data: {
            poliza: $("#ultima_poliza").val()
        },
        success: function(s){
            $('.datos_tabla').dataTable().fnClearTable();
            var debe = 0;
            var haber = 0;
            for(var i = 0; i < s.length; i++) {
                debe += parseFloat(s[i][6]);
                haber += parseFloat(s[i][7]);
                $('.datos_tabla').dataTable().fnAddData([
                    s[i][0],
                    s[i][1],
                    s[i][2],
                    s[i][3],
                    s[i][4],
                    s[i][5],
                    $.number( s[i][6], 2 ),
                    $.number( s[i][7], 2 ),
                    s[i][8],
                    s[i][9]
                ]);
            } // End For

            if(debe != haber) {
                $('#check_firme').prop('disabled', true);
                $("#sumas_totales").html("<div class='row'><div class='col-lg-3'></div><div class='col-lg-2 table-poliza-result-f'>Debe"+ "<br><span style='color:#848484; font-weight: normal; line-height: 200%;'>" + " $ "+$.number( debe, 2 )+ "</span></div><div class='col-lg-2'></div>"+"<div class='col-lg-2 table-poliza-result-f'>Haber"+ "<br><span style='color:#848484;font-weight: normal; line-height: 200%;'>" + " $ "+$.number( haber, 2 )+ "</span></div><div class='col-lg-3'></div></div>");

            } else {
                $('#check_firme').prop('disabled', false);
                $("#sumas_totales").html("<div class='row'><div class='col-lg-3'></div><div class='col-lg-2 table-poliza-result'>Debe"+ "<br><span style='color:#848484; font-weight: normal; line-height: 200%;'>" + " $ "+$.number( debe, 2 )+ "</span></div><div class='col-lg-2'></div>"+"<div class='col-lg-2 table-poliza-result'>Haber"+ "<br><span style='color:#848484;font-weight: normal; line-height: 200%;'>" + " $ "+$.number( haber, 2 )+ "</span></div><div class='col-lg-3'></div></div>");
            }

            $("#debe_hidden").val(debe);
            $("#haber_hidden").val(haber);

        },
        error: function(e){
            console.log(e.responseText);
        }
    });
}