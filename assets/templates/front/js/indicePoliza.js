var tipo_precompromiso;
var datos_tabla;

$(document).ready(function() {
    $('.datos_tabla').dataTable({
        "ajax": "/contabilidad/tabla_indice_polizas",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         'Buscar ',
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "dom": 'T<"clear">lfrtip',
        "tableTools": {
            "sSwfPath": "/swf/copy_csv_xls_pdf.swf"
        }
    });
    $('.datos_tabla_diario').dataTable({
        "ajax": "/contabilidad/tabla_indice_polizasDiario",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         'Buscar ',
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "dom": 'T<"clear">lfrtip',
        "tableTools": {
            "sSwfPath": "/swf/copy_csv_xls_pdf.swf"
        }
    });

    $('.datos_tabla_ingresos').dataTable({
        "ajax": "/contabilidad/tabla_indice_polizasIngresos",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         'Buscar ',
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "dom": 'T<"clear">lfrtip',
        "tableTools": {
            "sSwfPath": "/swf/copy_csv_xls_pdf.swf"
        }
    });

    $('.datos_tabla_egresos').dataTable({
        "ajax": "/contabilidad/tabla_indice_polizasEgresos",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         'Buscar ',
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "dom": 'T<"clear">lfrtip',
        "tableTools": {
            "sSwfPath": "/swf/copy_csv_xls_pdf.swf"
        }
    });

    //setInterval(refrescarIndice, 500);

});

$('.datos_tabla tbody').on( 'click', 'tr', function () {
    datos_tabla = $('.datos_tabla').DataTable().row( this ).data();
    $("#cancelar_poliza").val(datos_tabla[0]);
    $("#regenerar_poliza").val(datos_tabla[0]);
});

$('.modal_borrar').on('show.bs.modal', function (event) {
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this);
    modal.find('#cancelar_poliza').val(datos_tabla[0]);
});

$('.modal_regenerar').on('hide.bs.modal', function (event) {
    var modal = $(this);
    $("#elegir_regenerar_poliza").show();
});

$("#elegir_cancelar_poliza").click(function() {
    var poliza = $("#cancelar_poliza").val();

    var table = $('.datos_tabla').DataTable();

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/contabilidad/cancelar_poliza_caratula",
        data: {
            poliza: poliza
        }
    })
        .done(function( data ) {
            table.ajax.reload();
        })
        .fail(function(e) {
            console.log(e.responseText);
        });
});

$("#elegir_regenerar_poliza").click(function() {
    var poliza = $("#regenerar_poliza").val();

    var table = $('.datos_tabla').DataTable();

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/contabilidad/regenerar_poliza",
        data: {
            poliza: poliza
        },
        beforeSend: function (data) {
            $('#resultado_regenerar_poliza').html( '<img src="'+js_base_url("img/ajax-loader2.gif")+'" style="width: 20%;" />');
        }
    })
        .done(function( data ) {
            $("#resultado_regenerar_poliza").html(data.mensaje);
            $("#elegir_regenerar_poliza").hide();
            table.ajax.reload();
            //$('.modal_regenerar').modal('hide');
        })
        .fail(function(e) {
            console.log(e);
            console.log(e.responseText);
        });
});

function BtnTodas() {
    document.getElementById('tab-todas').style.display='block';
    document.getElementById('tab-diario').style.display='none';
    document.getElementById('tab-ingresos').style.display='none';
    document.getElementById('tab-egresos').style.display='none';
    document.getElementById('btn-todas').className='active';
    document.getElementById('btn-diario').className='noactive';
    document.getElementById('btn-ingresos').className='noactive';
    document.getElementById('btn-egresos').className='noactive';

}
function BtnDiario() {
    document.getElementById('tab-diario').style.display='block';
    document.getElementById('tab-todas').style.display='none';
    document.getElementById('tab-ingresos').style.display='none';
    document.getElementById('tab-egresos').style.display='none';
    document.getElementById('btn-diario').className='active';
    document.getElementById('btn-todas').className='noactive';
    document.getElementById('btn-ingresos').className='noactive';
    document.getElementById('btn-egresos').className='noactive';

}
function BtnIngresos() {
    document.getElementById('tab-ingresos').style.display='block';
    document.getElementById('tab-todas').style.display='none';
    document.getElementById('tab-diario').style.display='none';
    document.getElementById('tab-egresos').style.display='none';
    document.getElementById('btn-ingresos').className='active';
    document.getElementById('btn-todas').className='noactive';
    document.getElementById('btn-diario').className='noactive';
    document.getElementById('btn-egresos').className='noactive';
}
function BtnEgresos() {
    document.getElementById('tab-egresos').style.display='block';
    document.getElementById('tab-todas').style.display='none';
    document.getElementById('tab-diario').style.display='none';
    document.getElementById('tab-ingresos').style.display='none';
    document.getElementById('btn-egresos').className='active';
    document.getElementById('btn-todas').className='noactive';
    document.getElementById('btn-diario').className='noactive';
    document.getElementById('btn-ingresos').className='noactive';
}