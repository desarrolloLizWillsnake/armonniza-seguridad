var editor;

$(document).ready(function() {
    editor = new $.fn.dataTable.Editor( {
        ajax: "/catalogos/tabla_medicamentos",
        table: ".datos_tabla",
        fields: [ {
            label: "N�mero",
            name: "numero"
        }, {
            label: "Clave",
            name: "clave"
        }, {
            label: "Grupo Terap�utico",
            name: "grupo_terapeutico"
        }, {
            label: "Nombre Gen�rico",
            name: "nombre_generico"
        }, {
            label: "GI",
            name: "gi"
        }
        ],
        i18n: {
            create: {
                button: "<i class='fa fa-plus-circle circle ic-catalogo'></i> Nuevo",
                title:  "<i class='fa fa-plus-circle ic-catalogo-emergente'></i> Nuevo Concepto de Gasto",
                submit: "Ingresar"
            },
            edit: {
                button: "<i class='fa fa-edit circle ic-catalogo'></i> Editar",
                title:  "<i class='fa fa-edit ic-catalogo-emergente'></i> Editar Concepto de Gasto",
                submit: "Actualizar"
            },
            remove: {
                button: "<i class='fa fa-trash-o circle ic-catalogo'></i> Borrar",
                title:  "<i class='fa fa-trash-o ic-catalogo-emergente'></i> Borrar",
                submit: "Borrar",
                confirm: {
                    _: "�Esta seguro que desea eliminar los %d conceptos de gasto seleccionados?",
                    1: "�Esta seguro que desea eliminar el concepto de gasto seleccionado?"
                }
            },
            error: {
                system: "Ha ocurrido un error, por favor, contacte al administrador del sistema."
            }
        }
    } );

    $('.datos_tabla').DataTable( {
        dom: "Tfrtip",
        ajax: {
            url: "/catalogos/tabla_medicamentos",
            type: "POST"
        },
        serverSide: true,
        columns: [
            { data: "numero" },
            { data: "clave" },
            { data: "grupo_terapeutico" },
            { data: "nombre_generico" },
            { data: "gi" }
        ],
        tableTools: {
            sRowSelect: "os",
            aButtons: [
                { sExtends: "editor_create", editor: editor },
                { sExtends: "editor_edit",   editor: editor },
                { sExtends: "editor_remove", editor: editor }
            ]
        },
        language: {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ning�n dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "�ltimo",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    } );
} );