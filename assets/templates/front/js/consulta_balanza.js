$(document).ready(function() {

    var map = {};
    $(":input").each(function() {
        map[$(this).attr("name")] = $(this).val();
    });

    //var table = $('.datos_tabla').DataTable();

    $.ajax({
        url: 'generar_tabla_principal_balanza',
        method: 'POST',
        dataType: 'json',
        data: map,
        beforeSend: function(a){
            $("#espera").html('<img src="../img/ajax-loader2.gif" style="width: 50px;" />');
        },
        success: function(s){
            $("#espera").html('');
            refrescarDetalle();
            //table.ajax.reload();
        },
        error: function(e){
            console.log(e.responseText);
        }
    });


});

$("#exportar_consulta_balanza").on( "submit", function() {
    var map = {};
    $(":input").each(function() {
        map[$(this).attr("name")] = $(this).val();
    });

    var input = $("<input>")
        .attr("type", "hidden")
        .attr("name", "fecha_inicial").val();
    $('#forma_exportar').append($(input));

    var input = $("<input>")
        .attr("type", "hidden")
        .attr("name", "fecha_final").val();
    $('#forma_exportar').append($(input));
});

function refrescarDetalle() {

    $('.datos_tabla').dataTable({
        "ajax": "tabla_principal_balanza",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });

    //var map = {};
    //$(":input").each(function() {
    //    map[$(this).attr("name")] = $(this).val();
    //});

    //$.ajax({
    //    url: 'tabla_principal_balanza',
    //    dataType: 'json',
    //    method: 'POST',
    //    data: map,
    //    success: function(s){
    //        $('.datos_tabla').dataTable().fnClearTable();
    //        for(var i = 0; i < s.length; i++) {
    //            $('.datos_tabla').dataTable().fnAddData([
    //                s[i][0],
    //                s[i][1],
    //                s[i][2],
    //                s[i][3],
    //                s[i][4]
    //            ]);
    //        } // End For
    //    },
    //    error: function(e){
    //        console.log(e.responseText);
    //    }
    //});
}
