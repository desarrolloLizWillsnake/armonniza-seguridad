/**
 * Created by Lizbeth on 31/07/15.
 */

var datos_tabla;

$(document).ready(function() {
    $('.datos_tabla').dataTable({
        "ajax": "/administrador/tabla_usuarios",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "dom": 'T<"clear">lfrtip',
        "tableTools": {
            "sSwfPath": "/swf/copy_csv_xls_pdf.swf"
        },
        "columnDefs": [ {
            "targets": [ 0 ],
            "visible": false,
            "searchable": false
        } ]
    });

});

$('.datos_tabla tbody').on( 'click', 'tr', function () {
    datos_tabla = $('.datos_tabla').DataTable().row( this ).data();
});

$('.modal_bloquear').on('show.bs.modal', function (event) {
    var modal = $(this);
    modal.find('#bloquear_usuario_id').val(datos_tabla[0]);
});
$("#elegir_bloquear").click(function() {
    var usuario = $("#bloquear_usuario_id").val();

    var table = $('.datos_tabla').DataTable();

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/administrador/bloquear_usuario",
        data: {
            usuario: usuario
        }
    })
        .done(function( data ) {
            if(data.mensaje == "ok") {
                table.ajax.reload();
            }
            else {
                alert("Ha ocurrido un error");
            }
        })
        .fail(function(e) {
            // If there is no communication between the server, show an error
            console.log("Error");
            //alert( "error occured" );
        });
});

$('.modal_desbloquear').on('show.bs.modal', function (event) {
    var modal = $(this);
    modal.find('#desbloquear_usuario_id').val(datos_tabla[0]);
});
$("#elegir_desbloquear").click(function() {
    var usuario = $("#desbloquear_usuario_id").val();

    var table = $('.datos_tabla').DataTable();

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/administrador/desbloquear_usuario",
        data: {
            usuario: usuario
        }
    })
        .done(function( data ) {
            if(data.mensaje == "ok") {
                table.ajax.reload();
            }
            else {
                alert("Ha ocurrido un error");
            }
        })
        .fail(function(e) {
            // If there is no communication between the server, show an error
            console.log("Error");
            //alert( "error occured" );
        });
});

$('.modal_baja').on('show.bs.modal', function (event) {
    var modal = $(this);
    modal.find('#baja_usuario_id').val(datos_tabla[0]);
});
$("#elegir_baja").click(function() {
    var usuario = $("#baja_usuario_id").val();

    var table = $('.datos_tabla').DataTable();

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/administrador/baja_usuario",
        data: {
            usuario: usuario
        }
    })
        .done(function( data ) {
            if(data.mensaje == "ok") {
                table.ajax.reload();
            }
            else {
                alert("Ha ocurrido un error");
            }
        })
        .fail(function(e) {
            // If there is no communication between the server, show an error
            console.log("Error");
            //alert( "error occured" );
        });
});

$('.modal_activar').on('show.bs.modal', function (event) {
    var modal = $(this);
    modal.find('#activar_usuario_id').val(datos_tabla[0]);
});
$("#elegir_activar").click(function() {
    var usuario = $("#activar_usuario_id").val();

    var table = $('.datos_tabla').DataTable();

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/administrador/activar_usuario",
        data: {
            usuario: usuario
        }
    })
        .done(function( data ) {
            if(data.mensaje == "ok") {
                table.ajax.reload();
            }
            else {
                alert("Ha ocurrido un error");
            }
        })
        .fail(function(e) {
            // If there is no communication between the server, show an error
            console.log("Error");
            //alert( "error occured" );
        });
});

$('.modal_restaurar_contraseña').on('show.bs.modal', function (event) {
    var modal = $(this);
    modal.find('#restaurar_usuario_id').val(datos_tabla[0]);
});
$("#elegir_restaurar").click(function() {
    var usuario = $("#restaurar_usuario_id").val();

    var table = $('.datos_tabla').DataTable();

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/administrador/resetear_contrasena",
        data: {
            usuario: usuario
        }
    })
        .done(function( data ) {
            if(data.mensaje == "ok") {
                $('.resultado_restaurar_contraseña').modal('show');
                table.ajax.reload();
            }
            else {
                alert("Ha ocurrido un error");
            }
        })
        .fail(function(e) {
            // If there is no communication between the server, show an error
            console.log("Error");
            //alert( "error occured" );
        });
});