var datos_tabla_contrarecibo;
var datos_tabla_cuenta_cargo;
var datos_tabla_cuenta_abono;
var datos_tabla_gastos;
var datos_tabla_detalle;
var d = new Date();
var curr_date = d.getDate();
var curr_month = d.getMonth() + 1; //Months are zero based
var curr_year = d.getFullYear();

//Esta funcion es para traducir el datepicker
$(function($){
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);
});

//Se crean las cajas para la seleccion de fecha
$( "#fecha" ).datepicker({ dateFormat: 'yy-mm-dd', minDate: '2015-01-01' });

$("#guardar_nota_detalle").on( "click", function(e) {
    e.preventDefault();

    var map = {};
    $(":input").each(function() {
        map[$(this).attr("name")] = $(this).val();
    });

    map.nivel1 = $("#input_nivel1").val();
    map.nivel2 = $("#input_nivel2").val();
    map.nivel3 = $("#input_nivel3").val();
    map.nivel4 = $("#input_nivel4").val();
    map.nivel5 = $("#input_nivel5").val();
    map.nivel6 = $("#input_nivel6").val();

    $(".forma_nota").validate({
        focusInvalid: true,
        rules: {
            documento: {
                required: true
            },
            tipo_doc: {
                required: true
            },
            clave_proveedor: {
                required: true
            },
            proveedor: {
                required: true
            },
            observaciones: {
                required: true
            },
            fecha: {
                required: true,
                date: true
            },
            tipo_poliza: {
                required: true
            },
            condicion_entrega: {
                required: true
            }
        },
        messages: {
            documento: {
                required: "* Este campo es obligatorio"
            },
            tipo_doc: {
                required: "* Este campo es obligatorio"
            },
            clave_proveedor: {
                required: "* Este campo es obligatorio"
            },
            observaciones: {
                required: "* Este campo es obligatorio"
            },
            condicion_entrega: {
                required: "* Este campo es obligatorio"
            },
            fecha: {
                required: "* Este campo es obligatorio",
                date: "El campo tiene que tener el siguiente formato AAAA-MM-DD"
            },
            tipo_poliza: {
                required: "* Este campo es obligatorio"
            }
        }
    }).form();

    if($(".forma_nota").valid()){
        $.ajax({
            url: '/patrimonio/insertar_detalle_nota_entrada',
            method: 'POST',
            dataType: 'json',
            data: map,
            success: function(s){
                $("#resultado_insertar_detalle").html(s.mensaje);
                $("#input_nivel1").val('');
                $("#input_nivel2").val('');
                $("#input_nivel3").val('');
                $("#input_nivel4").val('');
                $("#input_nivel5").val('');
                $("#input_nivel6").val('');
                $("#gasto").val('');
                $("#titulo_gasto").val('');
                $("#u_medida").val('');
                $("#cantidad").val('');
                $("#precio").val('');
                $("#descripcion_detalle").val('');
                refrescarDetalle();
            },
            error: function(e){
                console.log(e.responseText);
            }
        });
    }
});

$("#guardar_nota").on( "click", function(e) {
    e.preventDefault();

    $(".forma_nota").validate({
        focusInvalid: true,
        rules: {
            documento: {
                required: false
            },
            tipo_doc: {
                required: false
            },
            clave_proveedor: {
                required: false
            },
            proveedor: {
                required: false
            },
            observaciones: {
                required: false
            },
            fecha: {
                required: false,
                date: true
            },
            tipo_poliza: {
                required: true
            },
            condicion_entrega: {
                required: false
            },
            gasto: {
                required: false
            },
            titulo_gasto: {
                required: false
            },
            u_medida: {
                required: false
            },
            cantidad: {
                required: false
            },
            precio: {
                required: false
            },
            descripcion_detalle: {
                required: false
            }
        },
        messages: {
            tipo_poliza: {
                required: "* Este campo es obligatorio"
            }
        }
    }).form();

if($(".forma_nota").valid()){
        var map = {};
        $(":input").each(function() {
            map[$(this).attr("name")] = $(this).val();
        });

        if($('#check_firme').is(':checked')){
            map.check_firme = 1;
        } else {
            map.check_firme = 0;
        }

        $.ajax({
            url: '/patrimonio/insertar_nota_entrada',
            method: 'POST',
            dataType: 'json',
            data: map,
            success: function(s){
                $("#resultado_insertar_caratula").html(s.mensaje);
            },
            error: function(e){
                console.log(e.responseText);
            }
        });
    }

});

$('#tabla_detalle tbody').on( 'click', 'tr', function () {
    datos_tabla_detalle = $('#tabla_detalle').DataTable().row( this ).data();
    $('#borrar_nota_hidden').val(datos_tabla_detalle[0]);
    $('#editar_cc_nota').val(datos_tabla_detalle[3]);
    $('#editar_articulo_nota').val(datos_tabla_detalle[7]);
    $('#precio_unitario_hidden').val(datos_tabla_detalle[10]);
});

$('#tabla_contrarecibo tbody').on( 'click', 'tr', function () {
    datos_tabla_contrarecibo = $('#tabla_contrarecibo').DataTable().row( this ).data();
    $('#hidden_no_contrarecibo').val(datos_tabla_contrarecibo[0]);
    $('#hidden_no_compromiso').val(datos_tabla_contrarecibo[1]);
});

$('#modal_contrarecibo').on('hidden.bs.modal', function () {
    var contrarecibo = $('#hidden_no_contrarecibo').val();
    var compromiso = $('#hidden_no_compromiso').val();
    var ultimo = $('#ultima_nota').val();

    $.ajax({
        url: '/patrimonio/copiar_datos_contrarecibo',
        dataType: 'json',
        method: 'POST',
        data: {
            contrarecibo: contrarecibo,
            compromiso: compromiso,
            ultima_nota: ultimo
        },
        success: function(s) {
            $('#no_contrarecibo').val(s.contrarecibo);
            $('#compromiso').val(s.compromiso);
            $('#clave_proveedor').val(s.id_proveedor);
            $('#id_proveedor').val(s.id_proveedor);
            $('#proveedor').val(s.proveedor);
            $('#tipo_documento').val(s.documento);
            $('#documento').val(s.no_documento);
            $('#observaciones').val(s.observaciones);
            refrescarDetalle();
        },
        error: function(e){
            console.log(e.responseText);
        }
    });
});

$('#tabla_proveedores tbody').on( 'click', 'tr', function () {
    datos_proveedor = $('#tabla_proveedores').DataTable().row( this ).data();
});

$('#modal_proveedores').on('hidden.bs.modal', function () {
    $('#proveedor').val(datos_proveedor[2]);
    $('#clave_proveedor').val(datos_proveedor[1]);
    $('#id_proveedor').val(datos_proveedor[1]);
});

$('#tabla_cuenta_cargo tbody').on( 'click', 'tr', function () {
    datos_tabla_cuenta_cargo = $('#tabla_cuenta_cargo').DataTable().row( this ).data();
});

$('#modal_cuenta_cargo').on('hidden.bs.modal', function () {
    $('#cuenta_cargo').val(datos_tabla_cuenta_cargo[0]);
    $('#descripcion_cuenta_cargo').val(datos_tabla_cuenta_cargo[1]);
});

$('#tabla_cuenta_abono tbody').on( 'click', 'tr', function () {
    datos_tabla_cuenta_abono = $('#tabla_cuenta_abono').DataTable().row( this ).data();
});

$('#modal_cuenta_abono').on('hidden.bs.modal', function () {
    $('#cuenta_abono').val(datos_tabla_cuenta_abono[0]);
    $('#descripcion_cuenta_abono').val(datos_tabla_cuenta_abono[1]);
});

$('#tabla_gastos tbody').on( 'click', 'tr', function () {
    datos_tabla_gastos = $('#tabla_gastos').DataTable().row( this ).data();
});

$('#modal_gasto').on('hidden.bs.modal', function () {
    $('#gasto').val(datos_tabla_gastos[0]);
    $('#titulo_gasto').val(datos_tabla_gastos[2]);
    $('#u_medida').val(datos_tabla_gastos[3]);
});

$("#input_nivel1").keyup(function() {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/egresos/obtener_datosUltimoNivel",
        data: {
            fecha: $("#fecha_solicitud").val(),
            nivel1: $("#input_nivel1").val(),
            nivel2: $("#input_nivel2").val(),
            nivel3: $("#input_nivel3").val(),
            nivel4: $("#input_nivel4").val(),
            nivel5: $("#input_nivel5").val(),
            nivel6: $("#input_nivel6").val()
        }
    })
        .done(function( data ) {
            $("#elegir_ultimoNivel").prop('disabled', false);
            var resultado = parseFloat(data.saldo) - parseFloat(data.mes_compromiso);
            // When the response to the AJAX request comes back render the chart with new data
            $("#mes_grafica").html("<p>"+data.mes+"</p>");
            chart.setData(
                [
                    { y: "Saldo del Mes", a: data.saldo },
                    { y: "Comprometido del Mes", a: data.mes_compromiso },
                    { y: "Por Ejercer", a: resultado }
                ]);
        })
        .fail(function(e) {
            // If there is no communication between the server, show an error
            console.log(e.responseText);
            //alert( "error occured" );
        });
});

$("#input_nivel2").keyup(function() {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/egresos/obtener_datosUltimoNivel",
        data: {
            fecha: $("#fecha_solicitud").val(),
            nivel1: $("#input_nivel1").val(),
            nivel2: $("#input_nivel2").val(),
            nivel3: $("#input_nivel3").val(),
            nivel4: $("#input_nivel4").val(),
            nivel5: $("#input_nivel5").val(),
            nivel6: $("#input_nivel6").val()
        }
    })
        .done(function( data ) {
            $("#elegir_ultimoNivel").prop('disabled', false);
            var resultado = parseFloat(data.saldo) - parseFloat(data.mes_compromiso);
            // When the response to the AJAX request comes back render the chart with new data
            $("#mes_grafica").html("<p>"+data.mes+"</p>");
            chart.setData(
                [
                    { y: "Saldo del Mes", a: data.saldo },
                    { y: "Comprometido del Mes", a: data.mes_compromiso },
                    { y: "Por Ejercer", a: resultado }
                ]);
        })
        .fail(function(e) {
            // If there is no communication between the server, show an error
            console.log(e.responseText);
            //alert( "error occured" );
        });
});

$("#input_nivel3").keyup(function() {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/egresos/obtener_datosUltimoNivel",
        data: {
            fecha: $("#fecha_solicitud").val(),
            nivel1: $("#input_nivel1").val(),
            nivel2: $("#input_nivel2").val(),
            nivel3: $("#input_nivel3").val(),
            nivel4: $("#input_nivel4").val(),
            nivel5: $("#input_nivel5").val(),
            nivel6: $("#input_nivel6").val()
        }
    })
        .done(function( data ) {
            $("#elegir_ultimoNivel").prop('disabled', false);
            var resultado = parseFloat(data.saldo) - parseFloat(data.mes_compromiso);
            // When the response to the AJAX request comes back render the chart with new data
            $("#mes_grafica").html("<p>"+data.mes+"</p>");
            chart.setData(
                [
                    { y: "Saldo del Mes", a: data.saldo },
                    { y: "Comprometido del Mes", a: data.mes_compromiso },
                    { y: "Por Ejercer", a: resultado }
                ]);
        })
        .fail(function(e) {
            // If there is no communication between the server, show an error
            console.log(e.responseText);
            //alert( "error occured" );
        });
});

$("#input_nivel4").keyup(function() {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/egresos/obtener_datosUltimoNivel",
        data: {
            fecha: $("#fecha_solicitud").val(),
            nivel1: $("#input_nivel1").val(),
            nivel2: $("#input_nivel2").val(),
            nivel3: $("#input_nivel3").val(),
            nivel4: $("#input_nivel4").val(),
            nivel5: $("#input_nivel5").val(),
            nivel6: $("#input_nivel6").val()
        }
    })
        .done(function( data ) {
            $("#elegir_ultimoNivel").prop('disabled', false);
            var resultado = parseFloat(data.saldo) - parseFloat(data.mes_compromiso);
            // When the response to the AJAX request comes back render the chart with new data
            $("#mes_grafica").html("<p>"+data.mes+"</p>");
            chart.setData(
                [
                    { y: "Saldo del Mes", a: data.saldo },
                    { y: "Comprometido del Mes", a: data.mes_compromiso },
                    { y: "Por Ejercer", a: resultado }
                ]);
        })
        .fail(function(e) {
            // If there is no communication between the server, show an error
            console.log(e.responseText);
            //alert( "error occured" );
        });
});

$("#input_nivel5").keyup(function() {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/egresos/obtener_datosUltimoNivel",
        data: {
            fecha: $("#fecha_solicitud").val(),
            nivel1: $("#input_nivel1").val(),
            nivel2: $("#input_nivel2").val(),
            nivel3: $("#input_nivel3").val(),
            nivel4: $("#input_nivel4").val(),
            nivel5: $("#input_nivel5").val(),
            nivel6: $("#input_nivel6").val()
        }
    })
        .done(function( data ) {
            $("#elegir_ultimoNivel").prop('disabled', false);
            var resultado = parseFloat(data.saldo) - parseFloat(data.mes_compromiso);
            // When the response to the AJAX request comes back render the chart with new data
            $("#mes_grafica").html("<p>"+data.mes+"</p>");
            chart.setData(
                [
                    { y: "Saldo del Mes", a: data.saldo },
                    { y: "Comprometido del Mes", a: data.mes_compromiso },
                    { y: "Por Ejercer", a: resultado }
                ]);
        })
        .fail(function(e) {
            // If there is no communication between the server, show an error
            console.log(e.responseText);
            //alert( "error occured" );
        });
});

$("#input_nivel6").keyup(function() {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/egresos/obtener_datosUltimoNivel",
        data: {
            fecha: $("#fecha_solicitud").val(),
            nivel1: $("#input_nivel1").val(),
            nivel2: $("#input_nivel2").val(),
            nivel3: $("#input_nivel3").val(),
            nivel4: $("#input_nivel4").val(),
            nivel5: $("#input_nivel5").val(),
            nivel6: $("#input_nivel6").val()
        }
    })
        .done(function( data ) {
            $("#elegir_ultimoNivel").prop('disabled', false);
            var resultado = parseFloat(data.saldo) - parseFloat(data.mes_compromiso);
            // When the response to the AJAX request comes back render the chart with new data
            $("#mes_grafica").html("<p>"+data.mes+"</p>");
            chart.setData(
                [
                    { y: "Saldo del Mes", a: data.saldo },
                    { y: "Comprometido del Mes", a: data.mes_compromiso },
                    { y: "Por Ejercer", a: resultado }
                ]);
        })
        .fail(function(e) {
            // If there is no communication between the server, show an error
            console.log(e.responseText);
            //alert( "error occured" );
        });
});

$(document).ready(function() {

    $('#tabla_contrarecibo').dataTable({
        "ajax": "tabla_contrarecibo_nota_entrada",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });

    $('#tabla_proveedores').dataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "/ciclo/tabla_proveedores_AJAX",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs": [ {
            "targets":  0 ,
            "visible": false,
            "searchable": false
        }, {
            "targets": -1,
            "data": null,
            "defaultContent":
                '<a data-dismiss="modal"><i class="fa fa-check"></i></a>'
        } ]
    });

    $('#tabla_cuenta_cargo').dataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": "/ciclo/tabla_cuentas_contables",
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs": [ {
            "targets": -1,
            "data": null,
            "defaultContent":
                '<a data-dismiss="modal"><i class="fa fa-check"></i></a>'
        } ]
    } );

    $('#tabla_cuenta_abono').dataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": "/ciclo/tabla_cuentas_contables",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs": [ {
            "targets": -1,
            "data": null,
            "defaultContent":
                '<a data-dismiss="modal"><i class="fa fa-check"></i></a>'
        } ]
    } );

    $('#tabla_gastos').dataTable({
        "ajax": "/ciclo/tabla_gasto",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });

    $('#tabla_detalle').dataTable({
        "ajax": "tabla_detalle_nota_entrada",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs": [ {
            "targets":  0 ,
            "visible": false,
            "searchable": false
        }]
    });

    $("#fecha").val(curr_year + "-" + curr_month + "-" + curr_date);

    $("#btn_detalle").click(function(){
        $("#detalle").toggle();
    });
});

function refrescarDetalle() {
    $.ajax({
        url: '/patrimonio/tabla_detalle_nota_entrada',
        dataType: 'json',
        method: 'POST',
        data: {
            nota: $('#ultima_nota').val()
        },
        success: function(s){
            $('#tabla_detalle').dataTable().fnClearTable();
            var total = 0;
            for(var i = 0; i < s.length; i++) {
                total += parseFloat(s[i][12]);
                $('#tabla_detalle').dataTable().fnAddData([
                    s[i][0],
                    s[i][1],
                    s[i][2],
                    s[i][3],
                    s[i][4],
                    s[i][5],
                    s[i][6],
                    s[i][7],
                    s[i][8],
                    s[i][9],
                    "$"+$.number( s[i][10], 2 ),
                    "$"+$.number( s[i][11], 2 ),
                    "$"+$.number( s[i][12], 2 ),
                    s[i][13],
                    s[i][14],
                    s[i][15]
                ]);
            } // End For

            $("#suma_total").html("Total"+ "<span style='color:#848484;'>" + " $ "+$.number( total, 2 )+ "</span>");
            $('#importe_total').val(total);
        },
        error: function(e){
            console.log(e.responseText);
        }
    });
}

/**
 * Sección de búsqueda de la estructura
 **/
$("#forma_nivel1").click(function(){
    $("#buscar_nivel3").prop('disabled', true);
    $( "#select_nivel3" ).html( '' );
    $("#buscar_nivel4").prop('disabled', true);
    $( "#select_nivel4" ).html( '' );
    $("#buscar_nivel5").prop('disabled', true);
    $( "#select_nivel5" ).html( '' );
    $("#buscar_nivel6").prop('disabled', true);
    $( "#select_nivel6" ).html( '' );

    $('#forma_nivel1 option:selected').each(function(){
        nivel1 = $(this).val();
        $.ajax( {
                url: "/egresos/obtener_nivel2",
                type: "POST",
                data: { nivel1: nivel1 },
                success:function(result) {
                    $("#input_nivel1").val(nivel1);
                    $("#buscar_nivel2").prop('disabled', false);
                    $( "#select_nivel2" ).html( '' );
                    $( "#select_nivel2" ).append( result );
                }
            }
        );
    });
});

$("#buscar_nivel1").keyup(function(){
    nivel_buscar1 = $('#buscar_nivel1').val();
    $.ajax( {
            url: "/egresos/buscar_nivel1",
            type: "POST",
            data: { nivel_buscar1: nivel_buscar1  },
            success:function(result) {
                $( "#select_nivel1" ).html( '' );
                $( "#select_nivel1" ).append( result );
            }
        }
    );
});

$("#forma_nivel2").click(function(){

    $("#buscar_nivel4").prop('disabled', true);
    $( "#select_nivel4" ).html( '' );
    $("#buscar_nivel5").prop('disabled', true);
    $( "#select_nivel5" ).html( '' );
    $("#buscar_nivel6").prop('disabled', true);
    $( "#select_nivel6" ).html( '' );

    $('#forma_nivel2 option:selected').each(function(){
        nivel2 = $(this).val();
        $.ajax( {
                url: "/egresos/obtener_nivel3",
                type: "POST",
                data: { nivel1: nivel1, nivel2: nivel2 },
                success: function(result) {
                    $("#input_nivel2").val(nivel2);
                    $("#buscar_nivel3").prop('disabled', false);
                    $( "#select_nivel3" ).html( '' );
                    $( "#select_nivel3" ).append( result );
                }
            }
        );
    });
});

$("#buscar_nivel2").keyup(function(){
    nivel_buscar2 = $('#buscar_nivel2').val();
    $.ajax( {
            url: "/egresos/buscar_nivel2",
            type: "POST",
            data: {
                nivel_buscar1: nivel1,
                nivel_buscar2: nivel_buscar2
            },
            success:function(result) {
                $( "#select_nivel2" ).html( '' );
                $( "#select_nivel2" ).append( result );
            }
        }
    );
});

$("#forma_nivel3").click(function(){

    $("#buscar_nivel5").prop('disabled', true);
    $( "#select_nivel5" ).html( '' );
    $("#buscar_nivel6").prop('disabled', true);
    $( "#select_nivel6" ).html( '' );

    $('#forma_nivel3 option:selected').each(function(){
        nivel3 = $(this).val();
        $.ajax( {
                url: "/egresos/obtener_nivel4",
                type: "POST",
                data: {
                    nivel1: nivel1,
                    nivel2: nivel2,
                    nivel3: nivel3
                },
                success: function(result) {
                    $("#input_nivel3").val(nivel3);
                    $("#buscar_nivel4").prop('disabled', false);
                    $( "#select_nivel4" ).html( '' );
                    $( "#select_nivel4" ).append( result );
                }
            }
        );
    });
});

$("#buscar_nivel3").keyup(function(){
    nivel_buscar3 = $('#buscar_nivel3').val();
    $.ajax( {
            url: "/egresos/buscar_nivel3",
            type: "POST",
            data: {
                nivel_buscar1: nivel1,
                nivel_buscar2: nivel2,
                nivel_buscar3: nivel_buscar3
            },
            success:function(result) {
                $( "#select_nivel3" ).html( '' );
                $( "#select_nivel3" ).append( result );
            }
        }
    );
});

$("#forma_nivel4").click(function(){
    $("#buscar__nivel6").prop('disabled', true);
    $( "#select__nivel6" ).html( '' );

    $('#forma_nivel4 option:selected').each(function(){
        nivel4 = $(this).val();
        $.ajax( {
                url: "/egresos/obtener_nivel5",
                type: "POST",
                data: {
                    nivel1: nivel1,
                    nivel2: nivel2,
                    nivel3: nivel3,
                    nivel4: nivel4
                },
                success: function(result) {
                    $("#input_nivel4").val(nivel4);
                    $("#buscar_nivel5").prop('disabled', false);
                    $( "#select_nivel5" ).html( '' );
                    $( "#select_nivel5" ).append( result );
                }
            }
        );
    });
});

$("#buscar_nivel4").keyup(function(){
    nivel_buscar4 = $('#buscar_nivel4').val();
    $.ajax( {
            url: "/egresos/buscar_nivel4",
            type: "POST",
            data: {
                nivel_buscar1: nivel1,
                nivel_buscar2: nivel2,
                nivel_buscar3: nivel3,
                nivel_buscar4: nivel_buscar4
            },
            success:function(result) {
                $( "#select_nivel4" ).html( '' );
                $( "#select_nivel4" ).append( result );
            }
        }
    );
});

$("#forma_nivel5").click(function(){
    $('#forma_nivel5 option:selected').each(function(){
        nivel5 = $(this).val();
        $.ajax( {
                url: "/egresos/obtener_nivel6",
                type: "POST",
                data: {
                    nivel1: nivel1,
                    nivel2: nivel2,
                    nivel3: nivel3,
                    nivel4: nivel4,
                    nivel5: nivel5
                },
                success: function(result) {
                    $("#input_nivel5").val(nivel5);
                    $("#buscar_nivel6").prop('disabled', false);
                    $( "#select_nivel6" ).html( '' );
                    $( "#select_nivel6" ).append( result );
                }
            }
        );
    });
});

$("#buscar_nivel5").keyup(function(){
    nivel_buscar5 = $('#buscar_nivel5').val();
    $.ajax( {
            url: "/egresos/buscar_nivel5",
            type: "POST",
            data: {
                nivel_buscar1: nivel1,
                nivel_buscar2: nivel2,
                nivel_buscar3: nivel3,
                nivel_buscar4: nivel4,
                nivel_buscar5: nivel_buscar5
            },
            success:function(result) {
                $( "#select_nivel5" ).html( '' );
                $( "#select_nivel5" ).append( result );
            }
        }
    );
});

//Esta funcion es para obtener los datos del ultimo nivel
$("#forma_nivel6").click(function(){
    $('#forma_nivel6 option:selected').each(function(){
        nivel6 = $(this).val();
        var d = new Date();
        var curr_date = d.getDate();
        var curr_month = d.getMonth() + 1;
        var curr_year = d.getFullYear();
        var fecha = curr_year + "-" + curr_month + "-" + curr_date;
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "/egresos/obtener_datosUltimoNivel",
            data: {
                fecha: fecha,
                nivel1: nivel1,
                nivel2: nivel2,
                nivel3: nivel3,
                nivel4: nivel4,
                nivel5: nivel5,
                nivel6: nivel6
            }
        })
            .done(function( data ) {
                $("#input_nivel6").val(nivel6);
                $("#elegir_ultimoNivel").prop('disabled', false);
                // When the response to the AJAX request comes back render the chart with new data
                $("#mes_grafica").html("<p>"+data.mes+"</p>");
                chart.setData(
                    [
                        { y: "Presupuesto Total", a: data.total_anual },
                        { y: "Saldo del Mes", a: data.saldo },
                        { y: "Precomprometido del Mes", a: data.mes_precompromiso },
                        { y: "Comprometido del Mes", a: data.mes_compromiso }
                    ]);
            })
            .fail(function(e) {
                // If there is no communication between the server, show an error
                alert( "error occured" );
            });
    });

});

$("#buscar_nivel6").keyup(function(){
    nivel_buscar6 = $('#buscar_nivel6').val();
    $.ajax( {
            url: "/egresos/buscar_nivel6",
            type: "POST",
            data: {
                nivel_buscar1: nivel1,
                nivel_buscar2: nivel2,
                nivel_buscar3: nivel3,
                nivel_buscar4: nivel4,
                nivel_buscar5: nivel5,
                nivel_buscar6: nivel_buscar6
            },
            success:function(result) {
                $( "#select_nivel6" ).html( '' );
                $( "#select_nivel6" ).append( result );
            }
        }
    );
});

$("#elegir_editar_nota_detalle").on("click", function() {

    var map = {};
    $(":input").each(function() {
        map[$(this).attr("name")] = $(this).val();
    });

   $.ajax({
       type: "POST",
       dataType: "json",
       url: "/patrimonio/editar_detalle_nota_entrada",
       data: map,
       success: function(s) {
            $('#resultado_insertar_detalle').html(s.mensaje);
            refrescarDetalle();
        }, 
        error: function(e){
            console.log(e.responseText);
        }
    });

});