$(document).ready(function() {

    $('#tabla_detalle').dataTable({
        "ajax": "../tabla_detalle_movimiento",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs": [ {
            "targets": [ 0, -1 ],
            "visible": false,
            "searchable": false
        } ]
    });

    refrescarDetalle();

});

function refrescarDetalle() {
    $.ajax({
        url: '/ciclo/tabla_detalle_movimiento',
        dataType: 'json',
        method: 'POST',
        data: {
            movimiento: $('#movimiento').val()
        },
        success: function(s){
            $('#tabla_detalle').dataTable().fnClearTable();
            var total = 0;
            for(var i = 0; i < s.length; i++) {
                total += parseFloat(s[i][3]);
                $('#tabla_detalle').dataTable().fnAddData([
                    s[i][0],
                    s[i][1],
                    s[i][2],
                    "$"+$.number( s[i][3], 2 ),
                    s[i][4]
                ]);
            } // End For
        },
        error: function(e){
            console.log(e.responseText);
        }
    });
}