/**
 * Created by Lizbeth on 06/03/2015.
 */
var datos_tabla;

//Fecha
var d = new Date();
var curr_date = d.getDate();
var curr_month = d.getMonth() + 1; //Months are zero based
var curr_year = d.getFullYear();

//Esta funcion es para traducir el datepicker
$(function($){
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);
});

//Se crean las cajas para la selección de fecha
$( "#fecha_solicitud" ).datepicker({ dateFormat: 'yy-mm-dd' });
$( "#fecha_inicial" ).datepicker({ dateFormat: 'yy-mm-dd' });
$( "#fecha_final" ).datepicker({ dateFormat: 'yy-mm-dd' });

$(document).ready(function() {
    $('.datos_tabla').dataTable({
        "ajax": "/recaudacion/tabla_indice_devengado",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs": [ {
            "targets": [ 0 ],
            "visible": false,
            "searchable": false
        } ]
    });

    $('#tabla_centro_recaudacion').dataTable({
        "ajax": "/recaudacion/tabla_centro_recaudacion",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });

    $('#tabla_cliente').dataTable({
        "ajax": "/recaudacion/tabla_cliente",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });

});

$('.datos_tabla tbody').on( 'click', 'tr', function () {
    datos_tabla = $('.datos_tabla').DataTable().row( this ).data();
});

$('.modal_borrar').on('show.bs.modal', function (event) {
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this);
    modal.find('#cancelar_devengado').val(datos_tabla[1]);
});

$('.modal_recaudar').on('show.bs.modal', function (event) {
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this);
    modal.find('#id_recaudar_hidden').val(datos_tabla[1]);
});

$('.modal_poliza').on('show.bs.modal', function (event) {
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this);
    modal.find('#numero_devengado_poliza').html(''+datos_tabla[1]);
    modal.find('#numero_devengado_poliza_hidden').val(datos_tabla[1]);
});

$('#tabla_centro_recaudacion tbody').on( 'click', 'tr', function () {
    datos_centro_costo = $('#tabla_centro_recaudacion').DataTable().row( this ).data();
});

$('#modal_centro_recaudacion').on('hidden.bs.modal', function () {
    $('#centro_recaudacion').val(datos_centro_costo[0]);
});

$('#tabla_cliente tbody').on( 'click', 'tr', function () {
    datos_cliente = $('#tabla_cliente').DataTable().row( this ).data();
});

$('#modal_cliente').on('hidden.bs.modal', function () {
    $('#cliente').val(datos_cliente[1]);
});

$("#elegir_cancelar_devengado").click(function() {
    var devengado = $("#cancelar_devengado").val();

    var table = $('.datos_tabla').DataTable();

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/recaudacion/cancelar_devengado_caratula",
        data: {
            devengado: devengado
        }
    })
        .done(function( data ) {
            if(data.mensaje == "ok") {
                table.ajax.reload();
            }
            else {
                alert("Ha ocurrido un error");
            }
        })
        .fail(function(e) {
            // If there is no communication between the server, show an error
            console.log("Error");
            //alert( "error occured" );
        });
});

$("#elegir_recaudar").click(function() {
    var devengado = $("#id_recaudar_hidden").val();
    var fecha = $("#fecha_solicitud").val();
    var table = $('.datos_tabla').DataTable();

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/recaudacion/recaudar_devengado",
        data: {
            devengado: devengado,
            fecha: fecha
        }
    })
        .done(function( data ) {
            $("#mensaje_resultado_recaudado").html(data.mensaje);
            $('.resultado_recaudado').modal('show');
            table.ajax.reload();
        })
        .fail(function(e) {
            console.log(e.responseText);
        });
});

$("#elegir_generar_poliza").click(function() {
    var devengado = $("#numero_devengado_poliza_hidden").val();

    var table = $('.datos_tabla').DataTable();

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/recaudacion/generar_poliza_devengado",
        data: {
            devengado: devengado
        }
    })
        .done(function( data ) {
            $("#mensaje_resultado_poliza").html(data.mensaje);
            $('.resultado_poliza').modal('show');
            table.ajax.reload();
        })
        .fail(function(e) {
            console.log(e.responseText);
        });
});