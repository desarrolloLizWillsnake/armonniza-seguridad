/**
 * Created by Lizbeth on 01/12/2015.
 */
$('#descargar_plantilla_ingresos').click(function(e) {
    e.preventDefault();  //stop the browser from following
    window.location.href = '../Estructuras_Iniciales/Ingresos/Estructura_Ingresos_Mayo_2015.xls';
});

$('#descargar_plantilla_egresos').click(function(e) {
    e.preventDefault();  //stop the browser from following
    window.location.href = '../Estructuras_Iniciales/Egresos/Estructura_egreso_Educal_2015_mayo_2015.xls';
});


$("#actualizar_info").on( "click", function(e) {
    e.preventDefault();

    var map = {};
    $(":input").each(function() {
        map[$(this).attr("name")] = $(this).val();
    });

    $.ajax({
        url: '/administrador/insertar_datos_estructura',
        method: 'POST',
        dataType: 'json',
        data: map,
        success: function(s){
            $("#resultado_actualizacion").html(s.mensaje);
        },
        error: function(e){
            console.log(e.responseText);
        }
    });
});