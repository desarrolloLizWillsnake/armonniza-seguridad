$(document).ready(function() {

    $('#tabla_datos_transferencia').dataTable({
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs": [ {
            "targets": [ 0 ],
            "visible": false,
            "searchable": false
        } ]
    });

    refrescarDetalle();

});

function refrescarDetalle() {
    $.ajax({
        url: '/ingresos/tabla_detalle_transferencia',
        dataType: 'json',
        method: 'POST',
        data: {
            transferencia: $("#ultimo").val()
        },
        success: function(s){
            $('#tabla_datos_transferencia').dataTable().fnClearTable();
            var total = 0;
            for(var i = 0; i < s.length; i++) {
                total += parseFloat(s[i][6]);
                $('#tabla_datos_transferencia').dataTable().fnAddData([
                    s[i][0],
                    s[i][1],
                    s[i][2],
                    s[i][3],
                    s[i][4],
                    s[i][5],
                    "$"+$.number( s[i][6], 2 ),
                    s[i][7],
                    s[i][8],
                    s[i][9],
                    s[i][10]
                ]);
            } // End For

            $("#total_hidden").val(total);
        },
        error: function(e){
            console.log(e.responseText);
        }
    });
}