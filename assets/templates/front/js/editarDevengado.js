/**
 * Created by Lizbeth on 21/04/2015.
 */
var datos_tabla_subsidio;
var datos_tabla_devengado;
var nivel1 = $("#input_nivel1").val();
var nivel2 = $("#input_nivel2").val();
var nivel3 = $("#input_nivel3").val();
var nivel4 = $("#input_nivel4").val();
var nivel5 = $("#input_nivel5").val();

//Fecha
var d = new Date();
var curr_date = d.getDate();
var curr_month = d.getMonth() + 1; //Months are zero based
var curr_year = d.getFullYear();

//Esta funcion es para traducir el datepicker
$(function($){
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);
});

//Se crean las cajas para la selección de fecha
$( "#fecha_solicitud" ).datepicker({ dateFormat: 'yy-mm-dd', minDate: '2015-01-01' });
$( "#fecha_aplicacion" ).datepicker({ dateFormat: 'yy-mm-dd', minDate: '2015-01-01' });
$( "#fecha_detalle" ).datepicker({ dateFormat: 'yy-mm-dd', minDate: '2015-01-01' });

$(document).ready(function() {

    $('#tabla_datos_devengado').dataTable({
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs": [ {
            "targets": [ 0 ],
            "visible": false,
            "searchable": false
        } ]
    });
    $('#tabla_subsidio').dataTable({
        "ajax": "../tabla_subsidio",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });
    $('#tabla_librerias').dataTable({
        "ajax": "../tabla_librerias",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });

    $("#fecha_solicitud").val(curr_year + "-" + curr_month + "-" + curr_date);
    $("#fecha_detalle").val(curr_year + "-" + curr_month + "-" + curr_date);

   refrescarDetalle();
});

//Tabla de Detalle Devengado
$('#tabla_datos_devengado tbody').on( 'click', 'tr', function () {
    datos_tabla_devengado = $('#tabla_datos_devengado').DataTable().row( this ).data();
    $("#borrar_devengado_hidden").val(datos_tabla_devengado[0]);
    console.log($("#borrar_devengado_hidden").val());
});

//Tabla de Subsidio en Modal
$('#tabla_subsidio tbody').on( 'click', 'tr', function () {
    datos_tabla_subsidio = $('#tabla_subsidio').DataTable().row( this ).data();
});
$('#modal_subsidio').on('hidden.bs.modal', function () {
    $("#subsidio").val(datos_tabla_subsidio[1]);
});

//Tabla de Librerías en Modal
$('#tabla_librerias tbody').on( 'click', 'tr', function () {
    datos_tabla_librerias = $('#tabla_librerias').DataTable().row( this ).data();
});
$('#modal_librerias').on('hidden.bs.modal', function () {
    $("#clave_libreria").val(datos_tabla_librerias[1]);
});

$("#guardar_devengado").on( "click", function(e) {
    e.preventDefault();

    var map = {};
    $(":input").each(function() {
        map[$(this).attr("name")] = $(this).val();
    });

    if($('#check_firme').is(':checked')){
        map.check_firme = 1;
    } else {
        map.check_firme = 0;
    }

    $.ajax({
        url: '/recaudacion/insertar_devengado',
        method: 'POST',
        dataType: 'json',
        data: map,
        success: function(s){
            $("#resultado_insertar_caratula").html(s.mensaje);
            $("#guardar_devengado").prop('disabled', true);
        },
        error: function(e){
            console.log(e.responseText);
        }
    });
});
//Acción guardar Devengado apartado Detalle
$("#guardar_devengado_detalle").on( "click", function() {

    $(".forma_devengado").validate({
        focusInvalid: true,
        rules: {
            clasificacion: {
                required: true
            },
            importe_total: {
                required: true,
                number: true
            },
            num_movimiento:{
                required: true
            },
            fecha_solicitud: {
                required: true
            },
            fecha_aplicacion: {
                required: true
            },
            descripcion: {
                required: true
            },
            input_nivel1: {
                required: true
            },
            input_nivel2: {
                required: true
            },
            input_nivel3: {
                required: true
            },
            input_nivel4: {
                required: true
            },
            fecha_detalle: {
                required: true
            },
            subsidio: {
                required: true
            },
            importe_grabado: {
                required: true,
                number: true
            },
            importe_no_grabado: {
                required: true,
                number: true
            },
            //iva: {
            //    required: true,
            //    number: true
            //},
            descripcion_detalle: {
                required: true
            }
        },
        //"* Este campo es obligatorio"
        //"* Este campo debe de ser un número positivo"
        //"Formato Correcto AAAA-MM-DD"
        messages: {
            clasificacion: {
                required: "* Este campo es obligatorio"
            },
            importe_total: {
                required: "* Este campo es obligatorio",
                number: "* Este campo debe de ser un número positivo"
            },
            num_movimiento: {
                required: "* Este campo es obligatorio"
            },
            fecha_solicitud: {
                required: "* Este campo es obligatorio",
                date: "Formato Correcto AAAA-MM-DD"
            },
            fecha_aplicacion: {
                required: "* Este campo es obligatorio",
                date: "Formato Correcto AAAA-MM-DD"
            },
            descripcion: {
                required: "* Este campo es obligatorio"
            },
            input_nivel1: {
                required: "* Este campo es obligatorio"
            },
            input_nivel2: {
                required: "* Este campo es obligatorio"
            },
            input_nivel3: {
                required: "* Este campo es obligatorio"
            },
            input_nivel4: {
                required: "* Este campo es obligatorio"
            },
            fecha_detalle: {
                required: "* Este campo es obligatorio",
                date: "Formato Correcto AAAA-MM-DD"
            },
            subsidio: {
                required: "* Este campo es obligatorio"
            },
            importe_grabado: {
                required: "* Este campo es obligatorio",
                number: "* Este campo debe de ser un número positivo"
            },
            importe_no_grabado: {
                required: "* Este campo es obligatorio",
                number: "* Este campo debe de ser un número positivo"
            },
            //iva: {
            //    required: "* Este campo es obligatorio",
            //    number: "* Este campo debe de ser un número positivo"
            //},
            descripcion_detalle: {
                required: "* Este campo es obligatorio"
            }
        }
    }).form();

    if($(".forma_devengado").valid()){
        var map = {};
        $(":input").each(function() {
            map[$(this).attr("name")] = $(this).val();
        });

        if($('#check_firme').is(':checked')){
            map.check_firme = 1;
        } else {
            map.check_firme = 0;
        }

        map.descripcion_detalle = $("#descripcion_detalle").val();

        map.nivel1 = $("#input_nivel1").val();
        map.nivel2 = $("#input_nivel2").val();
        map.nivel3 = $("#input_nivel3").val();
        map.nivel4 = $("#input_nivel4").val();
        map.nivel5 = $("#input_nivel5").val();

        //console.log(map);

        $.ajax({
            url: '/recaudacion/insertar_detalle_devengado',
            method: 'POST',
            dataType: 'json',
            data: map,
            success: function(s){
                if(s.estatus == "ok") {
                    $("#resultado_insertar_detalle").html(s.mensaje);
                    $("#input_nivel1").val('');
                    $("#input_nivel2").val('');
                    $("#input_nivel3").val('');
                    $("#input_nivel4").val('');
                    $("#input_nivel5").val('');
                    $("#fecha_detalle").val('');
                    $("#subsidio").val('');
                    $("#iva").val('');
                    $("#total").val('');
                    $("#descripcion_detalle").val('');
                    refrescarDetalle();

                }
                else {
                    $("#resultado_insertar_detalle").html(s.mensaje);
                    refrescarDetalle();
                }
            },
            error: function(e){
                console.log(e.responseText);
            }
        });
    }

});

$("#elegir_cancelar_devengado").click(function() {
    var id_devengado = $("#borrar_devengado_hidden").val();

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/recaudacion/borrar_detalle_devengado",
        data: {
            id_deven: id_devengado
        }
    })
        .done(function( data ) {
            console.log(data);
            if(data.mensaje == "ok") {
                refrescarDetalle();
            }
            else {
                alert("Ha ocurrido un error");
            }
        })
        .fail(function(e) {
            console.log(e.responseText);
            // If there is no communication between the server, show an error
            console.log("Error");
            //alert( "error occured" );
        });
});
$(".borrar_devengado_detalle").on( "click", function () {
    console.log($("#tabla_datos_devengado").val());
});


$("#datos_impresion").submit( function(eventObj){
    var input1 = $("#input_nivel1").val();
    var input2 = $("#input_nivel2").val();
    var input3 = $("#input_nivel3").val();
    var input4 = $("#input_nivel4").val();

    $('<input />').attr('type', 'hidden')
        .attr('name', "nivel1")
        .attr('value', input1)
        .appendTo('#datos_impresion');

    $('<input />').attr('type', 'hidden')
        .attr('name', "nivel2")
        .attr('value', input2)
        .appendTo('#datos_impresion');

    $('<input />').attr('type', 'hidden')
        .attr('name', "nivel3")
        .attr('value', input3)
        .appendTo('#datos_impresion');

    $('<input />').attr('type', 'hidden')
        .attr('name', "nivel4")
        .attr('value', input4)
        .appendTo('#datos_impresion');

    return true;
});

$("#forma_nivel1").click(function(){
    $("#buscar_nivel3").prop('disabled', true);
    $( "#select_nivel3" ).html( '' );
    $("#buscar_nivel4").prop('disabled', true);
    $( "#select_nivel4" ).html( '' );
    $("#buscar_nivel5").prop('disabled', true);
    $( "#select_nivel5" ).html( '' );

    $('#forma_nivel1 option:selected').each(function(){
        nivel1 = $(this).val();
        $.ajax( {
                url: "/ingresos/obtener_nivel2",
                type: "POST",
                data: { nivel1: nivel1 },
                success:function(result) {
                    $("#input_nivel1").val(nivel1);
                    $("#buscar_nivel2").prop('disabled', false);
                    $( "#select_nivel2" ).html( '' );
                    $( "#select_nivel2" ).append( result );
                }
            }
        );
    });
});

$("#buscar_nivel1").keyup(function(){
    nivel_buscar1 = $('#buscar_nivel1').val();
    $.ajax( {
            url: "/ingresos/buscar_nivel1",
            type: "POST",
            data: { nivel_buscar1: nivel_buscar1  },
            success:function(result) {
                $( "#select_nivel1" ).html( '' );
                $( "#select_nivel1" ).append( result );
            }
        }
    );
});

$("#forma_nivel2").click(function(){

    $("#buscar_nivel4").prop('disabled', true);
    $( "#select_nivel4" ).html( '' );
    $("#buscar_nivel5").prop('disabled', true);
    $( "#select_nivel5" ).html( '' );

    $('#forma_nivel2 option:selected').each(function(){
        nivel2 = $(this).val();
        $.ajax( {
                url: "/ingresos/obtener_nivel3",
                type: "POST",
                data: { nivel1: nivel1, nivel2: nivel2 },
                success: function(result) {
                    $("#input_nivel2").val(nivel2);
                    $("#buscar_nivel3").prop('disabled', false);
                    $( "#select_nivel3" ).html( '' );
                    $( "#select_nivel3" ).append( result );
                }
            }
        );
    });
});

$("#buscar_nivel2").keyup(function(){
    nivel_buscar2 = $('#buscar_nivel2').val();
    $.ajax( {
            url: "/ingresos/buscar_nivel2",
            type: "POST",
            data: {
                nivel_buscar1: nivel1,
                nivel_buscar2: nivel_buscar2
            },
            success:function(result) {
                $( "#select_nivel2" ).html( '' );
                $( "#select_nivel2" ).append( result );
            }
        }
    );
});

$("#forma_nivel3").click(function(){

    $("#buscar_nivel5").prop('disabled', true);
    $( "#select_nivel5" ).html( '' );

    $('#forma_nivel3 option:selected').each(function(){
        nivel3 = $(this).val();
        $.ajax( {
                url: "/ingresos/obtener_nivel4",
                type: "POST",
                data: {
                    nivel1: nivel1,
                    nivel2: nivel2,
                    nivel3: nivel3
                },
                success: function(result) {
                    $("#input_nivel3").val(nivel3);
                    $("#buscar_nivel4").prop('disabled', false);
                    $( "#select_nivel4" ).html( '' );
                    $( "#select_nivel4" ).append( result );
                }
            }
        );
    });
});

$("#buscar_nivel3").keyup(function(){
    nivel_buscar3 = $('#buscar_nivel3').val();
    $.ajax( {
            url: "/ingresos/buscar_nivel3",
            type: "POST",
            data: {
                nivel_buscar1: nivel1,
                nivel_buscar2: nivel2,
                nivel_buscar3: nivel_buscar3
            },
            success:function(result) {
                $( "#select_nivel3" ).html( '' );
                $( "#select_nivel3" ).append( result );
            }
        }
    );
});

$("#forma_nivel4").click(function(){

    $('#forma_nivel4 option:selected').each(function(){
        nivel4 = $(this).val();
        $.ajax( {
                url: "/ingresos/obtener_nivel5",
                type: "POST",
                data: {
                    nivel1: nivel1,
                    nivel2: nivel2,
                    nivel3: nivel3,
                    nivel4: nivel4
                },
                success: function(result) {
                    $("#input_nivel4").val(nivel4);
                    $("#buscar_nivel4").prop('disabled', false);
                    $( "#select_nivel5" ).html( '' );
                    $( "#select_nivel5" ).append( result );
                }
            }
        );
    });
});

$("#buscar_nivel4").keyup(function(){
    nivel_buscar4 = $('#buscar_nivel4').val();
    $.ajax( {
            url: "/ingresos/buscar_nivel4",
            type: "POST",
            data: {
                nivel_buscar1: nivel1,
                nivel_buscar2: nivel2,
                nivel_buscar3: nivel3,
                nivel_buscar4: nivel_buscar4
            },
            success:function(result) {
                $( "#select_nivel4" ).html( '' );
                $( "#select_nivel4" ).append( result );
            }
        }
    );
});

$("#forma_nivel5").click(function(){

    $('#forma_nivel5 option:selected').each(function(){
        nivel5 = $(this).val();
        $.ajax( {
                url: "/ingresos/obtenerDatosPartida",
                type: "POST",
                dataType: "json",
                data: {
                    nivel1: nivel1,
                    nivel2: nivel2,
                    nivel3: nivel3,
                    nivel4: nivel4,
                    nivel5: nivel5
                }
            }
        ).done(function( data ) {
                $("#elegir_ultimoNivel").prop('disabled', false);
                $("#input_nivel1").val(nivel1);
                $("#input_nivel2").val(nivel2);
                $("#input_nivel3").val(nivel3);
                $("#input_nivel4").val(nivel4);
                $("#input_nivel5").val(nivel5);
            })
            .fail(function(e) {
                // If there is no communication between the server, show an error
                alert( "error occured" );
            });
    });
});

$("#buscar_nivel5").keyup(function(){
    nivel_buscar5 = $('#buscar_nivel5').val();
    $.ajax( {
            url: "/ingresos/buscar_nivel5",
            type: "POST",
            data: {
                nivel_buscar1: nivel1,
                nivel_buscar2: nivel2,
                nivel_buscar3: nivel3,
                nivel_buscar4: nivel4,
                nivel_buscar5: nivel_buscar5
            },
            success:function(result) {
                $( "#select_nivel5" ).html( '' );
                $( "#select_nivel5" ).append( result );
            }
        }
    );
});

$("#borrar_cambios").on( "click", function() {
    var map = {};
    $(":input").each(function() {
        map[$(this).attr("name")] = $(this).val();
    });

    if($('#check_firme').is(':checked')){
        map.check_firme = 1;
    } else {
        map.check_firme = 0;
    }

    $.ajax({
        url: '/recaudacion/borrar_cambios_devengado',
        method: 'POST',
        dataType: 'json',
        data: map,
        success: function(s){
            if(s.mensaje == "ok") {
                location.href="/recaudacion/devengado";
            }
        },
        error: function(e){
            console.log(e.responseText);
        }
    });
});
$('.modal_borrar').on('show.bs.modal', function (event) {
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this);
    modal.find('#borrar_devengado_hidden').val(datos_tabla_devengado[0]);
});
$("#elegir_cancelar_devengado").click(function() {
    var id_devengado = $("#borrar_devengado_hidden").val();

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/recaudacion/borrar_detalle_devengado",
        data: {
            id_devengado: id_devengado
        }
    })
        .done(function( data ) {
            console.log(data);
            if(data.mensaje == "ok") {
                refrescarDetalle();
            }
            else {
                alert("Ha ocurrido un error");
            }
        })
        .fail(function(e) {
            console.log(e.responseText);
            // If there is no communication between the server, show an error
            console.log("Error");
            //alert( "error occured" );
        });
});

function refrescarDetalle() {
    $.ajax({
        url: '/recaudacion/tabla_detalle_devengado',
        dataType: 'json',
        method: 'POST',
        data: {
            devengado: $("#ultimo").val()
        },
        success: function(s){

            $('#tabla_datos_devengado').dataTable().fnClearTable();
            var total = 0;
            for(var i = 0; i < s.length; i++) {
                total += parseFloat(s[i][12]);
                $('#tabla_datos_devengado').dataTable().fnAddData([
                    s[i][0],
                    s[i][1],
                    s[i][2],
                    s[i][3],
                    s[i][4],
                    s[i][5],
                    s[i][6],
                    s[i][7],
                    s[i][8],
                    "<div class='table-formant-sign'>$"+"</div><div class='table-formant-coin'>"+$.number( s[i][9], 2 )+"</div>",
                    "<div class='table-formant-sign'>$"+"</div><div class='table-formant-coin'>"+$.number( s[i][10], 2 )+"</div>",
                    "<div class='table-formant-sign'>$"+"</div><div class='table-formant-coin'>"+$.number( s[i][11], 2 )+"</div>",
                    "<div class='table-formant-sign'>$"+"</div><div class='table-formant-coin'>"+$.number( s[i][12], 2 )+"</div>",
                    s[i][13]
                ]);
            } // End For

            $("#suma_total").html("Total"+ "<span style='color:#848484;'>" + " $ "+$.number( total, 2 )+ "</span>");

            $("#importe_total").val(total);

        },
        error: function(e){
            console.log(e.responseText);
        }
    });
}
