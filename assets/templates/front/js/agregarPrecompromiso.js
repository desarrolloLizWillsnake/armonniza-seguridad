var datos_tabla_gastos;
var datos_tabla_precompromiso;
var datos_tabla_viaticos_precompromiso;
var datos_proveedor;
var datos_centro_costo;
var datos_beneficiario;
var d = new Date();
var curr_date = d.getDate();
var curr_month = d.getMonth() + 1; //Months are zero based
var curr_year = d.getFullYear();
var nivel1 = $("#input_nivel1").val();
var nivel2 = $("#input_nivel2").val();
var nivel3 = $("#input_nivel3").val();
var nivel4 = $("#input_nivel4").val();
var nivel5 = $("#input_nivel5").val();
var nivel6 = $("#input_nivel6").val();

//Esta funcion es para traducir el datepicker
$(function($){
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);
});

//Se crean las cajas para la seleccion de fecha
//$( "#fecha_solicitud" ).datepicker({ dateFormat: 'yy-mm-dd', minDate: new Date() });
$( "#fecha_solicitud" ).datepicker({ dateFormat: 'yy-mm-dd', minDate: '2015-01-01' });
$( "#fecha_autoriza" ).datepicker({ dateFormat: 'yy-mm-dd', minDate: '2015-01-01' });
$( "#fecha_entrega" ).datepicker({ dateFormat: 'yy-mm-dd', minDate: '2015-01-01' });
$( "#fecha_inicio" ).datepicker({ dateFormat: 'yy-mm-dd', minDate: '2015-01-01' });
$( "#fecha_termina" ).datepicker({ dateFormat: 'yy-mm-dd', minDate: '2015-01-01' });

//Esta funcion es para tomar el valor del primer conjunto de radiobuttons
$(".forma_precompromiso input[name='tipo_radio']").change(function () {
    if ($(this).is(":checked")) {
        tipo_precompromiso = $(".forma_precompromiso input[name='tipo_radio']:checked").val();
    }
});

$( "#fecha_solicitud" ).on("change", function(){
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/egresos/obtener_datosUltimoNivel",
        data: {
            fecha: $("#fecha_solicitud").val(),
            nivel1: $("#input_nivel1").val(),
            nivel2: $("#input_nivel2").val(),
            nivel3: $("#input_nivel3").val(),
            nivel4: $("#input_nivel4").val(),
            nivel5: $("#input_nivel5").val(),
            nivel6: $("#input_nivel6").val()
        }
    })
        .done(function( data ) {
            $("#elegir_ultimoNivel").prop('disabled', false);
            var resultado = parseFloat(data.total_anual) - parseFloat(data.precomprometido_anual);
            // When the response to the AJAX request comes back render the chart with new data
            $("#mes_grafica").html("<p>"+data.mes+"</p>");
            chart.setData(
                [
                    { y: "Presupuesto Total", a: data.total_anual },
                    { y: "Precomprometido del Mes", a: data.mes_precompromiso },
                    { y: "Por Ejercer", a: resultado }
                ]);
        })
        .fail(function(e) {
            // If there is no communication between the server, show an error
            console.log("Error");
            //alert( "error occured" );
        });
});

$("#elegir_ultimoNivel").click(function() {
    $("#input_nivel1").val(nivel1);
    $("#input_nivel2").val(nivel2);
    $("#input_nivel3").val(nivel3);
    $("#input_nivel4").val(nivel4);
    $("#input_nivel5").val(nivel5);
    $("#input_nivel6").val(nivel6);
});

$("#cancelar_estructura").click(function() {
    $("#input_nivel1").val('');
    $("#input_nivel2").val('');
    $("#input_nivel3").val('');
    $("#input_nivel4").val('');
    $("#input_nivel5").val('');
    $("#input_nivel6").val('');
});

$("#input_nivel1").keyup(function() {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/egresos/obtener_datosUltimoNivel",
        data: {
            fecha: $("#fecha_solicitud").val(),
            nivel1: $("#input_nivel1").val(),
            nivel2: $("#input_nivel2").val(),
            nivel3: $("#input_nivel3").val(),
            nivel4: $("#input_nivel4").val(),
            nivel5: $("#input_nivel5").val(),
            nivel6: $("#input_nivel6").val()
        }
    })
        .done(function( data ) {
            var resultado = parseFloat(data.total_anual) - parseFloat(data.precomprometido_anual);
            // When the response to the AJAX request comes back render the chart with new data
            $("#mes_grafica").html("<p>"+data.mes+"</p>");
            chart.setData(
                [
                    { y: "Presupuesto Total", a: data.total_anual },
                    { y: "Precomprometido del Mes", a: data.mes_precompromiso },
                    { y: "Por Ejercer", a: resultado }
                ]);
        })
        .fail(function(e) {
            // If there is no communication between the server, show an error
            console.log("Error");
            //alert( "error occured" );
        });
});

$("#input_nivel2").keyup(function() {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/egresos/obtener_datosUltimoNivel",
        data: {
            fecha: $("#fecha_solicitud").val(),
            nivel1: $("#input_nivel1").val(),
            nivel2: $("#input_nivel2").val(),
            nivel3: $("#input_nivel3").val(),
            nivel4: $("#input_nivel4").val(),
            nivel5: $("#input_nivel5").val(),
            nivel6: $("#input_nivel6").val()
        }
    })
        .done(function( data ) {
            var resultado = parseFloat(data.total_anual) - parseFloat(data.precomprometido_anual);
            // When the response to the AJAX request comes back render the chart with new data
            $("#mes_grafica").html("<p>"+data.mes+"</p>");
            chart.setData(
                [
                    { y: "Presupuesto Total", a: data.total_anual },
                    { y: "Precomprometido del Mes", a: data.mes_precompromiso },
                    { y: "Por Ejercer", a: resultado }
                ]);
        })
        .fail(function(e) {
            // If there is no communication between the server, show an error
            console.log("Error");
            //alert( "error occured" );
        });
});

$("#input_nivel3").keyup(function() {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/egresos/obtener_datosUltimoNivel",
        data: {
            fecha: $("#fecha_solicitud").val(),
            nivel1: $("#input_nivel1").val(),
            nivel2: $("#input_nivel2").val(),
            nivel3: $("#input_nivel3").val(),
            nivel4: $("#input_nivel4").val(),
            nivel5: $("#input_nivel5").val(),
            nivel6: $("#input_nivel6").val()
        }
    })
        .done(function( data ) {
            var resultado = parseFloat(data.total_anual) - parseFloat(data.precomprometido_anual);
            // When the response to the AJAX request comes back render the chart with new data
            $("#mes_grafica").html("<p>"+data.mes+"</p>");
            chart.setData(
                [
                    { y: "Presupuesto Total", a: data.total_anual },
                    { y: "Precomprometido del Mes", a: data.mes_precompromiso },
                    { y: "Por Ejercer", a: resultado }
                ]);
        })
        .fail(function(e) {
            // If there is no communication between the server, show an error
            console.log("Error");
            //alert( "error occured" );
        });
});

$("#input_nivel4").keyup(function() {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/egresos/obtener_datosUltimoNivel",
        data: {
            fecha: $("#fecha_solicitud").val(),
            nivel1: $("#input_nivel1").val(),
            nivel2: $("#input_nivel2").val(),
            nivel3: $("#input_nivel3").val(),
            nivel4: $("#input_nivel4").val(),
            nivel5: $("#input_nivel5").val(),
            nivel6: $("#input_nivel6").val()
        }
    })
        .done(function( data ) {
            var resultado = parseFloat(data.total_anual) - parseFloat(data.precomprometido_anual);
            // When the response to the AJAX request comes back render the chart with new data
            $("#mes_grafica").html("<p>"+data.mes+"</p>");
            chart.setData(
                [
                    { y: "Presupuesto Total", a: data.total_anual },
                    { y: "Precomprometido del Mes", a: data.mes_precompromiso },
                    { y: "Por Ejercer", a: resultado }
                ]);
        })
        .fail(function(e) {
            // If there is no communication between the server, show an error
            console.log("Error");
            //alert( "error occured" );
        });
});

$("#input_nivel5").keyup(function() {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/egresos/obtener_datosUltimoNivel",
        data: {
            fecha: $("#fecha_solicitud").val(),
            nivel1: $("#input_nivel1").val(),
            nivel2: $("#input_nivel2").val(),
            nivel3: $("#input_nivel3").val(),
            nivel4: $("#input_nivel4").val(),
            nivel5: $("#input_nivel5").val(),
            nivel6: $("#input_nivel6").val()
        }
    })
        .done(function( data ) {
            var resultado = parseFloat(data.total_anual) - parseFloat(data.precomprometido_anual);
            // When the response to the AJAX request comes back render the chart with new data
            $("#mes_grafica").html("<p>"+data.mes+"</p>");
            chart.setData(
                [
                    { y: "Presupuesto Total", a: data.total_anual },
                    { y: "Precomprometido del Mes", a: data.mes_precompromiso },
                    { y: "Por Ejercer", a: resultado }
                ]);
        })
        .fail(function(e) {
            // If there is no communication between the server, show an error
            console.log("Error");
            //alert( "error occured" );
        });
});

$("#input_nivel6").keyup(function() {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/egresos/obtener_datosUltimoNivel",
        data: {
            fecha: $("#fecha_solicitud").val(),
            nivel1: $("#input_nivel1").val(),
            nivel2: $("#input_nivel2").val(),
            nivel3: $("#input_nivel3").val(),
            nivel4: $("#input_nivel4").val(),
            nivel5: $("#input_nivel5").val(),
            nivel6: $("#input_nivel6").val()
        }
    })
        .done(function( data ) {
            $("#elegir_ultimoNivel").prop('disabled', false);
            var resultado = parseFloat(data.total_anual) - parseFloat(data.precomprometido_anual);
            // When the response to the AJAX request comes back render the chart with new data
            $("#mes_grafica").html("<p>"+data.mes+"</p>");
            chart.setData(
                [
                    { y: "Presupuesto Total", a: data.total_anual },
                    { y: "Precomprometido del Mes", a: data.mes_precompromiso },
                    { y: "Por Ejercer", a: resultado }
                ]);
        })
        .fail(function(e) {
            // If there is no communication between the server, show an error
            console.log("Error");
            //alert( "error occured" );
        });
});

$(document).ready(function() {

    $('#tabla_gastos').dataTable({
        "ajax": "tabla_gasto",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });

    $('#tabla_datos_precompromiso').dataTable({
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs": [ {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            } ]
    });

    $('#tabla_proveedores').dataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "tabla_proveedores_AJAX",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs": [ {
            "targets":  0 ,
            "visible": false,
            "searchable": false
        }, {
            "targets": -1,
            "data": null,
            "defaultContent":
            '<a data-dismiss="modal"><i class="fa fa-check"></i></a>'
        } ]
    });

    $('#tabla_datos_viaticos_precompromiso').dataTable({
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs": [ {
            "targets":  0 ,
            "visible": false,
            "searchable": false
        }]
    });

    $('#tabla_centro_costos').dataTable({
        "ajax": "/contabilidad/tabla_centro_costos",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });

    $('#tabla_beneficiarios').dataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "tabla_beneficiarios",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs": [ {
            "targets":  0 ,
            "visible": false,
            "searchable": false
        }, {
            "targets": -1,
            "data": null,
            "defaultContent":
                '<a data-dismiss="modal"><i class="fa fa-check"></i></a>'
        } ]
    });

    $("#fecha_solicitud").val(curr_year + "-" + curr_month + "-" + curr_date);

});

$(".forma_precompromiso input[name='presupuesto']").on( "change", function() {
    if($(this).val() == "abierto") {
        $("#fecha_entrega").val(curr_year + "-12-31");
        $("#fecha_entrega").prop('disabled', true);
    }
    else {
        $("#fecha_entrega").val("");
        $("#fecha_entrega").prop('disabled', false);
    }
});

$('#tabla_datos_precompromiso tbody').on( 'click', 'tr', function () {
    datos_tabla_precompromiso = $('#tabla_datos_precompromiso').DataTable().row( this ).data();
    $("#borrar_precompromiso_hidden").val(datos_tabla_precompromiso[0]);
    $("#detalle_partida").html(datos_tabla_precompromiso[15]);
    $('#editar_cantidad_precompromiso').val(datos_tabla_precompromiso[9]);
    $('#editar_precio_precompromiso').val(datos_tabla_precompromiso[10]);
});

$('#tabla_gastos tbody').on( 'click', 'tr', function () {
    datos_tabla_gastos = $('#tabla_gastos').DataTable().row( this ).data();
});

$('#modal_gasto').on('hidden.bs.modal', function () {
    $("#gasto").val(datos_tabla_gastos[0]);
    $("#u_medida").val(datos_tabla_gastos[3]);
    $("#titulo_gasto").val(datos_tabla_gastos[2]);
});

$(".borrar_precompromiso_detalle").on( "click", function () {
    console.log($("#datos_tabla_precompromiso").val());
});

$("#guardar_precompromiso_detalle").on( "click", function() {

    $(".forma_precompromiso").validate({
        focusInvalid: true,
        rules: {
            tipo_precompromiso: {
                required: true
            },
            proveedor: {
                required: true
            },
            tipo_garantia: {
                required: true
            },
            porciento_garantia: {
                required: true
            },
            importe_civil: {
                required: true
            },
            lugar_adquisicion: {
                required: true
            },
            almacen: {
                required: true
            },
            tipo_radio: {
                required: true
            },
            ley: {
                required: true
            },
            articulo: {
                required: true
            },
            plurianualidad: {
                required: true
            },
            normas: {
                required: true
            },
            registros: {
                required: true
            },
            capacitacion: {
                required: true
            },
            fecha_solicitud: {
                required: true,
                date: true
            },
            fecha_entrega: {
                required: true,
                date: true
            },
            lugar_entrega: {
                required: true
            },
            numero_plazo: {
                required: true
            },
            tipo_plazo: {
                required: true
            },
            anexos: {
                required: true
            },
            concepto_especifico:{
                required: true
            },
            descripcion_general: {
                required: true
            },
            input_nivel1: {
                required: true
            },
            input_nivel2: {
                required: true
            },
            input_nivel3: {
                required: true
            },
            input_nivel4: {
                required: true
            },
            input_nivel5: {
                required: true
            },
            input_nivel6: {
                required: true
            },
            gasto: {
                required: true
            },
            titulo_gasto: {
                required: true
            },
            u_medida: {
                required: true
            },
            cantidad: {
                required: true,
                number: true
            },
            precio: {
                required: true,
                number: true
            },
            nombre_persona: {
                required: true
            },
            no_empleado: {
                required: true
            },
            puesto: {
                required: true
            },
            area: {
                required: true
            },
            clave: {
                required: true
            },
            forma_pago: {
                required: true
            },
            transporte: {
                required: true
            },
            grupo_jerarquico: {
                required: true
            }
        },
        //"* Este campo es obligatorio"
        //"* Este campo debe de ser un número positivo"
        //"Formato Correcto AAAA-MM-DD"
        messages: {
            tipo_precompromiso: {
                required: "* Este campo es obligatorio"
            },
            proveedor: {
                required: "* Este campo es obligatorio"
            },
            tipo_garantia: {
                required: "* Este campo es obligatorio"
            },
            porciento_garantia: {
                required: "* Este campo es obligatorio"
            },
            importe_civil: {
                required: "* Este campo es obligatorio"
            },
            lugar_adquisicion: {
                required: "* Este campo es obligatorio"
            },
            almacen: {
                required: "* Este campo es obligatorio"
            },
            tipo_radio: {
                required: "* Este campo es obligatorio"
            },
            ley: {
                required: "* Este campo es obligatorio"
            },
            articulo: {
                required: "* Este campo es obligatorio"
            },
            plurianualidad: {
                required: "* Este campo es obligatorio"
            },
            normas: {
                required: "* Este campo es obligatorio"
            },
            registros: {
                required: "* Este campo es obligatorio"
            },
            capacitacion: {
                required: "* Este campo es obligatorio"
            },
            fecha_solicitud: {
                required: "* Este campo es obligatorio",
                date: "Formato Correcto AAAA-MM-DD"
            },
            fecha_entrega: {
                required: "* Este campo es obligatorio",
                date: "Formato Correcto AAAA-MM-DD"
            },
            lugar_entrega: {
                required: "* Este campo es obligatorio"
            },
            numero_plazo: {
                required: "* Este campo es obligatorio"
            },
            tipo_plazo: {
                required: "* Este campo es obligatorio"
            },
            anexos: {
                required: "* Este campo es obligatorio"
            },
            concepto_especifico: {
                required: "* Este campo es obligatorio"
            },
            descripcion_general: {
                required: "* Este campo es obligatorio"
            },
            input_nivel1: {
                required: "* Este campo es obligatorio"
            },
            input_nivel2: {
                required: "* Este campo es obligatorio"
            },
            input_nivel3: {
                required: "* Este campo es obligatorio"
            },
            input_nivel4: {
                required: "* Este campo es obligatorio"
            },
            input_nivel5: {
                required: "* Este campo es obligatorio"
            },
            input_nivel6: {
                required: "* Este campo es obligatorio"
            },
            gasto: {
                required: "* Este campo es obligatorio"
            },
            titulo_gasto: {
                required: "* Este campo es obligatorio"
            },
            u_medida: {
                required: "* Este campo es obligatorio"
            },
            cantidad: {
                required: "* Este campo es obligatorio",
                number: "* Este campo debe de ser un número positivo"
            },
            precio: {
                required: "* Este campo es obligatorio",
                number: "* Este campo debe de ser un número positivo"
            },
            nombre_persona: {
                required: "* Este campo es obligatorio",
            },
            no_empleado: {
                required: "* Este campo es obligatorio",
            },
            puesto: {
                required: "* Este campo es obligatorio",
            },
            area: {
                required: "* Este campo es obligatorio",
            },
            clave: {
                required: "* Este campo es obligatorio",
            },
            forma_pago: {
                required: "* Este campo es obligatorio",
            },
            transporte: {
                required: "* Este campo es obligatorio",
            },
            grupo_jerarquico: {
                required: "* Este campo es obligatorio",
            }
        }
    }).form();

    if($(".forma_precompromiso").valid()){
        var map = {};
        $(":input").each(function() {
            map[$(this).attr("name")] = $(this).val();
        });

        if($('#check_firme').is(':checked')){
            map.check_firme = 1;
        } else {
            map.check_firme = 0;
        }

        //map.descripcion_detalle = $("#descripcion_detalle").html();

        map.nivel1 = $("#input_nivel1").val();
        map.nivel2 = $("#input_nivel2").val();
        map.nivel3 = $("#input_nivel3").val();
        map.nivel4 = $("#input_nivel4").val();
        map.nivel5 = $("#input_nivel5").val();
        map.nivel6 = $("#input_nivel6").val();

        map.tipo_radio = $('input[name=tipo_radio]:checked').val();
        map.anexos = $('input[name=anexos]:checked').val();

        map.iva = $('input[name=iva]:checked').val();

        $.ajax({
            url: '/ciclo/insertar_detalle',
            method: 'POST',
            dataType: 'json',
            data: map,
            success: function(s){
                if(s.estatus == "ok") {
                    $("#resultado_insertar_detalle").html(s.mensaje);
                    $("#input_nivel1").val('');
                    $("#input_nivel2").val('');
                    $("#input_nivel3").val('');
                    $("#input_nivel4").val('');
                    $("#input_nivel5").val('');
                    $("#input_nivel6").val('');
                    $("#gasto").val('');
                    $("#titulo_gasto").val('');
                    $("#u_medida").val('');
                    $("#cantidad").val('');
                    $("#precio").val('');
                    $("#descripcion_detalle").val('');
                    refrescarDetalle();

                }
                else {
                    $("#resultado_insertar_detalle").html(s.mensaje);
                    refrescarDetalle();
                }
            },
            error: function(e){
                console.log(e.responseText);
            }
        });
    }

});

$("#guardar_precompromiso_viaticos_detalle").on( "click", function() {

        var map = {};

        $(":input").each(function() {
            map[$(this).attr("name")] = $(this).val();
        });

        $.ajax({
            url: '/ciclo/insertar_detalle_viaticos',
            method: 'POST',
            dataType: 'json',
            data: map,
            success: function(s){
                $("#resultado_insertar_viaticos_detalle").html(s.mensaje);
                refrescarDetalleViaticos();
            },
            error: function(e){
                console.log(e.responseText);
            }
        });

});

$("#guardar_precompromiso").on( "click", function(e) {
    /*$(".forma_precompromiso").validate({
        focusInvalid: true,
        rules: {
            tipo_precompromiso: {
                required: true
            },
            proveedor: {
                required: true
            },
            tipo_garantia: {
                required: true
            },
            porciento_garantia: {
                required: true
            },
            importe_civil: {
                required: true
            },
            lugar_adquisicion: {
                required: true
            },
            almacen: {
                required: true
            },
            tipo_radio: {
                required: true
            },
            ley: {
                required: true
            },
            articulo: {
                required: true
            },
            plurianualidad: {
                required: true
            },
            normas: {
                required: true
            },
            registros: {
                required: true
            },
            capacitacion: {
                required: true
            },
            fecha_solicitud: {
                required: true,
                date: true
            },
            fecha_entrega: {
                required: true,
                date: true
            },
            lugar_entrega: {
                required: true
            },
            numero_plazo: {
                required: true
            },
            tipo_plazo: {
                required: true
            },
            anexos: {
                required: true
            },
            concepto_especifico:{
                required: true
            },
            descripcion_general: {
                required: true
            },
            nombre_persona: {
                required: true
            },
            no_empleado: {
                required: true
            },
            puesto: {
                required: true
            },
            area: {
                required: true
            },
            clave: {
                required: true
            },
            forma_pago: {
                required: true
            },
            transporte: {
                required: true
            },
            grupo_jerarquico: {
                required: true
            },
            gasto: {
                required: false
            },
            titulo_gasto: {
                required: false
            },
            u_medida: {
                required: false
            },
            cantidad: {
                required: false
            },
            precio: {
                required: false
            }
        },
        messages: {
            tipo_precompromiso: {
                required: "* Este campo es obligatorio"
            },
            proveedor: {
                required: "* Este campo es obligatorio"
            },
            tipo_garantia: {
                required: "* Este campo es obligatorio"
            },
            porciento_garantia: {
                required: "* Este campo es obligatorio"
            },
            importe_civil: {
                required: "* Este campo es obligatorio"
            },
            lugar_adquisicion: {
                required: "* Este campo es obligatorio"
            },
            almacen: {
                required: "* Este campo es obligatorio"
            },
            tipo_radio: {
                required: "* Este campo es obligatorio"
            },
            ley: {
                required: "* Este campo es obligatorio"
            },
            articulo: {
                required: "* Este campo es obligatorio"
            },
            plurianualidad: {
                required: "* Este campo es obligatorio"
            },
            normas: {
                required: "* Este campo es obligatorio"
            },
            registros: {
                required: "* Este campo es obligatorio"
            },
            capacitacion: {
                required: "* Este campo es obligatorio"
            },
            fecha_solicitud: {
                required: "* Este campo es obligatorio",
                date: "Formato Correcto AAAA-MM-DD"
            },
            fecha_entrega: {
                required: "* Este campo es obligatorio",
                date: "Formato Correcto AAAA-MM-DD"
            },
            lugar_entrega: {
                required: "* Este campo es obligatorio"
            },
            numero_plazo: {
                required: "* Este campo es obligatorio"
            },
            tipo_plazo: {
                required: "* Este campo es obligatorio"
            },
            anexos: {
                required: "* Este campo es obligatorio"
            },
            concepto_especifico: {
                required: "* Este campo es obligatorio"
            },
            descripcion_general: {
                required: "* Este campo es obligatorio"
            },
            nombre_persona: {
                required: "* Este campo es obligatorio"
            },
            no_empleado: {
                required: "* Este campo es obligatorio"
            },
            puesto: {
                required: "* Este campo es obligatorio"
            },
            area: {
                required: "* Este campo es obligatorio"
            },
            clave: {
                required: "* Este campo es obligatorio"
            },
            forma_pago: {
                required: "* Este campo es obligatorio"
            },
            transporte: {
                required: "* Este campo es obligatorio"
            },
            grupo_jerarquico: {
                required: "* Este campo es obligatorio"
            },
            gasto: {
                required: false
            },
            titulo_gasto: {
                required: false
            },
            u_medida: {
                required: false
            },
            cantidad: {
                required: false
            },
            precio: {
                required: false
            }
        }
    }).form();
    if($(".forma_precompromiso").valid()) {*/
        e.preventDefault();

        var map = {};
        $(":input").each(function () {
            map[$(this).attr("name")] = $(this).val();
        });

        if ($('#check_firme').is(':checked')) {
            map.check_firme = 1;
        } else {
            map.check_firme = 0;
        }

        if ($('#zona_marginada_check').is(':checked')) {
            map.zona_marginada_check = 1;
        } else {
            map.zona_marginada_check = 0;
        }

        if ($('#zona_mas_economica_check').is(':checked')) {
            map.zona_mas_economica_check = 1;
        } else {
            map.zona_mas_economica_check = 0;
        }

        if ($('#zona_menos_economica_check').is(':checked')) {
            map.zona_menos_economica_check = 1;
        } else {
            map.zona_menos_economica_check = 0;
        }

        map.tipo_radio = $('input[name=tipo_radio]:checked').val();
        map.anexos = $('input[name=anexos]:checked').val();

        $.ajax({
            url: '/ciclo/insertar_precompromiso',
            method: 'POST',
            dataType: 'json',
            data: map,
            success: function (s) {
                $("#resultado_insertar_caratula").html(s.mensaje_insertar);
                $("#guardar_precompromiso").prop('disabled', true);
            },
            error: function (e) {
                console.log(e.responseText);
            }
        });
    //}
});

$("#verificar_presupuesto").on( "click", function(e) {
    e.preventDefault();

    $("#resultado_verificado").html('<img src="../img/ajax-loader.gif" style="width: 50px;" />');

    var map = {};
    $(":input").each(function() {
        map[$(this).attr("name")] = $(this).val();
    });

    if($('#check_firme').is(':checked')){
        map.check_firme = 1;
    } else {
        map.check_firme = 0;
    }

    map.tipo_radio = $('input[name=tipo_radio]:checked').val();
    map.anexos = $('input[name=anexos]:checked').val();

    $.ajax({
        url: '/ciclo/verificar_presupuesto_precompromiso',
        method: 'POST',
        dataType: 'json',
        data: map,
        success: function(s){
            if(s.mensaje.indexOf("danger") > -1) {
                $('#check_firme').prop('disabled', true);
            } else {
                $('#check_firme').prop('disabled', false);
            }

            $("#resultado_verificado").html(s.mensaje);
            $("#guardar_precompromiso").prop('disabled', false);
        },
        error: function(e){
            console.log(e.responseText);
        }
    });
});

$("#borrar_cambios").on( "click", function() {
    var map = {};
    $(":input").each(function() {
        map[$(this).attr("name")] = $(this).val();
    });

    if($('#check_firme').is(':checked')){
        map.check_firme = 1;
    } else {
        map.check_firme = 0;
    }

    map.tipo_radio = $('input[name=tipo_radio]:checked').val();

    $.ajax({
        url: '/ciclo/borrar_cambios_precompromiso',
        method: 'POST',
        dataType: 'json',
        data: map,
        success: function(s){
            if(s.mensaje == "ok") {
                location.href="/ciclo/precompromiso";
            }
        },
        error: function(e){
            console.log(e.responseText);
        }
    });
});

$('.modal_borrar').on('show.bs.modal', function (event) {
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this);
    modal.find('#borrar_viatico_precompromiso_hidden').val(datos_tabla_precompromiso[0]);
});

$('.modal_borrar_detalle_viaticos').on('show.bs.modal', function (event) {
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this);
    modal.find('#borrar_viatico_precompromiso_hidden').val(datos_tabla_viaticos_precompromiso[0]);
});

$('#tabla_datos_viaticos_precompromiso tbody').on( 'click', 'tr', function () {
    datos_tabla_viaticos_precompromiso = $('#tabla_datos_viaticos_precompromiso').DataTable().row( this ).data();
    $("#borrar_viatico_precompromiso_hidden").val(datos_tabla_viaticos_precompromiso[0]);
});

$("#elegir_cancelar_precompromiso").click(function() {
    var id_precompromiso = $("#borrar_precompromiso_hidden").val();

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/ciclo/borrar_detalle_precompromiso",
        data: {
            id_precompro: id_precompromiso
        }
    })
        .done(function( data ) {
            console.log(data);
            if(data.mensaje == "ok") {
                refrescarDetalle();
            }
            else {
                alert("Ha ocurrido un error");
            }
        })
        .fail(function(e) {
            console.log(e.responseText);
            // If there is no communication between the server, show an error
            console.log("Error");
            //alert( "error occured" );
        });
});

$("#elegir_borrar_viatico").click(function() {
    var id_viatico = $("#borrar_viatico_precompromiso_hidden").val();

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/ciclo/borrar_detalle_viaticos_precompromiso",
        data: {
            id_viatico: id_viatico
        }
    })
        .done(function( data ) {
            if(data.mensaje == "ok") {
                refrescarDetalleViaticos();
            }
            else {
                alert("Ha ocurrido un error");
            }
        })
        .fail(function(e) {
            console.log(e.responseText);
            // If there is no communication between the server, show an error
            console.log("Error");
            //alert( "error occured" );
        });
});

$('#tabla_proveedores tbody').on( 'click', 'tr', function () {
    datos_proveedor = $('#tabla_proveedores').DataTable().row( this ).data();
});

$('#modal_proveedores').on('hidden.bs.modal', function () {
    $('#proveedor').val(datos_proveedor[2]);
    $('#id_proveedor').val(datos_proveedor[1]);
});

$("#iva").on( "change", function(){
    refrescarDetalle();
});

$("#firma1_check").change(function() {
    if(this.checked) {
        $("#firma1").val(1);
    }
    else {
        $("#firma1").val(0);
    }
});

$("#firma2_check").change(function() {
    if(this.checked) {
        $("#firma2").val(1);
    }
    else {
        $("#firma2").val(0);
    }
});

$("#firma3_check").change(function() {
    if(this.checked) {
        $("#firma3").val(1);
    }
    else {
        $("#firma3").val(0);
    }
});

$("#tipo_precompromiso").on("change", function() {
    var tipo = $("#tipo_precompromiso option:selected").text();
    if(tipo == "Fondo Revolvente" || tipo == "Gastos a Comprobar") {
        deshabilitar_campos();
        habilitar_fondos();
    } else if(tipo == "Viáticos Nacionales" || tipo == "Viáticos Internacionales") {
        deshabilitar_campos();
        habilitar_fondos();
        hablitar_viaticos();
    } else if(tipo == "Honorarios") {
        deshabilitar_campos();
        habilitar_precompromiso();
        $('input[name=iva]').filter('[value=1]').prop('checked', true);
        $('input[name=iva]').prop('disabled', true);
    } else {
        deshabilitar_campos();
        habilitar_precompromiso();
    }
});

function refrescarDetalle() {
    $.ajax({
        url: '/ciclo/tabla_detalle_AJAX',
        dataType: 'json',
        method: 'POST',
        data: {
          precompromiso: $("#ultimo_pre").val()
        },
        success: function(s){
            $('#tabla_datos_precompromiso').dataTable().fnClearTable();
            var total = 0;
            for(var i = 0; i < s.length; i++) {
                total += parseFloat(s[i][13]);
                $('#tabla_datos_precompromiso').dataTable().fnAddData([
                    s[i][0],
                    s[i][1],
                    s[i][2],
                    s[i][3],
                    s[i][4],
                    s[i][5],
                    s[i][6],
                    s[i][7],
                    s[i][8],
                    s[i][9],
                    "$"+$.number( s[i][10], 2 ),
                    "<div class='table-formant-sign'>$"+"</div><div class='table-formant-coin'>"+$.number( s[i][11], 2 )+"</div>",
                    "<div class='table-formant-sign'>$"+"</div><div class='table-formant-coin'>"+$.number( s[i][12], 2 )+"</div>",
                    "<div class='table-formant-sign'>$"+"</div><div class='table-formant-coin'>"+$.number( s[i][13], 2 )+"</div>",
                    s[i][14],
                    s[i][15],
                    s[i][16]
                ]);
            } // End For

            $("#suma_total").html("Total"+ "<span style='color:#848484;'>" + " $ "+$.number( total, 2 )+ "</span>");

            $("#total_hidden").val(total);
        },
        error: function(e){
            console.log(e.responseText);
        }
    });
}

function refrescarDetalleViaticos() {
    $.ajax({
        url: '/ciclo/tabla_detalle_viaticos_AJAX',
        dataType: 'json',
        method: 'POST',
        data: {
            precompromiso: $("#ultimo_pre").val()
        },
        success: function(s){
            $('#tabla_datos_viaticos_precompromiso').dataTable().fnClearTable();
            var total = 0;
            for(var i = 0; i < s.length; i++) {
                total += parseFloat(s[i][6]);
                $('#tabla_datos_viaticos_precompromiso').dataTable().fnAddData([
                    s[i][0],
                    s[i][1],
                    s[i][2],
                    s[i][3],
                    s[i][4],
                    "$"+$.number( s[i][5], 2 ),
                    "$"+$.number( s[i][6], 2 ),
                    s[i][7]
                ]);
            } // End For

            $("#suma_total_viaticos").html("Total"+ "<span style='color:#848484;'>" + " $ "+$.number( total, 2 )+ "</span>");

            $("#total_viaticos_hidden").val(total);
        },
        error: function(e){
            console.log(e.responseText);
        }
    });
}

function habilitar_precompromiso() {
    $("#tipo_garantia").prop('disabled', false);
    $("#porciento_garantia").prop('disabled', false);
    $("#importe_civil").prop('disabled', false);
    $("#lugar_adquisicion").prop('disabled', false);
    $("#almacen").prop('disabled', false);
    $("#ley").prop('disabled', false);
    $("#articulo").prop('disabled', false);
    $("#plurianualidad").prop('disabled', false);
    $("#normas").prop('disabled', false);
    $("#registros").prop('disabled', false);
    $("#capacitacion").prop('disabled', false);
    $("#fecha_autoriza").prop('disabled', false);
    $("#fecha_entrega").prop('disabled', false);
    $("#lugar_entrega").prop('disabled', false);
    $("#numero_plazo").prop('disabled', false);
    $("#tipo_plazo").prop('disabled', false);
    $("#anexos").prop('disabled', false);
    $("#concepto_especifico").prop('disabled', false);
    $('input[name=iva]').filter('[value=0]').prop('checked', true);
    $('input[name=iva]').prop('disabled', false);
    $( ".forma_caratula_normal" ).show();
    $( ".forma_fondo" ).hide();
    $( ".forma_viaticos" ).hide();
}

function habilitar_fondos() {
    $("#nombre_persona").prop('disabled', false);
    $("#clave").prop('disabled', false);
    $('input[name=iva]').filter('[value=0]').prop('checked', true);
    $('input[name=iva]').prop('disabled', true);
    $( ".forma_caratula_normal" ).hide();
    $( ".forma_fondo" ).show();
    $( ".forma_viaticos" ).hide();
}

function hablitar_viaticos() {
    $("#forma_pago").prop('disabled', false);
    $("#transporte").prop('disabled', false);
    $("#grupo_jerarquico").prop('disabled', false);
    $("#zona_marginada_check").prop('disabled', false);
    $("#zona_mas_economica_check").prop('disabled', false);
    $("#zona_menos_economica_check").prop('disabled', false);
    $("#transporte_detalle").prop('disabled', false);
    $("#cuota_diaria").prop('disabled', false);
    $("#fecha_inicio").prop('disabled', false);
    $("#fecha_termina").prop('disabled', false);
    $("#dias").prop('disabled', false);
    $("#lugar_comision").prop('disabled', false);
    $('input[name=iva]').filter('[value=0]').prop('checked', true);
    $('input[name=iva]').prop('disabled', true);
    $( ".forma_caratula_normal" ).hide();
    $( ".forma_fondo" ).show();
    $( ".forma_viaticos" ).show();
}

function deshabilitar_campos() {
    $(":input").each(function() {
        $(this).prop('disabled', true);
    });
    $("#tipo_precompromiso").prop('disabled', false);
    $("#fecha_solicitud").prop('disabled', false);
    $('input[type="radio"]').prop('disabled', false);
    $("#input_nivel1").prop('disabled', false);
    $("#input_nivel2").prop('disabled', false);
    $("#input_nivel3").prop('disabled', false);
    $("#input_nivel4").prop('disabled', false);
    $("#input_nivel5").prop('disabled', false);
    $("#input_nivel6").prop('disabled', false);
    $("#descripcion_general").prop('disabled', false);
    $("#concepto_especifico").prop('disabled', false);
    $("#firma1_check").prop('disabled', false);
    $("#firma2_check").prop('disabled', false);
    $("#firma3_check").prop('disabled', false);
    $("#gasto").prop('disabled', false);
    $("#titulo_gasto").prop('disabled', false);
    $("#u_medida").prop('disabled', false);
    $("#cantidad").prop('disabled', false);
    $("#precio").prop('disabled', false);
    $("#iva-detalle").prop('disabled', false);
    $("#guardar_precompromiso_detalle").prop('disabled', false);
    $("#verificar_presupuesto").prop('disabled', false);
    $("#guardar_precompromiso_viaticos_detalle").prop('disabled', false);
    $("#fecha_autoriza").prop('disabled', false);
    $( "select[name='tabla_datos_precompromiso_length']" ).prop('disabled', false);
    $( "select[name='tabla_datos_viaticos_precompromiso_length']" ).prop('disabled', false);
    $( "input[type='search']" ).prop('disabled', false);
    $( ".formas" ).prop('disabled', false);
    $( "#buscar_nivel1" ).prop('disabled', false);
    $( ".close" ).prop('disabled', false);
    $( ".btn" ).prop('disabled', false);
    $( "#proveedor" ).prop('disabled', false);
    $("#editar_cantidad_precompromiso").prop('disabled', false);
    $("#iva_editar").prop('disabled', false);
    $("#editar_precio_precompromiso").prop('disabled', false);
    $("#descripcion_detalle").prop('disabled', false);

    $("#guardar_precompromiso").prop('disabled', true);
}

$('#tabla_centro_costos tbody').on( 'click', 'tr', function () {
    datos_centro_costo = $('#tabla_centro_costos').DataTable().row( this ).data();
});

$('#modal_centro_costo').on('hidden.bs.modal', function () {
    $('#clave').val(datos_centro_costo[0]);
});

$('#tabla_beneficiarios tbody').on( 'click', 'tr', function () {
    datos_beneficiario = $('#tabla_beneficiarios').DataTable().row( this ).data();
});

$('#modal_beneficiarios').on('hidden.bs.modal', function () {
    $('#id_persona').val(datos_beneficiario[0]);
    $('#no_empleado').val(datos_beneficiario[1]);
    $('#nombre_persona').val(datos_beneficiario[2]);
    $('#area').val(datos_beneficiario[3]);
    $('#puesto').val(datos_beneficiario[4]);
});

$("#elegir_editar_precompromiso").click(function() {
    var fecha = $("#fecha_solicitud").val();
    var id_precompromiso = $("#borrar_precompromiso_hidden").val();
    var cantidad = $("#editar_cantidad_precompromiso").val();
    var precio = $("#editar_precio_precompromiso").val();
    var iva = $('input[name=iva_editar]:checked').val();

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/ciclo/editar_detalle_precompromiso",
        data: {
            id_precompromiso: id_precompromiso,
            cantidad: cantidad,
            fecha: fecha,
            precio: precio,
            iva: iva
        }
    })
        .done(function( data ) {
            $("#resultado_insertar_detalle").html(data.mensaje);
            $("#guardar_precompromiso").prop('disabled', true);
            refrescarDetalle();
        })
        .fail(function(e) {
            console.log(e.responseText);
        });
});